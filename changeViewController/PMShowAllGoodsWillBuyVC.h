//
//  PMShowAllGoodsWillBuyVC.h
//  changeViewController
//
//  Created by pmit on 14/12/18.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMSureOrderTVCell.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface PMShowAllGoodsWillBuyVC : UITableViewController
@property(nonatomic,strong)NSArray *listArray;

@end
