//
//  SWShopNameViewController.m
//  changeViewController
//
//  Created by P&M on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "SWShopNameViewController.h"
#import "PMToastHint.h"

@interface SWShopNameViewController ()

@end

@implementation SWShopNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"店铺名称";
    self.view.backgroundColor = HDVGPageBGGray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self createShopNameUI];
    [self createSaveControllerUI];
}

- (void)createShopNameUI
{
    UIView *nameView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 44)];
    nameView.backgroundColor = [UIColor whiteColor];
    self.nameView = nameView;
    [self.view addSubview:nameView];
    
    // 分隔线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    line1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [nameView.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(0, 44 - 0.5, WIDTH, HeightRate(1));
    line2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [nameView.layer addSublayer:line2];
    
    // 店铺名称输入框
    self.nameTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(36), 7, WIDTH - WidthRate(72), 30)];
    self.nameTF.delegate = self;
    self.nameTF.borderStyle = UITextBorderStyleNone;
    self.nameTF.tag = 1;
    self.nameTF.text = [self.shopDataDic objectNullForKey:@"shopName"];
    self.nameTF.placeholder = @"请输入店铺名称";
    self.nameTF.textColor = HDVGFontColor;
    self.nameTF.textAlignment = NSTextAlignmentLeft;
    self.nameTF.font = [UIFont systemFontOfSize:15.0f];
    self.nameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.nameTF.returnKeyType = UIReturnKeyDone;
    self.nameTF.keyboardType = UIKeyboardTypeDefault;
    [nameView addSubview:self.nameTF];
}

- (void)createSaveControllerUI
{
    // 创建保存按钮
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(shopNameSaveButtonClick:)];
    
    [saveBtn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR,NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = saveBtn;
}

// 按下Done，让软键盘回收
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nameTF resignFirstResponder];
}


#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([[[UITextInputMode currentInputMode]primaryLanguage] isEqualToString:@"emoji"]) {
        return NO;
    }
//    if ([self isContainsEmoji:textField.text])
//    {
//        return NO;
//    }
    
    if (self.nameTF.tag == 1 && textField == self.nameTF && textField.text.length - range.length + string.length > 12) {
        return NO;
    }
    
    

    
    return YES;
}

#pragma mark - 保存按钮响应事件
- (void)shopNameSaveButtonClick:(UIButton *)button
{
    [self.nameTF resignFirstResponder];
    
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    // 判断是否为空
    if (self.nameTF.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入店铺名称"];
        return;
    }
    
    if (self.nameTF.text.length > 12) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"店铺名称长度应不大于12个字符"];
        return;
    }
    NSDictionary *cashWithdrawals = @{@"PM_SID":userInfots.PM_SID,@"id":[self.shopDataDic objectNullForKey:@"id"], @"shopName":self.nameTF.text};
    
    [[PMNetworking defaultNetworking] request:PMRequestStateShareUpdateShopHome WithParameters:cashWithdrawals callBackBlock:^(NSDictionary *dict) {
        
        if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"店铺名称设置成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
            alertView.tag = 1;
            [alertView show];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }showIndicator:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        
        if ([_nameDelegate respondsToSelector:@selector(passShopName:)])
        {
            [_nameDelegate passShopName:self.nameTF.text];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)disable_emoji:(NSString *)text
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]" options:NSRegularExpressionCaseInsensitive error:nil];
    NSString *modifiedString = [regex stringByReplacingMatchesInString:text
                                                               options:0
                                                                 range:NSMakeRange(0, [text length])
                                                          withTemplate:@""];
    return modifiedString;
}

- (BOOL)isContainsEmoji:(NSString *)string {
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}

@end
