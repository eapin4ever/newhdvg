//
//  DistributionShopViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/8.
//  Copyright (c) 2014年 wallace. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMImageCroperSingleton.h"
#import "PMUserInfos.h"

@interface DistributionShopViewController : UIViewController
+ (DistributionShopViewController *)currentVC;

@property(nonatomic,strong)PMImageCroperSingleton *portrait;

@property(nonatomic,strong)NSDictionary *shopDataDic;

- (void)getShopData;
@end
