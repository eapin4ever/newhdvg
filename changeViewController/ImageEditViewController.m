//
//  ImageEditViewController.m
//  changeViewController
//
//  Created by pmit on 15/7/1.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "ImageEditViewController.h"
#import "UIImage+KIAdditions.h"
#import "KICropImageView.h"
#import "UIView+YYExtension.h"
#import "PMMyPhoneInfo.h"

@interface ImageEditViewController ()

@property (nonatomic , strong) KICropImageView *clipImageView;
@property (nonatomic , strong) UIImage *originalImage;
@property (strong,nonatomic) UIView *bottomView;

@end

@implementation ImageEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.clipImageView];
    _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 70, WIDTH, 70)];
    _bottomView.backgroundColor = [UIColor blackColor];
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(WidthRate(30), 0, 100, 50);
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelEdit:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:cancelBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WIDTH - WidthRate(30) - 100, 0, 100, 50);
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureEdit:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:sureBtn];
    [self.view addSubview:_bottomView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    self.navigationController.navigationBarHidden = NO;
}

- (KICropImageView *)clipImageView
{
    if (!_clipImageView)
    {
        _clipImageView = [[KICropImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds] AndIsHeader:NO];
        [_clipImageView setFrame:_clipImageView.frame];
        [_clipImageView setCropSize:CGSizeMake(_clipImageView.yy_width, _clipImageView.yy_width)];
        [_clipImageView setImage:self.originalImage];
    }
    
    return _clipImageView;
}

- (instancetype)initWithOriginalImage:(UIImage *)image
{
    if (self = [super init]) {
        self.originalImage = image;
    }
    return self;
}

- (void)cancelEdit:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sureEdit:(UIButton *)sender
{
    UIImage *editImage = [_clipImageView cropImage];
    NSData *imageData = UIImageJPEGRepresentation(editImage, 0.0001);
    if ([_editDelegate respondsToSelector:@selector(passImageData:)])
    {
        [_editDelegate passImageData:imageData];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
