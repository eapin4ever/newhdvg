//
//  SafeManageViewController.m
//  changeViewController
//
//  Created by P&M on 14/12/12.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "SafeManageViewController.h"
#import "MobileAuthcodeViewController.h"

@interface SafeManageViewController ()

@end

@implementation SafeManageViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"安全管理";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createSafeManageViewUI];
    
    [self initSafeManageDataList];
}

- (void)createSafeManageViewUI
{
    // 创建安全管理 view
    self.safeManageView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 132)];
    self.safeManageView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.safeManageView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.safeManageView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 43, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.safeManageView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(WidthRate(26), 88, WIDTH, HeightRate(1));
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.safeManageView.layer addSublayer:layer3];
    
    CALayer *layer4 = [[CALayer alloc] init];
    layer4.frame = CGRectMake(0, 132 - 0.5, WIDTH, 0.5);
    layer4.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.safeManageView.layer addSublayer:layer4];
    
    
    /**
     *  从服务器获取用户数据
     */
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    NSDictionary *dict = userInfos.userDataDic;
    
    // 邮箱
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(200), 30)];
    emailLabel.backgroundColor = [UIColor clearColor];
    emailLabel.text = @"邮       箱";
    emailLabel.textColor = RGBA(75, 75, 75, 1);
    emailLabel.textAlignment = NSTextAlignmentLeft;
    emailLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.safeManageView addSubview:emailLabel];
    
    UILabel *emailNumberLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(485), 7, WidthRate(430), 30)];
    emailNumberLab.backgroundColor = [UIColor clearColor];
    
    NSString *email = [[dict objectNullForKey:@"clientUser"] objectNullForKey:@"email"];
    
    if (isNull([[dict objectNullForKey:@"clientUser"] objectNullForKey:@"email"]) || [email isEqualToString:@"null"])
    {
        
        emailNumberLab.text = @"未绑定邮箱，可到官网绑定";
        emailNumberLab.textColor = [UIColor lightGrayColor];
    }
    else
    {
        emailNumberLab.text = email;
        emailNumberLab.textColor = RGBA(81, 81, 81, 1);
    }
    
    emailNumberLab.textAlignment = NSTextAlignmentRight;
    emailNumberLab.font = [UIFont systemFontOfSize:15.0f];
    [self.safeManageView addSubview:emailNumberLab];
    
    
    // 手机号码
    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 51, WidthRate(200), 30)];
    phoneLabel.backgroundColor = [UIColor clearColor];
    phoneLabel.text = @"手机号码";
    phoneLabel.textColor = RGBA(75, 75, 75, 1);
    phoneLabel.textAlignment = NSTextAlignmentLeft;
    phoneLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.safeManageView addSubview:phoneLabel];
    
    self.mobileNumberLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(485), 51, WidthRate(430), 30)];
    self.mobileNumberLab.backgroundColor = [UIColor clearColor];
    self.mobileNumberLab.text = [[dict objectNullForKey:@"clientUser"] objectNullForKey:@"mobile"];
    self.mobileNumberLab.textColor = RGBA(81, 81, 81, 1);
    self.mobileNumberLab.textAlignment = NSTextAlignmentRight;
    self.mobileNumberLab.font = [UIFont systemFontOfSize:15.0f];
    [self.safeManageView addSubview:self.mobileNumberLab];
    
    // 手机号码按钮
    UIButton *mobileButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mobileButton.frame = CGRectMake(0, 45, WIDTH, 42);
    [mobileButton addTarget:self action:@selector(mobileButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.safeManageView addSubview:mobileButton];
    
    UIImageView *arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 56, 20, 20)];
    [arrowImage setImage:[UIImage imageNamed:@"arrowRight"]];
    [self.safeManageView addSubview:arrowImage];
    
    
    // 登录密码
    UILabel *loginPassLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 95, WidthRate(200), 30)];
    loginPassLabel.backgroundColor = [UIColor clearColor];
    loginPassLabel.text = @"修改密码";
    loginPassLabel.textColor = RGBA(75, 75, 75, 1);
    loginPassLabel.textAlignment = NSTextAlignmentLeft;
    loginPassLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.safeManageView addSubview:loginPassLabel];
    
    // 登录密码按钮
    UIButton *loginPassButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginPassButton.frame = CGRectMake(0, 89, WIDTH, 42);
    [loginPassButton addTarget:self action:@selector(loginPassButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.safeManageView addSubview:loginPassButton];
    
    UIImageView *arrowImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 100, 20, 20)];
    [arrowImage2 setImage:[UIImage imageNamed:@"arrowRight"]];
    [self.safeManageView addSubview:arrowImage2];
}

- (void)mobileButtonClick:(id)sender
{
    // 手机号码
    MobileAuthcodeViewController *mobileAuthcodeVC = [[MobileAuthcodeViewController alloc] init];
    NSString *string = @"changeMobileAuthcode";
    mobileAuthcodeVC.mobileOrBankAuthcode = string;
    [self.navigationController pushViewController:mobileAuthcodeVC animated:YES];
}

- (void)loginPassButtonClick:(id)sender
{
    // 登录密码
    NewPassViewController *newPassVC = [[NewPassViewController alloc] init];
    [self.navigationController pushViewController:newPassVC animated:YES];
}


#pragma mark - 设置我的账户安全管理假数据
- (void)initSafeManageDataList
{
    if (!self.safeManageArray) {
        self.safeManageArray = [NSMutableArray array];
        
        for (int i = 0; i < 3; i++) {
            NSMutableDictionary *dict = [@{@"email":@"hengda@163.com", @"mobile":@"13888888888"} mutableCopy];
            
            [self.safeManageArray addObject:dict];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
