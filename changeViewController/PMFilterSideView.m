//
//  PMFilterSideView.m
//  testTableView
//
//  Created by pmit on 14/12/10.
//  Copyright (c) 2014年 pmit. All rights reserved.
//

#import "PMFilterSideView.h"
#import "PMToastHint.h"

@interface PMFilterSideView ()
@property(nonatomic,strong)NSMutableArray *openArray;
@property(nonatomic,strong)NSMutableArray *sectionArrowImageViewArray;
@property(nonatomic,strong)UIView *maskView;


@end

@implementation PMFilterSideView

- (void)viewDidLoad
{
    [super viewDidLoad];
    //初始化数据
    self.openArray = [NSMutableArray array];
    self.sectionArrowImageViewArray = [NSMutableArray array];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewHide:)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipe];
    self.selectedRowArr = [NSMutableArray array];
//  默认支持多项多选的。但是目前服务器只支持单项筛选
    for (int i = 0; i < self.sectionTitleArray.count; i++)
    {
        [self.selectedRowArr addObject:[NSIndexPath indexPathForRow:-1 inSection:i]];
    }
    
    [self initMaskView];

    [self initFilterSideView];
    [self initHeaderView];

}

#pragma mark - maskView
- (void)initMaskView
{
    UIView *maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.maskView = maskView;
    maskView.backgroundColor = [UIColor blackColor];
    maskView.alpha = 0;
    maskView.userInteractionEnabled = NO;
    
    [self.view addSubview:maskView];
}


#pragma mark - 显示/隐藏筛选栏
- (void)showFilterSideView
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect rect = self.filterView.frame;
        rect.origin.x = WIDTH - WidthRate(580);
        self.filterView.frame = rect;
        
        self.maskView.alpha = 0.6;
    }];
}

- (void)hideFilterSideViewFinish:(finishActionBlock)action
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect rect = self.filterView.frame;
        rect.origin.x = WIDTH;
        self.filterView.frame = rect;
        
        self.maskView.alpha = 0;
    }completion:^(BOOL finished) {
        if(action)
            action();
    } ];
}



#pragma mark - 创建多条件筛选侧边栏
- (void)initFilterSideView
{
    UITableView *filterView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH, 0, WidthRate(580), HEIGHT-64) style:UITableViewStylePlain];
    self.filterView = filterView;
    filterView.delegate = self;
    filterView.dataSource = self;
    
    [filterView registerClass:[PMSearchFilterCell class] forCellReuseIdentifier:@"Cell"];
    
    filterView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:filterView];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.sectionTitleArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.openArray containsObject:@(section)])
    {
        NSArray *arr = [self.allDataDic objectNullForKey:self.sectionTitleArray[section]];
        return arr.count;
    }
    else
        return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray *arr = [self.allDataDic objectNullForKey:self.sectionTitleArray[indexPath.section]];
    

    PMSearchFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [cell createUI];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];//如果这个没有设置，cell有可能会出现不明的边线
    cell.textLabel.text = arr[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.separatorInset = UIEdgeInsetsMake(0, WidthRate(60), 0, 0);
    
    
    //辅助视图上的钩 //目前只支持单项筛选 默认使用0位置
    if(self.selectedRowArr[0] == indexPath)
    {
        cell.accessoryView.hidden = NO;
    }
    else
    {
        cell.accessoryView.hidden = YES;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    headerView.backgroundColor = HDVGRed;
    [headerView.layer setBorderColor:RGBA(200, 199, 204, 1).CGColor];
    [headerView.layer setBorderWidth:0.5];
    
    headerView.tag = section;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSectionAction:)];
    [headerView addGestureRecognizer:tap];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), 5, 100, 40)];
    lb.text = self.sectionTitleArray[section];
    lb.font = [UIFont systemFontOfSize:16];
    lb.textColor = [UIColor whiteColor];
    [headerView addSubview:lb];
    
    //箭头
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(520), 15, 20, 20)];
    iv.image = [UIImage imageNamed:@"arrowDown"];
    [self.sectionArrowImageViewArray addObject:iv];
    
    [headerView addSubview:iv];
    
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *selectedIndexPath = self.selectedRowArr[0];
    if(selectedIndexPath != indexPath)
    {
        //初始化默认值是-1     //全部的辅助视图先隐藏
        if(selectedIndexPath.row >= 0)
        {
            PMSearchFilterCell *cell1 = (PMSearchFilterCell *)[self.filterView cellForRowAtIndexPath:selectedIndexPath];
            cell1.accessoryView.hidden = YES;
        }
        //再将选中的cell的辅助视图显示
        PMSearchFilterCell *cell2 = (PMSearchFilterCell *)[self.filterView cellForRowAtIndexPath:indexPath];
        cell2.accessoryView.hidden = NO;
//        目前服务器只支持单项筛选
        [self.selectedRowArr replaceObjectAtIndex:0 withObject:indexPath];
        
        //单项筛选
        if(indexPath.section == 0)//品牌 brand,
        {
            self.filter = [NSString stringWithFormat:@"brand,%@",cell2.textLabel.text];
        }
        else//类目 className,
        {
            self.filter = [NSString stringWithFormat:@"className,%@",cell2.textLabel.text];
        }
    }
    else
    {
        PMSearchFilterCell *cell1 = (PMSearchFilterCell *)[self.filterView cellForRowAtIndexPath:selectedIndexPath];
        cell1.accessoryView.hidden = YES;
        [self.selectedRowArr replaceObjectAtIndex:0 withObject:[NSIndexPath indexPathForRow:-1 inSection:indexPath.section]];
    }
}



#pragma mark - headerView
- (void)initHeaderView
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.filterView.bounds.size.width, 64)];
    
    //恢复按钮
    UIButton *recoverBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.recoverBtn = recoverBtn;
    recoverBtn.tag = 1;
    recoverBtn.frame = CGRectMake(WidthRate(360), 17, WidthRate(100), 30);
    [recoverBtn setTitle:@"收回" forState:UIControlStateNormal];
    recoverBtn.titleLabel.font = [UIFont boldSystemFontOfSize:HeightRate(28)];
    [recoverBtn setTitleColor:RGBA(101, 101, 101, 1) forState:UIControlStateNormal];
    recoverBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [headerView addSubview:recoverBtn];
    
    //确定按钮
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureBtn = sureBtn;
    sureBtn.tag = 2;
    sureBtn.frame = CGRectMake(WidthRate(460), 17, WidthRate(100), 30);
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    sureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:HeightRate(28)];
    [sureBtn setTitleColor:RGBA(189, 24, 41, 1) forState:UIControlStateNormal];
    sureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [headerView addSubview:sureBtn];
    
    
    self.filterView.tableHeaderView = headerView;
    
    self.filterView.tableFooterView = [[UIView alloc] init];

}



#pragma mark - 点击手势,展开和收起所选区域的rows
- (void)tapSectionAction:(UITapGestureRecognizer *)tap
{
    NSInteger section = tap.view.tag;
    NSMutableArray *changeArray = [NSMutableArray array];
    
    if([self.openArray containsObject:@(section)])
    {
        //收起该section所有row
        [self.openArray removeObject:@(section)];
        for (int i = 0; i<[self.filterView numberOfRowsInSection:section]; i++)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
            [changeArray addObject:indexPath];
        }
        
        ((UIImageView *)self.sectionArrowImageViewArray[section]).image = [UIImage imageNamed:@"arrowDown"];
        
        [self.filterView deleteRowsAtIndexPaths:changeArray withRowAnimation:UITableViewRowAnimationTop];
        
    }
    else
    {
        //打开该section所有row
        [self.openArray addObject:@(section)];
        
        if (((NSArray *)[self.allDataDic objectNullForKey:self.sectionTitleArray[section]]).count == 0)
        {
            if (section == 0)
            {
               [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"品牌下暂时没有筛选条件哦"];
            }
            else
            {
                [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"类目下暂时没有筛选条件哦"];
            }
            
        }
        else
        {
            for (int i = 0; i<((NSArray *)[self.allDataDic objectNullForKey:self.sectionTitleArray[section]]).count; i++)
            {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
                [changeArray addObject:indexPath];
            }
            
            ((UIImageView *)self.sectionArrowImageViewArray[section]).image = [UIImage imageNamed:@"arrowUp"];
            
            [self.filterView insertRowsAtIndexPaths:changeArray withRowAnimation:UITableViewRowAnimationTop];
        }
        
        
    }
    
    
}

- (void)viewHide:(UISwipeGestureRecognizer *)swipe
{
    if ([_filterDelegate respondsToSelector:@selector(filterHide)])
    {
        [_filterDelegate filterHide];
    }
}


@end
