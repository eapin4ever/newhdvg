//
//  PMThemeDetailCell.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//
#define BARHI 30

#import "PMThemeDetailCell.h"

@implementation PMThemeDetailCell
{
    UILabel *_titleLB;
    UILabel *_discountLB;

}

- (void)createUI
{
    if(!_iv)
    {
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(320)- BARHI)];
        [self.contentView addSubview:_iv];
        
        UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, WidthRate(320) - BARHI, WIDTH, BARHI)];
        barView.backgroundColor = [UIColor blackColor];
        [self.contentView addSubview:barView];
        
        _titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(25), 0, WidthRate(380), BARHI)];
        _titleLB.textColor = [UIColor whiteColor];
        _titleLB.font = [UIFont systemFontOfSize:12];
        [barView addSubview:_titleLB];
        
        
        _discountLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(420), 0, WidthRate(120), BARHI)];
        _discountLB.textColor = [UIColor whiteColor];
        _discountLB.font = [UIFont systemFontOfSize:12];
        [barView addSubview:_discountLB];
        
        _timeBtn = [[UIButton alloc] initWithFrame:CGRectMake(WidthRate(555), 0, WidthRate(180), BARHI)];
        _timeBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _timeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_timeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _timeBtn.userInteractionEnabled = NO;
        [_timeBtn setImage:[UIImage imageNamed:@"time"] forState:UIControlStateNormal];
        [barView addSubview:_timeBtn];
    }
}


- (void)setTitle:(NSString *)title discount:(NSString *)discount time:(NSString *)time
{
    _titleLB.text = title;
    _discountLB.text = discount;
    [_timeBtn setTitle:[NSString stringWithFormat:@"  %@",time] forState:UIControlStateNormal];
}

@end
