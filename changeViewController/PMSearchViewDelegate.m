//
//  PMSearchViewDelegate.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMSearchViewDelegate.h"
#import "PMMyPhoneInfo.h"

@implementation PMSearchViewDelegate


#pragma mark Table view data source & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.searchHistory.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"    %@",self.searchHistory[indexPath.row]];
    
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    
    return cell;
}

//不同的section不同的head
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 50)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(10, (headerView.bounds.size.height - HeightRate(30))/2, headerView.bounds.size.width, 30)];
    
    lb.font = [UIFont boldSystemFontOfSize:18];
    
    lb.text = @"历史关键字:";
    
    
    [headerView addSubview:lb];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    footerView.backgroundColor = [UIColor whiteColor];
    //    footerView.alpha = 0.8;
    
    UIButton *clearHistoryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearHistoryBtn setTitle:@"清除历史记录" forState:UIControlStateNormal];
    [clearHistoryBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [clearHistoryBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    clearHistoryBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    clearHistoryBtn.layer.cornerRadius = 8.0;
    clearHistoryBtn.frame = CGRectMake(0, 0, WidthRate(240), 30);
    clearHistoryBtn.center = footerView.center;
    clearHistoryBtn.layer.borderWidth = 1.5;
    clearHistoryBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [clearHistoryBtn addTarget:self action:@selector(clickToClearSearchHistory:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:clearHistoryBtn];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == 0)
        return 41;
    else
        return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    //收键盘
//    [self.homePageVC.keyWordTF resignFirstResponder];
    if ([self.myDelegate respondsToSelector:@selector(searchWithWord:)])
    {
        [self.myDelegate searchWithWord:self.searchHistory[indexPath.row]];
    }
    
    
}


#pragma mark 清除历史记录
- (void)clickToClearSearchHistory:(UIButton *)sender
{
    if ([self.myDelegate respondsToSelector:@selector(clearHistoryWord)])
    {
        [self.myDelegate clearHistoryWord];
    }
}




@end
