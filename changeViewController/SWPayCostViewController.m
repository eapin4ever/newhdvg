//
//  SWPayCostViewController.m
//  changeViewController
//
//  Created by P&M on 15/6/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//


//#define ipAddress @"http://192.168.1.26:8123/api/ios/alipay/pay?"
#define ipAddress @"http://hdvg.me/api/ios/alipay/pay?"
//#define ipAddress @"http://120.24.234.76/wei/api/ios/alipay/pay?"

#import "SWPayCostViewController.h"
#import "WXApi.h"
#import "AppDelegate.h"
#import "PMToastHint.h"


@interface SWPayCostViewController () <MyAppDelegate,UITextFieldDelegate,UIAlertViewDelegate>

@property (assign,nonatomic) CGFloat fromHeight;

@end

@implementation SWPayCostViewController
{
    NSInteger _selectBtnType;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"预存加盟费";
    self.view.backgroundColor = HDVGPageBGGray;
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.myDelegate = self;
    
    _selectBtnType = 0;
    
    self.costScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50)];
    self.costScrollView.showsHorizontalScrollIndicator = NO;
    self.costScrollView.showsVerticalScrollIndicator = NO;
    self.costScrollView.contentSize = CGSizeMake(0, HeightRate(160) + 430.5);
    [self.view addSubview:self.costScrollView];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillDismiss:) name:UIKeyboardWillHideNotification object:nil];
    
    [self createDistributionInfosUI];
    [self createJoiningFeesUI];
    [self createPaymentUI];
    [self buildCarInfoView];
    [self createConfirmPaymentUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.isFromJoin) {
        self.userDataDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"userInfos"];
    }
    
    // 显示个人信息
    NSString *nameStr = [self.userDataDict objectNullForKey:@"name"];
    NSString *mobileStr = [self.userDataDict objectNullForKey:@"mobile"];
    NSString *areaStr = [self.userDataDict objectNullForKey:@"areaName"];
    NSString *orderMoney = [self.userDataDict objectNullForKey:@"orderMoney"];
    
    self.nameLB.text = [NSString stringWithFormat:@"真实姓名: %@", nameStr];
    self.mobileLB.text = [NSString stringWithFormat:@"手机号码: %@", mobileStr];
    self.areaLB.text = [NSString stringWithFormat:@"所在区域: %@", areaStr];
    
    self.costLabel.text = [NSString stringWithFormat:@"%@元", orderMoney];
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2lf", [orderMoney floatValue]];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:self.userDataDict forKey:@"userInfos"];
}

- (void)createDistributionInfosUI
{
    self.infosView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(50), WIDTH, 160)];
    self.infosView.backgroundColor = [UIColor whiteColor];
    [self.costScrollView addSubview:self.infosView];
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(300), 30)];
    infoLabel.backgroundColor = [UIColor clearColor];
    infoLabel.text = @"个人信息";
    infoLabel.textColor = [UIColor blackColor];
    infoLabel.textAlignment = NSTextAlignmentLeft;
    infoLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.infosView addSubview:infoLabel];
    
    // 分隔线（虚线）
    for (NSInteger i = 0; i < 120; i++) {
        
        CALayer *layer1 = [[CALayer alloc] init];
        layer1.frame = CGRectMake(4*i, 40, 3, 0.5);
        layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
        [self.infosView.layer addSublayer:layer1];
    }
    
    // 姓名
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 45, WidthRate(500), 30)];
    nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLB = nameLabel;
    nameLabel.textColor = HDVGFontColor;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.infosView addSubview:nameLabel];
    
    // 电话
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 80, WidthRate(500), 30)];
    mobileLabel.backgroundColor = [UIColor clearColor];
    self.mobileLB = mobileLabel;
    mobileLabel.textColor = HDVGFontColor;
    mobileLabel.textAlignment = NSTextAlignmentLeft;
    mobileLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.infosView addSubview:mobileLabel];
    
    // 区域
    UILabel *areaLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 115, WidthRate(500), 30)];
    areaLabel.backgroundColor = [UIColor clearColor];
    self.areaLB = areaLabel;
    areaLabel.textColor = HDVGFontColor;
    areaLabel.textAlignment = NSTextAlignmentLeft;
    areaLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.infosView addSubview:areaLabel];
    
    if (self.isFromJoin)
    {
        UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithTitle:@"我的微购" style:UIBarButtonItemStylePlain target:self action:@selector(backToRootClick:)];
        self.navigationItem.leftBarButtonItem = backBtnItem;
    }
}

- (void)createJoiningFeesUI
{
    self.costView = [[UIView alloc] initWithFrame:CGRectMake(0, self.infosView.frame.origin.y + self.infosView.frame.size.height + HeightRate(30), WIDTH, 40)];
    self.costView.backgroundColor = [UIColor whiteColor];
    [self.costScrollView addSubview:self.costView];
    
    UILabel *joinLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(300), 30)];
    joinLabel.backgroundColor = [UIColor clearColor];
    joinLabel.text = @"加盟费用";
    joinLabel.textColor = [UIColor blackColor];
    joinLabel.textAlignment = NSTextAlignmentLeft;
    joinLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.costView addSubview:joinLabel];
    
    // 费用
    UILabel *costLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(236), 5, WidthRate(200), 30)];
    costLabel.backgroundColor = [UIColor clearColor];
    //costLabel.text = @"1000元";
    costLabel.textColor = HDVGFontColor;
    costLabel.textAlignment = NSTextAlignmentRight;
    costLabel.font = [UIFont systemFontOfSize:14.0f];
    self.costLabel = costLabel;
    [self.costView addSubview:costLabel];
}

- (void)createPaymentUI
{
    self.paymentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.costView.frame.origin.y + self.costView.frame.size.height + HeightRate(30), WIDTH, 150)];
    self.paymentView.backgroundColor = [UIColor whiteColor];
    [self.costScrollView addSubview:self.paymentView];
    
    UILabel *paymentLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(300), 30)];
    paymentLabel.backgroundColor = [UIColor clearColor];
    paymentLabel.text = @"付款方式";
    paymentLabel.textColor = [UIColor blackColor];
    paymentLabel.textAlignment = NSTextAlignmentLeft;
    paymentLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.paymentView addSubview:paymentLabel];
    
    // 分隔线（虚线）
    for (NSInteger i = 0; i < 120; i++) {
        
        CALayer *layer1 = [[CALayer alloc] init];
        layer1.frame = CGRectMake(4*i, 40, 3, 0.5);
        layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
        [self.paymentView.layer addSublayer:layer1];
    }
    
    // 支付宝
    UIButton *zfbBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self setButtonSelected:zfbBtn];
    zfbBtn.frame = CGRectMake(WidthRate(36), 55, 100, 25);
    [zfbBtn setImage:[UIImage imageNamed:@"zhifubao_image"] forState:UIControlStateNormal];
    zfbBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    zfbBtn.tag = 1111;
    zfbBtn.layer.cornerRadius = 6.0;
    zfbBtn.layer.borderColor = HDVGRed.CGColor;
    zfbBtn.layer.borderWidth = 1.0;
    [zfbBtn addTarget:self action:@selector(paymentClick:) forControlEvents:UIControlEventTouchDown];
    [self.paymentView addSubview:zfbBtn];
    
    UIButton *wxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    wxBtn.frame = CGRectMake(WidthRate(72) + 100, 55, 100, 25);
    [wxBtn.layer setBorderColor:RGBA(180, 24, 41, 1).CGColor];
    [wxBtn setImage:[UIImage imageNamed:@"weizhifu"] forState:UIControlStateNormal];
    wxBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    wxBtn.tag = 2222;
    wxBtn.layer.cornerRadius = 6.0;
    wxBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    wxBtn.layer.borderWidth = 1.0;
    [wxBtn addTarget:self action:@selector(paymentClick:) forControlEvents:UIControlEventTouchDown];
    [self.paymentView addSubview:wxBtn];
    
    CALayer *line2 = [CALayer layer];
    line2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    line2.frame = CGRectMake(0, 95.5, WIDTH, 0.5);
    [self.paymentView.layer addSublayer:line2];
    
    UIButton *carPayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    carPayBtn.frame = CGRectMake(WidthRate(36), 110, 144, 25);
//    [carPayBtn setTitle:@"微购会员卡支付" forState:UIControlStateNormal];
    [carPayBtn setImage:[UIImage imageNamed:@"vip"] forState:UIControlStateNormal];
//    [carPayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    carPayBtn.tag = 3333;
    carPayBtn.layer.cornerRadius = 6.0;
    carPayBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    carPayBtn.layer.borderWidth = 1.0;
    [carPayBtn addTarget:self action:@selector(paymentClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.paymentView addSubview:carPayBtn];
    
    CALayer *line3 = [CALayer layer];
    line3.frame = CGRectMake(0, 149.5, WIDTH, 0.5);
    line3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.paymentView.layer addSublayer:line3];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(letKeyBoardRF:)];
    [self.paymentView addGestureRecognizer:tap];
}

- (void)buildCarInfoView
{
    self.carInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.paymentView.frame), WIDTH, 80.5)];
    self.carInfoView.backgroundColor = [UIColor whiteColor];
    
    UILabel *numLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 10, 40, 20)];
    numLB.backgroundColor = [UIColor clearColor];
    numLB.text = @"卡号";
    numLB.textColor = [UIColor blackColor];
    numLB.textAlignment = NSTextAlignmentLeft;
    numLB.font = [UIFont systemFontOfSize:16.0f];
    [self.carInfoView addSubview:numLB];
    
    self.carNumField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(36) + 40, 5, WIDTH - WidthRate(72) - 40, 30)];
    self.carNumField.placeholder = @"输入八位数卡号";
    self.carNumField.returnKeyType = UIReturnKeyDone;
    self.carNumField.textAlignment = NSTextAlignmentRight;
    self.carNumField.font = [UIFont systemFontOfSize:15.0f];
    self.carNumField.textColor = [UIColor lightGrayColor];
    self.carNumField.delegate = self;
    self.carNumField.keyboardType = UIKeyboardTypeNumberPad;
    [self.carInfoView addSubview:self.carNumField];
    
    
    CALayer *line1 = [CALayer layer];
    line1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    line1.frame = CGRectMake(0, 40, WIDTH, 0.5);
    [self.carInfoView.layer addSublayer:line1];
    
    UILabel *passLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 50.5, 40, 20)];
    passLB.backgroundColor = [UIColor clearColor];
    passLB.text = @"密码";
    passLB.textColor = [UIColor blackColor];
    passLB.textAlignment = NSTextAlignmentLeft;
    passLB.font = [UIFont systemFontOfSize:16.0f];
    [self.carInfoView addSubview:passLB];
    
    self.carPassField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(36) + 40, 45.5, WIDTH - WidthRate(72) - 40, 30)];
    self.carPassField.placeholder = @"输入会员卡上的密码";
    self.carPassField.textAlignment = NSTextAlignmentRight;
    self.carPassField.font = [UIFont systemFontOfSize:15.0f];
    self.carPassField.textColor = [UIColor lightGrayColor];
    self.carPassField.returnKeyType = UIReturnKeyDone;
    self.carPassField.delegate = self;
    self.carPassField.secureTextEntry = YES;
    [self.carInfoView addSubview:self.carPassField];
    
    [self.costScrollView addSubview:self.carInfoView];
    
    self.carInfoView.hidden = YES;
    
    CALayer *line2 = [CALayer layer];
    line2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    line2.frame = CGRectMake(0, 80, WIDTH, 0.5);
    [self.carInfoView.layer addSublayer:line2];
}

- (void)createConfirmPaymentUI
{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50, WIDTH, 50)];
    bottomView.backgroundColor = RGBA(71, 70, 70, 1);
    bottomView.tag = 920109;
    [self.view addSubview:bottomView];
    
    // 支付金额
    NSString *string = @"¥";
    CGSize moneySize = [string sizeWithFont:[UIFont boldSystemFontOfSize:15.0f]];
    
    UILabel *moneyLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, moneySize.width, 45)];
    moneyLB.backgroundColor = [UIColor clearColor];
    moneyLB.text = string;
    moneyLB.textColor = [UIColor whiteColor];
    moneyLB.textAlignment = NSTextAlignmentLeft;
    moneyLB.font = [UIFont boldSystemFontOfSize:15.0f];
    [bottomView addSubview:moneyLB];
    
    UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(moneyLB.frame.origin.x + moneyLB.frame.size.width + 2, 0, WidthRate(300), 50)];
    moneyLabel.backgroundColor = [UIColor clearColor];
    //moneyLabel.text = @"1000.00";
    moneyLabel.textColor = [UIColor whiteColor];
    moneyLabel.textAlignment = NSTextAlignmentLeft;
    moneyLabel.font = [UIFont boldSystemFontOfSize:24.0f];
    self.moneyLabel = moneyLabel;
    [bottomView addSubview:moneyLabel];
    
    
    UIButton *paymentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    paymentBtn.frame = CGRectMake(WidthRate(500), 0, bottomView.frame.size.width - WidthRate(500), bottomView.frame.size.height);
    paymentBtn.backgroundColor = RGBA(202, 34, 50, 1);
    [paymentBtn setTitle:@"确认支付" forState:UIControlStateNormal];
    paymentBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    [paymentBtn addTarget:self action:@selector(paymentButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:paymentBtn];
}

#pragma mark - 支付方式选择
- (void)paymentClick:(UIButton *)button
{
    // 支付宝
    if (button.tag == 1111) {
        
        _selectBtnType = 0;
        self.carInfoView.hidden = YES;
        [self setButtonSelected:button];
    }
    
    // 微信支付
    if (button.tag == 2222) {
        
        if ([WXApi isWXAppInstalled]) {
            
            _selectBtnType = 1;
            [self setButtonSelected:button];
            self.carInfoView.hidden = YES;
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            app.isWXPay = YES;
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"您还未安装微信，请先安装微信" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    if (button.tag == 3333)
    {
        _selectBtnType = 2;
        self.carInfoView.hidden = NO;
        [self setButtonSelected:button];
    }
}

#pragma mark -设置按钮选中
- (void)setButtonSelected:(UIButton *)button
{
    self.selectButton.selected = NO;  // 将原本选择的按钮的状态设置为普通
    self.selectButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.selectButton = button;       // 改变当前支付方式的按钮
    self.selectButton.selected = YES; // 将当前的支付方式按钮重新设置为被选择
    self.selectButton.layer.borderColor = HDVGRed.CGColor;
}

#pragma mark - 确认支付按钮响应事件
- (void)paymentButtonClick:(id)sender
{
    // 支付宝
    if ([PMUserInfos shareUserInfo].PM_SID.length != 0 && _selectBtnType == 0) {
        
        NSString *orderCode = [self.userDataDict objectNullForKey:@"orderCode"];
        NSString *orderType = [self.userDataDict objectNullForKey:@"orderType"];
        
        NSDictionary *param = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID,@"orderCode":orderCode,@"orderType":orderType};
        
        [[PMNetworking defaultNetworking] alipay:param target:self Index:1];
    }
    
    // 微信支付
    if ([PMUserInfos shareUserInfo].PM_SID.length != 0 && _selectBtnType == 1) {
        
        NSString *orderCode = [self.userDataDict objectNullForKey:@"orderCode"];
        NSString *orderType = [self.userDataDict objectNullForKey:@"orderType"];
        
        NSDictionary *param = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID,@"orderCode":orderCode,@"orderType":orderType};
        
        [[PMNetworking defaultNetworking] request:PMRequestStateGetWXPay WithParameters:param callBackBlock:^(NSDictionary *dic) {
            
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [app weixinPayWithRequsetData:dic];
            
        } showIndicator:YES];
    }
    
    if ([PMUserInfos shareUserInfo].PM_SID.length != 0 && _selectBtnType == 2)
    {
        if (self.carNumField.text.length == 0 )
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入会员卡卡号"];
        }
        else if (self.carPassField.text.length == 0)
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入会员卡密码"];
        }
        else
        {
            NSDictionary *param = @{PMSID,@"cardNum":self.carNumField.text,@"cardPwd":self.carPassField.text,@"payType":@"hyk"};
            [[PMNetworking defaultNetworking] request:PMRequestStateDoShopByCard WithParameters:param callBackBlock:^(NSDictionary *dic) {
                if (intSuccess)
                {
                    UIAlertView *tipAlert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"开店成功，请移步到我的代言中对您的店铺进行设置吧" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                    [tipAlert show];
                }
                else
                {
                    NSString *failureMessage = [dic objectNullForKey:@"message"];
                    UIAlertView *tipAlert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:failureMessage delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [tipAlert show];
                    
                }
            } showIndicator:YES];
        }
    }
}

- (void)backToRootClick:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)successPay
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)failPay:(int)errorCode
{
    
}

- (void)keyBoardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    
    CGFloat y = keyboardRect.origin.y;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    NSArray *subviews = [self.view subviews];
    for (UIView *sub in subviews) {
        
        if ([sub isEqual:[self.view viewWithTag:920109]])
        {
            continue;
        }
        
        CGFloat maxY = CGRectGetMaxY(sub.frame);
        if (maxY > y - 2) {
            sub.center = CGPointMake(CGRectGetWidth(self.view.frame)/2.0, sub.center.y - maxY + y - 2);
            }
    }
    [UIView commitAnimations];
}

- (void)keyBoardWillDismiss:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    NSArray *subviews = [self.view subviews];
    for (UIView *sub in subviews) {
        
        if ([sub isEqual:[self.view viewWithTag:920109]])
        {
            continue;
        }
    
        
        if (sub.center.y < CGRectGetHeight(self.view.frame)/2.0) {
            sub.center = CGPointMake(CGRectGetWidth(self.view.frame)/2.0, (HEIGHT - 64 - 50) / 2);
        }
    }
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.carNumField resignFirstResponder];
    [self.carPassField resignFirstResponder];
    
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"1");
    CGFloat fromBottomHeight = HEIGHT - 64 - 50 - textField.superview.frame.origin.y;
    self.fromHeight = fromBottomHeight;
    NSLog(@"%@",@(HEIGHT));
    NSLog(@"%@",@(textField.superview.frame.origin.y));
    
    CGFloat sHeight = 0;
    
    if (fromBottomHeight < 216)
    {
        if (fromBottomHeight < 216 / 2)
        {
            sHeight = 216 - fromBottomHeight;
        }
        else
        {
            sHeight = fromBottomHeight;
        }
    }
    else
    {
        sHeight = 0;
    }
   
    
    for (UIView *subView in self.costScrollView.subviews)
    {
        subView.frame = CGRectMake(subView.frame.origin.x, subView.frame.origin.y - sHeight, subView.frame.size.width, subView.frame.size.height);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    NSLog(@"2");
    CGFloat sHeight = 0;
    
    if (self.fromHeight < 216)
    {
        if (self.fromHeight < 216 / 2)
        {
            sHeight = 216 - self.fromHeight;
        }
        else
        {
            sHeight = self.fromHeight;
        }
    }
    else
    {
        sHeight = 0;
    }
    
    for (UIView *subView in self.costScrollView.subviews)
    {
        subView.frame = CGRectMake(subView.frame.origin.x, subView.frame.origin.y + sHeight, subView.frame.size.width, subView.frame.size.height);
    }
}

- (void)letKeyBoardRF:(UITapGestureRecognizer *)tap
{
    [self.carNumField resignFirstResponder];
    [self.carPassField resignFirstResponder];
}

@end
