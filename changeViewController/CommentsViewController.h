//
//  CommentsViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/27.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMUserInfos.h"
#import "PMNetworking.h"
#import "PMResizeImage.h"
#import "MyTradeTableViewCell.h"

@interface CommentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIAlertViewDelegate>
@property(nonatomic, strong) NSMutableArray *productsArray;

@property (strong, nonatomic) UITableView *commentsTableView;
@property (strong, nonatomic) UITextView *commentsTextView;
@property (strong, nonatomic) UILabel *tipLabel;
@property (strong, nonatomic) NSString *levelsStr;
@property (strong, nonatomic) NSMutableDictionary *evaluationDict;
@property (strong, nonatomic) NSMutableArray *buttonArray;

@property (strong, nonatomic) PMNetworking *networking;

@end
