//
//  YBSureOrderViewController.h
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "PMShoppingCarViewController.h"


@interface YBSureOrderViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)NSArray *productsArray;
@property(nonatomic,copy)NSString *totalPrice;
@property (nonatomic,copy) NSString *redPackageTotalPrice;

@property(nonatomic,strong)NSDictionary *addressDic;
@property(nonatomic,strong)NSDictionary *productDic;
@property(nonatomic,assign)NSInteger totalProductNum;

@property(nonatomic,strong)UILabel *invoiceLab;

@property(nonatomic,strong)NSDictionary *billDic;
@property (assign,nonatomic) BOOL isWX;//是否是微信支付
@property (assign,nonatomic) BOOL isPrePay; //是否是预购
@property (strong,nonatomic) UILabel *redLabel;

@property (strong,nonatomic) UITableView *tableView;
@property (assign,nonatomic) BOOL isTradeBack;
@property (assign,nonatomic) BOOL isWaitBack; // 从我的交易进入
@property (copy,nonatomic) NSString *totalNum;
@property (copy,nonatomic) NSString *prePayTime;
@property (copy,nonatomic) NSString *actuallyPay;
@property (assign,nonatomic) double carriagePrice;
@property (copy,nonatomic) NSString *orderCode;
@property (assign,nonatomic) BOOL isHasActivityProduct;
@property (assign,nonatomic) double orderRedLimit;
@property (strong,nonatomic) PMShoppingCarViewController *carVC;


- (void)refreshData:(NSUInteger)index;

@end
