//
//  BindingMobileViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "BindingMobileViewController.h"
#import "PMToastHint.h"
#import "PMPublicClass.h"

@interface BindingMobileViewController ()
{
    NSInteger timerCount;
}

@property (strong, nonatomic) NSTimer *timer;

@end

@implementation BindingMobileViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"绑定新手机号";
        
        self.view.backgroundColor = HDVGPageBGGray;  // 背景色
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createBindingMobileViewUI];
    
    [self createTipUserUseMobileAndFinishButtonUI];
}

// 创建用户绑定手机号 view
- (void)createBindingMobileViewUI
{
    // 提示用户输入用户手机号码
    self.bindingMobileView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 88)];
    self.bindingMobileView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.bindingMobileView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.bindingMobileView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 43, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.bindingMobileView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(0, 88 - 0.5, WIDTH, 0.5);
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.bindingMobileView.layer addSublayer:layer3];
    
    // 手机号码
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(220), 30)];
    mobileLabel.backgroundColor = [UIColor clearColor];
    mobileLabel.text = @"新 手 机 号：";
    mobileLabel.textColor = HDVGFontColor;
    mobileLabel.textAlignment = NSTextAlignmentLeft;
    mobileLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.bindingMobileView addSubview:mobileLabel];
    
    self.mobileTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(270), 7, WidthRate(430), 30)];
    self.mobileTextField.borderStyle = UITextBorderStyleNone;
    self.mobileTextField.delegate = self;
    self.mobileTextField.tag = 1;
    self.mobileTextField.textAlignment = NSTextAlignmentLeft;
    self.mobileTextField.font = [UIFont systemFontOfSize:15.0f];
    self.mobileTextField.placeholder = @"请输入手机号码";
    self.mobileTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.mobileTextField.returnKeyType = UIReturnKeyDone;
    self.mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self.bindingMobileView addSubview:self.mobileTextField];
    
    // 获取验证码按钮
    CGFloat X = WIDTH - (WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30));
    CGFloat widtd = WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30) - WidthRate(46);
    UIButton *authcodeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    authcodeButton.frame = CGRectMake(X, 51, widtd, 30);
    [authcodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [authcodeButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [authcodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [authcodeButton addTarget:self action:@selector(authcodeForButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [authcodeButton setBackgroundColor:ButtonBgColor];
    [authcodeButton.layer setCornerRadius:4.0];
    [self.bindingMobileView addSubview:authcodeButton];
    
    
    // 短信验证码
    UILabel *authcodeLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 51, WidthRate(220), 30)];
    authcodeLab.backgroundColor = [UIColor clearColor];
    authcodeLab.text = @"短信验证码：";
    authcodeLab.textColor = HDVGFontColor;
    authcodeLab.textAlignment = NSTextAlignmentLeft;
    authcodeLab.font = [UIFont systemFontOfSize:15.0f];
    [self.bindingMobileView addSubview:authcodeLab];
    
    self.authcodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(270), 51, WidthRate(220), 30)];
    self.authcodeTextField.borderStyle = UITextBorderStyleNone;
    self.authcodeTextField.delegate = self;
    self.authcodeTextField.tag = 2;
    self.authcodeTextField.textAlignment = NSTextAlignmentLeft;
    self.authcodeTextField.font = [UIFont systemFontOfSize:15.0f];
    self.authcodeTextField.placeholder = @"请输入验证码";
    self.authcodeTextField.returnKeyType = UIReturnKeyDone;
    self.authcodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self.bindingMobileView addSubview:self.authcodeTextField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.mobileTextField resignFirstResponder];
    [self.authcodeTextField resignFirstResponder];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // 设置键盘最大输入为11位数字
    if (self.mobileTextField.tag == 1 && textField == self.mobileTextField && textField.text.length - range.length + string.length > 11) {
        return NO;
    }
    
    if (self.authcodeTextField.tag == 2 && textField == self.authcodeTextField && textField.text.length - range.length + string.length > 6) {
        return NO;
    }
    
    return YES;
}

// 创建提示用户使用手机和完成按钮 UI
- (void)createTipUserUseMobileAndFinishButtonUI
{
//    // 提示标签 暂时仅支持使用中国地区手机号码注册账户
//    UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(100), self.bindingMobileView.frame.origin.y + HeightRate(180), WidthRate(15), HeightRate(48))];
//    tipLabel.backgroundColor = [UIColor clearColor];
//    tipLabel.textAlignment = NSTextAlignmentLeft;
//    tipLabel.text = @"*";
//    tipLabel.textColor = [UIColor redColor];
//    tipLabel.font = [UIFont systemFontOfSize:15.0f];
//    [self.view addSubview:tipLabel];
//    
//    UILabel *tipLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(115), self.bindingMobileView.frame.origin.y + HeightRate(180), WIDTH, HeightRate(48))];
//    tipLabel2.backgroundColor = [UIColor clearColor];
//    tipLabel2.textAlignment = NSTextAlignmentLeft;
//    tipLabel2.text = @" 暂时仅支持使用中国地区手机号码注册账户";
//    tipLabel2.textColor = [UIColor darkGrayColor];
//    tipLabel2.font = [UIFont systemFontOfSize:12.0f];
//    [self.view addSubview:tipLabel2];
    
    // 完成设置按钮
    UIButton *finishedButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    finishedButton.frame = CGRectMake(WidthRate(46), self.bindingMobileView.frame.origin.y + 88 + HeightRate(120), WIDTH - WidthRate(46) * 2, 44);
    [finishedButton setTitle:@"确认绑定" forState:UIControlStateNormal];
    [finishedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [finishedButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [finishedButton addTarget:self action:@selector(finishedButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [finishedButton setBackgroundColor:ButtonBgColor];
    [finishedButton.layer setCornerRadius:6.0];
    [self.view addSubview:finishedButton];
}

#pragma mark 获取验证码按钮响应事件
- (void)authcodeForButtonClick:(UIButton *)sender
{
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"] || ![PMPublicClass checkMobile:self.mobileTextField.text])
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的手机号码"];
        return;
    }
    else if (self.mobileTextField.text.length == 11 && [self.mobileTextField.text hasPrefix:@"1"]) {
        
        /**
         * 用户输入手机号码获取短信验证码
         * 参数: sendSMS:(string)验证码
         */
        NSDictionary *phoneSendSMS = @{@"mobile":self.mobileTextField.text};
        self.networking = [PMNetworking defaultNetworking];
        
        
        [self.networking request:PMRequestStateSendSMS WithParameters:phoneSendSMS callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                if ([dic isKindOfClass:[NSDictionary class]])
                {
                    NSInteger messageCode = [[dic objectNullForKey:@"messageCode"] integerValue];
                    if (messageCode != 1)
                    {
                        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该号超出每天获取验证码条数"];
                    }
                    else
                    {
                        // 按钮变灰，倒数，“重发”
                        if(!self.timer.isValid)//正在倒数
                        {
                            sender.backgroundColor = [UIColor lightGrayColor];
                            sender.userInteractionEnabled = NO;
                            timerCount = 60;
                            [sender setTitle:@"重发(60)" forState:UIControlStateNormal];
                            [sender setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                            self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(resendAuthcodeClick:) userInfo:sender repeats:YES];
                        }
                    }
                }
            }
            
        }showIndicator:NO];
        
    }
}

#pragma mark  获取验证码按钮倒数
- (void)resendAuthcodeClick:(NSTimer *)timer
{
    UIButton *btn = timer.userInfo;
    
    if(timerCount == 0)
    {
        //停止运行timer
        [timer invalidate];
        //按钮恢复成红色
        [btn setBackgroundColor:ButtonBgColor];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:@"重发" forState:UIControlStateNormal];
        btn.userInteractionEnabled = YES;
    }
    else {
        timerCount --;
        [btn setTitle:[NSString stringWithFormat:@"重发(%ld)",(long)timerCount] forState:UIControlStateNormal];
    }
}


#pragma mark 确认绑定按钮响应事件
- (void)finishedButtonClick:(id)sender
{
    if ([self.authcodeTextField.text isEqual:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"验证码不能为空！" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
    }
    // 从服务器上获得的PM_SID
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    NSDictionary *dict = userInfos.userDataDic;
    
    NSString *PM_SID = [[dict objectNullForKey:@"token"] substringFromIndex:7];
    
    if (![self.authcodeTextField.text isEqual:@""]) {
        
        /**
         *  用户修改手机号码
         *  参数：updateMobile:(string)
         */
        NSDictionary *mobile = @{@"PM_SID":PM_SID, @"mobile":self.mobileTextField.text, @"validateCode":self.authcodeTextField.text};
        self.networking = [PMNetworking defaultNetworking];
        
        __block NSDictionary *callBackDict;
        
        [self.networking request:PMRequestStateSafeUpdateMobile WithParameters:mobile callBackBlock:^(NSDictionary *dict) {
           
            if ([dict isKindOfClass:[NSDictionary class]])
            {
                callBackDict = dict;
                if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"成功绑定新手机号！" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                    alertView.tag = 1;
                    [alertView show];
                }
                else {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }
        }showIndicator:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        //[self.navigationController popViewControllerAnimated:YES];
        
        // 跳转到安全管理页面
        NSArray *viewControllers = [self.navigationController viewControllers];
        UIViewController *controller = [viewControllers objectAtIndex:1];
        [self.navigationController popToViewController:controller animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
