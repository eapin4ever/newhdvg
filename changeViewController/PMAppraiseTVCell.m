//
//  PMAppraiseTVCell.m
//  changeViewController
//
//  Created by pmit on 14/12/15.
//  Copyright (c) 2014年 wallace. All rights reserved.
//
#define grayColor RGBA(123, 122, 128, 1)

#import "PMAppraiseTVCell.h"

@implementation PMAppraiseTVCell

- (void)createUI
{
    if(!self.startBtn1)
    {
//        _userLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), HeightRate(22), WidthRate(100), HeightRate(34))];
//        _userLB.font = [UIFont systemFontOfSize:HeightRate(32)];
//        _userLB.textColor = RGBA(100, 100, 100, 1);
//        [self.contentView addSubview:_userLB];
        UIView *starView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(40), HeightRate(22), WidthRate(170), HeightRate(34))];
        self.startBtn1 = [self createBtnWithSuperView:starView AndSort:0];
        [starView addSubview:self.startBtn1];
        
        self.startBtn2 = [self createBtnWithSuperView:starView AndSort:1];
        [starView addSubview:self.startBtn2];
        
        self.startBtn3 = [self createBtnWithSuperView:starView AndSort:2];
        [starView addSubview:self.startBtn3];
        
        self.startBtn4 = [self createBtnWithSuperView:starView AndSort:3];
        [starView addSubview:self.startBtn4];
        
        self.startBtn5 = [self createBtnWithSuperView:starView AndSort:4];
        [starView addSubview:self.startBtn5];
        
        [self.contentView addSubview:starView];
        
        _timeLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(286), HeightRate(22), WidthRate(250), HeightRate(34))];
        _timeLB.backgroundColor = [UIColor clearColor];
        _timeLB.font = [UIFont systemFontOfSize:HeightRate(24)];
        _timeLB.textColor = grayColor;
        _timeLB.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_timeLB];
        
        
        NSString *content = [self.remarkDict objectNullForKey:@"remark"];
        UIFont *font = [UIFont systemFontOfSize:HeightRate(28)];
        NSDictionary *sizeDic = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName, nil];
        CGSize sizeText = [content boundingRectWithSize:CGSizeMake(WIDTH - 30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:sizeDic context:nil].size;
        
        _appraiseLB = [[UILabel alloc] initWithFrame:CGRectMake(20, HeightRate(80), WIDTH - 30, HeightRate(300))];
        _appraiseLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), HeightRate(65), WIDTH - 30, sizeText.height)];
        _appraiseLB.backgroundColor = [UIColor clearColor];
        _appraiseLB.numberOfLines = 0;
        _appraiseLB.font = [UIFont systemFontOfSize:HeightRate(28)];
        _appraiseLB.textColor = grayColor;
        
        
        CALayer *bottomLine = [CALayer layer];
        bottomLine.backgroundColor = RGBA(231, 231, 231, 1).CGColor;
        bottomLine.frame = CGRectMake(0, sizeText.height + HeightRate(65) + 10, WIDTH, 0.8);
        
        [self.contentView addSubview:_appraiseLB];
      
        if (self.isBigVC)
        {
            [self.contentView.layer addSublayer:bottomLine];
        }
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

- (void)setContentUserName:(NSString *)name detail:(NSString *)detail time:(NSString *)time
{
//    _userLB.text = name;
    NSInteger level = [name integerValue];
    switch (level) {
        case 1:
            self.startBtn1.selected = YES;
            self.startBtn2.selected = NO;
            self.startBtn3.selected = NO;
            self.startBtn4.selected = NO;
            self.startBtn5.selected = NO;
            break;
        case 2:
            self.startBtn1.selected = YES;
            self.startBtn2.selected = YES;
            self.startBtn3.selected = NO;
            self.startBtn4.selected = NO;
            self.startBtn5.selected = NO;
            break;
        case 3:
            self.startBtn1.selected = YES;
            self.startBtn2.selected = YES;
            self.startBtn3.selected = YES;
            self.startBtn4.selected = NO;
            self.startBtn5.selected = NO;
            break;
        case 4:
            self.startBtn1.selected = YES;
            self.startBtn2.selected = YES;
            self.startBtn3.selected = YES;
            self.startBtn4.selected = YES;
            self.startBtn5.selected = NO;
            break;
        case 5:
            self.startBtn1.selected = YES;
            self.startBtn2.selected = YES;
            self.startBtn3.selected = YES;
            self.startBtn4.selected = YES;
            self.startBtn5.selected = YES;
            break;
            
        default:
            break;
    }
    _appraiseLB.text = detail;
    _timeLB.text = time;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIButton *)createBtnWithSuperView:(UIView *)sView AndSort:(NSInteger)i
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(sView.bounds.size.width / 5 * i, 0, sView.bounds.size.width / 5, sView.bounds.size.height);
    [btn setImage:[UIImage imageNamed:@"star@2x.png"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"star_pressed@2x.png"] forState:UIControlStateSelected];
    return btn;
}

@end
