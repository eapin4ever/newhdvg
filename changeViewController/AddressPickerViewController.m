//
//  AddressPickerViewController.m
//  changeViewController
//
//  Created by pmit on 15/9/15.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "AddressPickerViewController.h"
#import "PMMyPhoneInfo.h"
#import "CityDelegate.h"
#import "AreaDelegate.h"
#import "PMNetworking.h"

@interface AddressPickerViewController () <UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,CityDelegateDelegate,AreaDelegateDelegate>

@property (strong,nonatomic) UIScrollView *addressScrollView;
@property (strong,nonatomic) UITableView *provinceTableView;
@property (strong,nonatomic) UITableView *cityTableView;
@property (strong,nonatomic) UITableView *areaTableView;
@property (strong,nonatomic) CityDelegate *cityDelegate;
@property (strong,nonatomic) AreaDelegate *areaDelegate;
@property (strong,nonatomic) NSArray *provinceArr;
@property (strong,nonatomic) NSArray *cityArr;
@property (strong,nonatomic) NSArray *areaArr;

@end

@implementation AddressPickerViewController

static NSString *const provinceCell = @"provinceCell";
static NSString *const cityCell = @"cityCell";
static NSString *const areaCell = @"areaCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"省份选择";
    
    self.cityDelegate = [[CityDelegate alloc] init];
    self.cityDelegate.cityDelegateDelegate = self;
    
    self.areaDelegate = [[AreaDelegate alloc] init];
    self.areaDelegate.areaDDelegate = self;
    
    [self buildAddressScroll];
    [self buildThirdTableView];
    [self getProvinceDataByNet];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildAddressScroll
{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 44, 30);
    [backBtn setImage:[UIImage imageNamed:@"normal_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(pickBack) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    self.addressScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.addressScrollView.contentSize = CGSizeMake(WIDTH * 3, 0);
    self.addressScrollView.delegate = self;
    self.addressScrollView.backgroundColor = RGBA(243, 243, 243, 1);
    self.addressScrollView.showsHorizontalScrollIndicator = NO;
    self.addressScrollView.showsVerticalScrollIndicator = NO;
    self.addressScrollView.scrollEnabled = NO;
    [self.view addSubview:self.addressScrollView];
}

- (void)buildThirdTableView
{
    self.provinceTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.provinceTableView.delegate = self;
    self.provinceTableView.dataSource = self;
    [self.addressScrollView addSubview:self.provinceTableView];
    
    self.cityTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.cityTableView.delegate = self.cityDelegate;
    self.cityTableView.dataSource = self.cityDelegate;
    [self.cityTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cityCell];
    [self.addressScrollView addSubview:self.cityTableView];

    self.areaTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH * 2 , 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.areaTableView.delegate = self.areaDelegate;
    self.areaTableView.dataSource = self.areaDelegate;
    [self.areaTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:areaCell];
    [self.addressScrollView addSubview:self.areaTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.provinceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:provinceCell];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:provinceCell];
    }
    
    cell.textLabel.text = [self.provinceArr[indexPath.row] objectForKey:@"name"];
    return cell;
}

- (void)getProvinceDataByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateGetProvinceData WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.provinceArr = [dic objectNullForKey:@"data"];
            [self.provinceTableView reloadData];
        }
        else
        {
            
        }
        
    } showIndicator:NO];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *provinceId = [NSString stringWithFormat:@"%@",[self.provinceArr[indexPath.row] objectNullForKey:@"id"]];
    self.provinceName = [self.provinceArr[indexPath.row] objectNullForKey:@"name"];
    
    self.provinceId = provinceId;
    [[PMNetworking defaultNetworking] request:PMRequestStateGetCityData WithParameters:@{PMSID,@"parentId":provinceId} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSArray *cityArr = [dic objectNullForKey:@"data"];
            if (cityArr.count > 0)
            {
                self.cityArr = cityArr;
                self.cityDelegate.cityArr = self.cityArr;
                [self.cityTableView reloadData];
                
                [UIView animateWithDuration:0.3f animations:^{
                    
                    self.addressScrollView.contentOffset = CGPointMake(WIDTH, 0);
                    
                } completion:^(BOOL finished) {
                    
                    
                    
                }];
                
            }
            else
            {
                if ([self.addressDelegate respondsToSelector:@selector(finishPick:CityId:AreaId:ProvinceValue:CityName:AreaName:)])
                {
                    [self.addressDelegate finishPick:self.provinceId CityId:@"" AreaId:@"" ProvinceValue:self.provinceName CityName:@"" AreaName:@""];
                }
                
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else
        {
            
        }
    } showIndicator:NO];
}

- (void)successGetNextData:(NSArray *)areaArr CityId:(NSString *)cityId CityName:(NSString *)cityName
{
    self.areaDelegate.areaArr = areaArr;
    self.areaDelegate.cityName = cityName;
    self.areaDelegate.cityId = cityId;
    
    [self.areaTableView reloadData];
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.addressScrollView.contentOffset = CGPointMake(WIDTH * 2, 0);
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    
}

- (void)noNextData:(NSString *)cityId CityName:(NSString *)cityName
{
    self.cityId = cityId;
    if ([self.addressDelegate respondsToSelector:@selector(finishPick:CityId:AreaId:ProvinceValue:CityName:AreaName:)])
    {
        [self.addressDelegate finishPick:self.provinceId CityId:self.cityId AreaId:@"" ProvinceValue:self.provinceName CityName:cityName AreaName:@""];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger titleIndex = scrollView.contentOffset.x / WIDTH;
    switch (titleIndex) {
        case 0:
            self.title = @"省份选择";
            break;
        case 1:
            self.title = @"城区选择";
            break;
        case 2:
            self.title = @"区域选择";
            break;
        default:
            break;
    }
}

- (void)goNextData:(NSString *)areaId AreaName:(NSString *)areaName CityName:(NSString *)cityName CityId:(NSString *)cityId
{
    if ([self.addressDelegate respondsToSelector:@selector(finishPick:CityId:AreaId:ProvinceValue:CityName:AreaName:)])
    {
        [self.addressDelegate finishPick:self.provinceId CityId:cityId AreaId:areaId ProvinceValue:self.provinceName CityName:cityName AreaName:areaName];
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pickBack
{
    NSInteger titleIndex = self.addressScrollView.contentOffset.x / WIDTH;
    switch (titleIndex) {
        case 0:
            
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
        {
            
            [UIView animateWithDuration:0.3f animations:^{
                
                self.addressScrollView.contentOffset = CGPointMake(0, 0);
                
            } completion:^(BOOL finished) {
                
                
                
            }];
        }

            break;
        case 2:
        {
            [UIView animateWithDuration:0.3f animations:^{
                
                self.addressScrollView.contentOffset = CGPointMake(WIDTH, 0);
                
            } completion:^(BOOL finished) {
                
                
                
            }];
        }
            break;
        default:
            break;
    }
}

@end
