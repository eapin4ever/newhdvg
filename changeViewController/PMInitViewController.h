//
//  PMInitViewController.h
//  changeViewController
//
//  Created by P&M on 15/5/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMInitViewController : UIViewController

+ (PMInitViewController *)defaultInitVC;

@end
