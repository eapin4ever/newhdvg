//
//  YBThemeBtn.h
//  changeViewController
//
//  Created by ZhangEapin on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBThemeBtn : UIButton

@property (copy,nonatomic) NSString *englishName;
@property (strong,nonatomic) UILabel *englishLB;

@end
