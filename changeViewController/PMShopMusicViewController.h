//
//  PMShopMusicViewController.h
//  changeViewController
//
//  Created by pmit on 15/6/30.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMShopMusicViewController : UIViewController

@property (copy,nonatomic) NSString *shopId;
@property (strong,nonatomic) NSArray *musicArr;
@property (strong,nonatomic) NSString *muiscName;
@property (strong,nonatomic) NSDictionary *shopDic;

@end
