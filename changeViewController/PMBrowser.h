//
//  PMBrowser.h
//  changeViewController
//
//  Created by pmit on 14/11/8.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <KINWebBrowserViewController.h>

@interface PMBrowser : NSObject
@property(nonatomic,copy)NSString *currentURL;

+ (PMBrowser *)defaultWebBrowserWithController:(id<KINWebBrowserDelegate>)viewController;

- (void)getInShop:(NSString *)address ByPushController:(UIViewController *)viewController;

@end
