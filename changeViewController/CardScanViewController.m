//
//  CardScanViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/16.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "CardScanViewController.h"
#import <CardIO/CardIO.h>
#import <CardIOPaymentViewControllerDelegate.h>
#import "PMMyPhoneInfo.h"

@interface CardScanViewController () <CardIOPaymentViewControllerDelegate>

@end

@implementation CardScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"银行卡扫描";
    self.view.backgroundColor = [UIColor whiteColor];
    [self createUI];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [CardIOUtilities preload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createUI
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.center = CGPointMake(WIDTH / 2, (HEIGHT - 64) / 2);
    button.bounds = (CGRect){CGPointZero,{200,100}};
    [button setTitle:@"点我吧" forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor redColor]];
    [button addTarget:self action:@selector(startScan:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (void)startScan:(UIButton *)sender
{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.collectExpiry = NO;
    scanViewController.collectCVV = NO;
    scanViewController.useCardIOLogo = YES;
    [self presentViewController:scanViewController animated:YES completion:nil];
}

//取消扫描

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController

{
    
    NSLog(@"User canceled payment info");
    
    // Handle user cancellation here...
    
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
    
}

//扫描完成

-(void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController

{
    //扫描结果
    
        NSString *strTem = [info.cardNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *strTem2 = @"";
        if (strTem.length % 4 == 0)
        {
            NSInteger count = strTem.length / 4;
            for (int i = 0; i < count; i++)
            {
                NSString *str = [strTem substringWithRange:NSMakeRange(i * 4, 4)];
                strTem2 = [strTem2 stringByAppendingString:[NSString stringWithFormat:@"%@ ", str]];
            }
        }
        else
        {
            NSInteger count = strTem.length / 4;
            for (int j = 0; j <= count; j++)
            {
                if (j == count)
                {
                    NSString *str = [strTem substringWithRange:NSMakeRange(j * 4, strTem.length % 4)];
                    strTem2 = [strTem2 stringByAppendingString:[NSString stringWithFormat:@"%@ ", str]];
                }
                else
                {
                    NSString *str = [strTem substringWithRange:NSMakeRange(j * 4, 4)];
                    strTem2 = [strTem2 stringByAppendingString:[NSString stringWithFormat:@"%@ ", str]];
                }
            }
        }
    
    NSLog(@"cardNo --> %@",strTem2);
    
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
    
}


@end
