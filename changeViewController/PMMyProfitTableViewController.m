//
//  PMMyProfitTableViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMMyProfitTableViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMDistrubutionTableViewCell.h"
#import "PMMyClientsTableViewCell.h"

@interface PMMyProfitTableViewController ()

@end

@implementation PMMyProfitTableViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        //self.title = @"代言获利详情";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initDistributionProfitWeekData];
    
    // 没有代言获利数据
    [self createNoDataView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.showArray = [NSMutableArray array];
    
    [self createTitleViewUI];
    [self createSegmentControlUI];
    
    // 设置获利详情 cell 传数据
    [self.tableView registerClass:[PMDistrubutionTableViewCell class] forCellReuseIdentifier:@"profitDetailTableViewCell"];
    // 注册我的客户 Cell
    [self.tableView registerClass:[PMMyClientsTableViewCell class] forCellReuseIdentifier:@"myClientsTableViewCell"];
}

- (void)createTitleViewUI
{
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WidthRate(280), HeightRate(60))];
    titleView.backgroundColor = RGBA(213, 125, 133, 1);
    titleView.layer.cornerRadius = 5.0f;
    titleView.layer.masksToBounds = YES;
    
    // 创建时间和客户按钮
    NSArray *array = [NSArray arrayWithObjects:@"时间",@"客户", nil];
    self.buttonArray = [NSMutableArray array];
    for(int i = 0; i < array.count; i++) {
        
        self.timeAndClientBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.timeAndClientBtn.frame = CGRectMake(WidthRate(3) + WidthRate(137) * (i%3), HeightRate(3), WidthRate(137), HeightRate(54));
        // 设置tag，主要是为了添加点击事件时可以正确的知道是那个按钮触发的
        self.timeAndClientBtn.tag = i;
        if (i == 0) {
            self.timeAndClientBtn.selected = YES;
        }
        [self.timeAndClientBtn setBackgroundImage:[UIImage imageNamed:@"timeAndClient_bg.png"] forState:UIControlStateSelected];
        [self.timeAndClientBtn setTitle:array[i] forState:UIControlStateNormal];
        [self.timeAndClientBtn setTitleColor:RGBA(194, 48, 61, 1) forState:UIControlStateNormal];
        [self.timeAndClientBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.timeAndClientBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
        [self.timeAndClientBtn addTarget:self action:@selector(timeAndClientButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:self.timeAndClientBtn];
        
        [self.buttonArray addObject:self.timeAndClientBtn]; // 将按钮添加到数组里
    }
    self.navigationItem.titleView = titleView;
}

- (void)createSegmentControlUI
{
    // 创建表格 headerView
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(88))];
    headerView.backgroundColor = RGBA(245, 245, 245, 1);
    
    /**
     *  创建查看获利详情日期按钮
     */
    // 初始化分段控件，并申请空间
    self.segment = [[UISegmentedControl alloc] initWithItems:@[@"最近一周",@"最近一个月",@"全部"]];
    self.segment.frame = CGRectMake(0, 0, WIDTH, HeightRate(88));
    self.segment.selectedSegmentIndex = 0;
    
    // 选中按钮时标题颜色
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HDVGRed, NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    // 底部红色指示条
    self.tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/5/self.segment.numberOfSegments, HeightRate(80), WIDTH/5*3/self.segment.numberOfSegments, HeightRate(7))];
    self.tipLabel.backgroundColor = HDVGRed;
    [self.segment addSubview:self.tipLabel];
    
    // 调用segmentedClick的方法,以及控制事件类型
    [self.segment addTarget:self action:@selector(segmentedClick:) forControlEvents:UIControlEventValueChanged];
    // 设置控件填充颜色
    [self.segment setTintColor:[UIColor clearColor]];
    // 添加分段控件到当前界面显示
    [headerView addSubview:self.segment];
    
    self.tableView.tableHeaderView = headerView;
}

#pragma mark 时间和客户按钮响应事件
- (void)timeAndClientButtonClick:(UIButton *)button
{
    for (UIButton *btn in self.buttonArray) {
        btn.selected = NO;
    }
    
    // 改变按钮的选中状态
    button.selected = YES;
    if (button.tag == 0) { // 时间数据
        
        self.timeLabel.text = @"时间";
        self.orderLabel.text = @"订单金额（元）";
        self.brokerageLabel.text = @"佣金金额（元）";
        
        NSInteger selectedIndex = [self.segment selectedSegmentIndex];
        
        if (selectedIndex == 0) {
            // 最近一周
            [self initDistributionProfitWeekData];
        }
        else if (selectedIndex == 1) {
            // 最近一个月
            [self initDistributionProfitMonthData];
        }
        else if (selectedIndex == 2) {
            // 全部
            [self initDistributionProfitAllData];
        }
    }
    else if (button.tag == 1) { // 我的客户
        
        self.timeLabel.text = @"客户";
        self.orderLabel.text = @"付款笔数";
        self.brokerageLabel.text = @"交易额";
        
        NSInteger selectedIndex = [self.segment selectedSegmentIndex];
        if (selectedIndex == 0) {
            // 最近一周
            [self initMyClientsWeekData];
        }
        else if (selectedIndex == 1) {
            // 最近一个月
            [self initMyClientsMonthData];
        }
        else if (selectedIndex == 2) {
            // 全部
            [self initMyClientsAllData];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.showArray count];
}

#pragma mark - section For Header
// 设置 section 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(88);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 创建表格 headerView
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(88))];
    headerView.backgroundColor = RGBA(245, 245, 245, 1);
    
    UILabel *lineImage = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(1))];
    lineImage.backgroundColor = RGBA(128, 128, 128, 1);
    [headerView addSubview:lineImage];
    
    UILabel *lineImage2 = [[UILabel alloc] initWithFrame:CGRectMake(0, HeightRate(87.5), WIDTH, HeightRate(0.5))];
    lineImage2.backgroundColor = RGBA(128, 128, 128, 1);
    [headerView addSubview:lineImage2];
    
    // 时间
    UILabel *timeLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(20), WidthRate(230), HeightRate(48))];
    timeLB.backgroundColor = [UIColor clearColor];
    timeLB.textColor = RGBA(97, 97, 97, 1);
    timeLB.textAlignment = NSTextAlignmentCenter;
    timeLB.font = [UIFont systemFontOfSize:13.0f];
    self.timeLabel = timeLB;
    [headerView addSubview:timeLB];
    
    // 订单金额
    UILabel *orderLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(260), HeightRate(20), WidthRate(230), HeightRate(48))];
    orderLB.backgroundColor = [UIColor clearColor];
    orderLB.textColor = RGBA(97, 97, 97, 1);
    orderLB.textAlignment = NSTextAlignmentCenter;
    orderLB.font = [UIFont systemFontOfSize:13.0f];
    self.orderLabel = orderLB;
    [headerView addSubview:orderLB];
    
    // 佣金金额
    UILabel *brokerageLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(490), HeightRate(20), WidthRate(230), HeightRate(48))];
    brokerageLB.backgroundColor = [UIColor clearColor];
    brokerageLB.textColor = RGBA(97, 97, 97, 1);
    brokerageLB.textAlignment = NSTextAlignmentCenter;
    brokerageLB.font = [UIFont systemFontOfSize:13.0f];
    self.brokerageLabel = brokerageLB;
    [headerView addSubview:brokerageLB];
    
    UIButton *btn = self.buttonArray[0];
    if (btn.selected == YES)//如果第一个按钮是选中的话，就是显示获利详情
    {
        self.timeLabel.text = @"时间";
        self.orderLabel.text = @"订单金额（元）";
        self.brokerageLabel.text = @"佣金金额（元）";
    }
    else//否则的话显示客户数据
    {
        self.timeLabel.text = @"客户";
        self.orderLabel.text = @"付款笔数";
        self.brokerageLabel.text = @"交易额";
    }
    
    return headerView;
}

// 设置 footer 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return HeightRate(1);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(1))];
    footerView.backgroundColor = RGBA(200, 200, 200, 1);
    
    return footerView;
}

#pragma mark - 查看获利详情日期按钮事件
- (void)segmentedClick:(UISegmentedControl *)sender
{
    [self.showArray removeAllObjects];
    
    // 底部红色指示条滑动的动画
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.tipLabel.frame;
        rect.origin.x = WIDTH/5/sender.numberOfSegments + WIDTH/sender.numberOfSegments*sender.selectedSegmentIndex;
        self.tipLabel.frame = rect;
    }];
    
    
    NSInteger selectedIndex = [self.segment selectedSegmentIndex];
    
    UIButton *btn = self.buttonArray[0];
    if (btn.selected == YES) {
        if (selectedIndex == 0) {
            // 最近一周
            [self initDistributionProfitWeekData];
        }
        else if (selectedIndex == 1) {
            // 最近一个月
            [self initDistributionProfitMonthData];
        }
        else if (selectedIndex == 2) {
            // 全部
            [self initDistributionProfitAllData];
        }
    }
    else {
        // 客户数据
        if (selectedIndex == 0) {
            // 最近一周
            [self initMyClientsWeekData];
        }
        else if (selectedIndex == 1) {
            // 最近一个月
            [self initMyClientsMonthData];
        }
        else if (selectedIndex == 2) {
            // 全部
            [self initMyClientsAllData];
        }
    }
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIButton *btn = self.buttonArray[0];
    
    if (btn.selected == YES)//显示获利的数据
    {
        PMDistrubutionTableViewCell *profitCell = [tableView dequeueReusableCellWithIdentifier:@"profitDetailTableViewCell"];
        
        [profitCell setDistributionProfitDetailDataUI];
        
        NSDictionary *profitDict = self.showArray[indexPath.row];
        
        // 获取下来的数据
        [profitCell setTime:[profitDict objectNullForKey:@"time"] setOrderMoney:[[profitDict objectNullForKey:@"totalMoney"] doubleValue] setMoney:[[profitDict objectNullForKey:@"myInput"] doubleValue]];
        
        return profitCell;
    }
    else//显示客户的数据
    {
        PMMyClientsTableViewCell *clientsCell = [tableView dequeueReusableCellWithIdentifier:@"myClientsTableViewCell"];
        
        [clientsCell setMyClientsDataUI];
        
        NSDictionary *clientsDict = self.showArray[indexPath.row];
        
        // 获取下来的数据
        [clientsCell setNickname:[clientsDict objectNullForKey:@"userName"] setTradeNum:[[clientsDict objectNullForKey:@"tradeNum"] integerValue] setBrokerage:[[clientsDict objectNullForKey:@"tradeMoney"] doubleValue]];
        
        return clientsCell;
    }
    
    // 因为在上面的if else 方法中已经返回return cell了 在这里可以return nil
    return nil;
}

#pragma mark - 获取代言获利详情数据和我的客户数据
#pragma mark 
- (void)getProfitDetailData:(NSInteger)requestUrl withParam:(NSDictionary *)param
{
    self.networking = [PMNetworking defaultNetworking];
    
    [self.networking request:requestUrl WithParameters:param callBackBlock:^(NSDictionary *dict) {
        
        if ([[dict objectNullForKey:@"success"] integerValue] == 1)
        {
            self.noView.hidden = YES;
            
            UIButton *btn = self.buttonArray[0];
            
            if (btn.selected == YES)
            {
                // 我的客户数据
                self.myClientsArray = [[[dict objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                
                [self.showArray removeAllObjects]; // 去除重复数据
                
                for (NSDictionary *dict in self.myClientsArray)
                {
                    [self.showArray addObject:dict];
                }
            }
            // 代言获利数据
            else
            {
                self.profitDetailArray = [[[dict objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                
                [self.showArray removeAllObjects]; // 去除重复数据
                
                for (NSDictionary *dict in self.profitDetailArray)
                {
                    [self.showArray addObject:dict];
                }
            }
            [self.tableView reloadData];
        }
        else {
            // 没有代言获利数据
            self.noView.hidden = NO;
        }
        
    }showIndicator:NO];
}

#pragma mark 获取代言获利详情最近一周数据
- (void)initDistributionProfitWeekData
{
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    //获利详情
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     createDt      N    date        开始时间
     createDt2     N    date        结束时间
     flag	       N	String		标志  传”mobile”过来(根据时间查的时候)
     days	       N	Integer		天数，如前七天，则传-7
     */
    
    int currentPage = 1;
    
    NSString *mobile = @"mobile";
    NSInteger days = -7;
    
//    // 获得日期
//    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
//    NSString* date = [formatter stringFromDate:[NSDate date]];
//    
//    NSDate *createDt2 = [formatter dateFromString:date];
//    // 实现查找最近一周的时间，可以用这个方法
//    NSDate *createDt = [[NSDate alloc] init];
//    createDt = [createDt dateByAddingTimeInterval:-7*3600*24];
    
    NSDictionary *profitDetail = @{@"PM_SID":userInfots.PM_SID, @"currentPage":@(currentPage), @"flag":mobile, @"days":@(days)};
    
    [self getProfitDetailData:PMRequestStateShareShowProfit withParam:profitDetail];
}

#pragma mark 获取代言获利详情最近一个月数据
- (void)initDistributionProfitMonthData
{
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    //获利详情
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     createDt      N    date        开始时间
     createDt2     N    date        结束时间
     flag	       N	String		标志  传”mobile”过来(根据时间查的时候)
     days	       N	Integer		天数，如前七天，则传-30
     */
    
    int currentPage = 1;
    
    NSString *mobile = @"mobile";
    NSInteger days = -30;
    
    NSDictionary *profitDetail = @{@"PM_SID":userInfots.PM_SID, @"currentPage":@(currentPage), @"flag":mobile, @"days":@(days)};
    
    [self getProfitDetailData:PMRequestStateShareShowProfit withParam:profitDetail];
}

#pragma mark 获取代言获利详情全部数据
- (void)initDistributionProfitAllData
{
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    //获利详情
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     createDt      N    date        开始时间
     createDt2     N    date        结束时间
     flag	       N	String		标志  传”mobile”过来(根据时间查的时候)
     days	       N	Integer		天数为空
     */
    
    int currentPage = 1;
    
    NSString *mobile = @"";
    NSInteger days = 0;
    
    NSDictionary *profitDetail = @{@"PM_SID":userInfots.PM_SID, @"currentPage":@(currentPage), @"flag":mobile, @"days":@(days)};
    
    [self getProfitDetailData:PMRequestStateShareShowProfit withParam:profitDetail];
}


#pragma mark - 获取我的客户最近一周数据
#pragma mark
- (void)initMyClientsWeekData
{
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    //我的客户
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     createDt      N    date        开始时间
     createDt2     N    date        结束时间
     flag	       N	String		标志  传”mobile”过来(根据时间查的时候)
     days	       N	Integer		天数，如前七天，则传-7
     */
    
    int currentPage = 1;
    
    NSString *mobile = @"mobile";
    NSInteger days = -7;
    
    NSDictionary *myClients = @{@"PM_SID":userInfots.PM_SID, @"currentPage":@(currentPage), @"flag":mobile, @"days":@(days)};
    
    [self getProfitDetailData:PMRequestStateShareShowMyClient withParam:myClients];
}

#pragma mark 获取我的客户最近一个月数据
- (void)initMyClientsMonthData
{
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    //提现明细
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     createDt      N    date        开始时间
     createDt2     N    date        结束时间
     flag	       N	String		标志  传”mobile”过来(根据时间查的时候)
     days	       N	Integer		天数，如前七天，则传-30
     */
    
    int currentPage = 1;
    
    NSString *mobile = @"mobile";
    NSInteger days = -30;
    
    NSDictionary *myClients = @{@"PM_SID":userInfots.PM_SID, @"currentPage":@(currentPage), @"flag":mobile, @"days":@(days)};
    
    [self getProfitDetailData:PMRequestStateShareShowMyClient withParam:myClients];
}

#pragma mark 获取我的客户全部数据
- (void)initMyClientsAllData
{
    // 获取用户token
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    
    //我的客户
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     flag	       N	String		标志  传”mobile”过来(根据时间查的时候)
     days	       N	Integer		天数为空
     */
    
    int currentPage = 1;

    NSString *mobile = @"";
    NSInteger days = 0;
    
    NSDictionary *myClients = @{@"PM_SID":userInfos.PM_SID,@"currentPage":@(currentPage), @"flag":mobile, @"days":@(days)};
    
    [self getProfitDetailData:PMRequestStateShareShowMyClient withParam:myClients];
}



#pragma mark - 获取我的客户数据
- (void)initMyClientsDataList
{
    // 获取用户token
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    
    //我的客户
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     */
    int currentPage = 1;
    NSDictionary *myClients = @{@"PM_SID":userInfos.PM_SID,@"currentPage":@(currentPage)};
    self.networking = [PMNetworking defaultNetworking];
    
    [self.networking request:PMRequestStateShareShowMyClient WithParameters:myClients callBackBlock:^(NSDictionary *dict) {
        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            if ([[dict objectNullForKey:@"success"] integerValue] == 0) {
                
                // 还没有客户数据
                UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), HeightRate(400), WIDTH - WidthRate(40), HeightRate(60))];
                tipLabel.backgroundColor = [UIColor clearColor];
                tipLabel.text = @"亲，你暂时还没有客户数据";
                tipLabel.textColor = RGBA(65, 65, 65, 1);
                tipLabel.textAlignment = NSTextAlignmentCenter;
                tipLabel.font = [UIFont systemFontOfSize:20.0f];
                [self.view addSubview:tipLabel];
            }
            else {
                [self.showArray removeAllObjects];   // 去除重复数据
                
                self.myClientsArray = [[[dict objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                
                //默认显示最近一周的数据
                for (NSDictionary *dict in self.myClientsArray)
                {
                    [self.showArray addObject:dict];
                }
            }
            [self.tableView reloadData];
        }];
    }showIndicator:NO];
}

#pragma mark - 创建没有数据View
- (void)createNoDataView
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(88), WIDTH, HEIGHT - 64 - HeightRate(88))];
    noDataView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *noIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noDataView.frame.size.height)];
    noIV.image = [UIImage imageNamed:@"profit_null"];
    noIV.contentMode = UIViewContentModeScaleAspectFit;
    [noDataView addSubview:noIV];
    
    [self.view insertSubview:noDataView atIndex:1000];
    self.noView = noDataView;
    self.noView.hidden = YES;
}


#pragma mark - 设置代言获利详情假数据
- (void)initDistributionProfitData
{
    NSArray *timeArr = [NSArray arrayWithObjects:@"2014-12-28",@"2014-12-24",@"2014-11-30",@"2014-11-20",@"2014-10-12", nil];
    NSArray *moneyArr = [NSArray arrayWithObjects:@"¥1155.00",@"¥1265.00",@"¥155.00",@"¥1895.00",@"¥15.00", nil];
    NSArray *stateArr = [NSArray arrayWithObjects:@"¥155.00",@"¥158.00",@"¥15.00",@"¥195.00",@"¥1.00", nil];
    
    if (!self.profitDetailArray) {
        self.profitDetailArray = [NSMutableArray array];
        
        for (int i = 0; i < 5; i++) {
            NSMutableDictionary *dict = [@{@"time":timeArr[i], @"totalMoney":moneyArr[i], @"money":stateArr[i]} mutableCopy];
            
            [self.profitDetailArray addObject:dict];
        }
    }
    
    //默认显示最近一周的数据
    for (NSDictionary *dict in self.profitDetailArray)
    {
        [self.showArray addObject:dict];
    }
}

#pragma mark - 设置我的客户假数据
- (void)initMyClientsData
{
    NSArray *timeArr = [NSArray arrayWithObjects:@"2014-12-28",@"2014-12-20",@"2014-12-13",@"2014-11-12",@"2014-10-12", nil];
    NSArray *nameArr = [NSArray arrayWithObjects:@"芒果不吃芒果",@"雪梨不吃雪梨",@"苹果不吃苹果",@"西瓜不吃西瓜",@"柚子不吃柚子", nil];
    NSArray *moneyArr = [NSArray arrayWithObjects:@"¥6655.00",@"¥8265.00",@"¥755.00",@"¥6595.00",@"¥158.00", nil];
    NSArray *stateArr = [NSArray arrayWithObjects:@"¥2155.00",@"¥2358.00",@"¥95.00",@"¥2295.00",@"¥12.00", nil];
    if (!self.myClientsArray) {
        self.myClientsArray = [NSMutableArray array];
        
        for (int i = 0; i < 5; i++) {
            NSMutableDictionary *dict = [@{@"time":timeArr[i], @"nickName":nameArr[i], @"tradeMoney":moneyArr[i], @"brokerage":stateArr[i]} mutableCopy];
            
            [self.myClientsArray addObject:dict];
        }
    }
    
    //默认显示已最近一周的订单
    for (NSDictionary *dict in self.myClientsArray)
    {
        [self.showArray addObject:dict];
    }
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
