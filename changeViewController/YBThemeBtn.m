//
//  YBThemeBtn.m
//  changeViewController
//
//  Created by ZhangEapin on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBThemeBtn.h"
#import "PMMyPhoneInfo.h"

@implementation YBThemeBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        
        UILabel *englishLB = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height * 0.7, frame.size.width, frame.size.height * 0.2)];
        englishLB.font = [UIFont systemFontOfSize:9.0f];
        englishLB.textAlignment = NSTextAlignmentCenter;
        if (self.isHighlighted)
        {
            englishLB.textColor = [UIColor whiteColor];
        }
        else
        {
            englishLB.textColor = [UIColor lightGrayColor];
        }
        self.englishLB = englishLB;
        [self addSubview:englishLB];
    }
    
    return self;
}


- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 5, contentRect.size.width, contentRect.size.height * 0.4);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, contentRect.size.height * 0.5, contentRect.size.width, contentRect.size.height * 0.25);
}

@end
