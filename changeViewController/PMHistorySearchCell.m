//
//  PMHistorySearchCell.m
//  changeViewController
//
//  Created by ZhangEapin on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMHistorySearchCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMHistorySearchCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.historyLB)
    {
        self.historyLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), 0, WIDTH - WidthRate(40), 44)];
        self.historyLB.font = [UIFont systemFontOfSize:14.0f];
        self.historyLB.textColor = [UIColor darkGrayColor];
        [self.contentView addSubview:self.historyLB];
    }
}
@end
