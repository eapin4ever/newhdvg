//
//  PMReuseTableViewCell.m
//  changeViewController
//
//  Created by pmit on 14/11/22.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMReuseTableViewCell.h"

@implementation PMReuseTableViewCell

- (void)initCollectionViewWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout dataSource:(id<UICollectionViewDataSource>)dataSource delegate:(id<UICollectionViewDelegate>)delegate registerClass:(Class)registerClass reuseIdentify:(NSString *)identify
{
    self.cv = [[UICollectionView alloc]initWithFrame:frame collectionViewLayout:layout];
    [self.cv setDataSource:dataSource];
    [self.cv setDelegate:delegate];
    [self.cv registerClass:registerClass forCellWithReuseIdentifier:identify];
    [self.cv setContentInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    [self.cv setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:self.cv];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
