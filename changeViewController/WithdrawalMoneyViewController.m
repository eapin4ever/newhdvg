//
//  WithdrawalMoneyViewController.m
//  changeViewController
//
//  Created by P&M on 14/12/5.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "WithdrawalMoneyViewController.h"
#import "PMMyPhoneInfo.h"
#import "MasterViewController.h"
#import "PMNavigationController.h"
#import "BankcardViewController.h"
#import "PMGetCashTableViewController.h"
#import "PMToastHint.h"

@interface WithdrawalMoneyViewController () <BankcardViewControllerDelegate>

@end

@implementation WithdrawalMoneyViewController
@synthesize canMoneyStr;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"申请提现";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self checkBindingBank];
    
    [self createWithdrawalViewUI];
    [self createWithdrawalButton];
}

// 创建提现 view
- (void)createWithdrawalViewUI
{
    self.withdrawalView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 120)];
    self.withdrawalView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.withdrawalView];
    
    // 分隔线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    line1.backgroundColor = RGBA(208, 208, 212, 1).CGColor;
    [self.withdrawalView.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(WidthRate(26), 40, WIDTH, HeightRate(1));
    line2.backgroundColor = RGBA(208, 208, 212, 1).CGColor;
    [self.withdrawalView.layer addSublayer:line2];
    
    CALayer *line3 = [[CALayer alloc] init];
    line3.frame = CGRectMake(WidthRate(26), 80, WIDTH, HeightRate(1));
    line3.backgroundColor = RGBA(208, 208, 212, 1).CGColor;
    [self.withdrawalView.layer addSublayer:line3];
    
    CALayer *line4 = [[CALayer alloc] init];
    line4.frame = CGRectMake(0, 120 - 0.5, WIDTH, 0.5);
    line4.backgroundColor = RGBA(208, 208, 212, 1).CGColor;
    [self.withdrawalView.layer addSublayer:line4];
    
    
    // 读取提现数据
    
    // 可提现金额
    UILabel *canWithdrawalLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(32), 5, WidthRate(180), 30)];
    canWithdrawalLab.backgroundColor = [UIColor clearColor];
    canWithdrawalLab.text = @"可提现金额";
    canWithdrawalLab.textColor = HDVGFontColor;
    canWithdrawalLab.textAlignment = NSTextAlignmentLeft;
    canWithdrawalLab.font = [UIFont systemFontOfSize:15.0f];
    [self.withdrawalView addSubview:canWithdrawalLab];
    
    // 金额
    self.canWithdrawalMoney = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(370), 5, WidthRate(330), 30)];
    self.canWithdrawalMoney.backgroundColor = [UIColor clearColor];
    self.canWithdrawalMoney.text = [NSString stringWithFormat:@"¥%@", self.canMoneyStr];
    self.canWithdrawalMoney.textColor = HDVGFontColor;
    self.canWithdrawalMoney.textAlignment = NSTextAlignmentRight;
    self.canWithdrawalMoney.font = [UIFont systemFontOfSize:15.0f];
    [self.withdrawalView addSubview:self.canWithdrawalMoney];
    
    
    // 提现金额
    UILabel *withdrawalLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 45, WidthRate(180), 30)];
    withdrawalLab.backgroundColor = [UIColor clearColor];
    withdrawalLab.text = @"提 现 金 额";
    withdrawalLab.textColor = HDVGFontColor;
    withdrawalLab.textAlignment = NSTextAlignmentLeft;
    withdrawalLab.font = [UIFont systemFontOfSize:15.0f];
    [self.withdrawalView addSubview:withdrawalLab];
    
    // 金额
    self.canMoneyTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(230), 45, WidthRate(480), 30)];
    self.canMoneyTextField.borderStyle = UITextBorderStyleNone;
    self.canMoneyTextField.delegate = self;
    self.canMoneyTextField.tag = 1;
    self.canMoneyTextField.placeholder = @"请输入提现金额";
    self.canMoneyTextField.textColor = HDVGFontColor;
    self.canMoneyTextField.textAlignment = NSTextAlignmentRight;
    self.canMoneyTextField.font = [UIFont systemFontOfSize:15.0f];
    self.canMoneyTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.canMoneyTextField.returnKeyType = UIReturnKeyDone;
    self.canMoneyTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self.withdrawalView addSubview:self.canMoneyTextField];
    
    
    // 验证码
    UILabel *authcodeLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 85, WidthRate(180), 30)];
    authcodeLab.backgroundColor = [UIColor clearColor];
    authcodeLab.text = @"验   证   码";
    authcodeLab.textColor = HDVGFontColor;
    authcodeLab.textAlignment = NSTextAlignmentLeft;
    authcodeLab.font = [UIFont systemFontOfSize:15.0f];
    [self.withdrawalView addSubview:authcodeLab];
    
    // 验证码输入框
    self.authcodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(230), 85, WidthRate(270), 30)];
    self.authcodeTextField.borderStyle = UITextBorderStyleNone;
    self.authcodeTextField.delegate = self;
    self.authcodeTextField.tag = 2;
    self.authcodeTextField.placeholder = @"输入验证码";
    self.authcodeTextField.textColor = HDVGFontColor;
    self.authcodeTextField.textAlignment = NSTextAlignmentCenter;
    self.authcodeTextField.font = [UIFont systemFontOfSize:15.0f];
    self.authcodeTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.authcodeTextField.returnKeyType = UIReturnKeyDone;
    self.authcodeTextField.keyboardType = UIReturnKeyDefault;
    [self.withdrawalView addSubview:self.authcodeTextField];
    
    // 验证码图片
    UIImageView *authcodeImage = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(240), 85, WidthRate(200), 30)];
    authcodeImage.backgroundColor = [UIColor lightGrayColor];
    authcodeImage.image = [PMNetworking getVerifyCode];
    [self.withdrawalView addSubview:authcodeImage];
    // 点击图片更换验证码
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeAuthcodeImage:)];
    [authcodeImage addGestureRecognizer:tap];
    authcodeImage.userInteractionEnabled = YES;
    
}

#pragma mark 点击图片更换验证码
- (void)changeAuthcodeImage:(UITapGestureRecognizer *)tap
{
    UIImageView *iv = (UIImageView *)tap.view;
    iv.image = [PMNetworking getVerifyCode];
}

// 按下Done，让软键盘回收
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.canMoneyTextField resignFirstResponder];
    [self.authcodeTextField resignFirstResponder];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.canMoneyTextField.tag == 1 && textField == self.canMoneyTextField && textField.text.length - range.length + string.length > 12) {
        return NO;
    }
    
    if (self.authcodeTextField.tag == 2 && textField == self.authcodeTextField && textField.text.length - range.length + string.length > 6) {
        return NO;
    }
    
    return YES;
}

// 创建提现按钮 button
- (void)createWithdrawalButton
{
    self.withdrawalButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.withdrawalButton.frame = CGRectMake(WidthRate(36), self.withdrawalView.frame.origin.y + 120 + HeightRate(120), WIDTH - WidthRate(36) * 2, 40);
    [self.withdrawalButton setTitle:@"提交申请" forState:UIControlStateNormal];
    [self.withdrawalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.withdrawalButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [self.withdrawalButton setBackgroundColor:ButtonBgColor];
    [self.withdrawalButton.layer setCornerRadius:6.0f];
    [self.withdrawalButton addTarget:self action:@selector(withdrawalButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.withdrawalButton];
}

#pragma mark 检查是否已经绑定银行卡
- (void)checkBindingBank
{
    self.networking = [PMNetworking defaultNetworking];
    
    NSDictionary *checkBank = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID};
    
    [self.networking request:PMRequestStateShareCheckBank WithParameters:checkBank callBackBlock:^(NSDictionary *dict) {
        //已绑定银行卡
        if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
            
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"您还没有绑定银行卡，请绑定！" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alertView.tag = 1;
            [alertView show];
            
            self.withdrawalButton.enabled = NO;
        }
    }showIndicator:NO];
}

#pragma mark 提现按钮响应事件
- (void)withdrawalButton:(id)sender
{
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    // 判断是否为空
    if (self.canMoneyTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入提现金额"];
        return;
    }
    if (self.authcodeTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入验证码"];
        return;
    }
    
    if (self.canMoneyTextField.text.length != 0 && self.authcodeTextField.text.length != 0) {
        
        if ([self.canMoneyTextField.text doubleValue] >= 100.00) {
            if ([self.canMoneyTextField.text doubleValue] <= [self.canMoneyStr doubleValue]) {
                
                NSString *money = [NSString stringWithFormat:@"%.2lf", [self.canMoneyTextField.text doubleValue]];
                
                NSDictionary *cashWithdrawals = @{@"PM_SID":userInfots.PM_SID, @"money":money, @"verificationCode":self.authcodeTextField.text};
                self.networking = [PMNetworking defaultNetworking];
                
                [self.networking request:PMRequestStateShareApplyGetMoney WithParameters:cashWithdrawals callBackBlock:^(NSDictionary *dict) {
                    
                    if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"提现申请已提交" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
                        alertView.tag = 2;
                        [alertView show];
                    }
                    else {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alertView show];
                    }
                }showIndicator:YES];
            }
            else {
                [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"提现金额不足"];
            }
        }
        else {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"提现金额应不小于100元"];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (alertView.tag == 1 && buttonIndex == 1) {
        //[self.navigationController popToRootViewControllerAnimated:YES];
        
//        [[MasterViewController defaultMasterVC] transitionToBankcard];
//        [self.navigationController popViewControllerAnimated:NO];
        
        BankcardViewController *bankVC = [[BankcardViewController alloc] init];
        bankVC.isFromGet = YES;
        [self.navigationController pushViewController:bankVC animated:YES];
    }
    
    if (alertView.tag == 2) {
        //[self.navigationController popViewControllerAnimated:YES];
        
//        PMGetCashTableViewController *getCashVC = [[PMGetCashTableViewController alloc] init];
//        getCashVC.withdrawalMoney = YES;
//        [self.navigationController pushViewController:getCashVC animated:YES];
        
        [[MasterViewController defaultMasterVC] transitionToGetCashVC];
        [self.navigationController popViewControllerAnimated:NO];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
