//
//  MyCenterViewController.h
//  changeViewController
//
//  Created by pmit on 15/7/3.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFStretchableTableHeaderView.h"

@interface MyCenterViewController : UIViewController

@property (strong,nonatomic) HFStretchableTableHeaderView *stretchHeader;
@property (assign,nonatomic) BOOL isPayComeBack;
@property (assign,nonatomic) BOOL isNoFinish;
@property (nonatomic,strong) NSURL *photoUrl;
@property (assign,nonatomic) BOOL isFromWeb;

@end
