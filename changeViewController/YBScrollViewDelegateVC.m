//
//  YBScrollViewDelegateVC.m
//  changeViewController
//
//  Created by ZhangEapin on 15/5/1.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBScrollViewDelegateVC.h"
#import "PMMyPhoneInfo.h"

@interface YBScrollViewDelegateVC ()

@end

@implementation YBScrollViewDelegateVC

static YBScrollViewDelegateVC *_delegateVC;

+ (YBScrollViewDelegateVC *)defaultDelegate
{
    if (!_delegateVC)
    {
        _delegateVC = [[YBScrollViewDelegateVC alloc] init];
    }
    
    return _delegateVC;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.tag != 101)
        [_delegateVC.pageControl setCurrentPage: scrollView.contentOffset.x / scrollView.bounds.size.width];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    for (UIView *v in scrollView.subviews){
        return v;
    }
    return nil;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.imageScrollView){
        CGFloat x = scrollView.contentOffset.x;
        if (x==0.0){
            
        }
        else {
//            offset = x;
            for (UIScrollView *s in scrollView.subviews){
                if ([s isKindOfClass:[UIScrollView class]]){
                    [s setZoomScale:1.0];
                    UIImageView *image = [[s subviews] objectAtIndex:0];
                    image.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
                }
            }
        }
    }
}


@end
