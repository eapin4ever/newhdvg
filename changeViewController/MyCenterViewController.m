//
//  MyCenterViewController.m
//  changeViewController
//
//  Created by pmit on 15/7/3.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "MyCenterViewController.h"
#import "PMMyPhoneInfo.h"
#import "MyCenterCell.h"
#import "UserNameViewController.h"
#import "AddressManageTableViewController.h"
#import "PMMyUnreadViewController.h"
#import "YBMyRedViewController.h"
#import "DistributionShopViewController.h"
#import "MasterViewController.h"
#import "ParallaxHeaderView.h"
#import "PMImageCroperSingleton.h"
#import "DetailTradeViewController.h"
#import "SWPayCostViewController.h"
#import "PMMyCenterMyTradeCell.h"
#import "PMMyAllOrderViewController.h"

@interface MyCenterViewController () <UITableViewDataSource,UITableViewDelegate,PMImageCroperDelegate,UIActionSheetDelegate,PMMyCenterMyTradeCellDelegate>

@property (strong,nonatomic) UITableView *centerTableView;
@property (strong,nonatomic) NSArray *titleArr;
@property (strong,nonatomic) NSArray *imageArr;
@property (strong,nonatomic) PMImageCroperSingleton *portrait;
@property (strong,nonatomic) UIButton *takePhotoBtn;
@property (strong,nonatomic) UIButton *loginBtn;
@property (strong,nonatomic) UILabel *nameLabel;
@property (strong,nonatomic) UIImageView *sexIV;
@property (strong,nonatomic) UILabel *signLB;
@property (copy,nonatomic) NSString *payCountString;
@property (copy,nonatomic) NSString *receiveString;
@property (copy,nonatomic) NSString *complainString;
@property (copy,nonatomic) NSString *discussString;
@property (strong,nonatomic) NSDictionary *userDic;
@property (strong,nonatomic) UIButton *payBtn;
@property (strong,nonatomic) UIButton *waitBtn;
@property (strong,nonatomic) UIButton *discussBtn;
@property (strong,nonatomic) UIButton *relevantBtn;
@property (strong,nonatomic) UIView *tradeView;
@property (strong,nonatomic) UILabel *careProductCountLB;
@property (strong,nonatomic) UILabel *careShopCountLB;

@end

@implementation MyCenterViewController

static NSString *const centerCell = @"centerCell";
static NSString *const myCenterTradeCell = @"centerTradeCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的微购";
    self.navigationItem.rightBarButtonItem = [self CreateRightBarBtn];
    self.view.backgroundColor = HDVGPageBGGray;
    [self buildTableView];
    [self buildHeaderView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.isFromWeb)
    {
        
    }
    else
    {
        [[MasterViewController defaultMasterVC].tabBar setAlpha:1];
    }
    
    if ([PMUserInfos shareUserInfo].PM_SID)
    {
        [self getMyInfo];
    }
    
    [self.centerTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildTableView
{
    self.centerTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50) style:UITableViewStylePlain];
    if (self.isFromWeb)
    {
        self.centerTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
    }
    self.centerTableView.delegate = self;
    self.centerTableView.dataSource = self;
    self.centerTableView.backgroundColor = RGBA(247, 247, 247, 1);
    self.centerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.centerTableView registerClass:[MyCenterCell class] forCellReuseIdentifier:centerCell];
    [self.centerTableView registerClass:[PMMyCenterMyTradeCell class] forCellReuseIdentifier:myCenterTradeCell];
    [self.view addSubview:self.centerTableView];
    self.imageArr = @[@[@"order"],@[@"center_person",@"center_address",@"center_message"],@[@"center_red",@"center_distubition"]];
    self.titleArr = @[@[@"全部订单",@"占位用"],@[@"资料管理",@"地址管理",@"消息管理"],@[@"我的红包",@"我的代言"]];
    
}

- (void)buildHeaderView
{
    ParallaxHeaderView *headerView = [ParallaxHeaderView parallaxHeaderViewWithCGSize:CGSizeMake(WIDTH, 180)];
    headerView.headerImage = [UIImage imageNamed:@"background2"];
    [self.centerTableView setTableHeaderView:headerView];
    
    PMImageCroperSingleton *portrait = [PMImageCroperSingleton addProtraitToTarget:self SuperView:headerView  WithFrame:CGRectMake(WidthRate(40),22.5,80,80)];
    self.portrait = portrait;
    self.portrait.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageUpload:)];
    [portrait.portraitImageView addGestureRecognizer:tap];
    portrait.portraitImageView.userInteractionEnabled = YES;
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(portrait.portraitImageView.frame.origin.x+50, portrait.portraitImageView.frame.origin.y+55, 30, 30)];
    [btn setImage:[UIImage imageNamed:@"center_takephoto"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(changePortrait:) forControlEvents:UIControlEventTouchUpInside];
    self.takePhotoBtn = btn;
    [headerView addSubview:btn];
    
    self.loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loginBtn.frame = CGRectMake(portrait.portraitImageView.frame.origin.x + portrait.portraitImageView.frame.size.width + WidthRate(40), portrait.portraitImageView.frame.origin.y + 27, WidthRate(120), 25);
    [self.loginBtn setTitle:@"登 录" forState:UIControlStateNormal];
    [self.loginBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [self.loginBtn.layer setCornerRadius:4.0f];
    [self.loginBtn.layer setBorderWidth:0.5f];
    [self.loginBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    self.loginBtn = self.loginBtn;
    [self.loginBtn addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:self.loginBtn];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(portrait.portraitImageView.frame.origin.x + portrait.portraitImageView.frame.size.width + WidthRate(40), 35, WidthRate(300), 25)];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor whiteColor];
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    self.nameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:19];
    self.nameLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    self.nameLabel.shadowColor = [UIColor grayColor];
    [headerView addSubview:self.nameLabel];
    
    self.sexIV = [[UIImageView alloc] initWithFrame:CGRectMake(self.nameLabel.frame.origin.x + WidthRate(330), self.nameLabel.frame.origin.y + 2, 25, 25)];
    self.sexIV.contentMode = UIViewContentModeScaleAspectFit;
    [headerView addSubview:self.sexIV];
    
    self.signLB = [[UILabel alloc] initWithFrame:CGRectMake(portrait.portraitImageView.frame.origin.x + portrait.portraitImageView.frame.size.width + WidthRate(40), 60, WIDTH - (portrait.portraitImageView.frame.size.width + WidthRate(80) + 10), 40)];
    self.signLB.backgroundColor = [UIColor clearColor];
    self.signLB.numberOfLines = 2;
    self.signLB.textColor = [UIColor whiteColor];
    self.signLB.textAlignment = NSTextAlignmentLeft;
    self.signLB.font = [UIFont systemFontOfSize:14.0f];
    self.signLB.shadowOffset = CGSizeMake(1.0, 1.0);
    self.signLB.shadowColor = [UIColor grayColor];
    [headerView addSubview:self.signLB];
    
    if ([PMUserInfos shareUserInfo].PM_SID == nil)
    {
        [portrait.portraitImageView setImage:[UIImage imageNamed:@"default_user_portrait"]];
        [self hiddenOrShow:NO];
    }
    
    for (NSInteger i = 0; i < 2; i++)
    {
        UIView *careView = [[UIView alloc] initWithFrame:CGRectMake(i * (WIDTH - 1) / 2 + i * 1, self.signLB.frame.origin.y + 70, (WIDTH - 1) / 2, 190 - self.signLB.frame.origin.y - 80)];
        careView.alpha = 0.8;
        careView.backgroundColor = RGBA(51, 43, 69, 0.8);
        [headerView addSubview:careView];
        UILabel *countLB = [[UILabel alloc] initWithFrame:CGRectMake(0, careView.bounds.size.height * 0.1, careView.bounds.size.width, careView.bounds.size.height * 0.3)];
        countLB.textAlignment = NSTextAlignmentCenter;
        countLB.font = [UIFont systemFontOfSize:16.0f];
        countLB.textColor = [UIColor whiteColor];
        countLB.text = @"0";
        [careView addSubview:countLB];
        switch (i)
        {
            case 0:
                self.careProductCountLB = countLB;
                break;
            
            case 1:
                self.careShopCountLB = countLB;
                break;
                
            default:
                break;
        }
        
        UILabel *countTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, careView.bounds.size.height * 0.6, careView.bounds.size.width, careView.bounds.size.height * 0.2)];
        countTitleLB.textAlignment = NSTextAlignmentCenter;
        countTitleLB.font = [UIFont systemFontOfSize:12.0f];
        countTitleLB.textColor = [UIColor whiteColor];
        [careView addSubview:countTitleLB];
        switch (i)
        {
            case 0:
                countTitleLB.text = @"关注的商品";
                break;
                
            case 1:
                countTitleLB.text = @"关注的店铺";
                break;
                
            default:
                break;
        }
        
        UIButton *careBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        careBtn.frame = CGRectMake(0, 0, careView.bounds.size.width, careView.bounds.size.height);
        careBtn.tag = 200 + i;
        [careBtn addTarget:self action:@selector(goToMyCare:) forControlEvents:UIControlEventTouchUpInside];
        [careView addSubview:careBtn];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.titleArr[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!(indexPath.section == 0 && indexPath.row == 1))
    {
        MyCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:centerCell];
        [cell createUI];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.iconIV.image = [UIImage imageNamed:self.imageArr[indexPath.section][indexPath.row]];
        cell.titleLB.text = self.titleArr[indexPath.section][indexPath.row];
        if ((indexPath.section == 1 && indexPath.row == 0) || (indexPath.section == 2 && indexPath.row == 2))
        {
            if (indexPath.section % 2 == 0)
            {
                cell.tipLB.text = @"设置你的个性资料";
            }
            else
            {
                cell.tipLB.text = @"轻松开店,快速赚钱";
            }
        }
        else if (indexPath.section == 0 && indexPath.row == 0)
        {
            cell.tipLB.text = @"查看全部订单";
        }
        else
        {
            cell.tipLB.text = @"";
        }
        
        return cell;
    }
    else
    {
        PMMyCenterMyTradeCell *cell = [tableView dequeueReusableCellWithIdentifier:myCenterTradeCell];
        [cell createUI];
        cell.myCenterCellDelegate = self;
        if (![PMUserInfos shareUserInfo].PM_SID || [[PMUserInfos shareUserInfo].PM_SID isEqualToString:@""])
        {
            cell.payBtn.hidden = YES;
            cell.waitBtn.hidden = YES;
            cell.discussBtn.hidden = YES;
            cell.relevantBtn.hidden = YES;
        }
        else
        {
            cell.payBtn.hidden = YES;
            cell.waitBtn.hidden = YES;
            cell.discussBtn.hidden = YES;
            cell.relevantBtn.hidden = YES;
            [cell setCellData:[self.payCountString integerValue] GetCount:[self.receiveString integerValue] DiscussCount:[self.discussString integerValue] BackCount:[self.complainString integerValue]];
        }
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1)
    {
        return 50;
    }
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = RGBA(247, 247, 247, 1);
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![PMUserInfos shareUserInfo].PM_SID)
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            
            [self jumpToVC:indexPath.section AndRow:indexPath.row];
        };
        
        showAlertViewLogin;
    }
    else
    {
        [self jumpToVC:indexPath.section AndRow:indexPath.row];
    }
}

- (void)jumpToVC:(NSInteger)section AndRow:(NSInteger)row
{
    UIViewController *vc;
    switch (section)
    {
        case 0:
        {
            switch (row)
            {
                case 0:
                    
                    vc = [[PMMyAllOrderViewController alloc] init];
                    
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (row)
            {
                case 0:
                    vc = [[UserNameViewController alloc] init];
                    break;
                case 1:
                    vc = [[AddressManageTableViewController alloc] init];
                    break;
                case 2:
                    vc = [[PMMyUnreadViewController alloc] init];
                    break;
                    
                default:
                    break;
            }
        }
            
            break;
        case 2:
        {
            switch (row)
            {
                case 0:
                    vc = [[YBMyRedViewController alloc] init];
                    break;
//                case 1:
//                    vc = [[MyAttentionViewController alloc] init];
//                    break;
                case 1:
                    vc = [[DistributionShopViewController alloc] init];
//                    vc =  [[SWPayCostViewController alloc] init];
                    break;
                    
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.centerTableView)
    {
//         pass the current offset of the UITableView so that the ParallaxHeaderView layouts the subViews.
        [(ParallaxHeaderView *)self.centerTableView.tableHeaderView layoutHeaderViewForScrollViewOffset:scrollView.contentOffset];
    }
}

- (void)hiddenOrShow:(BOOL)isShow
{
    if (isShow)
    {
        self.nameLabel.hidden = NO;
        self.takePhotoBtn.hidden = NO;
        self.loginBtn.hidden = YES;
        self.signLB.hidden = NO;
        self.sexIV.hidden = NO;
    }
    else
    {
        self.nameLabel.hidden = YES;
        self.takePhotoBtn.hidden = YES;
        self.loginBtn.hidden = NO;
        self.signLB.hidden = YES;
        self.sexIV.hidden = YES;
    }
}

- (void)imageUpload:(UITapGestureRecognizer *)tap
{
    if ([PMUserInfos shareUserInfo].PM_SID)
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册中选取", nil];
        actionSheet.tag = 1;
        [actionSheet showInView:self.view];
    }
    else
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            
        };
        
        showAlertViewLogin;
    }
}

- (void)changePortrait:(UIButton *)sender
{
    if ([PMUserInfos shareUserInfo].PM_SID)
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册中选取", nil];
        actionSheet.tag = 1;
        [actionSheet showInView:self.view];
    }
    else
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            
        };
        
        showAlertViewLogin;
    }
}

- (void)login:(UIButton *)sender
{
    [MasterViewController defaultMasterVC].afterLoginAction = ^{
        
        [self getMyInfo];
    };
    
    showAlertViewLogin;
}

- (void)getMyInfo
{
    [[PMNetworking defaultNetworking] request:PMRequestStateMyInfo WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSDictionary *dataDic = [dic objectNullForKey:@"data"];
            NSDictionary *orderInfoDic = [dataDic objectNullForKey:@"orderInfo"];
            self.payCountString = [NSString stringWithFormat:@"%@",[orderInfoDic objectNullForKey:@"dfkCount"]];
            self.receiveString = [NSString stringWithFormat:@"%@",[orderInfoDic objectNullForKey:@"dshCount"]];
            self.complainString = [NSString stringWithFormat:@"%@",[orderInfoDic objectNullForKey:@"thCount"]];
            self.discussString = [NSString stringWithFormat:@"%@",[orderInfoDic objectNullForKey:@"dpjCount"]];
            NSInteger myCareProCnt = [[dataDic objectForKey:@"myCareProCnt"] integerValue];
            NSInteger myShopCareProCnt = [[dataDic objectForKey:@"myShopCareCnt"] integerValue];
            self.careProductCountLB.text = [NSString stringWithFormat:@"%@",@(myCareProCnt)];
            self.careShopCountLB.text = [NSString stringWithFormat:@"%@",@(myShopCareProCnt)];
            
            [self.centerTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            
            [PMUserInfos shareUserInfo].isReseller = [[[dataDic objectForKey:@"user"] objectForKey:@"isReseller"] integerValue];
            
            [self showOrHideByCount:self.payBtn AndCount:self.payCountString];
            [self showOrHideByCount:self.waitBtn AndCount:self.receiveString];
            [self showOrHideByCount:self.discussBtn AndCount:self.discussString];
            [self showOrHideByCount:self.relevantBtn AndCount:self.complainString];
            
            self.userDic = [dataDic objectNullForKey:@"user"];
            [self getUserMessage];
        }
        
    } showIndicator:NO];
}

- (void)getUserMessage
{
    NSString *myName = [self.userDic objectNullForKey:@"name"];
    CGSize lbSize = [myName sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:19]];
    self.nameLabel.frame = CGRectMake(self.nameLabel.frame.origin.x, self.nameLabel.frame.origin.y, lbSize.width, 30);
    self.nameLabel.text = myName;
    self.sexIV.frame = CGRectMake(self.nameLabel.frame.origin.x + lbSize.width + 10, self.sexIV.frame.origin.y, self.sexIV.frame.size.width, self.sexIV.frame.size.height);
    self.sexIV.image = [[self.userDic objectNullForKey:@"sex"] integerValue] == 1 ? [UIImage imageNamed:@"men60"] : [UIImage imageNamed:@"women60"];
    NSString *remarkStr = [self.userDic objectNullForKey:@"remark"];
    if ([[self.userDic objectNullForKey:@"photo"] isKindOfClass:[NSNull class]])
    {
        [self.portrait.portraitImageView setImage:[UIImage imageNamed:@"default_user_portrait"]];
    }
    else
    {
        [self.portrait.portraitImageView sd_setImageWithURL:[NSURL URLWithString:[self.userDic objectNullForKey:@"photo"]] placeholderImage:[UIImage imageNamed:@"default_user_portrait"]];
    }
    
    [self hiddenOrShow:YES];
    if (remarkStr.length == 0) {
        self.signLB.text = @"这家伙很懒，什么都没写！";
    }
    else
    {
        self.signLB.text = [self.userDic objectNullForKey:@"remark"];
    }
    
    [self hiddenOrShow:YES];
}

- (void)showOrHideByCount:(UIButton *)sender AndCount:(NSString *)countString
{
    sender.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    if ([countString integerValue] == 0)
    {
        sender.hidden = YES;
    }
    else if ([countString integerValue] > 0 && [countString integerValue] < 10)
    {
        sender.hidden = NO;
        [sender setTitle:countString forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        sender.hidden = NO;
        [sender setTitle:@"···" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)goToMyTrade:(UIButton *)sender
{
    if ([PMUserInfos shareUserInfo].PM_SID)
    {
        DetailTradeViewController *detailTradeVC = [[DetailTradeViewController alloc] init];
        switch (sender.tag) {
            case 100:
                detailTradeVC.tradeStatus = tradeStatusWaitPay;
                break;
            case 101:
                detailTradeVC.tradeStatus = tradeStatusWaitGet;
                break;
            case 102:
                detailTradeVC.tradeStatus = tradeStatusReceive;
                break;
            case 103:
                detailTradeVC.tradeStatus = tradeStatusTurnBack;
                break;
            default:
                break;
        }
        [self.navigationController pushViewController:detailTradeVC animated:YES];
    }
    else
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            DetailTradeViewController *detailTradeVC = [[DetailTradeViewController alloc] init];
            switch (sender.tag) {
                case 100:
                    detailTradeVC.tradeStatus = tradeStatusWaitPay;
                    break;
                case 101:
                    detailTradeVC.tradeStatus = tradeStatusWaitGet;
                    break;
                case 102:
                    detailTradeVC.tradeStatus = tradeStatusReceive;
                    break;
                case 103:
                    detailTradeVC.tradeStatus = tradeStatusTurnBack;
                    break;
                default:
                    break;
            }
            [self.navigationController pushViewController:detailTradeVC animated:YES];
        };
        
        showAlertViewLogin;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.portrait actionSheet:actionSheet clickedButtonAtIndex:buttonIndex];
}

- (void)finishActionWithCompressImgData:(NSData *)data
{
    NSDictionary *param = @{PMSID,@"flag":@"userPhoto"};
    
    //上传头像
    [[PMNetworking defaultNetworking] uploadImage:param imageData:data success:^(NSDictionary *dic) {
        self.portrait.portraitImageView.image = [UIImage imageWithData:data];
        [self getMyInfo];
    }];
}

- (UIBarButtonItem *)CreateRightBarBtn
{
    UIButton *settingBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [settingBtn setImage:[UIImage imageNamed:@"center_settings"] forState:UIControlStateNormal];
    settingBtn.tag = 99;
    [settingBtn addTarget:self action:@selector(goToSetting:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithCustomView:settingBtn];
    return bbi;
}

- (void)goToSetting:(UIButton *)sender
{
    SettingViewController *setVC = [[SettingViewController alloc] init];
    [self.navigationController pushViewController:setVC animated:YES];
}

- (void)goToOutTradeDetail:(UIButton *)sender
{
    [self goToMyTrade:sender];
}

- (void)goToMyCare:(UIButton *)sender
{
    if (![PMUserInfos shareUserInfo].PM_SID || [[PMUserInfos shareUserInfo].PM_SID isEqualToString:@""])
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            MyAttentionViewController *attentionVC = [[MyAttentionViewController alloc] init];
            attentionVC.isShop = sender.tag == 200 ? NO : YES;
            [self.navigationController pushViewController:attentionVC animated:YES];
        };
        
        showAlertViewLogin;
    }
    else
    {
        MyAttentionViewController *attentionVC = [[MyAttentionViewController alloc] init];
        attentionVC.isShop = sender.tag == 200 ? NO : YES;
        [self.navigationController pushViewController:attentionVC animated:YES];
    }
    
}

@end
