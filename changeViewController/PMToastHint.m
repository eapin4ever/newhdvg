//
//  PMToastHint.m
//  changeViewController
//
//  Created by pmit on 15/1/14.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMToastHint.h"

@interface PMToastHint()

@end

static PMToastHint *_toast;
@implementation PMToastHint
{
    UILabel *_toastLB;
}
+ (PMToastHint *)defaultToastWithRight:(BOOL)isRight
{
    if(!_toast)
    {
        _toast = [[PMToastHint alloc] initWithFrame:CGRectMake(0,0, WidthRate(420), HeightRate(260))];
//        _toast = [[PMToastHint alloc] init];
        [_toast createUI];
    }
     _toast.isRight = isRight;
    return _toast;
}

- (void)createUI
{
    self.alpha = 0;
    self.layer.cornerRadius = 6.0;
    self.clipsToBounds =YES;
    self.backgroundColor = [UIColor blackColor];
    
    _iv = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width/2 - WidthRate(40), HeightRate(45), WidthRate(80), WidthRate(80))];
   
    
    [self addSubview:_iv];

    _toastLB = [[UILabel alloc] initWithFrame:CGRectMake(0, HeightRate(160), self.bounds.size.width, HeightRate(80))];
    [self addSubview:_toastLB];
    
    _toastLB.font = [UIFont boldSystemFontOfSize:HeightRate(28)];
    _toastLB.textColor = [UIColor whiteColor];
    _toastLB.numberOfLines = 2;
    _toastLB.textAlignment = NSTextAlignmentCenter;
}

#pragma mark - 使用吐司式提示
- (void)showHintToView:(UIView *)view ByToast:(NSString *)title
{
    if (self.isRight)
    {
        _iv.image = [UIImage imageNamed:@"hintIcon"];
    }
    else
    {
        _iv.image = [UIImage imageNamed:@"wrongIcon"];
    }
    self.center = view.window.center;
   
    [view.window addSubview:self];
    
    _toastLB.text = title;

    
    
    [UIView animateWithDuration:0.6 animations:^{
        self.alpha = 0.8;
        view.userInteractionEnabled = NO;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.5f animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            view.userInteractionEnabled = YES;
        }];
    }];
}



@end
