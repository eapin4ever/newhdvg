//
//  PMHistorySearchCell.h
//  changeViewController
//
//  Created by ZhangEapin on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMHistorySearchCell : UITableViewCell

@property (strong,nonatomic) UILabel *historyLB;

- (void)createUI;

@end
