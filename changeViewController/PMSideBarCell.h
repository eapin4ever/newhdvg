//
//  PMSideBarCell.h
//  changeViewController
//
//  Created by pmit on 14/12/4.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMSideBarCell : UICollectionViewCell
- (void)createUI;
- (void)setImage:(UIImage *)image title:(NSString *)string;
@end
