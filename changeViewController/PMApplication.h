//
//  PMApplication.h
//  changeViewController
//
//  Created by pmit on 15/7/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kApplicationTimeoutInMinutes 30

#define kApplicationDidTimeoutNotification @"ApplicationDidTimeout"

@interface PMApplication : UIApplication
{
    NSTimer *_idleTimer;
}

- (void)resetIdleTimer;

@end
