//
//  MyAttentionViewController.h
//  changeViewController
//
//  Created by wallace on 14/11/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

#import "PMMyAttentionGoodsTVC.h"
#import "PMMyAttentionShopTVC.h"


@interface MyAttentionViewController : UIViewController
@property (strong,nonatomic) UIView *noResultView;
@property (strong,nonatomic) UIImageView *tipImage;
@property (assign,nonatomic) BOOL isShop;

//无数据提示图
- (void)createNoResult;

- (void)showNoResultViewWithIndex:(NSInteger)index;
@end
