//
//  PMNavigationController.m
//  changeViewController
//
//  Created by pmit on 14/12/1.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMNavigationController.h"
#import "MasterViewController.h"

@interface PMNavigationController ()

@end

@implementation PMNavigationController
- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if(self)
    {
        if(SYSTEM_VERSION >= 7.0)
        {
            [self.navigationBar setBarTintColor:NAVGray];
            [self.navigationBar setTranslucent:NO];
        }
        else
        {
            [self.navigationBar setTintColor:NAVGray];
        }
        [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:RGBA(160, 160, 160, 1), NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:19], NSFontAttributeName, nil]];
        
        self.navigationBar.tintColor = NAVTEXTCOLOR;
        
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    return self;
}

//重写push方法,自定义返回按钮
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    //push时隐藏tabBar
    [UIView animateWithDuration:0.3 animations:^{
        [[MasterViewController defaultMasterVC].tabBar setAlpha:0];
    }];
    
    [super pushViewController:viewController animated:animated];

}

//- (UIViewController *)popViewControllerAnimated:(BOOL)animated
//{
//    [super popViewControllerAnimated:animated];
//    //push时隐藏tabBar
//    [UIView animateWithDuration:0.3 animations:^{
//        [[MasterViewController defaultMasterVC].tabBar setAlpha:1];
//    }];
//    
//    return nil;
//}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
}


@end
