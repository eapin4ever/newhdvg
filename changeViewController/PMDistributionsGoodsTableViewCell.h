//
//  PMDistributionsGoodsTableViewCell.h
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "YBShareBtn.h"

@interface PMDistributionsGoodsTableViewCell : UITableViewCell
@property(nonatomic,strong)YBShareBtn *shareBtn;
@property(nonatomic,strong)UIImageView *iv;
@property(nonatomic,strong)UIButton *goDetailBtn;
@property(nonatomic,strong)UIView *messageView;
@property (strong,nonatomic) YBShareBtn *deleteBtn;

- (void)createUI;

- (void)setContentTitle:(NSString *)title unitPrice:(NSString *)price percent:(CGFloat)percent;

@end
