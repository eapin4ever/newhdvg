//
//  FeedbackViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/19.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "FeedbackViewController.h"
#import "PMToastHint.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"意见反馈";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    // 给页面添加背景色
    self.view.backgroundColor = HDVGPageBGGray;
    
    [self addFeedbackTextView];
    [self addFeedbackSubmitButton];
}

- (void)addFeedbackTextView
{
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, HeightRate(323))];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [bgView.layer addSublayer:layer];
    
    self.feedView = [[UITextView alloc] initWithFrame:CGRectMake(WidthRate(2), HeightRate(2), WIDTH - WidthRate(4), HeightRate(320))];
    self.feedView.delegate = self;
    self.feedView.textColor = RGBA(79, 79, 79, 1);
    self.feedView.font = [UIFont systemFontOfSize:14.0f];
    self.feedView.layer.cornerRadius = 0.0f;  // 设置圆角值
    self.feedView.layer.masksToBounds = YES;
    self.feedView.layer.borderWidth = 0.5f;   // 设置边框大小
    [self.feedView.layer setBorderColor:[[UIColor clearColor] CGColor]];
    self.feedView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.feedView.returnKeyType = UIReturnKeyDone;
    self.feedView.keyboardType = UIKeyboardTypeDefault;
    self.feedView.scrollEnabled = YES;
    [bgView addSubview:self.feedView];
    
    self.tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.feedView.frame.size.width, 25)];
    self.tipsLabel.backgroundColor = [UIColor clearColor];
    self.tipsLabel.text = @"请在此输入您的宝贵意见";
    self.tipsLabel.textColor = RGBA(231, 231, 231, 1);
    [self.feedView addSubview:self.tipsLabel];
    self.tipsLabel.hidden = NO;
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(0, self.feedView.frame.origin.y + HeightRate(320), WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [bgView.layer addSublayer:layer2];
}

- (void)addFeedbackSubmitButton
{
    // 提交按钮
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStylePlain target:self action:@selector(submitButtonClick:)];
    
    [submitButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR,NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = submitButton;
}

- (void)submitButtonClick:(id)sender
{
    if ([PMUserInfos shareUserInfo].PM_SID == nil) {
        
           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"亲，请先登录!!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
           [alertView show];
    }
    else {
        // 内容不能为空
        if (self.feedView.text.length == 0) {
            
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"意见反馈内容不能为空"];
        }
        
        if (self.feedView.text.length != 0) {
            
            /**
             参数：
             PM_SID	      Y	    String		   Token
             fbContent	  N	    String		   反馈内容
             remark	      Y	    String		   手机型号;系统版本号;软件版本号
             */
            
            PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
            
            // 手机型号
            NSString *phoneModel = [[UIDevice currentDevice] model];
            // 手机系统版本
            NSString *phoneVersion = [[UIDevice currentDevice] systemVersion];
            // 当前应用软件版本 比如：1.0.1
            NSString *appCurVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
            
            NSMutableString *string1 = [NSMutableString stringWithFormat:@"%@", phoneVersion];
            [string1 insertString:@";" atIndex:0];
            NSMutableString *string2 = [NSMutableString stringWithFormat:@"%@", appCurVersion];
            [string2 insertString:@";" atIndex:0];
            
            //可变字符串
            NSMutableString *string = [NSMutableString string];
            //添加字符串
            [string appendString:phoneModel];
            [string appendString:string1];
            [string appendString:string2];
            NSString *remark = [NSString stringWithFormat:@"%@", string];
            
            NSDictionary *feedBack = @{@"PM_SID":userInfos.PM_SID,@"fbContent":self.feedView.text,@"remark":remark};
            
            self.networking = [PMNetworking defaultNetworking];
            
            [self.networking request:PMRequestStateUserFeedBack WithParameters:feedBack callBackBlock:^(NSDictionary *dict) {
                
                if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"意见反馈提交成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                    alertView.tag = 1;
                    [alertView show];
                }
                else {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }showIndicator:NO];
        }
    }
}

// 隐藏键盘，实现UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        return NO;
    }
    
    if (![text isEqualToString:@""]) {
        
        self.tipsLabel.hidden = YES;
    }
    
    if ([text isEqualToString:@""] && range.location == 0 && range.length == 1) {
        
        self.tipsLabel.hidden = NO;
    }
    
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.feedView resignFirstResponder];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
