//
//  PMMyCenterMyTradeCell.h
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PMMyCenterMyTradeCellDelegate <NSObject>

- (void)goToOutTradeDetail:(UIButton *)sender;

@end

@interface PMMyCenterMyTradeCell : UITableViewCell

@property (strong,nonatomic) UIView *tradeView;
@property (strong,nonatomic) UIButton *payBtn;
@property (strong,nonatomic) UIButton *waitBtn;
@property (strong,nonatomic) UIButton *discussBtn;
@property (strong,nonatomic) UIButton *relevantBtn;
@property (weak,nonatomic) id<PMMyCenterMyTradeCellDelegate> myCenterCellDelegate;

- (void)createUI;
- (void)setCellData:(NSInteger)payCount GetCount:(NSInteger)getCount DiscussCount:(NSInteger)discussCount BackCount:(NSInteger)backCount;

@end
