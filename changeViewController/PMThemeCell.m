//
//  PMThemeCell.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//
#define CELLHI WidthRate(375)
#define dxdy 2

#import "PMThemeCell.h"

@implementation PMThemeCell

- (void)createUI
{
    if(!_iv1)
    {
        // 图片
         _iv1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/2 - dxdy, CELLHI)];
        _iv1.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv1];
        
        _iv2 = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH/2+dxdy, 0, WIDTH/2 - dxdy, CELLHI/2-dxdy)];
        _iv2.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv2];
        
        _iv3 = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH/2+dxdy, CELLHI/2+dxdy, WIDTH/2 - dxdy, CELLHI/2 - dxdy)];
        _iv3.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv3];
        
        
        // 按钮
        _btn1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, WIDTH/2 - dxdy, CELLHI)];
        [self.contentView addSubview:_btn1];
        
        _btn2 = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/2+dxdy, 0, WIDTH/2 - dxdy, CELLHI/2-dxdy)];
        [self.contentView addSubview:_btn2];
        
        _btn3 = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/2+dxdy, CELLHI/2+dxdy, WIDTH/2 - dxdy, CELLHI/2 - dxdy)];
        [self.contentView addSubview:_btn3];
        
        CALayer *line1 = [CALayer layer];
        line1.backgroundColor = HDVGPageBGGray.CGColor;
        line1.frame = CGRectMake(WIDTH/2 - dxdy, 30, 1, CELLHI - 60);
        [self.contentView.layer addSublayer:line1];
        
        CALayer *line2 = [CALayer layer];
        line2.backgroundColor = HDVGPageBGGray.CGColor;
        line2.frame = CGRectMake(WIDTH/2 - dxdy, CELLHI/2-dxdy, WIDTH/2 - dxdy - 30, 1);
        [self.contentView.layer addSublayer:line2];
        
    }
}

- (void)addTarget:(id)target selector:(SEL)sel
{
    _btn1.tag =0;
    _btn2.tag =0;
    _btn3.tag =0;
    
    [_btn1 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [_btn2 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [_btn3 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];

}

@end
