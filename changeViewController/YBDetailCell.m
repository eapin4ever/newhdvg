//
//  YBDetailCell.m
//  changeViewController
//
//  Created by EapinZhang on 15/4/3.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBDetailCell.h"
#import "PMMyPhoneInfo.h"

@implementation YBDetailCell
{
    UILabel *contentLB;
    UILabel *moneyLB;
    UILabel *dateLB;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!dateLB)
    {
        //日期
        dateLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 0, WIDTH / 4, self.frame.size.height)];
        dateLB.backgroundColor =[UIColor clearColor];
        dateLB.font = [UIFont systemFontOfSize:12.0f];
        dateLB.textColor = [UIColor lightGrayColor];
        dateLB.textAlignment = NSTextAlignmentCenter;
        
        //内容
        contentLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 4 + WidthRate(35), 0, WIDTH / 3 + WidthRate(30), self.frame.size.height)];
        contentLB.backgroundColor = [UIColor clearColor];
        contentLB.font = [UIFont systemFontOfSize:12.0f];
        contentLB.textAlignment = NSTextAlignmentCenter;
        contentLB.textColor = [UIColor lightGrayColor];
        
        //金额
        moneyLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WIDTH / 3 + WidthRate(10), 0, WIDTH / 3 - WidthRate(30), self.frame.size.height)];
        moneyLB.backgroundColor = [UIColor clearColor];
        moneyLB.textAlignment = NSTextAlignmentCenter;
        moneyLB.textColor = HDVGRed;
        moneyLB.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:dateLB];
        [self.contentView addSubview:contentLB];
        [self.contentView addSubview:moneyLB];
    }
}

- (void)setDetailDic:(NSDictionary *)detailDic
{
    _detailDic = detailDic;
    
    contentLB.text = [self.detailDic objectNullForKey:@"businessName"];
    if ([[detailDic objectNullForKey:@"tradeType"] isEqualToString:@"1"])
    {
        moneyLB.textColor = RGBA(255, 106, 106, 1);
    }
    else
    {
        moneyLB.textColor = RGBA(51, 105, 30, 1);
    }
    
    moneyLB.text = [[detailDic objectNullForKey:@"tradeType"] isEqualToString:@"1"] ? [NSString stringWithFormat:@"+ %.2lf",[[self.detailDic objectNullForKey:@"tradeMoney"] doubleValue]]: [NSString stringWithFormat:@"- %.2lf",[[self.detailDic objectNullForKey:@"tradeMoney"] doubleValue]];
    dateLB.text = [[self.detailDic objectNullForKey:@"createDt"] substringFromIndex:5];
}

@end
