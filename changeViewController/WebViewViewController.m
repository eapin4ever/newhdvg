//
//  WebViewViewController.m
//  changeViewController
//
//  Created by EapinZhang on 15/3/27.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "WebViewViewController.h"
#import "PMMyPhoneInfo.h"
#import "MasterViewController.h"
#import "PMUserInfos.h"
#import "AppDelegate.h"
#import "SecondViewController.h"
#import "DetailTradeViewController.h"
#import "YTGifActivityIndicator.h"
#import "SWPayCostViewController.h"
#import "PMMyGoodsViewController.h"
#import "PMToastHint.h"
#import "PMShoppingCarViewController.h"
#import "MyCenterViewController.h"
#import "YBMyRedViewController.h"

#define webIp(address) [NSString stringWithFormat:@"http://hdvg.me/html/%@",address]
//#define webIp(address) [NSString stringWithFormat:@"http://120.24.234.76/html/%@",address]
//#define webIp(address) [NSString stringWithFormat:@"http://192.168.1.26:8123",address]
//#define webIp(address) [NSString stringWithFormat:@"http://192.168.1.29/html/%@",address]
//#define webIp(address) [NSString stringWithFormat:@"http://192.168.1.3:8085/wei/html/%@",address]
@interface WebViewViewController () <UIWebViewDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) UIWebView *webView;
@property (strong,nonatomic) SecondViewController *secondView;
@property (assign,nonatomic) NSInteger clickCount;
@property (strong,nonatomic) UIBarButtonItem *leftBtn;
@property (strong,nonatomic) YTGifActivityIndicator *indicator;

@end

@implementation WebViewViewController

static WebViewViewController *_webVC;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self createWebView];
    [self createNoNetUI];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.clickCount = 0;
    [_indicator start];
//    if (self.webView)
//    {
//        [self.webView reload];
//    }
}

- (instancetype)initWithWebType:(webType)webType
{
    if (!self)
    {
        self = [[WebViewViewController alloc] init];
    }
    _indicator = [YTGifActivityIndicator defaultIndicator];
    
    self.webViewType = webType;
    return self;
}

- (void)createNoNetUI
{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (![app isNetConnect])
    {
        app.noNetView.hidden = NO;
    }
    
}


- (void)createWebView
{
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    webView.delegate = self;
    self.webView = webView;
    NSString *urlString = @"";
    switch (self.webViewType)
    {
        case webTypeHongBao:
        {
            NSString *aString = [NSString stringWithFormat:@"CashBonus.html?userId=%@",[PMUserInfos shareUserInfo].userId];
            urlString = webIp(aString);
            self.title = @"红包首页";
        }
            break;
        case webTypeDiscount:
            urlString = webIp(@"Brdiscount.html");
            self.title = @"品牌折扣";
            break;
        case webTypePreBuy:
            urlString = webIp(@"Purchase.html");
            urlString = [NSString stringWithFormat:@"%@?productId=%@",urlString,self.productId];
            self.title = @"预购最划算";
            break;
        case webTypeTheme:
        {
            NSString *aString = [NSString stringWithFormat:@"Topics.html?themeId=%@&imgUrl=%@",self.themeId,self.imgUrl];
            urlString = webIp(aString);
            self.title = @"主题卖场";
        }
            break;
        case webTypeShop:
            urlString = webIp(@"store_default.html");
            urlString = [NSString stringWithFormat:@"%@?shopId=%@&userId=%@",urlString,self.newsShopId,[PMUserInfos shareUserInfo].userId];
            break;
        case webTypeJoin:
        {
            UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithTitle:@"暂不支付" style:UIBarButtonItemStylePlain target:self action:@selector(noJoinNow:)];
            self.leftBtn = leftBtn;
            self.navigationItem.leftBarButtonItem = leftBtn;
            urlString = self.urlString;
        }
            break;
        case webTypePay:
        {
            UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithTitle:@"暂不支付" style:UIBarButtonItemStylePlain target:self action:@selector(noPayNow:)];
            self.leftBtn = leftBtn;
            self.navigationItem.leftBarButtonItem = leftBtn;
            urlString = self.urlString;
        }
            break;
        case webTypeHot:
        {
            NSString *themeString = [NSString stringWithFormat:@"Brdstore.html?id=%@",self.hotUrlString];
            urlString = webIp(themeString);
            self.title = @"热卖";
        }
        default:
            break;
    }
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    [window addSubview:_indicator];
    [_indicator start];
    self.navigationItem.leftBarButtonItem.enabled = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.indicator stop];
    self.navigationItem.leftBarButtonItem.enabled = YES;
    if (self.webViewType == webTypeShop)
    {
        [self.webView stringByEvaluatingJavaScriptFromString:@"media.play()"];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *urlString = [NSString stringWithFormat:@"%@",request.URL];
    NSURL *urls = [request URL];
    NSLog(@"urls -->%@",urls);
    NSLog(@"urlString --> %@",urlString);
    SecondViewController *secondWeb = [[SecondViewController alloc] init];
    self.secondView = secondWeb;
    
    if ([urlString rangeOfString:@"CashBonus."].length > 0)
    {
        self.title = @"红包首页";
    }
    else if ([urlString rangeOfString:@"Brdiscount"].length > 0)
    {
        self.title = @"品牌折扣";
    }
    else if ([urlString rangeOfString:@"alipay"].length > 0)
    {
        self.title = @"支付宝";
        if ([urlString rangeOfString:@"forsuccess"].length >0)
        {
            DetailTradeViewController *detail = [[DetailTradeViewController alloc] init];
            detail.tradeStatus = tradeStatusWaitGet;
            detail.isFromPay = YES;
            [self.navigationController pushViewController:detail animated:YES];
            return NO;
        }
        else if ([urlString rangeOfString:@"returnPay"].length > 0)
        {
            switch (self.webViewType)
            {
                case webTypeJoin:
                {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
                    break;
                case webTypePay:
                {
                    DetailTradeViewController *detail = [[DetailTradeViewController alloc] init];
                    detail.tradeStatus = tradeStatusWaitGet;
                    detail.isFromPay = YES;
                    [self.navigationController pushViewController:detail animated:YES];
                }
                default:
                    break;
            }
            
            return NO;
        }
        else if ([urlString rangeOfString:@"asyn_payment_result"].length > 0)
        {
            self.navigationItem.leftBarButtonItem = nil;
            [self.navigationItem setHidesBackButton:YES];
            return YES;
        }
        else if ([urlString rangeOfString:@"index"].length > 0)
        {
            DetailTradeViewController *detail = [[DetailTradeViewController alloc] init];
            detail.tradeStatus = tradeStatusWaitPay;
            detail.isFromPay = YES;
            [self.navigationController pushViewController:detail animated:YES];
            return NO;
        }
    }
    else if ([urlString rangeOfString:@"Brdstore"].length > 0)
    {
        secondWeb.urlString = urlString;
        secondWeb.webTitle = @"折扣商场";
        secondWeb.webType = @"discount";
        [self.navigationController pushViewController:secondWeb animated:YES];
        
        return NO;
    }
    else if ([urlString rangeOfString:@"Topicsdeta"].length > 0 && [urlString rangeOfString:@"id="].length > 0)
    {
        secondWeb.urlString = urlString;
        secondWeb.webTitle = @"专卖详情";
        secondWeb.webType = @"theme";
        [self.navigationController pushViewController:secondWeb animated:YES];
        return NO;
    }
    else if ([urlString rangeOfString:@"Purchase."].length > 0 && [urlString rangeOfString:@"productId="].length > 0)
    {
//        PMGoodsDetailVC *goodVC = [[PMGoodsDetailVC alloc] init];
//        YBGoodDetailViewController *goodVC = [[YBGoodDetailViewController alloc] init];
        PMMyGoodsViewController *goodVC = [[PMMyGoodsViewController alloc] init];
        goodVC.isPrePay = YES;
        goodVC.isDiscount = NO;
        NSInteger index = [urlString rangeOfString:@"productId"].location + @"productId=".length;
        NSString *productId = [urlString substringFromIndex:index];
        
        if (self.clickCount == 0)
        {
            self.clickCount++; //如果是第一次进入，那么就进入webView，否则进入app中
            return YES;
        }
        else
        {
            goodVC.productId = productId;
            [self.navigationController pushViewControllerWithNavigationControllerTransition:goodVC];
            return NO;
        }
        
    }
    else if ([urlString rangeOfString:@"productId"].length > 0)
    {
        NSInteger productIndex = [urlString rangeOfString:@"productId="].location + @"productId=".length;
        NSInteger shopIndex = 0;
        if (self.isPrepay)
        {
            shopIndex = [urlString rangeOfString:@"&from="].location;
        }
        else
        {
            shopIndex = [urlString rangeOfString:@"&shopId"].location;
        }
        NSRange range = NSMakeRange(productIndex, (shopIndex - productIndex));
        NSString *newsProductId = [urlString substringWithRange:range];
        
        // 获取shopId
        NSInteger shopIdIndex = [urlString rangeOfString:@"shopId="].location + @"shopId=".length;
        NSString *shopId = [urlString substringFromIndex:shopIdIndex];
        
        PMMyGoodsViewController *goodVC = [[PMMyGoodsViewController alloc] init];
        goodVC.isPrePay = self.isPrepay;
        goodVC.productId = newsProductId;
        goodVC.shopId = shopId;
        [self.navigationController pushViewControllerWithNavigationControllerTransition:goodVC];
        
        return NO;
    }
    else if ([urlString rangeOfString:@"m_cart"].length > 0)
    {
        PMShoppingCarViewController *shoppingCar = [[PMShoppingCarViewController alloc] init];
        shoppingCar.carSeletedArr = [NSMutableArray array];
        shoppingCar.isFromDetai = YES;
        [self.navigationController pushViewControllerWithNavigationControllerTransition:shoppingCar];
        
        return NO;
    }
    else if ([urlString rangeOfString:@"m_personal"].length > 0)
    {
        MyCenterViewController *center = [[MyCenterViewController alloc] init];
        center.isFromWeb = YES;
        [self.navigationController pushViewControllerWithNavigationControllerTransition:center];
        
        return NO;
    }
    else if ([urlString hasPrefix:@"ios://"])
    {
        [self pushToRedViewController];
    }
    
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"%@",[error description]);
}

- (void)turnToRoot:(UIButton *)rightBtn
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)checkIsLogin
{
    //检查是否已经登录
    if([PMUserInfos shareUserInfo].PM_SID == nil)
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            [self.navigationController pushViewController:self.secondView animated:YES];
        };
        
        showAlertViewLogin;
        return;
    }
}

- (void)noJoinNow:(UIBarButtonItem *)btn
{
    
    SWPayCostViewController *payCostVC = [[SWPayCostViewController alloc] init];
    payCostVC.isFromJoin = YES;
    [self.navigationController pushViewController:payCostVC animated:YES];
}

- (void)noPayNow:(UIBarButtonItem *)btn
{

    DetailTradeViewController *detail = [[DetailTradeViewController alloc] init];
    detail.isFromPay = YES;
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100)
    {
        if (buttonIndex == 0)
        {

            DetailTradeViewController *detail = [[DetailTradeViewController alloc] init];
            detail.isFromPay = YES;
            [self.navigationController pushViewController:detail animated:YES];
            
        }
    }
    else
    {
        if (buttonIndex == 0)
        {
            DetailTradeViewController *detail = [[DetailTradeViewController alloc] init];
            detail.isFromPay = YES;
            detail.tradeStatus = tradeStatusWaitGet;
            [self.navigationController pushViewController:detail animated:YES];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_indicator stop];
}

- (void)pushToRedViewController
{
    YBMyRedViewController *myRedVC = [[YBMyRedViewController alloc] init];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:myRedVC];
}

@end
