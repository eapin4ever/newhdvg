//
//  YBProductCellTwo.m
//  changeViewController
//
//  Created by ZhangEapin on 15/4/28.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBProductCellTwo.h"
#import "PMMyPhoneInfo.h"

@implementation YBProductCellTwo

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.bigIV)
    {
        UIView *productBigView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 10, 228)];
        productBigView.backgroundColor = [UIColor whiteColor];
        UIView *productContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, WIDTH - 10, 228 - 5)];
        productContentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:productBigView];
        [productBigView addSubview:productContentView];
        self.backgroundColor = [UIColor clearColor];
        
        //big
        self.bigView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, productContentView.bounds.size.width * 0.4, 223)];
        self.bigView.backgroundColor = [UIColor clearColor];
        [productContentView addSubview:self.bigView];
        
        CALayer *hLine1 = [CALayer layer];
        hLine1.frame = CGRectMake(productContentView.bounds.size.width * 0.4 + 1, self.bigView.bounds.size.height * 0.2, 1, self.bigView.bounds.size.height * 0.3 - 10);
        hLine1.backgroundColor = RGBA(210, 210, 210, 0.4).CGColor;
        [productContentView.layer addSublayer:hLine1];
        
        CALayer *hLine2 = [CALayer layer];
        hLine2.frame = CGRectMake(productContentView.bounds.size.width * 0.4 + 1, self.bigView.bounds.size.height * 0.5 + 10, 1, self.bigView.bounds.size.height * 0.3 - 10);
        hLine2.backgroundColor = RGBA(210, 210, 210, 0.4).CGColor;
        [productContentView.layer addSublayer:hLine2];
        
        self.bigIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bigView.bounds.size.width, self.bigView.bounds.size.height * 0.6)];
        self.bigTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bigView.bounds.size.height * 0.6, self.bigView.bounds.size.width, self.bigView.bounds.size.height * 0.4)];
        self.bigIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.bigView addSubview:self.bigIV];
        [self.bigView addSubview:self.bigTitleView];
        
        self.bigTitle = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), 0, self.bigTitleView.bounds.size.width - WidthRate(20), self.bigTitleView.bounds.size.height * 0.5)];
        self.bigTitle.numberOfLines = 2;
        self.bigTitle.font = [UIFont systemFontOfSize:11.0f];
        self.bigTitle.textColor = HDVGFontColor;
        [self.bigTitleView addSubview:self.bigTitle];
        
        self.bigRawPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), self.bigTitleView.bounds.size.height * 0.6, self.bigTitleView.bounds.size.width, self.bigTitleView.bounds.size.height * 0.1)];
        self.bigRawPriceLB.font = [UIFont systemFontOfSize:10.0f];
        self.bigRawPriceLB.textAlignment = NSTextAlignmentLeft;
        self.bigRawPriceLB.textColor = [UIColor lightGrayColor];
        [self.bigTitleView addSubview:self.bigRawPriceLB];
        
        self.bigLine = [CALayer layer];
        self.bigLine.backgroundColor = [UIColor lightGrayColor].CGColor;
        [self.bigRawPriceLB.layer addSublayer:self.bigLine];
        
        self.bigPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), self.bigTitleView.bounds.size.height * 0.75, self.bigTitleView.bounds.size.width - WidthRate(20), self.bigTitleView.bounds.size.height * 0.2)];
        self.bigPriceLB.font = [UIFont systemFontOfSize:14.0f];
        self.bigPriceLB.textColor = HDVGRed;
        [self.bigTitleView addSubview:self.bigPriceLB];
        
        self.bigZKBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.bigZKBtn.backgroundColor = RGBA(201, 81, 90, 1);
        self.bigZKBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
        self.bigZKBtn.frame = CGRectMake(WidthRate(10), self.bigTitleView.bounds.size.height * 0.6, self.bigTitleView.bounds.size.width * 0.3, self.bigTitleView.bounds.size.height * 0.15);
        [self.bigTitleView addSubview:self.bigZKBtn];
        
        
        //smallOne
        self.smallOneView = [[UIView alloc] initWithFrame:CGRectMake(productContentView.bounds.size.width * 0.4, 0, productContentView.bounds.size.width * 0.6, productContentView.bounds.size.height / 2 - 2)];
        [productContentView addSubview:self.smallOneView];
        self.smallOneIV = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, self.smallOneView.bounds.size.width / 2 - 3, self.smallOneView.bounds.size.height - 6)];
        self.smallOneIV.contentMode = UIViewContentModeScaleAspectFit;
        self.smallOneTitleView = [[UIView alloc] initWithFrame:CGRectMake(self.smallOneView.bounds.size.width / 2, 0, self.smallOneView.bounds.size.width / 2, 100)];
        [self.smallOneView addSubview:self.smallOneTitleView];
        
        CALayer *vLine = [CALayer layer];
        vLine.backgroundColor = RGBA(210, 210, 210, 0.4).CGColor;
        vLine.frame = CGRectMake(self.bigView.bounds.size.width + 20, 110, 1, 1);
        self.vLine = vLine;
        [productContentView.layer addSublayer:vLine];
        
        self.smallOneTitle = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), 0, self.smallOneTitleView.bounds.size.width - WidthRate(20), self.smallOneTitleView.bounds.size.height * 0.5)];
        self.smallOneTitle.numberOfLines = 2;
        self.smallOneTitle.font = [UIFont systemFontOfSize:11.0f];
        self.smallOneTitle.textColor = HDVGFontColor;
        [self.smallOneTitleView addSubview:self.smallOneTitle];
        [self.smallOneView addSubview:self.smallOneIV];
        
        self.smallOneRawPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(0, self.smallOneTitleView.bounds.size.height * 0.75, self.smallOneTitleView.bounds.size.width * 0.85, self.smallOneTitleView.bounds.size.height * 0.1)];
        self.smallOneRawPriceLB.font = [UIFont systemFontOfSize:10.0f];
        self.smallOneRawPriceLB.textAlignment = NSTextAlignmentRight;
        self.smallOneRawPriceLB.textColor = [UIColor lightGrayColor];
        [self.smallOneTitleView addSubview:self.smallOneRawPriceLB];
        
        self.smallOneLine = [CALayer layer];
        self.smallOneLine.backgroundColor = [UIColor lightGrayColor].CGColor;
        [self.smallOneRawPriceLB.layer addSublayer:self.smallOneLine];
        
        self.smallOnePriceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), self.smallOneTitleView.bounds.size.height * 0.9, self.smallOneTitleView.bounds.size.width - WidthRate(20), self.smallOneTitleView.bounds.size.height * 0.1 + 2)];
        self.smallOnePriceLB.textAlignment = NSTextAlignmentRight;
        self.smallOnePriceLB.font = [UIFont systemFontOfSize:14.0f];
        self.smallOnePriceLB.textColor = HDVGRed;
        [self.smallOneTitleView addSubview:self.smallOnePriceLB];
        
        self.smallOneZKBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.smallOneZKBtn.backgroundColor = RGBA(201, 81, 90, 1);
        self.smallOneZKBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
        self.smallOneZKBtn.frame = CGRectMake(self.smallOneTitleView.bounds.size.width * 0.7, self.smallOneTitleView.bounds.size.height * 7, self.smallOneTitleView.bounds.size.width * 0.3, self.smallOneTitleView.bounds.size.height * 0.1);
        [self.smallOneTitleView addSubview:self.smallOneZKBtn];
        
        //smallTwo
        self.smallTwoView = [[UIView alloc] initWithFrame:CGRectMake(productContentView.bounds.size.width * 0.4, productContentView.bounds.size.height / 2 + 2, productContentView.bounds.size.width * 0.6, productContentView.bounds.size.height / 2 - 2)];
        [productContentView addSubview:self.smallTwoView];
        
        self.smallTwoIV = [[UIImageView alloc] initWithFrame:CGRectMake(self.smallTwoView.bounds.size.width / 2 + 3, 3, self.smallTwoView.bounds.size.width / 2 - 6, self.smallTwoView.bounds.size.height - 6)];
        self.smallTwoIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.smallTwoView addSubview:self.smallTwoIV];
        self.smallTwoTitleView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, self.smallTwoView.bounds.size.width / 2, 100)];
        [self.smallTwoView addSubview:self.smallTwoTitleView];
        self.smallTwoTitle = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), 0, self.smallTwoTitleView.bounds.size.width - WidthRate(20), self.smallTwoTitleView.bounds.size.height * 0.5)];
        self.smallTwoTitle.numberOfLines = 2;
        self.smallTwoTitle.font = [UIFont systemFontOfSize:11.0f];
        self.smallTwoTitle.textColor = HDVGFontColor;
        [self.smallTwoTitleView addSubview:self.smallTwoTitle];
        
        self.smallTwoRawPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), self.smallTwoTitleView.bounds.size.height * 0.7, self.smallTwoTitleView.bounds.size.width, self.smallTwoTitleView.bounds.size.height * 0.1)];
        self.smallTwoRawPriceLB.font = [UIFont systemFontOfSize:10.0f];
        self.smallTwoRawPriceLB.textAlignment = NSTextAlignmentLeft;
        self.smallTwoRawPriceLB.textColor = [UIColor lightGrayColor];
        [self.smallTwoTitleView addSubview:self.smallTwoRawPriceLB];
        
        self.smallTwoLine = [CALayer layer];
        self.smallTwoLine.backgroundColor = [UIColor lightGrayColor].CGColor;
        [self.smallTwoRawPriceLB.layer addSublayer:self.smallTwoLine];
        
        self.smallTwoPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), self.smallTwoTitleView.bounds.size.height * 0.8, self.smallTwoTitleView.bounds.size.width - WidthRate(20), self.smallTwoTitleView.bounds.size.height * 0.2)];
        self.smallTwoPriceLB.font = [UIFont systemFontOfSize:14.0f];
        self.smallTwoPriceLB.textColor = HDVGRed;
        [self.smallTwoTitleView addSubview:self.smallTwoPriceLB];
        
        self.smallTwoZKBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.smallTwoZKBtn.backgroundColor = RGBA(201, 81, 90, 1);
        self.smallTwoZKBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
        self.smallTwoZKBtn.frame = CGRectMake(WidthRate(10), self.smallTwoTitleView.bounds.size.height * 0.7, self.smallTwoTitleView.bounds.size.width * 0.3, self.smallTwoTitleView.bounds.size.height * 0.1);
        [self.smallTwoTitleView addSubview:self.smallTwoZKBtn];
        
        self.btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn1.frame = CGRectMake(0, 0, self.bigView.bounds.size.width, self.bigView.bounds.size.height);
        
        self.btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn2.frame = CGRectMake(0, 0, self.smallOneView.bounds.size.width, self.smallOneView.bounds.size.height);
        
        self.btn3 = [UIButton buttonWithType:UIButtonTypeCustom];self.btn2.frame = CGRectMake(0, 0, self.smallOneView.bounds.size.width, self.smallOneView.bounds.size.height);
        self.btn3.frame = CGRectMake(0, 0, self.smallTwoView.bounds.size.width, self.smallTwoView.bounds.size.height);
        
        [self.bigView addSubview:self.btn1];
        [self.smallOneView addSubview:self.btn2];
        [self.smallTwoView addSubview:self.btn3];
    }
    
    if (self.isBigZK)
    {
        self.bigZKBtn.hidden = NO;
        self.bigRawPriceLB.hidden = YES;
    }
    else
    {
        self.bigZKBtn.hidden = YES;
        self.bigRawPriceLB.hidden = NO;
    }
    
    if (self.isSmallOneZK)
    {
        self.smallOneZKBtn.hidden = NO;
        self.smallOneRawPriceLB.hidden = YES;
    }
    else
    {
        self.smallOneZKBtn.hidden = YES;
        self.smallOneRawPriceLB.hidden = NO;
    }
    
    if (self.isSmallTwoZK)
    {
        self.smallTwoZKBtn.hidden = NO;
        self.smallTwoRawPriceLB.hidden = YES;
    }
    else
    {
        self.smallTwoZKBtn.hidden = YES;
        self.smallTwoRawPriceLB.hidden = NO;
    }
    
}

- (void)setProductOnetitle1:(NSString *)title1 price1:(double)price1 title2:(NSString *)title2 price2:(double)price2 title3:(NSString *)title3 price3:(double)price3 rawPrice1:(double)rawPrice1 rawPrice2:(double)rawPrice2 rawPrice3:(double)rawPrice3 zk1:(NSString *)zk1 zk2:(NSString *)zk2 zk3:(NSString *)zk3
{
    self.bigTitle.text = title1;
    self.smallOneTitle.text = title2;
    self.smallTwoTitle.text = title3;
    
    NSMutableAttributedString *noteStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.1lf",price1]];
    [noteStr1 addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} range:NSMakeRange(0, 1)];
    self.bigPriceLB.attributedText = noteStr1;
    
    NSMutableAttributedString *noteStr2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.1lf",price2]];
    [noteStr1 addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]} range:NSMakeRange(0, 1)];
    self.smallOnePriceLB.attributedText = noteStr2;
    
    NSMutableAttributedString *noteStr3 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.1lf",price3]];
    [noteStr1 addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]} range:NSMakeRange(0, 1)];
    self.smallTwoPriceLB.attributedText = noteStr3;
    
    NSString *bigRaw = [NSString stringWithFormat:@"￥%.1lf",rawPrice1];
    //    CGSize bigRawSize = [bigRaw sizeWithFont:[UIFont systemFontOfSize:10.0f]];
    CGSize bigRawSize = [bigRaw sizeWithFont:[UIFont systemFontOfSize:10.0f] constrainedToSize:CGSizeMake(MAXFLOAT, self.bigTitleView.bounds.size.height * 0.1)];
    self.bigRawPriceLB.frame = (CGRect){{self.bigRawPriceLB.frame.origin.x,self.bigRawPriceLB.frame.origin.y},{bigRawSize.width,self.bigTitleView.bounds.size.height * 0.1}};
    self.bigRawPriceLB.text = bigRaw;
    
    self.bigLine.frame = CGRectMake(WidthRate(10), self.bigRawPriceLB.bounds.size.height / 2, bigRawSize.width - 5, 0.5);
    
    NSString *smallOneRaw = [NSString stringWithFormat:@"￥%.1lf",rawPrice2];
    //    CGSize smallOneRawSize = [smallOneRaw sizeWithFont:[UIFont systemFontOfSize:10.0f]];
    CGSize smallOneRawSize = [smallOneRaw sizeWithFont:[UIFont systemFontOfSize:10.0f] constrainedToSize:CGSizeMake(MAXFLOAT, self.smallOneTitleView.bounds.size.height * 0.1)];
    self.smallOneRawPriceLB.frame = (CGRect){{self.smallOneTitleView.bounds.size.width - smallOneRawSize.width - WidthRate(20),self.smallOneRawPriceLB.frame.origin.y},{smallOneRawSize.width,self.smallOneTitleView.bounds.size.height * 0.1}};
    self.smallOneRawPriceLB.text = smallOneRaw;
    
    self.smallOneLine.frame = CGRectMake(WidthRate(10), self.smallOneRawPriceLB.bounds.size.height / 2, smallOneRawSize.width - 5, 0.5);
    
    NSString *smallTowRaw = [NSString stringWithFormat:@"￥%.1lf",rawPrice3];
    //    CGSize smallTowRawSize = [smallTowRaw sizeWithFont:[UIFont systemFontOfSize:10.0f]];
    CGSize smallTowRawSize = [smallTowRaw sizeWithFont:[UIFont systemFontOfSize:10.0f] constrainedToSize:CGSizeMake(MAXFLOAT, self.smallTwoTitleView.bounds.size.width * 0.1)];
    self.smallTwoRawPriceLB.frame = (CGRect){{self.smallTwoRawPriceLB.frame.origin.x,self.smallTwoRawPriceLB.frame.origin.y},{smallTowRawSize.width,smallTowRawSize.height}};
    self.smallTwoRawPriceLB.text = smallTowRaw;
    
    self.smallTwoLine.frame = CGRectMake(WidthRate(10), self.smallTwoRawPriceLB.bounds.size.height / 2, smallTowRawSize.width - 5, 0.5);
    
    NSString *smallPrice = [NSString stringWithFormat:@"￥%.1lf",price2];
    CGSize smallPriceSize = [smallPrice sizeWithFont:[UIFont systemFontOfSize:14.0]];
    self.vLine.frame = CGRectMake(self.vLine.frame.origin.x, self.vLine.frame.origin.y, self.smallOneView.bounds.size.width * 0.9 - smallPriceSize.width, 1);
    
    NSString *zkString1 = [NSString stringWithFormat:@"%@折",zk1];
    NSString *zkString2 = [NSString stringWithFormat:@"%@折",zk2];
    NSString *zkString3 = [NSString stringWithFormat:@"%@折",zk3];
    
    [self.bigZKBtn setTitle:zkString1 forState:UIControlStateNormal];
    [self.smallOneZKBtn setTitle:zkString2 forState:UIControlStateNormal];
    [self.smallTwoZKBtn setTitle:zkString3 forState:UIControlStateNormal];
    
}

- (void)addTarget:(id)target selector:(SEL)sel row:(NSInteger)row
{
    _btn1.tag = row*100 + 0;
    _btn2.tag = row*100 + 1;
    _btn3.tag = row*100 + 2;
    
    [_btn1 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [_btn2 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [_btn3 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    
}

@end
