//
//  PMDistubitionStoreViewController.h
//  changeViewController
//
//  Created by ZhangEapin on 15/7/20.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMProductStoreCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AFNetworking/AFNetworking.h>

@interface PMDistubitionStoreViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *firstClassIdArray;
@property (strong, nonatomic) NSMutableArray *secondClassIdArray;
@property (strong, nonatomic) NSMutableArray *thirdClassIdArray;
@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;

//@property (weak, nonatomic) id<PMStoreScreenDelegate> screenDelegate;


@end
