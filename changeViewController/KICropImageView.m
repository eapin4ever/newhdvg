//
//  KICropImageView.m
//  QiuBai
//
//  Created by Archmage on 14-4-16.
//  Copyright (c) 2014年 Less Everything. All rights reserved.
//

#import "KICropImageView.h"
#import "UIView+YYExtension.h"
#import "PMMyPhoneInfo.h"

#if !__has_feature(objc_arc)
#error "enable ARC by add -fobjc-arc"
#endif

@implementation KICropImageView

- (id)initWithFrame:(CGRect)frame AndIsHeader:(BOOL)isHeader
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _scrollView = [[UIScrollView alloc] init];
        [_scrollView setDelegate:self];
        [_scrollView setBounces:YES];
        [_scrollView setShowsHorizontalScrollIndicator:NO];
        [_scrollView setShowsVerticalScrollIndicator:NO];
        _scrollView.alwaysBounceVertical = YES;
        _scrollView.alwaysBounceHorizontal = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        [self addSubview:_scrollView];
        
        _imageView = [[UIImageView alloc] init];
        [_scrollView addSubview:_imageView];
        
        if (isHeader)
        {
            _maskView = [[KICropImageMaskView alloc] initWithHeader];
        }
        else
        {
            _maskView = [[KICropImageMaskView alloc] init];
        }
        [_maskView setBackgroundColor:[UIColor clearColor]];
        [_maskView setUserInteractionEnabled:NO];
        [self addSubview:_maskView];
        [self bringSubviewToFront:_maskView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _scrollView = [[UIScrollView alloc] init];
        [_scrollView setDelegate:self];
        [_scrollView setBounces:YES];
        [_scrollView setShowsHorizontalScrollIndicator:NO];
        [_scrollView setShowsVerticalScrollIndicator:NO];
        _scrollView.alwaysBounceVertical = YES;
        _scrollView.alwaysBounceHorizontal = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        [self addSubview:_scrollView];
        
        _imageView = [[UIImageView alloc] init];
        [_scrollView addSubview:_imageView];
        
        _maskView = [[KICropImageMaskView alloc] init];
        [_maskView setBackgroundColor:[UIColor clearColor]];
        [_maskView setUserInteractionEnabled:NO];
        [self addSubview:_maskView];
        [self bringSubviewToFront:_maskView];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    _scrollView.frame = frame;
    _maskView.frame = frame;
    if (CGSizeEqualToSize(_cropSize, CGSizeZero))
    {
        [self setCropSize:CGSizeMake(50, 50)];
    }
}

- (void)setCoverImg:(UIImage *)coverImg
{
    _coverImg = coverImg;
    _maskView.coverImg = coverImg;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    [_imageView setImage:_image];
    self.backgroundColor = [UIColor blackColor];
    
    [self updateZoomScale];
}

// 设置scrollview的数值
- (void)updateZoomScale
{
    // 根据图片设置imageview的frame
    CGFloat scale = [[UIScreen mainScreen] scale];
    CGFloat width = _image.size.width / scale;
    CGFloat height = _image.size.height / scale;
    
    _imageView.frame = CGRectMake(0, 0, width, height);
    
    // 算出合适的缩放比例
    CGFloat xScale = _cropSize.width / width;
    CGFloat yScale = _cropSize.height / height;
    
    CGFloat min = MAX(xScale, yScale);
    CGFloat max = 1.0;
    
    if (min > max)
    {
        max = min;
    }
    
    // 设置缩放比例，以及缩放比例的最小值和最大值
    _scrollView.minimumZoomScale = min;
    _scrollView.maximumZoomScale = max + 5.0f;
    _scrollView.zoomScale = min;
    
    // 设置使图片居中显示
    if (yScale > xScale)
    {
        // 横向居中
        _scrollView.contentOffset = CGPointMake((width * min - self.frame.size.width) / 2,
                                                -_maskView.cropRect.origin.y);
    }
    else if (xScale > yScale)
    {
        // 纵向居中
        _scrollView.contentOffset = CGPointMake(0,
                                                (height * min - self.frame.size.height) / 2);
    }
}

// 设置裁剪区域的大小
- (void)setCropSize:(CGSize)size
{
    _cropSize = size;
    
    CGFloat width = _cropSize.width;
    CGFloat height = _cropSize.height;
    
    CGRect realBounds = self.bounds;
    CGFloat x = (CGRectGetWidth(realBounds) - width) / 2;
    CGFloat y = (CGRectGetHeight(realBounds) - height - 45) / 2;

    // 设置可见的裁剪区域的大小和位置，位置通过计算居中
    [_maskView setCropSize:_cropSize];
    
    CGFloat top = y;
    CGFloat left = x;
    CGFloat right = CGRectGetWidth(realBounds)- width - x;
    CGFloat bottom = CGRectGetHeight(realBounds)- height - y;
    
    _imageInset = UIEdgeInsetsMake(top, left, bottom, right);
    _scrollView.contentInset = _imageInset;

}

// 裁剪图片
- (UIImage *)cropImage
{
    CGFloat zoomScale = _scrollView.zoomScale;
    CGFloat offsetX = _scrollView.contentOffset.x;
    CGFloat offsetY = _scrollView.contentOffset.y;
    CGFloat aX = offsetX>=0 ? offsetX+_imageInset.left : (_imageInset.left - ABS(offsetX));
    CGFloat aY = offsetY>=0 ? offsetY+_imageInset.top : (_imageInset.top - ABS(offsetY));
    
    // 先通过缩放的比例还原出屏幕上相对于图片的坐标
    aX = aX / zoomScale;
    aY = aY / zoomScale;
    CGFloat aWidth =  _cropSize.width / zoomScale;
    CGFloat aHeight = _cropSize.height / zoomScale;
    
    // 通过屏幕分辨率来还原可见区域相对于图片的实际坐标,并裁剪
    int scale = [[UIScreen mainScreen] scale];
    UIImage *image = [_image cropImageWithX:aX * scale y:aY * scale width:aWidth * scale height:aHeight * scale];
    
    return image;
}

#pragma UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}

- (void)dealloc
{
}
@end

#pragma KISnipImageMaskView

#define kMaskViewBorderWidth 1.0f
@implementation KICropImageMaskView
{
@private
    CGRect  _cropRect;
}
@synthesize cropRect = _cropRect;

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (id) initWithHeader
{
    self = [super init];
    if (self)
    {
        self.cirleOpacityView = [[CircleOpacity alloc] init];
        [self addSubview:_cirleOpacityView];
    }
    
    return self;
}


// 计算启示坐标，居中
- (void)setCropSize:(CGSize)size
{
    CGRect realBounds = self.bounds;
    CGFloat x = 0;
    CGFloat y = (CGRectGetHeight(realBounds) - WIDTH / 3 - 45) / 2;
    _cropRect = CGRectMake(x, y , WIDTH, WIDTH / 2);
//    self.cirleOpacityView.frame = _cropRect;
//    [self.cirleOpacityView.layer setCornerRadius:self.cirleOpacityView.yy_width * 0.5];
//    [self.cirleOpacityView setNeedsDisplay];
    // 绘制上层看到的裁剪页面
    [self setNeedsDisplay];
}

- (CGSize)cropSize
{
    return _cropRect.size;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(ctx, 0, 0, 0, 0.7);
    CGContextFillRect(ctx, self.bounds);
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor redColor].CGColor);
    CGContextStrokeRectWithWidth(ctx, _cropRect, kMaskViewBorderWidth);
    
    CGContextClearRect(ctx, _cropRect);
}

@end

@implementation CircleOpacity

- (id)init
{
    self = [super init];
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = YES;
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGFloat lengths[] = {4, 2};
//    CGContextSetRGBStrokeColor(context,1,1,1,0.8);//画笔线的颜色
//    CGContextSetLineWidth(context, 0.5);
    
    // 首先，圆的边框是白色的线条
    CGContextSetRGBStrokeColor(context,1,1,1,0.0);//画笔线的颜色
    CGContextSetLineWidth(context, 0.0);//线的宽度;
    CGContextAddPath(context, CGPathCreateWithEllipseInRect(rect, NULL));
    CGContextDrawPath(context, kCGPathStroke);
    
//    // 横竖各两条线，虚线
//    int padding = self.yy_height / 3;
//    // 然后就是里面横着两根，竖着两根的虚线，分别把宽和高分为三份，一共四条线
//    // 竖着的两根，
//    CGPoint aPoints[2];//坐标点
//    aPoints[0] = CGPointMake(padding, 0);//坐标1
//    aPoints[1] = CGPointMake(padding, self.yy_height);//坐标2
//    CGContextAddLines(context, aPoints, 2);//添加线
//    CGContextSetLineDash(context, 3, lengths, 2);
//    CGContextDrawPath(context, kCGPathStroke); //根据坐标绘制路径
//
//    CGPoint bPoints[2];//坐标点
//    bPoints[0] = CGPointMake(2 * padding, 0);//坐标2
//    bPoints[1] = CGPointMake(2 * padding, self.yy_height);//坐标2
//    CGContextAddLines(context, bPoints, 2);//添加线
//    CGContextSetLineDash(context, 3, lengths, 2);
//    CGContextDrawPath(context, kCGPathStroke); //根据坐标绘制路径
//
//    // 横着的两根
//    CGPoint cPoints[2];//坐标点
//    cPoints[0] = CGPointMake(0, padding);//坐标1
//    cPoints[1] = CGPointMake(self.yy_width, padding);//坐标2
//    CGContextAddLines(context, cPoints, 2);//添加线
//    CGContextSetLineDash(context, 3, lengths, 2);
//    CGContextDrawPath(context, kCGPathStroke); //根据坐标绘制路径
//
//    CGPoint dPoints[2];//坐标点
//    dPoints[0] = CGPointMake(0, 2 * padding);//坐标1
//    dPoints[1] = CGPointMake(self.yy_width, 2 * padding);//坐标2
//    CGContextAddLines(context, dPoints, 2);//添加线
//    CGContextSetLineDash(context, 3, lengths, 2);
//    CGContextDrawPath(context, kCGPathStroke); //根据坐标绘制路径
}

@end
