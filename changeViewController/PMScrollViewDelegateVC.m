//
//  PMScrollViewDelegateVC.m
//  changeViewController
//
//  Created by pmit on 15/1/8.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMScrollViewDelegateVC.h"
@implementation PMScrollViewDelegateVC
//scrollView 滚动时调用的方法 在这里使用是为了防止tableView调用导致不明崩溃
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.tag != 101)
        [self.pageControl setCurrentPage: scrollView.contentOffset.x / scrollView.bounds.size.width];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.viewForZoom;
}

@end
