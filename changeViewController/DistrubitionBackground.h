//
//  DistrubitionBackground.h
//  changeViewController
//
//  Created by pmit on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DistrubitionBackgroundDelegate <NSObject>

- (void)changeBgPhoto:(NSString *)urlString AndData:(NSData *)bgData;

@end

@interface DistrubitionBackground : UIViewController

@property (strong,nonatomic) NSArray *backgroundIVArr;
@property (copy,nonatomic) NSString *shopId;
@property (weak,nonatomic) id<DistrubitionBackgroundDelegate> bgDelegate;
@property (strong,nonatomic) NSDictionary *shopDic;
@property (copy,nonatomic) NSString *bgUrlString;


@end
