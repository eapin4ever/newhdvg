//
//  PMMyPhoneInfo.h
//  changeViewController
//
//  Created by pmit on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//
#import "PMNetworking.h"
#import "PMUserInfos.h"
#import "NSDictionary+PMDictionaryCategory.h"
#import "UINavigationController+TRVSNavigationControllerTransition.h"
#import "NSDictionary+PMDictionaryCategory.h"

#ifndef changeViewController_PMMyPhoneInfo_h
#define changeViewController_PMMyPhoneInfo_h

#define SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion]floatValue]
#define SCREEN_BOUNDS [[UIScreen mainScreen] bounds]
#define WIDTH SCREEN_BOUNDS.size.width
#define HEIGHT SCREEN_BOUNDS.size.height

#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]   // RGBA 颜色值、透明度
#define RGB 255.0
//导航栏灰色
#define NAVGray RGBA(231, 231, 231, 1)
//导航栏上的文字颜色
#define NAVTEXTCOLOR RGBA(160,160,160,1)

#define HDVGRed [UIColor colorWithRed:189/RGB green:24/RGB blue:41/RGB alpha:1]
//页面背景色
#define HDVGPageBGGray [UIColor colorWithRed:247/RGB green:247/RGB blue:247/RGB alpha:1]
//商品标题字体颜色、收货地址详情字体颜色
#define HDVGFontColor [UIColor colorWithRed:75/RGB green:75/RGB blue:75/RGB alpha:1]
//商品分类字体颜色
#define GoodsClassifyColor [UIColor colorWithRed:142/RGB green:141/RGB blue:146/RGB alpha:1]
//长按钮背景色和字体
#define ButtonBgColor [UIColor colorWithRed:218/RGB green:67/RGB blue:80/RGB alpha:1]
#define ButtonFont 17.0f

#define TabBarColor RGBA(63, 63, 63, 1)

#define ButtonBgYellow RGBA(230, 164, 62, 1)

#define tabBarSelectedColor RGBA(188, 29, 37, 1)

#define kTextColor RGBA(51,51,51,1)
#define kShadeColor RGBA(255,255,255,0.7)

// 判断iPhone4s的3.5英寸屏幕
#define iPhone4s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

// 判断iPhone6 的4.7英寸屏幕
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
// 判断iPhone6 的4.7英寸屏幕
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
// 判断iPhone6 Plus 的5.5英寸屏幕
#define iPhone6_Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

//tableView 的高度
#define TableViewHeight 44

//UI设计是按照iphone6设计的，这个是一个比率
#define UIChangeRate WIDTH/750
#define WidthRate(a) WIDTH/750*a
#define HeightRate(a) HEIGHT/1334*a

#define showAlertViewNotOpen UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"该功能尚未开放，敬请期待" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];[av show];

#define PMSID @"PM_SID":[PMUserInfos shareUserInfo].PM_SID

#define intSuccess [[dic objectNullForKey:@"success"] integerValue]

#define showRequestFailAlertView UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dic objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]; [av show];

#define showAlertViewLogin [[MasterViewController defaultMasterVC] transitionToLoginVC];


#define isNull(obj) ([obj isKindOfClass:[NSNull class]] || obj == nil)

#define PREPAY_STATE @"prepayState"

#ifdef DEBUG
#define DLog(...) NSLog(__VA_ARGS__)
#else
#define DLog(...)
#endif
#define ALog(...) NSLog(__VA_ARGS__)

typedef void(^myFinishBlock)(void);

#endif
