//
//  PMDistributionsShopItems.h
//  changeViewController
//
//  Created by pmit on 14/11/27.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDistributionsShopItems : UIButton
- (instancetype)initWithFrame:(CGRect)frame imageName:(NSString *)imageName title:(NSString *)myTitle;

@end
