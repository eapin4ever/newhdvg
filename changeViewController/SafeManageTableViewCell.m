//
//  SafeManageTableViewCell.m
//  changeViewController
//
//  Created by P&M on 14/12/12.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "SafeManageTableViewCell.h"

@implementation SafeManageTableViewCell
{
    UILabel *titleLabel;
    
    UILabel *nameLabel;
    UILabel *mobileLabel;
    UILabel *addressLabel;
}

- (void)setMyAccountCellUI
{
    if (!_imaView) {
        
        // 图片
        _imaView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 12, 20, 20)];
        [self.contentView addSubview:_imaView];
        
        // 标题
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 7, 70, 30)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = HDVGFontColor;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:titleLabel];
        
        // 告诉用户还有下一个页面
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        // 去掉cell被选中时的颜色
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

// 设置我的账户图片、标题
- (void)setImage:(UIImage *)image setTitle:(NSString *)title
{
    _imaView.image = image;
    titleLabel.text = title;
}

- (void)setNewAddressDataUI
{
    if (!nameLabel) {
        
        UIView *myContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 90)];
        myContentView.backgroundColor = [UIColor whiteColor];
        
        // 分隔线
        UIImageView *lineImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 4)];
        lineImage.image = [UIImage imageNamed:@"dottedLine_image.png"];
        [myContentView addSubview:lineImage];
        
        UIImageView *lineImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 86, WIDTH, 4)];
        lineImage2.image = [UIImage imageNamed:@"dottedLine_image.png"];
        [myContentView addSubview:lineImage2];
        
        // 头像
        UIImageView *avatarImage = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(40), 23.5, 15, 15)];
        avatarImage.contentMode = UIViewContentModeScaleAspectFit;
        avatarImage.image = [UIImage imageNamed:@"address_name.png"];
        [myContentView addSubview:avatarImage];
        
        // 电话图片
        UIImageView *phoneImage = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(300), 23.5, 15, 15)];
        phoneImage.contentMode = UIViewContentModeScaleAspectFit;
        phoneImage.image = [UIImage imageNamed:@"address_phone.png"];
        [myContentView addSubview:phoneImage];
        
        
        // 姓名
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(77), 21, WidthRate(180), 20)];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textColor = HDVGFontColor;
        nameLabel.textAlignment = NSTextAlignmentLeft;
        nameLabel.font = [UIFont systemFontOfSize:14];
        [myContentView addSubview:nameLabel];
        
        // 电话
        mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(335), 21, WidthRate(250), 20)];
        mobileLabel.backgroundColor = [UIColor clearColor];
        mobileLabel.textColor = HDVGFontColor;
        mobileLabel.textAlignment = NSTextAlignmentLeft;
        mobileLabel.font = [UIFont systemFontOfSize:14];
        [myContentView addSubview:mobileLabel];
    
        // 地址
        addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), 46, WIDTH - WidthRate(90), 40)];
        addressLabel.backgroundColor = [UIColor clearColor];
        addressLabel.textColor = HDVGFontColor;
        addressLabel.numberOfLines = 0;
        addressLabel.textAlignment = NSTextAlignmentLeft;
        addressLabel.font = [UIFont systemFontOfSize:14];
        [myContentView addSubview:addressLabel];
        
        
        UIImageView *defaultMark = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 45, 0, 45, 45)];
        self.defaultMark = defaultMark;
        defaultMark.image = [UIImage imageNamed:@"defaultAddress"];
        [myContentView addSubview:defaultMark];
        defaultMark.hidden = YES;
        [self.contentView addSubview:myContentView];
    }
}

// 设置新建地址的姓名、电话、地址数据
- (void)setName:(NSString *)name setMobile:(NSString *)mobile setAddress:(NSString *)address
{
    nameLabel.text = name;
    mobileLabel.text = mobile;
    addressLabel.text = address;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
