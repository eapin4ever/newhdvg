//
//  YBMessageCell.m
//  changeViewController
//
//  Created by pmit on 15/6/23.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBMessageCell.h"
#import "PMMyPhoneInfo.h"

@implementation YBMessageCell
{
    UILabel *_titleLB;
    UILabel *_contentLB;
    UILabel *_timeLB;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!_titleLB) {
        _titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 10, WIDTH * 0.5, 20)];
        _titleLB.font = [UIFont systemFontOfSize:16.0f];
        _titleLB.textColor = HDVGFontColor;
        [self.contentView addSubview:_titleLB];
        
        
        _contentLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 35, WIDTH - WidthRate(72), 40)];
        _contentLB.font = [UIFont systemFontOfSize:15.0f];
        _contentLB.textColor = [UIColor lightGrayColor];
        _contentLB.numberOfLines = 2;
        [self.contentView addSubview:_contentLB];
        
        _timeLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(286), 10, WidthRate(250), 20)];
        _timeLB.backgroundColor = [UIColor clearColor];
        _timeLB.font = [UIFont systemFontOfSize:14.0f];
        _timeLB.textColor = HDVGFontColor;
        _timeLB.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_timeLB];
        
    }
}

- (void)setMessageDic:(NSDictionary *)messageDic
{
    _messageDic = messageDic;
    _titleLB.text = [self.messageDic objectNullForKey:@"title"];
    _contentLB.text = [self.messageDic objectNullForKey:@"content"];
    _timeLB.text = [self.messageDic objectNullForKey:@"time"];
}

@end
