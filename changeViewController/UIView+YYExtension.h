//
//  UIView+YYExtension.h
//  Yeah
//
//  Created by KayWong on 15/5/24.
//  Copyright (c) 2015年 QiuShiBaiKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (YYExtension)
@property (nonatomic, assign) CGFloat yy_height;
@property (nonatomic, assign) CGFloat yy_width;

@property (nonatomic, assign) CGFloat yy_y;
@property (nonatomic, assign) CGFloat yy_x;

- (void)drawBorderWithWidth:(CGFloat)width;
- (void)drawBorderWithWidth:(CGFloat)width color:(UIColor *)color;

@end
