//
//  PMProductBtn.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define TITLEHI 30
#define PRICEHI 20

#import "PMProductBtn.h"

@implementation PMProductBtn
{
    CGFloat _imageX;
    CGFloat _imageY;
    CGFloat _imageWidth;
    CGFloat _imageHeight;
    
    
    CGFloat _titleX;
    CGFloat _titleY;
    CGFloat _titleWidth;
    CGFloat _titleHeight;
    
    
    CGFloat _priceX;
    CGFloat _priceY;
    CGFloat _priceWidth;
    CGFloat _priceHeight;
}


- (PMProductBtn *)initWithStyle:(PMGridStyle)style frame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setMyFrameWithStyle:style];
        [self createUI];
    }
    return self;
}


- (void)setMyFrameWithStyle:(PMGridStyle)style
{
    switch (style)
    {
        case PMGridStyleBig:
            _imageWidth = _imageHeight = iPhone6_Plus || iPhone6 ? 140 : 135;
            _imageX = (self.bounds.size.width - _imageWidth)/2.0;
            _imageY = 56/2.0 - 10;
            
            _titleWidth = WidthRate(320);
            _titleHeight = 58/2;
            _titleX = (self.bounds.size.width - _titleWidth)/2.0;
            _titleY = 316/2;
            
            _priceHeight = 15;
            _priceWidth = _titleWidth;
            _priceX = _titleX;
            _priceY = _titleY + _titleHeight;
            
            break;
            
        case PMGridStyleSmallImageOnLeft:
            _imageWidth = _imageHeight = iPhone6_Plus || iPhone6 ? 75 : 65;
            _imageX = WidthRate(16);
            _imageY = (self.bounds.size.height - _imageHeight)/2;
            
            _titleHeight = TITLEHI;
            _titleWidth = WidthRate(180);
            _titleX = WidthRate(170);
            _titleY = _imageY + 10;
            
            _priceHeight = PRICEHI;
            _priceWidth = _titleWidth;
            _priceX = _titleX;
            _priceY = _titleY + _titleHeight;
            
            break;
            
        case PMGridStyleSmallImageOnRight:
            
            _imageWidth = _imageHeight = iPhone6_Plus || iPhone6 ? 75 : 65;
            _imageX = WIDTH/2 - WidthRate(16) - _imageWidth;
            _imageY = (self.bounds.size.height - _imageHeight)/2;
            
            _titleHeight = TITLEHI;
            _titleWidth = WidthRate(180);
            _titleX = WidthRate(16);
            _titleY = _imageY + 10;
            
            _priceHeight = PRICEHI;
            _priceWidth = _titleWidth;
            _priceX = _titleX;
            _priceY = _titleY + _titleHeight;
            
            
            
            break;
    }
    
    
    
}


- (void)createUI
{
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(_titleX, _titleY, _titleWidth, _titleHeight)];
    self.titleLB = titleLB;
    titleLB.textColor = [UIColor grayColor];
    titleLB.font = [UIFont systemFontOfSize:12];
    titleLB.textAlignment = NSTextAlignmentLeft;
    titleLB.numberOfLines = 2;
    [self addSubview:titleLB];
    
    UILabel *priceLB = [[UILabel alloc] initWithFrame:CGRectMake(_priceX, _priceY, _priceWidth, _priceHeight)];
    self.priceLB = priceLB;
    priceLB.textColor = HDVGRed;
    priceLB.font = [UIFont systemFontOfSize:14];
    priceLB.textAlignment = NSTextAlignmentLeft;
    [self addSubview:priceLB];
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(_imageX, _imageY, _imageWidth, _imageHeight)];
    self.productIV = iv;
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:iv];
    
}





@end
