//
//  PMDistributionsGoodsViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMCurrentGoodsTableViewController.h"
#import "PMUnsoldGoodsTableViewController.h"
#import "JHRefresh.h"

typedef void(^finishAction)(void);

@interface PMDistributionsGoodsViewController : UIViewController
@property(nonatomic,strong)UIButton *selectAllBtn;
@property(nonatomic,strong)UIView *noResultView;
@property(nonatomic,strong)UIImageView *noResultIV;
@property(nonatomic,strong)UILabel *noResultLB;
@property (assign,nonatomic) BOOL isSold;

- (void)showNoResultViewWithtitle:(NSString *)title AndIsSold:(NSInteger)isSold;
@end
