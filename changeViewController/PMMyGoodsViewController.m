//
//  PMMyGoodsViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMMyGoodsViewController.h"
#import "PMNetworking.h"
#import "PMMyPhoneInfo.h"
#import "PMAppraiseTVCell.h"
#import "detailBtn.h"
#import "YBScrollViewDelegateVC.h"
#import "PMScrollViewDelegateVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "JHRefresh.h"
#import "PMToastHint.h"
#import "MasterViewController.h"
#import <ShareSDK/ShareSDK.h>
#import "PMAppraiseVC.h"
#import "PMSpecView.h"
#import "PMUserInfos.h"
#import "PMShoppingCarViewController.h"
#import "YBSureOrderViewController.h"
#import "GoodDetailMessageCell.h"
#import "AppDelegate.h"
#import "PMGoodPostCell.h"

#define btnWidth WidthRate(200)
#define btnHeight 30
#define btnFont [UIFont boldSystemFontOfSize:15.0f]
#define pinkColor RGBA(238, 89, 117, 1)
#define notGoodsColor RGBA(200, 200, 200, 1)

@interface PMMyGoodsViewController () <UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) NSArray *sectionTitleArray;
@property (strong,nonatomic) NSArray *productPropertiesListArray;
@property (strong,nonatomic) NSArray *currentAppraiseArray;
@property (strong,nonatomic) UIView *tabBar;
@property (assign,nonatomic) BOOL isCare;
@property (strong,nonatomic) NSDictionary *productDic;
@property (strong,nonatomic) NSString *imageUrlString;
@property (copy,nonatomic) NSString *sellNum;
@property (copy,nonatomic) NSString *shopNum;
@property (strong,nonatomic) NSArray *PSASListArray;
@property (strong,nonatomic) UIButton *attentionBtn;
@property (strong,nonatomic) NSDictionary *prePayDic;
@property (strong,nonatomic) NSDictionary *discountDic;
@property (strong,nonatomic) NSMutableArray *mainPhotoUrlArray;
@property (strong,nonatomic) NSMutableArray *morePhotoUrlArray;
@property (strong,nonatomic) NSMutableDictionary *morePhotoDic;
@property (strong,nonatomic) UIView *barView;
@property (strong,nonatomic) UIButton *backButton;
@property (strong,nonatomic) UIScrollView *imageScrollView;
@property (strong,nonatomic) PMScrollViewDelegateVC *scrollDelegateVC;
@property (strong,nonatomic) YBScrollViewDelegateVC *newsScrollDelegateVC;
@property (strong,nonatomic) UIPageControl *pageControl;
@property (strong,nonatomic) UIScrollView *newsScrollView;
@property (assign,nonatomic) BOOL isZoom;
@property (strong,nonatomic) UIView *darkView;
@property (strong,nonatomic) UIView *typeSelectedView;
@property (strong,nonatomic) UIWebView *imageWebView;
@property (strong,nonatomic) UIView *testView;
@property (strong,nonatomic) UIButton *topButton;
@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;
@property (assign,nonatomic) CGFloat webHeight;
@property (strong,nonatomic) NSMutableArray *aSpecArr;
@property (strong,nonatomic) NSMutableArray *bSpecArr;
@property (strong,nonatomic) NSMutableArray *cSpecArr;
@property (strong,nonatomic) NSMutableArray *dSpecArr;
@property (strong,nonatomic) NSMutableArray *eSpecArr;
@property(nonatomic,strong)NSArray *abtnArr;
@property(nonatomic,strong)NSArray *bBtnArr;
@property (strong,nonatomic) NSArray *cBtnArr;
@property (strong,nonatomic) NSArray *dBtnArr;
@property (strong,nonatomic) NSArray *eBtnArr;
@property (copy,nonatomic) NSString *productCount;
@property (assign,nonatomic) CGFloat specRowHeight;
@property (copy,nonatomic) NSString *showPrice;
@property (strong,nonatomic) UILabel *typePriceLB;
@property (copy,nonatomic) NSString *shopCarCount;
@property (strong,nonatomic) UIImageView *noSaleIV;
@property (strong,nonatomic) UIView *testDarkView;
@property (strong,nonatomic) UIButton *numBtn;
@property (strong,nonatomic) detailBtn *carBtn;
@property (strong,nonatomic) UITableView *typeTableView;
@property (copy,nonatomic) NSString *stockId;
@property (strong,nonatomic) UILabel *specLB;
@property (assign,nonatomic) BOOL isPropertyOpen;
@property (assign,nonatomic) BOOL isAddCarFinish;
@property (strong,nonatomic) UIButton *addCarBtn;
@property (assign,nonatomic) BOOL isFirst;
@property (strong,nonatomic) UILabel *moreLB;
@property (strong,nonatomic) NSArray *frontImageArr;
@property (strong,nonatomic) AppDelegate *app;
@property (copy,nonatomic) NSString *defaultAValueId;
@property (copy,nonatomic) NSString *defaultBValueId;
@property (copy,nonatomic) NSString *defaultCValueId;
@property (copy,nonatomic) NSString *defaultDValueId;
@property (copy,nonatomic) NSString *defaultEValueId;
@property (copy,nonatomic) NSString *globoPostString;
@property (copy,nonatomic) NSString *globoProString;

@end

@implementation PMMyGoodsViewController
{
    NSString *_aid;
    NSString *_bid;
    NSString *_cid;
    NSString *_did;
    NSString *_eid;
    NSString *_aName;
    NSString *_bName;
    NSString *_cName;
    NSString *_dName;
    NSString *_eName;
    NSString *_discount;
    NSString *_discountPrice;
    NSString *_price;
    NSString *_rawPrice;
    NSString *_total;
    NSString *_shopId;
    UILabel *saleNumLB;
    NSInteger saleNum;
    UILabel *saleTitleLB;
    
}

static NSString *const normalCell = @"normalCell";
static NSString *const specCell = @"specCell";
static NSString *const morePhotoCell = @"morePhotoCell";
static NSString *const webImageCell = @"webImageCell";
static NSString *const appraiseCell = @"appraiseCell";
static NSString *const goodMessageCell = @"goodMessageCell";
static NSString *const goodPostCell = @"goodPostCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isFirst = YES;
    _isCare = NO;
    [self createUI];
    [self tableHeaderView];
    [self createBar];
    [self initTabBar];
    [self getMyProduct];
    [self getStockAndSellNum];
    [self createCarNumBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changenNum:) name:@"changeNum" object:nil];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popToLastView:)];
    [self.view addGestureRecognizer:swipe];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    self.sectionTitleArray = nil;
    self.productPropertiesListArray = nil;
    self.currentAppraiseArray = nil;
    self.tabBar = nil;
    self.productDic = nil;
    self.imageUrlString = nil;
    
    self.PSASListArray = nil;
    self.prePayDic = nil;
    self.discountDic = nil;
    self.mainPhotoUrlArray = nil;
    self.morePhotoUrlArray = nil;
    self.morePhotoDic = nil;
    self.barView = nil;
    
    self.imageScrollView = nil;
    self.scrollDelegateVC = nil;
    self.newsScrollDelegateVC = nil;
    self.pageControl = nil;
    self.newsScrollView = nil;
    self.darkView = nil;
    self.typeSelectedView = nil;
    self.imageWebView = nil;
    self.testView = nil;
    self.activityIndicator = nil;
    
    self.aSpecArr = nil;
    self.bSpecArr = nil;
    self.cSpecArr = nil;
    self.dSpecArr = nil;
    self.eSpecArr = nil;
    self.abtnArr = nil;
    self.bBtnArr = nil;
    self.cBtnArr = nil;
    self.dBtnArr = nil;
    self.eBtnArr = nil;
    
    self.typeTableView = nil;
    self.frontImageArr = nil;
}

- (instancetype)initWIthGoodsData:(NSDictionary *)goodsData
{
    self = [super init];
    if(self)
    {
        self.title = [goodsData objectNullForKey:@"title"];
    }
    return self;
}

- (void)createBar
{
    self.barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 64)];
    self.barView.backgroundColor = [UIColor clearColor];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(WidthRate(30), 27, 40, 40);
    [backBtn setImage:[UIImage imageNamed:@"icon_for_nav01"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(clickToPopVC:) forControlEvents:UIControlEventTouchUpInside];
    self.backButton = backBtn;
    [self.barView addSubview:backBtn];
    
    // 创建分享按钮
    UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    shareBtn.frame = CGRectMake(WidthRate(630), 27, 40, 40);
    [shareBtn setImage:[UIImage imageNamed:@"icon_share"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    shareBtn.tag = 11;
    [self.barView addSubview:shareBtn];
    
    [self.view addSubview:self.barView];
    
}

- (void)createUI
{
    self.sectionTitleArray = [@[@"自定义 占位用",@"自定义 占位用2",@"产品规格",@"产品参数",@"商品评价",@""] mutableCopy];
    self.frontImageArr = @[[UIImage imageNamed:@"goods_4"],[UIImage imageNamed:@"goods_5"],[UIImage imageNamed:@"goods_6"]];
    self.detailTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 50) style:UITableViewStylePlain];
    self.detailTableView.delegate = self;
    self.detailTableView.dataSource = self;
    self.detailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.detailTableView registerClass:[PMAppraiseTVCell class] forCellReuseIdentifier:appraiseCell];
    [self.detailTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:normalCell];
    [self.detailTableView registerClass:[GoodDetailMessageCell class] forCellReuseIdentifier:goodMessageCell];
    [self.detailTableView registerClass:[PMGoodPostCell class] forCellReuseIdentifier:goodPostCell];
    self.detailTableView.showsHorizontalScrollIndicator = NO;
    self.detailTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.detailTableView];
    
    self.darkView = [[UIView alloc] initWithFrame:self.view.frame];
    self.darkView.backgroundColor = [UIColor clearColor];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:self.darkView.frame];
    alphaView.backgroundColor = [UIColor darkGrayColor];
    alphaView.alpha = 0.8;
    [self.darkView addSubview:alphaView];
    [self.view addSubview:self.darkView];
    
    self.darkView.hidden = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(typeViewDismiss:)];
    [alphaView addGestureRecognizer:tap];
    
    self.testView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 50, WIDTH, HEIGHT - 50)];
    self.testView.backgroundColor = [UIColor whiteColor];
    
    UIView *tipsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 30)];
    UITapGestureRecognizer *backTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goBackMain:)];
    [tipsView addGestureRecognizer:backTap];
    UILabel *tipsLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, WIDTH, 10)];
    tipsLB.text = @"--点击此处或继续下滑回到商品详情--";
    tipsLB.font = [UIFont systemFontOfSize:12.0f];
    tipsLB.textColor = HDVGFontColor;
    tipsLB.textAlignment = NSTextAlignmentCenter;
    tipsView.backgroundColor = [UIColor whiteColor];
    tipsView.alpha = 1.0;
    [tipsView addSubview:tipsLB];
    self.imageWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0,0, WIDTH, HEIGHT - 50)];
    self.imageWebView.delegate = self;
    self.imageWebView.scrollView.delegate = self;
    
    UISwipeGestureRecognizer *webSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backToDetail:)];
    [webSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.testView addGestureRecognizer:webSwipe];
    
    [self.testView addSubview:self.imageWebView];
    
    UIButton *topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    topBtn.frame = CGRectMake(WIDTH - 60, HEIGHT - 60 - 50, 60, 60);
    [topBtn setBackgroundImage:[UIImage imageNamed:@"back60"] forState:UIControlStateNormal];
    topBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [topBtn addTarget:self action:@selector(backToTop:) forControlEvents:UIControlEventTouchUpInside];
    self.topButton = topBtn;
    [self.testView addSubview:topBtn];
    [self.view addSubview:self.testView];
    
    self.testDarkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.testView.bounds.size.width, self.testView.bounds.size.height)];
    self.testDarkView.backgroundColor = [UIColor clearColor];
    UIView *darkAlphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.testDarkView.bounds.size.width, self.testDarkView.bounds.size.height)];
    darkAlphaView.backgroundColor = [UIColor darkGrayColor];
    darkAlphaView.alpha = 0.8;
    UITapGestureRecognizer *webTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(typeViewDismiss:)];
    [darkAlphaView addGestureRecognizer:webTap];
    self.testDarkView.hidden = YES;
    [self.testDarkView addSubview:darkAlphaView];
    [self.testView addSubview:self.testDarkView];
   
}

- (void)createCarNumBtn
{
    UIButton *numBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.numBtn = numBtn;
    [numBtn setBackgroundColor:[UIColor redColor]];
    numBtn.frame = CGRectMake((WIDTH / 3)*3 - WidthRate(110), 5, 12, 12);
    numBtn.layer.cornerRadius = numBtn.bounds.size.width / 2;
    numBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
    [numBtn setTitle:@"0" forState:UIControlStateNormal];
    [numBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.numBtn.hidden = YES;
    [self.tabBar insertSubview:numBtn atIndex:1000];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionTitleArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 3)
    {
        if (self.isPropertyOpen)
        {
            return self.productPropertiesListArray.count;
        }
        else
        {
            return 0;
        }
    }
    else if (section == 4)
    {
        return 0;
    }
    else if (section == 5 || section == 2)
    {
        return 0;
    }
    else if (section == 1)
    {
        return 6;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:normalCell];

        NSDictionary *dic = self.productPropertiesListArray[indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ ：%@",[dic objectNullForKey:@"tp_name"],[dic objectNullForKey:@"tpv_properties_value"]];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if (indexPath.section == 4)
    {
        if (self.currentAppraiseArray.count == 0)
        {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"noCountCell"];
            UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, cell.contentView.frame.size.height)];
            
            UILabel *noCountTipsLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, noDataView.bounds.size.width, noDataView.bounds.size.height)];
            noCountTipsLB.text = @"暂时还没有评价哦~";
            noCountTipsLB.textColor = HDVGFontColor;
            noCountTipsLB.font = [UIFont systemFontOfSize:15.0];
            noCountTipsLB.textAlignment = NSTextAlignmentCenter;
            [noDataView addSubview:noCountTipsLB];
            [cell.contentView addSubview:noDataView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else
        {
            PMAppraiseTVCell *cell = [tableView dequeueReusableCellWithIdentifier:appraiseCell];
            NSDictionary *dic = self.currentAppraiseArray[indexPath.row];
            
            NSString *remarkString = [dic objectNullForKey:@"remark"];
            cell.remarkDict = @{@"remark":remarkString};
            cell.isBigVC = YES;
            [cell createUI];
            [cell setContentUserName:[NSString stringWithFormat:@"%@星",[dic objectNullForKey:@"level"]] detail:[[dic objectNullForKey:@"remark"] isEqualToString:@""]?@"这家伙好懒，什么评论也没写。":[dic objectNullForKey:@"remark"] time:[dic objectNullForKey:@"createDt"]];
        
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"productNameCell"];
            NSString *productName = [self.productDic objectNullForKey:@"productName"];
            CGSize sizeText = [productName sizeWithFont:[UIFont systemFontOfSize:18.0f] constrainedToSize:CGSizeMake(WIDTH - 50, MAXFLOAT)];
            UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 50, sizeText.height)];
            titleLB.text = productName;
            titleLB.numberOfLines = 0;
            titleLB.textColor = HDVGFontColor;
            titleLB.font = [UIFont systemFontOfSize:18.0f];
            [cell.contentView addSubview:titleLB];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else if (indexPath.row == 1)
        {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"remarkCell"];
            id productRemark = [self.productDic objectNullForKey:@"remark"];
            if ([productRemark isKindOfClass:[NSString class]] && ![productRemark isEqualToString:@""])
            {
                CGSize remarkSize = [productRemark sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(WIDTH - 30, MAXFLOAT)];
                
                UILabel *remarkLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 30, remarkSize.height + 10)];
                remarkLB.font = [UIFont systemFontOfSize:14.0f];
                remarkLB.textColor = [UIColor lightGrayColor];
                remarkLB.numberOfLines = 2;
                remarkLB.text = productRemark;
                [cell.contentView addSubview:remarkLB];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
        else if (indexPath.row == 2)
        {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"moneyCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSString *myPrice = [self.productDic objectNullForKey:@"productPrice"];
            
            UIFont *bigFont = [UIFont systemFontOfSize:20.0f];
            NSString *priceString = self.isPrePay ? [NSString stringWithFormat:@"￥%@",[self.prePayDic objectNullForKey:@"defPrice"]] : !self.isDiscount ?[NSString stringWithFormat:@"￥%.2lf",[myPrice doubleValue]] : [NSString stringWithFormat:@"￥%.2lf",[_discountPrice doubleValue]];
            
            NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
            [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} range:NSMakeRange(0, 1)];
            
            
            CGSize priceSize = [priceString sizeWithFont:bigFont constrainedToSize:CGSizeMake(MAXFLOAT, 44)];
            UILabel *priceLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, priceSize.width, 44)];
            priceLB.font = bigFont;
            priceLB.attributedText = nodeString;
            priceLB.textColor = [UIColor redColor];
            [cell.contentView addSubview:priceLB];
            
            UIFont *smallFont = [UIFont systemFontOfSize:14.0f];
            
            NSString *rawPriceString = [NSString stringWithFormat:@"￥%.2lf",[[self.productDic objectNullForKey:@"productRawPrice"] doubleValue]];
            CGSize rawSize = [rawPriceString sizeWithFont:smallFont constrainedToSize:CGSizeMake(MAXFLOAT, 22)];
            UILabel *rawPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(35) + priceSize.width + 5, 12, rawSize.width, 22)];
            rawPriceLB.text = rawPriceString;
            rawPriceLB.font = smallFont;
            rawPriceLB.textAlignment = NSTextAlignmentLeft;
            rawPriceLB.textColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:rawPriceLB];
            
            CALayer *line = [CALayer layer];
            line.backgroundColor = [UIColor lightGrayColor].CGColor;
            line.frame = CGRectMake(2, 11, rawSize.width, 1);
            [rawPriceLB.layer addSublayer:line];
            
            if (self.isDiscount)
            {
                
                UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(rawPriceLB.frame.origin.x + rawPriceLB.frame.size.width + WidthRate(40), 9, WidthRate(95), 22)];
                backView.backgroundColor = [UIColor blackColor];
                [cell.contentView addSubview:backView];
                
                //折扣
                UILabel *discountLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, backView.frame.size.width, backView.frame.size.height)];
                discountLB.text = [NSString stringWithFormat:@"%.1lf折",[_discount doubleValue] * 10];
                discountLB.font = smallFont;
                discountLB.textColor = [UIColor whiteColor];
                discountLB.textAlignment = NSTextAlignmentCenter;
                [backView addSubview:discountLB];
            }
        
            return cell;
        }
        else if (indexPath.row == 3)
        {
            PMGoodPostCell *cell = [tableView dequeueReusableCellWithIdentifier:goodPostCell];
            [cell createUI];
            [cell setCellData:[self.globoPostString isKindOfClass:[NSString class]] && ![self.globoPostString isEqualToString:@""] ? self.globoPostString : @"" Pro:[self.globoProString isKindOfClass:[NSString class]] && ![self.globoProString isEqualToString:@""] ? self.globoProString : @""];
            return cell;
        }
        else if (indexPath.row == 5)
        {
            GoodDetailMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:goodMessageCell];
            [cell createUI];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            CALayer *topLine = [CALayer layer];
            topLine.backgroundColor = HDVGPageBGGray.CGColor;
            topLine.frame = CGRectMake(0, 0, WIDTH, 1);
            [cell.layer addSublayer:topLine];
            
            CALayer *bottomLine = [CALayer layer];
            bottomLine.backgroundColor = HDVGPageBGGray.CGColor;
            bottomLine.frame = CGRectMake(0, 59, WIDTH, 1);
            [cell.layer addSublayer:bottomLine];
            
            return cell;
        }
        else
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"saleCell"];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"saleCell"];
            }
            
            if (!saleNumLB)
            {
                NSString *saleTitleString = @"销量:";
                CGSize saleTitleSize = [saleTitleString sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 24)];
                saleTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, saleTitleSize.width, 24)];
                saleTitleLB.font = [UIFont systemFontOfSize:14.0f];
                saleTitleLB.textColor = [UIColor lightGrayColor];
                saleTitleLB.text = saleTitleString;
                [cell.contentView addSubview:saleTitleLB];
                
                saleNumLB = [[UILabel alloc] initWithFrame:CGRectMake(15 + saleTitleSize.width + 5, 0, WIDTH - 35 - saleTitleSize.width, 24)];
                saleNumLB.font = [UIFont systemFontOfSize:14.0f];
                saleNumLB.textColor = [UIColor lightGrayColor];
                saleNumLB.textAlignment = NSTextAlignmentLeft;
                [cell.contentView addSubview:saleNumLB];
            }
            
            saleNumLB.text = [NSString stringWithFormat:@"%@件",@(saleNum)];
            
            if (saleNum == 0)
            {
                saleNumLB.hidden = YES;
                saleTitleLB.hidden = YES;
            }
            else
            {
                saleNumLB.hidden = NO;
                saleTitleLB.hidden = NO;
            }
            
            return cell;
        }
        
    }
    else if (indexPath.section == 2)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cells"];
        cell.textLabel.text = @"123";
        return cell;
    }
    else
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cells"];
        cell.textLabel.text = @"123";
        return cell;
    }
}



- (void)getCommentWithProductId
{
    [[PMNetworking defaultNetworking] request:PMRequestStateShopShowShopDisucss WithParameters:@{@"currentPage":@1,@"productId":self.productId,@"pageSize":@3,@"discussType":@"A"} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.currentAppraiseArray = [[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"];
            [self.detailTableView reloadData];
        }
        else
        {
            
        }
        
    } showIndicator:NO];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 1)
    {
        return nil;
    }
    else if (section == 0)
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 10)];
        headerView.backgroundColor = [UIColor whiteColor];
        return headerView;
    }
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 50)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = HDVGPageBGGray.CGColor;
    line.frame = CGRectMake(0, 0, WIDTH, 1);
    [headerView.layer addSublayer:line];
    
    headerView.tag = section;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSectionAction:)];
    [headerView addGestureRecognizer:tap];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(15 + 30 + 5, 10, WidthRate(200), 30)];
    lb.text = self.sectionTitleArray[section];
    lb.textColor = [UIColor lightGrayColor];
    lb.font = [UIFont boldSystemFontOfSize:16.0f];
    [headerView addSubview:lb];
    UIImageView *frontIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 30, 20)];
    frontIV.contentMode = UIViewContentModeScaleAspectFit;
    [headerView addSubview:frontIV];
    UIImageView *arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 15, 20, 20)];
    arrowIV.image = [UIImage imageNamed:@"arrowRight"];
    
    if (section == 2)
    {
        
        frontIV.image = self.frontImageArr[0];
        
        self.specLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(220) + 40, 10, WidthRate(550) - WidthRate(220), 30)];
        self.specLB.textColor = RGBA(109, 109, 109, 1);
        
        UILabel *moreLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(550), 15, WidthRate(670) - WidthRate(550), 20)];
        self.moreLB = moreLB;
        if ([self.specLB.text isEqualToString:@""] && ([_aName isKindOfClass:[NSNull class]] || !_aName || [_aName isEqualToString:@""]))
        {
            moreLB.text = @"";
        }
        else
        {
            moreLB.text = @"去选择";
        }
        moreLB.font = [UIFont systemFontOfSize:13.0f];
        moreLB.textColor = [UIColor lightGrayColor];
        moreLB.textAlignment = NSTextAlignmentRight;
        self.specLB.text = @"";
        self.specLB.font = [UIFont boldSystemFontOfSize:15.0f];
        [headerView addSubview:self.specLB];
        [headerView addSubview:moreLB];
        [headerView addSubview:arrowIV];
    }
    else if (section == 4)
    {
        frontIV.image = self.frontImageArr[2];
        UILabel *moreLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(250), 15, WidthRate(670) - WidthRate(250), 20)];
        if (self.currentAppraiseArray.count > 0)
        {
            moreLB.text = [NSString stringWithFormat:@"%@条评论",@(self.currentAppraiseArray.count)];
        }
        else
        {
            moreLB.text = @"暂无评论";
        }
        
        moreLB.font = [UIFont systemFontOfSize:13.0f];
        moreLB.textColor = [UIColor lightGrayColor];
        moreLB.textAlignment = NSTextAlignmentRight;
        [headerView addSubview:moreLB];
        [headerView addSubview:arrowIV];
    }
    else if (section == 5)
    {
        //angle = self.isImageOpen ? M_PI_2 : 0;
        
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), 10, WIDTH - WidthRate(80), 30)];
        lb.text = @"--向上滑动有惊喜--";
        lb.textColor = [UIColor lightGrayColor];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:15.0f];
        [headerView addSubview:lb];
    }
    else if (section == 3)
    {
        CGFloat angles = self.isPropertyOpen ? 90.0f : 0.0f;
        frontIV.image = self.frontImageArr[1];
        arrowIV.transform = CGAffineTransformMakeRotation((angles * M_PI) / 180.0f);
        [headerView addSubview:arrowIV];
    }
    
    if (section != 5)
    {
        CALayer *bottomLine = [CALayer layer];
        bottomLine.frame = CGRectMake(0, headerView.bounds.size.height, WIDTH, 0.8);
        bottomLine.backgroundColor = HDVGPageBGGray.CGColor;
        [headerView.layer addSublayer:bottomLine];
    }
    
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 10;
    
    }
    else if (section == 1)
    {
        return 0;
    }
    else
    {
        return 50;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1)
    {
        return 10;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3)
    {
        return 30;
    }
    else if (indexPath.section == 2)
    {
        return 35;
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 5)
        {
            return 60;
        }
        else if (indexPath.row == 3)
        {
            NSString *postString = @"";
            if ([self.globoPostString isEqualToString:@""] && [self.globoProString isEqualToString:@""])
            {
                return 0;
            }
            else
            {
                if ([self.globoProString isEqualToString:@""])
                {
                    postString = [NSString stringWithFormat:@"%@",self.globoPostString];
                }
                else
                {
                    postString = [NSString stringWithFormat:@"%@[%@]",self.globoPostString,self.globoProString];
                }
            }
            
            CGSize postStringSize = [postString boundingRectWithSize:CGSizeMake(WIDTH - 30, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
            return postStringSize.height + 3;
        }
        else if (indexPath.row == 1)
        {
            id productRemark = [self.productDic objectNullForKey:@"remark"];
            if ([productRemark isKindOfClass:[NSString class]] && ![productRemark isEqualToString:@""])
            {
                CGSize remarkSize = [productRemark sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(WIDTH - 30, MAXFLOAT)];
                return remarkSize.height;
            }
            else
            {
                return 0;
            }
            
        }
        else if (indexPath.row == 2)
        {
            return 40;
        }
        else if (indexPath.row == 0)
        {
            NSString *productName = [self.productDic objectNullForKey:@"productName"];
            NSDictionary *sizeDic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:18.0f],NSFontAttributeName, nil];
            CGSize sizeText = [productName boundingRectWithSize:CGSizeMake(WIDTH - 50, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:sizeDic context:nil].size;
            NSString *oneLineString = @"魔炮智能音响灯";
            CGSize oneLineSize = [oneLineString sizeWithFont:[UIFont systemFontOfSize:18.0f] constrainedToSize:CGSizeMake(WIDTH - 50, MAXFLOAT)];
            if (oneLineSize.height == sizeText.height)
            {
                return sizeText.height + 2;
            }
            else
            {
                return sizeText.height + 10;
            }
        }
        else
        {
            if (saleNum == 0)
            {
                return 0;
            }
            else
            {
                return 24;
            }
        }
        
    }
    else if (indexPath.section == 0)
    {
        return 0;
    }
    else
    {
        return 44;
    }
}

#pragma mark - tabBar
- (void)initTabBar
{
    UIView *tabBar = [[UIView alloc] initWithFrame:CGRectMake(- (WIDTH / 3), HEIGHT - 50 , WIDTH + WIDTH / 3, 50)];
    tabBar.backgroundColor = TabBarColor;
    self.tabBar = tabBar;
    
    UIButton *buyNowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    buyNowBtn.frame = CGRectMake(0, 0, WIDTH / 3, 50);
    buyNowBtn.backgroundColor = RGBA(28, 133, 18, 1);
    [buyNowBtn setTitle:@"直接去付款" forState:UIControlStateNormal];
    [buyNowBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buyNowBtn.titleLabel setFont:btnFont];
    [buyNowBtn addTarget:self action:@selector(goToPay:) forControlEvents:UIControlEventTouchUpInside];
    [tabBar addSubview:buyNowBtn];
    
    // 创建关注按钮
    detailBtn *attentionBtn = [[detailBtn alloc] initWithFrame:CGRectMake(((WIDTH / 3) - 25) / 2 + WIDTH / 3, 5, 45, 40)];
    self.attentionBtn = attentionBtn;
    [attentionBtn setImage:[UIImage imageNamed:@"detail_attention1"] forState:UIControlStateNormal];
    [attentionBtn setTitle:@"关注" forState:UIControlStateNormal];
    [attentionBtn setImage:[UIImage imageNamed:@"detail_attention2"] forState:UIControlStateSelected];
    [attentionBtn setTitle:@"已关注" forState:UIControlStateSelected];
    attentionBtn.selected = self.isCare;
    [attentionBtn addTarget:self action:@selector(clickToAddAttention:) forControlEvents:UIControlEventTouchUpInside];
    [tabBar addSubview:attentionBtn];
    
    
    //购物车按钮
    detailBtn *carBtn = [[detailBtn alloc] initWithFrame:CGRectMake(WIDTH / 3 + WIDTH / 3, 5, WIDTH / 3, 40)];
    [carBtn setTitle:@"购物车" forState:UIControlStateNormal];
    [carBtn setImage:[UIImage imageNamed:@"detail_car"] forState:UIControlStateNormal];
    [carBtn addTarget:self action:@selector(clickToPushShoppingCarVC:) forControlEvents:UIControlEventTouchUpInside];
    self.carBtn = carBtn;
    [tabBar addSubview:carBtn];
    
    
    //加入购物车
    UIButton *addToShoppingCarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addToShoppingCarBtn.frame = CGRectMake(WIDTH, 0, WIDTH / 3, 50);
    [addToShoppingCarBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
    [addToShoppingCarBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addToShoppingCarBtn.titleLabel setFont:btnFont];
    [addToShoppingCarBtn setBackgroundColor:HDVGRed];
    [addToShoppingCarBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    addToShoppingCarBtn.tag = 13;
    self.addCarBtn = addToShoppingCarBtn;
    [tabBar addSubview:addToShoppingCarBtn];
    
    
    [self.view addSubview:tabBar];
}

#pragma mark  筛选得到主图和详情图
- (void)getMainPhotoAndMorePhotoWithPhotoList:(NSArray *)arr
{
    self.mainPhotoUrlArray = [NSMutableArray array];
    self.morePhotoUrlArray = [NSMutableArray array];
    self.morePhotoDic = [NSMutableDictionary dictionary];
    
    for (NSDictionary *dic in arr)
    {
        NSString *urlString = [dic objectNullForKey:@"photoUrl"];
        if(!isNull(urlString) && ![urlString isEqualToString:@""])
        {
            //等于1是主图
            if([[dic objectNullForKey:@"isNarrow"] integerValue] == 1)
            {
                [self.mainPhotoUrlArray addObject:urlString];
            }
            else//-1是详情图
            {
                [self.morePhotoUrlArray addObject:urlString];
                //先使用默认图片
                [self.morePhotoDic setValue:[UIImage imageNamed:@"loading_image"] forKey:urlString];
            }
        }
    }
}

#pragma mark  headerView 滚动视图等
- (void)tableHeaderView
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH)];
    
    self.imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH)];
    [self.imageScrollView setShowsHorizontalScrollIndicator:NO];
    [self.imageScrollView setPagingEnabled:YES];
    [self.imageScrollView setBackgroundColor:[UIColor whiteColor]];
    
    
    
    //使用另外一个类来处理协议方法，防止不明崩溃
    self.scrollDelegateVC = [[PMScrollViewDelegateVC alloc] init];
    //设置代理，改变scrollView时会调用协议方法
    [self.imageScrollView setDelegate:self.scrollDelegateVC];
    [headerView addSubview:self.imageScrollView];
    
    //将主图也加到主图片数组中
    NSString *urlString = [self.productDic objectNullForKey:@"productLogo"];
    
    //得到图片链接数组
    if(urlString != nil)
        [self.mainPhotoUrlArray insertObject:urlString atIndex:0];
    NSMutableArray *imageUrlArray = [self.mainPhotoUrlArray copy];
    
    for (int i = 0; i<imageUrlArray.count; i++)
    {
        //添加广告图
        UIImageView *ad = [[UIImageView alloc] initWithFrame:CGRectMake(self.imageScrollView.bounds.size.width*i, 0, self.imageScrollView.bounds.size.width, self.imageScrollView.bounds.size.height)];
        
        ad.contentMode = UIViewContentModeScaleAspectFit;
        
        [self.imageScrollView addSubview:ad];
        
        //加载图片
        [ad sd_setImageWithURL:imageUrlArray[i] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageRetryFailed];
        
        ad.userInteractionEnabled = YES;
        ad.tag = i;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToZoom:)];
        [ad addGestureRecognizer:tap];
    }
    
    
    [self.imageScrollView setContentSize:CGSizeMake(self.imageScrollView.bounds.size.width * imageUrlArray.count, 0)];
    
    
    //广告翻页 pageControl
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, WIDTH - HeightRate(55), self.imageScrollView.bounds.size.width, HeightRate(20))];
    
    [self.pageControl setPageIndicatorTintColor:RGBA(186, 186, 186, 1)];
    [self.pageControl setCurrentPageIndicatorTintColor:RGBA(203, 45, 54, 1)];
    [self.pageControl setNumberOfPages:[self.imageScrollView.subviews count]];
    [headerView addSubview:self.pageControl];
    
    self.detailTableView.tableHeaderView = headerView;
    //使用另一个类处理scrollViewDidScroll方法，这个翻页也需要带过去随时更改
    self.scrollDelegateVC.pageControl = self.pageControl;
    
    
    
//    __weak YBGoodDetailViewController *weakSelf = self;
    __weak PMMyGoodsViewController *weakSelf = self;
    [self.detailTableView addRefreshFooterViewWithAniViewClass:[JHRefreshAniBaseView class] beginRefresh:^{
        if (isNull(self.imageUrlString) || !self.imageUrlString)
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:weakSelf.view ByToast:@"抱歉，该商品暂无图片"];
        }
        else
        {
            [UIView animateWithDuration:0.3f animations:^{
                weakSelf.detailTableView.contentOffset = CGPointMake(0, HEIGHT - 20);
                weakSelf.detailTableView.frame = CGRectMake(0, - HEIGHT - 50, WIDTH, HEIGHT - 50);
                weakSelf.testView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 50);
            } completion:^(BOOL finished) {
                [[UIApplication sharedApplication] setStatusBarHidden:YES];
            }];
        }
        [weakSelf.detailTableView footerEndRefreshing];
        
    }];
    
}

#pragma mark  添加关注
- (void)clickToAddAttention:(UIButton *)sender
{
    if([PMUserInfos shareUserInfo].PM_SID == nil)
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            [self clickToAddAttention:sender];
        };
        showAlertViewLogin;
    }
    else
    {
        
        NSMutableDictionary *param;
        //判断店铺id是否为空
        if(isNull([self.productDic objectNullForKey:@"shopId"]) || [[self.productDic objectNullForKey:@"shopId"] isEqualToString:@""])
            param = [@{@"productId":[self.productDic objectNullForKey:@"id"]} mutableCopy];
        else
            param = [@{@"productId":[self.productDic objectNullForKey:@"id"],@"shopId":[self.productDic objectNullForKey:@"shopId"]} mutableCopy];
        
        NSString *userId = [[[PMUserInfos shareUserInfo].userDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"id"];
        [param setValue:userId forKey:@"userId"];
        
        NSInteger requestState;
        UIColor *btnColor;
        NSString *toastString;
        if(_isCare)//已关注，则取消关注
        {
            //取消关注
            /*
             shopId     N	String		店铺id
             userId     Y	String		用户id
             productId	Y	String		商品id
             */
            requestState = PMRequestStateCareDeleteCareProduct;
            sender.selected = !sender.isSelected;
            toastString = @"取消关注成功";
        }
        else//未关注，则添加关注
        {
            requestState = PMRequestStateCareAddCareProduct;
            sender.selected = !sender.isSelected;
            toastString = @"关注成功";
        }
        
        
        [[PMNetworking defaultNetworking] request:requestState WithParameters:param callBackBlock:^(NSDictionary *dic) {
            if(intSuccess == 1)
            {
                _isCare = !_isCare;
                PMToastHint *hint = [PMToastHint defaultToastWithRight:YES];
                [hint showHintToView:self.view ByToast:toastString];
                [sender.layer setBackgroundColor:btnColor.CGColor];
            }
            else
            {
                showRequestFailAlertView;
            }
        }showIndicator:NO];
    }
}

#pragma mark 点击轮播图，放大
- (void)tapToZoom:(UITapGestureRecognizer *)tap
{
    self.imageScrollView.userInteractionEnabled = NO;
    self.isZoom = YES;
    if (self.isZoom)
    {
        self.newsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        [self.newsScrollView setShowsHorizontalScrollIndicator:NO];
        [self.newsScrollView setPagingEnabled:YES];
        [self.newsScrollView setBackgroundColor:[UIColor whiteColor]];
        
        
        //使用另外一个类来处理协议方法，防止不明崩溃
        self.newsScrollDelegateVC = [YBScrollViewDelegateVC defaultDelegate];
        self.newsScrollDelegateVC.imageScrollView = self.newsScrollView;
        //设置代理，改变scrollView时会调用协议方法
        [self.newsScrollView setDelegate:self.newsScrollDelegateVC];
        [self.view addSubview:self.newsScrollView];
        
        NSMutableArray *imageUrlArray = [self.mainPhotoUrlArray copy];
        
        
        for (int i = 0; i<imageUrlArray.count; i++)
        {
            //添加广告图
            UIScrollView *s = [[UIScrollView alloc] initWithFrame:CGRectMake(self.newsScrollView.bounds.size.width*i, 0, self.newsScrollView.bounds.size.width, self.newsScrollView.bounds.size.height)];
            s.delegate = [YBScrollViewDelegateVC defaultDelegate];
            s.minimumZoomScale = 1.0;
            s.maximumZoomScale = 3.0;
            s.tag = i+1;
            [s setZoomScale:1.0];
            
            UIImageView *ad = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, s.bounds.size.width, s.bounds.size.height)];
            ad.contentMode = UIViewContentModeScaleAspectFit;
            
            [s addSubview:ad];
            [self.newsScrollView addSubview:s];
            
            //加载图片
            [ad sd_setImageWithURL:imageUrlArray[i] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageRetryFailed];
            
            ad.userInteractionEnabled = YES;
            ad.tag = i;
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToExit:)];
            [ad addGestureRecognizer:tap];
        }
        
        
        [self.newsScrollView setContentSize:CGSizeMake(self.newsScrollView .bounds.size.width * imageUrlArray.count, 0)];
        NSInteger index = tap.view.tag;
        [self.newsScrollView setContentOffset:CGPointMake(self.newsScrollView.bounds.size.width * index, 0)];
    }
}

- (void)tapToExit:(UITapGestureRecognizer *)tap
{
    self.imageScrollView.userInteractionEnabled = YES;
    self.isZoom = NO;
    [UIView animateWithDuration:0.3 animations:^{
        
        self.newsScrollView.delegate = nil;
        self.newsScrollDelegateVC = nil;
        [self.newsScrollView removeFromSuperview];
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)clickAction:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 11:
        {
            [self initShareMessage:sender];
        }
            break;
        
        case 13:
        {
            sender.enabled = NO;
            self.view.userInteractionEnabled = NO;
            if ([_aName isKindOfClass:[NSNull class]] || [_aName isEqualToString:@""])
            {
                [self animationInCar];
            }
            else
            {
                self.view.userInteractionEnabled = YES;
                sender.enabled = YES;
                self.darkView.hidden = NO;
                self.testDarkView.hidden = NO;
                [UIView animateWithDuration:0.3f animations:^{
                    
                    CGRect frame = self.typeSelectedView.frame;
                    frame.origin.x = 40;
                    self.typeSelectedView.frame = frame;
                    self.backButton.enabled = NO;
                }];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)clickToPopVC:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  跳转到购物车
- (void)clickToPushShoppingCarVC:(UIButton *)sender
{
    if([PMUserInfos shareUserInfo].PM_SID == nil)
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            [self clickToPushShoppingCarVC:sender];
        };
        showAlertViewLogin;
    }
    else
    {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
//        ShoppingCarViewController *carVC = [[ShoppingCarViewController alloc] init];
        PMShoppingCarViewController *carVC = [[PMShoppingCarViewController alloc] init];
        carVC.isFromDetai = YES;
        [self.navigationController pushViewController:carVC animated:YES];
    }
}

- (void)initShareMessage:(id)sender
{
    //1+创建弹出菜单容器（iPad必要）
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionDown];
    
    
//    NSString *content = @"恒大微购商城";
    
//    id<ISSCAttachment> shareImage = [ShareSDK imageWithUrl:[self.productDic objectNullForKey:@"productLogo"]];
    __block UIImage *urlData;
    
    [[SDWebImageManager sharedManager] downloadImageWithURL:[self.productDic objectNullForKey:@"productLogo"] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
        urlData = image;
        
    }];
    
    id<ISSCAttachment> shareImage = [ShareSDK jpegImageWithImage:urlData quality:0.5];
    
    NSString *shareTitle = [self.productDic objectNullForKey:@"productName"];
    NSString *shareUrl = nil;
    
    //判断店铺id是否为空
    if(isNull([self.productDic objectNullForKey:@"shopId"]) || [[self.productDic objectNullForKey:@"shopId"] isEqualToString:@""]) {
        shareUrl = [NSString stringWithFormat:@"%@",[self.productDic objectNullForKey:@"showUrl"]];
    }
    else {
        shareUrl = [NSString stringWithFormat:@"%@",[self.productDic objectNullForKey:@"showUrl"]];
        //shareUrl = [shareUrl stringByAppendingString:[NSString stringWithFormat:@"&=%@",[self.productDic objectNullForKey:@"shopId"]]];
    }
    
    shareUrl = [shareUrl stringByReplacingOccurrencesOfString:@"details" withString:@"m_details"];
    NSString *description = [self.productDic objectNullForKey:@"productAbout"];
    NSString *remarkString  = @"";
    if ([[self.productDic objectNullForKey:@"remark"] isKindOfClass:[NSNull class]] || [[self.productDic objectNullForKey:@"remark"] isEqualToString:@""])
    {
        
    }
    else
    {
        remarkString = [self.productDic objectNullForKey:@"remark"];
    }
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:remarkString
                                       defaultContent:@"恒大微购--移动商城"
                                                image:shareImage
                                                title:shareTitle
                                                  url:shareUrl
                                          description:description
                                            mediaType:SSPublishContentMediaTypeNews];
    
    NSString *detailString = [self.productDic objectNullForKey:@"showUrl"];
    detailString = [detailString stringByReplacingOccurrencesOfString:@"details.html" withString:@"m_details.html"];
    
    //定制短信信息
    [publishContent addSMSUnitWithContent:[NSString stringWithFormat:@"%@:%@",shareTitle,detailString]];
    
    
    //定制复制信息
    [publishContent addCopyUnitWithContent:[NSString stringWithFormat:@"%@",[self.productDic objectNullForKey:@"showUrl"]] image:nil];
//    NSLog(@"showUrl --> %@",[self.productDic objectNullForKey:@"showUrl"]);
    
    
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"123");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"很抱歉,分享失败了"];
                                }
                            }];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.imageWebView.scrollView.delegate = nil;
    
    self.navigationController.navigationBar.barTintColor = NAVGray;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    CGRect frame = self.barView.frame;
    frame.origin.y = 0;
    self.barView.frame = frame;
    
    self.addCarBtn.userInteractionEnabled = YES;
    self.carBtn.userInteractionEnabled = YES;
    self.view.userInteractionEnabled = YES;
    
    self.navigationController.navigationBar.alpha = 1;
    
    self.navigationController.navigationBarHidden = NO;
    
    self.tabBar.hidden = YES;
}

- (void)createTypeSelected
{
    self.typeSelectedView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH - 40, HEIGHT)];
    self.typeSelectedView.backgroundColor = [UIColor whiteColor];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(typeViewHidden:)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.typeSelectedView addGestureRecognizer:swipe];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(26), 20 + HeightRate(26), self.typeSelectedView.bounds.size.width * 0.25, self.typeSelectedView.bounds.size.width * 0.25)];
    NSString *imageUrlString = [self.productDic objectNullForKey:@"productLogo"];
    [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrlString]];
    [self.typeSelectedView addSubview:imageView];
    
    NSString *productTitle = [self.productDic objectNullForKey:@"productName"];
//    CGSize titleSize = [productTitle sizeWithFont:[UIFont systemFontOfSize:16.0f] constrainedToSize:CGSizeMake(self.typeSelectedView.bounds.size.width * 0.75 - 20 - WidthRate(26), MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
//    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(self.typeSelectedView.bounds.size.width * 0.25 + 20, 20 + HeightRate(26), titleSize.width, titleSize.height)];
    
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(self.typeSelectedView.bounds.size.width * 0.25 + 20, 20 + HeightRate(26), self.typeSelectedView.bounds.size.width - self.typeSelectedView.bounds.size.width * 0.25 - WidthRate(52), self.typeSelectedView.bounds.size.width * 0.25 * 0.8)];
    titleLB.backgroundColor = [UIColor clearColor];
    titleLB.text = productTitle;
    titleLB.font = [UIFont systemFontOfSize:14.0f];
    titleLB.textAlignment = NSTextAlignmentLeft;
    titleLB.textColor = HDVGFontColor;
    titleLB.numberOfLines = 3;
    [self.typeSelectedView addSubview:titleLB];
    
    UIImageView *noSaleIV = [[UIImageView alloc] initWithFrame:CGRectMake(self.typeSelectedView.bounds.size.width * 0.6 + 20, 20 + HeightRate(30), WidthRate(200), WidthRate(100))];
    noSaleIV.image = [UIImage imageNamed:@"noSale"];
    noSaleIV.contentMode = UIViewContentModeScaleAspectFit;
    self.noSaleIV = noSaleIV;
    self.noSaleIV.hidden = YES;
    [self.typeSelectedView addSubview:noSaleIV];
    
    self.typePriceLB = [[UILabel alloc] initWithFrame:CGRectMake(self.typeSelectedView.bounds.size.width * 0.25 + 20, self.typeSelectedView.bounds.size.width * 0.25 - self.typeSelectedView.bounds.size.width * 0.1 + 30 + HeightRate(26), self.typeSelectedView.bounds.size.width * 0.75 - 20, self.typeSelectedView.bounds.size.width * 0.08)];
    self.typePriceLB.backgroundColor = [UIColor clearColor];
    self.typePriceLB.font = [UIFont systemFontOfSize:20.0f];
    self.typePriceLB.textColor = HDVGRed;
    self.typePriceLB.textAlignment = NSTextAlignmentLeft;
    [self.typeSelectedView addSubview:self.typePriceLB];
    
    CALayer *line1 = [CALayer layer];
    line1.backgroundColor = HDVGPageBGGray.CGColor;
    line1.frame = CGRectMake(0, self.typeSelectedView.bounds.size.width * 0.25 + 30 + HeightRate(26), self.typeSelectedView.bounds.size.width, 1);
    [self.typeSelectedView.layer addSublayer:line1];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, HEIGHT - 50, self.typeSelectedView.bounds.size.width, 50);
//    [button setTitle:@"加入购物车" forState:UIControlStateNormal];
    [button setTitle:@"确定" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:btnFont];
    [button setBackgroundColor:HDVGRed];
    [button addTarget:self action:@selector(sureSpecTypeSeleted:) forControlEvents:UIControlEventTouchUpInside];
    [self.typeSelectedView addSubview:button];
    
    UIScrollView *typeScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(WidthRate(20), self.typeSelectedView.bounds.size.width * 0.25 + 40 + HeightRate(26), self.typeSelectedView.bounds.size.width - WidthRate(30), HEIGHT - self.typeSelectedView.bounds.size.width * 0.25 - 40 - HeightRate(26) - 50)];
    typeScroll.contentSize = CGSizeMake(0, _specRowHeight + 40);
    typeScroll.showsHorizontalScrollIndicator = NO;
    typeScroll.showsVerticalScrollIndicator = NO;
    
//    PMSpecView *specView = [[PMSpecView alloc] initWithFrame:CGRectMake(WidthRate(20), self.typeSelectedView.bounds.size.width * 0.4 + 40 + HeightRate(26), self.typeSelectedView.bounds.size.width - WidthRate(30), _specRowHeight + 40)];
    PMSpecView *specView = [[PMSpecView alloc] initWithFrame:CGRectMake(0, 0,typeScroll.bounds.size.width,typeScroll.bounds.size.height)];
    specView.defaultAValueId = self.defaultAValueId;
    specView.defaultBValueId = self.defaultBValueId;
    specView.defaultCValueId = self.defaultCValueId;
    specView.defaultDValueId = self.defaultDValueId;
    specView.defaultEValueId = self.defaultEValueId;
    
    NSDictionary *dic;
    
    if(self.PSASListArray.count >0)
    {
        dic = self.PSASListArray[0];
        NSLog(@"aaa--> %@",dic);
        //默认规格值
//        _aid = [dic objectNullForKey:@"tpsvr_a_spec_val_id"];
//        _bid = [dic objectNullForKey:@"tpsvr_b_spec_val_id"];
//        _cid = [dic objectNullForKey:@"tpsvr_c_spec_val_id"];
//        _did = [dic objectNullForKey:@"tpsvr_d_spec_val_id"];
//        _eid = [dic objectNullForKey:@"tpsvr_e_spec_val_id"];
//        _aName = [dic objectNullForKey:@"a_spec_value_values"];
//        _bName = [dic objectNullForKey:@"b_spec_value_values"];
//        _cName = [dic objectNullForKey:@"c_spec_value_values"];
//        _dName = [dic objectNullForKey:@"d_spec_value_values"];
//        _eName = [dic objectNullForKey:@"e_spec_value_values"];
        _aid = self.defaultAValueId;
        _bid = self.defaultBValueId;
        _cid = self.defaultCValueId;
        _did = self.defaultDValueId;
        _eid = self.defaultEValueId;
        
        for (NSDictionary *aSpecDic in self.aSpecArr)
        {
            if ([[aSpecDic objectForKey:@"id"] isEqualToString:self.defaultAValueId])
            {
                _aName = [aSpecDic objectForKey:@"value"];
                break;
            }
        }
        
        for (NSDictionary *bSpecDic in self.bSpecArr)
        {
            _bName = [bSpecDic objectForKey:@"value"];
            break;
        }
        
        for (NSDictionary *cSpecDic in self.cSpecArr)
        {
            _cName = [cSpecDic objectForKey:@"value"];
            break;
        }
        
        for (NSDictionary *dSpecDic in self.dSpecArr)
        {
            _dName = [dSpecDic objectForKey:@"value"];
            break;
        }
        
        for (NSDictionary *eSpecDic in self.eSpecArr)
        {
            _eName = [eSpecDic objectForKey:@"value"];
            break;
        }
        
    }
    
     [specView createUIWithASpecName:[dic objectNullForKey:@"a_spec_name"] BSpecName:[dic objectNullForKey:@"b_spec_name"] CSpecName:[dic objectNullForKey:@"c_spec_name"] DSpecName:[dic objectNullForKey:@"d_spec_name"] ESpecName:[dic objectNullForKey:@"e_spec_name"] ASpecArr:self.aSpecArr BSpecArr:self.bSpecArr CSpecArr:self.cSpecArr DSpecArr:self.dSpecArr ESpecArr:self.eSpecArr];
    
    for (UIButton *btn in specView.aBtnArray)
    {
        [btn addTarget:self action:@selector(selectSpec:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    for (UIButton *btn in specView.bBtnArray)
    {
        [btn addTarget:self action:@selector(selectSpec:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    for (UIButton *btn in specView.cBtnArray)
    {
        [btn addTarget:self action:@selector(selectSpec:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    for (UIButton *btn in specView.dBtnArray)
    {
        [btn addTarget:self action:@selector(selectSpec:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    for (UIButton *btn in specView.eBtnArray)
    {
        [btn addTarget:self action:@selector(selectSpec:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.abtnArr = specView.aBtnArray;
    self.bBtnArr = specView.bBtnArray;
    self.cBtnArr = specView.cBtnArray;
    self.dBtnArr = specView.dBtnArray;
    self.eBtnArr = specView.eBtnArray;
    
//    //请求默认规格的商品库存销量售价等信息
    [self getStockAndSellNum];
//
    [self.typeSelectedView addSubview:typeScroll];
    [typeScroll addSubview:specView];
    [self.view addSubview:self.typeSelectedView];
}

#pragma mark 点击选择规格
- (void)selectSpec:(UIButton *)sender
{
    //遍历按钮数组，得到selected = YES的按钮tag
    //第一个规格的index
    NSInteger index = sender.tag%100;
    
    //大于100是第一个规格
    if(sender.tag>=100000)
    {
        NSDictionary *dic = self.aSpecArr[index];
        _aid = [dic objectNullForKey:@"id"];
        _aName = [dic objectNullForKey:@"value"];
    }
    else if(sender.tag >= 10000 && sender.tag < 100000)
    {
        NSDictionary *dic = self.bSpecArr[index];
        _bid = [dic objectNullForKey:@"id"];
        _bName = [dic objectNullForKey:@"value"];
    }
    else if (sender.tag >= 1000 && sender.tag < 10000)
    {
        NSDictionary *dic = self.cSpecArr[index];
        _cid = [dic objectNullForKey:@"id"];
        _cName = [dic objectNullForKey:@"value"];
    }
    else if (sender.tag >= 100 && sender.tag < 1000)
    {
        NSDictionary *dic = self.dSpecArr[index];
        _did = [dic objectNullForKey:@"id"];
        _dName = [dic objectNullForKey:@"value"];
    }
    else
    {
        NSDictionary *dic = self.eSpecArr[index];
        _eid = [dic objectNullForKey:@"id"];
        _eName = [dic objectNullForKey:@"value"];
    }
    
    [self getStockAndSellNum];
    
    
    //大于100000是第一个规格
    if(sender.tag>=100000)
    {
        for (UIButton *btn in self.abtnArr)
        {
            btn.selected = NO;
        }
        sender.selected = YES;
    }
    else if(sender.tag >= 10000 && sender.tag < 100000)
    {
        for (UIButton *btn in self.bBtnArr)
        {
            btn.selected = NO;
        }
        sender.selected = YES;
    }
    else if (sender.tag >= 1000 && sender.tag < 10000)
    {
        for (UIButton *btn in self.cBtnArr)
        {
            btn.selected = NO;
        }
        sender.selected = YES;
    }
    else if (sender.tag >= 100 && sender.tag < 1000)
    {
        for (UIButton *btn in self.dBtnArr)
        {
            btn.selected = NO;
        }
        sender.selected = YES;
    }
    else
    {
        for (UIButton *btn in self.eBtnArr)
        {
            btn.selected = NO;
        }
        sender.selected = YES;
    }
}

- (void)typeViewDismiss:(UITapGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.3f animations:^{
       
        CGRect frame = self.typeSelectedView.frame;
        frame.origin.x = WIDTH;
        self.typeSelectedView.frame = frame;
        
    } completion:^(BOOL finished) {
        
        self.darkView.hidden = YES;
        self.testDarkView.hidden = YES;
        self.backButton.enabled = YES;
        
    }];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.imageWebView.scrollView)
    {
        
        if (scrollView.contentOffset.y < -100)
        {
            [UIView animateWithDuration:0.3f animations:^{
                
                self.testView.frame = CGRectMake(0, HEIGHT - 50, WIDTH, HEIGHT - 50);
                self.detailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 50);
                self.detailTableView.contentOffset = CGPointZero;
                
            } completion:^(BOOL finished) {
                //                self.imageWebView.scrollView.contentOffset = CGPointMake(0, 0);
                [[UIApplication sharedApplication] setStatusBarHidden:NO];
                CGRect frame = self.barView.frame;
                frame.origin.y = 0;
                self.barView.frame = frame;
                
            }];
        }
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 50)];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha:0.5];
    
    
    UIView *noWaitView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 50)];
    [noWaitView setTag:108];
    noWaitView.alpha = 1.0f;
    noWaitView.backgroundColor = [UIColor clearColor];
    
    UIButton *noWaitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    noWaitBtn.frame = CGRectMake(0, 0, WIDTH, 50);
    [noWaitBtn setTitle:@"不想等了,回到详情" forState:UIControlStateNormal];
    [noWaitBtn setBackgroundColor:[UIColor lightGrayColor]];
    [noWaitBtn addTarget:self action:@selector(stopWait:) forControlEvents:UIControlEventTouchUpInside];
    
    [noWaitView addSubview:view];
//    [noWaitView addSubview:noWaitBtn];
    [self.testView addSubview:noWaitView];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0,0,50,50)];
    [self.activityIndicator setCenter:view.center];
    [self.activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [noWaitView addSubview:self.activityIndicator];
    
    [self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicator stopAnimating];
    UIView *view = (UIView*)[self.view viewWithTag:108];
    [view removeFromSuperview];
    CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue];
    self.webHeight = height;
}

- (void)backToTop:(UIButton *)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [UIView animateWithDuration:0.3f animations:^{
        
        self.testView.frame = CGRectMake(0, HEIGHT - 50, WIDTH, HEIGHT - 50);
        self.detailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 50);
        
    } completion:^(BOOL finished) {
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        
        CGRect frame = self.barView.frame;
        frame.origin.y = 0;
        self.barView.frame = frame;
        self.imageWebView.scrollView.contentOffset = CGPointMake(0, 0);
        
    }];
}

- (void)stopWait:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.testView.frame = CGRectMake(0, HEIGHT - 50, WIDTH, HEIGHT - 50);
        self.detailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 50);
        
    } completion:^(BOOL finished) {
        //                self.imageWebView.scrollView.contentOffset = CGPointMake(0, 0);
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        CGRect frame = self.barView.frame;
        frame.origin.y = 0;
        self.barView.frame = frame;
        
    }];
}

- (void)tapSectionAction:(UITapGestureRecognizer *)tap
{
    NSInteger section = tap.view.tag;
    if(section == self.sectionTitleArray.count-2)//商品评价
    {
        //判断是否已经登录
        if([PMUserInfos shareUserInfo].PM_SID == nil)
        {
            [MasterViewController defaultMasterVC].afterLoginAction = ^{
                [self tapSectionAction:tap];
            };
            
            showAlertViewLogin;
            
        }
        else
        {
            PMAppraiseVC *vc = [[PMAppraiseVC alloc] init];
            vc.goodsVC = self;
            [vc getModelsWithProductId:[self.productDic objectNullForKey:@"id"] finishAction:^{
                [self.navigationController pushViewController:vc animated:YES];
                
            }];
        }
        return;
    }
    else if (section == self.sectionTitleArray.count - 4)
    {
        if ([[[self.PSASListArray objectAtIndex:0] objectNullForKey:@"a_spec_name"] isKindOfClass:[NSNull class]] || ![[self.PSASListArray objectAtIndex:0] objectNullForKey:@"a_spec_name"] || [[[self.PSASListArray objectAtIndex:0] objectNullForKey:@"a_spec_name"] isEqualToString:@""])
        {
            [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view ByToast:@"该商品没有规格,可以直接加入购物车哟!"];
        }
        else
        {
            self.darkView.hidden = NO;
            self.testDarkView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
                
                CGRect frame = self.typeSelectedView.frame;
                frame.origin.x = 40;
                self.typeSelectedView.frame = frame;
                self.backButton.enabled = NO;
                
            }];
        }
        
    }
    else if (section == self.sectionTitleArray.count - 3)
    {
        if (self.productPropertiesListArray.count == 0)
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该商品暂时没有参数哦!"];
        }
        else
        {
            self.isPropertyOpen = !self.isPropertyOpen;
            NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:section];
            [self.detailTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            if (self.isPropertyOpen)
            {
                [self.detailTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
            
        }
    }
}

#pragma mark - view will appear & disappear
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tabBar && self.tabBar.hidden == YES) {
        self.tabBar.hidden = NO;
    }
    
    if(self.imageWebView)
    {
        self.imageWebView.scrollView.delegate = self;
    }
    
    if (self.testView.frame.origin.y != HEIGHT - 50)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        CGRect frame = self.barView.frame;
        frame.origin.y = -frame.size.height;
        self.barView.frame = frame;
    }
    
    [MasterViewController defaultMasterVC].tabBar.alpha = 0;
    
    self.navigationController.navigationBarHidden = YES;
    [self getCarNumByNet];
    
}

- (void)addCar:(UIButton *)sender
{
    if (![PMUserInfos shareUserInfo].PM_SID)
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            if ([_total integerValue] == 0) {
                PMToastHint *hint = [PMToastHint defaultToastWithRight:NO];
                [hint showHintToView:self.view ByToast:@"该商品目前库存数量为零"];
            }
            
            else if ([self.shopCarCount integerValue] + 1 > [self.productCount integerValue] && [_total integerValue] != 0)
            {
                
                //[[PMToastHint defaultToast] showHintToView:self.view ByToast:@"该规格商品库存不足"];
                PMToastHint *hint = [PMToastHint defaultToastWithRight:NO];
                [hint showHintToView:self.view ByToast:@"购物车数量已达到库存数量"];
            }
            else
            {
                self.carBtn.userInteractionEnabled = NO;
                //加入购物车
                [self animationInCar];
            }
        };
        showAlertViewLogin;
    }
    else
    {
        self.carBtn.userInteractionEnabled = NO;
        [self animationInCar];
    }
}

- (void)addToShopCarFinishAction:(finishAction)finishAction
{
    if([PMUserInfos shareUserInfo].PM_SID == nil)
    {
        [MasterViewController defaultMasterVC].afterLoginAction = ^{
            [self addToShopCarFinishAction:finishAction];
        };
        showAlertViewLogin;
    }
    else
    {
        
        //加入购物车
        /*
         参数:
         PM_SID         Y	String		Token
         productId      Y	String		商品ID
         productCount	Y	String		商品数量
         shopId         N	String		店铺ID
         aSpecValId     N	String		商品规格值ID（比如颜色）
         bSpecValId     N	String		商品规格值ID（比如尺码）
         areaId         Y	String		区域id（商品详情接口能拿到）
         */
        
        //        NSMutableDictionary *param = [@{PMSID,@"productId":[self.productDic objectNullForKey:@"id"],@"productCount":self.productNumLB.text,@"areaId":[self.productDic objectNullForKey:@"areaId"]} mutableCopy] ;
        
        
        // 确保 productDic 数据不为空
        if(!isNull(self.productDic) && [self.productDic isKindOfClass:[NSDictionary class]] && self.productDic != nil)
        {
            NSMutableDictionary *param = [@{PMSID,@"productId":[self.productDic objectNullForKey:@"id"],@"productCount":@"1",@"areaId":[self.productDic objectNullForKey:@"areaId"]} mutableCopy] ;
            
            //判断是否有shopId
//            if(!isNull([self.productDic objectForKey:@"shopId"]))
//            {
//                [param setValue:[self.productDic objectNullForKey:@"shopId"] forKey:@"shopId"];
//            }
            if (self.shopId && ![self.shopId isEqualToString:@""])
            {
                [param setValue:self.shopId forKey:@"shopId"];
            }
            
            if (self.isDiscount)
            {
                [param setValue:[self.discountDic objectNullForKey:@"discountScale"] forKey:@"productPrice"];
            }
            else
            {
                [param setValue:_price forKey:@"productPrice"];
            }
            
            [param setValue:self.stockId forKey:@"stockId"];
            
            // 加入购物车
            [[PMNetworking defaultNetworking] request:PMRequestStateShopCartAddTo WithParameters:param callBackBlock:^(NSDictionary *dic) {
                if([[dic objectNullForKey:@"success"] integerValue] == 1)
                {
                    NSString *urlString = [dic objectNullForKey:@"data"];
                    NSArray *arr = [urlString componentsSeparatedByString:@"&"];
                    for (NSString *str in arr)
                    {
                        if([str rangeOfString:@"shopId"].length>0)
                        {
                            _shopId = [str substringFromIndex:7];
                        }
                    }
                    [self getCarNumByNet];
                    if(finishAction)
                        finishAction();
                }
                else {
                    self.view.userInteractionEnabled = YES;
                }
            }showIndicator:NO];
        }
        else {
            self.view.userInteractionEnabled = YES;
        }
    }
}

#pragma mark - 选择规格按钮得到规格ID
- (NSString *)getSpecId:(NSArray *)specBtnArray SpecArray:(NSMutableArray *)specArray
{
    NSString *specValId = @"null";
    for (UIButton *btn in specBtnArray)
    {
        if(btn.selected == YES)
        {
            NSInteger index = [specBtnArray indexOfObject:btn];
            NSDictionary *dic = specArray[index];
            specValId = [dic objectNullForKey:@"id"];
            break;
        }
    }
    
    return specValId;
}

#pragma mark  获得各规格的个数
- (void)initSpecArray
{
    self.aSpecArr = [NSMutableArray array];
    self.bSpecArr = [NSMutableArray array];
    self.cSpecArr = [NSMutableArray array];
    self.dSpecArr = [NSMutableArray array];
    self.eSpecArr = [NSMutableArray array];
    
    for (int i = 0; i<self.PSASListArray.count; i++)
    {
        NSDictionary *dic = self.PSASListArray[i];
        //五个规格值
        NSString *aSpecValue = @"";
        NSString *bSpecValue = [dic objectNullForKey:@"b_spec_value_values"];
        NSString *cSpecValue = [dic objectNullForKey:@"c_spec_value_values"];
        NSString *dSpecValue = [dic objectNullForKey:@"d_spec_value_values"];
        NSString *eSpecValue = [dic objectNullForKey:@"e_spec_value_values"];
        
        //五个id值
        NSString *aSpecID = [dic objectNullForKey:@"tpsvr_a_spec_val_id"];
        NSString *bSpecID = [dic objectNullForKey:@"tpsvr_b_spec_val_id"];
        NSString *cSpecID = [dic objectNullForKey:@"tpsvr_c_spec_val_id"];
        NSString *dSpecID = [dic objectNullForKey:@"tpsvr_d_spec_val_id"];
        NSString *eSpecID = [dic objectNullForKey:@"tpsvr_e_spec_val_id"];
        
        if (!isNull([dic objectNullForKey:@"tpsvr_a_spec_val_id"]) && isNull([dic objectNullForKey:@"tpsvr_b_spec_val_id"]))
        {
            aSpecValue = [dic objectNullForKey:@"a_spec_value_values"];
        }
        else if (!isNull([dic objectNullForKey:@"tpsvr_b_spec_val_id"]) && isNull([dic objectNullForKey:@"tpsvr_c_spec_val_id"]))
        {
            aSpecValue = [dic objectNullForKey:@"a_spec_value_values"];
            bSpecValue = [dic objectNullForKey:@"b_spec_value_values"];
        }
        else if (!isNull([dic objectNullForKey:@"tpsvr_c_spec_val_id"]) && isNull([dic objectNullForKey:@"tpsvr_d_spec_val_id"]))
        {
            aSpecValue = [dic objectNullForKey:@"a_spec_value_values"];
            bSpecValue = [dic objectNullForKey:@"b_spec_value_values"];
            cSpecValue = [dic objectNullForKey:@"c_spec_value_values"];
        }
        else if (!isNull([dic objectNullForKey:@"tpsvr_d_spec_val_id"]) && isNull([dic objectNullForKey:@"tpsvr_e_spec_val_id"]))
        {
            aSpecValue = [dic objectNullForKey:@"a_spec_value_values"];
            bSpecValue = [dic objectNullForKey:@"b_spec_value_values"];
            cSpecValue = [dic objectNullForKey:@"c_spec_value_values"];
            dSpecValue = [dic objectNullForKey:@"d_spec_value_values"];
        }
        else if (!isNull([dic objectNullForKey:@"tpsvr_e_spec_val_id"]))
        {
            aSpecValue = [dic objectNullForKey:@"a_spec_value_values"];
            bSpecValue = [dic objectNullForKey:@"b_spec_value_values"];
            cSpecValue = [dic objectNullForKey:@"c_spec_value_values"];
            dSpecValue = [dic objectNullForKey:@"d_spec_value_values"];
            eSpecValue = [dic objectNullForKey:@"e_spec_value_values"];
        }
        
        if(i == 0)
        {
            if (!isNull([dic objectNullForKey:@"tpsvr_a_spec_val_id"]) && isNull([dic objectNullForKey:@"tpsvr_b_spec_val_id"]))
            {
                [self.aSpecArr addObject:@{@"id":aSpecID,@"value":aSpecValue}];
            }
            else if (!isNull([dic objectNullForKey:@"tpsvr_b_spec_val_id"]) && isNull([dic objectNullForKey:@"tpsvr_c_spec_val_id"]))
            {
                [self.aSpecArr addObject:@{@"id":aSpecID,@"value":aSpecValue}];
                [self.bSpecArr addObject:@{@"id":bSpecID,@"value":bSpecValue}];
            }
            else if (!isNull([dic objectNullForKey:@"tpsvr_c_spec_val_id"]) && isNull([dic objectNullForKey:@"tpsvr_d_spec_val_id"]))
            {
                [self.aSpecArr addObject:@{@"id":aSpecID,@"value":aSpecValue}];
                [self.bSpecArr addObject:@{@"id":bSpecID,@"value":bSpecValue}];
                [self.cSpecArr addObject:@{@"id":cSpecID,@"value":cSpecValue}];
            }
            else if (!isNull([dic objectNullForKey:@"tpsvr_d_spec_val_id"]) && isNull([dic objectNullForKey:@"tpsvr_e_spec_val_id"]))
            {
                [self.aSpecArr addObject:@{@"id":aSpecID,@"value":aSpecValue}];
                [self.bSpecArr addObject:@{@"id":bSpecID,@"value":bSpecValue}];
                [self.cSpecArr addObject:@{@"id":cSpecID,@"value":cSpecValue}];
                [self.dSpecArr addObject:@{@"id":dSpecID,@"value":dSpecValue}];
            }
            else if (!isNull([dic objectNullForKey:@"tpsvr_e_spec_val_id"]))
            {
                [self.aSpecArr addObject:@{@"id":aSpecID,@"value":aSpecValue}];
                [self.bSpecArr addObject:@{@"id":bSpecID,@"value":bSpecValue}];
                [self.cSpecArr addObject:@{@"id":cSpecID,@"value":cSpecValue}];
                [self.dSpecArr addObject:@{@"id":dSpecID,@"value":dSpecValue}];
                [self.eSpecArr addObject:@{@"id":eSpecID,@"value":eSpecValue}];
            }
            
        }
        else
        {
            if (!isNull(aSpecID) && isNull(bSpecID))
            {
                [self addTOSpecArray:self.aSpecArr SpecId:aSpecID SpecValue:aSpecValue];
            }
            else if (!isNull(bSpecID) && isNull(cSpecID))
            {
                [self addTOSpecArray:self.aSpecArr SpecId:aSpecID SpecValue:aSpecValue];
                [self addTOSpecArray:self.bSpecArr SpecId:bSpecID SpecValue:bSpecValue];
            }
            else if (!isNull(cSpecID) && isNull(dSpecID))
            {
                [self addTOSpecArray:self.aSpecArr SpecId:aSpecID SpecValue:aSpecValue];
                [self addTOSpecArray:self.bSpecArr SpecId:bSpecID SpecValue:bSpecValue];
                [self addTOSpecArray:self.cSpecArr SpecId:cSpecID SpecValue:cSpecValue];
            }
            else if (!isNull(dSpecID) && isNull(eSpecID))
            {
                [self addTOSpecArray:self.aSpecArr SpecId:aSpecID SpecValue:aSpecValue];
                [self addTOSpecArray:self.bSpecArr SpecId:bSpecID SpecValue:bSpecValue];
                [self addTOSpecArray:self.cSpecArr SpecId:cSpecID SpecValue:cSpecValue];
                [self addTOSpecArray:self.dSpecArr SpecId:dSpecID SpecValue:dSpecValue];
            }
            else if (!isNull(eSpecID))
            {
                [self addTOSpecArray:self.aSpecArr SpecId:aSpecID SpecValue:aSpecValue];
                [self addTOSpecArray:self.bSpecArr SpecId:bSpecID SpecValue:bSpecValue];
                [self addTOSpecArray:self.cSpecArr SpecId:cSpecID SpecValue:cSpecValue];
                [self addTOSpecArray:self.dSpecArr SpecId:dSpecID SpecValue:dSpecValue];
                [self addTOSpecArray:self.eSpecArr SpecId:eSpecID SpecValue:eSpecValue];
            }
        }
    }
    _specRowHeight = 0;
    if(self.aSpecArr.count >0)
        _specRowHeight += 40;
    if(self.bSpecArr.count >0)
        _specRowHeight += 40;
    if(self.cSpecArr.count >0)
        _specRowHeight += 40;
    if(self.dSpecArr.count >0)
        _specRowHeight += 40;
    if(self.eSpecArr.count >0)
        _specRowHeight += 40;
    
    
    [self getRowHeightWithSpecArr:self.aSpecArr];
    [self getRowHeightWithSpecArr:self.bSpecArr];
    [self getRowHeightWithSpecArr:self.cSpecArr];
    [self getRowHeightWithSpecArr:self.dSpecArr];
    [self getRowHeightWithSpecArr:self.eSpecArr];
    
    //行后留白
    _specRowHeight += 15;
}

#pragma mark - 添加到对应的SpecArray中
- (void)addTOSpecArray:(NSMutableArray *)specArray SpecId:(NSString *)specId SpecValue:(NSString *)specValue
{
    BOOL hasSame = NO;
    for (NSDictionary *specDic in specArray)
    {
        if ([[specDic objectNullForKey:@"id"] isEqualToString:specId])
        {
            hasSame = YES;
            break;
        }
    }
    if (hasSame == NO)
    {
        [specArray addObject:@{@"id":specId,@"value":specValue}];
    }
    
}

//通过规格个数，文字长短，计算行数，进而计算行高
- (void)getRowHeightWithSpecArr:(NSArray *)arr
{
    CGFloat maxWidth = WIDTH - WidthRate(20);
    CGFloat lastX = WidthRate(20);
    CGFloat lastWidth = WidthRate(90);
    
    for (NSDictionary *dic in arr)
    {
        NSString *specTitle = [dic objectNullForKey:@"value"];
        if(!isNull(specTitle))
        {
            CGFloat currentWidth = [specTitle lengthOfBytesUsingEncoding:NSUTF8StringEncoding]*4 +30;
            CGFloat currentX = lastX + lastWidth + WidthRate(20);
            //如果总和相加超过MaxWidth
            if(currentX + currentWidth >maxWidth)
            {
                //x回到初始值
                lastX = WidthRate(130);
                //y增加
                _specRowHeight += 40;
            }
            else
            {
                lastX = currentX;
                lastWidth = currentWidth;
            }
        }
    }
}

#pragma mark 点击规格按钮获取库存销量等
- (void)getStockAndSellNum
{
    /*
     calcFor	Y	String		A或者B或者AB(表示A规格或者B规格或者AB规格)
     aid        Y	String		A规格值的ID
     bid        Y	String		B规格值的ID
     productId	Y	String		商品ID
     areaId     Y	String		区域编号
     areaName	Y	String		区域名称
     */
    
    NSString *productId = [self.productDic objectNullForKey:@"id"];
    NSString *areaId = [self.productDic objectNullForKey:@"areaId"];
    NSString *areaName = [self.productDic objectNullForKey:@"areaName"];
    
    NSMutableDictionary *param;
    //初始化可变字典
    if(productId != nil && areaId != nil && areaName != nil)
        param = [@{@"productId":productId,@"areaId":areaId,@"areaName":areaName} mutableCopy];
    
    //判断有几种规格
    NSString *calcFor;
    if (self.abtnArr.count > 0 && self.bBtnArr.count == 0)
    {
        calcFor = @"A";
        [param setValue:_aid forKey:@"aid"];
    }
    else if (self.bBtnArr.count > 0 && self.cBtnArr.count == 0)
    {
        calcFor = @"AB";
        [param setValue:_aid forKey:@"aid"];
        [param setValue:_bid forKey:@"bid"];
    }
    else if (self.cBtnArr.count > 0 && self.dBtnArr.count == 0)
    {
        calcFor = @"ABC";
        [param setValue:_aid forKey:@"aid"];
        [param setValue:_bid forKey:@"bid"];
        [param setValue:_cid forKey:@"cid"];
    }
    else if (self.dBtnArr.count > 0 && self.eBtnArr.count == 0)
    {
        calcFor = @"ABCD";
        [param setValue:_aid forKey:@"aid"];
        [param setValue:_bid forKey:@"bid"];
        [param setValue:_cid forKey:@"cid"];
        [param setValue:_did forKey:@"did"];
    }
    else if (self.eBtnArr.count > 0)
    {
        calcFor = @"ABCDE";
        [param setValue:_aid forKey:@"aid"];
        [param setValue:_bid forKey:@"bid"];
        [param setValue:_cid forKey:@"cid"];
        [param setValue:_did forKey:@"did"];
        [param setValue:_eid forKey:@"eid"];
    }
    else
    {
        calcFor = @"";
    }
    [param setValue:calcFor forKey:@"calcFor"];
    
    
    //开始请求
    [[PMNetworking defaultNetworking] request:PMRequestStateProductCalcStockProduct WithParameters:param callBackBlock:^(NSDictionary *dic) {
        NSDictionary *dataDic = [dic objectNullForKey:@"data"];
        _discountPrice = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"discountPrice"]];
        _discount = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"zkbl"]];
        _price = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"productPrice"]];
        _sellNum = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"sellNum"]];
        _total = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"total"]];
        self.stockId = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"stockId"]];
        _rawPrice = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"productRawPrice"]];
        self.productCount = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"total"]];
        
//        NSIndexSet *indexSet=[[NSIndexSet alloc] initWithIndex:1];
//        [self.detailTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self.detailTableView reloadData];
        
        NSString *showPrice = @"";
        
        if (self.discountDic)
        {
            showPrice = [NSString stringWithFormat:@"￥%.2lf",[_discountPrice doubleValue]];
        }
        else
        {
            if ([_price isEqualToString:@"0"] || !_price)
            {
                showPrice = [NSString stringWithFormat:@"￥%.2lf",[[self.productDic objectNullForKey:@"productPrice"] doubleValue]];
            }
            else
            {
                showPrice = [NSString stringWithFormat:@"￥%.2lf",[_price doubleValue]];
            }
        }
        
        NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:showPrice];
        [nodeString addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16.0f]} range:NSMakeRange(0, 1)];
        self.typePriceLB.attributedText = nodeString;
        if ([_total integerValue] < 1)
        {
            self.noSaleIV.hidden = NO;
        }
        else
        {
            self.noSaleIV.hidden = YES;
        }
    }showIndicator:NO];
}

- (void)animationInCar
{
    if ([_total integerValue] == 0) {
        PMToastHint *hint = [PMToastHint defaultToastWithRight:NO];
        [hint showHintToView:self.view ByToast:@"该商品目前库存数量为零"];
        self.addCarBtn.enabled = YES;
    }
    
    else if ([self.shopCarCount integerValue] + 1 > [self.productCount integerValue] && [_total integerValue] != 0)
    {
        //[[PMToastHint defaultToast] showHintToView:self.view ByToast:@"该规格商品库存不足"];
        PMToastHint *hint = [PMToastHint defaultToastWithRight:NO];
        [hint showHintToView:self.view ByToast:@"购物车数量已达到库存数量"];
        self.addCarBtn.userInteractionEnabled = YES;
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            CGRect frame = self.typeSelectedView.frame;
            frame.origin.x = WIDTH;
            self.typeSelectedView.frame = frame;
            self.backButton.enabled = YES;
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.3f animations:^{
                
                self.darkView.hidden = YES;
                self.testDarkView.hidden = YES;
                
            } completion:^(BOOL finished) {
                
                UIGraphicsBeginImageContext(self.detailTableView.frame.size);
                CGContextRef context = UIGraphicsGetCurrentContext();
                [self.detailTableView.layer renderInContext:context];
                UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                UIImageView *photoIV = [[UIImageView alloc] initWithFrame:self.detailTableView.frame];
                [photoIV setImage:theImage];
                photoIV.contentMode = UIViewContentModeScaleAspectFit;
                [self.view addSubview:photoIV];
                
                [UIView animateWithDuration:0.5f animations:^{
                    
                    photoIV.frame = CGRectMake(0, 80, 20, 20);
                    
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:1.0f animations:^{
                        if (self.isAddCarFinish)
                        {
                            photoIV.frame = CGRectMake(WIDTH / 3  + WIDTH / 8 + 8  + WIDTH / 3 , HEIGHT - 45, 20, 20);
                        }
                        else
                        {
                            photoIV.frame = CGRectMake(WIDTH / 3  + WIDTH / 8 + 8, HEIGHT - 45, 20, 20);
                        }
                        
                        
                    } completion:^(BOOL finished) {
                        
                        [UIView animateWithDuration:0.5f animations:^{
                            
                            photoIV.alpha = 0;
                            
                        } completion:^(BOOL finished) {
                            
                            [photoIV removeFromSuperview];
                            __weak PMMyGoodsViewController *selfVC = self;
                            [self addToShopCarFinishAction:^{
                                selfVC.isAddCarFinish = YES;
                                PMToastHint *hint = [PMToastHint defaultToastWithRight:YES];
                                [hint showHintToView:selfVC.view ByToast:@"成功加入购物车"];
                                [UIView animateWithDuration:0.3f animations:^{
                                    selfVC.tabBar.frame = CGRectMake(0, selfVC.tabBar.frame.origin.y, WIDTH, 50);
                                } completion:^(BOOL finished) {
                                    selfVC.carBtn.userInteractionEnabled = YES;
                                   
                                    selfVC.addCarBtn.userInteractionEnabled = YES;
                                    selfVC.view.userInteractionEnabled = YES;
                                }];
                                
                            }];
                        }];
                        
                    }];
                }];
                
            }];
            
        }];
    }
}


- (void)changenNum:(NSNotification *)notify
{
    [self getCarNumByNet];
    
    
}

- (void)goBackMain:(UITapGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.testView.frame = CGRectMake(0, HEIGHT - 50, WIDTH, HEIGHT - 50);
        self.detailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 50);
        
    } completion:^(BOOL finished) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        CGRect frame = self.barView.frame;
        frame.origin.y = 0;
        self.barView.frame = frame;
        self.imageWebView.scrollView.contentOffset = CGPointMake(0, 0);
        
    }];
}

- (void)getCarNumByNet
{
    if ([PMUserInfos shareUserInfo].PM_SID)
    {
        NSDictionary *param = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID};
        [[PMNetworking defaultNetworking] request:PMRequestStateSafeShopCartShowBuyCart WithParameters:param callBackBlock:^(NSDictionary *dic) {
            if([[dic objectNullForKey:@"success"] integerValue] == 1)
            {
                NSInteger carNum = 0;
                NSArray *pasArr = [[dic objectNullForKey:@"data"] objectNullForKey:@"pas"];
                for (NSDictionary *cartDic in pasArr)
                {
                    NSInteger productNum = [[cartDic objectNullForKey:@"productCount"] integerValue];
                    carNum += productNum;
                }
                
                if (carNum == 0)
                {
                    [self.numBtn setHidden:YES];
                }
                else
                {
                    if (carNum > 99)
                    {
                        [self.numBtn setTitle:@"···" forState:UIControlStateNormal];
                    }
                    else
                    {
                        [self.numBtn setTitle:[NSString stringWithFormat:@"%@",@(carNum)] forState:UIControlStateNormal];
                    }
                    
                    [self.numBtn setHidden:NO];
                    
                }
            }
            else
            {
                //请求失败或没有数据的时候刷掉原有的数据
                [self.numBtn setHidden:YES];
            }
            
        }showIndicator:NO];
    }else
    {
        [self.numBtn setHidden:YES];
    }
}

- (void)typeViewHidden:(UISwipeGestureRecognizer *)swipe
{
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect frame = self.typeSelectedView.frame;
        frame.origin.x = WIDTH;
        self.typeSelectedView.frame = frame;
        
    } completion:^(BOOL finished) {
        
        self.darkView.hidden = YES;
        self.testDarkView.hidden = YES;
        self.backButton.enabled = YES;
        
    }];
}

- (void)sureSpecTypeSeleted:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect frame = self.typeSelectedView.frame;
        frame.origin.x = WIDTH;
        self.typeSelectedView.frame = frame;
        
    } completion:^(BOOL finished) {
        
        self.darkView.hidden = YES;
        self.testDarkView.hidden = YES;
        self.backButton.enabled = YES;
        
        self.moreLB.text = @"";
        self.specLB.text = [self getSpecString];
        [self animationInCar];
        
        
    }];
}

- (NSString *)getSpecString
{
    NSString *specString = @"";
    if ((![_aName isKindOfClass:[NSNull class]] && (_aName && ![_aName isEqualToString:@""])) && ([_bName isKindOfClass:[NSNull class]] || !_bName || ([_bName isKindOfClass:[NSString class]] && [_bName isEqualToString:@""])))
    {
        specString = [NSString stringWithFormat:@"%@",_aName];
    }
    else if ((![_bName isKindOfClass:[NSNull class]] && (_bName && ![_bName isEqualToString:@""])) && ([_cName isKindOfClass:[NSNull class]] || !_cName || ([_cName isKindOfClass:[NSString class]] && [_cName isEqualToString:@""])))
    {
        specString = [NSString stringWithFormat:@"%@/%@",_aName,_bName];
    }
    else if (![_cName isKindOfClass:[NSNull class]] && (_cName && ![_cName isEqualToString:@""]) && ([_dName isKindOfClass:[NSNull class]] || !_dName ||([_dName isKindOfClass:[NSString class]] && [_dName isEqualToString:@""])))
    {
        specString = [NSString stringWithFormat:@"%@/%@/%@",_aName,_bName,_cName];
    }
    else if ((![_dName isKindOfClass:[NSNull class]] && _dName && ![_dName isEqualToString:@""]) && ([_eName isKindOfClass:[NSNull class]] || !_eName ||([_eName isKindOfClass:[NSString class]] && [_eName isEqualToString:@""])))
    {
        specString = [NSString stringWithFormat:@"%@/%@/%@/%@",_aName,_bName,_cName,_dName];
    }
    else if (![_eName isKindOfClass:[NSNull class]] && _eName && ![_eName isEqualToString:@""])
    {
        specString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",_aName,_bName,_cName,_dName,_eName];
    }
    
    return specString;
}

- (void)goToPay:(UIButton *)sender
{
    NSString *idString = @"";
    if ([_shopId isKindOfClass:[NSNull class]] || [_shopId isEqualToString:@""])
    {
        idString = [NSString stringWithFormat:@"%@_%@_%@_null",[self.productDic objectNullForKey:@"id"],[self.productDic objectNullForKey:@"areaId"],self.stockId];
    }
    else
    {
        idString = [NSString stringWithFormat:@"%@_%@_%@_%@",[self.productDic objectNullForKey:@"id"],[self.productDic objectNullForKey:@"areaId"],self.stockId,_shopId];
    }
    
    NSDictionary *param = @{PMSID,@"ids":idString,@"productCount":@(1)};
    [[PMNetworking defaultNetworking] request:PMRequestStateShowOrderOne WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if([[dic objectNullForKey:@"success"] integerValue] == 1)
        {
            //请求成功，跳转页面
            NSDictionary *dataDic = [dic objectNullForKey:@"data"];
            
            //PMSureOrderVC *vc = [[PMSureOrderVC alloc] init];
            YBSureOrderViewController *vc = [[YBSureOrderViewController alloc] init];
            //地址信息
            
            // 当默认地址为空的时候
            id addressData = [dataDic objectNullForKey:@"defaultAddress"];
            NSString *redPackageTotalPrice = [NSString stringWithFormat:@"%@",[dataDic objectForKey:@"redPackageTotalPrice"]];
            if ([addressData isKindOfClass:[NSDictionary class]] && !isNull(addressData))
            {
                //默认地址信息
                vc.addressDic = [dataDic objectNullForKey:@"defaultAddress"];
            }
            
            vc.isTradeBack = NO;
            vc.orderRedLimit = [[dataDic objectForKey:@"orderRedPackLimit"] doubleValue];
            //商品信息
            vc.productsArray = [dataDic objectNullForKey:@"products"];
            if (self.isPrePay)
            {
                vc.totalPrice = [self.prePayDic objectNullForKey:@"defPrice"];
                vc.prePayTime = [self.prePayDic objectNullForKey:@"payStartTime"];
            }
            else if (self.isDiscount)
            {
                vc.totalPrice = _discountPrice;
            }
            else
            {
                vc.totalPrice = [dataDic objectNullForKey:@"totalPrice"];
            }
            vc.totalProductNum = [[dataDic objectNullForKey:@"totalNum"] integerValue];
            vc.isPrePay = self.isPrePay;
            
            if (isNull([dataDic objectNullForKey:@"carriage"]))
            {
                vc.carriagePrice = 0.00;
            }
            else
            {
                vc.carriagePrice = [[dataDic objectNullForKey:@"carriage"] doubleValue];
            }
            
            NSString *activeString = [dataDic objectNullForKey:@"hasActivityProduct"];
            BOOL isHasActive = NO;
            if ([activeString isEqualToString:@"0"])
            {
                isHasActive = NO;
            }
            else
            {
                isHasActive = YES;
            }
            
            vc.isHasActivityProduct = isHasActive;
            vc.redPackageTotalPrice = redPackageTotalPrice;
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dic objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [av show];
        }
    }showIndicator:YES];
}

- (void)popToLastView:(UISwipeGestureRecognizer *)swipe
{
    [UIView animateWithDuration:0.3f animations:^{
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

- (void)getMyProduct
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.productId forKey:@"id"];
    if ([self.shopId isKindOfClass:[NSString class]] && ![self.shopId isEqualToString:@""] )
    {
        [param setValue:self.shopId forKey:@"shopId"];
    }
    
    if ([PMUserInfos shareUserInfo].userId && ![[PMUserInfos shareUserInfo].userId isEqualToString:@""])
    {
        [param setValue:[PMUserInfos shareUserInfo].userId forKey:@"userId"];
    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateProductInfo WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSDictionary *dataDic = [dic objectNullForKey:@"data"];
            NSString *status = [dic objectNullForKey:@"messageCode"];
            self.sellNum = [dataDic objectNullForKey:@"sellNum"];
            self.shopNum = [[dataDic objectNullForKey:@"zkProduct"] objectNullForKey:@"limitBuyNum"];
            //商品主要数据
            self.productDic = [dataDic objectNullForKey:@"product"];
            self.imageUrlString = [self.productDic objectNullForKey:@"productAboutHtml"];
            //商品属性
            self.productPropertiesListArray = [dataDic objectNullForKey:@"productPropertiesList"];
            //商品规格
            self.PSASListArray = [dataDic objectNullForKey:@"PSASList"];
            self.imageUrlString = [self.productDic objectNullForKey:@"productAboutHtml"];
            saleNum = [[dataDic objectNullForKey:@"sellNum"] integerValue];
            //商品id
//            self.productId = [self.productDic objectNullForKey:@"id"];
            self.defaultAValueId = [dataDic objectNullForKey:@"a_spec_valueId"];
            self.defaultBValueId = [dataDic objectNullForKey:@"b_spec_valueId"];
            self.defaultCValueId = [dataDic objectNullForKey:@"c_spec_valueId"];
            self.defaultDValueId = [dataDic objectNullForKey:@"d_spec_valueId"];
            self.defaultEValueId = [dataDic objectNullForKey:@"e_spec_valueId"];
            
            //是否已经关注
            _isCare = [[dataDic objectNullForKey:@"alreadyCare"] boolValue];
            NSLog(@"isCare --> %d",_isCare);
            self.attentionBtn.selected = _isCare;
            
            if ([status isEqualToString:@"zk"])
            {
                self.isPrePay = NO;
                self.isDiscount = YES;
                self.discountDic = [dataDic objectNullForKey:@"zkProduct"];
            }
            else if ([status isEqualToString:@"pt"])
            {
                self.isPrePay = NO;
                self.isDiscount = NO;
            }
            
            self.globoPostString = [dataDic objectNullForKey:@"postageFreeGlobal"];
            self.globoProString = [dataDic objectNullForKey:@"postageFreePro"];
            
            [self getMainPhotoAndMorePhotoWithPhotoList:[dataDic objectNullForKey:@"productPhotoList"]];
            
            [self initSpecArray];
            [self createTypeSelected];
            [self getCommentWithProductId];
            [self tableHeaderView];
            if (isNull(self.imageUrlString) || !self.imageUrlString)
            {
                
            }
            else
            {
                [self.imageWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.imageUrlString]]];
            }
            [self.detailTableView reloadData];
        }
        else
        {
            self.app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            if (![self.app isNetConnect])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"没网啦,快去开启网络吧" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"很抱歉,这个商品已经下架了!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
            
            
        }
        
    } showIndicator:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0)
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.detailTableView.contentOffset = CGPointMake(0, HEIGHT - 20);
            self.detailTableView.frame = CGRectMake(0, - HEIGHT - 50, WIDTH, HEIGHT - 50);
            self.testView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 50);
        } completion:^(BOOL finished) {
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
        }];
    }
}

- (void)backToDetail:(UISwipeGestureRecognizer *)swipe
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.testView.frame = CGRectMake(0, HEIGHT - 50, WIDTH, HEIGHT - 50);
        self.detailTableView.contentOffset = CGPointZero;
        self.detailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 50);
        
    } completion:^(BOOL finished) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        CGRect frame = self.barView.frame;
        frame.origin.y = 0;
        self.barView.frame = frame;
        
    }];
}

@end
