//
//  MobileAuthcodeViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  修改绑定手机号验证码控制器
 */

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "PMUserInfos.h"

@interface MobileAuthcodeViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIView *authcodeView;
@property (strong, nonatomic) UILabel *mobileLabel;
@property (strong, nonatomic) UITextField *mobileAuthcodeTF;

// 判断是修改密码还是银行卡进入的页面
@property(copy, nonatomic) NSString *mobileOrBankAuthcode;

@property (strong, nonatomic) PMNetworking *networking;

@end
