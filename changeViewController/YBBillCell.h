//
//  YBBillCell.h
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YBSureOrderViewController;

@interface YBBillCell : UITableViewCell<UITextFieldDelegate>

@property(nonatomic,strong)UITextField *tf;
@property(nonatomic,strong)UILabel *kind;
@property (strong,nonatomic) UIButton *typeCurrentButton;
@property (strong,nonatomic) UIButton *contentCurrentButton;
@property (assign,nonatomic) BOOL isTradeBack;

@property(nonatomic,weak) YBSureOrderViewController *sureOrderVC;

@end
