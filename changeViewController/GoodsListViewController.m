//
//  GoodsListViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/7.
//  Copyright (c) 2014年 wallace. All rights reserved.
//



#import "GoodsListViewController.h"
#import "MasterViewController.h"
#import "PMShowClass.h"
#import "PMSearchResultViewController.h"
#import "YTNoResultView.h"
#import "SearchResultViewController.h"

@interface GoodsListViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UIGestureRecognizerDelegate>
//商品分类名称侧栏
@property(nonatomic,strong)UITableView *classifyTitle;
@property(nonatomic,strong)UITableView *classifyDetail;
@property(nonatomic,strong)PMShowClass *showClass;

@property(nonatomic,weak)MasterViewController *masterVC;
//line
@property(nonatomic,strong)CALayer *redLine;
//选中的行
@property(nonatomic,assign)NSInteger selectedRow;

//数据
@property(nonatomic,strong)NSArray *classArray;
@property(nonatomic,strong)NSMutableArray *parentClassArray;
@property (strong,nonatomic) UIView *noDataView;
@end

@implementation GoodsListViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"商品分类";
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.masterVC = [MasterViewController defaultMasterVC];

    
    //左侧标签栏
    [self initClassifyTitle];
    
    //右侧二三级分类
    [self createClassTableView];
    
    [self createNoDataView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getModels];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //从服务器获取数据
    //push时，默认是隐藏tabBar的，这里需要把tabBar显示回来
    [UIView animateWithDuration:0.3 animations:^{
        [[MasterViewController defaultMasterVC].tabBar setAlpha:1];
    }];
    
}


#pragma mark - 左侧商品分类名称
- (void)initClassifyTitle
{
    //添加表单控件
    self.classifyTitle = [[UITableView alloc] initWithFrame:CGRectMake(0, -64, WidthRate(220), HEIGHT)];
    
    [self.classifyTitle registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    [self.classifyTitle setDelegate: self];
    [self.classifyTitle setDataSource:self];
    
    [self.view addSubview:self.classifyTitle];
    [self.view sendSubviewToBack:self.classifyTitle];
    
    self.classifyTitle.backgroundColor = RGBA(244, 244, 244, 1);
    self.classifyTitle.separatorColor = [UIColor clearColor];
    
    [self.classifyTitle setContentInset:UIEdgeInsetsMake(64, 0, 49, 0)];
    
    //    //分割线
    //    CALayer *line = [CALayer layer];
    //    line.frame = CGRectMake(self.classifyTitle.bounds.size.width-0.4, 0, 0.4, HEIGHT);
    //    line.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
    //    [self.classifyTitle.layer addSublayer:line];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.classArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = HDVGPageBGGray;
    
    //选中时的背景图
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.contentView.bounds];
    cell.selectedBackgroundView.backgroundColor = RGBA(244, 244, 244, 1);
    
    //选中时的红线
    CALayer *line = [[CALayer alloc] init];
    line.frame = CGRectMake(0, 0, WidthRate(8), cell.selectedBackgroundView.bounds.size.height+1);
    line.backgroundColor = RGBA(188, 24, 40, 1).CGColor;
    [cell.selectedBackgroundView.layer addSublayer:line];
    
    //数据显示
    NSDictionary *dic = self.classArray[indexPath.row];
    
    [cell.textLabel setText:[NSString stringWithFormat:@" %@",[dic objectNullForKey:@"preParentClassName"]]];
    [cell.textLabel setFont:[UIFont systemFontOfSize:12]];
    
    //    //用于遮盖IOS7出现的不明线段
    //    cell.textLabel.layer.borderWidth = 1;
    //    cell.textLabel.layer.borderColor = [UIColor clearColor].CGColor;
    
    
    if(indexPath.row == self.selectedRow)
    {
        [cell setSelected:YES animated:NO];
        
        cell.textLabel.textColor = RGBA(150, 38, 55, 1);
    }
    else
    {
        [cell setSelected:NO animated:NO];
        cell.textLabel.textColor = [UIColor blackColor];
        
        cell.textLabel.layer.borderColor = HDVGPageBGGray.CGColor;
        
    }
    
    return cell;
}

#pragma mark 选中--请求数据
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
    //改变文字颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = RGBA(150, 38, 55, 1);
    
    cell.textLabel.layer.borderColor = [UIColor clearColor].CGColor;
    if (self.classArray.count != 0)
    {
        NSDictionary *dic = self.classArray[indexPath.row];
        //请求数据
        [self getSelectedClassifyDetailWithClassId:[dic objectNullForKey:@"preParentClassId"]];
    }
    
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //改变文字颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = HDVGPageBGGray;
    cell.textLabel.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark - 右侧二三级分类
- (void)createClassTableView
{
    UITableView *classDetail = [[UITableView alloc] initWithFrame:CGRectMake(WidthRate(250), -64, WidthRate(480), HEIGHT - 49)];
    self.classifyDetail = classDetail;
    [classDetail registerClass:[PMProductClassTVCell class] forCellReuseIdentifier:@"productClassCell"];
    [self.view addSubview:classDetail];
    classDetail.backgroundColor = [UIColor whiteColor];
    classDetail.separatorColor = [UIColor clearColor];
    classDetail.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    classDetail.showsVerticalScrollIndicator = NO;
    
    //使用另一个类当协议代理者，方便代码管理
    PMShowClass *showClass = [[PMShowClass alloc] init];
    showClass.goodsListVC = self;
    self.showClass = showClass;
    classDetail.delegate = showClass;
    classDetail.dataSource = showClass;
}

#pragma mark  点击进行搜索
- (void)clickBtn:(UIButton *)sender
{
    NSInteger section = sender.tag/1000;
    NSInteger row = sender.tag%1000/100;
    //一行有三个按钮  0  1  2
    //              3  4  5
    NSInteger arrIndex = sender.tag%10 + row*3;
    
    NSDictionary *dic = self.parentClassArray[section];
    NSArray *arr = [dic objectNullForKey:@"class"];
    NSDictionary *productDic = arr[arrIndex];

    SearchResultViewController *searchVC = [[SearchResultViewController alloc] init];
    searchVC.searchString = [productDic objectNullForKey:@"className"];
    
    NSString *areaName = [NSString stringWithFormat:@"%@,%@",[productDic objectNullForKey:@"areaId"],[productDic objectNullForKey:@"areaName"]];
    searchVC.areaName = areaName;
   
    [self.navigationController pushViewControllerWithNavigationControllerTransition:searchVC];

    
}


//
//#pragma mark - 下拉刷新
//- (void)initRefreshControl
//{
//    self.classifyTitle.alwaysBounceVertical = YES;
//    self.classifyDetail.alwaysBounceVertical = YES;
//    /*
//     参数1：添加下拉刷新的目标
//     参数2：执行的controller
//     参数3：执行的方法
//     参数4：坐标点集plist文件
//     参数5：颜色
//     参数6：线宽
//     参数7：下拉高度 x+ 开始刷新
//     参数8：图的大小比例
//     参数9：杠条从上往下来的随机位置的宽度
//     参数10：是否反向闪亮
//     参数11：分散的速度
//     */
//    self.refreshControl = [CBStoreHouseRefreshControl attachToScrollView:self.classifyDetail target:self refreshAction:@selector(refreshTriggered:) plist:@"storehouse" color:[UIColor colorWithRed:224/255.0 green:21/255.0 blue:1/255.0 alpha:1] lineWidth:2 dropHeight:60 scale:1.0 horizontalRandomness:self.classifyDetail.bounds.size.width reverseLoadingAnimation:YES internalAnimationFactor:1];
//
//}
//
//- (void)refreshTriggered:(id)sender
//{
//    //从服务器下载信息，下载完成后调用加载完成的方法
//    //这里模拟正在加载的延迟效果
//    [self performSelector:@selector(finishRefreshControl) withObject:nil afterDelay:3 inModes:@[NSRunLoopCommonModes]];
//}
//
//- (void)finishRefreshControl
//{
//    [self.refreshControl finishingLoading];
//    [self.classifyDetail reloadData];
//}
//
//#pragma mark tableView下拉时代理方法
////tableView 是继承 scrollView 的
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    [self.refreshControl scrollViewDidScroll];
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    [self.refreshControl scrollViewDidEndDragging];
//}

/*暂时不使用上下隐藏
 #pragma mark 隐藏navBar 和 tabBar
 - (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
 {
 //隐藏navgationBar 和 tabBar
 [UIView animateWithDuration:0.2 animations:^{
 self.navigationController.navigationBar.center = CGPointMake(self.masterVC.tabBar.center.x, -32);
 self.masterVC.tabBar.center = CGPointMake(self.masterVC.tabBar.center.x, HEIGHT + 49/2);
 } completion:^(BOOL finished) {
 //        [self.masterVC.nvBar setHidden:YES];
 [self.masterVC.tabBar setHidden:YES];
 }];
 
 }
 
 - (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
 {
 //显示navgationBar 和 tabBar
 //    [self.masterVC.nvBar setHidden:NO];
 [self.masterVC.tabBar setHidden:NO];
 
 [UIView animateWithDuration:0.2 animations:^{
 self.navigationController.navigationBar.center = CGPointMake(self.masterVC.tabBar.center.x, 22+20);
 self.masterVC.tabBar.center = CGPointMake(self.masterVC.tabBar.center.x, HEIGHT - 49/2);
 } completion:^(BOOL finished) {
 
 }];;
 }
 */


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - 从服务器请求数据
#pragma mark 请求一级类目数据
- (void)getModels
{
    //从首页通过经纬度请求商品信息时，得到的areaId
    //以 , 逗号分开字符串，得到第一个的就是areaId
//    NSArray *tempArr = [[PMUserInfos shareUserInfo].defaultArea componentsSeparatedByString:@","];
    NSDictionary *param;
//    if (tempArr.count > 0)
//    {
//        if (isNull([tempArr firstObject]) || [[tempArr firstObject] isEqualToString:@""])
//        {
//            param = @{@"areaId":@"440100"};
//        }
//        else
//        {
//            param = @{@"areaId":[tempArr firstObject]};
//        }
//    }
//    else
//    {
//        param = @{@"areaId":@"440100"};
//    }
    param = @{@"areaId":@"440100"};
    
    
    [[PMNetworking defaultNetworking] request:PMRequestStateSelectClass WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1 && [[dic objectNullForKey:@"data"] isKindOfClass:[NSArray class]])
        {
            self.classArray = [dic objectNullForKey:@"data"];
            if (self.classArray.count <= 0)
            {
                self.noDataView.hidden = NO;
                self.classifyTitle.hidden = YES;
                self.classifyDetail.hidden = YES;
            }
            else
            {
                self.noDataView.hidden = YES;
                self.classifyTitle.hidden = NO;
                self.classifyDetail.hidden = NO;
                NSIndexPath *indexPath;
                //默认选择第一项
                if (self.isFromMore)
                {
                    NSInteger selectIndex = 0;
                    for (NSInteger i = 0; i < self.classArray.count; i++)
                    {
                        NSDictionary *parentDic = self.classArray[i];
                        if ([self.classTitle isEqualToString:[parentDic objectNullForKey:@"preParentClassName"]])
                        {
                            selectIndex = i;
                            break;
                        }
                    }
                    indexPath = [NSIndexPath indexPathForRow:selectIndex inSection:0];
                    [self tableView:self.classifyTitle didSelectRowAtIndexPath:indexPath];
                }
                else
                {
                    indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self tableView:self.classifyTitle didSelectRowAtIndexPath:indexPath];
                }
                [self.classifyTitle reloadData];
                
                if (indexPath.row >= 12)
                {
                    [self.classifyTitle selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionBottom];
                }
                else
                {
                    [self.classifyTitle selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        }
        else
        {
            YTNoResultView *nrView = [[YTNoResultView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT) toView:self.view];
            [nrView showNoDataViewWithTitle:@"暂时没有分类数据哦"];
            
        }
    }showIndicator:NO];
    
    
    
}

#pragma mark 请求右边的二三级类目数据
- (void)getSelectedClassifyDetailWithClassId:(NSString *)classId
{
    [[PMNetworking defaultNetworking] request:PMRequestStateSelectPreAndChildClass WithParameters:@{@"classId":classId} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            NSArray *dicArray = [dic objectNullForKey:@"data"];
            
            BOOL isGet = NO;
            self.parentClassArray = [NSMutableArray array];
            //将得到的数据分类，以二级id为准
            for (NSDictionary *parentClassDic in dicArray)
            {
                //筛选有图片的 -- 如果有图片的话
                if([[[parentClassDic objectNullForKey:@"classLogo"] lastPathComponent] rangeOfString:@"null"].length == 0)
                {
                    
                    NSString *parentClassId = [parentClassDic objectNullForKey:@"parentClassId"];
                    for (NSDictionary *classDic in self.parentClassArray)
                    {
                        //如果是与已经存在的二级类目id相同，则加入到该类目中
                        if([[classDic objectNullForKey:@"parentClassId"] isEqualToString:parentClassId])
                        {
                            
                            //加入到该数组中
                            NSMutableArray *arr = [classDic objectNullForKey:@"class"];
                            [arr addObject:parentClassDic];
                            //标记该字典已经使用了
                            isGet = YES;
                            break;
                            
                        }
                    }
                    
                    //遍历完都没有加入到数组，说明没有一个相同的id，新增一个二级类目
                    if(isGet == NO)
                    {
                        //新字典
                        NSMutableDictionary *newDic = [NSMutableDictionary dictionary];
                        
                        NSMutableArray *newArr = [NSMutableArray array];
                        [newArr addObject:parentClassDic];
                        
                        [newDic setValuesForKeysWithDictionary:@{@"parentClassId":parentClassId,@"parentClassName":[parentClassDic objectNullForKey:@"parentClassName"],@"class":newArr}];
                        
                        //将字典加入到属性的数组中
                        [self.parentClassArray addObject:newDic];
                    }
                    else
                        isGet = NO;
                }
            }
            
            //得到数据并且处理完成后，刷新右侧页面
            self.showClass.parentClassArray = self.parentClassArray;
            [self.classifyDetail reloadData];
            
        }
        else
        {
            showRequestFailAlertView;
        }
    }showIndicator:YES];
}

#pragma mark - 没有分类视图
- (void)createNoDataView
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.classifyTitle.bounds), 0, WIDTH - CGRectGetWidth(self.classifyTitle.bounds), HEIGHT - 64)];
    self.noDataView = noDataView;
    //关闭交互，防止在最前面时挡住后面的用户交互
    noDataView.userInteractionEnabled = NO;
    UIImageView *noIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, noDataView.frame.size.height / 2 - HeightRate(300), CGRectGetWidth(noDataView.bounds), HeightRate(400))];
    noIV.image = [UIImage imageNamed:@"noData"];
    noIV.contentMode = UIViewContentModeScaleAspectFit;
    [noDataView addSubview:noIV];
    
    UILabel *noLB = [[UILabel alloc] initWithFrame:CGRectMake(0, noDataView.frame.size.height / 2 + HeightRate(100) , CGRectGetWidth(noDataView.bounds), HeightRate(100))];
    noLB.text = @"暂时没有该分类的信息";
    noLB.textColor = HDVGFontColor;
    noLB.font = [UIFont systemFontOfSize:17.0f];
    noLB.numberOfLines = 0;
    noLB.textAlignment = NSTextAlignmentCenter;
    [noDataView addSubview:noLB];
    noDataView.hidden = YES;
    
    [self.view insertSubview:noDataView atIndex:1000];
}

- (void)showNoResultView:(BOOL)show
{
    [self.view bringSubviewToFront:self.noDataView];
    self.noDataView.hidden = !show;
}

- (void)getMore
{
    NSInteger selectIndex = 0;
    for (NSInteger i = 0; i < self.classArray.count; i++)
    {
        NSDictionary *parentDic = self.classArray[i];
        if ([self.classTitle isEqualToString:[parentDic objectNullForKey:@"preParentClassName"]])
        {
            selectIndex = i;
            break;
        }
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectIndex inSection:0];
    [self tableView:self.classifyTitle didSelectRowAtIndexPath:indexPath];
    [self.classifyTitle reloadData];
    if (selectIndex >= 12)
    {
        [self.classifyTitle selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionBottom];
    }
    else
    {
        [self.classifyTitle selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
}

@end
