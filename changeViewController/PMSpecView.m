//
//  PMSpecView.m
//  changeViewController
//
//  Created by pmit on 15/6/11.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define getViewX(view) view.frame.origin.x
#define getViewY(view) view.frame.origin.y
#define getViewWidth(view) view.bounds.size.width
#define getViewHeigh(view) view.bounds.size.height

#define itemX WidthRate(20)
#define itemY 15
#define itemDY 15
#define itemHeight 25
#define lbWidth WidthRate(96)
//所有的规格按钮和间距的和不得超过MaxWidth
#define MaxWidth WIDTH - WidthRate(20) - 40

#define textFont [UIFont boldSystemFontOfSize:12]
#define myBorderColor RGBA(190, 190, 190, 1).CGColor

#import "PMSpecView.h"
#import "PMMyPhoneInfo.h"

@implementation PMSpecView
{
    CGFloat _lastWidth;
    CGFloat _lastX;
    CGFloat _currentY;
}

- (void)createUIWithASpecName:(NSString *)aName BSpecName:(NSString *)bName CSpecName:(NSString *)cName DSpecName:(NSString *)dName ESpecName:(NSString *)eName ASpecArr:(NSArray *)ASpecArr BSpecArr:(NSArray *)BSpecArr CSpecArr:(NSArray *)CSpecArr DSpecArr:(NSArray *)DSpecArr ESpecArr:(NSArray *)ESpecArr
{
    if ([aName isKindOfClass:[NSString class]] && aName && ![aName isEqualToString:@""])
    {
        UILabel *aSpecNameLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, itemY, lbWidth, itemHeight)];
        aSpecNameLB.textAlignment = NSTextAlignmentCenter;
        aSpecNameLB.text = aName;
        aSpecNameLB.textColor = [UIColor darkGrayColor];
        aSpecNameLB.font = textFont;
        [self addSubview:aSpecNameLB];
        _lastWidth = getViewWidth(aSpecNameLB);
        _lastX = getViewX(aSpecNameLB);
        _currentY = getViewY(aSpecNameLB);
        
        self.aBtnArray = [NSMutableArray array];
        for (int i = 0; i<ASpecArr.count; i++)
        {
            NSDictionary *dic = ASpecArr[i];
            NSString *btnTitle = [dic objectNullForKey:@"value"];
            CGFloat currentWidth = [btnTitle lengthOfBytesUsingEncoding:NSUTF8StringEncoding]*5 +30;
            CGFloat currentX = _lastX + _lastWidth + WidthRate(20);
            //如果总和相加超过MaxWidth
            if(currentX + currentWidth >MaxWidth)
            {
                //x回到初始值
                currentX = getViewX(aSpecNameLB) + getViewWidth(aSpecNameLB) + WidthRate(20);
                //y增加
                _currentY += itemHeight + itemDY;
            }
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(currentX, _currentY, currentWidth, getViewHeigh(aSpecNameLB))];
            _lastWidth = btn.bounds.size.width;
            _lastX = btn.frame.origin.x;
            
            [self addSubview:btn];
            btn.tag = i+100000;
            
            [btn setTitle:btnTitle forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [btn setTitleColor:HDVGRed forState:UIControlStateSelected];
            btn.titleLabel.font = textFont;
            //            btn.layer.borderColor = myBorderColor;
            //            btn.layer.borderWidth = 0.4;
            UIImage *image1 = [UIImage imageNamed:@"01normal@2x"];
            UIImage *image2 = [UIImage imageNamed:@"01selected@2x"];
            // 图片拉伸参数的格式是(top,left,bottom,right)，从上、左、下、右
            UIImage *bg1 = [image1 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            UIImage *bg2 = [image2 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            [btn setBackgroundImage:bg1 forState:UIControlStateNormal];
            [btn setBackgroundImage:bg2 forState:UIControlStateSelected];
            btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.aBtnArray addObject:btn];
            
//            if(i == 0)
//            {
//                btn.selected = YES;
//            }
            NSDictionary *aspecDic = [ASpecArr objectAtIndex:i];
            NSString *aSpecId = [aspecDic objectForKey:@"id"];
            if ([self.defaultAValueId isEqualToString:aSpecId])
            {
                btn.selected = YES;
            }
            else
            {
                btn.selected = NO;
            }
        }
    }
    
    if ([bName isKindOfClass:[NSString class]] && bName && ![bName isEqualToString:@""])
    {
        _currentY += itemDY + itemHeight;
        
        UILabel *bSpecNameLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, _currentY, lbWidth, itemHeight)];
        bSpecNameLB.textAlignment = NSTextAlignmentCenter;
        bSpecNameLB.text = bName;
        bSpecNameLB.font = textFont;
        bSpecNameLB.textColor = [UIColor darkGrayColor];
        [self addSubview:bSpecNameLB];
        _lastWidth = getViewWidth(bSpecNameLB);
        _lastX = getViewX(bSpecNameLB);
        
        self.bBtnArray = [NSMutableArray array];
        for (int i = 0; i<BSpecArr.count; i++)
        {
            NSDictionary *dic = BSpecArr[i];
            //得到文字计算长度，让按钮自动适配大小
            NSString *btnTitle = [dic objectNullForKey:@"value"];
            //计算总长是否超过屏幕
            CGFloat currentWidth = btnTitle.length*11 +20;
            CGFloat currentX = _lastX + _lastWidth + WidthRate(20);
            //如果总和相加超过MaxWidth
            if(currentX + currentWidth >MaxWidth)
            {
                //x回到初始值
                currentX = getViewX(bSpecNameLB) + getViewWidth(bSpecNameLB) + WidthRate(20);
                //y增加
                _currentY += itemHeight + itemDY;
            }
            
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(currentX, _currentY, currentWidth, getViewHeigh(bSpecNameLB))];
            _lastWidth = btn.bounds.size.width;
            _lastX = btn.frame.origin.x;
            
            [self addSubview:btn];
            btn.tag = i + 10000;
            
            [btn setTitle:btnTitle forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [btn setTitleColor:HDVGRed forState:UIControlStateSelected];
            btn.titleLabel.font = textFont;
            [btn setBackgroundColor:[UIColor whiteColor]];
            
            UIImage *image1 = [UIImage imageNamed:@"01normal@2x"];
            UIImage *image2 = [UIImage imageNamed:@"01selected@2x"];
            UIImage *bg1 = [image1 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            UIImage *bg2 = [image2 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            [btn setBackgroundImage:bg1 forState:UIControlStateNormal];
            [btn setBackgroundImage:bg2 forState:UIControlStateSelected];
            btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.bBtnArray addObject:btn];
            
            NSDictionary *bspecDic = [BSpecArr objectAtIndex:i];
            NSString *bSpecId = [bspecDic objectForKey:@"id"];
            if ([self.defaultBValueId isEqualToString:bSpecId])
            {
                btn.selected = YES;
            }
            else
            {
                btn.selected = NO;
            }
        }
    }
    
    if ([cName isKindOfClass:[NSString class]] && cName && ![cName isEqualToString:@""])
    {
        _currentY += itemDY + itemHeight;
        
        UILabel *cSpecNameLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, _currentY, lbWidth, itemHeight)];
        cSpecNameLB.textAlignment = NSTextAlignmentCenter;
        cSpecNameLB.text = cName;
        cSpecNameLB.font = textFont;
        cSpecNameLB.textColor = [UIColor darkGrayColor];
        [self addSubview:cSpecNameLB];
        _lastWidth = getViewWidth(cSpecNameLB);
        _lastX = getViewX(cSpecNameLB);
        
        self.cBtnArray = [NSMutableArray array];
        for (int i = 0; i<CSpecArr.count; i++)
        {
            NSDictionary *dic = CSpecArr[i];
            //得到文字计算长度，让按钮自动适配大小
            NSString *btnTitle = [dic objectNullForKey:@"value"];
            //计算总长是否超过屏幕
            CGFloat currentWidth = btnTitle.length*11 +20;
            CGFloat currentX = _lastX + _lastWidth + WidthRate(20);
            //如果总和相加超过MaxWidth
            if(currentX + currentWidth >MaxWidth)
            {
                //x回到初始值
                currentX = getViewX(cSpecNameLB) + getViewWidth(cSpecNameLB) + WidthRate(20);
                //y增加
                _currentY += itemHeight + itemDY;
            }
            
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(currentX, _currentY, currentWidth, getViewHeigh(cSpecNameLB))];
            _lastWidth = btn.bounds.size.width;
            _lastX = btn.frame.origin.x;
            
            [self addSubview:btn];
            btn.tag = i + 1000;
            
            [btn setTitle:btnTitle forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [btn setTitleColor:HDVGRed forState:UIControlStateSelected];
            btn.titleLabel.font = textFont;
            [btn setBackgroundColor:[UIColor whiteColor]];
            
            UIImage *image1 = [UIImage imageNamed:@"01normal@2x"];
            UIImage *image2 = [UIImage imageNamed:@"01selected@2x"];
            UIImage *bg1 = [image1 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            UIImage *bg2 = [image2 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            [btn setBackgroundImage:bg1 forState:UIControlStateNormal];
            [btn setBackgroundImage:bg2 forState:UIControlStateSelected];
            btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.cBtnArray addObject:btn];
            
            NSDictionary *cspecDic = [CSpecArr objectAtIndex:i];
            NSString *cSpecId = [cspecDic objectForKey:@"id"];
            if ([self.defaultCValueId isEqualToString:cSpecId])
            {
                btn.selected = YES;
            }
            else
            {
                btn.selected = NO;
            }
        }
    }
    
    if ([dName isKindOfClass:[NSString class]] && dName && ![dName isEqualToString:@""])
    {
        _currentY += itemDY + itemHeight;
        
        UILabel *dSpecNameLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, _currentY, lbWidth, itemHeight)];
        dSpecNameLB.textAlignment = NSTextAlignmentCenter;
        dSpecNameLB.text = dName;
        dSpecNameLB.font = textFont;
        dSpecNameLB.textColor = [UIColor darkGrayColor];
        [self addSubview:dSpecNameLB];
        _lastWidth = getViewWidth(dSpecNameLB);
        _lastX = getViewX(dSpecNameLB);
        
        self.dBtnArray = [NSMutableArray array];
        for (int i = 0; i<DSpecArr.count; i++)
        {
            NSDictionary *dic = DSpecArr[i];
            //得到文字计算长度，让按钮自动适配大小
            NSString *btnTitle = [dic objectNullForKey:@"value"];
            //计算总长是否超过屏幕
            CGFloat currentWidth = btnTitle.length*11 +20;
            CGFloat currentX = _lastX + _lastWidth + WidthRate(20);
            //如果总和相加超过MaxWidth
            if(currentX + currentWidth >MaxWidth)
            {
                //x回到初始值
                currentX = getViewX(dSpecNameLB) + getViewWidth(dSpecNameLB) + WidthRate(20);
                //y增加
                _currentY += itemHeight + itemDY;
            }
            
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(currentX, _currentY, currentWidth, getViewHeigh(dSpecNameLB))];
            _lastWidth = btn.bounds.size.width;
            _lastX = btn.frame.origin.x;
            
            [self addSubview:btn];
            btn.tag = i + 100;
            
            [btn setTitle:btnTitle forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [btn setTitleColor:HDVGRed forState:UIControlStateSelected];
            btn.titleLabel.font = textFont;
            [btn setBackgroundColor:[UIColor whiteColor]];
            
            UIImage *image1 = [UIImage imageNamed:@"01normal@2x"];
            UIImage *image2 = [UIImage imageNamed:@"01selected@2x"];
            // 图片拉伸参数的格式是(top,left,bottom,right)，从上、左、下、右
            UIImage *bg1 = [image1 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            UIImage *bg2 = [image2 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            [btn setBackgroundImage:bg1 forState:UIControlStateNormal];
            [btn setBackgroundImage:bg2 forState:UIControlStateSelected];
            btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.dBtnArray addObject:btn];
            
            NSDictionary *dspecDic = [DSpecArr objectAtIndex:i];
            NSString *dSpecId = [dspecDic objectForKey:@"id"];
            if ([self.defaultDValueId isEqualToString:dSpecId])
            {
                btn.selected = YES;
            }
            else
            {
                btn.selected = NO;
            }
        }
    }
    
    if ([eName isKindOfClass:[NSString class]] && eName && ![eName isEqualToString:@""])
    {
        _currentY += itemDY + itemHeight;
        
        UILabel *eSpecNameLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, _currentY, lbWidth, itemHeight)];
        eSpecNameLB.textAlignment = NSTextAlignmentCenter;
        eSpecNameLB.text = eName;
        eSpecNameLB.font = textFont;
        eSpecNameLB.textColor = [UIColor darkGrayColor];
        [self addSubview:eSpecNameLB];
        _lastWidth = getViewWidth(eSpecNameLB);
        _lastX = getViewX(eSpecNameLB);
        
        self.eBtnArray = [NSMutableArray array];
        for (int i = 0; i<ESpecArr.count; i++)
        {
            NSDictionary *dic = ESpecArr[i];
            //得到文字计算长度，让按钮自动适配大小
            NSString *btnTitle = [dic objectNullForKey:@"value"];
            //计算总长是否超过屏幕
            CGFloat currentWidth = btnTitle.length*11 +20;
            CGFloat currentX = _lastX + _lastWidth + WidthRate(20);
            //如果总和相加超过MaxWidth
            if(currentX + currentWidth >MaxWidth)
            {
                //x回到初始值
                currentX = getViewX(eSpecNameLB) + getViewWidth(eSpecNameLB) + WidthRate(20);
                //y增加
                _currentY += itemHeight + itemDY;
            }
            
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(currentX, _currentY, currentWidth, getViewHeigh(eSpecNameLB))];
            _lastWidth = btn.bounds.size.width;
            _lastX = btn.frame.origin.x;
            
            [self addSubview:btn];
            btn.tag = i;
            
            [btn setTitle:btnTitle forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [btn setTitleColor:HDVGRed forState:UIControlStateSelected];
            btn.titleLabel.font = textFont;
            [btn setBackgroundColor:[UIColor whiteColor]];
            
            UIImage *image1 = [UIImage imageNamed:@"01normal@2x"];
            UIImage *image2 = [UIImage imageNamed:@"01selected@2x"];
            UIImage *bg1 = [image1 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            UIImage *bg2 = [image2 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20) resizingMode:UIImageResizingModeStretch];
            [btn setBackgroundImage:bg1 forState:UIControlStateNormal];
            [btn setBackgroundImage:bg2 forState:UIControlStateSelected];
            btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.eBtnArray addObject:btn];
            
            NSDictionary *especDic = [ESpecArr objectAtIndex:i];
            NSString *eSpecId = [especDic objectForKey:@"id"];
            if ([self.defaultEValueId isEqualToString:eSpecId])
            {
                btn.selected = YES;
            }
            else
            {
                btn.selected = NO;
            }
        }
    }
}
@end
