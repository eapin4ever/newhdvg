//
//  PMSureOrderTVCell.h
//  changeViewController
//
//  Created by pmit on 14/12/17.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMSureOrderTVCell : UITableViewCell
@property(nonatomic,strong)UIImageView *iv;
@property (strong,nonatomic) UIImageView *activeIV;
@property (assign,nonatomic) BOOL isTradeBack;

- (void)createUI;

- (void)setContentTitle:(NSString *)title classify1:(NSString *)classify1 classify2:(NSString *)classify2  Classify3:(NSString *)classify3 Classify4:(NSString *)classify4 Classify5:(NSString *)classify5 distanceOrLocation:(NSString *)distance number:(NSString *)num price:(double)price;

@end
