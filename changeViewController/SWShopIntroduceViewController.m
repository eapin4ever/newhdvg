//
//  SWShopIntroduceViewController.m
//  changeViewController
//
//  Created by P&M on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "SWShopIntroduceViewController.h"
#include "PMToastHint.h"

@interface SWShopIntroduceViewController ()

@end

@implementation SWShopIntroduceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"店铺介绍";
    self.view.backgroundColor = HDVGPageBGGray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self createShopIntroduceUI];
    [self createSaveControllerUI];
}

- (void)createShopIntroduceUI
{
    UIView *introduceView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 60)];
    introduceView.backgroundColor = [UIColor whiteColor];
    self.introduceView = introduceView;
    [self.view addSubview:introduceView];
    
    // 分隔线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    line1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [introduceView.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(0, 60 - 0.5, WIDTH, HeightRate(1));
    line2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [introduceView.layer addSublayer:line2];
    
    // 店铺介绍输入框
    UITextView *introduceTV = [[UITextView alloc] initWithFrame:CGRectMake(WidthRate(36), 1, WidthRate(680), 58)];
    introduceTV.delegate = self;
    //这个字段如果不是空，就赋值
    NSString *shopAbout = [self.shopDataDic objectNullForKey:@"shopAbout"];
    if(shopAbout.length == 0)
        introduceTV.text = @"这家伙很懒，什么都没有写!";
    else
        introduceTV.text = [self.shopDataDic objectNullForKey:@"shopAbout"];
    introduceTV.font = [UIFont systemFontOfSize:15.0f];
//    [introduceTV.layer setBorderColor:HDVGFontColor.CGColor];
//    [introduceTV.layer setBorderWidth:.5];
//    [introduceTV.layer setCornerRadius:4.0];
    introduceTV.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    introduceTV.returnKeyType = UIReturnKeyDone;
    introduceTV.keyboardType = UIKeyboardTypeDefault;
    self.introduceTV = introduceTV;
    [introduceView addSubview:introduceTV];
    
    
//    self.placeholderLB = [[UILabel alloc]initWithFrame:CGRectMake(5, 3, 200, 25)];
//    self.placeholderLB.text = @"请输入店铺介绍";
//    self.placeholderLB.font =  [UIFont systemFontOfSize:15.0f];
//    self.placeholderLB.textColor = [UIColor lightGrayColor];
//    [introduceTV addSubview:self.placeholderLB];
    
    
    // 提示输入字数
    self.tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(180), introduceView.frame.origin.y + introduceView.frame.size.height, WidthRate(140), HeightRate(48))];
    self.tipLabel.backgroundColor = [UIColor clearColor];
    self.tipLabel.text = [NSString stringWithFormat:@"%lu/25", (unsigned long)self.introduceTV.text.length];
    self.tipLabel.textColor = [UIColor grayColor];
    self.tipLabel.textAlignment = NSTextAlignmentRight;
    self.tipLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.view addSubview:self.tipLabel];
}

- (void)createSaveControllerUI
{
    // 创建保存按钮
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(shopIntroduceSaveButtonClick:)];
    
    [saveBtn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR,NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = saveBtn;
    
    
//    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    saveBtn.frame = CGRectMake(WidthRate(46), HEIGHT - 64 - 60, WIDTH - WidthRate(46) * 2, 44);
//    [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
//    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [saveBtn addTarget:self action:@selector(shopIntroduceSaveButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [saveBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
//    [saveBtn setBackgroundColor:ButtonBgColor];
//    [saveBtn.layer setCornerRadius:6.0f];
//    [self.view addSubview:saveBtn];
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.introduceTV resignFirstResponder];
}

#pragma mark 隐藏键盘，实现UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
//    if ([self isContainsEmoji:text])
//    {
//        return NO;
//    }
    
    // 判断输入是否已经超出最大可输入长度
    if ([text isEqualToString:@""] && range.length > 0) {
        
        //删除字符肯定是安全的
        return YES;
    }
    else {
        NSInteger maxLength = 25;
        NSInteger strLength = textView.text.length - range.length + text.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

// 改变输入字数多少，显示几分之几
- (void)textViewDidChange:(UITextView *)textView
{
    if ([textView.text length] == 0) {
        [self.placeholderLB setHidden:NO];
    }
    else {
        [self.placeholderLB setHidden:YES];
    }
    
//    self.tipLabel.text = [NSString stringWithFormat:@"%lu/25", (unsigned long)self.introduceTV.text.length];
}

#pragma mark - 保存按钮响应事件
- (void)shopIntroduceSaveButtonClick:(UIButton *)button
{
    [self.introduceTV resignFirstResponder];
    
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    // 判断是否为空
    if (self.introduceTV.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入店铺介绍"];
        return;
    }
    if (self.introduceTV.text.length > 25) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"店铺介绍长度应不大于25个字符"];
        return;
    }
    
    NSLog(@"introtext -->%@",self.introduceTV.text);
    
    NSDictionary *cashWithdrawals = @{@"PM_SID":userInfots.PM_SID,@"id":[self.shopDataDic objectNullForKey:@"id"], @"shopAbout":self.introduceTV.text};
    
    [[PMNetworking defaultNetworking] request:PMRequestStateShareUpdateShopHome WithParameters:cashWithdrawals callBackBlock:^(NSDictionary *dict) {
        
        if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"店铺介绍设置成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
            alertView.tag = 1;
            [alertView show];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }showIndicator:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if ([_remarkDelegate respondsToSelector:@selector(passRemark:)])
        {
            [_remarkDelegate passRemark:self.introduceTV.text];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)isContainsEmoji:(NSString *)string {
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}

@end
