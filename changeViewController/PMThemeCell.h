//
//  PMThemeCell.h
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMThemeCell : UITableViewCell

@property (strong, nonatomic) UIImageView *iv1;
@property (strong, nonatomic) UIImageView *iv2;
@property (strong, nonatomic) UIImageView *iv3;

@property(nonatomic,strong)UIButton *btn1;
@property(nonatomic,strong)UIButton *btn2;
@property(nonatomic,strong)UIButton *btn3;

- (void)createUI;

- (void)addTarget:(id)target selector:(SEL)sel;

@end
