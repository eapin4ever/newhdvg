//
//  PMDistributionGoodsCell.h
//  changeViewController
//
//  Created by pmit on 15/7/28.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBShareBtn.h"

@interface PMDistributionGoodsCell : UITableViewCell

@property(nonatomic,strong) YBShareBtn *shareBtn;
@property (nonatomic,strong) UILabel *lossLB;
@property(nonatomic,strong) UIImageView *iv;
@property(nonatomic,strong) UIButton *goDetailBtn;
@property(nonatomic,strong) UIView *messageView;
@property(strong,nonatomic) YBShareBtn *deleteBtn;
@property (strong,nonatomic) UIButton *seletedBtn;
@property (strong,nonatomic) UILabel *titleLB;
@property (assign,nonatomic) BOOL isDown;


- (void)createUI;

- (void)setContentTitle:(NSString *)title unitPrice:(NSString *)price percent:(CGFloat)percent;

@end
