//
//  PMUserInfos.h
//  changeViewController
//
//  Created by pmit on 14/12/16.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GetMyLocation.h"

@interface PMUserInfos : NSObject
@property(nonatomic,strong)NSMutableDictionary *userDataDic;
@property(nonatomic,copy)NSString *userId;
@property(nonatomic,copy)NSString *defaultArea;
@property(nonatomic,assign)NSInteger isReseller;
@property(nonatomic,copy)NSString *PM_SID;
@property (copy,nonatomic) NSString *myLocation;
@property (assign,nonatomic) double loginTime;


+ (PMUserInfos *)shareUserInfo;
- (void)logout;
- (BOOL)checkPMSIDTime;
@end
