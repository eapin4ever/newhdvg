//
//  PMProductStoreCell.m
//  changeViewController
//
//  Created by pmit on 15/1/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMProductStoreCell.h"

#define itemHeight 20
#define itemY 45
#define rowHeight HeightRate(267)

@implementation PMProductStoreCell
{
    UILabel *_titleLB;
    UIButton *_locationBtn;
    UILabel *_sellNumLB;
    UILabel *_rateLB;
    UILabel *_priceLB;
}

- (void)createUI
{
    if (!_iv)
    {
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(25), 15, WidthRate(200), WidthRate(200))];
        _iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv];
        
        _titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50) + WidthRate(200), 15, WIDTH - WidthRate(50) - WidthRate(200) - 70, 45)];
        _titleLB.numberOfLines = 2;
        _titleLB.font = [UIFont systemFontOfSize:13.0f];
        _titleLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:_titleLB];
        
        self.addToShopBtn = [[YBShareBtn alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(130), 0, WidthRate(130), WidthRate(200) + 50)];
        [self.addToShopBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
        [self.addToShopBtn setTitle:@"添加到店铺" forState:UIControlStateNormal];
        [self.addToShopBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.contentView addSubview:self.addToShopBtn];
        
        self.detailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.detailBtn.frame = CGRectMake(0, 0, WIDTH - WidthRate(130), WidthRate(200));
        [self.contentView addSubview:self.detailBtn];
        
        UIView *messageView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(50) + WidthRate(200), 65, WIDTH - WidthRate(50) - WidthRate(200) - WidthRate(130), WidthRate(200) - 10)];
        UILabel *perPriceTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, messageView.bounds.size.width / 2, 20)];
        perPriceTitleLB.text = @"单价";
        perPriceTitleLB.textAlignment = NSTextAlignmentCenter;
        perPriceTitleLB.textColor = [UIColor lightGrayColor];
        perPriceTitleLB.font = [UIFont systemFontOfSize:12.0f];
        [messageView addSubview:perPriceTitleLB];
        
        CALayer *line1 = [CALayer layer];
        line1.backgroundColor = [UIColor lightGrayColor].CGColor;
        line1.frame = CGRectMake(0, 20, messageView.frame.size.width, 0.5);
        [messageView.layer addSublayer:line1];
        
        CALayer *line2 = [CALayer layer];
        line2.backgroundColor = [UIColor lightGrayColor].CGColor;
        line2.frame = CGRectMake(messageView.frame.size.width / 2, 20, 0.5, 50);
        [messageView.layer addSublayer:line2];
        
        UILabel *persentTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(messageView.bounds.size.width / 2, 20, messageView.bounds.size.width / 2, 20)];
        persentTitleLB.text = @"佣金";
        persentTitleLB.textAlignment = NSTextAlignmentCenter;
        persentTitleLB.textColor = [UIColor lightGrayColor];
        persentTitleLB.font = [UIFont systemFontOfSize:12.0f];
        [messageView addSubview:persentTitleLB];

        
        UILabel *saleTotalTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, messageView.bounds.size.width / 2, 20)];
        saleTotalTitleLB.text = @"销量";
        saleTotalTitleLB.textAlignment = NSTextAlignmentCenter;
        saleTotalTitleLB.textColor = [UIColor lightGrayColor];
        saleTotalTitleLB.font = [UIFont systemFontOfSize:12.0f];
        [messageView addSubview:saleTotalTitleLB];
        
        _priceLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, messageView.bounds.size.width / 2, 30)];
        _priceLB.font = [UIFont systemFontOfSize:15.0f];
        _priceLB.textColor = HDVGRed;
        _priceLB.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:_priceLB];
        
        _rateLB = [[UILabel alloc] initWithFrame:CGRectMake(messageView.bounds.size.width / 2, 40, messageView.bounds.size.width / 2, 30)];
        _rateLB.font = [UIFont systemFontOfSize:15.0f];
        _rateLB.textColor = HDVGRed;
        _rateLB.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:_rateLB];
        
        _sellNumLB = [[UILabel alloc] initWithFrame:CGRectMake(messageView.bounds.size.width / 2 ,0, messageView.bounds.size.width / 2, 20)];
        _sellNumLB.font = [UIFont systemFontOfSize:15.0f];
        _sellNumLB.textColor = HDVGRed;
        _sellNumLB.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:_sellNumLB];
    
        [self.contentView addSubview:messageView];
  
    }
}

- (void)setContentTitle:(NSString *)title location:(NSString *)location sellNum:(id)num rate:(id)rate price:(id)price
{
    _titleLB.text = title;
    [_locationBtn setTitle:location forState:UIControlStateNormal];
    
    NSMutableAttributedString *noteStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.2f",[price doubleValue]]];
    NSRange litteRange = NSMakeRange([[noteStr1 string] rangeOfString:@"."].location,3);
    [noteStr1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0f] range:NSMakeRange(0, 1)];
    [noteStr1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0f] range:litteRange];
    _priceLB.attributedText = noteStr1;
    
    NSMutableAttributedString *noteStr2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.2lf",[rate doubleValue]]];
    NSRange litteRange2 = NSMakeRange([[noteStr2 string] rangeOfString:@"."].location,3);
    [noteStr2 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0f] range:NSMakeRange(0, 1)];
    [noteStr2 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0f] range:litteRange2];
    _rateLB.attributedText = noteStr2;
    
    _sellNumLB.text = [NSString stringWithFormat:@"%ld",(long)[num integerValue]];
    
}


@end
