//
//  YBDetailCell.h
//  changeViewController
//
//  Created by EapinZhang on 15/4/3.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBDetailCell : UITableViewCell

@property (strong,nonatomic) NSDictionary *detailDic;

- (void)createUI;
@end
