//
//  PMRepairGetAreaID.m
//  changeViewController
//
//  Created by pmit on 15/7/21.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMRepairGetAreaID.h"

@implementation PMRepairGetAreaID

+ (NSString *)getAreaIDByArea:(NSString *)area
{
    NSString *provinceKey = @"";
    NSString *areaId = @"";
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"area.json" ofType:nil];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *jsonDic =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    NSDictionary *provinceDic = [jsonDic objectForKey:@"area0"];
    NSArray *provinceAllValue = [provinceDic allValues];
    NSArray *provinceAllKey = [provinceDic allKeys];
    NSString *areaProvinceValue = @"";
    for (NSString *province in provinceAllValue)
    {
        if ([area containsString:province])
        {
            areaProvinceValue = province;
            break;
        }
    }
    
    for (NSString *thisKey in provinceAllKey)
    {
        NSString *thisValue = [provinceDic objectForKey:thisKey];
        if ([thisValue isEqualToString:areaProvinceValue])
        {
            provinceKey = thisKey;
            break;
        }
    }
    
    NSString *cityKey = @"";
    if ([area containsString:@"北京"] || [area containsString:@"上海"] || [area containsString:@"天津"] || [area containsString:@"重庆"])
    {
        NSDictionary *cityDic = [jsonDic objectForKey:@"area1"];
        NSArray *proviceCityArr = [cityDic objectForKey:provinceKey];
        cityKey = [[proviceCityArr firstObject] lastObject];
    }
    else
    {
        NSDictionary *cityDic = [jsonDic objectForKey:@"area1"];
        NSArray *proviceCityArr = [cityDic objectForKey:provinceKey];
        for (NSArray *cityArr in proviceCityArr)
        {
            NSString *cityName = [cityArr firstObject];
            if ([area containsString:cityName])
            {
                cityKey = [cityArr lastObject];
                break;
            }
        }
    }
    
    NSDictionary *quDic = [jsonDic objectForKey:@"area2"];
    if ([area containsString:@"北京"] || [area containsString:@"上海"] || [area containsString:@"天津"] || [area containsString:@"重庆"])
    {
        NSArray *cityQuArr = [quDic objectForKey:cityKey];
        for (NSArray *quArr in cityQuArr)
        {
            NSString *quName = [quArr firstObject];
            if ([area containsString:quName])
            {
                areaId = [quArr lastObject];
                break;
            }
        }
    }
    else
    {
        NSArray *cityQuArr = [quDic objectForKey:cityKey];
        if (cityQuArr.count == 0)
        {
            areaId = cityKey;
        }
        else
        {
            for (NSArray *quArr in cityQuArr)
            {
                NSString *quName = [quArr firstObject];
                if ([area containsString:quName])
                {
                    areaId = [quArr lastObject];
                    break;
                }
            }
        }
    }
    
    return areaId;
}

@end
