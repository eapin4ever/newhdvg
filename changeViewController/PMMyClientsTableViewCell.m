//
//  PMMyClientsTableViewCell.m
//  changeViewController
//
//  Created by P&M on 15/1/21.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMMyClientsTableViewCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMMyClientsTableViewCell
{
    // 我的客户
    UILabel *clientsName;
    UILabel *tradeNumber;
    UILabel *tradeMoney;
}

#pragma mark - 我的客户
- (void)setMyClientsDataUI
{
    if (!clientsName) {
        // 客户昵称
        clientsName = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 7, WidthRate(230), 30)];
        clientsName.backgroundColor = [UIColor clearColor];
        clientsName.textColor = RGBA(150, 150, 150, 1);
        clientsName.textAlignment = NSTextAlignmentCenter;
        clientsName.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:clientsName];
        
        // 交易额
        tradeNumber = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(260), 7, WidthRate(230), 30)];
        tradeNumber.backgroundColor = [UIColor clearColor];
        tradeNumber.textColor = RGBA(186, 0, 14, 1);
        tradeNumber.textAlignment = NSTextAlignmentCenter;
        tradeNumber.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:tradeNumber];
        
        // 佣金总额
        tradeMoney = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(490), 7, WidthRate(230), 30)];
        tradeMoney.backgroundColor = [UIColor clearColor];
        tradeMoney.textColor = RGBA(186, 0, 14, 1);
        tradeMoney.textAlignment = NSTextAlignmentCenter;
        tradeMoney.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:tradeMoney];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

// 设置我的客户昵称、付款笔数、交易额数据
- (void)setNickname:(NSString *)nickname setTradeNum:(NSInteger)tradeNum setBrokerage:(double)brokerage
{
    clientsName.text = nickname;
    tradeNumber.text = [NSString stringWithFormat:@"%ld", (long)tradeNum];
    tradeMoney.text = [NSString stringWithFormat:@"¥%.2lf", brokerage];
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
