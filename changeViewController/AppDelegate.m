//
//  AppDelegate.m
//  changeViewController
//
//  Created by wallace on 14/11/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "AppDelegate.h"
#import <AFNetworkActivityIndicatorManager.h>
#import "PMNetworking.h"
#import <ShareSDK/ShareSDK.h>

#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"
#import "WeiboApi.h"
#import "WeiboApiObject.h"
#import "PMUserInfos.h"
#import "BaiduMobStat.h"
#import "PMMyPhoneInfo.h"
#import "Reachability.h"
#import "MasterViewController.h"
#import "PMInitViewController.h"
#import "WXApi.h"
#import "PMInitViewController.h"
#import "PMApplication.h"
#import <CoreData/CoreData.h>
#import "MyCenterViewController.h"
#import "MD5Util.h"
#import "FeedbackViewController.h"
#import "CatchObject.h"
#import <AVFoundation/AVFoundation.h>


#define IosAppVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define BAIDU_TONGJI_APPKEY @"af3c62073b"
#define WX_APPID @"wx76f7dad6f35cd8fc"

@interface AppDelegate () <UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,WXApiDelegate>

@property (strong,nonatomic) NSString *updateUrl;
@property (strong,nonatomic) UIAlertView *updateAlert;
@property (strong,nonatomic) NSArray *contentArr;
@property (strong,nonatomic) UITableView *alertTableView;
@property (strong,nonatomic) UIView *backView;
@property (strong,nonatomic) NSString *newsVersion;
@property (strong,nonatomic) UIImageView *noNetIV;
@property (strong,nonatomic) UILabel *noNetLB;
@property (strong,nonatomic) UIButton *againBtn;
@property (assign,nonatomic) NSInteger retryCount;
@property (strong,nonatomic) UIView *waitingView;
@end

static AppDelegate *_appDelegate;
@implementation AppDelegate

+ (AppDelegate *)shareAppDelegate
{
    return _appDelegate;
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    UIViewController *vc = [[UIViewController alloc] init];
//    self.window.rootViewController = vc;
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app checkClientVersionNotUpdate:nil];
    
    MasterViewController *master = [[MasterViewController alloc] init];
    PMNavigationController *navi = [[PMNavigationController alloc] initWithRootViewController:master];
    [UIApplication sharedApplication].statusBarHidden = NO;
    self.window.rootViewController = navi;
    
//    UIView *waitingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
//    imageView.image = [UIImage imageNamed:@"rootLoading"];
//    [waitingView addSubview:imageView];
//    self.waitingView = waitingView;
//    [self.window addSubview:waitingView];
    [self.window makeKeyAndVisible];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidTimeout:) name:kApplicationDidTimeoutNotification object:nil];
    
    //-----------------tao-------------------
    //崩溃数据收集
    BaiduMobStat* statTracker = [BaiduMobStat defaultStat];
    statTracker.enableExceptionLog = YES; // 是否允许截获并发送崩溃信息，请设置YES或者NO
    statTracker.logStrategy = BaiduMobStatLogStrategyAppLaunch;//根据开发者设定的发送策略,发送日志
    //    statTracker.logSendInterval = 1;  //为1时表示发送日志的时间间隔为1小时,当logStrategy设置为BaiduMobStatLogStrategyCustom时生效
    statTracker.logSendWifiOnly = NO; //是否仅在WIfi情况下发送日志数据，默认为NO，即在任何网络都发送日志
    statTracker.sessionResumeInterval = 60;//设置应用进入后台再回到前台为同一次session的间隔时间[0~600s],超过600s则设为600s，默认为30s
    statTracker.shortAppVersion  = IosAppVersion; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
    statTracker.enableDebugOn = YES; //调试的时候打开，会有log打印，发布时候关闭
    
    [statTracker startWithAppId:BAIDU_TONGJI_APPKEY];//设置您在mtj网站上添加的app的appkey,此处AppId即为应用的appKey
    //-----------------tao-------------------
    
    self.retryCount = 0;
    
    //保单例
    _appDelegate = self;
    
    [self createCrashView];
    
    //检查版本更新
//    [self checkClientVersionNotUpdate:nil];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    //创建缓存
    NSURLCache *URLCache = [NSURLCache sharedURLCache];
    [NSURLCache setSharedURLCache:URLCache];
    
    //网络活动指示器
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    
    //1.初始化ShareSDK应用
    [ShareSDK registerApp:@"528c08745f85"];
    

    //2. 初始化社交平台
    //2.1 代码初始化社交平台的方法
    [self initializePlat];
    
    [BPush setupChannel:launchOptions];

    
    
    [BPush setDelegate:self];// 必须。参数对象必须实现onMethod: response:方法，本示例中为self
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)categories:nil]];
        
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        //这里还是原来的代码
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    [WXApi registerApp:WX_APPID];
    
    
    [NSTimer scheduledTimerWithTimeInterval:30 * 60 target:self selector:@selector(reCheck:) userInfo:nil repeats:YES];
    
    return YES;
}

- (void)checkClientVersionNotUpdate:(finishAction)notUpdate
{
    /**
     *  如果有版本更新，用户打开程序时提示用户更新
     */
    //获取版本号
    NSString *appVersionStr = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    
    NSString *type = @"2-1-1";
    
    NSDictionary *versionDict = @{@"versionNo":appVersionStr,@"type":type};
    
    PMNetworking *networking = [PMNetworking defaultNetworking];
    
    [networking request:PMRequestStateCheckSysVersion WithParameters:versionDict callBackBlock:^(NSDictionary *dict) {
        if([[dict objectNullForKey:@"success"] integerValue] == 1)
        {
            if ([dict isKindOfClass:[NSDictionary class]])
            {
                if ([[dict objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *dataDic = [dict objectNullForKey:@"data"];
                    self.updateUrl = [dataDic objectNullForKey:@"updateUrl"];
                    
                    NSString *upDateTitle = [NSString stringWithFormat:@"v%@ 版本升级提示",[dataDic objectNullForKey:@"versionNo"]];
                    NSString *content = [dataDic objectNullForKey:@"content"];
                    self.newsVersion = [dataDic objectNullForKey:@"versionNo"];
                    self.contentArr = [content componentsSeparatedByString:@"\n"];
                    if ([[dataDic objectNullForKey:@"isUpdate"] boolValue] == YES)
                    {
                        [self createAlertView:upDateTitle AndContent:[dataDic objectNullForKey:@"content"] IsMust:YES];
                    }
                    else
                    {
                        [self createAlertView:upDateTitle AndContent:[dataDic objectNullForKey:@"content"] IsMust:NO];
                    }
                }
            }
        }
        else
        {
            if(notUpdate != nil)
                notUpdate();
            [self.waitingView removeFromSuperview];
            MasterViewController *master = [[MasterViewController alloc] init];
            PMNavigationController *navi = [[PMNavigationController alloc] initWithRootViewController:master];
            [UIApplication sharedApplication].statusBarHidden = NO;
            self.window.rootViewController = navi;
        }
    }showIndicator:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1 && alertView.tag == 1001) {
        NSString *urlString = [NSString stringWithFormat:@"itms-services:///?action=download-manifest&url=%@",self.updateUrl];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }
    else if (alertView.tag == 1800)
    {
        if ([PMUserInfos shareUserInfo].PM_SID)
        {
            [[MasterViewController defaultMasterVC] userLogout];
        }
        [[MasterViewController defaultMasterVC] transitionToLoginVC];
    }
}

#pragma mark - 初始化分享平台
//初始化社交平台
- (void)initializePlat
{
    [ShareSDK connectWeChatWithAppId:@"wx76f7dad6f35cd8fc"
                           appSecret:@"d68ce2d40651b92902bb472142c06822"
                           wechatCls:[WXApi class]];
    
    ///加减 QQ开发者：APP ID:1103955889  APP KEY:myujdzORFHUs0HTf  URL Schemes(QQ+AppID16进制) QQ41CD07B1
    //添加QQ空间应用  注册网址  http://connect.qq.com/intro/login/
//    [ShareSDK connectQZoneWithAppKey:@"1103955889"
//                           appSecret:@"myujdzORFHUs0HTf"
//                   qqApiInterfaceCls:[QQApiInterface class]
//                     tencentOAuthCls:[TencentOAuth class]];
    
    //添加QQ应用  注册网址  http://open.qq.com/
    [ShareSDK connectQQWithQZoneAppKey:@"1103955889"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    //添加腾讯微博应用 注册网址 http://dev.t.qq.com
//    [ShareSDK connectTencentWeiboWithAppKey:@"801558629"
//                                  appSecret:@"6b3764d7daa55b03566d39fa45e0e01f"
//                                redirectUri:@"http://www.hdvg.tv/html/download.html"
//                                   wbApiCls:[WeiboApi class]];
    
    
    ///加减的新浪appKey 530375133 appSecret 502fd9cc7098179e06e4f8bef2b088fc
    //添加新浪微博应用 注册网址 http://open.weibo.com
//    [ShareSDK connectSinaWeiboWithAppKey:@"530375133"
//                               appSecret:@"502fd9cc7098179e06e4f8bef2b088fc"
//                             redirectUri:@"http://weigou.hdvg.tv/html/index.html"];
    //当使用新浪微博客户端分享的时候需要按照下面的方法来初始化新浪的平台
//    [ShareSDK  connectSinaWeiboWithAppKey:@"530375133"
//                                appSecret:@"502fd9cc7098179e06e4f8bef2b088fc"
//                              redirectUri:@"http://www.hdvg.tv/html/index.html"
//                              weiboSDKCls:[WeiboSDK class]];
    
//    连接拷贝
    [ShareSDK connectCopy];
//    连接短信分享
    [ShareSDK connectSMS];
    
}



#pragma mark - 微信分享需要的两个方法
- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [WXApi handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    if([sourceApplication rangeOfString:@"sina"].length>0)
        return [ShareSDK handleOpenURL:url sourceApplication:sourceApplication annotation:annotation wxDelegate:self];
    
    return  [WXApi handleOpenURL:url delegate:self];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 推送
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"deviceToken-->%@",deviceToken);
    [BPush registerDeviceToken:deviceToken]; // 必须
    
    [BPush bindChannel]; // 必须。可以在其它时机调用，只有在该方法返回（通过onMethod:response:回调）绑定成功时，app才能接收到Push消息。一个app绑定成功至少一次即可（如果access token变更请重新绑定）。
    
    NSLog(@"绑定成功");
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void) onMethod:(NSString*)method response:(NSDictionary*)data {
    NSDictionary* res = [[NSDictionary alloc] initWithDictionary:data];
        if ([BPushRequestMethod_Bind isEqualToString:method])
        {
        //从字典里取值
            NSString *appid = [res valueForKey:BPushRequestAppIdKey];
            NSString *userid = [res valueForKey:BPushRequestUserIdKey];
            NSString *channelid = [res valueForKey:BPushRequestChannelIdKey];
            int returnCode = [[res valueForKey:BPushRequestErrorCodeKey] intValue];
        
            if (returnCode == BPushErrorCode_Success)
            {
            
            
                // 在内存中备份，以便短时间内进入可以看到这些值，而不需要重新bind
                self.appId = appid;
                self.channelId = channelid;
                self.userId = userid;
            
                [[NSUserDefaults standardUserDefaults] setObject:userid forKey:@"BAIDUNOTIFICATION_USERID"];
                [[NSUserDefaults standardUserDefaults] setObject:channelid forKey:@"BAIDUNOTIFICATION_CHANNELID"];
                
                
            
            } else if ([BPushRequestMethod_Unbind isEqualToString:method])
            {
                int returnCode = [[res valueForKey:BPushRequestErrorCodeKey] intValue];
                if (returnCode == BPushErrorCode_Success) {
                
                }
            }
        }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSMutableArray *pushArray = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pushMessage"] mutableCopy];
    if (!pushArray) {
        pushArray = [NSMutableArray array];
    }
    NSString *alert = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//    NSString *alertTitle = [userInfo objectNullForKey:@"title"];
    if (application.applicationState == UIApplicationStateActive) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"系统消息" message:[NSString stringWithFormat:@"%@", alert] delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil];
        [alertView show];
    }
    [application setApplicationIconBadgeNumber:0];
    NSDictionary *dic = @{@"title":@"系统消息",@"message":alert,@"time":[NSDate new]};
    [pushArray insertObject:dic atIndex:0];
    [[NSUserDefaults standardUserDefaults] setValue:pushArray forKey:@"pushMessage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil userInfo:nil];
    
    [BPush handleNotification:userInfo]; // 可选
}

- (void)onResp:(BaseResp *)resp
{
    if ([resp isKindOfClass:[PayResp class]])
    {
        PayResp *response = (PayResp *)resp;
        if (response.errCode == WXSuccess)
        {
            if (self.myDelegate && [self.myDelegate respondsToSelector:@selector(successPay)])
            {
                [self.myDelegate successPay];
            }
        }
        else if (response.errCode == WXErrCodeUserCancel)
        {
            if (self.myDelegate && [self.myDelegate respondsToSelector:@selector(failPay:)])
            {
                [self.myDelegate failPay:response.errCode];
            }
        }
        else
        {
            NSLog(@"error --> %@,%@",@(response.errCode),response.errStr);
            if (self.myDelegate && [self.myDelegate respondsToSelector:@selector(failPay:)])
            {
                [self.myDelegate failPay:response.errCode];
            }
        }
    }
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"Error:%@",str);
}

#pragma mark - 百度云绑定
- (void)bindBaiduyun
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"BAIDUNOTIFICATION_USERID"] != nil)
    {
        NSDictionary *param = @{@"bdyUserId":[[NSUserDefaults standardUserDefaults] objectForKey:@"BAIDUNOTIFICATION_USERID"],@"bdyChannelId":[[NSUserDefaults standardUserDefaults] objectForKey:@"BAIDUNOTIFICATION_CHANNELID"],@"clientType":@"4",@"PM_SID":[PMUserInfos shareUserInfo].PM_SID};
        [[PMNetworking defaultNetworking] request:PMRequestStateGetPushLog WithParameters:param callBackBlock:^(NSDictionary *dic) {
            if ([dic[@"success"] intValue] == 1)
            {
                
            }
            else
            {
//                NSString *message = dic[@"message"];
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"绑定失败" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//                [alertView show];
            }
        }showIndicator:NO];
    }
}

#pragma mark - 判断是否有网络
- (BOOL)isNetConnect
{
    BOOL isConnect = YES;
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    switch ([reachability currentReachabilityStatus]) {
        case NotReachable:
            isConnect = NO;
            break;
        case ReachableViaWiFi:
            isConnect = YES;
            break;
        case ReachableViaWWAN:
            isConnect = YES;
            break;
        default:
            break;
    }
    
    return isConnect;
}

#pragma mark - 创建没有网络界面
#pragma mark - 创建没有网络UI
- (void)createNoNetUI
{
    UIView *noNetView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.noNetView = noNetView;
    noNetView.backgroundColor = [UIColor whiteColor];
    UIImageView *noNetIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, WIDTH, noNetView.bounds.size.height / 2)];
    self.noNetIV = noNetIV;
    noNetIV.image = [UIImage imageNamed:@"noNet"];
    noNetIV.contentMode = UIViewContentModeScaleAspectFit;
    [noNetView addSubview:noNetIV];
    
    UILabel *noNetLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50), self.noNetIV.frame.origin.y + self.noNetIV.frame.size.height + 20, WIDTH - WidthRate(100), HeightRate(70))];
    self.noNetLB = noNetLB;
    noNetLB.text = @"抱歉，当前网络无法连接!!!";
    noNetLB.font = [UIFont systemFontOfSize:18.0f];
    noNetLB.textAlignment = NSTextAlignmentCenter;
    [noNetView addSubview:noNetLB];
    
    UIButton *againBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    againBtn.frame = CGRectMake(WidthRate(50), noNetLB.frame.origin.y + noNetLB.frame.size.height + 20, WIDTH - WidthRate(100), HeightRate(100));
    self.againBtn = againBtn;
    self.againBtn.hidden = NO;
    [againBtn setTitle:@"重试" forState:UIControlStateNormal];
    [againBtn setTitleColor:HDVGRed forState:UIControlStateNormal];
    [againBtn.layer setBorderColor:HDVGRed.CGColor];
    againBtn.layer.cornerRadius = 4.0f;
    [againBtn.layer setBorderWidth:1.0f];
    againBtn.tag = 20;
    [againBtn addTarget:self action:@selector(netClick:) forControlEvents:UIControlEventTouchUpInside];
    [noNetView addSubview:againBtn];
    
    self.noNetView = noNetView;
    self.noNetView.hidden = YES;
    [self.window addSubview:self.noNetView];
    
    /*
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.center = CGPointMake(WIDTH / 2, noNetView.bounds.size.height / 2 + HeightRate(250));
    backBtn.bounds = (CGRect){CGPointZero,{WIDTH - WidthRate(200),HeightRate(70)}};
    [backBtn setTitle:@"返回" forState:UIControlStateNormal];
    [backBtn setBackgroundColor:[UIColor redColor]];
    backBtn.tag = 30;
    [backBtn addTarget:self action:@selector(netClick:) forControlEvents:UIControlEventTouchUpInside];
    [noNetView addSubview:backBtn];
    self.noNetView = noNetView;
    self.noNetView.hidden = YES;
    [self.window addSubview:self.noNetView];
     */
}

- (void)netClick:(UIButton *)button
{
    if (button.tag == 20)
    {
        if (self.isNetConnect)
        {
            self.noNetView.hidden = YES;
            PMNetworking *netWoring = [PMNetworking defaultNetworking];
            [MasterViewController defaultMasterVC].view.userInteractionEnabled = YES;
            if (netWoring.retry) {
                netWoring.retry();
            }
        }
        else {
            self.retryCount ++;
            
            if (self.retryCount == 3) {
                
                self.againBtn.hidden = YES;
                self.noNetLB.hidden = YES;
                
                NSString *string = @"我们已经很努力为你连接网络了，但还是没有找到可用网络，请陛下查看当前网络状态";
                
                UILabel *textLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50), self.noNetIV.frame.origin.y + self.noNetIV.frame.size.height + 20, WIDTH - WidthRate(100), HeightRate(180))];
                textLab.backgroundColor = [UIColor clearColor];
                textLab.textAlignment = NSTextAlignmentLeft;
                textLab.font = [UIFont systemFontOfSize:18.0f];
                self.noNetLB = textLab;
                self.noNetLB.text = string;
                self.noNetLB.numberOfLines = 0;
                [self.noNetView addSubview:textLab];
                
                UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                backBtn.frame = CGRectMake(WidthRate(50), self.noNetLB.frame.origin.y + self.noNetLB.frame.size.height + 20, WIDTH - WidthRate(100), HeightRate(100));
                //backBtn.center = CGPointMake(WIDTH / 2, self.noNetLB.bounds.size.height / 2 + HeightRate(250));
                //backBtn.bounds = (CGRect){CGPointZero,{WIDTH - WidthRate(200),HeightRate(70)}};
                [backBtn setTitle:@"返回" forState:UIControlStateNormal];
                [backBtn setTitleColor:HDVGRed forState:UIControlStateNormal];
                [backBtn.layer setBorderColor:HDVGRed.CGColor];
                backBtn.layer.cornerRadius = 4.0f;
                [backBtn.layer setBorderWidth:1.0f];
                backBtn.tag = 30;
                [backBtn addTarget:self action:@selector(netClick:) forControlEvents:UIControlEventTouchUpInside];
                [self.noNetView addSubview:backBtn];
                //self.noNetView.hidden = YES;
                [self.window addSubview:self.noNetView];
            }
        }
        
    }
    else
    {
        self.noNetView.hidden = YES;
        self.window.userInteractionEnabled = YES;
    }
}

#pragma mark - 微信支付
- (void)weixinPayWithRequsetData:(NSDictionary *)dict
{
    if(dict != nil){
        NSMutableString *stamp  = [dict objectNullForKey:@"timestamp"];
        PayReq *request = [[PayReq alloc] init];
        request.partnerId = [dict objectNullForKey:@"mch_id"];
        request.prepayId = [dict objectNullForKey:@"prepay_id"];
        request.package = @"Sign=WXPay";
        request.nonceStr= [dict objectNullForKey:@"nonce_str"];
        request.timeStamp= stamp.intValue;
        request.sign= [dict objectNullForKey:@"app_sign"];
        [WXApi sendReq:request];
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微信支付提示" message:[NSString stringWithFormat:@"支付失败:%@",[dict objectNullForKey:@"retmsg"]] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
    }
}

#pragma mark - 重写alertView
- (void)createAlertView:(NSString *)title AndContent:(NSString *)content IsMust:(BOOL)isMust
{
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    backView.backgroundColor = [UIColor clearColor];
    self.backView = backView;
    [self.window addSubview:backView];
    
    UIView *backAlphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    backAlphaView.backgroundColor = [UIColor darkGrayColor];
    backAlphaView.alpha = 0.8;
    [backView addSubview:backAlphaView];
    
    UIView *alertView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(100), HeightRate(250), WIDTH - WidthRate(200), HeightRate(700))];
    alertView.backgroundColor = [UIColor whiteColor];
    alertView.alpha = 1.0;
    [backView addSubview:alertView];
    
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, alertView.bounds.size.width, HeightRate(80))];
    titleLB.text = title;
    titleLB.textAlignment = NSTextAlignmentCenter;
    titleLB.font = [UIFont systemFontOfSize:18.0f];
    titleLB.textColor = HDVGFontColor;
    [alertView addSubview:titleLB];
    
   
    
    UITableView *contentTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, HeightRate(100), alertView.frame.size.width, HeightRate(500))];
    contentTableView.delegate = self;
    contentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    contentTableView.delegate = self;
    contentTableView.dataSource = self;
    [alertView addSubview:contentTableView];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = HDVGPageBGGray.CGColor;
    line.frame = CGRectMake(0, HeightRate(80), alertView.frame.size.width, 1);
    [alertView.layer addSublayer:line];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(600), alertView.frame.size.width, HeightRate(100))];
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setTitle:@"立即更新" forState:UIControlStateNormal];
    [sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (isMust)
    {
        sureButton.frame = CGRectMake(0, 0,buttonView.frame.size.width, HeightRate(100));
    }
    else
    {
        sureButton.frame = CGRectMake(buttonView.frame.size.width / 2, 0, buttonView.frame.size.width / 2, HeightRate(100));
    }
    
    sureButton.backgroundColor = HDVGRed;
    sureButton.tag = 1;
    [sureButton addTarget:self action:@selector(upDateVersion:) forControlEvents:UIControlEventTouchUpInside];
    [buttonView addSubview:sureButton];
    
//    CALayer *line1 = [CALayer layer];
//    line1.backgroundColor = HDVGPageBGGray.CGColor;
//    line1.frame = CGRectMake(0, 0, alertView.frame.size.width, 1);
//    [buttonView.layer addSublayer:line1];
//
    if (!isMust)
    {
        CALayer *line2 = [CALayer layer];
        line2.backgroundColor = HDVGPageBGGray.CGColor;
        line2.frame = CGRectMake(buttonView.frame.size.width / 2 + 1, 0, 1, HeightRate(100));
        [buttonView.layer addSublayer:line2];
        
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelButton setTitle:@"继续使用" forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cancelButton.backgroundColor = [UIColor darkGrayColor];
        cancelButton.frame = CGRectMake(0, 0, buttonView.frame.size.width / 2, HeightRate(100));
        [cancelButton addTarget:self action:@selector(upDateVersion:) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.tag = 2;
        [buttonView addSubview:cancelButton];

    }
    
    [alertView addSubview:buttonView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contentArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contentCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"contentCell"];
    }
    
    cell.textLabel.text = self.contentArr[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
    cell.textLabel.numberOfLines = 0;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *content = self.contentArr[indexPath.row];
    CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:15.0f]];
    return size.height + 20;
}

#pragma mark - 升级相关事件
- (void)upDateVersion:(UIButton *)button
{
    if (button.tag == 1)
    {
        self.backView.hidden = YES;
        [[[NSBundle mainBundle] infoDictionary] setValue:self.newsVersion forKey:@"CFBundleVersion"];
        NSString *urlString = [NSString stringWithFormat:@"itms-services:///?action=download-manifest&url=%@",self.updateUrl];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        [self exitApplication];
    }
    else
    {
        [self.backView removeFromSuperview];
        MasterViewController *master = [[MasterViewController alloc] init];
        PMNavigationController *navi = [[PMNavigationController alloc] initWithRootViewController:master];
        [UIApplication sharedApplication].statusBarHidden = NO;
        self.window.rootViewController = navi;
    }
}

- (void)exitApplication {
    
    [UIView animateWithDuration:1.0f animations:^{
        self.window.alpha = 0;
        self.window.window.frame = CGRectMake(0, self.window.bounds.size.width, 0, 0);
    } completion:^(BOOL finished) {
        exit(0);
    }];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.9588.f3.zst.ec.ifjw" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"HomePage" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"HomePageNews.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)applicationDidTimeout:(NSNotification *)notify
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *code = @"";
    NSString *pass = @"";
    NSDictionary *codeAndPassDic = [ud objectForKey:@"codeAndPass"];
    if ([codeAndPassDic objectNullForKey:@"code"] && ![[codeAndPassDic objectNullForKey:@"code"] isEqualToString:@""])
    {
        code = [codeAndPassDic objectNullForKey:@"code"];
    }
    
    if ([codeAndPassDic objectNullForKey:@"password"] && ![[codeAndPassDic objectNullForKey:@"password"] isEqualToString:@""])
    {
        pass = [codeAndPassDic objectNullForKey:@"password"];
    }
    
    if (![code isEqualToString:@""] && ![pass isEqualToString:@""])
    {
        [[PMNetworking defaultNetworking] request:PMRequestStateLogin WithParameters:@{@"code":code,@"password":pass} callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                if ([dic isKindOfClass:[NSDictionary class]])
                {
                    [ud setObject:@{@"code":code,@"password":pass} forKey:@"codeAndPass"];
                    
//                    [(PMApplication *)[UIApplication sharedApplication] resetIdleTimer];
                    
                    NSDictionary *callBackDataDic = [dic objectNullForKey:@"data"];
                    
                    
                    //将数据保存到 UserInfo单例中
                    [PMUserInfos shareUserInfo].userDataDic = [callBackDataDic mutableCopy];
                    //保存是否代言商
                    [PMUserInfos shareUserInfo].isReseller = [[[callBackDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"isReseller"] integerValue];
                    NSString *token = [callBackDataDic objectNullForKey:@"token"];
                    
                    //9.取子字符串，从第n个开始（参数），取到字符串结尾
                    [PMUserInfos shareUserInfo].PM_SID = [token substringFromIndex:7];
                }
            }
        } showIndicator:NO];
    }
}

- (void)createCrashView
{
    UIView *crashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    crashView.backgroundColor = RGBA(234, 234, 234, 1);
    self.crashView = crashView;
    
    UIImageView *crashIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, HEIGHT * 0.2, WIDTH - 50, HEIGHT * 0.2)];
    crashIV.image = [UIImage imageNamed:@"app_crash"];
    crashIV.contentMode = UIViewContentModeScaleAspectFit;
    [self.crashView addSubview:crashIV];
    
    UILabel *tipsLB = [[UILabel alloc] initWithFrame:CGRectMake(0, HEIGHT * 0.4, WIDTH, HEIGHT * 0.1)];
    tipsLB.text = @"小V跑太快了,没电了\n你能为它充下电吗?";
    tipsLB.textAlignment = NSTextAlignmentCenter;
    tipsLB.numberOfLines = 0;
    [self.crashView addSubview:tipsLB];
    
    UIButton *restartBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    restartBtn.center = CGPointMake(self.crashView.center.x, self.crashView.center.y + HEIGHT * 0.06);
    restartBtn.layer.cornerRadius = 3.0f;
    [restartBtn setBackgroundColor:[UIColor lightGrayColor]];
    restartBtn.bounds = CGRectMake(0, 0, 150, 50);
    [restartBtn setTitle:@"充电关闭 小V" forState:UIControlStateNormal];
    [restartBtn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [restartBtn addTarget:self action:@selector(closeV:) forControlEvents:UIControlEventTouchUpInside];
    [self.crashView addSubview:restartBtn];
    
    
    UIButton *adversieBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [adversieBtn setTitle:@"提交错误报告" forState:UIControlStateNormal];
    adversieBtn.center = CGPointMake(self.crashView.center.x, self.crashView.center.y + 70 + HEIGHT * 0.06);
    adversieBtn.bounds = CGRectMake(0, 0, 150, 50);
    [adversieBtn setBackgroundColor:[UIColor lightGrayColor]];
    [adversieBtn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [adversieBtn addTarget:self action:@selector(goToAdervise:) forControlEvents:UIControlEventTouchUpInside];
    [self.crashView addSubview:adversieBtn];
    
    self.crashView.hidden = YES;
    [self.window addSubview:crashView];
}

- (void)closeV:(UIButton *)sender
{
    [UIView animateWithDuration:1.0f animations:^{
        self.window.alpha = 0;
        self.window.window.frame = CGRectMake(0, self.window.bounds.size.width, 0, 0);
    } completion:^(BOOL finished) {
        exit(0);
    }];
}

- (void)goToAdervise:(UIButton *)sender
{
    self.crashView.hidden = YES;
    FeedbackViewController *feedBackVC = [[FeedbackViewController alloc] init];
    [((PMNavigationController *)[MasterViewController defaultMasterVC].currentViewController) pushViewControllerWithNavigationControllerTransition:feedBackVC];
}

NSUncaughtExceptionHandler* _uncaughtExceptionHandler = nil;
void UncaughtExceptionHandler(NSException *exception) {
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
    
    // 异常的堆栈信息
    NSArray *stackArray = [exception callStackSymbols];
    // 出现异常的原因
    NSString *reason = [exception reason];
    // 异常名称
    NSString *name = [exception name];
    
    NSString *syserror = [NSString stringWithFormat:@"mailto://824243782@qq.com?subject=bug报告&body=感谢您的配合!<br><br><br>"
                          "Error Detail:<br>%@<br>--------------------------<br>%@<br>---------------------<br>%@",
                          name,reason,[stackArray componentsJoinedByString:@"<br>"]];
    
    NSURL *url = [NSURL URLWithString:[syserror stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:url];
    return;
}

- (void)reCheck:(NSTimer *)timer
{
    if ([PMUserInfos shareUserInfo].PM_SID)
    {
        [[PMNetworking defaultNetworking] request:PMRequestStateIsLogin WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                
            }
            else
            {
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                NSString *code = @"";
                NSString *pass = @"";
                NSDictionary *codeAndPassDic = [ud objectForKey:@"codeAndPass"];
                if ([codeAndPassDic objectNullForKey:@"code"] && ![[codeAndPassDic objectNullForKey:@"code"] isEqualToString:@""])
                {
                    code = [codeAndPassDic objectNullForKey:@"code"];
                }
                
                if ([codeAndPassDic objectNullForKey:@"password"] && ![[codeAndPassDic objectNullForKey:@"password"] isEqualToString:@""])
                {
                    pass = [codeAndPassDic objectNullForKey:@"password"];
                }
                
                if (![code isEqualToString:@""] && ![pass isEqualToString:@""])
                {
                    [[PMNetworking defaultNetworking] request:PMRequestStateLogin WithParameters:@{@"code":code,@"password":pass} callBackBlock:^(NSDictionary *dic) {
                        if (intSuccess == 1)
                        {
                            if ([dic isKindOfClass:[NSDictionary class]])
                            {
                                [ud setObject:@{@"code":code,@"password":pass} forKey:@"codeAndPass"];
                                
//                                [(PMApplication *)[UIApplication sharedApplication] resetIdleTimer];
                                
                                NSDictionary *callBackDataDic = [dic objectNullForKey:@"data"];
                                
                                
                                //将数据保存到 UserInfo单例中
                                [PMUserInfos shareUserInfo].userDataDic = [callBackDataDic mutableCopy];
                                //保存是否代言商
                                [PMUserInfos shareUserInfo].isReseller = [[[callBackDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"isReseller"] integerValue];
                                NSString *token = [callBackDataDic objectNullForKey:@"token"];
                                
                                //9.取子字符串，从第n个开始（参数），取到字符串结尾
                                [PMUserInfos shareUserInfo].PM_SID = [token substringFromIndex:7];
                            }
                        }
                    } showIndicator:NO];
                }
            }
        } showIndicator:NO];
    }
}

@end
