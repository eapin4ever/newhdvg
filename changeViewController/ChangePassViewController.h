//
//  ChangePassViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 * 忘记密码修改控制器
 */
#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"

@interface ChangePassViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) UIView *changePassView;
@property (strong, nonatomic) UITextField *mobileTextField;
@property (strong, nonatomic) UITextField *authcodeTextField;

@property (strong, nonatomic) UILabel *tipLabel;
@property (strong, nonatomic) UIButton *authcodeButton;
@property (strong, nonatomic) UIButton *commitButton;

@property (strong, nonatomic) PMNetworking *networking;

@end
