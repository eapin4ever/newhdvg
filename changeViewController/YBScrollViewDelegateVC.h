//
//  YBScrollViewDelegateVC.h
//  changeViewController
//
//  Created by ZhangEapin on 15/5/1.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBScrollViewDelegateVC : UIViewController <UIScrollViewDelegate>

+ (YBScrollViewDelegateVC *)defaultDelegate;

@property(nonatomic,weak)UIPageControl *pageControl;
@property(nonatomic,strong)UIScrollView *imageScrollView;

@end
