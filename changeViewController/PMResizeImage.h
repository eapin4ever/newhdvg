//
//  PMResizeImage.h
//  changeViewController
//
//  Created by pmit on 14/12/2.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMResizeImage : NSObject
+ (UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize;

#pragma mark - 从连接得到图片
+ (UIImage *)getImageWithURLString:(NSString *)urlString;
+ (void)getImageWithURLString:(NSString *)urlString imageView:(UIImageView *)imageView;
@end
