//
//  UnReadMessageCell.m
//  changeViewController
//
//  Created by EapinZhang on 15/4/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "UnReadMessageCell.h"
#import "PMMyPhoneInfo.h"

@implementation UnReadMessageCell
{
    UILabel *_titleLB;
    UILabel *_contentLB;
    UILabel *_timeLB;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!_titleLB) {
        _titleLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, WIDTH * 0.6, 20)];
        _titleLB.font = [UIFont systemFontOfSize:15.0f];
        _titleLB.textColor = HDVGFontColor;
        [self.contentView addSubview:_titleLB];
        
        NSString *content = [self.messageDic objectNullForKey:@"content"];
        UIFont *font = [UIFont systemFontOfSize:14.0f];
        NSDictionary *sizeDic = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName, nil];
        CGSize sizeText = [content boundingRectWithSize:CGSizeMake(WIDTH - 20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:sizeDic context:nil].size;
        
        _contentLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, WIDTH - 20, sizeText.height)];
        _contentLB.font = [UIFont systemFontOfSize:14.0f];
        _contentLB.textColor = [UIColor lightGrayColor];
        _contentLB.numberOfLines = 0;
        [self.contentView addSubview:_contentLB];
        
        _timeLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH * 0.6 + 5, 10, WIDTH *0.4 - 20, 20)];
        _timeLB.backgroundColor = [UIColor clearColor];
        _timeLB.font = [UIFont systemFontOfSize:14.0f];
        _timeLB.textColor = HDVGFontColor;
        _timeLB.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_timeLB];
        
    }
}

- (void)getData
{
    _titleLB.text = [self.messageDic objectNullForKey:@"title"];
    _contentLB.text = [self.messageDic objectNullForKey:@"content"];
    _timeLB.text = [self.messageDic objectNullForKey:@"time"];
}

@end
