//
//  NewPassViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "NewPassViewController.h"
#import "PMToastHint.h"

@interface NewPassViewController ()

@end

@implementation NewPassViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"修改密码";
        
        self.view.backgroundColor = HDVGPageBGGray;   // 背景色
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self createSetNewPassUI];
    
    [self createSetNewPassControllerUI];
}

// 创建设置新密码 UI
- (void)createSetNewPassUI
{
    // 创建一个view
    self.setNewPassView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 88)];
    self.setNewPassView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.setNewPassView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.setNewPassView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 44, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.setNewPassView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(0, 88 - 0.5, WIDTH, 0.5);
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.setNewPassView.layer addSublayer:layer3];
    
    // 旧密码
    UILabel *oldPassLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 7, WidthRate(160), 30)];
    oldPassLabel.backgroundColor = [UIColor clearColor];
    oldPassLabel.text = @"当前密码:";
    oldPassLabel.textColor = HDVGFontColor;
    oldPassLabel.textAlignment = NSTextAlignmentCenter;
    oldPassLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.setNewPassView addSubview:oldPassLabel];
    
    self.oldPassTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(210), 7, WIDTH - WidthRate(240), 30)];
    self.oldPassTextField.borderStyle = UITextBorderStyleNone;
    self.oldPassTextField.delegate = self;
    self.oldPassTextField.secureTextEntry = YES;
    self.oldPassTextField.textAlignment = NSTextAlignmentLeft;
    self.oldPassTextField.font = [UIFont systemFontOfSize:14.0f];
    self.oldPassTextField.placeholder = @"请输入旧密码";
    self.oldPassTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.oldPassTextField.returnKeyType = UIReturnKeyDone;
    self.oldPassTextField.keyboardType = UIReturnKeyDefault;
    [self.setNewPassView addSubview:self.oldPassTextField];
    
    // 新密码
    UILabel *newPassLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 51, WidthRate(160), 30)];
    newPassLabel.backgroundColor = [UIColor clearColor];
    newPassLabel.text = @"新  密  码:";
    newPassLabel.textColor = HDVGFontColor;
    newPassLabel.textAlignment = NSTextAlignmentCenter;
    newPassLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.setNewPassView addSubview:newPassLabel];
    
    self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(210), 51, WIDTH - WidthRate(240), 30)];
    self.passwordTextField.borderStyle = UITextBorderStyleNone;
    self.passwordTextField.delegate = self;
    self.passwordTextField.secureTextEntry = YES;
    self.passwordTextField.textAlignment = NSTextAlignmentLeft;
    self.passwordTextField.font = [UIFont systemFontOfSize:14.0f];
    // attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请留下你的联系方式?" attributes:@{NSForegroundColorAttributeName:secColor}];
    self.passwordTextField.placeholder = @"长度为6～16位数字、字母、符号";
    self.passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.passwordTextField.returnKeyType = UIReturnKeyDone;
    self.passwordTextField.keyboardType = UIReturnKeyDefault;
    [self.setNewPassView addSubview:self.passwordTextField];
}

// 创建设置密码控件 UI
- (void)createSetNewPassControllerUI
{
    // 初始化显示密码按钮
    UIButton *showButton = [UIButton buttonWithType:UIButtonTypeCustom];
    showButton.frame = CGRectMake(WIDTH - WidthRate(230), self.setNewPassView.frame.origin.y + 88 + HeightRate(10), WidthRate(190), 18);
    [showButton setImage:[UIImage imageNamed:@"showpass_no.png"] forState:UIControlStateNormal];
    [showButton setImage:[UIImage imageNamed:@"showpass_yes.png"] forState:UIControlStateSelected];
    [showButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [showButton.titleLabel setFont:[UIFont systemFontOfSize:HeightRate(30)]];
    [showButton setTitle:@"显示密码" forState:UIControlStateNormal];
    [showButton setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [showButton addTarget:self action:@selector(showPassButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:showButton];
    
    // 初始化完成设置按钮
    UIBarButtonItem *finishButton = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(finishSetButtonClick:)];
    
    [finishButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR, NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = finishButton;
}

// 按下Done按钮的调用方法，让软键盘隐藏
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.oldPassTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

- (void)showPassButtonClick:(UIButton *)button
{
    // 改变按钮的选中状态
    button.selected = !button.selected;
    if (button.selected) {
        self.passwordTextField.secureTextEntry = NO;
    }
    if (!button.selected) {
        self.passwordTextField.secureTextEntry = YES;
    }
}

- (void)finishSetButtonClick:(id)sender
{
    // 密码不能为空
    if (self.oldPassTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入旧密码"];
        return;
    }
    if ([self.passwordTextField.text isEqual:@""] || self.passwordTextField.text.length < 6 || self.passwordTextField.text.length > 16) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请按要求设置新密码"];
        return;
    }
    
    /**
     *  从服务器获取用户token
     */
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    NSDictionary *dict = userInfos.userDataDic;
    
    NSString *PM_SID = [[dict objectNullForKey:@"token"] substringFromIndex:7];
    
    if (![self.oldPassTextField.text isEqual:@""] && ![self.passwordTextField.text isEqual:@""] && (self.passwordTextField.text.length > 5) && (self.passwordTextField.text.length < 17)) {
        
        /**
         * 用户修改登录密码
         * 参数: modifyPwd:(string)
         */
        NSDictionary *modifyPwd = @{@"PM_SID":PM_SID, @"oldPwd":[MD5Util md5:self.oldPassTextField.text], @"newPwd":[MD5Util md5:self.passwordTextField.text]};
        self.networking = [PMNetworking defaultNetworking];
        
        __block NSDictionary *callBackDict;
        
        [self.networking request:PMRequestStateSafeModifyPassword WithParameters:modifyPwd callBackBlock:^(NSDictionary *dict) {
            
            callBackDict = dict;
            if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"修改登录密码成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                
                //将新密码保存到userDefault中
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                NSMutableDictionary *muDic = [[ud objectForKey:@"codeAndPass"] mutableCopy];
                [muDic setValue:[MD5Util md5:self.passwordTextField.text] forKey:@"password"];
                [ud setObject:muDic forKey:@"codeAndPass"];
                
                alertView.tag = 1;
                [alertView show];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }showIndicator:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
