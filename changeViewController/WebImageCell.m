//
//  WebImageCell.m
//  changeViewController
//
//  Created by ZhangEapin on 15/4/26.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "WebImageCell.h"
#import "PMMyPhoneInfo.h"
#import "YTGifActivityIndicator.h"

@implementation WebImageCell
{
    YTGifActivityIndicator *_indicator;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (self.imageWeb)
    {
        [self.imageWeb removeFromSuperview];
    }
    UIWebView *imageWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.rowHeight)];
    self.imageWeb = imageWeb;
    imageWeb.scrollView.scrollEnabled = NO;
    [self.contentView addSubview:imageWeb];
    
    
    
    
}

- (void)loadWeb
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
    [self.imageWeb loadRequest:request];
}



@end
