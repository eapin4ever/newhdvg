//
//  HomeData.m
//  changeViewController
//
//  Created by pmit on 15/7/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "HomeData.h"


@implementation HomeData

@dynamic discount;
@dynamic headerADName;
@dynamic homeImage;
@dynamic isDiscount;
@dynamic isHeadeAd;
@dynamic isProduct;
@dynamic isScroll;
@dynamic productId;
@dynamic productName;
@dynamic productPrice;
@dynamic productSection;
@dynamic productTitle;
@dynamic rawPrice;

@end
