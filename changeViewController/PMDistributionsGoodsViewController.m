//
//  PMDistributionsGoodsViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMDistributionsGoodsViewController.h"
#import "PMProductStoreViewController.h"
#import "PMDistubitionStoreViewController.h"

@interface PMDistributionsGoodsViewController () <UITextFieldDelegate>
@property(nonatomic,strong)UILabel *redLine;
@property(nonatomic,strong)UITableViewController *currentVC;
@property(nonatomic,strong)UIButton *upDownGoodsBtn;
@property(nonatomic,strong)UIView *tabBar;
@property (strong,nonatomic) UIButton *goNowBtn;
@property (strong,nonatomic) UIView *searchView;
@property (assign,nonatomic) BOOL isShow;
@property (strong,nonatomic) UISegmentedControl *segment;
@property (strong,nonatomic) UITextField *searchText;

@end

@implementation PMDistributionsGoodsViewController
{
    BOOL transiting;
}
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"代言商品管理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isShow = NO;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(seeSearchView:) name:@"mySearchView" object:nil];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 20, 20);
    [rightBtn setImage:[UIImage imageNamed:@"search_01"] forState:UIControlStateNormal];
    rightBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [rightBtn addTarget:self action:@selector(searchShow:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = searchItem;
    
    [self addChildViewControllers];
    [self initSearchView];
    
    //创建没有数据时的提示页面
    [self createNoResult];
    //检查库存
//    [self checkStock];
    
    [self initSegmentedControl];
    [self initTabBar];
}

- (void)addChildViewControllers
{
    PMCurrentGoodsTableViewController *cg = [[PMCurrentGoodsTableViewController alloc] init];
    [self addChildViewController:cg];
    self.currentVC = cg;
    [self.view addSubview:cg.tableView];
    
    PMUnsoldGoodsTableViewController *ug = [[PMUnsoldGoodsTableViewController alloc] init];
    [self addChildViewController:ug];
}

- (void)initSearchView
{
    UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    searchView.backgroundColor = RGBA(231, 231, 231, 1);
    UITextField *searchText = [[UITextField alloc] initWithFrame:CGRectMake(10, 7, WIDTH - 20 - 60, 26)];
    searchText.backgroundColor = [UIColor whiteColor];
    
    searchText.placeholder = @"请输入要搜索的商品";
    [searchText setValue:[UIFont boldSystemFontOfSize:12] forKeyPath:@"_placeholderLabel.font"];
    searchText.contentHorizontalAlignment = UIControlContentVerticalAlignmentCenter;
    [searchText setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, searchText.bounds.size.width,searchText.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [searchText setValue:NAVTEXTCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    
    [searchText.layer setBorderWidth:0];
    searchText.returnKeyType = UIReturnKeySearch;
    searchText.textColor = [UIColor blackColor];
    searchText.font = [UIFont systemFontOfSize:14];
    
    searchText.delegate = self;
    [searchView addSubview:searchText];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.backgroundColor = [UIColor redColor];
    [searchBtn setTitle:@"Search" forState:UIControlStateNormal];
    searchBtn.frame = CGRectMake(WIDTH - 20 - 70, 7, 70, 26);
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [searchBtn addTarget:self action:@selector(searchGoods:) forControlEvents:UIControlEventTouchUpInside];
    [searchView addSubview:searchBtn];
    
    self.searchText = searchText;
    [self.view addSubview:searchView];
}


#pragma mark - 切换商品/店铺
- (void)initSegmentedControl
{
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:@[@"代言中商品",@"已下架商品"]];
    segment.frame = CGRectMake(0, 0, WIDTH, 40);
    segment.selectedSegmentIndex = 0;
    
    [segment addTarget:self action:@selector(segmentedChangeValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segment];
    
    [segment setTintColor:[UIColor clearColor]];
    //选中时字体颜色
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HDVGRed,NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:15], NSFontAttributeName, nil] forState:UIControlStateSelected];
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor lightGrayColor],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:15], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    segment.backgroundColor = RGBA(244, 244, 244, 1);
    //中间分割线
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(segment.center.x - 0.5, 0, 1, 40)];
    line.backgroundColor = RGBA(228, 228, 228, 1);
    [segment addSubview:line];
    
    //指示条
    self.redLine = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/5/segment.numberOfSegments, 37.5, WIDTH/5*3/segment.numberOfSegments, 2.5)];
    self.redLine.backgroundColor = HDVGRed;
    self.segment = segment;
    [segment addSubview:self.redLine];
}


- (void)segmentedChangeValue:(UISegmentedControl *)sender
{
    //指示条滑动的动画
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.redLine.frame;
        rect.origin.x = WIDTH/5/sender.numberOfSegments + WIDTH/sender.numberOfSegments*sender.selectedSegmentIndex;
        self.redLine.frame = rect;
    }];
    
    [self.searchText resignFirstResponder];
    //transiting是防止快速触发多次动画，导致出现警告
    if(transiting)
    {
        return;
    }
    transiting = YES;
    //更换页面
    [self transitionFromViewController:self.currentVC toViewController:self.childViewControllers[sender.selectedSegmentIndex] duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^{
        //添加动画代码
        [self.view bringSubviewToFront:sender];
        [self.upDownGoodsBtn setTitle:((UIViewController *)self.childViewControllers[sender.selectedSegmentIndex]).title forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        //完成后需要做的事
        self.currentVC = self.childViewControllers[sender.selectedSegmentIndex];
        [self.view bringSubviewToFront:self.tabBar];
        transiting = NO;///
    }];
    
    [self.selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
}


- (void)initTabBar
{
    UIView *tabBar = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 49 - 64, WIDTH, 50)];
    self.tabBar = tabBar;
    tabBar.backgroundColor = TabBarColor;
    [self.view addSubview:tabBar];
    self.tabBar.hidden = NO;
    
    UIButton *selectAllBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    self.selectAllBtn = selectAllBtn;
    selectAllBtn.frame = CGRectMake(-4, 12.5, 80, 24);
    
    [selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
    [selectAllBtn setTitle:@"全选" forState:UIControlStateNormal];
    [selectAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectAllBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    selectAllBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [selectAllBtn addTarget:self action:@selector(clickToSelectAll:) forControlEvents:UIControlEventTouchUpInside];
    [tabBar addSubview:selectAllBtn];
    
    
    UIButton *upDownGoodsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.upDownGoodsBtn = upDownGoodsBtn;
    upDownGoodsBtn.frame = CGRectMake(WidthRate(468), 0, WIDTH - WidthRate(468), 50);
    upDownGoodsBtn.titleLabel.font = [UIFont boldSystemFontOfSize:WidthRate(30)];
    
    [upDownGoodsBtn setTitle:@"下架选中商品" forState:UIControlStateNormal];
    [upDownGoodsBtn setBackgroundColor:HDVGRed];
    [tabBar addSubview:upDownGoodsBtn];
    
    
    [upDownGoodsBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)clickAction:(UIButton *)sender
{
    [(PMCurrentGoodsTableViewController *)self.currentVC unsoldSelectedGoods];
}

- (void)clickToSelectAll:(UIButton *)sender
{
    if (self.segment.selectedSegmentIndex == 0)
    {
        PMCurrentGoodsTableViewController *vc = (PMCurrentGoodsTableViewController *)self.currentVC;
        [vc selectAllRows:sender];
    }
    else
    {
        PMUnsoldGoodsTableViewController *vc = (PMUnsoldGoodsTableViewController *)self.currentVC;
        [vc selectAllRows:sender];
    }
    
    
}

#pragma mark - 检查是否有0库存的商品
- (void)checkStock
{
    
    [[PMNetworking defaultNetworking] request:PMRequestStateShareSelectAllShopProduct WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            //没有下架的商品
        }
        else
        {
            //有下架的商品
        }
        
        //请求代言中的商品
//        PMCurrentGoodsTableViewController *vc1 = self.childViewControllers[0];
//        PMUnsoldGoodsTableViewController *vc2 = self.childViewControllers[1];
//        [vc1 getModelsBySearchString:nil finish:^{
//            [vc2 getModelsBySearchString:nil finish:^{
//                
//            }error:nil];
//        }error:nil];
        
        
    } showIndicator:NO];
}


#pragma mark - 创建没有数据的界面
- (void)createNoResult
{
    UIView *noResultView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, WIDTH, HEIGHT - 104)];
    noResultView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *noResultIV = [[UIImageView alloc] init];
    self.noResultIV = noResultIV;
    noResultIV.contentMode = UIViewContentModeScaleAspectFit;
    [noResultView addSubview:noResultIV];
    
//    UILabel *noResultLB = [[UILabel alloc] initWithFrame:CGRectMake(0, noResultView.bounds.size.height / 2 - HeightRate(100), WIDTH, HeightRate(120))];
//    self.noResultLB = noResultLB;
//    noResultLB.numberOfLines = 0;
//    noResultLB.font = [UIFont systemFontOfSize:16.0f];
//    noResultLB.textAlignment = NSTextAlignmentCenter;
//    noResultLB.textColor = HDVGFontColor;
//    [noResultView addSubview:noResultLB];
    
    UIButton *goNowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    goNowBtn.frame = CGRectMake(WIDTH - (iPhone4s ? WidthRate(390) : WidthRate(370)), noResultView.bounds.size.height / 2 - HeightRate(20), WidthRate(180), WidthRate(60));
    [goNowBtn setBackgroundImage:[UIImage imageNamed:@"dis_go_addButton"] forState:UIControlStateNormal];
    self.goNowBtn = goNowBtn;
    [goNowBtn addTarget:self action:@selector(goToMyKu:) forControlEvents:UIControlEventTouchUpInside];
    [noResultView addSubview:goNowBtn];
    
    self.noResultView = noResultView;
    [self.view insertSubview:noResultView atIndex:1000];
    self.noResultView.hidden = YES;
}

- (void)showNoResultViewWithtitle:(NSString *)title AndIsSold:(NSInteger)isSold
{
    if (isSold == 1) {
        
        [self.view bringSubviewToFront:self.noResultView];
        self.noResultView.hidden = NO;
        //self.noResultLB.text = title;
        
        self.goNowBtn.hidden = NO;
        self.noResultIV.frame = CGRectMake(0, 0, WIDTH, self.noResultView.frame.size.height + 40);
        self.noResultIV.image = [UIImage imageNamed:@"noDistubiton"];
    }
    else if (isSold == 2) {
        
        [self.view bringSubviewToFront:self.noResultView];
        self.noResultView.hidden = NO;
        //self.noResultLB.text = title;
        
        self.tabBar.hidden = YES;
        self.goNowBtn.hidden = YES;
        
        self.noResultIV.frame = CGRectMake(0, 0, WIDTH, self.noResultView.frame.size.height);
        self.noResultIV.image = [UIImage imageNamed:@"dis_goods_null2"];
    }
    else if (isSold == 3) {
        self.noResultView.hidden = YES;
        self.tabBar.hidden = NO;
    }
    else {
        self.noResultView.hidden = YES;
    }
}


#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)goToMyKu:(UIButton *)sender
{
//    PMProductStoreViewController *vc = [[PMProductStoreViewController alloc] init];
//    [vc getModelsWithParam:@{PMSID,@"currentPage":@(1),@"pageSize":@(20),@"areaId":@"440100",@"priceOrder":@"asc",@"xlTotalOrder":@"",@"sellScale":@""} finish:^{
//        [self.navigationController pushViewController:vc animated:YES];
//    } error:^{
//        
//    }];
    PMDistubitionStoreViewController *vc = [[PMDistubitionStoreViewController alloc] init];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];
}

- (void)searchShow:(UIButton *)sender
{
    self.isShow = !self.isShow;
    [self.searchText resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"mySearchView" object:@(self.isShow)];
}

- (void)seeSearchView:(NSNotification *)notification
{
    BOOL isViewShow = [[notification object] boolValue];
    if (isViewShow)
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.searchView.frame = CGRectMake(0, 0, WIDTH, 40);
            self.searchView.backgroundColor = RGBA(231, 231, 231, 1);
            self.segment.frame = CGRectMake(0, 40, WIDTH, 40);
            self.noResultView.frame = CGRectMake(0, 80, WIDTH, HEIGHT - 104);
        }];
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.searchView.frame = CGRectMake(0, 0, WIDTH, 0);
            self.segment.frame = CGRectMake(0, 0, WIDTH, 40);
            self.noResultView.frame = CGRectMake(0, 40, WIDTH, HEIGHT - 104);
        }];
        if (self.segment.selectedSegmentIndex == 0)
        {
             [((PMCurrentGoodsTableViewController *)self.currentVC) getModelsBySearchString:nil finish:nil error:nil];
        }
        else
        {
            [((PMUnsoldGoodsTableViewController *)self.currentVC) getModelsBySearchString:nil finish:nil error:nil];
        }
    }
}

- (void)searchGoods:(UIButton *)sender
{
    [self.searchText resignFirstResponder];
    if (self.segment.selectedSegmentIndex == 0)
    {
        ((PMCurrentGoodsTableViewController *)self.currentVC).isLoad = NO;
        ((PMCurrentGoodsTableViewController *)self.currentVC).isSearch = YES;
        [((PMCurrentGoodsTableViewController *)self.currentVC) getModelsBySearchString:self.searchText.text finish:nil error:nil];
    }
    else
    {
        ((PMUnsoldGoodsTableViewController *)self.currentVC).isLoad = NO;
        ((PMUnsoldGoodsTableViewController *)self.currentVC).isSearch = YES;
        [((PMUnsoldGoodsTableViewController *)self.currentVC) getModelsBySearchString:self.searchText.text finish:nil error:nil];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.searchText resignFirstResponder];
    if (self.segment.selectedSegmentIndex == 0)
    {
        [((PMCurrentGoodsTableViewController *)self.currentVC) getModelsBySearchString:self.searchText.text finish:nil error:nil];
    }
    else
    {
        [((PMUnsoldGoodsTableViewController *)self.currentVC) getModelsBySearchString:self.searchText.text finish:nil error:nil];
    }
    
    return YES;
}


@end
