//
//  LogisticsDetailViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/27.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "LogisticsDetailViewController.h"

@interface LogisticsDetailViewController ()

@end

@implementation LogisticsDetailViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"物流详情";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 初始化物流详情 tableView
    self.detailTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 2000)];
    [self.detailTableView setShowsVerticalScrollIndicator:YES];
    self.detailTableView.delegate = self;
    self.detailTableView.dataSource = self;
    [self.view addSubview:self.detailTableView];
    
    
    // 初始化headerView
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.detailTableView.frame.size.height)];
    headerView.backgroundColor = [UIColor clearColor];
    
    
    // 物流公司
    UILabel *logisticsLabel = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - WidthRate(240)) / 2, HeightRate(30), WidthRate(240), HeightRate(40))];
    logisticsLabel.backgroundColor = [UIColor clearColor];
    logisticsLabel.text = [[[self.logisticsDict objectNullForKey:@"data"] objectNullForKey:@"content"] objectNullForKey:@"companyName"];
    logisticsLabel.textColor = RGBA(120, 119, 125, 1);
    logisticsLabel.textAlignment = NSTextAlignmentCenter;
    logisticsLabel.font = [UIFont systemFontOfSize:16.0f];
    [headerView addSubview:logisticsLabel];
    
    // 运单编号
    UILabel *logisticsNumLabel = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - WidthRate(450)) / 2, HeightRate(70), WidthRate(450), HeightRate(40))];
    logisticsNumLabel.backgroundColor = [UIColor clearColor];
    logisticsNumLabel.text = [NSString stringWithFormat:@"物流单号：%@", [[[self.logisticsDict objectNullForKey:@"data"] objectNullForKey:@"content"] objectNullForKey:@"nu"]];
    logisticsNumLabel.textColor = [UIColor lightGrayColor];
    logisticsNumLabel.textAlignment = NSTextAlignmentCenter;
    logisticsNumLabel.font = [UIFont systemFontOfSize:10.0f];
    [headerView addSubview:logisticsNumLabel];
    
    
    /**
     * 筛选物流详情数据
     */
    NSArray *array = [[[self.logisticsDict objectNullForKey:@"data"] objectNullForKey:@"content"] objectNullForKey:@"data"];
    
    if ([[[self.logisticsDict objectNullForKey:@"data"] objectNullForKey:@"content"] isKindOfClass:[NSDictionary class]] && [[self.logisticsDict objectNullForKey:@"data"] objectNullForKey:@"content"] != nil) {
        
        for (NSInteger i = 0; i < array.count; i++) {
            
            // 中间粉红色分隔点和分隔线
            UIView *pinkView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(360), HeightRate(130) + (HeightRate(130) * i + (HeightRate(30) * i)), WidthRate(30), WidthRate(30))];
            [pinkView.layer setCornerRadius:CGRectGetHeight([pinkView bounds]) / 2];
            pinkView.layer.masksToBounds = YES;
            pinkView.backgroundColor = HDVGRed;
            [headerView addSubview:pinkView];
            
            
            CALayer *layer = [[CALayer alloc] init];
            layer.frame = CGRectMake(WidthRate(375), HeightRate(160) + (HeightRate(160) * i), WidthRate(1), HeightRate(130));
            layer.backgroundColor = HDVGRed.CGColor;
            [headerView.layer addSublayer:layer];
            
            if (i % 2 == 0) {
                // 物流详情地址（右边）
                NSDictionary *dict = [array objectAtIndex:i];
                UILabel *detailAddLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(400), HeightRate(160) + HeightRate(160) * i, WidthRate(320), HeightRate(130))];
                detailAddLabel.backgroundColor = [UIColor clearColor];
                detailAddLabel.text = [dict objectNullForKey:@"context"];
                detailAddLabel.textColor = RGBA(120, 119, 125, 1);
                detailAddLabel.numberOfLines = 0;
                detailAddLabel.textAlignment = NSTextAlignmentLeft;
                detailAddLabel.font = [UIFont systemFontOfSize:12.0f];
                [headerView addSubview:detailAddLabel];
                
                // 签收时间
                UILabel *signTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(400), detailAddLabel.frame.origin.y + HeightRate(125), WidthRate(300), HeightRate(20))];
                signTimeLabel.backgroundColor = [UIColor clearColor];
                signTimeLabel.text = [NSString stringWithFormat:@"时间：%@", [dict objectNullForKey:@"time"]];
                signTimeLabel.textColor = RGBA(120, 119, 125, 1);
                signTimeLabel.textAlignment = NSTextAlignmentLeft;
                signTimeLabel.font = [UIFont systemFontOfSize:9.0f];
                [headerView addSubview:signTimeLabel];
            }
            else {
                
                // 物流详情地址（左边）
                NSDictionary *dict2 = [array objectAtIndex:i];
                UILabel *detailAddLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(160) + HeightRate(160) * i, WidthRate(320), HeightRate(130))];
                detailAddLabel2.backgroundColor = [UIColor clearColor];
                detailAddLabel2.text = [dict2 objectNullForKey:@"context"];
                detailAddLabel2.textColor = RGBA(120, 119, 125, 1);
                detailAddLabel2.numberOfLines = 0;
                detailAddLabel2.textAlignment = NSTextAlignmentCenter;
                detailAddLabel2.font = [UIFont systemFontOfSize:12.0f];
                [headerView addSubview:detailAddLabel2];
                
                // 签收时间
                UILabel *signTimeLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), detailAddLabel2.frame.origin.y + HeightRate(125), WidthRate(300), HeightRate(20))];
                signTimeLabel2.backgroundColor = [UIColor clearColor];
                signTimeLabel2.text = [NSString stringWithFormat:@"时间：%@", [dict2 objectNullForKey:@"time"]];
                signTimeLabel2.textColor = RGBA(120, 119, 125, 1);
                signTimeLabel2.textAlignment = NSTextAlignmentLeft;
                signTimeLabel2.font = [UIFont systemFontOfSize:9.0f];
                [headerView addSubview:signTimeLabel2];
            }
            
            // 插入增加可滚动高度
            self.detailTableView.contentInset = UIEdgeInsetsMake(0, 0, HeightRate(100) * i, 0);
        }
    }
    else {
        
        UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
        noDataView.backgroundColor = [UIColor whiteColor];
        
        UIImageView *noIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noDataView.frame.size.height)];
        noIV.image = [UIImage imageNamed:@"logistics_null"];
        noIV.contentMode = UIViewContentModeScaleAspectFit;
        [noDataView addSubview:noIV];
        
        [self.view insertSubview:noDataView atIndex:1000];
    }
    
    self.detailTableView.tableHeaderView = headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *detailTableView = @"detailCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:detailTableView];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:detailTableView];
    }
    
    if (indexPath.row == 0) {
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
