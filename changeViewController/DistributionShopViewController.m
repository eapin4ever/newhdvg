//
//  DistributionShopViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/8.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#define itemWidth WidthRate(235)
#define itemHeight (iPhone4s ? HeightRate(280) : HeightRate(235))
#define itemX WidthRate(11.5)
#define itemY 100 + itemX
#define dx (itemX + itemWidth)
#define dy (itemHeight + HeightRate(11))

#define shopAddress(shopId) [NSString stringWithFormat:@"http://hdvg.me/html/store_default.html?shopId=%@", shopId]
//#define shopAddress(shopId) [NSString stringWithFormat:@"http://120.24.234.76/html/store_default.html?shopId=%@", shopId]
//#define shopAddress(shopId) [NSString stringWithFormat:@"http://192.168.1.26:8123/html/store_default.html?shopId=%@", shopId]


#import "DistributionShopViewController.h"
#import "MasterViewController.h"
#import "ShopViewController.h"
#import "WebViewViewController.h"
#import "PMDistributionsShopItems.h"
#import "DistributionSettingViewController.h"
#import "PMDistributionsGoodsViewController.h"
#import "PMProductStoreViewController.h"
#import "PMMyProfitViewController.h"
#import "PMGetCashTableViewController.h"
#import "BankcardViewController.h"
#import "SWContactAddViewController.h"
#import "SWPayCostViewController.h"
#import <QuartzCore/QuartzCore.h>
#include "SWCountingLabel.h"
#import "CardScanViewController.h"
#import "PMToastHint.h"
#import "PMDistubitionStoreViewController.h"

@interface DistributionShopViewController ()<UIGestureRecognizerDelegate,UIActionSheetDelegate,UIAlertViewDelegate,PMImageCroperDelegate,KINWebBrowserDelegate,UINavigationBarDelegate>
@property(nonatomic,strong)UILabel *shopNameLB;
@property(nonatomic,strong)UILabel *shopDiscribtionLB;

@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIView *disView;
@property(nonatomic,strong)UIImageView *bgView;

@property(nonatomic,copy)NSString *shopId;
@property(nonatomic,copy)NSString *productId;
@property(nonatomic,strong)NSURL *lastLoadURL;
@property (strong,nonatomic) UIView *noResultView;
@property (strong,nonatomic) UIView *anotherView;
@property (strong,nonatomic) NSMutableArray *buttonArr;
@property (strong,nonatomic) NSMutableArray *rectArr;
@property (strong,nonatomic) SWCountingLabel *moneyLB;
@property (strong,nonatomic) UILabel *coinLB;
@property (strong,nonatomic) UILabel *visitLB;
@property (strong,nonatomic) UILabel *buyerLB;
@property (strong,nonatomic) UILabel *booketLB;

@property (strong,nonatomic) UIButton *applyDisBtn;

@property (strong,nonatomic) UIImageView *firtsIma;
@property (strong,nonatomic) UIImageView *secondIma;
@property (strong,nonatomic) UIImageView *thirdIma;

@property (strong,nonatomic) NSMutableDictionary *reSellerDataDict;
@property (assign,nonatomic) BOOL isPush;


@end

static DistributionShopViewController *_currentVC;

@implementation DistributionShopViewController

+ (DistributionShopViewController *)currentVC
{
    return _currentVC;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"我的代言";
        _currentVC = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.rectArr = [NSMutableArray array];
    self.buttonArr = [NSMutableArray array];
    
    [[MasterViewController defaultMasterVC] checkLogin];
    
    
    [self.view setBackgroundColor:HDVGPageBGGray];
    [self initShopImage];
    [self createDistributionUI];
    [self createNoReseller];
    [self initItems];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.userInteractionEnabled = YES;
    //检查是否已经登陆
    [self checkIsLogin];
    
    //我的代言，如果是已经注册代言，执行以下代码
    if ([PMUserInfos shareUserInfo].isReseller == 1)
    {
        self.noResultView.hidden = YES;
        self.anotherView.hidden = YES;
        self.firtsIma.hidden = YES;
        self.secondIma.hidden = YES;
        self.thirdIma.hidden = YES;
        [self getShopData];
        [self getToDayPrice];
    }
    else
    {
        [self getApplyReSellerData];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self buttonAnimation];
}

- (void)initShopImage
{
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 125)];
    iv.image = [UIImage imageNamed:@"background"];
    iv.userInteractionEnabled = YES;
    self.bgView = iv;
    [self.view addSubview:iv];
    
    //添加头像
    PMImageCroperSingleton *portrait = [PMImageCroperSingleton addProtraitToTarget:self SuperView:iv  WithFrame:CGRectMake(WidthRate(40), 22.5, 80, 80)];
    
    self.portrait = portrait;
    portrait.delegate = self;
    
    portrait.portraitImageView.userInteractionEnabled = YES;
    
//    UITapGestureRecognizer *portraitTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editPortrait)];
//    [portrait.portraitImageView addGestureRecognizer:portraitTap];
    
    
    UILabel *shopNameLB = [[UILabel alloc] initWithFrame:CGRectMake(portrait.portraitImageView.frame.origin.x + portrait.portraitImageView.frame.size.width + WidthRate(40), 30, WidthRate(350), 25)];
    self.shopNameLB = shopNameLB;
    shopNameLB.textColor = [UIColor whiteColor];
    shopNameLB.font = [UIFont boldSystemFontOfSize:16.0f];
    shopNameLB.shadowOffset = CGSizeMake(1.0, 1.0);
    shopNameLB.shadowColor = [UIColor grayColor];
    [iv addSubview:shopNameLB];
    
    UILabel *dis = [[UILabel alloc] initWithFrame:CGRectMake(portrait.portraitImageView.frame.origin.x + portrait.portraitImageView.frame.size.width + WidthRate(40), 55, WIDTH - portrait.portraitImageView.frame.size.width - WidthRate(100), 40)];
    dis.backgroundColor = [UIColor clearColor];
    self.shopDiscribtionLB = dis;
    dis.textColor = [UIColor whiteColor];
    dis.font = [UIFont systemFontOfSize:14.0f];
    dis.numberOfLines = 2;
    dis.shadowOffset = CGSizeMake(1.0, 1.0);
    dis.shadowColor = [UIColor grayColor];
    [iv addSubview:dis];

    
    CGFloat ivHi = iv.bounds.size.height;
//    CGRectMake(0, ivHi+1, WIDTH, 41);
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(0, ivHi+1, WIDTH/2-0.5, 35)];
    [btn1 setTitle:@" 查看店铺" forState:UIControlStateNormal];
    btn1.backgroundColor = [UIColor whiteColor];
    btn1.titleLabel.font = [UIFont systemFontOfSize:14];
    [btn1 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btn1 setImage:[UIImage imageNamed:@"checkshop_icon"] forState:UIControlStateNormal];
    [self.view addSubview:btn1];
    btn1.tag = 1001;
    [btn1 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/2+0.5, ivHi+1, WIDTH/2, 35)];
    [btn2 setTitle:@" 分享店铺" forState:UIControlStateNormal];
    btn2.backgroundColor = [UIColor whiteColor];
    btn2.titleLabel.font = [UIFont systemFontOfSize:14];
    [btn2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btn2 setImage:[UIImage imageNamed:@"share_shop"] forState:UIControlStateNormal];
    [self.view addSubview:btn2];
    btn2.tag = 1002;
    [btn2 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)createDistributionUI
{
    CGFloat ivHeight = self.bgView.bounds.size.height + 36;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, ivHeight + 5, WIDTH, HEIGHT - 64 - ivHeight + 5)];
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.contentSize = CGSizeMake(0, 325);
    self.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.scrollView];
    
    UIView *disView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 50)];
    disView.backgroundColor = [UIColor whiteColor];
    self.disView = disView;
    [self.scrollView addSubview:disView];
    
    UIButton *goTodayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    goTodayBtn.frame = CGRectMake(0, 0, disView.bounds.size.width, disView.bounds.size.height);
    [goTodayBtn addTarget:self action:@selector(goToToday:) forControlEvents:UIControlEventTouchUpInside];
    [disView addSubview:goTodayBtn];
    
    UILabel *lb1 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 10, WidthRate(350), 25)];
    lb1.text = @"累计收益";
    lb1.textColor = [UIColor grayColor];
    lb1.textAlignment = NSTextAlignmentLeft;
    lb1.font = [UIFont systemFontOfSize:16.0f];
    [disView addSubview:lb1];
    
    // 累计收益
    NSString *money = @"0.00";
    CGSize moneySize = [money sizeWithFont:[UIFont systemFontOfSize:20.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
    
    self.moneyLB = [[SWCountingLabel alloc] initWithFrame:CGRectMake(WIDTH - 30 - moneySize.width, 6, moneySize.width, 30)];
    self.moneyLB.backgroundColor = [UIColor clearColor];
    self.moneyLB.textColor = HDVGRed;
    self.moneyLB.textAlignment = NSTextAlignmentLeft;
    self.moneyLB.font = [UIFont systemFontOfSize:20.0f];
    self.moneyLB.text = money;
    [self.disView addSubview:self.moneyLB];
    
    self.coinLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 30 - moneySize.width - 15, 7, 15, 30)];
    self.coinLB.font = [UIFont systemFontOfSize:15.0f];
    self.coinLB.textColor = HDVGRed;
    self.coinLB.textAlignment = NSTextAlignmentCenter;
    self.coinLB.text = @"¥";
    [disView addSubview:self.coinLB];
    
    
    // 剪头图片
    UIImageView *arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 12, 20, 20)];
    [arrowImage setImage:[UIImage imageNamed:@"arrowRight"]];
    [disView addSubview:arrowImage];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 45, WIDTH, HeightRate(1));
    layer1.backgroundColor = HDVGPageBGGray.CGColor;
    [disView.layer addSublayer:layer1];

    // 访客
    UILabel *num1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, WIDTH / 3, 20)];
    num1.text = @"0";
    num1.textColor = [UIColor blackColor];
    num1.textAlignment = NSTextAlignmentCenter;
    num1.font = [UIFont systemFontOfSize:16.0f];
    self.visitLB = num1;
    [disView addSubview:num1];
    
    UILabel *visitor = [[UILabel alloc] initWithFrame:CGRectMake(0, 70, WIDTH / 3, 15)];
    visitor.text = @"累计点击量";
    visitor.textColor = [UIColor grayColor];
    visitor.textAlignment = NSTextAlignmentCenter;
    visitor.font = [UIFont systemFontOfSize:14.0f];
    [disView addSubview:visitor];
    
    // 买家
    UILabel *num2 = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3, 50, WIDTH / 3, 20)];
    num2.text = @"0";
    num2.textColor = [UIColor blackColor];
    num2.textAlignment = NSTextAlignmentCenter;
    num2.font = [UIFont systemFontOfSize:16.0f];
    self.buyerLB = num2;
    [disView addSubview:num2];
    
    UILabel *buyer = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3, 70, WIDTH / 3, 15)];
    buyer.text = @"累计买家";
    buyer.textColor = [UIColor grayColor];
    buyer.textAlignment = NSTextAlignmentCenter;
    buyer.font = [UIFont systemFontOfSize:14.0f];
    [disView addSubview:buyer];
    
    // 订单
    UILabel *num3 = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH / 3) * 2, 50, WIDTH / 3, 20)];
    num3.text = @"0";
    num3.textColor = [UIColor blackColor];
    num3.textAlignment = NSTextAlignmentCenter;
    num3.font = [UIFont systemFontOfSize:16.0f];
    self.booketLB = num3;
    [disView addSubview:num3];
    
    UILabel *order = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH / 3) * 2, 70, WIDTH / 3, 15)];
    order.text = @"累计订单";
    order.textColor = [UIColor grayColor];
    order.textAlignment = NSTextAlignmentCenter;
    order.font = [UIFont systemFontOfSize:14.0f];
    [disView addSubview:order];
    
    // 分隔线
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(0, 89, WIDTH, HeightRate(1));
    layer2.backgroundColor = HDVGPageBGGray.CGColor;
//    [disView.layer addSublayer:layer2];
}


- (void)clickAction:(UIButton *)sender
{
    
    if(sender.tag == 1001)//查看店铺
    {
        self.isPush = YES;
        WebViewViewController *webVC = [[WebViewViewController alloc] initWithWebType:webTypeShop];
        webVC.newsShopId = [self.shopDataDic objectNullForKey:@"id"];
        [self.navigationController pushViewController:webVC animated:YES];
    }
    else//分享店铺
    {
        [self initShareMessage:sender];
    }
}


#pragma mark - 更改账户头像图片方法
- (void)editPortrait {
    UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册中选取", nil];
    [choiceSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.portrait actionSheet:actionSheet clickedButtonAtIndex:buttonIndex];
}


- (void)initItems
{
    NSArray *iconArray = @[@"shop_setting",@"shop_goods",@"shop_house",@"shop_get_money",@"shop_order",@"shop_card"];
    NSArray *titlaArray = @[@"店铺设置",@"代言商品",@"商品库",@"代言获利",@"提现明细",@"银行卡"];
    
    CGFloat lastY = 0.0;
    for (NSInteger i = 0; i<iconArray.count; i++)
    {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(itemX +dx* (i%3), itemY +dy*(i/3), itemWidth, itemHeight)];
        [btn setBackgroundImage:[UIImage imageNamed:titlaArray[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickToPushVC:) forControlEvents:UIControlEventTouchUpInside];
        [btn.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(-HeightRate(60), 0, 0, 0)];
        btn.tag = i;
        [self.buttonArr addObject:btn];
        [self.scrollView addSubview:btn];
        CGRect frame = btn.frame;
        NSValue *value = [NSValue valueWithCGRect:frame];
        [self.rectArr addObject:value];
        btn.frame = CGRectZero;
        
        lastY = btn.frame.origin.y + btn.frame.size.height;
    }
}

- (void)clickToPushVC:(UIButton *)sender
{
    self.isPush = YES;
    self.view.userInteractionEnabled = NO;
    switch (sender.tag)
    {
        case 0:
        {
            //店铺设置
            DistributionSettingViewController *vc = [[DistributionSettingViewController alloc] init];
//            vc.shopDataDic = self.shopDataDic;
            vc.shopVC = self;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:
        {
            //代言商品
            PMDistributionsGoodsViewController *vc = [[PMDistributionsGoodsViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            //商品库显示
            /*
             PM_SID             Y	String		Token
             currentPage		Y	Int         当前页
             pageSize           N	Int         一页显示多少个
             searchContent      N	String		搜索框中的内容
             areaId             Y	String		区域id
             priceOrder         Y	String		价格排序asc 递增  desc递减
             xlTotalOrder       Y	String		销量排序asc 递增  desc递减
             sellScale          Y	String		佣金比率排序asc 递增  desc递减
             
             //////三选一，不选的传@""
             */

            //从首页请求中得到的defaultArea中得到areaId
            
//            PMProductStoreViewController *vc = [[PMProductStoreViewController alloc] init];
//            [vc getModelsWithParam:@{PMSID,@"currentPage":@(1),@"pageSize":@(20),@"areaId":@"440100",@"priceOrder":@"asc",@"xlTotalOrder":@"",@"sellScale":@""} finish:^{
//                [self.navigationController pushViewController:vc animated:YES];
//            } error:^{
//                [self.navigationController pushViewController:vc animated:YES];
//            }];
            PMDistubitionStoreViewController *vc = [[PMDistubitionStoreViewController alloc] init];
            [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];
        }
            break;

        case 3:
        {
            //代言获利
            PMMyProfitViewController *vc = [[PMMyProfitViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 4:
        {
            //提现明细
            PMGetCashTableViewController *vc = [[PMGetCashTableViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 5:
        {
            //绑定银行卡修改
            BankcardViewController *bankcardVC = [[BankcardViewController alloc] init];
            [self.navigationController pushViewController:bankcardVC animated:YES];
//            CardScanViewController *cardScanVC = [[CardScanViewController alloc] init];
//            [self.navigationController pushViewController:cardScanVC animated:YES];
        }
            break;
    }
    
}

- (void)checkIsLogin
{
    NSString *pmsid = [PMUserInfos shareUserInfo].PM_SID;
    if(pmsid == nil)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请先登录" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        av.tag = 101;
        [av show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101)
    {
        [[MasterViewController defaultMasterVC] transitionToLoginVC];
    }
}


#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - 获取店铺信息

- (void)getShopData
{
    [[PMNetworking defaultNetworking] request:PMRequestStateShareShowShopHome WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            self.shopDataDic = [dic objectNullForKey:@"data"];
            
            // 店铺头像
            [self.portrait.portraitImageView sd_setImageWithURL:[self.shopDataDic objectNullForKey:@"shopLogo"] placeholderImage:[UIImage imageNamed:@"default_user_portrait"]];
            
            // 店铺名称
            NSString *shopName = [self.shopDataDic objectNullForKey:@"shopName"];
            if (shopName.length == 0)
                self.shopNameLB.text = @"我的店铺";
            else
                self.shopNameLB.text = [self.shopDataDic objectNullForKey:@"shopName"];
            
            // 店铺介绍
            NSString *shopAbout = [self.shopDataDic objectNullForKey:@"shopAbout"];
            if (shopAbout.length == 0)
                self.shopDiscribtionLB.text = @"这家伙很懒，什么都没写！";
            else
                self.shopDiscribtionLB.text = [self.shopDataDic objectNullForKey:@"shopAbout"];
            if ([[self.shopDataDic objectNullForKey:@"shopPhoto"] isKindOfClass:[NSString class]] && [self.shopDataDic objectNullForKey:@"shopPhoto"] && ![[self.shopDataDic objectNullForKey:@"shopPhoto"] isEqualToString:@""])
            {
                [self.bgView sd_setImageWithURL:[NSURL URLWithString:[self.shopDataDic objectNullForKey:@"shopPhoto"]] placeholderImage:[UIImage imageNamed:@"background"]];
            }
            else
            {
                self.bgView.image  = [UIImage imageNamed:@"background"];
            }
        }
        else
        {
            
        }
    }showIndicator:YES];
}

#pragma mark - 获取代言商信息
- (void)getApplyReSellerData
{
    NSDictionary *pmsid = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID};
    
    [[PMNetworking defaultNetworking] request:PMRequestStateGetApplyReSeller WithParameters:pmsid callBackBlock:^(NSDictionary *dict) {
        
        if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
            
            // 获取代言商信息
            NSString *shopCertifyId = [[dict objectNullForKey:@"data"] objectNullForKey:@"shopCertifyId"];
            NSString *applyStatus = [[dict objectNullForKey:@"data"] objectNullForKey:@"applyStatus"];
            NSString *areaId = [[dict objectNullForKey:@"data"] objectNullForKey:@"areaId"];
            NSString *areaName = [[dict objectNullForKey:@"data"] objectNullForKey:@"areaName"];
            NSString *mobile = [[dict objectNullForKey:@"data"] objectNullForKey:@"mobile"];
            NSString *name = [[dict objectNullForKey:@"data"] objectNullForKey:@"name"];
            NSString *certifyNum = [[dict objectNullForKey:@"data"] objectNullForKey:@"certifyNum"];
            NSString *certifyType = [[dict objectNullForKey:@"data"] objectNullForKey:@"certifyType"];
        
    
            self.reSellerDataDict = [@{@"shopCertifyId":shopCertifyId,@"applyStatus":applyStatus,@"areaId":areaId,@"areaName":areaName,@"mobile":mobile,@"name":name,@"certifyType":certifyType,@"certifyNum":certifyNum} mutableCopy];
            
            // 检查申请代言用户是否已支付加盟费，如果为0即还没支付
            if (![applyStatus isEqualToString:@""] && [applyStatus integerValue] == 0) {
                
                self.firtsIma.hidden = NO;
                self.secondIma.hidden = NO;
                self.thirdIma.hidden = NO;
                
            }
            
            // 申请代言通过
            if ([[[[PMUserInfos shareUserInfo].userDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"isReseller"] integerValue] == 1) {
                self.noResultView.hidden = YES;
                self.anotherView.hidden = YES;
            }
            else {
                self.noResultView.hidden = NO;
                self.anotherView.hidden = NO;
            }
        }
        
    }showIndicator:YES];
}

#pragma mark - 上传头像
- (void)finishActionWithCompressImgData:(NSData *)data
{
    NSDictionary *param = @{PMSID,@"flag":@"shopLogo"};
    
    //上传头像
    [[PMNetworking defaultNetworking] uploadImage:param imageData:data success:^(NSDictionary *dic) {
        [self getShopData];
    }];
}


#pragma mark - 分享店铺
- (void)initShareMessage:(id)sender
{
   
    //1+创建弹出菜单容器（iPad必要）
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionDown];
    
    NSString *shopUrl = shopAddress([self.shopDataDic objectNullForKey:@"id"]);
    NSData *data = UIImageJPEGRepresentation([UIImage imageNamed:@"noImage"], 0.1);
    __block UIImage *urlData;
    
    if (isNull([self.shopDataDic objectNullForKey:@"shopLogo"]) || [[self.shopDataDic objectNullForKey:@"shopLogo"] isEqualToString:@""])
    {
        
    }
    else
    {
        [[SDWebImageManager sharedManager] downloadImageWithURL:[self.shopDataDic objectNullForKey:@"shopLogo"] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
            
            
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            
            urlData = image;
            
        }];
    }
    
    
     NSLog(@"shopMessage --> %@",@(isNull([self.shopDataDic objectNullForKey:@"shopLogo"])));
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:isNull([self.shopDataDic objectNullForKey:@"shopAbout"]) ? @"这家伙很懒，什么都没写" : [self.shopDataDic objectNullForKey:@"shopAbout"]
                                       defaultContent:@"恒大微购--移动商城"
                                                image:(isNull([self.shopDataDic objectNullForKey:@"shopLogo"]) || [[self.shopDataDic objectNullForKey:@"shopLogo"] isEqualToString:@""])? [ShareSDK imageWithData:data fileName:@"noImage" mimeType:@"png"]:[ShareSDK jpegImageWithImage:urlData quality:0.5]
                                                title:[self.shopDataDic objectNullForKey:@"shopName"]
                                                  url:shopUrl
                                          description:isNull([self.shopDataDic objectNullForKey:@"shopAbout"]) ? @"这家伙很懒，什么都没写" : [self.shopDataDic objectNullForKey:@"shopAbout"]
                                            mediaType:SSPublishContentMediaTypeNews];
    
    //定制短信信息
    [publishContent addSMSUnitWithContent:[NSString stringWithFormat:@"恒大微购商城 %@",shopUrl]];
    
    //定制复制信息
    [publishContent addCopyUnitWithContent:[NSString stringWithFormat:@"%@",shopUrl] image:nil];
    
    
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    if (type == ShareTypeCopy)
                                    {
                                        PMToastHint *hint = [PMToastHint defaultToastWithRight:YES];
                                        [hint showHintToView:self.view ByToast:@"店铺地址已复制"];
                                    }
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    if([[error errorDescription] rangeOfString:@"Insufficient app permissions"].length>0)
                                    {
                                        //新浪分享失败,重新分享不带图片的
                                        [self shareToSinaWithContainner:container content:@"恒大微购--移动商城" title:[self.shopDataDic objectNullForKey:@"shopName"] url:shopUrl];
                                    }
                                    else
                                    {
                                        if (type == ShareTypeCopy)
                                        {
                                            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view  ByToast:@"店铺地址复制失败"];
                                        }
                                        else
                                        {
                                            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view  ByToast:@"分享失败"];
                                        }
                                    }
                                }
                            }];
    
}

#pragma mark - 暂时的补救措施，新浪微博暂时不能分享带图片的
- (void)shareToSinaWithContainner:(id<ISSContainer>)container content:(NSString *)content title:(NSString *)shareTitle url:(NSString *)urlString
{
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:content
                                       defaultContent:@"恒大微购--移动商城"
                                                image:nil
                                                title:shareTitle
                                                  url:urlString
                                          description:@"有种别看我"
                                            mediaType:SSPublishContentMediaTypeNews];
    
    //不弹选择窗，直接分享
    [ShareSDK shareContent:publishContent type:ShareTypeSinaWeibo authOptions:nil shareOptions:nil statusBarTips:YES result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        if (state == SSResponseStateSuccess)
        {
            [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view  ByToast:@"分享成功"];
            
        }
        else if (state == SSResponseStateFail)
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view  ByToast:@"分享失败"];
        }
    }];
}

#pragma mark - 创建没有代言权限提示UI
- (void)createNoReseller
{
    UIView *noResellerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    noResellerView.backgroundColor = [UIColor blackColor];
    noResellerView.alpha = 0.8;
    self.noResultView = noResellerView;
    [self.view addSubview:noResellerView];
    
    UIView *anotherView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    anotherView.backgroundColor = [UIColor clearColor];
    anotherView.alpha = 1;
    self.anotherView = anotherView;
    [self.view addSubview:anotherView];
    
    /**
     * 代言认证操作步骤
     */
    UILabel *stepLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, WIDTH, 20)];
    stepLB.font = [UIFont systemFontOfSize:17.0f];
    stepLB.textColor = [UIColor whiteColor];
    stepLB.textAlignment = NSTextAlignmentCenter;
    stepLB.text = @"代言认证操作步骤";
    [self.anotherView addSubview:stepLB];
    
    // 步骤提示图
    UIImageView *stepIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(130), 70, 60, 310)];
    stepIV.image = [UIImage imageNamed:@"distribution_step"];
    stepIV.contentMode = UIViewContentModeScaleAspectFit;
    [self.anotherView addSubview:stepIV];
    
    // 文字操作步骤提示
    UILabel *firstLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(130) + 60 + 30, 90, WIDTH - WidthRate(260) - 90, 40)];
    firstLB.backgroundColor = [UIColor clearColor];
    firstLB.textAlignment = NSTextAlignmentLeft;
    firstLB.text = @"1.验证手机号码";
    firstLB.font = [UIFont systemFontOfSize:15.0f];
    firstLB.textColor = [UIColor whiteColor];
    [self.anotherView addSubview:firstLB];
    
    UILabel *secondLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(130) + 60 + 30, 185, WIDTH - WidthRate(260) - 90, 40)];
    secondLB.textAlignment = NSTextAlignmentLeft;
    secondLB.text = @"2.填写个人信息";
    secondLB.font = [UIFont systemFontOfSize:15.0f];
    secondLB.textColor = [UIColor whiteColor];
    [self.anotherView addSubview:secondLB];
    
    UILabel *thirdLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(130) + 60 + 30, 280, WIDTH - WidthRate(260) - 90, 40)];
    thirdLB.textAlignment = NSTextAlignmentLeft;
    thirdLB.text = @"3.预存加盟费";
    thirdLB.font = [UIFont systemFontOfSize:15.0f];
    thirdLB.textColor = [UIColor whiteColor];
    [self.anotherView addSubview:thirdLB];
    
    
    // 申请完成步骤提示图
    UIImageView *firstIV = [[UIImageView alloc] initWithFrame:CGRectMake(firstLB.frame.origin.x + firstLB.frame.size.width + 5, 95, 30, 30)];
    firstIV.image = [UIImage imageNamed:@"step_image"];
    firstIV.contentMode = UIViewContentModeScaleAspectFit;
    firstIV.hidden = YES;
    self.firtsIma = firstIV;
    [self.anotherView addSubview:firstIV];
    
    UIImageView *secondIV = [[UIImageView alloc] initWithFrame:CGRectMake(firstLB.frame.origin.x + firstLB.frame.size.width + 5, 190, 30, 30)];
    secondIV.image = [UIImage imageNamed:@"step_image"];
    secondIV.contentMode = UIViewContentModeScaleAspectFit;
    secondIV.hidden = YES;
    self.secondIma = secondIV;
    [self.anotherView addSubview:secondIV];
    
    UIImageView *thirdIV = [[UIImageView alloc] initWithFrame:CGRectMake(firstLB.frame.origin.x + firstLB.frame.size.width + 5, 285, 30, 30)];
    thirdIV.image = [UIImage imageNamed:@"unfinish_image"];
    thirdIV.contentMode = UIViewContentModeScaleAspectFit;
    thirdIV.hidden = YES;
    self.thirdIma = thirdIV;
    [self.anotherView addSubview:thirdIV];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(WidthRate(130), 360, (WIDTH - WidthRate(260)), 40);
    button.layer.cornerRadius = 4.0f;
    button.backgroundColor = RGBA(233, 91, 80, 1);
    [button setTitle:@"申请代言" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(registerReseller:) forControlEvents:UIControlEventTouchUpInside];
    self.applyDisBtn = button;
    [self.anotherView addSubview:button];
    
    self.noResultView.hidden = NO;
    self.anotherView.hidden = NO;
    
//    if ([[[[PMUserInfos shareUserInfo].userDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"isReseller"] integerValue] == 1)
//    {
//        self.noResultView.hidden = YES;
//        self.anotherView.hidden = YES;
//    }
//    else
//    {
//        self.noResultView.hidden = NO;
//        self.anotherView.hidden = NO;
//    }
}

- (void)registerReseller:(UIButton *)sender
{
    self.isPush = YES;
    SWContactAddViewController *authcodeVC = [[SWContactAddViewController alloc] init];
    authcodeVC.reSellerDataDict = self.reSellerDataDict;
    [self.navigationController pushViewController:authcodeVC animated:YES];
}

- (void)buttonAnimation
{
    if ([[[[PMUserInfos shareUserInfo].userDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"isReseller"] integerValue] == 1)
    {
        for (NSInteger i = 0; i < self.buttonArr.count; i++)
        {
            CGRect frame = [self.rectArr[i] CGRectValue];
            UIButton *button = self.buttonArr[i];
            button.frame = frame;
            [UIView animateWithDuration:0.3f delay:i * 0.2f options:UIViewAnimationOptionCurveEaseOut animations:^(void){
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:button cache:YES];
            } completion:^(BOOL finish){
                
            }];
        }
    }
    else
    {
        for (NSInteger i = 0; i < self.buttonArr.count; i++)
        {
            CGRect frame = [self.rectArr[i] CGRectValue];
            UIButton *button = self.buttonArr[i];
            button.frame = frame;
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    for (UIButton *button in self.buttonArr)
    {
        button.frame = CGRectZero;
    }
}

- (void)goToToday:(UIButton *)sender
{
    self.isPush = YES;
    PMMyProfitViewController *vc = [[PMMyProfitViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getToDayPrice
{
    NSDictionary *paramter = @{PMSID};
    [[PMNetworking defaultNetworking] request:PMRequestStateShareShowMySell WithParameters:paramter callBackBlock:^(NSDictionary *dic) {
        
        if ([[dic objectNullForKey:@"success"] integerValue] == 1) {
            
            NSDictionary *mySellDic = [dic objectNullForKey:@"data"];
            NSDictionary *sellDic = [mySellDic objectNullForKey:@"mySell"];
            
            
            /*
             NSString *money = [NSString stringWithFormat:@"%.2lf元",[[sellDic objectNullForKey:@"totalMoney"] doubleValue]];
             NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:money];
             [noteStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.0f] range:NSMakeRange(0, [[noteStr string] rangeOfString:@"元"].location)];
             [noteStr addAttribute:NSForegroundColorAttributeName value:HDVGRed range:NSMakeRange(0, [[noteStr string] rangeOfString:@"元"].location)];
             self.moneyLB.attributedText = noteStr;
             */
            
            
            NSString *money = [NSString stringWithFormat:@"%.2lf",[[sellDic objectNullForKey:@"totalMoney"] doubleValue]];
            CGSize moneySize = [money sizeWithFont:[UIFont systemFontOfSize:20.0f]];
            
            // 累计收益
            self.moneyLB.frame = CGRectMake(WIDTH - 30 - moneySize.width, 6, moneySize.width, 30);
            [self.disView addSubview:self.moneyLB];
            
            self.coinLB.frame = CGRectMake(WIDTH - 30 - moneySize.width - 15, 7, 15, 30);
            self.coinLB.text = @"¥";
            
            double totalMoney = [[sellDic objectNullForKey:@"totalMoney"] doubleValue];
            [self withNumberLabel:self.moneyLB changeNumber:totalMoney];
            
            self.visitLB.text = [NSString stringWithFormat:@"%@",[sellDic objectNullForKey:@"clickRate"]];
            self.booketLB.text = [NSString stringWithFormat:@"%@",[sellDic objectNullForKey:@"completeTotal"]];
            self.buyerLB.text = [NSString stringWithFormat:@"%@",[sellDic objectNullForKey:@"careTotal"]];
        }
        
    } showIndicator:NO];
}

// 累计收益数字变动效果实现
- (void)withNumberLabel:(SWCountingLabel *)label changeNumber:(double)number
{
    label.method = UILabelCountingMethodLinear;
    label.format = @"%.2f";
    [label countFrom:0 to:number withDuration:1.0];
}


@end
