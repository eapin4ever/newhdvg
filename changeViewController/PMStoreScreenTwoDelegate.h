//
//  PMStoreScreenTwoDelegate.h
//  changeViewController
//
//  Created by P&M on 15/7/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class PMDistubitionStoreViewController;


@interface PMStoreScreenTwoDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *storeScreenTwoTV;
@property (strong, nonatomic) NSMutableArray *screenTwoArray;
@property (strong, nonatomic) NSMutableArray *screenDetailArray;

@property (strong, nonatomic) NSDictionary *screenTwoDict;

@property (weak, nonatomic) PMDistubitionStoreViewController *productStoreVC;


@end
