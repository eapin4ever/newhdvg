//
//  KICropImageView.h
//  QiuBai
//
//  Created by Archmage on 14-4-16.
//  Copyright (c) 2014年 Less Everything. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+KIAdditions.h"

@class KICropImageMaskView;
@interface KICropImageView : UIView <UIScrollViewDelegate> {
    @private
    UIScrollView        *_scrollView;
    UIImageView         *_imageView;
    KICropImageMaskView *_maskView;
    UIImage             *_image;
    UIEdgeInsets        _imageInset;
    CGSize              _cropSize;
}
@property (nonatomic, strong) UIImage *coverImg;
@property (nonatomic,assign) BOOL isHeader;

- (void)setImage:(UIImage *)image;
- (void)setCropSize:(CGSize)size;

- (UIImage *)cropImage;
- (id)initWithFrame:(CGRect)frame AndIsHeader:(BOOL)isHeader;

@end

@class CircleOpacity;
@interface KICropImageMaskView : UIView

@property (nonatomic, assign) CGRect cropRect;
@property (nonatomic, strong) CircleOpacity *cirleOpacityView;
@property (nonatomic, assign) CGSize cropSize;

@property (nonatomic, strong) UIImage *coverImg;

- (id)initWithHeader;

@end

@interface CircleOpacity : UIView

@end