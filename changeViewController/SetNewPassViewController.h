//
//  SetNewPassViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/15.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "MD5Util.h"

@interface SetNewPassViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIView *headView;
@property (strong, nonatomic) UITextField *setNewPassTextField;
@property (strong, nonatomic) NSString *accountString;
@property (strong, nonatomic) NSString *authcodeString;

@property (strong, nonatomic) PMNetworking *networking;

@end
