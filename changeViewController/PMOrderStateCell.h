//
//  PMOrderStateCell.h
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMOrderStateCell : UITableViewCell

@property (strong,nonatomic) UIImageView *stateLogoIV;
@property (strong,nonatomic) UILabel *stateLB;

- (void)createUI;
- (void)setCellData:(NSString *)stateString;

@end
