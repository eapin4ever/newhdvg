//
//  PMOrderAddressCell.m
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMOrderAddressCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMOrderAddressCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.myContentView)
    {
        self.myContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 90)];
        self.myContentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.myContentView];
        
        UIImageView *lineImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 4)];
        lineImage.image = [UIImage imageNamed:@"dottedLine_image.png"];
        [self.myContentView addSubview:lineImage];
        
        UIImageView *lineImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.myContentView.bounds.size.height - 4, WIDTH, 4)];
        lineImage2.image = [UIImage imageNamed:@"dottedLine_image.png"];
        lineImage2.tag = 2;
        [self.myContentView addSubview:lineImage2];
        
        self.addressLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addressState"]];
        self.addressLogo.center = CGPointMake(25, self.myContentView.bounds.size.height / 2);
        self.addressLogo.bounds = (CGRect){CGPointZero,{20,20}};
        [self.myContentView addSubview:self.addressLogo];
        
        self.nameLB = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, (WIDTH - 65) / 2, 20)];
        self.nameLB.textAlignment = NSTextAlignmentLeft;
        self.nameLB.font = [UIFont systemFontOfSize:14.0f];
        self.nameLB.textColor = [UIColor lightGrayColor];
        [self.myContentView addSubview:self.nameLB];
        
        self.phoneLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.nameLB.frame) + 10, 10, (WIDTH - 65) / 2 - 10, 20)];
        self.phoneLB.textAlignment = NSTextAlignmentLeft;
        self.phoneLB.textColor = [UIColor lightGrayColor];
        self.phoneLB.font = [UIFont systemFontOfSize:14.0f];
        [self.myContentView addSubview:self.phoneLB];
        
        self.detailAddressLB = [[UILabel alloc] initWithFrame:CGRectMake(self.nameLB.frame.origin.x, 40, WIDTH - 65, 20)];
        self.detailAddressLB.textAlignment = NSTextAlignmentLeft;
        self.detailAddressLB.numberOfLines = 0;
        self.detailAddressLB.textColor = [UIColor lightGrayColor];
        self.detailAddressLB.font = [UIFont systemFontOfSize:14.0f];
        [self.myContentView addSubview:self.detailAddressLB];
    }
}

- (void)setCellData:(NSDictionary *)addressDic
{
    NSString *addressUserStirng = [NSString stringWithFormat:@"收货人:%@",[addressDic objectForKey:@"name"]];
    CGSize userNameStringSize = [addressUserStirng boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
    self.nameLB.frame = CGRectMake(self.nameLB.frame.origin.x, self.nameLB.frame.origin.y, userNameStringSize.width, 20);
    self.nameLB.text = addressUserStirng;
    
    NSString *addressPhoneString = [NSString stringWithFormat:@"%@",[addressDic objectForKey:@"mobile"]];
    CGSize userPhoneSize = [addressPhoneString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
    self.phoneLB.frame = CGRectMake(CGRectGetMaxX(self.nameLB.frame) + 20, self.phoneLB.frame.origin.y, userPhoneSize.width, 20);
    self.phoneLB.text = addressPhoneString;
    
    NSString *detailAddressString = [NSString stringWithFormat:@"%@ %@",[addressDic objectForKey:@"areas"],[addressDic objectForKey:@"address"]];
    CGSize detailAddressSize = [detailAddressString boundingRectWithSize:CGSizeMake(WIDTH - 65, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
    self.detailAddressLB.frame = CGRectMake(self.detailAddressLB.frame.origin.x, CGRectGetMaxY(self.nameLB.frame) + 10, WIDTH - 65, detailAddressSize.height);
    self.detailAddressLB.text = detailAddressString;
    self.myContentView.frame = CGRectMake(self.myContentView.frame.origin.x, self.myContentView.frame.origin.y, WIDTH, CGRectGetMaxY(self.detailAddressLB.frame) + 10);
     UIImageView *lineImage2 = (UIImageView *)[self.myContentView viewWithTag:2];
    lineImage2.frame = CGRectMake(0, self.myContentView.bounds.size.height - 4, WIDTH, 4);
    self.addressLogo.center = CGPointMake(25, self.myContentView.bounds.size.height / 2);
    self.addressLogo.bounds = (CGRect){CGPointZero,{20,20}};
}

@end
