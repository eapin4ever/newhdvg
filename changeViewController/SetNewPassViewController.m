//
//  SetNewPassViewController.m
//  changeViewController
//
//  Created by P&M on 14/12/15.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "SetNewPassViewController.h"
#import "PMToastHint.h"

@interface SetNewPassViewController ()

@end

@implementation SetNewPassViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"设置新密码";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self createSetNewPassUI];
    [self createSetNewPassControllerUI];
}

// 创建设置新密码 UI
- (void)createSetNewPassUI
{
    self.headView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 44)];
    self.headView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.headView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.headView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 44 - 0.5, WIDTH, 0.5);
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.headView.layer addSublayer:layer2];
    
    // 新密码
    UILabel *newPassLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(160), 30)];
    newPassLabel.backgroundColor = [UIColor clearColor];
    newPassLabel.text = @"新  密  码:";
    newPassLabel.textColor = HDVGFontColor;
    newPassLabel.textAlignment = NSTextAlignmentLeft;
    newPassLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.headView addSubview:newPassLabel];
    
    self.setNewPassTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(210), 7, WidthRate(490), 30)];
    self.setNewPassTextField.borderStyle = UITextBorderStyleNone;
    self.setNewPassTextField.delegate = self;
    self.setNewPassTextField.secureTextEntry = YES;
    self.setNewPassTextField.textAlignment = NSTextAlignmentLeft;
    self.setNewPassTextField.font = [UIFont systemFontOfSize:13.0f];
    self.setNewPassTextField.placeholder = @"长度为6～16位的数字、字母、符号";
    self.setNewPassTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.setNewPassTextField.returnKeyType = UIReturnKeyDone;
    self.setNewPassTextField.keyboardType = UIReturnKeyDefault;
    [self.headView addSubview:self.setNewPassTextField];
}

// 创建设置密码控件 UI
- (void)createSetNewPassControllerUI
{
    // 初始化显示密码按钮
    UIButton *showButton = [UIButton buttonWithType:UIButtonTypeCustom];
    showButton.frame = CGRectMake(WIDTH - WidthRate(230), self.headView.frame.origin.y + 44 + HeightRate(10), WidthRate(190), 18);
    [showButton setImage:[UIImage imageNamed:@"showpass_no.png"] forState:UIControlStateNormal];
    [showButton setImage:[UIImage imageNamed:@"showpass_yes.png"] forState:UIControlStateSelected];
    [showButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [showButton.titleLabel setFont:[UIFont systemFontOfSize:HeightRate(30)]];
    [showButton setTitle:@"显示密码" forState:UIControlStateNormal];
    [showButton setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [showButton addTarget:self action:@selector(showPassForButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:showButton];
    
    // 初始化完成按钮
    UIBarButtonItem *finishedButton = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(setNewPassFinishClick:)];
    
    [finishedButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR, NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = finishedButton;
}

// 按下Done按钮的调用方法，让软键盘隐藏
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.setNewPassTextField resignFirstResponder];
}

- (void)showPassForButtonClick:(UIButton *)button
{
    // 改变按钮的选中状态
    button.selected = !button.selected;
    if (button.selected) {
        self.setNewPassTextField.secureTextEntry = NO;
    }
    if (!button.selected) {
        self.setNewPassTextField.secureTextEntry = YES;
    }
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == self.setNewPassTextField && textField.text.length - range.length + string.length > 16) {
        return NO;
    }
    
    return YES;
}

- (void)setNewPassFinishClick:(id)sender
{
    // 内容不能为空
    if (self.setNewPassTextField.text.length == 0 || self.setNewPassTextField.text.length < 6 || self.setNewPassTextField.text.length > 16) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请按要求设置新密码"];
    }
    
    if (self.setNewPassTextField.text.length != 0 && (self.setNewPassTextField.text.length > 5) && (self.setNewPassTextField.text.length < 17)) {
        
        /**
         * 用户忘记密码，设置新密码
         * 参数: password:(string)密码
         */
        NSDictionary *getUserInfo = @{@"code":self.accountString,@"password":[MD5Util md5:self.setNewPassTextField.text],@"validCode":[MD5Util md5:self.authcodeString]};
        self.networking = [PMNetworking defaultNetworking];
        
        __block NSDictionary *callBackDict;
        
        [self.networking request:PMRequestStateSetPassword WithParameters:getUserInfo callBackBlock:^(NSDictionary *dict) {
            
            callBackDict = dict;
            if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"设置新密码成功，请登录" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 1;
                [alertView show];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }showIndicator:NO];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
