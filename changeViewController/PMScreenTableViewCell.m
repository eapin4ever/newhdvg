//
//  PMScreenTableViewCell.m
//  changeViewController
//
//  Created by P&M on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMScreenTableViewCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMScreenTableViewCell

- (void)createScreenUI
{
    if (!self.titleLabel) {
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 7, WidthRate(200), 30)];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.titleLabel];
        
        UIView *tipView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, WidthRate(200) + 25, 20)];
        self.tipLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WidthRate(200), 20)];
        self.tipLB.font = [UIFont systemFontOfSize:12.0f];
        self.tipLB.textColor = HDVGRed;
        self.tipLB.textAlignment = NSTextAlignmentRight;
        [tipView addSubview:self.tipLB];
        
        UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(200) + 5, 0, 20, 20)];
        tipIV.contentMode = UIViewContentModeScaleAspectFit;
        tipIV.image = [UIImage imageNamed:@"arrowRight"];
        [tipView addSubview:tipIV];
        
        self.accessoryView = tipView;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone; // 去掉cell被选中时的颜色
    }
}

- (void)setScreenTitle:(NSString *)title
{
    self.titleLabel.text = title;
}


@end
