//
//  WithdrawalMoneyViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/5.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  代言获利提现申请控制器
 */

#import <UIKit/UIKit.h>
#import "PMUserInfos.h"
#import "PMNetworking.h"

@interface WithdrawalMoneyViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIView *withdrawalView;
@property (strong, nonatomic) UILabel *canWithdrawalMoney;
@property (strong, nonatomic) UITextField *canMoneyTextField;
@property (strong, nonatomic) UITextField *authcodeTextField;
@property (strong, nonatomic) UIButton *withdrawalButton;
@property (strong, nonatomic) NSMutableArray *withdrawalMoneyArray;

@property (strong, nonatomic) NSString *canMoneyStr;

@property (strong, nonatomic) PMNetworking *networking;

@end
