//
//  PMNearbySearchViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/26.
//  Copyright (c) 2014年 wallace. All rights reserved.
//
#define moveDistance WidthRate(100)

#import "PMNearbySearchViewController.h"
#import "MasterViewController.h"
#import "PMNearbySearchResultVC.h"


@interface PMNearbySearchViewController ()<UIGestureRecognizerDelegate>
//@property(nonatomic,strong)UIImageView *scope;
@property(nonatomic,strong)UIButton *searchBtn;

@property(nonatomic,strong)PMNearbySearchResultVC *resultVC;
@property (assign,nonatomic) CGRect oldFrame;

@property(nonatomic,weak)MasterViewController *masterVC;

@end

@implementation PMNearbySearchViewController
{
    BOOL _isShowResult;
    BOOL _isStartGetModel;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"智能搜索";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isStartGetModel = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.masterVC = [MasterViewController defaultMasterVC];
    
    [self initMapView];
    
    //搜索结果视图
    [self initSearchResultVC];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //push时，默认是隐藏tabBar的，这里需要把tabBar显示回来
    [UIView animateWithDuration:0.3 animations:^{
        [[MasterViewController defaultMasterVC].tabBar setAlpha:1];
    }];
    
    self.masterVC.nearbySearchAgain = ^{
        if (self.scope)
        {
            self.scope.frame = CGRectMake(WidthRate(300), HeightRate(400), WidthRate(120), HeightRate(120));
        }
        [self searchAgain];
    };
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //没显示、并且没有开始请求数据时，才执行动画显示，防止多次触发
    if(_isShowResult == NO && _isStartGetModel == NO)
    {
        [self scopeAnimate];
    }
}

- (void)initMapView
{
    UIImageView *mapImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/1.5, WIDTH/1.5)];
    mapImage.center = CGPointMake(WIDTH/2, HEIGHT/2 - 64);
    mapImage.image = [UIImage imageNamed:@"wall"];
    mapImage.clipsToBounds = YES;
    mapImage.contentMode = UIViewContentModeScaleAspectFill;

    [mapImage.layer setCornerRadius:WIDTH/3];
    [mapImage.layer setBorderColor:RGBA(134, 215, 253, 1).CGColor];
    [mapImage.layer setBorderWidth:1.0];
    [self.view addSubview:mapImage];

    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(300), HeightRate(400), WidthRate(120), HeightRate(120))];
    self.scope = iv;
    self.oldFrame = iv.frame;
    iv.image = [UIImage imageNamed:@"scope"];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    iv.alpha = 0.95;
    [self.view addSubview:iv];
    
}

- (void)searchAgain
{
    [self hideResultVC];
}


- (void)initSearchResultVC
{
    if(!self.resultVC)
    {
        self.resultVC = [[PMNearbySearchResultVC alloc] init];
        self.resultVC.view.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 49);
        self.resultVC.searchVC = self;
        [self addChildViewController:self.resultVC];
        [self.view addSubview:self.resultVC.view];
    }
}

- (void)showResultVC
{
    [self moveItem:self.resultVC.view changeX:0 changeY: - HEIGHT finishAction:^{
        [MasterViewController defaultMasterVC].midBtn.enabled = YES;
        _isShowResult = YES;
    }];
}

- (void)hideResultVC
{
    [self moveItem:self.resultVC.view changeX:0 changeY: HEIGHT finishAction:^{
        _isShowResult = NO;
        _isStartGetModel = NO;
        [self scopeAnimate];
    }];
}



#pragma mark - 放大镜动画
- (void)scopeAnimate
{
    if(!_isStartGetModel)
    {
        _isStartGetModel = YES;
        [self.resultVC getModelsWithParam:nil finish:^{
            [self showResultVC];
        } error:nil];
    }
    
    [self moveItem:self.scope changeX:moveDistance changeY:moveDistance finishAction:^{
        [self moveItem:self.scope changeX:- moveDistance changeY:moveDistance finishAction:^{
            [self moveItem:self.scope changeX:- moveDistance changeY:- moveDistance finishAction:^{
                [self moveItem:self.scope changeX:moveDistance changeY:- moveDistance finishAction:^{
                    
                    if(!_isShowResult)
                        [self scopeAnimate];
                    
                }];
            }];
        }];
    }];

}

- (void)moveItem:(UIView *)item changeX:(CGFloat)x changeY:(CGFloat)y finishAction:(myFinishBlock)finish
{
    NSTimeInterval timer = 0.5;
    if(_isShowResult)
    {
        if ([item isEqual:self.resultVC])
        {
            
        }
        else
        {
            //回到原点
            timer = 0;
            //return;
        }
    }
    
    
    [UIView animateWithDuration:timer animations:^{
        CGRect rect = item.frame;
        rect.origin.x += x;
        rect.origin.y += y;
        item.frame = rect;
    } completion:^(BOOL finished) {
        if(finish != nil)
            finish();
    }];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
