//
//  YBProductCellOne.h
//  changeViewController
//
//  Created by ZhangEapin on 15/4/28.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBProductCellOne : UITableViewCell

@property (strong,nonatomic) NSArray *imageArr;
@property (strong,nonatomic) UIView *smallOneTitleView;
@property (strong,nonatomic) UIView *smallTwoTitleView;
@property (strong,nonatomic) UIView *bigTitleView;
@property (strong,nonatomic) UIImageView *bigIV;
@property (strong,nonatomic) UIImageView *smallOneIV;
@property (strong,nonatomic) UIImageView *smallTwoIV;
@property (strong,nonatomic) UILabel *smallOneTitle;
@property (strong,nonatomic) UILabel *smallTwoTitle;
@property (strong,nonatomic) UILabel *bigTitle;
@property (strong,nonatomic) UILabel *smallOnePriceLB;
@property (strong,nonatomic) UILabel *smallTwoPriceLB;
@property (strong,nonatomic) UILabel *bigPriceLB;
@property (strong,nonatomic) UIButton *btn1;
@property (strong,nonatomic) UIButton *btn2;
@property (strong,nonatomic) UIButton *btn3;
@property (strong,nonatomic) UILabel *oneLine;
@property (strong,nonatomic) UILabel *twoLine;
@property (assign,nonatomic) BOOL isBigZK;
@property (assign,nonatomic) BOOL isSmallOneZK;
@property (assign,nonatomic) BOOL isSmallTwoZK;
@property (strong,nonatomic) UIButton *bigZKBtn;
@property (strong,nonatomic) UIButton *smallOneZKBtn;
@property (strong,nonatomic) UIButton *smallTwoZKBtn;
@property (strong,nonatomic) UILabel *bigRawPriceLB;
@property (strong,nonatomic) UILabel *smallOneRawPriceLB;
@property (strong,nonatomic) UILabel *smallTwoRawPriceLB;
@property (strong,nonatomic) CALayer *bigLine;
@property (strong,nonatomic) CALayer *smallOneLine;
@property (strong,nonatomic) CALayer *smallTwoLine;
@property (strong,nonatomic) UIView *bigView;
@property (strong,nonatomic) UIView *smallOneView;
@property (strong,nonatomic) UIView *smallTwoView;
@property (strong,nonatomic) CALayer *vLine;

- (void)createUI;

- (void)setProductOnetitle1:(NSString *)title1 price1:(double)price1 title2:(NSString *)title2 price2:(double)price2 title3:(NSString *)title3 price3:(double)price3 rawPrice1:(double)rawPrice1 rawPrice2:(double)rawPrice2 rawPrice3:(double)rawPrice3 zk1:(NSString *)zk1 zk2:(NSString *)zk2 zk3:(NSString *)zk3;

- (void)addTarget:(id)target selector:(SEL)sel row:(NSInteger)row;


@end
