//
//  HomeStyleFourCell.m
//  changeViewController
//
//  Created by pmit on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "HomeStyleFourCell.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define kCellWidth (WIDTH - 20)

@implementation HomeStyleFourCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}


- (void)createUI
{
    if (!self.upOneIV)
    {
        UIView *upOneView = [self buildStyleOneSecondWithFrame:CGRectMake(5, 5, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"one"];
        [self.contentView addSubview:upOneView];
        self.upOneView = upOneView;
        
        UIView *upTwoView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth / 3 + 10, 5, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"two"];
        [self.contentView addSubview:upTwoView];
        self.upTwoView = upTwoView;
        
        UIView *upThreeView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth / 3 * 2 + 15, 5, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"three"];
        [self.contentView addSubview:upThreeView];
        self.upThreeView = upThreeView;
        
        UIView *upFourView = [self buildStyleOneSecondWithFrame:CGRectMake(5, kCellWidth * 0.48 + 10, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"four"];
        [self.contentView addSubview:upFourView];
        self.upFourView = upFourView;
        
        UIView *upFiveView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth / 3 + 10, kCellWidth * 0.48 + 10, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"five"];
        [self.contentView addSubview:upFiveView];
        self.upFiveView = upFiveView;
        
        UIView *upSixView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth / 3 * 2 + 15, kCellWidth * 0.48 + 10, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"six"];
        [self.contentView addSubview:upSixView];
        self.upSixView = upSixView;
    }
}

- (UIView *)buildStyleOneSecondWithFrame:(CGRect)frame isMiddle:(BOOL)isMiddle AndIsUp:(BOOL)isUp AndStation:(NSString *)station
{
    UIView *bgWhiteView = [[UIView alloc] initWithFrame:frame];
    bgWhiteView.backgroundColor = [UIColor whiteColor];
    UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, bgWhiteView.bounds.size.width, bgWhiteView.bounds.size.width)];
    [bgWhiteView addSubview:productIV];
    if (isMiddle)
    {
        UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(2, productIV.frame.origin.y + productIV.frame.size.height + (kCellWidth * 0.02), bgWhiteView.bounds.size.width - 4, 20)];
        titleLB.lineBreakMode = NSLineBreakByClipping;
        titleLB.textAlignment = NSTextAlignmentRight;
        titleLB.font = [UIFont systemFontOfSize:WidthRate(24)];
        [bgWhiteView addSubview:titleLB];
        UILabel *priceLB = [[UILabel alloc] initWithFrame:CGRectMake(2, titleLB.frame.origin.y + 20, bgWhiteView.bounds.size.width - 4, 20)];
        priceLB.textAlignment = NSTextAlignmentRight;
        priceLB.font = [UIFont boldSystemFontOfSize:13.0f];
        [bgWhiteView addSubview:priceLB];
        
        if ([station isEqualToString:@"one"])
        {
            self.upOneTitleLB = titleLB;
            self.onePriceLB = priceLB;
            self.upOneIV = productIV;
        }
        else if ([station isEqualToString:@"two"])
        {
            self.upTwoTitleLB = titleLB;
            self.twoPriceLB = priceLB;
            self.upTwoIV = productIV;
        }
        else if ([station isEqualToString:@"three"])
        {
            self.upThreeTitleLB = titleLB;
            self.threePriceLB = priceLB;
            self.upThreeIV = productIV;
        }
        else if ([station isEqualToString:@"four"])
        {
            self.upFourTitleLB = titleLB;
            self.upFourIV = productIV;
            self.fourPriceLB = priceLB;
        }
        else if ([station isEqualToString:@"five"])
        {
            self.upFiveTitleLB = titleLB;
            self.upFiveIV = productIV;
            self.fivePriceLB = priceLB;
        }
        else
        {
            self.upSixTitleLB = titleLB;
            self.upSixIV = productIV;
            self.sixPriceLB = priceLB;
        }
    }
    
    return bgWhiteView;
}

- (void)setCellWithOneIV:(NSString *)oneString AndTwoIV:(NSString *)twoIV AndThreeIV:(NSString *)threeIV AndFourIV:(NSString *)fourIV AndFiveIV:(NSString *)fiveIV AndSixIV:(NSString *)sixIV AndOneTitle:(NSString *)oneTitle AndTwoTitle:(NSString *)twoTitle AndThreeTitle:(NSString *)threeTitle AndFourTitle:(NSString *)fourTitle AndFiveTitle:(NSString *)fiveTitle AndSixTitle:(NSString *)sixTitle AndOnePrice:(NSString *)onePrice AndTwoPrice:(NSString *)twoPrice AndThreePrice:(NSString *)threePrice AndFourPrice:(NSString *)fourPrice AndFivePrice:(NSString *)fivePrice AndSixPrice:(NSString *)sixPrice
{
    [self.upOneIV sd_setImageWithURL:[NSURL URLWithString:oneString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upTwoIV sd_setImageWithURL:[NSURL URLWithString:twoIV] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upThreeIV sd_setImageWithURL:[NSURL URLWithString:threeIV] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upFourIV sd_setImageWithURL:[NSURL URLWithString:fourIV] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upFiveIV sd_setImageWithURL:[NSURL URLWithString:fiveIV] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upSixIV sd_setImageWithURL:[NSURL URLWithString:sixIV] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    
    
    if (oneTitle.length > 9)
    {
        oneTitle = [oneTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (twoTitle.length > 9)
    {
        twoTitle = [twoTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (threeTitle.length > 9)
    {
        threeTitle = [threeTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (fourTitle.length > 9)
    {
        fourTitle = [fourTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (fiveTitle.length > 9)
    {
        fiveTitle = [fiveTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (sixTitle.length > 9)
    {
        sixTitle = [sixTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    self.upOneTitleLB.text = oneTitle;
    self.upTwoTitleLB.text = twoTitle;
    self.upThreeTitleLB.text = threeTitle;
    self.upFourTitleLB.text = fourTitle;
    self.upFiveTitleLB.text = fiveTitle;
    self.upSixTitleLB.text = sixTitle;
    
    self.onePriceLB.text = onePrice;
    self.twoPriceLB.text = twoPrice;
    self.threePriceLB.text = threePrice;
    self.fourPriceLB.text = fourPrice;
    self.fivePriceLB.text = fivePrice;
    self.sixPriceLB.text = sixPrice;
}

@end
