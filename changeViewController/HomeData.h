//
//  HomeData.h
//  changeViewController
//
//  Created by pmit on 15/7/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface HomeData : NSManagedObject

@property (nonatomic, retain) NSNumber * discount;
@property (nonatomic, retain) NSString * headerADName;
@property (nonatomic, retain) NSString * homeImage;
@property (nonatomic, retain) NSNumber * isDiscount;
@property (nonatomic, retain) NSNumber * isHeadeAd;
@property (nonatomic, retain) NSNumber * isProduct;
@property (nonatomic, retain) NSNumber * isScroll;
@property (nonatomic, retain) NSString * productId;
@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSNumber * productPrice;
@property (nonatomic, retain) NSNumber * productSection;
@property (nonatomic, retain) NSString * productTitle;
@property (nonatomic, retain) NSNumber * rawPrice;

@end
