//
//  ChangePassViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "ChangePassViewController.h"
#import "SetNewPassViewController.h"
#import "PMToastHint.h"

@interface ChangePassViewController ()
@property(nonatomic,strong)NSTimer *timer;
@property (strong,nonatomic) UILabel *resendCodeLabel;

@end


@implementation ChangePassViewController
{
    NSInteger _timerCount;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"忘记密码";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createChangePassUI];
    [self createButtonControllerUI];
}

// 创建新用户账户和密码
- (void)createChangePassUI
{
    // 创建注册验证码 view
    self.changePassView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 88)];
    self.changePassView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.changePassView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.changePassView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 43, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.changePassView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(0, 88 - 0.5, WIDTH, 0.5);
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.changePassView.layer addSublayer:layer3];
    
    // 手机号码
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(235), 30)];
    mobileLabel.backgroundColor = [UIColor clearColor];
    mobileLabel.text = @"手 机 号 码：";
    mobileLabel.textColor = HDVGFontColor;
    mobileLabel.textAlignment = NSTextAlignmentLeft;
    mobileLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.changePassView addSubview:mobileLabel];
    
    self.mobileTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(280), 7, WidthRate(430), 30)];
    self.mobileTextField.borderStyle = UITextBorderStyleNone;
    self.mobileTextField.delegate = self;
    self.mobileTextField.textAlignment = NSTextAlignmentLeft;
    self.mobileTextField.font = [UIFont systemFontOfSize:15.0f];
    self.mobileTextField.placeholder = @"请输入手机号";
    self.mobileTextField.returnKeyType = UIReturnKeyDone;
    self.mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self.changePassView addSubview:self.mobileTextField];

    // 设置新密码
    UILabel *authcodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 51, WidthRate(220), 30)];
    authcodeLabel.backgroundColor = [UIColor clearColor];
    authcodeLabel.text = @"短信验证码:";
    authcodeLabel.textColor = HDVGFontColor;
    authcodeLabel.textAlignment = NSTextAlignmentLeft;
    authcodeLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.changePassView addSubview:authcodeLabel];
    
    self.authcodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(280), 51, WIDTH - WidthRate(290) - WidthRate(10), 30)];
    self.authcodeTextField.borderStyle = UITextBorderStyleNone;
    self.authcodeTextField.delegate = self;
    self.authcodeTextField.tag = 2;
    self.authcodeTextField.textAlignment = NSTextAlignmentLeft;
    self.authcodeTextField.font = [UIFont systemFontOfSize:15.0f];
    self.authcodeTextField.placeholder = @"请输入验证码";
    self.authcodeTextField.returnKeyType = UIReturnKeyDone;
    self.authcodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self.changePassView addSubview:self.authcodeTextField];
    
    // 获取验证码按钮
    CGFloat X = WIDTH - (WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30));
    CGFloat widtd = WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30) - WidthRate(46);
    self.authcodeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.authcodeButton.frame = CGRectMake(X, 51, widtd, 30);
    [self.authcodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.authcodeButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.authcodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.authcodeButton addTarget:self action:@selector(getAuthcodeClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.authcodeButton setBackgroundColor:ButtonBgColor];
    [self.authcodeButton.layer setCornerRadius:4.0];
    [self.changePassView addSubview:self.authcodeButton];
    
    self.resendCodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(X, 51, widtd, 30)];
    self.resendCodeLabel.textAlignment = NSTextAlignmentCenter;
    self.resendCodeLabel.hidden = YES;
    self.resendCodeLabel.text = @"重发(60)";
    self.resendCodeLabel.textColor = [UIColor darkGrayColor];
    self.resendCodeLabel.font = [UIFont systemFontOfSize:12.0f];
    self.resendCodeLabel.textAlignment = NSTextAlignmentCenter;
    [self.changePassView addSubview:self.resendCodeLabel];
   
}

// 创建按钮控件 UI
- (void)createButtonControllerUI
{
    // 初始化提交按钮
    self.commitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.commitButton.frame = CGRectMake(WidthRate(46), self.changePassView.frame.origin.y + 88 + HeightRate(120), WIDTH - WidthRate(46) * 2, 44);
    [self.commitButton setTitle:@"下一步" forState:UIControlStateNormal];
    [self.commitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.commitButton addTarget:self action:@selector(commitForButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.commitButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [self.commitButton setBackgroundColor:ButtonBgColor];
    [self.commitButton.layer setCornerRadius:6.0f];
    [self.view addSubview:self.commitButton];
    
//    
//    // 重发
//    UILabel *retryLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(420), commitButton.frame.origin.y + HeightRate(120), WidthRate(280), HeightRate(48))];
//    retryLabel.backgroundColor = [UIColor clearColor];
//    retryLabel.text = @"没有收到验证码?";
//    retryLabel.textColor = RGBA(129, 129, 129, 1);
//    retryLabel.textAlignment = NSTextAlignmentRight;
//    retryLabel.font = [UIFont systemFontOfSize:14.0f];
//    [self.view addSubview:retryLabel];
//    
//    UIButton *retryButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    retryButton.frame = CGRectMake(WIDTH - WidthRate(138), commitButton.frame.origin.y + WidthRate(115), WidthRate(92), HeightRate(60));
//    [retryButton setTitle:@"重发" forState:UIControlStateNormal];
//    [retryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [retryButton.titleLabel setFont:[UIFont systemFontOfSize:13.0f]];
//    [retryButton addTarget:self action:@selector(changePassRetryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [retryButton setBackgroundColor:RGBA(218, 67, 80, 1)];
//    [retryButton.layer setCornerRadius:5.0f];
//    [self.view addSubview:retryButton];
    
}

// 按下Done按钮的调用方法，让软键盘隐藏
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.mobileTextField resignFirstResponder];
    [self.authcodeTextField resignFirstResponder];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length - range.length + string.length == 11)
    {
        NSString *mobileNumber;
        if(range.length == 0)
        {
            mobileNumber = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            mobileNumber = [textField.text substringToIndex:11];
        }
        //如果是刚输入完成的输入框是用户昵称的话,就检查用户名
        if(textField == self.mobileTextField)
        {
            /**
             * 检查用户名是否存在
             * 参数: userName:(string)用户名
             */
            NSDictionary *userNameString = @{@"userName":mobileNumber};
            self.networking = [PMNetworking defaultNetworking];
            
            __block NSDictionary *callBackDict;
            
            [self.networking request:PMRequestStateCheckClientUserName WithParameters:userNameString callBackBlock:^(NSDictionary *dict) {
                
                if ([dict isKindOfClass:[NSDictionary class]])
                {
                    callBackDict = dict;
                    
                    // 如果返回的信息中，success的值是true则用户已存在
                    if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1 && textField == self.mobileTextField)
                    {
                        self.tipLabel.hidden = YES;
                        [self.authcodeButton setUserInteractionEnabled:YES];
                        [self.commitButton setUserInteractionEnabled:YES];
                    }
                    else if ([[callBackDict objectNullForKey:@"success"] boolValue] == 0 && textField == self.mobileTextField) {
                        if(!self.tipLabel)
                        {
                            self.tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.mobileTextField.frame.size.width -  WidthRate(200), 0, WidthRate(200), 30)];
                            self.tipLabel.backgroundColor = [UIColor clearColor];
                            self.tipLabel.text = @"* 账号不存在";
                            self.tipLabel.font = [UIFont systemFontOfSize:12.0f];
                            self.tipLabel.textAlignment = NSTextAlignmentRight;
                            self.tipLabel.textColor = [UIColor redColor];
                            [self.mobileTextField addSubview:self.tipLabel];
                        }
                        self.tipLabel.hidden = NO;
                        [self.authcodeButton setUserInteractionEnabled:NO];
                        [self.commitButton setUserInteractionEnabled:NO];
                    }
                }
                
            }showIndicator:NO];
        }
    }
    // 设置键盘最大输入为11位数字
    else if (textField.text.length - range.length + string.length > 11) {
        return NO;
    }
    
    if (self.authcodeTextField.tag == 2 && textField == self.authcodeTextField && textField.text.length - range.length + string.length > 6) {
        return NO;
    }
    
    return YES;
}

#pragma mark - 获取验证码
- (void)getAuthcodeClick:(UIButton *)sender
{
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"])
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的手机号码"];
    }
    else {
        /**
         * 用户输入手机号码获取短信验证码
         * 参数: sendSMS:(string)验证码
         */
        NSDictionary *phoneSendSMS = @{@"mobile":self.mobileTextField.text};
        self.networking = [PMNetworking defaultNetworking];
        
        
        [self.networking request:PMRequestStateSendSMS WithParameters:phoneSendSMS callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                NSInteger messageCode = [[dic objectNullForKey:@"messageCode"] integerValue];
                if (messageCode != 1)
                {
                    [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该号超出每天获取验证码条数"];
                }
                else
                {
                    // 按钮变灰，倒数，“重发”
                    if(!self.timer.isValid)//正在倒数
                    {
                        sender.backgroundColor = [UIColor lightGrayColor];
                        sender.userInteractionEnabled = NO;
                        _timerCount = 60;
                        [sender setTitle:@"重发(60)" forState:UIControlStateNormal];
                        [sender setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCountDown:) userInfo:sender repeats:YES];
                    }
                }
            }
            
        }showIndicator:NO];
        
    }
}


#pragma mark  获取验证码按钮倒数
- (void)timerCountDown:(NSTimer *)timer
{
    UIButton *btn = timer.userInfo;
    
    if(_timerCount == 0)
    {
        //停止运行timer
        [timer invalidate];
        //按钮恢复成红色
        
        [btn setBackgroundColor:RGBA(218, 67, 80, 1)];
        self.resendCodeLabel.hidden = YES;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:@"重发" forState:UIControlStateNormal];
        btn.userInteractionEnabled = YES;
    }
    else
    {
        _timerCount --;
        self.resendCodeLabel.hidden = NO;
        self.resendCodeLabel.text = [NSString stringWithFormat:@"重发(%ld)",(long)_timerCount];
        [btn setTitle:@"" forState:UIControlStateNormal];
        //        [btn setTitle:[NSString stringWithFormat:@"重 发(%ld)",(long)_timerCount] forState:UIControlStateNormal];
    }
}

- (void)commitForButtonClick:(id)sender
{
    // 判断是否为空
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"]) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的手机号码"];
        return;
    }
    if (self.authcodeTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入获取到的手机验证码"];
        return;
    }
    
    /**
     * 用户忘记密码，获取短信验证码后进入设置新密码
     * 参数: getUserInfo:(string)验证码
     */
    NSDictionary *getUserInfo = @{@"code":self.mobileTextField.text, @"verificationCode":self.authcodeTextField.text};
    self.networking = [PMNetworking defaultNetworking];
    
    __block NSDictionary *callBackDict;
    
    [self.networking request:PMRequestStateGetUserInfo WithParameters:getUserInfo callBackBlock:^(NSDictionary *dict) {
        
        callBackDict = dict;
        if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
            
            SetNewPassViewController *setNewPsaaVC = [[SetNewPassViewController alloc] init];
            setNewPsaaVC.accountString = self.mobileTextField.text;
            setNewPsaaVC.authcodeString = self.authcodeTextField.text;
            [self.navigationController pushViewController:setNewPsaaVC animated:YES];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }showIndicator:NO];
}
//
//// 如果60s后没有收到验证码，重新获取验证码
//- (void)changePassRetryButtonClick:(id)sender
//{
//    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"]) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入正确的手机号码" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
//        [alertView show];
//    }
//    else if (self.mobileTextField.text.length == 11 && [self.mobileTextField.text hasPrefix:@"1"]) {
//        
//        /**
//         * 用户输入手机号码获取短信验证码
//         * 参数: sendSMS:(string)验证码
//         */
//        NSDictionary *phoneSendSMS = @{@"mobile":self.mobileTextField.text};
//        self.networking = [PMNetworking defaultNetworking];
//        
//        __block NSDictionary *callBackDict;
//        
//        [self.networking request:PMRequestStateSendSMS WithParameters:phoneSendSMS callBackBlock:^(NSDictionary *dict) {
//            
//            callBackDict = dict;
//        }];
//    }
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
