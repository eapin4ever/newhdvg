//
//  PMPrepayTableViewController.m
//  changeViewController
//
//  Created by pmit on 15/2/5.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMPrepayTableViewController.h"
#import "PMPrebuyDetailCell.h"

@interface PMPrepayTableViewController ()
@property(nonatomic,strong)NSArray *dicArray;
@end

@implementation PMPrepayTableViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self initFakeModels];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预购最划算";
    
    [self.tableView registerClass:[PMPrebuyDetailCell class] forCellReuseIdentifier:@"Cell"];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = HDVGPageBGGray;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self createHeaderView];
}

- (void)createHeaderView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(300))];
    UIImageView *iv = [[UIImageView alloc] initWithFrame:view.frame];
    iv.image = [UIImage imageNamed:@"prebuyBanner"];
    [view addSubview:iv];
    
    self.tableView.tableHeaderView = view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = self.dicArray[indexPath.section];
    
    PMPrebuyDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    [cell createUI];
    
    [cell setTitle:[dic objectNullForKey:@"title"] time:[dic objectNullForKey:@"time"] price:[dic objectNullForKey:@"price"] prepay:[dic objectNullForKey:@"prepay"]];

    cell.iv.image = [dic objectNullForKey:@"logo"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)initFakeModels
{
    self.dicArray = @[@{@"logo":[UIImage imageNamed:@"product_first"],@"title":@"正品代购香奈儿chanel摩登可可小姐COCO女淡/浓香水EDP50ml 100ml",@"time":@"23:18:39",@"price":@"￥899",@"prepay":@"269"},@{@"logo":[UIImage imageNamed:@"product_second"],@"title":@"帮宝适 超薄干爽超大彩箱装中号纸尿裤尿不湿M192片",@"time":@"18:34:12",@"price":@"￥235",@"prepay":@"70"},@{@"logo":[UIImage imageNamed:@"product_third"],@"title":@"Canon/佳能 IXUS 145 数码 高清 长焦卡片机 普通家用照相机145",@"time":@"14:12:33",@"price":@"￥869",@"prepay":@"260"}];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 256/2;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}



@end
