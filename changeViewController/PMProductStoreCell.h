//
//  PMProductStoreCell.h
//  changeViewController
//
//  Created by pmit on 15/1/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "YBShareBtn.h"

@interface PMProductStoreCell : UITableViewCell
@property(nonatomic,strong)YBShareBtn *addToShopBtn;
@property(nonatomic,strong)UIImageView *iv;
@property (nonatomic,strong) UIButton *detailBtn;

- (void)createUI;
- (void)setContentTitle:(NSString *)title location:(NSString *)location sellNum:(id)num rate:(id)rate price:(id)price;

@end
