//
//  MyTradeTableViewCell.h
//  changeViewController
//
//  Created by P&M on 14/12/2.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MyTradeTableViewCell : UITableViewCell

@property (strong, nonatomic) UIImageView *iv;
@property (strong, nonatomic) UIButton *goDetailBtn;
@property (strong, nonatomic) UIButton *turnBackBtn;

- (void)setMyTradeOrderDataUI;
- (void)setUnreadMessagesTimeUI;

// 设置我的交易详情的商品图片、商品标题、商品数量、商品价格数据
- (void)setTitle:(NSString *)goodsTitle setSpecification:(NSString *)specification setNumber:(NSInteger)goodsNumber setPrice:(double)goodsPrice;


// 设置交易详情的商品图片、商品标题、商品分类、商品数量数据
// 设置客户服务投诉的商品图片、商品标题、商品分类、商品数量数据
- (void)setTitle:(NSString *)goodsTitle setClssify:(NSString *)goodClassify setNumber:(NSInteger)goodsNumber;

- (void)setImage:(NSString *)urlString setTitle:(NSString *)goodsTitle setClssify:(NSString *)goodClassify setNumber:(NSInteger)goodsNumber;

// 设置未读消息时间数据
- (void)setUnreadMessagesTime:(NSString *)time;

@end
