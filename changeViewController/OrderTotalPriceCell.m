//
//  OrderTotalPriceCell.m
//  changeViewController
//
//  Created by pmit on 15/10/29.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "OrderTotalPriceCell.h"
#import "PMMyPhoneInfo.h"

@implementation OrderTotalPriceCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.totalTitleLB)
    {
        NSString *titleString = @"实付款：";
        CGSize titleSize = [titleString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0f]} context:nil].size;
        self.totalTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, titleSize.width, 20)];
        self.totalTitleLB.font = [UIFont systemFontOfSize:16.0f];
        self.totalTitleLB.textAlignment = NSTextAlignmentLeft;
        self.totalTitleLB.textColor = [UIColor blackColor];
        self.totalTitleLB.text = titleString;
        [self.contentView addSubview:self.totalTitleLB];
        
        self.totalPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.totalTitleLB.frame), 10, WIDTH - CGRectGetMaxX(self.totalTitleLB.frame) - 15, 20)];
        self.totalPriceLB.textColor = RGBA(224, 28, 46, 1);
        self.totalPriceLB.textAlignment = NSTextAlignmentRight;
        self.totalPriceLB.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:self.totalPriceLB];
    }
}

- (void)setCellData:(double)price
{
    NSString *priceString = [NSString stringWithFormat:@"￥%.2lf",price];
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} range:NSMakeRange(0, 1)];
    self.totalPriceLB.attributedText = nodeString;
}

@end
