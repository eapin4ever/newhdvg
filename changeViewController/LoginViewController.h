//
//  LoginViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/22.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "MD5Util.h"
#import "PMUserInfos.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate>

//用于判断是不是被present出来的
@property(nonatomic,assign)BOOL isPresented;
//用于present时的右侧取消登录按钮事件
- (void)cancelLogin:(UIBarButtonItem *)bbi;

+ (LoginViewController *)shareInstance;
@end
