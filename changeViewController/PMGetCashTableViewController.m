//
//  PMGetCashTableViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMGetCashTableViewController.h"

@interface PMGetCashTableViewController ()

@end

@implementation PMGetCashTableViewController

static PMGetCashTableViewController *_getCashVC;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"提现明细";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

+ (PMGetCashTableViewController *)shareInstance
{
    if (!_getCashVC)
    {
        _getCashVC = [[PMGetCashTableViewController alloc] init];
    }
    return _getCashVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self initWithdrawalDetailData];
    [self initWithdrawalDetailDataList];
    
    [self.tableView registerClass:[PMDistrubutionTableViewCell class] forCellReuseIdentifier:@"withdrawalDetailCell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.withdrawalMoney == YES) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.withdrawalDetailArray count];
}

// set Cell height
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return HeightRate(88);
//}

#pragma mark - section For Header
// 设置 section 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(110);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(110))];
    self.headerView.backgroundColor = HDVGPageBGGray;
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, HeightRate(109), WIDTH, HeightRate(1));
    layer.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.headerView.layer addSublayer:layer];
    
    // 创建时间
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(30), WidthRate(230), HeightRate(50))];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.text = @"创建时间";
    self.timeLabel.textColor = RGBA(65, 65, 65, 1);
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.font = [UIFont systemFontOfSize:13.0f];
    [self.headerView addSubview:self.timeLabel];
    
    // 提款金额
    self.orderMoneyLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(280), HeightRate(30), WidthRate(210), HeightRate(50))];
    self.orderMoneyLab.backgroundColor = [UIColor clearColor];
    self.orderMoneyLab.text = @"金额（元）";
    self.orderMoneyLab.textColor = RGBA(65, 65, 65, 1);
    self.orderMoneyLab.textAlignment = NSTextAlignmentCenter;
    self.orderMoneyLab.font = [UIFont systemFontOfSize:13.0f];
    [self.headerView addSubview:self.orderMoneyLab];
    
    // 交易状态
    self.stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(490), HeightRate(30), WidthRate(230), HeightRate(50))];
    self.stateLabel.backgroundColor = [UIColor clearColor];
    self.stateLabel.text = @"状态";
    self.stateLabel.textColor = RGBA(65, 65, 65, 1);
    self.stateLabel.textAlignment = NSTextAlignmentCenter;
    self.stateLabel.font = [UIFont systemFontOfSize:13.0f];
    [self.headerView addSubview:self.stateLabel];
    
    return self.headerView;
}

// 设置 footer 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return HeightRate(1);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(1))];
    footerView.backgroundColor = RGBA(200, 200, 200, 1);
    
    return footerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMDistrubutionTableViewCell *withdrawalCell = [tableView dequeueReusableCellWithIdentifier:@"withdrawalDetailCell"];
    [withdrawalCell setWithdrawalDetailDataUI];
    NSDictionary *dict = self.withdrawalDetailArray[indexPath.row];
    
    // 获取下来的数据
    
    [withdrawalCell setTime:[NSString stringWithFormat:@"%@",[dict objectNullForKey:@"createDt"]] setWithdrawalMoney:[[dict objectNullForKey:@"money"] doubleValue] setState:[dict objectNullForKey:@"statusStr"]];
//    [withdrawalCell setTime:[dict objectNullForKey:@"createDt"] setWithdrawalMoney:[dict objectNullForKey:@"money"] setState:[dict objectNullForKey:@"statusStr"]];
    
    if (indexPath.section == 0) {
        // 告诉用户还有下一个页面
        withdrawalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    withdrawalCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return withdrawalCell;
}

#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WithdrawalDetailViewController *withdrawalDetailVC = [[WithdrawalDetailViewController alloc] init];
    withdrawalDetailVC.detailDict = self.withdrawalDetailArray[indexPath.row];
    [self.navigationController pushViewController:withdrawalDetailVC animated:YES];
    
    return nil;
}


#pragma mark - 代言提现明细数据
- (void)initWithdrawalDetailDataList
{
    // 获取用户token
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    
    // 如果用户还没有登录 userInfos 为空
    if (userInfos.PM_SID == nil) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"你还没有登录，请先登录" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
    }
    else {
        
        //提现明细
        /*
         参数：
         PM_SID        Y    String      Token
         currentPage   N    int         当前页
         createDt      N    date        开始时间
         createDt2     N    date        结束时间
         */
        
        int currentPage = 1;
        
        NSDictionary *showMyOrderList = @{@"PM_SID":userInfos.PM_SID, @"currentPage":@(currentPage)};
        self.networking = [PMNetworking defaultNetworking];
        
        [self.networking request:PMRequestStateShareShowGetMoney WithParameters:showMyOrderList callBackBlock:^(NSDictionary *dict) {
            [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                
                if ([[dict objectNullForKey:@"success"] integerValue] == 0) {
                    
                    // 还没有提现明细数据
                    [self createNoDataView];
                }
                else {
                    self.withdrawalDetailArray = [[[dict objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                    
                    for (NSDictionary *dict in self.withdrawalDetailArray) {
                        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                            
                        }];
                    }
                }
                [self.tableView reloadData];
            }];
        }showIndicator:YES];
    }
}

#pragma mark - 创建没有数据View
- (void)createNoDataView
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    noDataView.backgroundColor = [UIColor whiteColor];
    UIImageView *noIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noDataView.frame.size.height)];
    noIV.image = [UIImage imageNamed:@"getcash_null"];
    noIV.contentMode = UIViewContentModeScaleAspectFit;
    [noDataView addSubview:noIV];
    
    [self.view insertSubview:noDataView atIndex:1000];
}

#pragma mark 返回 消除掉当前页面
- (void)backClick:(UIBarButtonItem *)bbi
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


#pragma mark - 设置代言提现明细假数据
- (void)initWithdrawalDetailData
{
    NSArray *timeArr = [NSArray arrayWithObjects:@"2014-12-24",@"2014-10-22",@"2014-06-30",@"2014-06-12",@"2014-01-12", nil];
    NSArray *moneyArr = [NSArray arrayWithObjects:@"¥1155.00",@"¥1265.00",@"¥155.00",@"¥1895.00",@"¥15.00", nil];
    NSArray *stateArr = [NSArray arrayWithObjects:@"已完成",@"处理中",@"取消",@"审核中",@"已完成", nil];
    
    if (!self.withdrawalDetailArray) {
        self.withdrawalDetailArray = [NSMutableArray array];
        
        for (int i = 0; i < 5; i++) {
            NSMutableDictionary *dict = [@{@"createDt":timeArr[i],  @"money":moneyArr[i], @"statusStr":stateArr[i], @"bank":@"中国农业银行",@"bankCardNum":@"622202360222020202", @"serialNumber":@"201404121412160065"} mutableCopy];
            
            [self.withdrawalDetailArray addObject:dict];
        }
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
