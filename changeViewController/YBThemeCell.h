//
//  YBThemeCell.h
//  changeViewController
//
//  Created by ZhangEapin on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBThemeCell : UITableViewCell

@property (strong,nonatomic) UIImageView *upLeftIV;
@property (strong,nonatomic) UIImageView *upRightIV;
@property (strong,nonatomic) UIImageView *downLeftIV;
@property (strong,nonatomic) UIImageView *downRightIV;

@property (strong,nonatomic) UIButton *upLeftBtn;
@property (strong,nonatomic) UIButton *upRightBtn;
@property (strong,nonatomic) UIButton *downLeftBtn;
@property (strong,nonatomic) UIButton *downRightBtn;


- (void)createUI;
- (void)addTarget:(id)target selector:(SEL)sel;

@end
