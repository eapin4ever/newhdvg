//
//  PMCurrentGoodsTableViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMCurrentGoodsTableViewController.h"
#import "PMUnsoldGoodsTableViewController.h"
#import "PMDistributionsGoodsViewController.h" 
#import "PMToastHint.h"
#import "PMMyGoodsViewController.h"
#import "PMDistributionGoodsCell.h"

@interface PMCurrentGoodsTableViewController ()<UITextFieldDelegate>
@property(nonatomic,strong)UITextField *searchTF;

@property(nonatomic,assign)NSInteger totalPages;
@property(nonatomic,assign)NSInteger currentPage;
@property (strong,nonatomic) NSMutableArray *hasRefreshArr;
@property (strong,nonatomic) NSMutableArray *selectedArr;

@end

static NSString *const normalCell = @"Cell";
static NSString *const shareCell = @"shareCell";
static PMCurrentGoodsTableViewController *_currentVC;
@implementation PMCurrentGoodsTableViewController
+ (PMCurrentGoodsTableViewController *)currentVC
{
    return _currentVC;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"下架选中商品";
        self.view.frame = CGRectMake(0, 40, WIDTH, HEIGHT - 64 - 25);
        _currentVC = self;
        self.allRowsIndexPath = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isLoad = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(seeSearchView:) name:@"mySearchView" object:nil];
    
//    [self.tableView registerClass:[PMDistributionsGoodsTableViewCell class] forCellReuseIdentifier:normalCell];
    [self.tableView registerClass:[PMDistributionGoodsCell class] forCellReuseIdentifier:normalCell];
    self.tableView.backgroundColor = HDVGPageBGGray;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
//    self.tableView.allowsMultipleSelectionDuringEditing = YES;
//    self.tableView.editing = YES;
    self.hasRefreshArr = [NSMutableArray array];
    self.selectedArr = [NSMutableArray array];
//    [self initTableHeaderView];
    [self addRefreshAction];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ((PMDistributionsGoodsViewController *)self.parentViewController).noResultView.hidden = YES;
    self.isSelectAll = NO;
    
    [self getModelsBySearchString:nil finish:nil error:nil];
    [self.tableView reloadData];
    [self refreshAllIndexPathArray];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMDistributionGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [cell createUI];
    
    cell.isDown = NO;
    
    NSDictionary *dic = self.listArray[indexPath.section];
    CGFloat inputPrice = [[dic objectNullForKey:@"input"] doubleValue];
    
    [cell.iv sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    [cell setContentTitle:[dic objectNullForKey:@"productName"] unitPrice:[dic objectNullForKey:@"productPrice"] percent:inputPrice];
    
    cell.shareBtn.hidden = NO;
    cell.deleteBtn.hidden = YES;
//    cell.deleteBtn.tag = indexPath.section;
    cell.goDetailBtn.tag = indexPath.section;
    cell.shareBtn.tag = indexPath.section;
    cell.seletedBtn.tag = indexPath.section * 100000 + 10000;
    [cell.goDetailBtn addTarget:self action:@selector(showProductDetail:) forControlEvents:UIControlEventTouchUpInside];
    [cell.shareBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.seletedBtn addTarget:self action:@selector(seletedCurrent:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([self.selectedArr containsObject:dic])
    {
        cell.seletedBtn.selected = YES;
    }
    else
    {
        cell.seletedBtn.selected = NO;
    }
    
    if (indexPath.section == self.listArray.count - 10)
    {
        if (![self.hasRefreshArr containsObject:@(indexPath.section)])
        {
            [self.hasRefreshArr addObject:@(indexPath.section)];
            
            if(self.currentPage < self.totalPages)//如果小于总页数则刷新
            {
                self.isLoad = YES;
                //获取数据
                [self getModelsBySearchString:self.searchTF.text finish:^{
                    
                    [self.tableView reloadData];
                    
                } error:^{
                    
                }];
            }
            
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
       return WidthRate(200) + 50;
    else
        return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *noUserView = [[UIView alloc] init];
    noUserView.backgroundColor = HDVGPageBGGray;
    return noUserView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return 5;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
        return YES;
    else
        return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.6;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedIndexPaths addObject:indexPath];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedIndexPaths removeObject:indexPath];
    self.isSelectAll = NO;
    [((PMDistributionsGoodsViewController *)self.parentViewController).selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
}


#pragma mark  区尾
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(16))];
    footerView.backgroundColor = [UIColor clearColor];
    
    return footerView;
}

#pragma mark - 点击事件
#pragma mark 点击跳转到商品详情
- (void)showProductDetail:(UIButton *)sender
{
    NSDictionary *dic = self.listArray[sender.tag];
    PMMyGoodsViewController *vc = [[PMMyGoodsViewController alloc] init];
    vc.productId = [dic objectForKey:@"productId"];
    vc.shopId = [dic objectNullForKey:@"shopId"];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];
}

#pragma mark  点击分享按钮执行方法
- (void)clickAction:(UIButton *)sender
{
    //得到section
    NSInteger section = sender.tag;
    //得到对应的数据
    NSDictionary *dic = self.listArray[section];

    [self initShareMessage:sender data:dic];

}

#pragma mark - 表头和表尾
- (void)initTableHeaderView
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    headerView.backgroundColor = RGBA(231, 231, 231, 1);
    
    UILabel *searchLB1 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(100), 10, WidthRate(120), 10)];
    searchLB1.text = @"搜索商品";
    searchLB1.font = [UIFont systemFontOfSize:9.0f];
    searchLB1.textAlignment = NSTextAlignmentCenter;
    searchLB1.textColor = RGBA(83, 83, 83, 1);
    [headerView addSubview:searchLB1];
    
    UILabel *searchLB2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(100), 20, WidthRate(120), 10)];
    searchLB2.text = @"SEARCH";
    searchLB2.font = [UIFont systemFontOfSize:9.0f];
    searchLB2.textAlignment = NSTextAlignmentCenter;
    searchLB2.textColor = RGBA(135, 135, 135, 1);
    [headerView addSubview:searchLB2];
    
    
    //输入框
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH/2 - WidthRate(160), 8, WidthRate(320), 24)];
    self.searchTF = tf;
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:12.0f];
    tf.placeholder = @" 请输入商品名称";
    
    [tf setValue:[UIFont boldSystemFontOfSize:12.0f] forKeyPath:@"_placeholderLabel.font"];
    [tf setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, tf.bounds.size.width, tf.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    
    tf.returnKeyType = UIReturnKeySearch;
    //使用leftView让输入的文字缩进
    tf.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 1)];
    tf.leftViewMode = UITextFieldViewModeAlways;
    tf.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:tf];
    
    
    UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(tf.frame.origin.x + tf.bounds.size.width, 8, WidthRate(80), 24)];
    searchBtn.backgroundColor = RGBA(218, 67, 80, 1);
    [headerView addSubview:searchBtn];
    
    //放大镜
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(40) - 7, 5, 14, 14)];
    iv.image = [UIImage imageNamed:@"search_white"];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    
    [searchBtn addSubview:iv];
    

    self.tableView.tableHeaderView = headerView;
    self.tableView.tableFooterView = [[UIView alloc] init];
}


#pragma mark - 全选  下架
- (void)selectAllRows:(UIButton *)sender
{
    if(self.isSelectAll)
    {
        self.isSelectAll = NO;
        [sender setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
        self.selectedArr = [NSMutableArray array];
        for (NSInteger i = 0; i < self.listArray.count; i++)
        {
            PMDistributionGoodsCell *cell = (PMDistributionGoodsCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
            cell.seletedBtn.selected = NO;
        }
        
    }
    else
    {
        self.isSelectAll = YES;
        [sender setImage:[UIImage imageNamed:@"selectAll_1"] forState:UIControlStateNormal];
        for (NSInteger i = 0; i < self.listArray.count; i++)
        {
            NSDictionary *carProductDic = self.listArray[i];
            if ([self.selectedArr containsObject:carProductDic])
            {
                continue;
            }
            else
            {
                [self.selectedArr addObject:carProductDic];
            }
            
            PMDistributionGoodsCell *cell = (PMDistributionGoodsCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
            cell.seletedBtn.selected = YES;
            [self.tableView reloadData];
        }
    }
    
}


- (void)unsoldSelectedGoods
{
    PMUnsoldGoodsTableViewController *vc = [PMUnsoldGoodsTableViewController currentVC];

//    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    
    
    //需要同时删除多行，否则会崩溃
    //要注意的是，这里是删除区，不是行
//    for (NSIndexPath *indexPath in self.selectedIndexPaths)
//    {
//        [indexSet addIndex:indexPath.section];
//    }
    
    //得到选中的数组
//    NSArray *selectedArr = [self.listArray objectsAtIndexes:indexSet];
    
    //判断是否有选中商品
    if(self.selectedArr.count == 0)
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请选择要下架的商品"];
        return;
    }
    
    //向服务器发送请求，请求成功后下架
    [self unsoldSelectedGoods:self.selectedArr FinishAction:^{
        [vc.listArray addObjectsFromArray:self.selectedArr];

        [self.listArray removeObjectsInArray:self.selectedArr];

//        [self.selectedArr removeAllObjects];
        self.selectedArr = [NSMutableArray array];
        
        [self.tableView reloadData];
        
        if(self.isSelectAll)
        {
            //重新请求数据
            [self getModelsBySearchString:self.searchTF.text finish:nil error:nil];
            //取消全选
            [((PMDistributionsGoodsViewController *)self.parentViewController).selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
            self.isSelectAll = NO;
        }
    }];
    
}

#pragma mark - 搜索
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self getModelsBySearchString:textField.text finish:nil error:nil];
    return YES;
}

#pragma mark 收键盘
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchTF resignFirstResponder];
    if (scrollView == self.tableView)
    {
        CGFloat sectionHeaderHeight = 5;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}



#pragma mark  刷新所有的区行indexPath
- (void)refreshAllIndexPathArray
{
    [self.allIndexPaths removeAllObjects];
    
    for(int i = 0;i < self.listArray.count;i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:i];
        [self.allIndexPaths addObject:indexPath];
    }
}


#pragma mark - 添加刷新机制
- (void)addRefreshAction
{
    __weak PMCurrentGoodsTableViewController *weakSelf = self;
    //使用普通的下拉刷新
    [self.tableView addRefreshHeaderViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        weakSelf.isLoad = NO;
        //重新请求
        [weakSelf getModelsBySearchString:weakSelf.searchTF.text finish:^{
            weakSelf.hasRefreshArr = [NSMutableArray array];
            [weakSelf.tableView reloadData];
            //请求成功,显示刷新成功
            [weakSelf.tableView headerEndRefreshingWithResult:JHRefreshResultSuccess];
        } error:^{
            //请求失败,显示刷新失败
            [weakSelf.tableView headerEndRefreshingWithResult:JHRefreshResultFailure];
        }];
        
    }];
    
    
    //使用普通的上拉加载
//    [self.tableView addRefreshFooterViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
//        
//        if(weakSelf.currentPage < weakSelf.totalPages)//如果小于总页数则刷新
//        {
//            weakSelf.isLoad = YES;
//            //获取数据
//            [weakSelf getModelsBySearchString:weakSelf.searchTF.text finish:^{
//                
//                [weakSelf.tableView reloadData];
//                //加载刷新
//                [weakSelf.tableView footerEndRefreshing];
//                
//            } error:^{
//                //不用刷新
//                [weakSelf.tableView footerEndRefreshing];
//            }];
//        }
//        else
//        {
//            //不用刷新
//            [weakSelf.tableView footerEndRefreshing];
//        }
//    }];
}


#pragma mark - 获取代言中商品的数据
- (void)getModelsBySearchString:(NSString *)searchString finish:(myFinishBlock)finish error:(myFinishBlock)error
{
    
    //显示代言中商品/下架商品
    /*
     PM_SID         Y	String		Token
     currentPage	Y	Int         当前页
     productName	N	String		商品名称，用于搜索
     flag           Y	String		1: 代言中商品  -1: 下架商品
     */
    
    if (self.isSearch)
    {
        self.hasRefreshArr = [NSMutableArray array];
        self.isSearch = NO;
    }
    
    if(self.isLoad)
    {
        self.currentPage++;
    }
    else
    {
        self.currentPage = 1;
    }
    
    NSDictionary *param;
    if(searchString == nil || [searchString isEqual:@""])
    {
        param = @{PMSID,@"currentPage":@(self.currentPage),@"pageSize":@(20),@"flag":@"1"};
    }
    else
    {
        param = @{PMSID,@"currentPage":@(self.currentPage),@"pageSize":@(20),@"flag":@"1",@"productName":searchString};
    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateShareShowSellsProducts WithParameters:param callBackBlock:^(NSDictionary *dic) {
        
        if (intSuccess == 1)
        {
            if(self.isLoad == NO)self.listArray = [NSMutableArray array];
            [self.listArray addObjectsFromArray:[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"]];
            //获得总页数
            self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
            [self.tableView reloadData];
            //初始化并刷新所有行的indexPath
            self.selectedIndexPaths = [NSMutableArray array];
            self.allIndexPaths = [NSMutableArray array];
            [self refreshAllIndexPathArray];
            
            if(finish)
                finish();
            [((PMDistributionsGoodsViewController *)self.parentViewController) showNoResultViewWithtitle:nil AndIsSold:3];
        }
        else
        {
            if(error)
                error();
            [((PMDistributionsGoodsViewController *)self.parentViewController) showNoResultViewWithtitle:@"没有代言中商品\n去商品库添加几个呗" AndIsSold:1];
        }
    }showIndicator:!self.isLoad];
}

#pragma mark - 将选中的商品下架
- (void)unsoldSelectedGoods:(NSArray *)productArr FinishAction:(finishAction)finish
{
    //上架、下架
    /*
     PM_SID         Y	String		Token
     status         Y	String		标志  1：上架   -1：下架
     productIds     N	String		多个商品Id的拼接，如04652eb5e5fa4d1a9153730d7238857f,04855d5313af43df80d4766707def575
     */
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dic in productArr)
    {
        [arr addObject:[dic objectNullForKey:@"productId"]];
    }
    NSString *productIds = [arr componentsJoinedByString:@","];
    
    
    NSDictionary *param = @{PMSID,@"status":@"-1",@"productIds":productIds};
    [[PMNetworking defaultNetworking] request:PMRequestStateShareUpdateBatchDown WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            finish();
        }
        else
        {
            showRequestFailAlertView;
        }
    }showIndicator:YES];
}

#pragma mark - 创建分享内容
- (void)initShareMessage:(id)sender data:(NSDictionary *)dic
{
    NSString *urlString = [[PMNetworking defaultNetworking] shareShopProductWithProductId:[dic objectNullForKey:@"productId"] shopId:[dic objectNullForKey:@"shopId"]];
    
    
    //1+创建弹出菜单容器（iPad必要）
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionDown];
    
    
    NSString *content = [dic objectNullForKey:@"productName"];
//    id<ISSCAttachment> shareImage = [ShareSDK imageWithUrl:[dic objectNullForKey:@"productLogo"]];
    NSData *data = UIImageJPEGRepresentation([UIImage imageNamed:@"noImage"], 1.0);
    id<ISSCAttachment> shareImage = (isNull([dic objectNullForKey:@"productLogo"]) || [[dic objectNullForKey:@"productLogo"] isEqualToString:@""]) ? [ShareSDK imageWithData:data fileName:@"noImage" mimeType:@"jpg"]:[ShareSDK imageWithUrl:[dic objectNullForKey:@"productLogo"]];
    NSString *shareTitle = [dic objectNullForKey:@"productName"];
    
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:shareTitle
                                       defaultContent:@"恒大微购--移动商城"
                                                image:shareImage
                                                title:shareTitle
                                                  url:urlString
                                          description:@""
                                            mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    if (type == ShareTypeCopy)
                                    {
                                        [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view ByToast:@"商品地址已复制"];
                                    }
                                    else
                                    {
                                        [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view  ByToast:@"分享成功"];
                                    }
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
                                    
                                    if([[error errorDescription] rangeOfString:@"Insufficient app permissions"].length>0)
                                    {
                                        //新浪分享失败,重新分享不带图片的
                                        [self shareToSinaWithContainner:container content:content title:shareTitle url:urlString];
                                    }
                                    else
                                    {
                                        if (type == ShareTypeCopy)
                                        {
                                            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view  ByToast:@"商品地址复制失败"];
                                        }
                                        else
                                        {
                                            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view  ByToast:@"分享失败"];
                                        }
                                    }
                                }
                            }];
    
}

#pragma mark - 暂时的补救措施，新浪微博暂时不能分享带图片的
- (void)shareToSinaWithContainner:(id<ISSContainer>)container content:(NSString *)content title:(NSString *)shareTitle url:(NSString *)urlString
{
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:content
                                       defaultContent:@"恒大微购--移动商城"
                                                image:nil
                                                title:shareTitle
                                                  url:urlString
                                          description:@"有种别看我"
                                            mediaType:SSPublishContentMediaTypeNews];
    
    //不弹选择窗，直接分享
    [ShareSDK shareContent:publishContent type:ShareTypeSinaWeibo authOptions:nil shareOptions:nil statusBarTips:YES result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        if (state == SSResponseStateSuccess)
        {
            NSLog(@"分享成功");
            [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view  ByToast:@"分享成功"];
            
        }
        else if (state == SSResponseStateFail)
        {
            NSLog(@"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view  ByToast:@"分享失败"];
        }
    }];
}

- (void)seeSearchView:(NSNotification *)notification
{
    BOOL isViewShow = [[notification object] boolValue];
    if (isViewShow)
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.view.frame = CGRectMake(0, 80, WIDTH, HEIGHT - 64 - 80 - 50);
        }];
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.view.frame = CGRectMake(0, 40, WIDTH, HEIGHT - 64 - 40 - 50);
        }];
    }
}

- (void)seletedCurrent:(UIButton *)sender
{
    NSInteger index = (sender.tag - 10000) / 100000;
    NSDictionary *dic = [self.listArray objectAtIndex:index];
    sender.selected = !sender.selected;
    if (sender.selected)
    {
        if (![self.selectedArr containsObject:dic])
        {
            [self.selectedArr addObject:dic];
        }
    }
    else
    {
        [self.selectedArr removeObject:dic];
    }
    
    if (self.selectedArr.count == self.listArray.count)
    {
        [((PMDistributionsGoodsViewController *)self.parentViewController).selectAllBtn setImage:[UIImage imageNamed:@"selectAll_1"] forState:UIControlStateNormal];
        self.isSelectAll = YES;
    }
    else
    {
        [((PMDistributionsGoodsViewController *)self.parentViewController).selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
        self.isSelectAll = NO;
    }
}

@end
