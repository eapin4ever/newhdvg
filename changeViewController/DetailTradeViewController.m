//
//  DetailTradeViewController.m
//  changeViewController
//
//  Created by pmit on 15/11/3.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "DetailTradeViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMOrderTimeStatesCell.h"
#import "MyTradeTableViewCell.h"
#import "PMOrderBtnCell.h"
#import "YBTurnBackViewController.h"
#import "MasterViewController.h"
#import "ZSTChatroomLoadMoreView.h"
#import "ComplaintsViewController.h"
#import "YBComplaintsViewController.h"
#import "YBBillInfos.h"
#import "YBSureOrderViewController.h"
#import "PMToastHint.h"
#import "PMOrderDetailViewController.h"

@interface DetailTradeViewController () <UITableViewDataSource,UITableViewDelegate,PMOrderBtnCellDelegate,ComplaintsViewControllerDelegate,YBComplaintsViewControllerDelegate,PMOrderBtnCellDelegate,PMOrderDetailViewControllerDelegate>

@property (strong,nonatomic) NSMutableArray *listArr;
@property (assign,nonatomic) NSInteger currentPage;
@property (strong,nonatomic) UITableView *myTradeTableView;
@property (strong,nonatomic) NSMutableArray *allArray;
@property (assign,nonatomic) NSInteger totalPages;
@property (strong,nonatomic) UIView *noResultView;
@property (assign,nonatomic) BOOL isHasMore;
@property (assign,nonatomic) BOOL isLoding;
@property (strong,nonatomic) ZSTChatroomLoadMoreView *loadMoreView;

@end

@implementation DetailTradeViewController

static NSString *const orderTimeCell = @"orderTimeCell";
static NSString *const tradeCellIdetify = @"tradeCell";
static NSString *const orderBtnCell = @"orderBtnCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listArr = [NSMutableArray array];
    self.allArray = [NSMutableArray array];
    self.currentPage = 1;
    [self createUI];
    [self createTableView];
    [self createNoResult];
    [self getNetWork];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createUI
{
    switch (self.tradeStatus)
    {
        case tradeStatusWaitPay:
            self.title = @"待付款";
            break;
        case tradeStatusWaitGet:
            self.title = @"待收货";
            break;
        case tradeStatusReceive:
            self.title = @"待评价";
            break;
        case tradeStatusTurnBack:
            self.title = @"退货";
            break;
        default:
            break;
    }
    
    if (self.isFromPay)
    {
        UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@"回到首页" style:UIBarButtonItemStylePlain target:self action:@selector(backToRoot:)];
        self.navigationItem.leftBarButtonItem = backBtn;
    }
}

- (void)backToRoot:(UIBarButtonItem *)button
{
    //    [self.navigationController popToRootViewControllerAnimated:NO];
    [[MasterViewController defaultMasterVC] transitionToHomePage];
    [self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma mark - 创建表格
- (void)createTableView
{
    self.myTradeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    if (self.tradeStatus == tradeStatusTurnBack)
    {
        self.myTradeTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50);
    }
    self.myTradeTableView.delegate = self;
    self.myTradeTableView.dataSource = self;
    [self.myTradeTableView registerClass:[PMOrderTimeStatesCell class] forCellReuseIdentifier:orderTimeCell];
    [self.myTradeTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:tradeCellIdetify];
    [self.myTradeTableView registerClass:[PMOrderBtnCell class] forCellReuseIdentifier:orderBtnCell];
    self.myTradeTableView.backgroundColor = HDVGPageBGGray;
    
    self.myTradeTableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.myTradeTableView];
    
    if (self.tradeStatus == tradeStatusTurnBack)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50, WIDTH, 50)];
        view.backgroundColor = RGBA(202, 45, 52, 1);
        [self.view addSubview:view];
        
        UIImageView *souIV = [[UIImageView alloc] initWithFrame:CGRectMake((WIDTH - 140) / 2, 5, 50, 40)];
        souIV.image = [UIImage imageNamed:@"sous60"];
        souIV.contentMode = UIViewContentModeScaleAspectFit;
        [view addSubview:souIV];
        
        UILabel *souLB = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - 140) / 2 + 40, 5, 102, 40)];
        souLB.font = [UIFont systemFontOfSize:18.0f];
        souLB.textColor = [UIColor whiteColor];
        souLB.text = @"进度查询";
        souLB.textAlignment = NSTextAlignmentLeft;
        [view addSubview:souLB];
        
        UIButton *souBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        souBtn.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
        [souBtn addTarget:self action:@selector(goTurnBack) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:souBtn];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.listArr.count == 0) {
        self.noResultView.hidden = NO;
    }
    return self.listArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listArr[section] count] + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *oneDic = [self.listArr[indexPath.section] firstObject];
    if (indexPath.row == 0)
    {
        PMOrderTimeStatesCell *cell = [tableView dequeueReusableCellWithIdentifier:orderTimeCell];
        [cell createUI];
        NSString *statusString = [oneDic objectForKey:@"tradeStatus"];
        NSString *orderStateString = @"";
        
        if (self.tradeStatus == tradeStatusTurnBack)
        {
            orderStateString = @"";
        }
        else
        {
            if ([statusString isEqualToString:@"待付款"])
            {
                orderStateString = @"等待买家付款";
            }
            else if ([statusString isEqualToString:@"待发货"])
            {
                orderStateString = @"卖家准备发货";
            }
            else if ([statusString isEqualToString:@"待收货"] || [statusString isEqualToString:@"已发货"])
            {
                orderStateString = @"等待买家收货";
            }
            else if ([statusString isEqualToString:@"已收货"])
            {
                orderStateString = @"交易完成";
            }
            else if ([statusString isEqualToString:@"已取消"])
            {
                orderStateString = @"订单已取消";
            }
        }
        
        [cell setCellData:[oneDic objectForKey:@"createDt"] State:orderStateString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == [self.listArr[indexPath.section] count] + 1)
    {
        NSInteger status = 0;
        NSString *statusString = [oneDic objectForKey:@"tradeStatus"];
        PMOrderBtnCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderBtnCell"];
        [cell createUI];
        cell.orderBtnDelegate = self;
        cell.goToPayBtn.tag = indexPath.section * 10000 + 1001;
        cell.cancelPayBtn.tag = indexPath.section * 10000 + 1002;
        cell.waitSendBtn.tag = indexPath.section * 10000 + 1003;
        cell.checkLogBtn.tag = indexPath.section * 10000 + 1004;
        cell.sureGetBtn.tag = indexPath.section * 10000 + 1005;
        cell.discussBtn.tag = indexPath.section * 10000 + 1006;
        cell.hasDiscussBtn.tag = indexPath.section * 10000 + 1007;
        cell.deleteBtn.tag = indexPath.section * 10000 + 1008;
        
        if ([statusString isEqualToString:@"待付款"])
        {
            status = -11;
        }
        else if ([statusString isEqualToString:@"待发货"])
        {
            status = 3;
        }
        else if ([statusString isEqualToString:@"待收货"] || [statusString isEqualToString:@"已发货"])
        {
            status = 0;
        }
        else if ([statusString isEqualToString:@"已收货"] && self.tradeStatus != tradeStatusTurnBack)
        {
            status = 1;
        }
        else if ([statusString isEqualToString:@"已取消"])
        {
            status = 9999;
        }
        else if (self.tradeStatus == tradeStatusTurnBack)
        {
            status = - 9999;
        }
        
        switch (status)
        {
            case 3:
                [cell setCellData:[NSString stringWithFormat:@"￥%.2lf",[[oneDic objectForKey:@"allPrice"] doubleValue]] OrderBtnType:PMOrderBtnTypeWaitSend DiscussStatus:NO];
                break;
            case -11:
                [cell setCellData:[NSString stringWithFormat:@"￥%.2lf",[[oneDic objectForKey:@"allPrice"] doubleValue]] OrderBtnType:PMOrderBtnTypeWaitPay DiscussStatus:NO];
                break;
            case 0:
                [cell setCellData:[NSString stringWithFormat:@"￥%.2lf",[[oneDic objectForKey:@"allPrice"] doubleValue]] OrderBtnType:PMOrderBtnTypeWaitGet DiscussStatus:NO];
                break;
            case 1:
                [cell setCellData:[NSString stringWithFormat:@"￥%.2lf",[[oneDic objectForKey:@"allPrice"] doubleValue]] OrderBtnType:PMOrderBtnTypeWaitDiscuss DiscussStatus:[[oneDic objectForKey:@"discussStatus"] integerValue] == 0 ? NO : YES];
                break;
            case 2:
                [cell setCellData:[NSString stringWithFormat:@"￥%.2lf",[[oneDic objectForKey:@"allPrice"] doubleValue]] OrderBtnType:PMOrderBtnTypeWaitGet DiscussStatus:NO];
                break;
            case 9999:
                [cell setCellData:[NSString stringWithFormat:@"￥%.2lf",[[oneDic objectForKey:@"allPrice"] doubleValue]] OrderBtnType:PMOrderBtnTypeHasCancel DiscussStatus:NO];
                break;
            case -9999:
                [cell setCellData:[NSString stringWithFormat:@"￥%.2lf",[[oneDic objectForKey:@"allPrice"] doubleValue]] OrderBtnType:PMOrderBtnTypeTurnBack DiscussStatus:NO];
                break;
            default:
                break;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        MyTradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tradeCellIdetify];
        [cell setMyTradeOrderDataUI];
        
        NSArray *arr = self.listArr[indexPath.section];
        NSDictionary *dict = arr[indexPath.row - 1];
        
        //给一张默认图片，先使用默认图片，当图片加载完成后再替换
        [cell.iv sd_setImageWithURL:[dict objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
        
        NSString *aSpecVal = [dict objectNullForKey:@"aSpecVal"];
        NSString *bSpecVal = [dict objectNullForKey:@"bSpecVal"];
        NSString *cSpecVal = [dict objectNullForKey:@"cSpecVal"];
        NSString *dSpecVal = [dict objectNullForKey:@"dSpecVal"];
        NSString *eSpecVal = [dict objectNullForKey:@"eSpecVal"];
        
        NSMutableString *specificationStr = [NSMutableString string];
        
        if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", aSpecVal];
        }
        else if (bSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", bSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", cSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", dSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", eSpecVal];
        }
        
        [cell setTitle:[dict objectNullForKey:@"productName"] setSpecification:specificationStr setNumber:[[dict objectNullForKey:@"productNum"] integerValue] setPrice:[[dict objectNullForKey:@"productPrice"] doubleValue]];
        
        if (self.tradeStatus == tradeStatusTurnBack)
        {
            cell.turnBackBtn.tag = indexPath.section * 1000 + indexPath.row;
            [cell.turnBackBtn addTarget:self action:@selector(goRev:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.turnBackBtn.hidden = YES;
        }
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 40;
    }
    else if (indexPath.row != [self.listArr[indexPath.section] count] + 1)
    {
        return (iPhone6_Plus || iPhone6 ? 110 : 90);
    }
    
    return 44;
}

#pragma mark - 获取数据
- (void)getNetWork
{
    NSString *tradeSta = @"";
    if (self.tradeStatus != tradeStatusTurnBack && self.tradeStatus != tradeStatusReceive)
    {
        switch (self.tradeStatus)
        {
            case tradeStatusWaitPay:
                tradeSta = @"-11";
                break;
            case tradeStatusWaitGet:
                tradeSta = @"3,0";
                break;
            default:
                break;
        }
        
        NSMutableDictionary *parameter = [@{PMSID,@"currentPage":@(self.currentPage),@"pageSize":@(10),@"tradeStatus":tradeSta} mutableCopy];
        [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
            if(intSuccess == 1)
            {
                //得到所有的数据
                self.noResultView.hidden = YES;
                self.allArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                
                //总页数
                self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
                
                //遍历所有数据，得到对应交易状态的数据，加入到数组中
                self.listArr = [self sortArr:self.allArray];
                
                if (self.totalPages >= self.currentPage + 1)
                {
                    self.currentPage++;
                    self.isHasMore = YES;
                    if (!self.loadMoreView)
                    {
                        self.loadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                        self.myTradeTableView.tableFooterView = self.loadMoreView;
                    }
                }
                
                [self.myTradeTableView reloadData];
            }
            else
            {
                self.noResultView.hidden = NO;
            }
            
        } showIndicator:YES];
    }
    else if (self.tradeStatus == tradeStatusTurnBack)
    {
        NSMutableDictionary *parameter = [@{PMSID,@"currentPage":@(self.currentPage),@"pageSize":@(10),@"tradeStatus":@"1"} mutableCopy];
        [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
            if(intSuccess == 1)
            {
                //得到所有的数据
                self.noResultView.hidden = YES;
                self.allArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                
                //总页数
                self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
                
                self.listArr = [self sortArr:self.allArray];
                
                if (self.totalPages >= self.currentPage + 1)
                {
                    self.currentPage++;
                    self.isHasMore = YES;
                    if (!self.loadMoreView)
                    {
                        self.loadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                        self.myTradeTableView.tableFooterView = self.loadMoreView;
                    }
                }
                
                [self.myTradeTableView reloadData];
            }
            else
            {
                self.noResultView.hidden = NO;
            }
            [self.myTradeTableView reloadData];
        }showIndicator:YES];
    }
    else
    {
        NSDictionary *parameter = @{PMSID,@"currentPage":@(_currentPage),@"isDiscussed":@"no",@"pageSize":@(20)};
        [[PMNetworking defaultNetworking] request:PMRequestStateShowMyOrderProductDiscussList WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
            
            if(intSuccess ==1)
            {
                self.noResultView.hidden = YES;
                NSArray *beanListArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                self.listArr = [self sortArr:beanListArray];
                [self.myTradeTableView reloadData];
            }
            else
            {
                [self.listArr removeAllObjects];
                self.noResultView.hidden = NO;
            }
            [self.myTradeTableView reloadData];
            
            
        } showIndicator:YES];
    }
    
}

- (void)goTurnBack
{
    YBTurnBackViewController *turnBackView = [[YBTurnBackViewController alloc] init];
    [self.navigationController pushViewController:turnBackView animated:YES];
}

#pragma mark - 得到所有的数据
- (NSMutableArray *)sortArr:(NSArray *)resultArr
{
    NSMutableArray *tempArr = [NSMutableArray array];
    BOOL isSame = NO;
    for (NSDictionary *dic in resultArr)
    {
        isSame = NO;
        for (NSInteger i = 0; i < tempArr.count; i++)
        {
            NSMutableArray *dicArr = tempArr[i];
            NSDictionary *showArrayDic = dicArr[0];
            if ([[showArrayDic objectNullForKey:@"orderCode"] isEqualToString:[dic objectNullForKey:@"orderCode"]])
            {
                isSame = YES;
                [dicArr addObject:dic];
                break;
            }
        }
        //如果没有相同订单号的话，就新创建一个数组
        if(isSame == NO)
        {
            NSMutableArray *newArr = [NSMutableArray array];
            [newArr addObject:dic];
            [tempArr addObject:newArr];
        }
    }
    
    return tempArr;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = RGBA(243, 243, 243, 1);
    return sectionHeaderView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.myTradeTableView)
    {
        CGFloat sectionHeaderHeight = 10;//设置你footer高度
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
        
        if (self.isHasMore && !self.isLoding) {
            CGFloat scrollPosition = scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y;
            if (scrollPosition < [self footerLoadMoreHeight]) {
//                self.isAllOrderShow = NO;
                [self loadMore];
            }
        }
    }
}

- (CGFloat)footerLoadMoreHeight
{
    if (self.loadMoreView)
        return self.loadMoreView.frame.size.height;
    else
        return 10;
}

- (BOOL) loadMore
{
    if (self.isLoding)
        return NO;
    
    [self willBeginLoadingMore];
    self.isLoding = YES;
    return YES;
}

#pragma mark - 开始加载更多
- (void)willBeginLoadingMore
{
    ZSTChatroomLoadMoreView *fv = (ZSTChatroomLoadMoreView *)self.loadMoreView;
    [fv becomLoading];
    [self getMoreByNet];
//    [self getAllOrderByNet];
}

- (void)getMoreByNet
{
    NSString *tradeSta = @"";
    if (self.tradeStatus != tradeStatusTurnBack && self.tradeStatus != tradeStatusReceive)
    {
        switch (self.tradeStatus)
        {
            case tradeStatusWaitPay:
                tradeSta = @"-11";
                break;
            case tradeStatusWaitGet:
                tradeSta = @"3,0";
                break;
            default:
                break;
        }
        
        NSMutableDictionary *parameter = [@{PMSID,@"currentPage":@(self.currentPage),@"pageSize":@(10),@"tradeStatus":tradeSta} mutableCopy];
        [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
            if(intSuccess == 1)
            {
                //得到所有的数据
                self.allArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                
                //总页数
                self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
                
                //遍历所有数据，得到对应交易状态的数据，加入到数组中
                NSArray *moreListArr = [self sortArr:self.allArray];
                [self.listArr addObjectsFromArray:moreListArr];
                [self.myTradeTableView reloadData];
                [self loadMoreCompleted];
                self.currentPage++;
                if (self.currentPage > self.totalPages)
                {
                    self.isHasMore = NO;
                    self.myTradeTableView.tableFooterView = nil;
                }
                else
                {
                    self.isHasMore = YES;
                }
            }
        } showIndicator:NO];
    }
    else if (self.tradeStatus == tradeStatusTurnBack)
    {
        NSMutableDictionary *parameter = [@{PMSID,@"currentPage":@(self.currentPage),@"pageSize":@(10),@"tradeStatus":@"1"} mutableCopy];
        [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
            if(intSuccess == 1)
            {
                //得到所有的数据
                self.allArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                
                //总页数
                self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
                NSArray *moreListArr = [self sortArr:self.allArray];
                [self.listArr addObjectsFromArray:moreListArr];
                [self.myTradeTableView reloadData];
                [self loadMoreCompleted];
                self.currentPage++;
                if (self.currentPage > self.totalPages)
                {
                    self.isHasMore = NO;
                    self.myTradeTableView.tableFooterView = nil;
                }
                else
                {
                    self.isHasMore = YES;
                }
            }
        }showIndicator:NO];
    }
    else
    {
        NSDictionary *parameter = @{PMSID,@"currentPage":@(_currentPage),@"isDiscussed":@"no",@"pageSize":@(20)};
        [[PMNetworking defaultNetworking] request:PMRequestStateShowMyOrderProductDiscussList WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
            
            if(intSuccess ==1)
            {
                NSArray *beanListArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                NSArray *moreListArr = [self sortArr:beanListArray];
                [self.listArr addObjectsFromArray:moreListArr];
                [self.myTradeTableView reloadData];
                [self loadMoreCompleted];
                self.currentPage++;
                if (self.currentPage > self.totalPages)
                {
                    self.isHasMore = NO;
                    self.myTradeTableView.tableFooterView = nil;
                }
                else
                {
                    self.isHasMore = YES;
                }
            }
            
        } showIndicator:NO];
    }
}

- (void)cellBtnClickAction:(UIButton *)sender
{
    NSInteger sectionIndex = sender.tag / 10000;
    NSInteger btnType = sender.tag % 10000;
    NSArray *orderArray = self.listArr[sectionIndex];
    
    switch (btnType)
    {
        case 1001:
        {
            [self getParamsWithOrderArray:orderArray];
        }
            break;
        case 1002:
        {
            [self cancelTrade:orderArray Section:sectionIndex];
        }
            break;
        case 1003:
            
            break;
        case 1004:
        {
            ComplaintsViewController *complaintsVC = [[ComplaintsViewController alloc] init];
            complaintsVC.productsArray = orderArray; // 传当前section展示的数据
            complaintsVC.state = PMOrderStateLogistics;
            [self.navigationController pushViewControllerWithNavigationControllerTransition:complaintsVC];
            self.view.userInteractionEnabled = YES;
        }
            break;
        case 1005:
        {
            ComplaintsViewController *complaintsVC = [[ComplaintsViewController alloc] init];
            complaintsVC.productsArray = orderArray; // 传当前section展示的数据
            complaintsVC.state = PMOrderStateSureOrder;
            complaintsVC.isFromDetail = NO;
            complaintsVC.complainDelegate = self;
            [self.navigationController pushViewController:complaintsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            break;
        case 1006:
        {
            YBComplaintsViewController *commentsVC = [[YBComplaintsViewController alloc] init];
            commentsVC.productArr = orderArray;
            commentsVC.complainDelegate = self;
            commentsVC.isHasDiscuss = NO;
            commentsVC.isFromDetail = NO;
            [self.navigationController pushViewController:commentsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            break;
        case 1007:
        {
            YBComplaintsViewController *commentsVC = [[YBComplaintsViewController alloc] init];
            commentsVC.productArr = orderArray;
            commentsVC.complainDelegate = self;
            commentsVC.isHasDiscuss = YES;
            commentsVC.isFromDetail = NO;
            commentsVC.orderCode = [[orderArray firstObject] objectForKey:@"orderCode"];
            [self.navigationController pushViewController:commentsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            
            break;
            
        default:
            break;
    }
}

#pragma mark - 付款需要使用的参数
- (void)getParamsWithOrderArray:(NSArray *)array
{
    NSMutableString *products = [NSMutableString string];
    for (NSInteger i = 0; i < array.count; i++)
    {
        NSDictionary *productDic = array[i];
        NSString *productId = [productDic objectNullForKey:@"productId"];
        NSString *aSpecValId = [productDic objectNullForKey:@"aSpecValId"];
        NSString *bSpecValId = [productDic objectNullForKey:@"bSpecValId"];
        NSString *cSpecValId = [productDic objectNullForKey:@"cSpecValId"];
        NSString *dSpecValId = [productDic objectNullForKey:@"dSpecValId"];
        NSString *eSpecValId = [productDic objectNullForKey:@"eSpecValId"];
        NSString *buyNum = [productDic objectNullForKey:@"productNum"];
        if (i == 0)
        {
            [products appendString:@"[{"];
        }
        else
        {
            [products appendString:@",{"];
        }
        NSString *productString = [NSString stringWithFormat:@"\"productId\":\"%@\",\"aSpecValId\":\"%@\",\"bSpecValId\":\"%@\",\"cSpecValId\":\"%@\",\"dSpecValId\":\"%@\",\"eSpecValId\":\"%@\",\"buyNum\":\"%@\"",productId,aSpecValId,bSpecValId,cSpecValId,dSpecValId,eSpecValId,buyNum];
        [products appendString:productString];
        
        if (i == array.count - 1)
        {
            [products appendString:@"}]"];
        }
        else
        {
            [products appendString:@"}"];
        }
    }
    NSDictionary *parameter = @{@"products":products};
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckStockTotal WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            if ([[dic objectNullForKey:@"messageCode"] integerValue] == 0)
            {
                NSDictionary *dic = array[0];
                NSString *orderCode = dic[@"orderCode"];
                
                NSDictionary *param = @{@"orderCode":orderCode,PMSID};
                
                [[PMNetworking defaultNetworking] request:PMRequestStateGetOrderByOrderCode WithParameters:param callBackBlock:^(NSDictionary *dic) {
                    
                    NSMutableArray *newProductArray = [NSMutableArray array];
                    
                    YBSureOrderViewController *vc = [[YBSureOrderViewController alloc] init];
                    
                    
                    vc.orderCode = orderCode;
                    NSDictionary *dataDic = [dic objectNullForKey:@"data"];
                    NSString *actuallyPay = [dataDic objectNullForKey:@"actuallyPay"];
                    vc.actuallyPay = actuallyPay;
                    NSArray *oldProductArray = [dataDic objectNullForKey:@"orderProductMap"];
                    for (NSDictionary *oldProductDic in oldProductArray)
                    {
                        NSDictionary *newProductDic = @{@"product":oldProductDic};
                        [newProductArray addObject:newProductDic];
                    }
                    
                    
                    NSDictionary *oldAddressDic = [dataDic objectNullForKey:@"receiveMap"];
                    NSDictionary *newAddressDic = @{@"name":[oldAddressDic objectNullForKey:@"receiveName"],@"mobile":[oldAddressDic objectNullForKey:@"receiveMobile"],@"areas":[oldAddressDic objectNullForKey:@"receiveArea"],@"address":[oldAddressDic objectNullForKey:@"receiveAddressDetail"],@"defaultAddressId":[oldAddressDic objectNullForKey:@"receiveAreaId"]};
                    
                    
                    NSDictionary *billDic = [dataDic objectNullForKey:@"billMap"];
                    [YBBillInfos shareInstance].billNum = @"null";
                    [YBBillInfos shareInstance].billContent = 0;
                    [YBBillInfos shareInstance].billType = 0;
                    
                    if (!isNull([billDic objectNullForKey:@"billTitle"]))
                    {
                        [YBBillInfos shareInstance].billNum = [billDic objectNullForKey:@"billTitle"];
                        [YBBillInfos shareInstance].billType = [[billDic objectNullForKey:@"billType"] integerValue];
                        [YBBillInfos shareInstance].billContent = [[billDic objectNullForKey:@"billContent"] integerValue];
                    }
                    
                    vc.productsArray = newProductArray;
                    vc.addressDic = newAddressDic;
                    vc.actuallyPay = [dataDic objectNullForKey:@"actuallyPay"];
                    vc.totalPrice = [dataDic objectNullForKey:@"orderMoney"];
                    vc.totalNum = [dataDic objectNullForKey:@"totalNum"];
                    vc.carriagePrice = [[dataDic objectNullForKey:@"logisticsPrice"] doubleValue];
                    vc.isTradeBack = YES;
                    
                    
                    [vc.tableView reloadData];
                    
                    [self.navigationController pushViewController:vc animated:YES];
                    
                    self.view.userInteractionEnabled = YES;
                    
                }showIndicator:YES];
            }
            else
            {
                NSMutableString *noEnoughString = [NSMutableString string];
                NSArray *dataArr = [dic objectNullForKey:@"data"];
                for (NSDictionary *dataDic in dataArr)
                {
                    NSString *productName = [dataDic objectNullForKey:@"productName"];
                    [noEnoughString appendString:productName];
                }
                
                NSString *alertMessage = [NSString stringWithFormat:@"%@\n库存不足",noEnoughString];
                self.view.userInteractionEnabled = YES;
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:alertMessage delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else
        {
            self.view.userInteractionEnabled = YES;
            showRequestFailAlertView;
        }
        
        
    } showIndicator:NO];
    
    /**
     参数：
     PM_SID  	  Y	   String		Token
     orderCode	  Y	   String		订单编号（根据上一步提交订单取得）
     totalPrice	  Y	   doubel		总价（根据订单信息获取）
     totalNum	  Y	   int		    商品笔数（根据订单信息获取）
     pro	      Y	   String		商品名称（多个商品用","逗号隔开）
     */
    
}

#pragma mark - 取消订单
- (void)cancelTrade:(NSArray *)orderArr Section:(NSInteger)sectionIndex
{
    NSString *orderCode = [orderArr[0] objectNullForKey:@"orderCode"];
    NSString *orderId = [orderArr[0] objectNullForKey:@"orderId"];
    
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckOrderIsPay WithParameters:@{PMSID,@"orderId":orderId} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            [[PMNetworking defaultNetworking] request:PMRequestStateUpdateOrderStatus WithParameters:@{@"status":@"9999",@"orderCode":orderCode} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view ByToast:@"订单取消成功"];
                    self.view.userInteractionEnabled = YES;
                    if ([self.listArr containsObject:orderArr])
                    {
                        NSInteger deleteIndex = [self.listArr indexOfObject:orderArr];
                        [self.listArr removeObject:orderArr];
                        [self.myTradeTableView deleteSections:[NSIndexSet indexSetWithIndex:deleteIndex] withRowAnimation:UITableViewRowAnimationNone];
                    }
                }
                else
                {
                    self.view.userInteractionEnabled = YES;
                    showRequestFailAlertView;
                }
                
            } showIndicator:YES];
        }
        else
        {
            self.view.userInteractionEnabled = YES;
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该订单已经支付"];
        }
        
    } showIndicator:NO];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 0 && indexPath.row != [self.listArr[indexPath.section] count] + 1 )
    {
        NSArray *arr = self.listArr[indexPath.section];
        NSDictionary *dict = arr[indexPath.row - 1];
        NSString *orderCode = [dict objectNullForKey:@"orderCode"];
        PMOrderDetailViewController *orderDetailVC = [[PMOrderDetailViewController alloc] init];
        orderDetailVC.orderCode = orderCode;
        orderDetailVC.detailViewDelegate = self;
        orderDetailVC.productArr = arr;
        orderDetailVC.orderStatus = [dict objectForKey:@"tradeStatus"];
        orderDetailVC.isHasDiscuss = [[dict objectForKey:@"discussStatus"] integerValue] == 0 ? NO : YES;
        [self.navigationController pushViewController:orderDetailVC animated:YES];
    }
}

- (void)finishComplain:(NSArray *)productArr
{
    if ([self.listArr containsObject:productArr])
    {
        NSInteger deleteIndex = [self.listArr indexOfObject:productArr];
        [self.listArr removeObject:productArr];
        [self.myTradeTableView deleteSections:[NSIndexSet indexSetWithIndex:deleteIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)detailFinishSureOrder:(NSArray *)orderArr
{
    [self finishSureOrder:orderArr];
}

- (void)finishSureOrder:(NSArray *)productArr
{
    if ([self.listArr containsObject:productArr])
    {
        NSInteger deleteIndex = [self.listArr indexOfObject:productArr];
        [self.listArr removeObject:productArr];
        [self.myTradeTableView deleteSections:[NSIndexSet indexSetWithIndex:deleteIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)detailFinishComments:(NSArray *)orderArr
{
    [self finishComplain:orderArr];
}

#pragma mark - 结束加载更多
- (void) loadMoreCompleted
{
    ZSTChatroomLoadMoreView *fv = self.loadMoreView;
    self.isLoding = NO;
    [fv endLoading];
}

- (void)goRev:(UIButton *)sender
{
    NSInteger section = sender.tag / 1000;
    NSInteger row = sender.tag % 1000;
    
    NSString *orderId = [[self.listArr[section] objectAtIndex:(row - 1)] objectNullForKey:@"orderId"];
    
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckReturnBack WithParameters:@{@"orderId":orderId} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            RelevantViewController *relevantVC = [[RelevantViewController alloc] init];
            relevantVC.relevantDict = self.listArr[section][row - 1];
            [self.navigationController pushViewController:relevantVC animated:YES];
        }
        else
        {
            NSString *message = [dic objectNullForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        
    } showIndicator:NO];
    
}

- (void)createNoResult
{
    UIView *noResultView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    noResultView.backgroundColor = [UIColor whiteColor];
    self.noResultView = noResultView;
    
    UIImageView *noResultIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noResultView.frame.size.height)];
    noResultIV.image = [UIImage imageNamed:@"order_null2"];
    noResultIV.contentMode = UIViewContentModeScaleAspectFit;
    [noResultView addSubview:noResultIV];
    
    [self.view insertSubview:noResultView atIndex:100];
    
    noResultView.hidden = YES;
}


@end
