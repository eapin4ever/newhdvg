//
//  RegisterPassViewController.h
//  changeViewController
//
//  Created by pmit on 15/6/8.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterPassViewController : UIViewController

@property (copy,nonatomic) NSString *userName;
@property (copy,nonatomic) NSString *phoneNum;
@property (copy,nonatomic) NSString *verifyCode;

@end
