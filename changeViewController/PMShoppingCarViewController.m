//
//  PMShoppingCarViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/27.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMShoppingCarViewController.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PMNetworking.h"
#import "PMShoppingCarCell.h"
#import "MasterViewController.h"
#import "PMMyGoodsViewController.h"
#import "YBSureOrderViewController.h"
#import "JHRefresh.h"
#import "PMToastHint.h"

@interface PMShoppingCarViewController () <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *shopCarTableView;
@property (strong,nonatomic) NSMutableArray *carProductArr;
@property (strong,nonatomic) NSMutableArray *carProductPriceArr;
@property (strong,nonatomic) NSMutableArray *carProductSelectedArr;
@property (strong,nonatomic) UIButton *buyBtn;
@property (strong,nonatomic) UIButton *seletedAllBtn;
@property (strong,nonatomic) UIView *toolBar;
@property (strong,nonatomic) UIBarButtonItem *deleteBarBtn;
@property (strong,nonatomic) UIBarButtonItem *refreshBarBtn;
@property (strong,nonatomic) UILabel *totalPriceLB;
@property (assign,nonatomic) NSInteger cartNum;
@property (assign,nonatomic) double totalPrice;
@property (strong,nonatomic) UIView *noView;
@property (strong,nonatomic) NSMutableArray *stockIdArr;
@property (strong,nonatomic) NSMutableArray *shopIdArr;
@property (strong,nonatomic) NSMutableArray *maxCountArr;
@property (strong,nonatomic) UIView *noDataView;
@property (strong,nonatomic) UIImageView *noDataIV;
@property (strong,nonatomic) UIButton *buyButton;

@end

@implementation PMShoppingCarViewController

static NSString *const shopCarCell = @"shopCarCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"购物车";
    self.view.backgroundColor = HDVGPageBGGray;
    self.carSeletedArr = [NSMutableArray array];
    self.maxCountArr = [NSMutableArray array];
    [self buildNavigation];
    [self buildTableView];
    [self buildToolBar];
    [self refreshToolBar];
    [self createShopCarNoData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getCarDataByNet];
    [self changeToolBarFrame:self.isFromDetai];
    [self changeTableViewFrame:self.isFromDetai];
//    self.carSeletedArr = [NSMutableArray array];
    if (self.isFromDetai)
    {
        [[MasterViewController defaultMasterVC].tabBar setAlpha:0];
    }
    else
    {
        [[MasterViewController defaultMasterVC].tabBar setAlpha:1];
    }
}

- (void)buildNavigation
{
    UIBarButtonItem *delteBtn = [[UIBarButtonItem alloc] initWithTitle:@"删除" style:UIBarButtonItemStyleDone target:self action:@selector(delCarProduct)];
    self.navigationItem.rightBarButtonItem = delteBtn;
}

- (void)changeNavigationItem:(BOOL)isFromDetail
{
//    UIBarButtonItem *refreshBtn = [[UIBarButtonItem alloc] initWithTitle:@"刷新" style:UIBarButtonItemStyleDone target:self action:@selector(refreshCarProduct)];
//    self.navigationItem.rightBarButtonItem = refreshBtn;
}

- (void)buildTableView
{
    self.shopCarTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50 - 50) style:UITableViewStylePlain];
    self.shopCarTableView.delegate = self;
    self.shopCarTableView.dataSource = self;
    self.shopCarTableView.backgroundColor = HDVGPageBGGray;
    self.shopCarTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.shopCarTableView registerClass:[PMShoppingCarCell class] forCellReuseIdentifier:shopCarCell];
    [self.view addSubview:self.shopCarTableView];
}

- (void)changeTableViewFrame:(BOOL)isFromDetail
{
    if (isFromDetail)
    {
        self.shopCarTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50);
    }
    else
    {
        self.shopCarTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50 - 50);
    }
    
}

- (void)buildToolBar
{
    self.toolBar = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50 - 50, WIDTH, 50)];
    //footerView.backgroundColor = TabBarColor;
    self.toolBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.toolBar];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 50)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.7;
    [self.toolBar addSubview:alphaView];
    
    UIButton *selectAllBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.seletedAllBtn = selectAllBtn;
    selectAllBtn.frame = CGRectMake(-2.5, 12.5, 70, 20);
    [selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
    [selectAllBtn setImage:[UIImage imageNamed:@"selectAll_1"] forState:UIControlStateSelected];
    [selectAllBtn setTitle:@"全选" forState:UIControlStateNormal];
    [selectAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectAllBtn.titleLabel.font = [UIFont boldSystemFontOfSize:10];
    selectAllBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [selectAllBtn addTarget:self action:@selector(clickToSelectAllRows:) forControlEvents:UIControlEventTouchUpInside];
    [alphaView addSubview:selectAllBtn];
    
    //合计  不含运费
    UILabel *total = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(180), 5, WidthRate(80), 20)];
    total.text = @"合 计";
    total.textColor = [UIColor whiteColor];
    total.textAlignment = NSTextAlignmentCenter;
    total.font = [UIFont systemFontOfSize:13];
    [alphaView addSubview:total];
    
    UILabel *hint = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(170), 25, WidthRate(100), 20)];
    hint.text = @"不含运费";
    hint.textAlignment = NSTextAlignmentCenter;
    hint.font = [UIFont systemFontOfSize:9];
    hint.textColor = [UIColor whiteColor];
    [alphaView addSubview:hint];
    
    
    //合计总价
    _totalPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(280), 10, WidthRate(250), 30)];
    _totalPriceLB.textColor = [UIColor redColor];
    _totalPriceLB.font = [UIFont boldSystemFontOfSize:16.0f];
    [alphaView addSubview:_totalPriceLB];
    
    //结算
    UIButton *buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.buyBtn = buyBtn;
    [buyBtn setFrame:CGRectMake(WidthRate(500), 0, WIDTH - WidthRate(500), 50)];
    [buyBtn setBackgroundColor:RGBA(203, 34, 50, 1)];
    [buyBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [buyBtn addTarget:self action:@selector(clickToBuyNow:) forControlEvents:UIControlEventTouchUpInside];
    [self.toolBar addSubview:buyBtn];
}

- (void)changeToolBarFrame:(BOOL)isFromDetail
{
    if (isFromDetail)
    {
        self.toolBar.frame = CGRectMake(0, HEIGHT - 64 - 50, WIDTH, 50);
    }
    else
    {
        self.toolBar.frame = CGRectMake(0, HEIGHT - 64 - 50 - 50, WIDTH, 50);
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.carProductArr.count == 0) {
        self.noDataView.hidden = NO;
    }
    
    return self.carProductArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMShoppingCarCell *cell = [tableView dequeueReusableCellWithIdentifier:shopCarCell];
    [cell createUI];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *carProductDic = self.carProductArr[indexPath.section];
    NSDictionary *productDic = [carProductDic objectNullForKey:@"product"];
    NSString *zkString = [carProductDic objectNullForKey:@"productType"];
    if ([zkString isEqualToString:@"zk"])
    {
        cell.activeIV.hidden = NO;
    }
    else
    {
        cell.activeIV.hidden = YES;
    }
    
    [cell.iv sd_setImageWithURL:[carProductDic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    cell.iv.tag = indexPath.section;
    cell.iv.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProductDetail:)];
    [cell.iv addGestureRecognizer:tap];
    
    cell.titleLB.userInteractionEnabled = YES;
    cell.titleLB.tag = indexPath.section;
    UITapGestureRecognizer *lbTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProductDetail:)];
    [cell.titleLB addGestureRecognizer:lbTap];
    cell.maxCount = [[productDic objectNullForKey:@"total"] integerValue];
    
    
    [cell setContentTitle:[productDic objectNullForKey:@"productName"] classify1:[carProductDic objectNullForKey:@"aSpecVal"] classify2:[carProductDic objectNullForKey:@"bSpecVal"] distanceOrLocation:nil number:[NSString stringWithFormat:@"%@",[carProductDic objectNullForKey:@"productCount"]] price:isNull([carProductDic objectNullForKey:@"productPrice"]) ? 0.00 : [[carProductDic objectNullForKey:@"productPrice"] doubleValue]];
    
    if (self.isFromDetai)
    {
        if (indexPath.section == 0)
        {
            if (![self.carSeletedArr containsObject:carProductDic])
            {
                [self.carSeletedArr addObject:carProductDic];
            }
            
        }
    }
    else
    {
        cell.selectedBtn.selected = NO;
    }
    
    if ([self.carSeletedArr containsObject:carProductDic])
    {
        cell.selectedBtn.selected = YES;
    }
    else
    {
        cell.selectedBtn.selected = NO;
    }
    
    [self refreshToolBar];
    
    cell.minusBtn.tag = indexPath.section;
    cell.plusBtn.tag = indexPath.section;
    
    [cell.minusBtn addTarget:self action:@selector(changeModelMinus:) forControlEvents:UIControlEventTouchUpInside];
    [cell.minusBtn addTarget:cell action:@selector(numberMinus:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.plusBtn addTarget:self action:@selector(changeModelPlus:) forControlEvents:UIControlEventTouchUpInside];
    [cell.plusBtn addTarget:cell action:@selector(numberPlus:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.selectedBtn addTarget:self action:@selector(seletedInCar:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectedBtn.tag = indexPath.section;
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = HDVGPageBGGray;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    else {
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (void)getCarDataByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateSafeShopCartShowBuyCart WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
        
        if (intSuccess == 1)
        {
            self.noDataView.hidden = YES;
            
            self.carProductArr = [NSMutableArray array];
            self.stockIdArr = [NSMutableArray array];
            self.shopIdArr = [NSMutableArray array];
            self.carProductPriceArr = [NSMutableArray array];
            NSArray *carNetArr = [[dic objectNullForKey:@"data"] objectNullForKey:@"pas"];
            if (carNetArr.count == 0)
            {
                
            }
            else
            {
                for (NSDictionary *carNetDic in carNetArr)
                {
                    if ([[[carNetDic objectNullForKey:@"product"] objectNullForKey:@"status"] isEqualToString:@"UP"])
                    {
                        NSMutableDictionary *params = [NSMutableDictionary dictionary];
                        NSString *productId = [carNetDic objectNullForKey:@"productId"];
                        NSString *stockId = [carNetDic objectNullForKey:@"stockId"];
                        NSString *shopId = [carNetDic objectNullForKey:@"shopId"];
                        NSString *aSpecVal = [carNetDic objectNullForKey:@"aSpecValId"];
                        NSString *bSpecVal = [carNetDic objectNullForKey:@"bSpecValId"];
                        NSString *cSpecVal = [carNetDic objectNullForKey:@"cSpecValId"];
                        NSString *dSpecVal = [carNetDic objectNullForKey:@"dSpecValId"];
                        NSString *eSpecVal = [carNetDic objectNullForKey:@"eSpecValId"];
                        NSString *areaId = [carNetDic objectNullForKey:@"areaId"];
                        NSString *areaName = [[carNetDic objectNullForKey:@"product"] objectNullForKey:@"areaName"];
                        NSString *calcFor = @"";
                        if (([productId isKindOfClass:[NSString class]] && productId && ![productId isEqualToString:@""]) && ( [areaId isKindOfClass:[NSString class]] && areaId && ![areaId isEqualToString:@""] && areaName))
                        {
                            params = [@{@"productId":productId,@"areaId":areaId,@"areaName":areaName} mutableCopy];
                        }
                        
                        if (!isNull(aSpecVal) && isNull(bSpecVal))
                        {
                            calcFor = @"A";
                            [params setValue:aSpecVal forKey:@"aid"];
                        }
                        else if (!isNull(bSpecVal) && isNull(cSpecVal))
                        {
                            calcFor = @"AB";
                            [params setValue:aSpecVal forKey:@"aid"];
                            [params setValue:bSpecVal forKey:@"bid"];
                        }
                        else if (!isNull(cSpecVal) && isNull(dSpecVal))
                        {
                            calcFor = @"ABC";
                            [params setValue:aSpecVal forKey:@"aid"];
                            [params setValue:bSpecVal forKey:@"bid"];
                            [params setValue:cSpecVal forKey:@"cid"];
                        }
                        else if (!isNull(dSpecVal) && isNull(eSpecVal))
                        {
                            calcFor = @"ABCD";
                            [params setValue:aSpecVal forKey:@"aid"];
                            [params setValue:bSpecVal forKey:@"bid"];
                            [params setValue:cSpecVal forKey:@"cid"];
                            [params setValue:dSpecVal forKey:@"did"];
                        }
                        else if (!isNull(eSpecVal))
                        {
                            calcFor = @"ABCDE";
                            [params setValue:aSpecVal forKey:@"aid"];
                            [params setValue:bSpecVal forKey:@"bid"];
                            [params setValue:cSpecVal forKey:@"cid"];
                            [params setValue:dSpecVal forKey:@"did"];
                            [params setValue:eSpecVal forKey:@"eid"];
                        }
                        else
                        {
                            calcFor = @"";
                        }
                        [params setValue:calcFor forKey:@"calcFor"];
                        NSString *status = [dic objectNullForKey:@"messageCode"];
                        [self getCalcDataByNet:params AndStatus:status];
                        [self.carProductArr addObject:carNetDic];
                        [self.stockIdArr addObject:stockId];
                        
                        if (![shopId isKindOfClass:[NSString class]]) {
                            shopId = @"";
                        }
                        [self.shopIdArr addObject:shopId];
                        
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            [self.shopCarTableView reloadData];
        }
        else {
            self.noDataView.hidden = NO;// 购物车数据为空时提示
        }
        
    } showIndicator:YES];
}

- (void)getCalcDataByNet:(NSDictionary *)param AndStatus:(NSString *)status
{
    [[PMNetworking defaultNetworking] request:PMRequestStateProductCalcStockProduct WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSDictionary *calcDic = [dic objectNullForKey:@"data"];
            NSString *discountPrice = [calcDic objectNullForKey:@"discountPrice"];
            NSString *productPrice = [calcDic objectNullForKey:@"productPrice"];
            //NSString *stockId = [calcDic objectNullForKey:@"stockId"];
            NSInteger totalNum = [[calcDic objectNullForKey:@"total"] integerValue];
            [self.maxCountArr addObject:@(totalNum)];
            //[self.stockIdArr addObject:stockId];
            if ([status isEqualToString:@"zk"])
            {
                [self.carProductPriceArr addObject:discountPrice];
            }
            else
            {
                [self.carProductPriceArr addObject:productPrice];
            }
        }
    } showIndicator:NO];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.shopCarTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    
}

- (void)showProductDetail:(UITapGestureRecognizer *)tap
{
    NSDictionary *dic = self.carProductArr[tap.view.tag];
    
    NSDictionary *productDic = [dic objectNullForKey:@"product"];
    
    PMMyGoodsViewController *vc = [[PMMyGoodsViewController alloc] init];
    
    if([[dic objectNullForKey:@"shopHome"] isKindOfClass:[NSDictionary class]])
    {
        vc.shopId = [[dic objectNullForKey:@"shopHome"] objectNullForKey:@"id"];
    }
    
    vc.productId = [productDic objectNullForKey:@"id"];
    
    [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];
}

- (void)changeNumByNet:(NSInteger)index AndIsPlus:(BOOL)isPlus
{
    NSDictionary *changeProductDic = [self.carProductArr[index] mutableCopy];
    NSString *productId = [changeProductDic objectNullForKey:@"productId"];
    NSString *areaId = [changeProductDic objectNullForKey:@"areaId"];
    NSString *stockId = self.stockIdArr[index];
    NSString *shopId = self.shopIdArr[index];
    
     __block NSInteger num = [[changeProductDic objectNullForKey:@"productCount"] integerValue];
    BOOL isHas = NO;
    NSInteger seletedIndex = -1;
    if ([self.carSeletedArr containsObject:changeProductDic])
    {
        isHas = YES;
        seletedIndex = [self.carSeletedArr indexOfObject:changeProductDic];
    }
    else
    {
        isHas = NO;
    }
    
    if (isPlus)
    {
        num++;
    }
    else if(num > 1)
    {
        num--;
    }
    else
    {
        return;
    }
    
    
    [[PMNetworking defaultNetworking] request:PMRequestStateSafeShopCartModifyCount WithParameters:@{PMSID,@"count":@(num),@"productId":productId,@"stockId":stockId,@"shopId":shopId,@"areaId":areaId} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            double aPrice = [self.carProductPriceArr[index] doubleValue];
            
            if(isPlus)
            {
                self.cartNum++;
                self.totalPrice += aPrice;
            }
            else
            {
                self.cartNum--;
                self.totalPrice -= aPrice;
            }
            [changeProductDic setValue:@(num) forKey:@"productCount"];
            [self.carProductArr replaceObjectAtIndex:index withObject:changeProductDic];
            
            if (isHas)
            {
                [self.carSeletedArr replaceObjectAtIndex:seletedIndex withObject:changeProductDic];
            }
            [self refreshToolBar];
        }
        else
        {
            NSString *message = [dic objectNullForKey:@"message"];
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:message];
        }
        
    } showIndicator:NO];
}

- (void)changeModelMinus:(UIButton *)sender
{
    [self changeNumByNet:sender.tag AndIsPlus:NO];
}

- (void)changeModelPlus:(UIButton *)sender
{
    [self changeNumByNet:sender.tag AndIsPlus:YES];
}

- (void)refreshToolBar
{
    double price = 0.0;
    
    for (NSDictionary *seletedCarProdcut in self.carSeletedArr)
    {
        price += [[seletedCarProdcut objectNullForKey:@"productPrice"] doubleValue] * [[seletedCarProdcut objectNullForKey:@"productCount"] integerValue];
    }
    
    if (self.carSeletedArr.count == self.carProductArr.count && self.carProductArr.count != 0)
    {
        self.seletedAllBtn.selected = YES;
    }
    else
    {
        self.seletedAllBtn.selected = NO;
    }
    
    _totalPriceLB.text = [NSString stringWithFormat:@"￥%.2lf", price];
    
    [self.buyBtn setTitle:[NSString stringWithFormat:@"立即结算(%@)",@(self.carSeletedArr.count)] forState:UIControlStateNormal];
}

- (void)seletedInCar:(UIButton *)sender
{
    sender.selected = !sender.isSelected;
    NSDictionary *seletedCarProduct = self.carProductArr[sender.tag];
    if (sender.isSelected)
    {
        if (![self.carSeletedArr containsObject:seletedCarProduct])
        {
            [self.carSeletedArr addObject:seletedCarProduct];
        }
    }
    else
    {
        [self.carSeletedArr removeObject:seletedCarProduct];
    }
    
    if (self.carSeletedArr.count == self.carProductArr.count)
    {
        self.seletedAllBtn.selected = YES;
    }
    else
    {
        self.seletedAllBtn.selected = NO;
    }
    
    [self refreshToolBar];
}

- (void)clickToSelectAllRows:(UIButton *)sender
{
    sender.selected = !sender.isSelected;
    if (sender.selected)
    {
        for (NSInteger i = 0; i < self.carProductArr.count; i++)
        {
            NSDictionary *carProductDic = self.carProductArr[i];
            if ([self.carSeletedArr containsObject:carProductDic])
            {
                continue;
            }
            else
            {
                [self.carSeletedArr addObject:carProductDic];
            }
            
            PMShoppingCarCell *cell = (PMShoppingCarCell *)[self.shopCarTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
            cell.selectedBtn.selected = YES;
        }
    }
    else
    {
        self.carSeletedArr = [NSMutableArray array];
        for (NSInteger i = 0; i < self.carProductArr.count; i++)
        {
            PMShoppingCarCell *cell = (PMShoppingCarCell *)[self.shopCarTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
            cell.selectedBtn.selected = NO;
        }
    }
    [self refreshToolBar];
}

#pragma mark 购物车删除商品请求
- (void)delCarProduct
{
    if (self.carProductPriceArr.count == 0) {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"您的购物车还没有商品，请购买"];
        return;
    }
    
    if (self.carSeletedArr.count == 0)
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请选择要删除的商品"];
    }
    else
    {
        NSMutableArray *idsArr = [NSMutableArray array];
        for (NSDictionary *seletedProductDic in self.carSeletedArr)
        {
            NSString *productId = [seletedProductDic objectNullForKey:@"productId"];
            NSString *areaId = [seletedProductDic objectNullForKey:@"areaId"];
            NSString *stockId = [seletedProductDic objectNullForKey:@"stockId"];
            NSString *shopId = ([[seletedProductDic objectNullForKey:@"shopId"] isKindOfClass:[NSString class]] && [seletedProductDic objectNullForKey:@"shopId"] && ![[seletedProductDic objectNullForKey:@"shopId"] isEqualToString:@""]) ? [seletedProductDic objectNullForKey:@"shopId"] : @"null";
            NSString *buyCartId = [seletedProductDic objectNullForKey:@"buyCartId"];
            NSString *oneIdsString = [NSString stringWithFormat:@"%@_%@_%@_%@_%@",productId,areaId,stockId,shopId,buyCartId];
            [idsArr addObject:oneIdsString];
        }
        
        NSString *idsString = [idsArr componentsJoinedByString:@","];
        [[PMNetworking  defaultNetworking] request:PMRequestStateSafeShopCartDelBatchProduct WithParameters:@{PMSID,@"productSpecIds":idsString} callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                for (NSDictionary *seletedProductDic in self.carSeletedArr)
                {
                    [self.carProductArr removeObject:seletedProductDic];
                }
                
                [self.carSeletedArr removeAllObjects];
                [self.shopCarTableView reloadData];
                if (self.carSeletedArr.count == self.carProductArr.count && self.carProductArr.count != 0)
                {
                    self.seletedAllBtn.selected = YES;
                }
                else
                {
                    self.seletedAllBtn.selected = NO;
                }
                
                [self refreshToolBar];
            
            }
        } showIndicator:NO];
    }
}

#pragma mark 立即结算请求 先判断库存
- (void)clickToBuyNow:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    if (self.carSeletedArr.count == 0)
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请选择要结算的商品"];
    }
    else
    {
        NSLog(@"selearr --> %@",self.carSeletedArr);
        NSMutableArray *idsArr = [NSMutableArray array];
        for (NSInteger i = 0; i < self.carSeletedArr.count; i++)
        {
            NSDictionary *seletedProductDic = self.carSeletedArr[i];
            NSString *productId = [seletedProductDic objectNullForKey:@"productId"];
            NSInteger productCount = [[seletedProductDic objectNullForKey:@"productCount"] integerValue];
            NSString *oneString = @"";
            if (i == 0)
            {
                if (self.carSeletedArr.count > 1)
                {
                    oneString = [NSString stringWithFormat:@"[{\"productId\":\"%@\",\"buyNum\":\"%@\"}",productId,@(productCount)];
                }
                else
                {
                    oneString = [NSString stringWithFormat:@"[{\"productId\":\"%@\",\"buyNum\":\"%@\"}]",productId,@(productCount)];
                }
                
            }
            else if (i == self.carSeletedArr.count - 1)
            {
                oneString = [NSString stringWithFormat:@"{\"productId\":\"%@\",\"buyNum\":\"%@\"}]",productId,@(productCount)];
            }
            else
            {
                oneString = [NSString stringWithFormat:@"{\"productId\":\"%@\",\"buyNum\":\"%@\"}",productId,@(productCount)];
            }
            
            [idsArr addObject:oneString];
            
        }
        
        NSString *idsString = [idsArr componentsJoinedByString:@","];
        [[PMNetworking defaultNetworking] request:PMRequestStateCheckLimitBuyNum WithParameters:@{PMSID,@"products":idsString,@"from":@"buycart"} callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                [self showOrderByNet];
            }
            else
            {
                self.view.userInteractionEnabled = YES;
                NSMutableString *noEnoughString = [NSMutableString string];
                NSArray *dataArr = [dic objectNullForKey:@"data"];
                for (NSDictionary *dataDic in dataArr)
                {
                    NSString *productName = [dataDic objectNullForKey:@"productName"];
                    [noEnoughString appendString:productName];
                }
                
                NSString *alertMessage = [NSString stringWithFormat:@"%@\n库存不足",noEnoughString];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:alertMessage delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        } showIndicator:NO];
    }
}

- (void)showOrderByNet
{
    NSMutableArray *idsArr = [NSMutableArray array];
    for (NSDictionary *seletedCarDic in self.carSeletedArr)
    {
        NSString *productId = [seletedCarDic objectNullForKey:@"productId"];
        NSString *areaId = [seletedCarDic objectNullForKey:@"areaId"];
        NSString *stockId = [seletedCarDic objectNullForKey:@"stockId"];
        NSString *shopId = (![[seletedCarDic objectNullForKey:@"shopId"] isKindOfClass:[NSNull class]] && [seletedCarDic objectNullForKey:@"shopId"] && ![[seletedCarDic objectNullForKey:@"shopId"] isKindOfClass:[NSNull class]] && ![[seletedCarDic objectNullForKey:@"shopId"] isEqualToString:@""]) ? [seletedCarDic objectNullForKey:@"shopId"] : @"null";
        NSInteger productCount = [[seletedCarDic objectNullForKey:@"productCount"] integerValue];
        NSString *oneIdsString = [NSString stringWithFormat:@"%@_%@_%@_%@_%@",productId,areaId,stockId,shopId,@(productCount)];
        [idsArr addObject:oneIdsString];
    }
    NSString *idsString = [idsArr componentsJoinedByString:@","];
    [[PMNetworking defaultNetworking] request:PMRequestStateShowOrder WithParameters:@{PMSID,@"ids":idsString} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSDictionary *dataDic = [dic objectNullForKey:@"data"];
            BOOL isHasActive = NO;
            NSString *activeString = [NSString stringWithFormat:@"%@",[dataDic objectNullForKey:@"hasActivityProduct"]];
            NSString *redPackageTotalPrice = [NSString stringWithFormat:@"%@",[dataDic objectForKey:@"redPackageTotalPrice"]];
            if ([activeString isEqualToString:@"0"])
            {
                isHasActive = NO;
            }
            else
            {
                isHasActive = YES;
            }
            
            YBSureOrderViewController *vc = [[YBSureOrderViewController alloc] init];
            vc.addressDic = [dataDic objectNullForKey:@"defaultAddress"];
            vc.isHasActivityProduct = isHasActive;
            vc.carVC = self;
            vc.orderRedLimit = [[dataDic objectNullForKey:@"orderRedPackLimit"] doubleValue];
            vc.redPackageTotalPrice = redPackageTotalPrice;
            if (isNull([dataDic objectNullForKey:@"carriage"]))
            {
                vc.carriagePrice = 0.00;
            }
            else
            {
                vc.carriagePrice = [[dataDic objectNullForKey:@"carriage"] doubleValue];
            }
            
            //商品信息
            vc.productsArray = [dataDic objectNullForKey:@"products"];
            vc.totalPrice = [dataDic objectNullForKey:@"totalPrice"];
            vc.totalProductNum = [[dataDic objectNullForKey:@"totalNum"] integerValue];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    } showIndicator:NO];
}

#pragma mark - 创建购物车没有数据的界面
- (void)createShopCarNoData
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 114)];
    noDataView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *noDataIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noDataView.frame.size.height)];
    self.noDataIV = noDataIV;
    noDataIV.image = [UIImage imageNamed:@"car_null"];
    noDataIV.contentMode = UIViewContentModeScaleAspectFit;
    [noDataView addSubview:noDataIV];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buyButton.frame = CGRectMake(WIDTH - WidthRate(350), noDataView.bounds.size.height / 2 + (iPhone4s ? HeightRate(80) : HeightRate(100)), WidthRate(160), WidthRate(60));
    [buyButton setBackgroundImage:[UIImage imageNamed:@"car_buyButton"] forState:UIControlStateNormal];
    buyButton.contentMode = UIViewContentModeScaleAspectFit;
    self.buyButton = buyButton;
    [buyButton addTarget:self action:@selector(shopCarBuyClick:) forControlEvents:UIControlEventTouchUpInside];
    [noDataView addSubview:buyButton];
    
    self.noDataView = noDataView;
    [self.view insertSubview:noDataView atIndex:1000];
    self.noDataView.hidden = YES;
}

#pragma mark 购物车没有商品时点击买 跳到首页
- (void)shopCarBuyClick:(UIButton *)button
{
    [[MasterViewController defaultMasterVC] transitionToHomePage];
    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.view.userInteractionEnabled = YES;
//    [self.carSeletedArr removeAllObjects];
    [self refreshToolBar];
}

@end
