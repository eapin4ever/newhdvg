//
//  PMThemeDetailCell.h
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMThemeDetailCell : UITableViewCell
@property(nonatomic,strong)UIImageView *iv;
@property(nonatomic,strong)UIButton *timeBtn;

- (void)createUI;

- (void)setTitle:(NSString *)title discount:(NSString *)discount time:(NSString *)time;
@end
