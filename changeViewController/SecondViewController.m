//
//  SecondViewController.m
//  changeViewController
//
//  Created by EapinZhang on 15/4/1.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "SecondViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMUserInfos.h"
#import "MasterViewController.h"
#import "PMUserInfos.h"
#import "YTGifActivityIndicator.h"
#import "PMMyGoodsViewController.h"
#import "PMToastHint.h"

@interface SecondViewController () <UIWebViewDelegate>

@property (strong,nonatomic) UIWebView *webView;
@property (strong,nonatomic) YTGifActivityIndicator *indicator;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     _indicator = [YTGifActivityIndicator defaultIndicator];
    [self createWebUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createWebUI
{
    self.title = self.webTitle;
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    webView.delegate = self;
    self.webView = webView;
    
    if ([self.urlString rangeOfString:@"CashBonus_list"].length > 0)
    {
        self.urlString = [NSString stringWithFormat:@"%@&userId=%@",self.urlString,[PMUserInfos shareUserInfo].userId];
    }
    
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *urlString = [NSString stringWithFormat:@"%@",request.URL];
    if([urlString rangeOfString:@"productId="].length > 0 && [urlString rangeOfString:@"details."].length > 0)
    {
        NSInteger index = [urlString rangeOfString:@"productId"].location + @"productId=".length;
        NSString *productId = [urlString substringFromIndex:index];
        
        PMMyGoodsViewController *vc = [[PMMyGoodsViewController alloc] init];
        if ([self.webType isEqualToString:@"discount"] || [self.webType isEqualToString:@"theme"])
        {
            vc.isDiscount = YES;
            vc.isPrePay = NO;
            vc.productId = productId;
            [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];
        }
        else
        {
            vc.productId = productId;
            [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];
        }
        
        
        
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    [window addSubview:_indicator];
    [_indicator start];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.indicator stop];
}

@end
