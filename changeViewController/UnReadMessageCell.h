//
//  UnReadMessageCell.h
//  changeViewController
//
//  Created by EapinZhang on 15/4/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnReadMessageCell : UITableViewCell

@property (strong,nonatomic) NSDictionary *messageDic;

- (void)createUI;
- (void)getData;

@end
