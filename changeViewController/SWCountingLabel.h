//
//  SWCountingLabel.h
//  changeViewController
//
//  Created by P&M on 15/5/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, UILabelCountingMethod) {
    UILabelCountingMethodEaseInOut,
    UILabelCountingMethodEaseIn,
    UILabelCountingMethodEaseOut,
    UILabelCountingMethodLinear
};

typedef NSString * (^UICountingLabelFormatBlock)(double value);
typedef NSAttributedString * (^UICountingLabelAttributedFormatBlock)(double value);

@interface SWCountingLabel : UILabel

@property (strong, nonatomic) NSString *format;
@property (assign, nonatomic) UILabelCountingMethod method;
@property (assign, nonatomic) NSTimeInterval animationDuration;

@property (copy, nonatomic) UICountingLabelFormatBlock formatBlock;
@property (copy, nonatomic) UICountingLabelAttributedFormatBlock attributedFormatBlock;
@property (copy, nonatomic) void (^completionBlock)();

- (void)countFrom:(double)startValue to:(double)endValue;
- (void)countFrom:(double)startValue to:(double)endValue withDuration:(NSTimeInterval)duration;

- (void)countFromCurrentValueTo:(double)endValue;
- (void)countFromCurrentValueTo:(double)endValue withDuration:(NSTimeInterval)duration;

- (void)countFromZeroTo:(double)endValue;
- (void)countFromZeroTo:(double)endValue withDuration:(NSTimeInterval)duration;

- (double)currentValue;


@end
