//
//  PMShowClass.m
//  changeViewController
//
//  Created by pmit on 15/1/6.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define itemWidth tableView.bounds.size.width/3
#define itemHeight tableView.bounds.size.width/3/2

#import "PMShowClass.h"
#import "GoodsListViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PMShowClass

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger num = self.parentClassArray.count;

//    [self.goodsListVC showNoResultView:num>0?NO:YES];
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = [self.parentClassArray[section] objectNullForKey:@"class"];
    
    //3个1行，如果有余数，则+1
    NSInteger rows = arr.count/3 + (arr.count%3>0 ? 1:0);
    
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 142;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMProductClassTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"productClassCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //根据区获取数据
    NSDictionary *dic = self.parentClassArray[indexPath.section];
    //行
    NSArray *arr = [dic objectNullForKey:@"class"];
    
    //删除没有图片的数据
    
    
    [cell createUI];
    NSLog(@"name -- >%@",[arr[indexPath.row*3] objectNullForKey:@"className"]);
    NSLog(@"name -- >%@",arr.count >indexPath.row*3+1?[arr[indexPath.row*3+1] objectNullForKey:@"className"]:nil);
    NSLog(@"name -- >%@",arr.count >indexPath.row*3+2?[arr[indexPath.row*3+2] objectNullForKey:@"className"]:nil);
    
    [cell setContentLb1Text:[arr[indexPath.row*3] objectNullForKey:@"className"] Lb2Text:arr.count >indexPath.row*3+1?[arr[indexPath.row*3+1] objectNullForKey:@"className"]:nil Lb3Text:arr.count >indexPath.row*3+2?[arr[indexPath.row*3+2] objectNullForKey:@"className"]:nil];
    
    
    [cell.iv1 sd_setImageWithURL:[arr[indexPath.row*3] objectNullForKey:@"classLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    
    [cell.iv2 sd_setImageWithURL:arr.count >indexPath.row*3+1?[arr[indexPath.row*3+1] objectNullForKey:@"classLogo"]:nil placeholderImage:[UIImage imageNamed:@"loading_image"]];

    
    [cell.iv3 sd_setImageWithURL:arr.count >indexPath.row*3+2?[arr[indexPath.row*3+2] objectNullForKey:@"classLogo"]:nil placeholderImage:[UIImage imageNamed:@"loading_image"]];

    
    [cell setBtnTarget:self.goodsListVC selector:@selector(clickBtn:) indexPath:indexPath];
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *dic = self.parentClassArray[section];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, HeightRate(70))];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, headerView.bounds.size.width, headerView.bounds.size.height)];
    lb.font = [UIFont systemFontOfSize:14];
    lb.textColor = [UIColor grayColor];
    lb.text = [dic objectNullForKey:@"parentClassName"];
    
    [headerView addSubview:lb];
    headerView.backgroundColor = [UIColor whiteColor];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(30))];
    
    footerView.backgroundColor = [UIColor clearColor];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(70);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return HeightRate(30);
}




@end
