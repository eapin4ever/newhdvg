//
//  PMStoreScreenDetailCell.m
//  changeViewController
//
//  Created by P&M on 15/7/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMStoreScreenDetailCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMStoreScreenDetailCell

- (void)createScreenDetailUI
{
    if (!self.detailTitleLB)
    {
        self.detailTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(400), 40)];
        self.detailTitleLB.font = [UIFont systemFontOfSize:14.0f];
        self.detailTitleLB.textAlignment = NSTextAlignmentLeft;
        self.detailTitleLB.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:self.detailTitleLB];
        
        self.checkIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
        self.checkIV.image = [UIImage imageNamed:@"item_red_selected"];
        self.checkIV.contentMode = UIViewContentModeScaleAspectFit;
        self.checkIV.hidden = YES;
        self.accessoryView = self.checkIV;
    }
}

- (void)setStoreScreenDetails:(NSString *)title
{
    self.detailTitleLB.text = title;
}

@end
