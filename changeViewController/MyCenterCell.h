//
//  MyCenterCell.h
//  changeViewController
//
//  Created by pmit on 15/7/3.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCenterCell : UITableViewCell

@property (strong,nonatomic) UIImageView *iconIV;
@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UILabel *tipLB;

- (void)createUI;

@end
