//
//  PMMusicTableViewCell.m
//  changeViewController
//
//  Created by P&M on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMMusicTableViewCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMMusicTableViewCell

- (void)createUI
{
    if (!self.selectedBtn)
    {
        self.selectedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.selectedBtn.frame = CGRectMake(WidthRate(36), 5, WIDTH * 0.08 - WidthRate(10), 40);
        self.selectedBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.selectedBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
        [self.selectedBtn setImage:[UIImage imageNamed:@"selectAll_1"] forState:UIControlStateSelected];
        [self.contentView addSubview:self.selectedBtn];
        
        self.musicTitle = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH * 0.1 + WidthRate(46), 5, WIDTH - WIDTH * 0.1 - WidthRate(60) - 50, 40)];
        self.musicTitle.font = [UIFont systemFontOfSize:15.0f];
        self.musicTitle.textAlignment = NSTextAlignmentLeft;
        self.musicTitle.textColor = HDVGFontColor;
        [self.contentView addSubview:self.musicTitle];
        
        self.onAndOffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.onAndOffBtn.frame = CGRectMake(WIDTH - 40, 12.5, 25, 25);
        [self.onAndOffBtn setImage:[UIImage imageNamed:@"music_off"] forState:UIControlStateNormal];
        [self.onAndOffBtn setImage:[UIImage imageNamed:@"music_on"] forState:UIControlStateSelected];
        [self.contentView addSubview:self.onAndOffBtn];
    }
}

- (void)setShopBgMusicTitle:(NSString *)title
{
    self.musicTitle.text = title;
}

@end
