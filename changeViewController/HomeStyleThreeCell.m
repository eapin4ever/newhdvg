//
//  HomeStyleThreeCell.m
//  changeViewController
//
//  Created by pmit on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "HomeStyleThreeCell.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define kCellWidth (WIDTH - 20)

@implementation HomeStyleThreeCell

- (void)createStyleThreeUI
{
    if (!self.upOneIV)
    {
        UIView *upOneView = [self buildStyleOneSecondWithFrame:CGRectMake(5, 5, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"one"];
        [self.contentView addSubview:upOneView];
        self.upOneView = upOneView;
        
        UIView *upTwoView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth / 3 + 10, 5, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"two"];
        [self.contentView addSubview:upTwoView];
        self.upTwoView = upTwoView;
        
        UIView *upThreeView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth / 3 * 2 + 15, 5, kCellWidth / 3, kCellWidth * 0.48) isMiddle:YES AndIsUp:NO AndStation:@"three"];
        [self.contentView addSubview:upThreeView];
        self.upThreeView = upThreeView;
        
        UIView *donwLeftView = [self buildStyleOneThreeWithFrame:CGRectMake(5, kCellWidth * 0.48 + 10, (kCellWidth + 5) * 0.5, kCellWidth * 0.25) AndDirect:NO];
        [self.contentView addSubview:donwLeftView];
        self.downLeftView = donwLeftView;
        
        UIView *downRightView = [self buildStyleOneThreeWithFrame:CGRectMake((kCellWidth + 5) * 0.5 + 10, kCellWidth * 0.48 + 10, (kCellWidth + 5) * 0.5, kCellWidth * 0.25) AndDirect:YES];
        [self.contentView addSubview:downRightView];
        self.downRightView = downRightView;
    }
}

- (UIView *)buildStyleOneSecondWithFrame:(CGRect)frame isMiddle:(BOOL)isMiddle AndIsUp:(BOOL)isUp AndStation:(NSString *)station
{
    UIView *bgWhiteView = [[UIView alloc] initWithFrame:frame];
    bgWhiteView.backgroundColor = [UIColor whiteColor];
    UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, bgWhiteView.bounds.size.width, bgWhiteView.bounds.size.width)];
    [bgWhiteView addSubview:productIV];
    if (isMiddle)
    {
        UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(2, productIV.frame.origin.y + productIV.frame.size.height + (kCellWidth * 0.02), bgWhiteView.bounds.size.width - 4, 20)];
        titleLB.textColor = kTextColor;
        titleLB.textAlignment = NSTextAlignmentRight;
        titleLB.lineBreakMode = NSLineBreakByClipping;
        titleLB.font = [UIFont systemFontOfSize:WidthRate(24)];
        [bgWhiteView addSubview:titleLB];
        UILabel *priceLB = [[UILabel alloc] initWithFrame:CGRectMake(2, titleLB.frame.origin.y + 20, bgWhiteView.bounds.size.width - 4, 20)];
        priceLB.textAlignment = NSTextAlignmentRight;
        priceLB.textColor = kTextColor;
        priceLB.font = [UIFont boldSystemFontOfSize:13.0f];
        [bgWhiteView addSubview:priceLB];
        
        if ([station isEqualToString:@"one"])
        {
            self.upOneTitleLB = titleLB;
            self.upOnePriceLB = priceLB;
            self.upOneIV = productIV;
        }
        else if ([station isEqualToString:@"two"])
        {
            self.upTwoTitleLB = titleLB;
            self.upTwoPriceLB = priceLB;
            self.upTwoIV = productIV;
        }
        else if ([station isEqualToString:@"three"])
        {
            self.upThreeTitleLB = titleLB;
            self.upThreePriceLB = priceLB;
            self.upThreeIV = productIV;
        }
    }
    
    return bgWhiteView;
}

- (UIView *)buildStyleOneThreeWithFrame:(CGRect)frame AndDirect:(BOOL)isRight
{
    UIView *bgWhiteView = [[UIView alloc] initWithFrame:frame];
    bgWhiteView.backgroundColor = [UIColor whiteColor];
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(5, bgWhiteView.bounds.size.height * 0.3 - 3, bgWhiteView.bounds.size.width * 0.5 - 10, 20)];
    titleLB.font = [UIFont systemFontOfSize:WidthRate(24)];
    titleLB.lineBreakMode = NSLineBreakByClipping;
    titleLB.textColor = kTextColor;
    [bgWhiteView addSubview:titleLB];
    UILabel *priceLB = [[UILabel alloc] initWithFrame:CGRectMake(5, titleLB.frame.origin.y + 20, bgWhiteView.bounds.size.width * 0.5 - 10, 20)];
    priceLB.font = [UIFont boldSystemFontOfSize:13.0f];
    priceLB.textColor = kTextColor;
    [bgWhiteView addSubview:priceLB];
    UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(bgWhiteView.bounds.size.width * 0.5 + 5, 0, bgWhiteView.bounds.size.width * 0.5 - 5, bgWhiteView.bounds.size.height)];
    [bgWhiteView addSubview:productIV];
    
    if (isRight)
    {
        self.downRightIV = productIV;
        self.downRightPriceLB = priceLB;
        self.downRightTitleLB = titleLB;
    }
    else
    {
        self.downLeftIV = productIV;
        self.downLeftPriceLB = priceLB;
        self.downLeftTitleLB = titleLB;
    }
    
    return bgWhiteView;
}

- (void)setCellWithUpOneIV:(NSString *)upOneString AndUpTwoIV:(NSString *)upTwoString AndUpThreeIV:(NSString *)upThreeString AndDwonLeftIV:(NSString *)downLeftString AndDwonRightIV:(NSString *)DownRightString AndUpOneTitle:(NSString *)upOneTitle AndUpTwoTitle:(NSString *)upTwoTitle AndUpThreeTitle:(NSString *)upThreeTitle AndDownLeftTitle:(NSString *)downLeftTitle AndDownRightTitle:(NSString *)downRightTitle AndOnePrice:(NSString *)onePrice AndUpTwoPrice:(NSString *)upTwoPrice AndUpThreePrice:(NSString *)upThreePrice AndDwonLeftPrice:(NSString *)downLeftPrice AndDownRightPrice:(NSString *)downRightPrice
{
    [self.upOneIV sd_setImageWithURL:[NSURL URLWithString:upOneString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upTwoIV sd_setImageWithURL:[NSURL URLWithString:upTwoString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upThreeIV sd_setImageWithURL:[NSURL URLWithString:upThreeString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.downLeftIV sd_setImageWithURL:[NSURL URLWithString:downLeftString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.downRightIV sd_setImageWithURL:[NSURL URLWithString:DownRightString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    
    if (upOneTitle.length > 9)
    {
        upOneTitle = [upOneTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (upTwoTitle.length > 9)
    {
        upTwoTitle = [upTwoTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (upThreeTitle.length > 9)
    {
        upThreeTitle = [upThreeTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (downLeftTitle.length > 6)
    {
        downLeftTitle = [downLeftTitle substringWithRange:NSMakeRange(0, 6)];
    }
    
    if (downRightTitle.length > 6)
    {
        downRightTitle = [downRightTitle substringWithRange:NSMakeRange(0, 6)];
    }
    
    self.upOneTitleLB.text = upOneTitle;
    self.upTwoTitleLB.text = upTwoTitle;
    self.upThreeTitleLB.text = upThreeTitle;
    self.downLeftTitleLB.text = downLeftTitle;
    self.downRightTitleLB.text = downRightTitle;
    
    self.upOnePriceLB.text = onePrice;
    self.upTwoPriceLB.text = upTwoPrice;
    self.upThreePriceLB.text = upThreePrice;
    self.downLeftPriceLB.text = downLeftPrice;
    self.downRightPriceLB.text = downRightPrice;
}

@end
