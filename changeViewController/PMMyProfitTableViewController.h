//
//  PMMyProfitTableViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  代言获利详情控制器
 */

#import <UIKit/UIKit.h>
#import "PMNetworking.h"

@interface PMMyProfitTableViewController : UITableViewController

@property (strong, nonatomic) UIButton *timeAndClientBtn;
@property (strong, nonatomic) NSMutableArray *buttonArray;
@property (strong, nonatomic) NSMutableArray *showArray;
@property (strong, nonatomic) NSArray *profitStateArray;
@property (strong, nonatomic) NSMutableArray *timeArray;
@property (strong, nonatomic) NSMutableArray *profitDetailArray;
@property (strong, nonatomic) NSMutableArray *myClientsArray;

@property (strong, nonatomic) UISegmentedControl *segment;
@property (strong, nonatomic) NSArray *titleArray;
@property (strong, nonatomic) UILabel *tipLabel;
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *orderLabel;
@property (strong, nonatomic) UILabel *brokerageLabel;
@property (strong, nonatomic) NSMutableDictionary *dictionary;
@property (strong, nonatomic) NSArray *currentArray;
@property (strong, nonatomic) NSArray *keysArray;

@property (assign, nonatomic) NSInteger sysYear;
@property (assign, nonatomic) NSInteger sysMonth;
@property (assign, nonatomic) NSInteger sysDay;
@property (assign, nonatomic) NSInteger iMonth;
@property (assign, nonatomic) NSInteger iDay;

@property (strong, nonatomic) UIView *noView;

@property (strong, nonatomic) PMNetworking *networking;

@end
