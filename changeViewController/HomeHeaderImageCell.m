//
//  HomeHeaderImageCell.m
//  changeViewController
//
//  Created by pmit on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "HomeHeaderImageCell.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation HomeHeaderImageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.headerImageView)
    {
        self.headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, WIDTH - 10, (WIDTH - 10) / 3)];
        self.headerImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.headerImageView];
    }
}

- (void)setCellHeaderImageView:(NSString *)urlString
{
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:urlString]];
}

@end
