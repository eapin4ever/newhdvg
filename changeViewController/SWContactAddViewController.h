//
//  SWContactAddViewController.h
//  changeViewController
//
//  Created by P&M on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

// 验证手机号码控制器

#import <UIKit/UIKit.h>
#import "PMNetworking.h"

@interface SWContactAddViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) UIView *mobileView;
@property (strong, nonatomic) UITextField *mobileTF;
@property (strong, nonatomic) CALayer *layer2;

@property (strong, nonatomic) UIView *authcodeView;
@property (strong, nonatomic) UITextField *authcodeTF;
@property (strong, nonatomic) UIButton *authcodeBtn;
@property (strong, nonatomic) UILabel *retryLab;

@property (strong, nonatomic) NSMutableDictionary *reSellerDataDict;

@property (strong, nonatomic) NSString *applyStatus;

@property (strong, nonatomic) NSTimer *timer;

@property (strong, nonatomic) PMNetworking *networking;
@property (copy,nonatomic) NSString *cityName;
@property (copy,nonatomic) NSString *cityId;

@end
