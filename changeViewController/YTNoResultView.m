//
//  YTNoResultView.m
//  changeViewController
//
//  Created by pmit on 15/4/18.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YTNoResultView.h"
#import "PMMyPhoneInfo.h"

@implementation YTNoResultView
{
    UILabel *_noLB;
    UIView *_toView;
}
- (instancetype)initWithFrame:(CGRect)frame toView:(UIView *)toView
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.frame = frame;
        self.backgroundColor = [UIColor whiteColor];
        [self createNoDataView];
        _toView = toView;
        [toView addSubview:self];
    }
    return self;
}


#pragma mark - 没有分类视图
- (void)createNoDataView
{
    //关闭交互，防止在最前面时挡住后面的用户交互
    self.userInteractionEnabled = NO;
    UIImageView *noIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height / 2 - HeightRate(300), CGRectGetWidth(self.bounds), HeightRate(400))];
    noIV.image = [UIImage imageNamed:@"noData"];
    noIV.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:noIV];
    
    _noLB = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height / 2 + HeightRate(100) , CGRectGetWidth(self.bounds), HeightRate(100))];
    _noLB.textColor = HDVGFontColor;
    _noLB.font = [UIFont systemFontOfSize:17.0f];
    _noLB.numberOfLines = 0;
    _noLB.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_noLB];
    self.hidden = YES;
}

- (void)showNoDataViewWithTitle:(NSString *)title
{
    _noLB.text = title;
    [_toView bringSubviewToFront:self];
    self.hidden = NO;
}

@end
