//
//  PMThemeViewController.m
//  changeViewController
//
//  Created by pmit on 15/2/5.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define originY WidthRate(250)

#import "PMThemeViewController.h"
#import "PMGoodsReusableView.h"

@interface PMThemeViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)UICollectionView *resultCollectionView;
@property(nonatomic,strong)NSArray *dicArray;
@end

@implementation PMThemeViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initFakeModels];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"热销年货";
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(300))];
    UIImageView *iv = [[UIImageView alloc] initWithFrame:view.frame];
    iv.image = [UIImage imageNamed:@"banner_theme"];
    [view addSubview:iv];
    [self.view addSubview:view];
    
    [self searchResultUI];
}



#pragma mark - 搜索结果展示视图
- (void)searchResultUI
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    [flowLayout setMinimumInteritemSpacing:5];
    [flowLayout setMinimumLineSpacing:HeightRate(40)];
    [flowLayout setItemSize:CGSizeMake(WidthRate(300), WidthRate(400))];
    [flowLayout setSectionInset:UIEdgeInsetsMake(20, WidthRate(50), 0, WidthRate(50))];
    
    self.resultCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, originY , WIDTH, HEIGHT - originY - 64) collectionViewLayout:flowLayout];
    [self.resultCollectionView setBackgroundColor:[UIColor whiteColor]];
    [self.resultCollectionView setDataSource:self];
    [self.resultCollectionView setDelegate:self];
    [self.resultCollectionView registerClass:[PMGoodsReusableView class] forCellWithReuseIdentifier:@"reuseItem"];
    
//    [self.resultCollectionView setContentInset:UIEdgeInsetsMake(0, 0, 49, 0)];
    
    [self.view addSubview:self.resultCollectionView];
    
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dicArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMGoodsReusableView *item = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseItem" forIndexPath:indexPath];
    
    [item createUI];
    
    NSDictionary *dic = self.dicArray[indexPath.row];
    
    item.showDiscount = NO;
    
    item.imageView.image = [dic objectNullForKey:@"logo"];
    
    [item setContentTitle:[dic objectNullForKey:@"title"] price:[[dic objectNullForKey:@"price"] doubleValue] discount:@""];
    
    
    return item;
}

- (void)initFakeModels
{
    
    self.dicArray = @[@{@"title":@"联想乐蒙K3",@"price":@"699",@"logo":[UIImage imageNamed:@"theme1"]},@{@"title":@"美的 真功夫柴火饭电饭煲",@"price":@"359",@"logo":[UIImage imageNamed:@"theme2"]},@{@"title":@"东芝高清LED液晶电视",@"price":@"1999",@"logo":[UIImage imageNamed:@"theme3"]},@{@"title":@"荣耀畅玩4X",@"price":@"799",@"logo":[UIImage imageNamed:@"theme4"]}];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
