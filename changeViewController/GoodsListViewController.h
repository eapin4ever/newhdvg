//
//  GoodsListViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/7.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"

#import "PMProductClassTVCell.h"


@interface GoodsListViewController : UIViewController

@property (copy,nonatomic) NSString *classTitle;
@property (assign,nonatomic) BOOL isFromMore;

- (void)clickBtn:(UIButton *)sender;

- (void)showNoResultView:(BOOL)show;
@end
