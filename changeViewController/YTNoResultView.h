//
//  YTNoResultView.h
//  changeViewController
//
//  Created by pmit on 15/4/18.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YTNoResultView : UIView
- (instancetype)initWithFrame:(CGRect)frame toView:(UIView *)toView;

- (void)showNoDataViewWithTitle:(NSString *)title;

@end
