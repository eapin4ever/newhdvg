//
//  PMSearchViewCell.m
//  changeViewController
//
//  Created by pmit on 14/12/9.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMSearchViewCell.h"

@implementation PMSearchViewCell

- (void)setText:(NSString *)text
{
    if(!self.lb)
    {
        self.lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.height)];
        self.lb.textAlignment = NSTextAlignmentCenter;
        self.lb.font = [UIFont systemFontOfSize:12];
        self.lb.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:self.lb];
        
        
    }
    
    self.lb.text = text;
    
}

- (instancetype)initWithLabelText:(NSString *)text
{
    self = [super init];
    if(self)
    {
        self.lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.height)];
        self.lb.textAlignment = NSTextAlignmentCenter;
        self.lb.font = [UIFont systemFontOfSize:12];
        self.lb.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:self.lb];
        self.lb.text = text;
    }
    return self;
}

@end
