//
//  QRCodeScannerViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/7.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMBrowser.h"
#import "PMMyPhoneInfo.h"

@interface QRCodeScannerViewController : UIViewController

+ (QRCodeScannerViewController *)QRCodeScanner;
@end
