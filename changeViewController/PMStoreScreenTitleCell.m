//
//  PMStoreScreenTitleCell.m
//  changeViewController
//
//  Created by P&M on 15/7/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMStoreScreenTitleCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMStoreScreenTitleCell

- (void)createScreenTitleUI
{
    if (!self.titleLabel)
    {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WIDTH - WIDTH * 0.1 - WidthRate(60) - 50, 40)];
        self.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = HDVGFontColor;
        [self.contentView addSubview:self.titleLabel];
        self.titleLabel.highlightedTextColor = [UIColor lightGrayColor];
        
        UIView *selectedBackGroundView = [[UIView alloc] initWithFrame:self.frame];
        selectedBackGroundView.backgroundColor = [UIColor whiteColor];
        self.selectedBackgroundView = selectedBackGroundView;
    }
}

- (void)setStoreScreenTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

@end
