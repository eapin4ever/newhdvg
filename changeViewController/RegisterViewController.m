//
//  RegisterViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "RegisterPassViewController.h"
#import "PMToastHint.h"
#import "PMPublicClass.h"

@interface RegisterViewController ()
@property(nonatomic,strong)NSTimer *timer;
@property (strong,nonatomic) UILabel *resendCodeLabel;
@end

@implementation RegisterViewController
{
    NSInteger _timerCount;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"注册账户";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self createNewUserAndPassUI];
    
    [self createButtonControllerUI];
}

// 创建新用户昵称和密码
- (void)createNewUserAndPassUI
{
    // 创建注册验证码 view
    self.registerView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 132)];
    self.registerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.registerView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.registerView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 43, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.registerView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(WidthRate(26), 88, WIDTH, HeightRate(1));
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.registerView.layer addSublayer:layer3];
    
    CALayer *layer4 = [[CALayer alloc] init];
    layer4.frame = CGRectMake(0, 132 - 0.5, WIDTH, HeightRate(1));
    layer4.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.registerView.layer addSublayer:layer4];
    
//    CALayer *layer5 = [[CALayer alloc] init];
//    layer5.frame = CGRectMake(0, 176 - 0.5, WIDTH, 0.5);
//    layer5.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
//    [self.registerView.layer addSublayer:layer5];
    
    
    // 注册新账户昵称
    UILabel *newNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(220), 30)];
    newNameLabel.backgroundColor = [UIColor clearColor];
    newNameLabel.text = @"用   户   名：";
    newNameLabel.textColor = HDVGFontColor;
    newNameLabel.textAlignment = NSTextAlignmentLeft;
    newNameLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.registerView addSubview:newNameLabel];
    
    self.aNewNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(260), 7, WIDTH - WidthRate(270) - WidthRate(40), 30)];
    self.aNewNameTextField.borderStyle = UITextBorderStyleNone;
    self.aNewNameTextField.delegate = self;
    self.aNewNameTextField.tag = 0;
    self.aNewNameTextField.textAlignment = NSTextAlignmentLeft;
    self.aNewNameTextField.font = [UIFont systemFontOfSize:14.0f];
    self.aNewNameTextField.placeholder = @"请输入新用户名";
    self.aNewNameTextField.returnKeyType = UIReturnKeyDone;
    self.aNewNameTextField.keyboardType = UIReturnKeyDefault;
    [self.registerView addSubview:self.aNewNameTextField];
    
    // 手机号码
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 51, WidthRate(220), 30)];
    mobileLabel.backgroundColor = [UIColor clearColor];
    mobileLabel.text = @"手 机 号 码：";
    mobileLabel.textColor = HDVGFontColor;
    mobileLabel.textAlignment = NSTextAlignmentLeft;
    mobileLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.registerView addSubview:mobileLabel];
    
    self.mobileTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(260), 51, WIDTH - WidthRate(270) - WidthRate(40), 30)];
    self.mobileTextField.borderStyle = UITextBorderStyleNone;
    self.mobileTextField.delegate = self;
    self.mobileTextField.tag = 1;
    self.mobileTextField.textAlignment = NSTextAlignmentLeft;
    self.mobileTextField.font = [UIFont systemFontOfSize:14.0f];
    self.mobileTextField.placeholder = @"请输入手机号码";
    self.mobileTextField.returnKeyType = UIReturnKeyDone;
    self.mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self.registerView addSubview:self.mobileTextField];
    
    // 短信验证码
    UILabel *authcodeLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 95, WIDTH / 3 , 30)];
    authcodeLab.backgroundColor = [UIColor clearColor];
    authcodeLab.text = @"短信验证码：";
    authcodeLab.textColor = HDVGFontColor;
    authcodeLab.textAlignment = NSTextAlignmentLeft;
    authcodeLab.font = [UIFont systemFontOfSize:15.0f];
    [self.registerView addSubview:authcodeLab];
    
    self.registerAuthcodeTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(260), 95, WIDTH - WidthRate(270) - HeightRate(80), 30)];
    self.registerAuthcodeTF.borderStyle = UITextBorderStyleNone;
    self.registerAuthcodeTF.delegate = self;
    self.registerAuthcodeTF.tag = 2;
    self.registerAuthcodeTF.textAlignment = NSTextAlignmentLeft;
    self.registerAuthcodeTF.font = [UIFont systemFontOfSize:14.0f];
    self.registerAuthcodeTF.placeholder = @"请输入验证码";
    self.registerAuthcodeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.registerAuthcodeTF.returnKeyType = UIReturnKeyDone;
    self.registerAuthcodeTF.keyboardType = UIKeyboardTypeNumberPad;
    [self.registerView addSubview:self.registerAuthcodeTF];
    
    // 获取验证码按钮
    CGFloat X = WIDTH - (WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30));
    CGFloat widtd = WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30) - WidthRate(46);
    self.authcodeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.authcodeButton.frame = CGRectMake(X, 95, widtd, 30);
    [self.authcodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.authcodeButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.authcodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.authcodeButton addTarget:self action:@selector(authcodeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.authcodeButton setBackgroundColor:ButtonBgColor];
    [self.authcodeButton.layer setCornerRadius:4.0];
    [self.registerView addSubview:self.authcodeButton];
    
    self.resendCodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(X, 95, widtd, 30)];
    self.resendCodeLabel.textAlignment = NSTextAlignmentCenter;
    self.resendCodeLabel.hidden = YES;
    self.resendCodeLabel.text = @"重发(60)";
    self.resendCodeLabel.textColor = [UIColor darkGrayColor];
    self.resendCodeLabel.font = [UIFont systemFontOfSize:12.0f];
    self.resendCodeLabel.textAlignment = NSTextAlignmentCenter;
    [self.registerView addSubview:self.resendCodeLabel];
    
    // 注册新账户密码
//    UILabel *newPassLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 137, WidthRate(220), 30)];
//    newPassLabel.backgroundColor = [UIColor clearColor];
//    newPassLabel.text = @"密          码：";
//    newPassLabel.textColor = HDVGFontColor;
//    newPassLabel.textAlignment = NSTextAlignmentLeft;
//    newPassLabel.font = [UIFont systemFontOfSize:15.0f];
//    [self.registerView addSubview:newPassLabel];
//    
//    self.aNewAccountPassTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(260), 137, WIDTH - WidthRate(270) - WidthRate(20), 30)];
//    self.aNewAccountPassTF.borderStyle = UITextBorderStyleNone;
//    self.aNewAccountPassTF.delegate = self;
//    self.aNewAccountPassTF.tag = 3;
//    self.aNewAccountPassTF.secureTextEntry = YES;
//    self.aNewAccountPassTF.textAlignment = NSTextAlignmentLeft;
//    self.aNewAccountPassTF.font = [UIFont systemFontOfSize:14.0f];
//    self.aNewAccountPassTF.placeholder = @"长度6～16位数字、字母、符号";
//    self.aNewAccountPassTF.clearButtonMode = UITextFieldViewModeWhileEditing;
//    self.aNewAccountPassTF.returnKeyType = UIReturnKeyDone;
//    self.aNewAccountPassTF.keyboardType = UIReturnKeyDefault;
//    [self.registerView addSubview:self.aNewAccountPassTF];
}

// 创建按钮控件 UI
- (void)createButtonControllerUI
{
//     初始化显示密码按钮
//    UIButton *showButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    showButton.frame = CGRectMake(WIDTH - WidthRate(230), self.registerView.frame.origin.y + 176 + HeightRate(10), WidthRate(190), 18);
//    [showButton setImage:[UIImage imageNamed:@"showpass_no.png"] forState:UIControlStateNormal];
//    [showButton setImage:[UIImage imageNamed:@"showpass_yes.png"] forState:UIControlStateSelected];
//    [showButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
//    [showButton.titleLabel setFont:[UIFont systemFontOfSize:HeightRate(30)]];
//    [showButton setTitle:@"显示密码" forState:UIControlStateNormal];
//    [showButton setTitleColor:HDVGFontColor forState:UIControlStateNormal];
//    [showButton addTarget:self action:@selector(showPassButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:showButton];
    
    // 初始化完成设置按钮
//    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.registerButton.frame = CGRectMake(WidthRate(46), self.registerView.frame.origin.y + 176 + HeightRate(120), WIDTH - WidthRate(46) * 2, 44);
//    [self.registerButton setTitle:@"下一步" forState:UIControlStateNormal];
//    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self.registerButton addTarget:self action:@selector(registerButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.registerButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
//    [self.registerButton setBackgroundColor:ButtonBgColor];
//    [self.registerButton.layer setCornerRadius:6.0f];
//    [self.view addSubview:self.registerButton];
    self.nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextBtn.frame = CGRectMake(WidthRate(46), self.registerView.frame.origin.y + 132 + HeightRate(120), WIDTH - WidthRate(46) * 2, 44);
    [self.nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
    [self.nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.nextBtn addTarget:self action:@selector(enterPassSet:) forControlEvents:UIControlEventTouchUpInside];
    [self.nextBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [self.nextBtn setBackgroundColor:ButtonBgColor];
    [self.nextBtn.layer setCornerRadius:6.0f];
    [self.view addSubview:self.nextBtn];
    
}

- (void)showPassButtonClick:(UIButton *)button
{
    // 改变按钮的选中状态
    button.selected = !button.selected;
    if (button.selected)
    {
        self.aNewAccountPassTF.secureTextEntry = NO;
    }
    else
    {
        self.aNewAccountPassTF.secureTextEntry = YES;
    }
}

// 隐藏软键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.aNewNameTextField resignFirstResponder];
    [self.aNewAccountPassTF resignFirstResponder];
    [self.mobileTextField resignFirstResponder];
    [self.registerAuthcodeTF resignFirstResponder];
}

#pragma mark - textField delegate
#pragma mark 输入框被键盘遮挡解决方法
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // 如果详细地址输入框被键盘遮挡了，就向上移动50个像素
    if (self.aNewAccountPassTF.tag == 3 && textField == self.aNewAccountPassTF) {
        // 输入框监听事件
        [textField addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventEditingDidBegin];
        [textField addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return YES;
}

// 开始编辑时，整体上移
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.aNewAccountPassTF.tag == 3 && textField == self.aNewAccountPassTF) {
        [self moveView:(iPhone4s ? -50 : 0)];
    }
}


- (void)moveView:(CGFloat)move
{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y += move;//view的y轴上移
    self.view.frame = frame;
    [UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}


#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.aNewNameTextField.tag == 0 && textField == self.aNewNameTextField && textField.text.length - range.length + string.length > 12) {
        return NO;
    }
    
    if (self.mobileTextField.tag == 1 && textField == self.mobileTextField && textField.text.length - range.length + string.length > 11) {
        return NO;
    }
    
    if (self.registerAuthcodeTF.tag == 2 && textField == self.registerAuthcodeTF && textField.text.length - range.length + string.length > 6) {
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // 结束编辑时，整体下移
    if (self.aNewAccountPassTF.tag == 3 && textField == self.aNewAccountPassTF) {
        [self moveView:(iPhone4s ? 50 : 0)];
    }
    
    if (self.aNewNameTextField.tag == 0 && textField == self.aNewNameTextField && textField.text.length != 0)
    {
        if(self.aNewNameTextField.tag == 0 && textField == self.aNewNameTextField)
        {
            if (self.aNewNameTextField.text.length >= 20) {
                
                self.aNewNameTextField.enabled = NO;
            }
            /**
             * 检查用户名是否存在
             * 参数: userName:(string)用户名
             */
            NSDictionary *userNameString = @{@"userName":self.aNewNameTextField.text};
            self.networking = [PMNetworking defaultNetworking];
            
            __block NSDictionary *callBackDict;
            
            [self.networking request:PMRequestStateCheckClientUserName WithParameters:userNameString callBackBlock:^(NSDictionary *dict) {
                callBackDict = dict;
                
                // 如果返回的信息中，success的值是true则用户已存在
                if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1)
                {
                    if(!self.nicknameLabel)
                    {
                        self.nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.aNewNameTextField.frame.size.width -  WidthRate(200), 0, WidthRate(200), 30)];
                        self.nicknameLabel.backgroundColor = [UIColor clearColor];
                        self.nicknameLabel.text = @"* 用户名已存在";
                        self.nicknameLabel.font = [UIFont systemFontOfSize:12.0f];
                        self.nicknameLabel.textAlignment = NSTextAlignmentRight;
                        self.nicknameLabel.textColor = [UIColor redColor];
                        [self.aNewNameTextField addSubview:self.nicknameLabel];
                    }
                    self.nicknameLabel.hidden = NO;
//                    [self.registerButton setUserInteractionEnabled:NO];
                }
                else if ([[callBackDict objectNullForKey:@"success"] boolValue] != 1 && textField == self.aNewNameTextField) {
                    self.nicknameLabel.hidden = YES;
                }
                if ([[callBackDict objectNullForKey:@"success"] boolValue] == 0) {
//                    [self.registerButton setUserInteractionEnabled:YES];
                }
            }showIndicator:YES];
        }
    }
    // 手机号
    if (self.mobileTextField.tag == 1 && textField == self.mobileTextField && textField.text.length == 11)
    {
        if(self.mobileTextField.tag == 1 && textField == self.mobileTextField)
        {
            /**
             * 检查手机号是否已注册
             * 参数: userName:(string)手机号
             */
            NSDictionary *userNameString = @{@"userName":self.mobileTextField.text};
            self.networking = [PMNetworking defaultNetworking];
            
            [self.networking request:PMRequestStateCheckClientUserName WithParameters:userNameString callBackBlock:^(NSDictionary *dic) {
                
                // 如果返回的信息中，success的值是true则手机号已存在
                if ([[dic objectNullForKey:@"success"] boolValue] == 1)
                {
                    if(!self.mobileLabel)
                    {
                        self.mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.mobileTextField.frame.size.width -  WidthRate(210), 0, WidthRate(210), 30)];
                        self.mobileLabel.backgroundColor = [UIColor clearColor];
                        self.mobileLabel.text = @"* 手机号已注册";
                        self.mobileLabel.font = [UIFont systemFontOfSize:12.0f];
                        self.mobileLabel.textAlignment = NSTextAlignmentRight;
                        self.mobileLabel.textColor = [UIColor redColor];
                        [self.mobileTextField addSubview:self.mobileLabel];
                    }
                    self.mobileLabel.hidden = NO;
                    [self.authcodeButton setUserInteractionEnabled:NO];
//                    [self.registerButton setUserInteractionEnabled:NO];
                }
                else if ([[dic objectNullForKey:@"success"] boolValue] != 1 && textField == self.mobileTextField) {
                    self.mobileLabel.hidden = YES;
                }
                if ([[dic objectNullForKey:@"success"] boolValue] == 0) {
                    [self.authcodeButton setUserInteractionEnabled:YES];
//                    [self.registerButton setUserInteractionEnabled:YES];
                }
            }showIndicator:YES];
        }
    }
}


- (void)authcodeButtonClick:(UIButton *)sender
{
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"] || ![PMPublicClass checkMobile:self.mobileTextField.text])
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的手机号码"];
    }
    else {
        /**
         * 用户输入手机号码获取短信验证码
         * 参数: sendSMS:(string)验证码
         */
        NSDictionary *phoneSendSMS = @{@"mobile":self.mobileTextField.text};
        self.networking = [PMNetworking defaultNetworking];
        
        
        [self.networking request:PMRequestStateSendSMS WithParameters:phoneSendSMS callBackBlock:^(NSDictionary *dic) {
            
            if (intSuccess == 1)
            {
                NSInteger messageCode = [[dic objectNullForKey:@"messageCode"] integerValue];
                if (messageCode != 1)
                {
                    [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该号超出每天获取验证码条数"];
                }
                else
                {
                    // 按钮变灰，倒数，“重发”
                    if(!self.timer.isValid)//正在倒数
                    {
                        sender.backgroundColor = [UIColor lightGrayColor];
                        sender.userInteractionEnabled = NO;
                        _timerCount = 60;
                        [sender setTitle:@"重发(60)" forState:UIControlStateNormal];
                        [sender setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCountDown:) userInfo:sender repeats:YES];
                    }
                }
            }
            
        }showIndicator:NO];
    }
}

#pragma mark  获取验证码按钮倒数
- (void)timerCountDown:(NSTimer *)timer
{
    UIButton *btn = timer.userInfo;

    if(_timerCount == 0)
    {
        //停止运行timer
        [timer invalidate];
        //按钮恢复成红色
        
        [btn setBackgroundColor:RGBA(218, 67, 80, 1)];
        self.resendCodeLabel.hidden = YES;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:@"重发" forState:UIControlStateNormal];
        btn.userInteractionEnabled = YES;
    }
    else
    {
        _timerCount --;
        self.resendCodeLabel.hidden = NO;
        self.resendCodeLabel.text = [NSString stringWithFormat:@"重发(%ld)",(long)_timerCount];
        [btn setTitle:@"" forState:UIControlStateNormal];
//        [btn setTitle:[NSString stringWithFormat:@"重 发(%ld)",(long)_timerCount] forState:UIControlStateNormal];
    }
    
}

#pragma mark - 注册按钮响应事件
- (void)registerButtonClick:(id)sender
{
    // 姓名、密码不能为空
    if (self.aNewNameTextField.text.length == 0 || self.aNewAccountPassTF.text.length < 6 || self.aNewAccountPassTF.text.length > 16 || self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"] || self.registerAuthcodeTF.text.length == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请按要求填写完整注册信息！" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
    }
    if (![self.aNewNameTextField.text isEqual:@""] && (self.aNewAccountPassTF.text.length > 5) && (self.aNewAccountPassTF.text.length < 17) && self.mobileTextField.text.length == 11 && [self.mobileTextField.text hasPrefix:@"1"] && ![self.registerAuthcodeTF.text isEqual:@""]) {
        
        // 完成注册
        NSDictionary *nameAndPassString = @{@"name":self.aNewNameTextField.text,@"mobile":self.mobileTextField.text,@"password":[MD5Util md5:self.aNewAccountPassTF.text],@"verificationCode":self.registerAuthcodeTF.text};
        
        __block NSDictionary *callBackDict;
        
        [self.networking request:PMRequestStateRegister WithParameters:nameAndPassString callBackBlock:^(NSDictionary *dict) {
            
            callBackDict = dict;
            
            // 如果返回的信息中，success的值是true则注册成功
            if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"注册成功，请登录" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 1;
                [alertView show];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
        }showIndicator:NO];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)enterPassSet:(UIButton *)sender
{
    // 判断是否为空
    if (self.aNewNameTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入用户名"];
        return;
    }
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"]) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的手机号码"];
        return;
    }
    if (self.registerAuthcodeTF.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入获取到的手机验证码"];
        return;
    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckValidCode WithParameters:@{@"validateCode":self.registerAuthcodeTF.text} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            RegisterPassViewController *passVC = [[RegisterPassViewController alloc] init];
            passVC.userName = self.aNewNameTextField.text;
            passVC.phoneNum = self.mobileTextField.text;
            passVC.verifyCode = self.registerAuthcodeTF.text;
            [self.navigationController pushViewController:passVC animated:YES];
        }
        else
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"验证码不正确，请重新输入"];
        }
        
    } showIndicator:YES];
}

@end
