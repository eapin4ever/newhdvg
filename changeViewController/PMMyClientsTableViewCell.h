//
//  PMMyClientsTableViewCell.h
//  changeViewController
//
//  Created by P&M on 15/1/21.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMMyClientsTableViewCell : UITableViewCell

- (void)setMyClientsDataUI;

// 设置我的客户昵称、付款笔数、交易额数据
- (void)setNickname:(NSString *)nickname setTradeNum:(NSInteger)tradeNum setBrokerage:(double)brokerage;

@end
