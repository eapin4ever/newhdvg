//
//  PMBillInfoCell.h
//  changeViewController
//
//  Created by pmit on 15/11/4.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMBillInfoCell : UITableViewCell

@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UILabel *contentLB;

- (void)createUI;
- (void)setCellData:(NSString *)billTitleString Content:(NSString *)billContentString;

@end
