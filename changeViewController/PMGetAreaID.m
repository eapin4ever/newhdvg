//
//  PMGetAreaID.m
//  changeViewController
//
//  Created by pmit on 14/12/19.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMGetAreaID.h"


@implementation PMGetAreaID
+ (NSString *)getAreaIDByArea:(NSString *)area
{
    NSArray *arr = [area componentsSeparatedByString:@" "];
    
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"city.plist" ofType:nil]];
    
    NSString *areaID;
    for (int i = (int)arr.count-1; i>=0; i--)
    {
        NSString *string = arr[i];
        if(string.length <= 0)
            continue;
        areaID = [dic objectForKey:string];
        
        NSString *str0;
        if(areaID == nil)
        {
            str0 = [string stringByAppendingString:@"市"];
            areaID = [dic objectForKey:str0];
        }
        
        if(areaID == nil)
        {
            if ([[arr firstObject] containsString:@"广东"])
            {
                if ([[arr lastObject] containsString:@"白云"])
                {
                    areaID = @"440111";
                }
                else
                {
                    str0 = [string stringByAppendingString:@"区"];
                    areaID = [dic objectForKey:str0];
                }
            }
            else
            {
                str0 = [string stringByAppendingString:@"区"];
                areaID = [dic objectForKey:str0];
            }
        }
        
        if(areaID == nil)
        {
            str0 = [string stringByAppendingString:@"县"];
            areaID = [dic objectForKey:str0];
        }
        
        if(areaID == nil)
        {
            for (NSString *key in dic)
            {
                if([key rangeOfString:string].length>0)
                {
                    areaID = [dic objectForKey:key];
                    break;
                }
            }
        }
        
        break;
    }
    
    
    return areaID;
}
@end
