//
//  PMRepairGetAreaID.h
//  changeViewController
//
//  Created by pmit on 15/7/21.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMRepairGetAreaID : NSObject

+ (NSString *)getAreaIDByArea:(NSString *)area;

@end
