//
//  PMSearchViewCell.h
//  changeViewController
//
//  Created by pmit on 14/12/9.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMSearchViewCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *lb;
@property(nonatomic,copy)NSString *text;

- (instancetype)initWithLabelText:(NSString *)text;
@end
