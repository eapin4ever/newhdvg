//
//  PMOrderAddressCell.h
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMOrderAddressCell : UITableViewCell

@property (strong,nonatomic) UIView *myContentView;
@property (strong,nonatomic) UILabel *nameLB;
@property (strong,nonatomic) UILabel *phoneLB;
@property (strong,nonatomic) UILabel *detailAddressLB;
@property (strong,nonatomic) UIImageView *addressLogo;

- (void)createUI;
- (void)setCellData:(NSDictionary *)addressDic;

@end
