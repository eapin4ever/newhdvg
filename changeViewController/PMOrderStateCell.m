//
//  PMOrderStateCell.m
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMOrderStateCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMOrderStateCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.stateLogoIV)
    {
        self.stateLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 20, 20)];
        self.stateLogoIV.image = [UIImage imageNamed:@"orderState"];
        [self.contentView addSubview:self.stateLogoIV];
        
        self.stateLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.stateLogoIV.frame) + 15, 15, WIDTH - CGRectGetMaxX(self.stateLogoIV.frame) - 30, 20)];
        self.stateLB.font = [UIFont systemFontOfSize:16.0f];
        self.stateLB.textColor = RGBA(59, 59, 59, 1);
        self.stateLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.stateLB];
    }
}

- (void)setCellData:(NSString *)stateString
{
    self.stateLB.text = stateString;
}

@end
