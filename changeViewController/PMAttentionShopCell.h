//
//  PMAttentionShopCell.h
//  changeViewController
//
//  Created by pmit on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMAttentionShopCell : UITableViewCell
@property(nonatomic,strong)UIImageView *iv;
- (void)createUI;

- (void)setContentShopName:(NSString *)shopName accessoryTitle:(NSString *)title;
@end
