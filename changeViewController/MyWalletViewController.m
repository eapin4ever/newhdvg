//
//  MyWalletViewController.m
//  changeViewController
//
//  Created by EapinZhang on 15/4/2.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "MyWalletViewController.h"
#import "PMMyPhoneInfo.h"
#import "BankcardViewController.h"
#import "MyWalletDetailViewController.h"
#import "YBMyRedViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


#define itemX headView.bounds.size.width
#define itemY headView.bounds.size.height

@interface MyWalletViewController () <UITableViewDelegate,UITableViewDataSource>

@property (strong,nonatomic) UITableView *myWalletTableView;
@property (strong,nonatomic) UILabel *bankLB; //进入时查询是否绑定银行卡
@property (strong,nonatomic) UILabel *redMoneyLB;

@end

@implementation MyWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的钱包";
    
    [self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self checkIsBankCard];

    [self checkRedMoney];
}

- (void)createUI
{
    
    UITableView *myWalletTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    myWalletTableView.delegate = self;
    myWalletTableView.dataSource = self;
    self.myWalletTableView = myWalletTableView;
    myWalletTableView.scrollEnabled = NO;
    [self.view addSubview:myWalletTableView];
    
    UIView *footView = [[UIView alloc] init];
    self.myWalletTableView.tableFooterView = footView;
    self.myWalletTableView.backgroundColor = RGBA(236, 236, 236, 1);
    
//    //暂定，表格头部显示钱包和红包余额View
//    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
//    headView.backgroundColor = [UIColor whiteColor];
//    myWalletTableView.tableHeaderView = headView;
//    
//    
//    CALayer *lineLayer = [CALayer layer];
//    lineLayer.frame = CGRectMake(0, 59, WIDTH, 1);
//    lineLayer.backgroundColor = RGBA(236, 236, 236, 1).CGColor;
//    [headView.layer addSublayer:lineLayer];
//    
//    CALayer *lineLayer2 = [CALayer layer];
//    lineLayer2.frame = CGRectMake(WIDTH / 2, HeightRate(10), WidthRate(1), headView.frame.size.height - HeightRate(20));
//    lineLayer2.backgroundColor = RGBA(236, 236, 236, 1).CGColor;
//    [headView.layer addSublayer:lineLayer2];
//    
//    UILabel *moneyTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(10, HeightRate(5), WIDTH / 2 - 10, HeightRate(30))];
//    moneyTitleLB.text = @"钱包余额:";
//    moneyTitleLB.font = [UIFont systemFontOfSize:15.0f];
//    moneyTitleLB.textColor = HDVGFontColor;
//    [headView addSubview:moneyTitleLB];
//    
//    UILabel *moneyLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 4 - WidthRate(100), itemY / 2, WIDTH / 3 , itemY / 2 - HeightRate(50))];
//    moneyLB.text = @"￥1100.00";
//    moneyLB.textColor = HDVGRed;
//    moneyLB.font = [UIFont systemFontOfSize:18.0f];
//    [headView addSubview:moneyLB];
//    
//    UILabel *bankTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX / 2 + 10, HeightRate(5), WIDTH / 2 - 10, HeightRate(30))];
//    bankTitleLB.text = @"银行卡:";
//    bankTitleLB.font = [UIFont systemFontOfSize:15.0f];
//    bankTitleLB.textColor = HDVGFontColor;
//    [headView addSubview:bankTitleLB];
//
//    UILabel *bankLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 2 + WidthRate(100), itemY / 2, WIDTH / 4 - WidthRate(100) , itemY / 4)];
//    bankLB.text = @"1";
//    bankLB.textColor = HDVGRed;
//    self.bankLB = bankLB;
//    bankLB.font = [UIFont systemFontOfSize:18.0f];
//    [headView addSubview:bankLB];
//    
//    UILabel *specLB = [[UILabel alloc] initWithFrame:CGRectMake(bankLB.frame.origin.x + bankLB.frame.size.width, itemY / 2, WidthRate(100), itemY / 4)];
//    specLB.text = @"张";
//    specLB.textColor = HDVGFontColor;
//    specLB.font = [UIFont systemFontOfSize:15.0f];
//    [headView addSubview:specLB];
//    
//    UIButton *moneyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    moneyBtn.frame = CGRectMake(0, itemY / 2, itemX / 2, itemY / 2);
//    moneyBtn.alpha = 1;
//    moneyBtn.tag = 1;
//    [moneyBtn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
//    [headView addSubview:moneyBtn];
//    
//    UIButton *bankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    bankBtn.frame = CGRectMake(itemX / 2, itemY / 2, itemX / 2, itemY / 2);
//    bankBtn.alpha = 1;
//    bankBtn.tag = 2;
//    [bankBtn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
//    [headView addSubview:bankBtn];
//    
//    CALayer *lineLayer3 = [CALayer layer];
//    lineLayer3.frame = CGRectMake(0, itemY - 1, itemX, 1);
//    lineLayer3.backgroundColor = RGBA(236, 236, 236, 1).CGColor;
//    [headView.layer addSublayer:lineLayer3];
//    
//    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 60 - 87)];
//    footView.backgroundColor = [UIColor whiteColor];
//    myWalletTableView.tableFooterView = footView;
//    
//    CALayer *lineLayer4 = [CALayer layer];
//    lineLayer4.frame = CGRectMake(WidthRate(100), 60 + 98, WIDTH, 1);
//    lineLayer4.backgroundColor = RGBA(236, 236, 236, 1).CGColor;
//    [myWalletTableView.layer addSublayer:lineLayer4];
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myWalletCell"];
    NSString *imageName = @"";
    if (indexPath.row == 0)
    {
        cell.textLabel.text = @"银 行 卡";
        imageName = @"icon-qianbao";
    }
    else
    {
        cell.textLabel.text = @"红包余额";
        imageName = @"icon-hongbao";
    }
    
    UIView *accessoriesView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WidthRate(300), cell.frame.size.height)];
    UIImageView *imageIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(300 - 10), 15, 15, 15)];
    imageIV.image = [UIImage imageNamed:@"arrowRight"];
    imageIV.contentMode = UIViewContentModeScaleAspectFit;
    [accessoriesView addSubview:imageIV];
    if (indexPath.row == 0)
    {
        self.bankLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, WidthRate(250), cell.frame.size.height)];
        self.bankLB.textAlignment = NSTextAlignmentRight;
        self.bankLB.text = [NSString stringWithFormat:@"%@张",@"1"];
        self.bankLB.font = [UIFont systemFontOfSize:13.0f];
        self.bankLB.textColor = HDVGRed;
        [accessoriesView addSubview:self.bankLB];
    }
    else
    {
        self.redMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, WidthRate(250), cell.frame.size.height)];
        self.redMoneyLB.textAlignment = NSTextAlignmentRight;
        self.redMoneyLB.text = @"0.00元";
        self.redMoneyLB.font = [UIFont systemFontOfSize:13.0f];
        self.redMoneyLB.textColor = HDVGRed;
        [accessoriesView addSubview:self.redMoneyLB];
    }

    cell.backgroundColor = [UIColor whiteColor];
    cell.accessoryView = accessoriesView;
    cell.imageView.image = [UIImage imageNamed:imageName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 10;
//}

- (void)clickBtn:(UIButton *)button
{
    if (button.tag == 1)
    {
        MyWalletDetailViewController *detailVC = [[MyWalletDetailViewController alloc] init];
        [self.navigationController pushViewController:detailVC animated:YES];
    }
    else
    {
        BankcardViewController *bankcardVC = [[BankcardViewController alloc] init];
        [self.navigationController pushViewController:bankcardVC animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
//        MyWalletDetailViewController *detailVC = [[MyWalletDetailViewController alloc] init];
//        [self.navigationController pushViewController:detailVC animated:YES];
        BankcardViewController *bankcardVC = [[BankcardViewController alloc] init];
        [self.navigationController pushViewController:bankcardVC animated:YES];
    }
    else
    {
        YBMyRedViewController *detailVC = [[YBMyRedViewController alloc] init];
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

#pragma mark - 检测是否有绑定银行卡
- (void)checkIsBankCard
{
    PMNetworking *netWorking = [PMNetworking defaultNetworking];
    [netWorking request:PMRequestStateShareCheckBank WithParameters:@{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID} callBackBlock:^(NSDictionary *dic) {
        
        if ([[dic objectNullForKey:@"success"] integerValue] == 1)
        {
            self.bankLB.text = @"1张";
        }
        else
        {
            self.bankLB.text = @"0张";
        }
        
    } showIndicator:NO];
}

#pragma mark - 查询余额
- (void)checkRedMoney
{
    [[PMNetworking defaultNetworking] request:PMRequestStateGetOptTradeDetail WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
        
        id dataArr = [dic objectNullForKey:@"data"];
        if ([dataArr isKindOfClass:[NSString class]])
        {
            self.redMoneyLB.text = @"0.00元";
        }
        else if ([dataArr isKindOfClass:[NSArray class]])
        {
            NSDictionary *redDic = dataArr[0];
            self.redMoneyLB.text = [NSString stringWithFormat:@"%@元",[redDic objectNullForKey:@"myHdMoney"]];
        }
        
    } showIndicator:NO];
}

@end
