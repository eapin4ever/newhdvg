//
//  ProductScrollCell.m
//  changeViewController
//
//  Created by pmit on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "ProductScrollCell.h"
#import "PMMyPhoneInfo.h"
#import "HomeData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PMMyGoodsViewController.h"

@implementation ProductScrollCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    self.backgroundColor = HDVGPageBGGray;
    if (!self.productScroll)
    {
        self.productScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(5, 0, WIDTH - 10, 100)];
        self.productScroll.showsHorizontalScrollIndicator = NO;
        self.productScroll.showsVerticalScrollIndicator = NO;
        [self.contentView addSubview:self.productScroll];
    }
}

- (void)setScrollViewContent:(NSArray *)contentArr
{
    for (NSInteger i = 0; i < contentArr.count; i++)
    {
        HomeData *homeData = contentArr[i];
        double nowsPrice = [homeData.productPrice doubleValue];
        double rawPrice = [homeData.rawPrice doubleValue];
        double discount = nowsPrice / rawPrice * 10;
        if ([homeData.isDiscount boolValue])
        {
            discount = [homeData.discount doubleValue] * 10;
        }

        NSString *urlString = homeData.homeImage;
        UIView *sView = [[UIView alloc] initWithFrame:CGRectMake((WIDTH - 10) / 4 * i, 0, (WIDTH - 10 - 15) / 4, 100)];
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, sView.bounds.size.width, sView.bounds.size.height * 0.8)];
        
        iv.image = [UIImage imageNamed:@"loading_image"];
        [iv sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageRetryFailed | SDWebImageLowPriority];
        
        iv.contentMode = UIViewContentModeScaleAspectFill;
        UIView *priceView = [[UIView alloc] initWithFrame:CGRectMake(0, sView.bounds.size.height * 0.8 , sView.bounds.size.width, sView.bounds.size.height * 0.2)];
        priceView.backgroundColor = RGBA(51, 51, 51, 1);
        
        UILabel *priceLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, priceView.bounds.size.width, priceView.bounds.size.height)];
        priceLB.font = [UIFont systemFontOfSize:13.0f];
        NSString *priceContent = [NSString stringWithFormat:@"%.1lf折",discount];
        NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:priceContent];
        NSRange litteRange = NSMakeRange(0, 1);
        [noteStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0f] range:litteRange];
        [priceLB setAttributedText:noteStr];
        priceLB.textColor = [UIColor whiteColor];
        priceLB.textAlignment = NSTextAlignmentCenter;
        [priceView addSubview:priceLB];
        
        UIButton *goBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        goBtn.tag = i;
        goBtn.frame = CGRectMake(0, 0, sView.frame.size.width, sView.frame.size.height);
        [goBtn addTarget:self action:@selector(goToDetail:) forControlEvents:UIControlEventTouchUpInside];
        [sView addSubview:iv];
        [sView addSubview:priceView];
        [sView addSubview:goBtn];
        [self.productScroll addSubview:sView];
    }
    [self.productScroll setContentSize:CGSizeMake(self.contentArr.count * WIDTH / 4, 0)];
}

- (void)goToDetail:(UIButton *)sender
{
    HomeData *homeData = self.contentArr[sender.tag];
    NSString *productId = homeData.productId;
    
    if ([_cellDelegate respondsToSelector:@selector(goToHomeDetail:)])
    {
        [_cellDelegate goToHomeDetail:productId];
    }
}

@end
