//
//  PMHongBaoTableViewController.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMHongBaoTableViewController.h"
#import "PMHongBaoCell.h"

@interface PMHongBaoTableViewController ()
@property(nonatomic,strong)NSArray *dicArray;
@end

@implementation PMHongBaoTableViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self initFakeModels];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"抢红包";
    
    [self.tableView registerClass:[PMHongBaoCell class] forCellReuseIdentifier:@"Cell"];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = HDVGPageBGGray;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self createHeaderView];
}

- (void)createHeaderView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(300))];
    UIImageView *iv = [[UIImageView alloc] initWithFrame:view.frame];
    iv.image = [UIImage imageNamed:@"hongbao_banner"];
    [view addSubview:iv];
    
    self.tableView.tableHeaderView = view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PMHongBaoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDictionary *dic = self.dicArray[indexPath.section];
    
    [cell createUI];
    
    cell.iv.image = [dic objectNullForKey:@"logo"];
    
    [cell setTitle:[dic objectNullForKey:@"title"] people:[dic objectNullForKey:@"people"]];
    
    if(indexPath.section == 0)
    {
        cell.timeBtn.backgroundColor = RGBA(229, 144, 85, 1);
        [cell.timeBtn setTitle:@"抢红包" forState:UIControlStateNormal];
    }
    else
    {
        cell.timeBtn.backgroundColor = RGBA(160, 160, 160, 1);
        [cell.timeBtn setTitle:@"来晚了" forState:UIControlStateNormal];
    }
    
    
    return cell;
}

- (void)initFakeModels
{
    self.dicArray = @[@{@"logo":[UIImage imageNamed:@"hongbao01"],@"title":@"欧莱雅 新年红包",@"people":@"5538"},@{@"logo":[UIImage imageNamed:@"hongbao02"],@"title":@"李宁 双11备战红包开抢啦",@"people":@"10864"}];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return WidthRate(320);
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
