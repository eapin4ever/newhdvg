//
//  TranSelectViewController.m
//  changeViewController
//
//  Created by ZhangEapin on 15/5/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "TranSelectViewController.h"
#import "TurnBackSendViewController.h"
#import "PMNetworking.h"
#import "PMMyPhoneInfo.h"

@interface TranSelectViewController ()

@property (strong,nonatomic) NSMutableArray *tranArr;
@property (strong,nonatomic) NSMutableArray *tranValueArr;
@property (strong,nonatomic) NSDictionary *tranDic;

@end

@implementation TranSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"快递选择";
    self.tranArr = [NSMutableArray array];
    self.tranValueArr = [NSMutableArray array];
    [self getModels];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.tranDic allKeys] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tranCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tranCell"];
    }
    

    NSString *thisKey = [[self.tranDic allKeys] objectAtIndex:indexPath.row];
    NSString *thisValue = [self.tranDic objectForKey:thisKey];
    cell.textLabel.text = thisValue;
    cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
    cell.textLabel.textColor = [UIColor blackColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    self.myDelegate.tranName = self.tranArr[indexPath.row];
//    self.myDelegate.companyText.text = self.tranArr[indexPath.row];
//    self.myDelegate.tranValue = self.tranValueArr[indexPath.row];
    self.myDelegate.tranName = [[self.tranDic allValues] objectAtIndex:indexPath.row];
    self.myDelegate.companyText.text = [[self.tranDic allValues] objectAtIndex:indexPath.row];
    self.myDelegate.tranValue = [[self.tranDic allKeys] objectAtIndex:indexPath.row];
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)getModels
{
    [[PMNetworking defaultNetworking] request:PMRequestStateLogCompany WithParameters:nil callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.tranDic = [dic objectNullForKey:@"data"];
            [self.tableView reloadData];
        }
    } showIndicator:YES];
}


@end
