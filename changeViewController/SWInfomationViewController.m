//
//  SWInfomationViewController.m
//  changeViewController
//
//  Created by P&M on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "SWInfomationViewController.h"
#import "PMMyPhoneInfo.h"
#import "SWPayCostViewController.h"
#import "PMToastHint.h"
#import "SWContactAddViewController.h"
#import "AddressPickerViewController.h"

@interface SWInfomationViewController () <AddressPickerViewControllerDelegate>

@end

@implementation SWInfomationViewController
@synthesize areaValue = _areaValue;
@synthesize locatePicker = _locatePicker;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"个人信息";
        
        self.view.backgroundColor = HDVGPageBGGray; // 设置背景色
    }
    return self;
}

- (void)setAreaValue:(NSString *)areaValue
{
    if (![_areaValue isEqualToString:areaValue]) {
        
        _areaValue = areaValue;
        self.areaTF.text = areaValue;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.certifyType = @"1"; // 默认为1，资质为社会人士
    
    
    [self createInfomationUI];
    [self createButtonControllerUI];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.applyStatus = [self.reSellerDataDict objectNullForKey:@"applyStatus"];
    
    // 申请代言用户还没支付加盟费
    if (![self.applyStatus isEqualToString:@""] && [self.applyStatus integerValue] == 0) {
        
        self.shopCertifyId = [self.reSellerDataDict objectNullForKey:@"shopCertifyId"];
        self.nameTF.text = [NSString stringWithFormat:@"%@", [self.reSellerDataDict objectNullForKey:@"name"]];
        if (self.cityString && [self.cityString isKindOfClass:[NSString class]] && ![self.cityString isEqualToString:@""])
        {
            self.areaTF.text = self.cityString;
        }
        else
        {
            self.areaTF.text = [NSString stringWithFormat:@"%@", [self.reSellerDataDict objectNullForKey:@"areaName"]];
        }
        
        if (self.cityId && [self.cityId isKindOfClass:[NSString class]] && ![self.cityId isEqualToString:@""])
        {
            self.areaID = self.cityId;
        }
        else
        {
            self.areaID = [NSString stringWithFormat:@"%@", [self.reSellerDataDict objectNullForKey:@"areaId"]];
        }
        
        NSString *certifyType = [self.reSellerDataDict objectNullForKey:@"certifyType"];
        if ([certifyType isEqualToString:@"0"]) {
            self.studenView.hidden = NO;
            self.layer4.frame = CGRectMake(WidthRate(26), 132 - 0.5, WIDTH, 0.5);
            self.statusLab.text = @"在校学生";
            self.studenTF.text = [NSString stringWithFormat:@"%@", [self.reSellerDataDict objectNullForKey:@"certifyNum"]];
        }
    }
}

// 创建修改绑定手机号 view
- (void)createInfomationUI
{
    // 提示用户输入用户手机号码
    self.infomationView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 132)];
    self.infomationView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.infomationView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.infomationView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 44, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.infomationView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(WidthRate(26), 88, WIDTH, HeightRate(1));
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.infomationView.layer addSublayer:layer3];
    
    CALayer *layer4 = [[CALayer alloc] init];
    layer4.frame = CGRectMake(0, 132 - 0.5, WIDTH, 0.5);
    layer4.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    self.layer4 = layer4;
    [self.infomationView.layer addSublayer:layer4];

    
    // 真实姓名
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(200), 30)];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = @"真实姓名";
    nameLabel.textColor = HDVGFontColor;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.infomationView addSubview:nameLabel];
    
    // 真实姓名输入框
    self.nameTF = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(496), 7, WidthRate(450), 30)];
    self.nameTF.borderStyle = UITextBorderStyleNone;
    self.nameTF.delegate = self;
    self.nameTF.tag = 1;
    self.nameTF.placeholder = @"请输入真实姓名";
    self.nameTF.textColor = RGBA(100, 100, 100, 1);
    self.nameTF.textAlignment = NSTextAlignmentRight;
    self.nameTF.font = [UIFont systemFontOfSize:15.0f];
    self.nameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.nameTF.returnKeyType = UIReturnKeyDone;
    self.nameTF.keyboardType = UIReturnKeyDefault;
    [self.infomationView addSubview:self.nameTF];
    
    // 所在地区
    UILabel *areaLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 51, WidthRate(200), 30)];
    areaLabel.backgroundColor = [UIColor clearColor];
    areaLabel.text = @"所在地区";
    areaLabel.textColor = HDVGFontColor;
    areaLabel.textAlignment = NSTextAlignmentLeft;
    areaLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.infomationView addSubview:areaLabel];
    
    // 所在地区输入框
    UITextField *areaTextField = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(496), 51, WidthRate(450), 30)];
    areaTextField.borderStyle = UITextBorderStyleNone;
    areaTextField.delegate = self;
    areaTextField.tag = 2;
    areaTextField.placeholder = @"选择您所在地区";
    areaTextField.textColor = RGBA(100, 100, 100, 1);
    areaTextField.textAlignment = NSTextAlignmentRight;
    areaTextField.font = [UIFont systemFontOfSize:15.0f];
    areaTextField.returnKeyType = UIReturnKeyDone;
    areaTextField.keyboardType = UIReturnKeyDefault;
    self.areaTF = areaTextField;
    [self.infomationView addSubview:areaTextField];
    
    // 资质类型
    UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 95, WidthRate(200), 30)];
    statusLabel.backgroundColor = [UIColor clearColor];
    statusLabel.text = @"申请资质";
    statusLabel.textColor = HDVGFontColor;
    statusLabel.textAlignment = NSTextAlignmentLeft;
    statusLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.infomationView addSubview:statusLabel];
    
    // 显示资质人类型
    UILabel *statusLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(496), 90, WidthRate(430), 40)];
    statusLab.backgroundColor = [UIColor clearColor];
    statusLab.text = @"社会人士";
    statusLab.textColor = RGBA(100, 100, 100, 1);
    statusLab.textAlignment = NSTextAlignmentRight;
    statusLab.font = [UIFont systemFontOfSize:15.0f];
    [self.infomationView addSubview:statusLab];
    self.statusLab = statusLab;
    
    // 剪头图片
    UIImageView *arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(70), 100, 20, 20)];
    [arrowImage setImage:[UIImage imageNamed:@"arrowRight"]];
    [self.infomationView addSubview:arrowImage];
    
    UITapGestureRecognizer *statusTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeStatusType)];
    [statusLab addGestureRecognizer:statusTap];
    statusLab.userInteractionEnabled = YES;
    
    // 创建学生证号 view
    self.studenView = [[UIView alloc] initWithFrame:CGRectMake(0, self.infomationView.frame.origin.y + self.infomationView.frame.size.height, WIDTH, 44)];
    self.studenView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.studenView];
    self.studenView.hidden = YES;
    
    // 真实姓名
    UILabel *studenLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(200), 30)];
    studenLabel.backgroundColor = [UIColor clearColor];
    studenLabel.text = @"学生证号";
    studenLabel.textColor = HDVGFontColor;
    studenLabel.textAlignment = NSTextAlignmentLeft;
    studenLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.studenView addSubview:studenLabel];
    
    // 真实姓名输入框
    self.studenTF = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(496), 7, WidthRate(450), 30)];
    self.studenTF.borderStyle = UITextBorderStyleNone;
    self.studenTF.delegate = self;
    self.studenTF.tag = 3;
    self.studenTF.placeholder = @"请输入学生证号";
    self.studenTF.textColor = RGBA(100, 100, 100, 1);
    self.studenTF.textAlignment = NSTextAlignmentRight;
    self.studenTF.font = [UIFont systemFontOfSize:15.0f];
    self.studenTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.studenTF.returnKeyType = UIReturnKeyDone;
    self.studenTF.keyboardType = UIKeyboardTypeNumberPad;
    [self.studenView addSubview:self.studenTF];
    
    CALayer *layer5 = [[CALayer alloc] init];
    layer5.frame = CGRectMake(0, 44 - 0.5, WIDTH, 0.5);
    layer5.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.studenView.layer addSublayer:layer5];
}

// 创建按钮控件 UI
- (void)createButtonControllerUI
{
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:@"下一步" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonClick:)];
    
    [submitButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR,NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = submitButton;
    
//    // 初始化完成设置按钮
//    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    nextButton.frame = CGRectMake(WidthRate(46), HEIGHT - 64 - 60, WIDTH - WidthRate(46) * 2, 44);
//    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
//    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [nextButton addTarget:self action:@selector(nextButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [nextButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
//    [nextButton setBackgroundColor:ButtonBgColor];
//    [nextButton.layer setCornerRadius:6.0f];
//    self.nextButton = nextButton;
//    [self.view addSubview:nextButton];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    [self.studenTF resignFirstResponder];
    [self.nameTF resignFirstResponder];
    [self.areaTF resignFirstResponder];
    
    [self cancelLocatePicker];
}

#pragma mark - HZAreaPicker delegate
- (void)pickerDidChaneStatus:(HZAreaPickerView *)picker
{
    self.areaValue = [NSString stringWithFormat:@"%@%@%@", picker.locate.state, picker.locate.city, picker.locate.district];
    self.fatherVC.cityName = self.areaValue;
    self.cityString = self.areaValue;
    //获取地区ID
//    self.areaID = [PMGetAreaID getAreaIDByArea:[NSString stringWithFormat:@"%@ %@ %@", picker.locate.state, picker.locate.city, picker.locate.district]];
    self.areaID = picker.locate.areaId;
    self.fatherVC.cityId = self.areaID;
    self.cityId = self.areaID; 
}

- (void)finishPick:(NSString *)provinceId CityId:(NSString *)cityId AreaId:(NSString *)areaId ProvinceValue:(NSString *)provinceName CityName:(NSString *)cityName AreaName:(NSString *)areaName
{
    self.areaValue = [NSString stringWithFormat:@"%@%@%@",provinceName,cityName,areaName];
    self.fatherVC.cityName = self.areaValue;
    self.cityString = self.areaValue;
    if (areaId && ![areaId isEqualToString:@""])
    {
        self.cityId = areaId;
        self.areaID = areaId;
    }
    else if (cityId && ![cityId isEqualToString:@""])
    {
        self.cityId = cityId;
        self.areaID = cityId;
    }
    else
    {
        self.cityId = provinceId;
        self.areaID = provinceId;
    }
    
}

- (void)cancelLocatePicker
{
    [self.locatePicker cancelPicker];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.nameTF.tag == 1 && textField == self.nameTF && textField.text.length - range.length + string.length > 12) {
        return NO;
    }
    
    if (self.studenTF.tag == 3 && textField == self.studenTF && textField.text.length - range.length + string.length > 16) {
        return NO;
    }
    
    return YES;
}

#pragma mark - TextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // 如果iPhone4s输入框被键盘遮挡了，就向上移动50个像素
    if (self.studenTF.tag == 3 && textField == self.studenTF) {
        
        if (iPhone4s) {
            // 输入框监听事件
            [textField addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventEditingDidBegin];
            [textField addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
        }
    }
    
    if ([textField isEqual:self.areaTF]) {
        [self.studenTF resignFirstResponder];
        [self.nameTF resignFirstResponder];
        [self.areaTF resignFirstResponder];
        
//        if(!self.locatePicker)
//            self.locatePicker = [[HZAreaPickerView alloc] initWithStyle:HZAreaPickerWithStateAndCityAndDistrict delegate:self];
//        [self.locatePicker showInView:self.view];
        
        AddressPickerViewController *addPicker = [[AddressPickerViewController alloc] init];
        addPicker.addressDelegate = self;
        [self.navigationController pushViewController:addPicker animated:YES];
        
        return NO;
    }
    else {
        [self cancelLocatePicker];
        return YES;
    }
}

// 开始编辑时，整体上移
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.studenTF.tag == 3 && textField == self.studenTF) {
        
        if (iPhone4s) {
            [self moveView:-50];
        }
    }
}
// 结束编辑时，整体下移
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // 用户名过滤敏感字符
//    if (self.nameTF.tag == 1 && textField == self.nameTF) {
//        
//        NSDictionary *dataDict = @{@"name":self.nameTF.text};
//        
//        self.networking = [PMNetworking defaultNetworking];
//        
//        [self.networking request:PMRequestStateApplyReturn WithParameters:dataDict callBackBlock:^(NSDictionary *dict) {
//            
//            if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
//                self.filterMessage = [dict objectNullForKey:@"message"];
//                [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:self.filterMessage];
//                self.nextButton.enabled = NO;
//                self.filterString = @"1";
//            }
//            else {
//                self.nextButton.enabled = YES;
//                self.filterString = @"0";
//            }
//            
//        }showIndicator:NO];
//    }
    
    if (self.studenTF.tag == 3 && textField == self.studenTF) {
        
        if (iPhone4s) {
            [self moveView:50];
        }
    }
}

- (void)moveView:(CGFloat)move
{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y += move;//view的y轴上移
    self.view.frame = frame;
    [UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}


- (void)changeStatusType
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"社会人士",@"在校学生", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            self.statusLab.text = @"社会人士";
            self.certifyType = @"1";
            self.layer4.frame = CGRectMake(0, 132 - 0.5, WIDTH, 0.5);
            self.studenView.hidden = YES;
            break;
        case 1:
            self.statusLab.text = @"在校学生";
            self.certifyType = @"0";
            self.layer4.frame = CGRectMake(WidthRate(26), 132 - 0.5, WIDTH, 0.5);
            self.studenView.hidden = NO;
            break;
    }
}


#pragma mark - 下一步按钮响应事件
- (void)nextButtonClick:(UIButton *)sender
{
    // 内容不能为空
    if (self.nameTF.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入代言人真实姓名"];
        return;
    }
    if (self.areaTF.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请选择代言人所在区域"];
        return;
    }
    
    if (self.nameTF.text.length != 0 && self.areaTF.text.length != 0) {
        
        if ([self.certifyType isEqualToString:@"0"] && self.studenTF.text.length == 0) {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入学生证号"];
        }
        else {
            // 用户名过滤敏感字符
            if ([self.filterString isEqualToString:@"1"]) {
                [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:self.filterMessage];
            }
            else {
                // 个人资料
                NSMutableDictionary *userInfoData;
                
                // 申请代言用户还没支付加盟费
                if (![self.applyStatus isEqualToString:@""] && [self.applyStatus integerValue] == 0) {
                    
                    NSString *shopCertifyId = [self.reSellerDataDict objectNullForKey:@"shopCertifyId"];
                    NSString *mobile = [self.reSellerDataDict objectNullForKey:@"mobile"];
                    NSString *name = [self.reSellerDataDict objectNullForKey:@"name"];
                    NSString *areaName = [self.reSellerDataDict objectNullForKey:@"areaName"];
                    NSString *certifyNum = [self.reSellerDataDict objectNullForKey:@"certifyNum"];
                    NSString *certifyType = [self.reSellerDataDict objectNullForKey:@"certifyType"];
                    NSString *areaId = @"";
                    if (self.areaID && ![self.areaID isEqualToString:@""])
                    {
                        areaId = self.areaID;
                    }
                    else
                    {
                        areaId = [self.reSellerDataDict objectNullForKey:@"areaId"];
                    }
                    NSString *isValidate = @"YES";
                    
                    // 如果修改了数据
                    if (![self.nameTF.text isEqualToString:name] || ![self.areaTF.text isEqualToString:areaName] || ![self.certifyType isEqualToString:certifyType]) {
                        
                        NSString *name = [NSString stringWithFormat:@"%@", self.nameTF.text];
                        NSString *areaName = [NSString stringWithFormat:@"%@", self.areaTF.text];
                        NSString *areaId = [NSString stringWithFormat:@"%@", self.areaID];
                        
                        if ([self.certifyType isEqualToString:@"0"] || ![certifyType isEqualToString:@"1"]) {
                            certifyNum = [NSString stringWithFormat:@"%@", self.studenTF.text];
                            
                            userInfoData = [@{@"shopCertifyId":shopCertifyId,@"mobile":mobile,@"name":name,@"areaName":areaName,@"areaId":areaId,@"certifyType":certifyType,@"certifyNum":certifyNum,@"isValidate":isValidate} mutableCopy];
                        }
                        else {
                            NSString *certifyNum = @"";
                            userInfoData = [@{@"shopCertifyId":shopCertifyId,@"mobile":mobile,@"name":name,@"areaName":areaName,@"areaId":areaId,@"certifyType":self.certifyType,@"certifyNum":certifyNum,@"isValidate":isValidate} mutableCopy];
                        }
                    }
                    else {
                        userInfoData = [@{@"shopCertifyId":shopCertifyId,@"mobile":mobile,@"name":name,@"areaName":areaName,@"areaId":areaId,@"certifyType":certifyType,@"certifyNum":certifyNum,@"isValidate":isValidate} mutableCopy];
                    }
                    
                }
                else {
                    NSString *mobile = [self.reSellerDataDict objectNullForKey:@"mobile"];
                    NSString *name = [NSString stringWithFormat:@"%@", self.nameTF.text];
                    NSString *areaName = [NSString stringWithFormat:@"%@", self.areaTF.text];
                    NSString *areaId = [NSString stringWithFormat:@"%@", self.areaID];
                    
                    NSString *certifyNum = @"";
                    if ([self.certifyType isEqualToString:@"0"]) {
                        certifyNum = [NSString stringWithFormat:@"%@", self.studenTF.text];
                    }
                    NSString *isValidate = @"YES";
                    
                    userInfoData = [@{@"mobile":mobile,@"name":name,@"areaName":areaName,@"areaId":areaId,@"certifyType":self.certifyType,@"certifyNum":certifyNum,@"isValidate":isValidate} mutableCopy];
                }
                
                
//                SWPayCostViewController *payCostVC = [[SWPayCostViewController alloc] init];
//                payCostVC.userDataDict = userInfoData;
//                [self.navigationController pushViewController:payCostVC animated:YES];
                
                
                self.networking = [PMNetworking defaultNetworking];
                
                [self.networking request:PMRequestStateApplyReSeller WithParameters:userInfoData callBackBlock:^(NSDictionary *dict) {
                    
                    if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
                        
                        NSString *orderCode = [[dict objectNullForKey:@"data"] objectNullForKey:@"orderCode"];
                        NSString *orderType = [[dict objectNullForKey:@"data"] objectNullForKey:@"orderType"];
                        NSString *orderMoney = [[dict objectNullForKey:@"data"] objectNullForKey:@"orderMoney"];
                        
                        // 页面跳转以及传送数据
                        SWPayCostViewController *payCostVC = [[SWPayCostViewController alloc] init];
                        
                        //NSMutableDictionary *userInfoData = [userInfoData mutableCopy];
                        [userInfoData setObject:orderCode forKey:@"orderCode"];
                        [userInfoData setObject:orderType forKey:@"orderType"];
                        [userInfoData setObject:orderMoney forKey:@"orderMoney"];
                        payCostVC.userDataDict = userInfoData;// 传字典数据
                        [self.navigationController pushViewController:payCostVC animated:YES];
                    }
                    else {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alertView show];
                    }
                    
                }showIndicator:NO];
                
            }
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
