//
//  SWPayCostViewController.h
//  changeViewController
//
//  Created by P&M on 15/6/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

// 支付开店费用控制器
#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"

@interface SWPayCostViewController : UIViewController

@property (strong, nonatomic) UIView *infosView;
@property (strong, nonatomic) UILabel *nameLB;
@property (strong, nonatomic) UILabel *mobileLB;
@property (strong, nonatomic) UILabel *areaLB;

@property (strong, nonatomic) UIView *costView;
@property (strong, nonatomic) UILabel *costLabel;

@property (strong, nonatomic) UIView *paymentView;
@property (strong, nonatomic) UIButton *selectButton;
@property (strong,nonatomic) UIView *carInfoView;
@property (strong,nonatomic) UITextField *carNumField;
@property (strong,nonatomic) UITextField *carPassField;

@property (strong, nonatomic) UILabel *moneyLabel;

@property (strong, nonatomic) NSMutableDictionary *userDataDict;

@property (assign, nonatomic) BOOL isFromJoin;

@property (strong,nonatomic) UIScrollView *costScrollView;

@end
