//
//  TranSelectViewController.h
//  changeViewController
//
//  Created by ZhangEapin on 15/5/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TurnBackSendViewController;

@interface TranSelectViewController : UITableViewController

@property (weak,nonatomic) TurnBackSendViewController *myDelegate;

@end
