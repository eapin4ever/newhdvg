//
//  PMChooseAddressVC.m
//  changeViewController
//
//  Created by pmit on 14/12/18.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMChooseAddressVC.h"
#import "YBSureOrderViewController.h"

@interface PMChooseAddressVC ()

@end

@implementation PMChooseAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.sureOrderVC.addressDic = self.addressSectionArray[indexPath.section];
    NSString *addressId = [self.sureOrderVC.addressDic objectNullForKey:@"id"];
    NSString *areaId = [self.sureOrderVC.addressDic objectNullForKey:@"areaId"];
    NSMutableArray *idsArr = [NSMutableArray array];
//    NSMutableString *ids = [NSMutableString string];
    NSInteger allCount = 0;
    for (NSInteger i = 0; i < self.productArr.count; i++)
    {
        NSDictionary *productDic = self.productArr[i];
        NSLog(@"productDic --> %@",productDic);
        NSString *productId = [[productDic objectNullForKey:@"product"] objectNullForKey:@"id"];
        NSString *stockId = [productDic objectNullForKey:@"stockId"];
        NSString *shopId = @"null";
        NSString *productCount = [productDic objectNullForKey:@"productCount"];
        allCount += [productCount integerValue];
        
        if (!isNull([productDic objectNullForKey:@"shopId"]) && ![[productDic objectNullForKey:@"shopId"] isEqualToString:@""]) {
            shopId = [productDic objectNullForKey:@"shopId"];
        }
        
        NSString *idString = [NSString stringWithFormat:@"%@_%@_%@_%@_%@",productId,areaId,stockId,shopId,productCount];
        [idsArr addObject:idString];
    }
    NSString *ids = [idsArr componentsJoinedByString:@","];
    NSDictionary *parameter = @{@"ids":ids,@"addressId":addressId,@"productCount":@(allCount)};
    
    [[PMNetworking defaultNetworking] request:PMRequestStateGetCarrige WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
        self.sureOrderVC.carriagePrice = [[[dic objectNullForKey:@"data"] objectNullForKey:@"carriage"] doubleValue];
        [self.sureOrderVC refreshData:0];
        [self.sureOrderVC refreshData:5];
        [self.navigationController popViewControllerAnimated:YES];
        
    } showIndicator:NO];
    
}

@end
