//
//  PMToastHint.h
//  changeViewController
//
//  Created by pmit on 15/1/14.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMToastHint : UIView
//可更换提示图片
@property(nonatomic,strong)UIImageView *iv;
@property (assign,nonatomic) BOOL isRight;
+ (PMToastHint *)defaultToastWithRight:(BOOL)isRight;
- (void)showHintToView:(UIView *)view ByToast:(NSString *)title;
@end
