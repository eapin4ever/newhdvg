//
//  PMNetworking.h
//  changeViewController
//
//  Created by pmit on 14/11/11.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

//自定义一个返回字典类型的block
typedef void (^callBackDicBlock)(NSDictionary *dic);
typedef void(^connectRetry)();

typedef NS_ENUM(NSInteger, PMRequestState)
{
    //登陆
    /*
     参数：
     code:(string)用户名,
     password:(string)密码(md5加密),
     verificationCode:(string)验证码
     rememberMe:(BOOL)是否记住我
     point:(string)坐标
     */
    PMRequestStateLogin = 0,
    //注册
    /*
     参数：
     name:(string)用户账号
     password:(string)密码(md5加密)
     mobile:(string)手机号码
     email:(string)邮箱地址
     verificationCode:(string)验证码
     */
    PMRequestStateRegister,
    //登出
    /*
     参数：
     PM_SID:(string)Token
     */
    PMRequestStateLogout,
    //检查用户名是否存在
    /*
     参数:
     userName:(string)用户名
     */
    PMRequestStateCheckClientUserName,
    //记住我,不需要参数
    PMRequestStateGetRememberMe,
    
    
    //获取短信验证码
    /*
     参数:
     sendSMS:(string)验证码
     */
    PMRequestStateSendSMS,
    
    //验证手机是否注册过申请代言并且获取短信验证码(带当前用户)
    /*
     参数:
     PM_SID	     Y	   String		Token
     mobile	     Y	   String		手机号
     */
    PMRequestStateGetMobileCode,
    
    //验证短信验证码(申请代言验证用户资料时用到)
    /*
     参数:
     validateCode	Y	String		短信验证码
     */
    PMRequestStateCheckValidCode,
    
    //获取代言商信息（申请代言）
    /*
     参数:
     PM_SID	     Y	    String		Token
     */
    PMRequestStateGetApplyReSeller,
    
    //申请代言 (最终提交申请审核全部数据)
    /*
     参数:
     PM_SID         	   Y	String		Token
     shopCertifyId	       Y	String		店铺资质表(t_shop_certify)主键
     name	               Y	String		真实姓名
     userCard:	           Y	String		身份证号码
     mobile:	           Y	String		电话号码
     email	               Y	String		电子邮件
     areaId	               Y	String		区编号
     cityId	               Y	String		市编号
     provinceId	           Y	String		省编号
     userAndCardPhotoId	   Y	String		手持身份证照
     userCardPhotoId	   Y	String		身份证正面照
     */
    PMRequestStateApplyReSeller,
    
    
    //忘记密码
    /*
     参数:
     code	            Y	String		登录账号(用户名、手机、邮箱)
     verificationCode	Y	String		验证码
     */
    PMRequestStateGetUserInfo,
    
    //忘记密码发送邮箱验证
    /*
     参数:
     mail	Y	String		邮箱
     */
    PMRequestStateSendMail,
    
    //设置新密码
    /*
     参数:
     code	    Y	String		账号
     password	Y	String		md5加密
     validCode  Y	String		md5加密
     */
    PMRequestStateSetPassword,
    
    /*商品首页*/
    //首页
    /*
     longitude	N	String		经度（如果是手机端必须传，pc端不用传）
     latitude	N	String		纬度（如果是手机端必须传，pc端不用传）
     */
    PMRequestStateProductIndex,
    
    //搜索商品
    /*
     query          N	String		搜索词
     areaName       N	String		商品首页时取到（区域id,区域名）中间使用逗号隔开,如果是筛选条件，则可不填
     currentPage	Y	int         当前页
     userId         N	String		用户ID
     */
    PMRequestStateProductSearch,
    
    /*商品详情*/
    /*
     id         Y	String		商品id
     shopId     N	String		店铺Id
     userId     N	String		用户Id
     */
    PMRequestStateProductInfo,
    //商品评价
    /*
     PM_SID         Y	String		Token
     currentPage	Y	Int         当前页
     productId      Y	String		商品id
     */
    PMRequestStateShopShowShopDisucss,
    //获取商品的价格和库存数
    /*
     calcFor	Y	String		A或者B或者AB(表示A规格或者B规格或者AB规格)
     aid        Y	String		A规格值的ID
     bid        Y	String		B规格值的ID
     productId	Y	String		商品ID
     areaId     Y	String		区域编号
     areaName	Y	String		区域名称
     */
    PMRequestStateProductCalcStockProduct,
    
    //+我的代言
    /*
     PM_SID     Y	String		Token
     areaId     Y	String		区域ID
     productId	Y	String		商品id
     */
    PMRequestStateShareAddToShop,
    //关注商品
    /*
     productId	Y	String		商品id
     shopId     N	String		店铺 id
     userId     Y	String		用户id
     */
    PMRequestStateCareAddCareProduct,
    //取消关注商品
    /*
     shopId     N	String		店铺id
     PM_SID     Y	String		用户id
     productId	Y	String		商品id
     */
    PMRequestStateCareDeleteCareProduct,
    //关注店铺
    /*
     shopId	Y	String		店铺id
     userId	Y	String		用户id
     */
    PMRequestStateCareAddCareHome,
    
    //我的资料
    /*
     参数:
     PM_SID:(string)token
     */
    PMRequestStateMyInfo,
    
    //修改我的资料
    /*
     PM_SID	    Y	String		Token
     nickName	N	String		昵称
     sex	    N	String		1：男   -1：女
     birthday	N	String		yyyy-mm-dd
     areaId	    N	String		区编码
     */
    PMRequestStateMyUpdate,
    
    /*收货地址*/
    //获取收货地址
    /*
     参数:
     PM_SID	   Y	String		Token
     */
    PMRequestStateMyGetAddress,
    
    //新增收货地址
    /*
     参数:
     PM_SID	    Y	String		Token
     areaId	    Y	String		区域ID
     address	Y	String		详细地址
     name	    Y	String		收货人姓名
     mobile	    Y	String		手机号码
     isDefault	N	String		是否为默认地址（1：是  -1：否）
     */
    PMRequestStateMyAddAddress,
    
    //更新收货地址
    /*
     参数:
     PM_SID     Y	String		Token
     id         Y	String		地址id
     areaId     Y	String		区域ID
     address	Y	String		详细地址
     name       Y	String		收货人姓名
     mobile     Y	String		手机号码
     isDefault	N	String		是否为默认地址（1：是  -1：否）
     */
    PMRequestStateMyUpdateAddress,
    
    //删除收货地址
    /*
     参数:
     PM_SID	    Y	String		Token
     addressId	Y	String		收货地址ID
     */
    PMRequestStateMyDelAddress,
    
    //设置默认收货地址
    /*
     参数:
     PM_SID	    Y	String		Token
     addressId	Y	String		收货地址ID
     */
    PMRequestStateSetDefaultAddress,
    
    //账号安全
    /*
     参数:
     PM_SID	   Y	String		Token
     */
    PMRequestStateSafeInfo,
    
    //邮箱绑定
    /*
     参数:
     PM_SID	   Y	String		Token
     name	   Y	String		用户名
     email	   Y	String		新邮箱
     */
    PMRequestStateSafeUpdateEmail,
    
    //手机绑定
    /*
     参数:
     PM_SID	        Y	String		Token
     validateCode	Y	String		验证码（通过获取短信验证码接口获取）
     mobile	        Y	String		新手机
     */
    PMRequestStateSafeUpdateMobile,
    
    //修改密码
    /*
     参数:
     PM_SID	   Y	String		Token
     oldPwd	   Y	String		旧密码md5加密
     newPwd	   Y	String		新密码md5加密
     */
    PMRequestStateSafeModifyPassword,
    
    /*购物车*/
    //加入购物车
    /*
     参数:
     PM_SID	Y	String		Token
     productId	Y	String		商品ID
     productCount	Y	String		商品数量
     aSpecValId	N	String		商品规格值ID（比如颜色）
     bSpecValId	N	String		商品规格值ID（比如尺码）
     */
    PMRequestStateShopCartAddTo,
    //显示购物车
    /*
     参数:
     PM_SID     Y	String		Token
     */
    PMRequestStateSafeShopCartShowBuyCart,
    //修改购物车
    /*
     参数:
     PM_SID         Y	String		Token
     productId      Y	String		商品ID
     count          Y	int		商品数量
     aSpecValId     N	String		商品规格值ID（比如颜色）
     bSpecValId     N	String		商品规格值ID（比如尺码）
     totalPrice     Y	double		商品总价
     cartNum        Y	int		商品总件数
     */
    PMRequestStateSafeShopCartModifyCount,
    //删除单条购物车
    /*
     参数:
     PM_SID         Y	String		Token
     productId      Y	String		商品ID
     aSpecValId     N	String		商品规格值ID（比如颜色）
     bSpecValId     N	String		商品规格值ID（比如尺码）
     */
    PMRequestStateSafeShopCartDelProduct,
    //删除多条购物车
    /*
     参数:
     PM_SID         Y	String		Token
     productSpecId	Y	String		商品ID_规格值ID_规格值ID（多个使用,逗号隔开）
     */
    PMRequestStateSafeShopCartDelBatchProduct,
    
    
    /*3.15订单确认*/
    //订单信息（从购物车进来）
    /*
     参数:
     PM_SID	   Y	String		Token
     ids	   Y	String		商品ID_规格值ID_规格值ID（多个使用,逗号隔开）
     */
    PMRequestStateShowOrder,
    
    //订单信息（点击立刻购买时）
    /*
     参数:
     PM_SID         Y	String		Token
     ids            Y	String		商品ID_区域ID_A规格值ID_B规格值ID_店铺ID（多个使用,逗号隔开）,没有值用null代替，如1c02875ca13f48f7967a7c87f0840dc3_440100_7ae953f8c0184ee480fecac2107f9469_null_null,fcc342ce42584dbaad9c835b7c8eb2ef_440100_6f69869961d24a54b823b61f3eff5187_null_null
     
     productCount	Y	Integer		详情页用户选择的商品个数
     */
    PMRequestStateShowOrderOne,

    
    //提交订单
    /*
     参数:
     PM_SID	            Y	String		Token
     defaultAddressId	Y	String		选择的收货地址id
     payWay	            N	String		支付方式
     receiptTitle	    N	String		发票抬头
     receiptType	    N	String		发票类型（1：个人   -1：单位）
     receiptContent     N	String		发票内容（1：商品明细  -1：办公用品）
     */
    PMRequestStateSubmitOrder,
    
    //立即支付
    /*
     参数:
     PM_SID     Y	String		Token
     orderCode	Y	String		订单编号（根据上一步提交订单取得）
     totalPrice	Y	doubel		总价（根据订单信息获取）
     totalNum	Y	int         商品笔数（根据订单信息获取）
     pro        Y	String		商品名称（多个商品用,逗号隔开）
     */
    PMRequestStateAlipay,
    
    /*3.16我的交易*/
    //我的订单
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   Y    int         当前页
     pageSize      N    int         一页显示条数（默认20条）
     tradeStatus   N    String      交易状态
     tradeTime     N    String      交易时间
     */
    PMRequestStateShopOrderShowMyOrderList,
    
    //取消订单
    /*
     参数：
     status        Y    String      状态
     orderCode     Y    String      订单号
     */
    PMRequestStateUpdateOrderStatus,
    
    //查看物流
    /*
     参数：
     orderId	   Y	String		订单id
     */
    PMRequestStateShopOrderLogistics,
    
    //确认收货验证检查
    /*
     参数：
     orderId	   Y	String		订单编号(是某一个商品id不是orderCode)
     */
    PMRequestStateCheckIsGetProduct,
    
    //确认收货
    /*
     参数：
     orderId	   Y	String		订单编号(是某一个商品id不是orderCode)
     */
    PMRequestStateSureGetProduct,
    
    /*退换货管理*/
    //退换货验证检查
    /*
     参数：
     orderId	   Y	String		订单编号(是订单的id不是orderCode)
     */
    PMRequestStateCheckReturnBack,
    
    //查看退/换货记录
    /*
     参数：
     tabNum        Y    Integer     选项卡标志 0：换货1：退货  26：退换货
     currentPage   Y    int         当前页
     orderCode     N    String      订单号
     PM_SID        Y    String      Token
     */
    PMRequestStateShowReturn,
     
    /*商品评价*/
    //获取商品评价信息
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   Y    int         当前页
     pageSize      N    int         一页显示条数（默认20条）
     tradeStatus   N    String      交易状态
     tradeTime     N    String      交易时间
     */
    PMRequestStateShowMyOrderProductDiscussList,
    
    //评价商品信息
    PMRequestStateAddOrderDiscuss,
    
    //申请退换货
    /**
     参数：
     PM_SID	       Y	String		Token
     orderId	   Y	String		订单Id
     productId	   Y	String		商品Id
     productNum	   Y	String		商品数量
     reason	       Y	String		退换货原因（默认传1给后台） 1：收到商品破损  2：商品错发/漏发 3：收到商品与描述不符  4：收到假货
     remark	       N	String		退换货说明
     flag	       Y	Integer		标志退换货 1：退货，  -1：换货
     */
     PMRequestStateApplyReturn,
    
    /*3.17代言店铺*/
    //检查是否已经绑定银行卡
    /*
     参数：
     PM_SID        Y    String      Token
     */
    PMRequestStateShareCheckBank,
    
    //绑定银行卡/解绑银行卡 (注：解绑银行卡只需 PM_SID 一个参数)
    /*
     //绑定
     PM_SID     Y	String		Token
     name       Y	String		真实姓名
     userCard	Y	String		身份证
     bankCard	Y	String		银行卡号
     bank       Y	String		开户银行
     
     //解绑
     参数：
     PM_SID     Y	String		Token
     bank       Y	String		双引号（空字符串）
     bankCard	Y	String		双引号（空字符串）
     */
    PMRequestStateShareUpdateBank,
    
    //我的客户
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     */
    PMRequestStateShareShowMyClient,
    
    //代言获利
    /*
     参数：
     PM_SID        Y    String      Token
     */
    PMRequestStateShareShowMySell,
    
    //获利详情
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     createDt      N    date        开始时间
     createDt2     N    date        结束时间
     */
    PMRequestStateShareShowProfit,
    
    //提现申请
    /*
     参数：
     PM_SID            Y    String      Token
     money             Y    double      提现金额
     verificationCode  Y    String      验证码
     */
    PMRequestStateShareApplyGetMoney,

    //提现明细
    /*
     参数：
     PM_SID        Y    String      Token
     currentPage   N    int         当前页
     createDt      N    date        开始时间
     createDt2     N    date        结束时间
     */
    PMRequestStateShareShowGetMoney,
    
    //取消/删除提现明细
    /*
     参数：
     PM_SID        Y    String      Token
     id            Y    String      提现明细id
     flag          N    date        取消/删除
     */
    PMRequestStateShareCancelGetMoney,
    
    //图片上传
    /*
     参数：
     PM_SID        Y    String      Token
     flag          Y    String      userPhoto：用户头像  mobileShopLogo：手机标志
     */
    PMRequestStateUploadImg,
    
    /*更多*/
    //意见反馈
    /*
     参数：
     PM_SID        Y    String      Token
     fbContent     Y    String      反馈内容
     */
    PMRequestStateUserFeedBack,
    
    /*店铺设置*/
    //获取店铺信息
    /*
     参数：
     PM_SID        Y    String      Token
     */
    PMRequestStateShareShowShopHome,
    //获取店铺信息
    /*
     参数：
     PM_SID     Y	String		Token
     id         Y	String		店铺id（根据店铺信息获取）
     shopName	Y	String		店铺名称
     telephone	Y	String		客服电话
     shopAbout	N	String		店铺介绍
     */
    PMRequestStateShareUpdateShopHome,
    
    //代言的商品
    //检查库存
    /*
     PM_SID     Y	String		Token
     */
    PMRequestStateShareSelectAllShopProduct,
    
    //显示代言中的商品/下架的商品
    /*
     PM_SID         Y	String		Token
     currentPage	Y	Int         当前页
     productName	N	String		商品名称，用于搜索
     flag           Y	String		1: 代言中商品  -1: 下架商品
     */
    PMRequestStateShareShowSellsProducts,
    
    //上架、下架
    /*
     PM_SID         Y	String		Token
     status         Y	String		标志  1：上架   -1：下架
     productIds     N	String		多个商品Id的拼接，如04652eb5e5fa4d1a9153730d7238857f,04855d5313af43df80d4766707def575
     */
    PMRequestStateShareUpdateBatchDown,
    
    //商品库显示
    /*
     PM_SID             Y	String		Token
     currentPage		Y	Int         当前页
     pageSize           N	Int         一页显示多少个
     searchContent      N	String		搜索框中的内容
     areaId             Y	String		价格排序asc 递增  desc递减
     priceOrder         Y	String		价格排序asc 递增  desc递减
     xlTotalOrder       Y	String		销量排序asc 递增  desc递减
     sellScale          Y	String		佣金比率排序asc 递增  desc递减
     */
    PMRequestStateShareShowProductStoreInfo,
    
    //商品库筛选
    /*
     PM_SID             Y	String		Token
     currentPage		Y	Int         当前页
     pageSize           N	Int         一页显示多少个
     searchContent      N	String		搜索框中的内容
     areaId             Y	String		价格排序asc 递增  desc递减
     priceOrder         Y	String		价格排序asc 递增  desc递减
     xlTotalOrder       Y	String		销量排序asc 递增  desc递减
     sellScale          Y	String		佣金比率排序asc 递增  desc递减
     firstClassId       Y   string      一级分类
     secondClassId      Y   string      二级分类
     threeClassId       Y   string      三级分类
     */
    PMRequestStateClassGetAllClassByTree,
    
    //验证购买的活动或折扣商品数量
    /*
     products	    Y	  String		每个产品的ID
     buyNum         Y     Int           购买数量
     PM_SID	        Y	  String		用户ID
     */
    PMRequestStateCheckLimitBuyNum,
    
    //检查库存接口
    /*
     productId	      Y	    String		商品ID
     aSpecValId	      N	    String		A规格值ID
     bSpecValId	      N	    String		B规格值ID
     cSpecValId	      N	    String		C规格值ID
     dSpecValId	      N	    String		D规格值ID
     eSpecValId	      N	    String		E规格值ID
     */
    PMRequestStateCheckStockTotal,
    
    
    /*我的关注*/
    //关注的商品
    /*
     currentPage	Y	int         当前页
     PM_SID         Y	String		Token
     */
    PMRequestStateCareShowCareProduct,
    //关注的店铺
    /*
     currentPage	Y	int         当前页
     PM_SID         Y	String		Token
     */
    PMRequestStateCareShowCareHome,
    //取消关注商品
    /*
     id             Y	String		商品ID
     */
    PMRequestStateCareCancelCareProduct,
    //批量取消关注商品
    /*
     ids            Y	String		用showCareHome接口返回的scpId组成的，例如：ed767d9b65cf441fafd02c0a1b58cd81,f332862ca8cf417fa016422f525adadd,f38c4cd1cc5f489d82bfe95e55123f4d
     */
    PMRequestStateCareCancelBatchCareProduct,
    //批量取消关注店铺
    /*
     ids            Y	String		用showCareHome接口返回的scpId组成的，例如：ed767d9b65cf441fafd02c0a1b58cd81,f332862ca8cf417fa016422f525adadd,f38c4cd1cc5f489d82bfe95e55123f4d
     */
    PMRequestStateCareCancelBatchCare,
    
    
    
    
    /*版本更新
     参数:
     versionNo      Y	String		手机自身当前版本号
     type           Y	String		1-1：android-phone,
                                    1-2：android-pad,
                                    2-1-1：ios-ec-phone,
                                    2-1-2：ios-online-phone,
                                    2-2-1：ios-ec-pad,
                                    2-2-2：ios-online-pad
     */
    PMRequestStateCheckSysVersion,
    
    
    /*分类*/
    //一级类目
    /*
     参数：
     areaId     Y	String		区域Id
     */
    PMRequestStateSelectClass,
    //所有类目
    /*
     classId	Y	String		一级类目的ID
     */
    PMRequestStateSelectPreAndChildClass,
    
    /*智能搜*/
    /*
     PM_SID         Y	String		Token
     currentPage	Y	int         当前页
     pageSize       N	int         一页显示条数（默认20条）
     longitude      N	String		经度
     latitude       N	String		纬度
     */
    
    PMRequestStateSearchProductInCountInfo,
    
    /*检查是否在线*/
    /*
     PM_SID     Y	String		Token
     */
    PMRequestStateIsLogin,
    
    /*
     参数:
     ak=VTimUSgGn3QGYu1Hz97isxR0
     location=纬度,经度
     output=json
     pois=0   //默认值0 不显示周边信息
     */
//    PMRequestStateGetLocationDetail,
    
    PMRequestStateGetPushLog,
    
    /*
     微信支付
     orderCode=订单ID
     
     */
    PMRequestStateGetWXPay,
    
    /*
     通过orderCode查询商品
     PM_SID:用户token
     orderCode:订单号
     */
    
    PMRequestStateGetOrderByOrderCode,
    
    /*
     根据年月判断得出用户的红包记录
     
     */
    
    PMRequestStateGetOptTradeDetail,
    
    /*
     获取个人信息箱列表
     */
    PMRequestStateGetPushMessageList,
    
    /*
     获取消息具体
     */
    PMRequestStateGetPushMessageDetail,
    
    /*
     获取到主题前三个商品
     */
    PMRequestStateShowZtList,
    
    /*
     获取预售商品
     */
    PMRequestStateList,
    
    /*
     品牌折扣商品
     */
    PMRequestStateZKProductList,
    
    /*
     预售商品详情
     id             商品ID
     shopId         店铺ID
     userId         用户ID
     longitude      经度
     latitude       纬度
     */
    PMRequestStateYSProductInfo,
    
    /*
     折扣商品详情
     id             商品ID
     shopId         店铺ID
     userId         用户ID
     longitude      经度
     latitude       纬度
     */
    PMRequestStateZKProductInfo,
    
    PMRequestStateGetBan,
    
    PMRequestStateGetCarrige,
    
    PMRequestStateGetIsUseRed,
    
    /*
     广告
     typeName  广告类型
     */
    PMRequestStateGetAdvertising,
    
    /*
     退换货物流信息
     expressCompany  快递公司
     expressNo       快递单号
     id              
     PM_SID          Token
     */
    PMRequestStateUpdateLogistics,
    
    PMRequestStateShowReturnDetail,
    
    PMRequestStateShowBackAddress,
    
    PMRequestStatesCancelApply,
    
    PMRequestStateLogCompany,
    
    PMRequestStateCheckOrderIsPay,
    
    PMRequestStateGetShopFile,
    
    PMRequestStateUpdateShopHome,
    
    PMRequestStateCheckIndexVersion,
    
    PMRequestStateSearchKey,
    
    PMRequestStateDeleteHomeProduct,
    
    PMRequestStateDoShopByCard,
    
    PMRequestStateCheckOrderStatus,
    
    PMRequestStateGetProvinceData,
    
    PMRequestStateGetCityData,
    
    PMRequestStateCancelBank,
    
    PMRequestStateShowMyLogistics
    
};

@interface PMNetworking : NSObject

@property (copy,nonatomic) connectRetry retry;

+ (PMNetworking *)defaultNetworking;

//验证码，类方法，直接调用
+ (UIImage *)getVerifyCode;


//各种网络请求
- (void)request:(PMRequestState)requestState WithParameters:(id)parameter callBackBlock:(callBackDicBlock)callback showIndicator:(BOOL)show;

- (void)requestWithPost:(PMRequestState)requestState WithParameters:(id)parameter callBackBlock:(callBackDicBlock)callback showIndicator:(BOOL)show;

//测试上传图片的方法
- (void)uploadImage:(NSDictionary *)params imageData:(NSData *)imageData success:(callBackDicBlock)successAction;


- (void)alipay:(NSDictionary *)params target:(id)target Index:(NSInteger)index;

//微信支付跳转
//- (void)wxPay:(NSDictionary *)params target:(id)target;

- (void)getInShopWithShopId:(NSString *)shopId target:(id)target;

- (NSString *)shareShopProductWithProductId:(NSString *)productId shopId:(NSString *)shopId;


@end
