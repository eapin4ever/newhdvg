//
//  SearchResultViewController.h
//  changeViewController
//
//  Created by pmit on 15/7/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@interface SearchResultViewController : UIViewController

@property (copy,nonatomic) NSString *searchString;
@property (strong,nonatomic) NSMutableArray *brandIds;
@property (strong,nonatomic) NSMutableArray *specIds;
@property (copy,nonatomic) NSString *classIdString;
@property (copy,nonatomic) NSString *className;
@property (strong,nonatomic) NSMutableArray *brandNameStringArr;
@property (strong,nonatomic) UITableView *firstClassTableView;//一级类别tableview
@property (strong,nonatomic) UITableView *secondClassTableView;//一级类别tableview
@property (strong,nonatomic) NSMutableDictionary *specDic;
@property(nonatomic,strong) NSMutableArray *dicArray;
@property (copy,nonatomic) NSString *areaName;
@property (strong,nonatomic) AFHTTPRequestOperationManager *manager;
@property (copy,nonatomic) NSString *secondClassId;


@end
