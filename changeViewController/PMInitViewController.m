//
//  PMInitViewController.m
//  changeViewController
//
//  Created by P&M on 15/5/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMInitViewController.h"
#import "MasterViewController.h"

@interface PMInitViewController ()

@property (strong,nonatomic) NSTimer *myTimer;
@property (assign,nonatomic) NSInteger timerCount;
@property (strong,nonatomic) NSArray *starImageArr;
@property (strong,nonatomic) UIScrollView *myScroll;

@end

//单例，方便调用
static PMInitViewController *_initViewVC;

@implementation PMInitViewController

+ (PMInitViewController *)defaultInitVC
{
    return _initViewVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.timerCount = 0;
    UIImage *startImage1 = [UIImage imageNamed:@"start1"];
    UIImage *startImage2 = [UIImage imageNamed:@"start2"];
    self.starImageArr = @[startImage1,startImage2];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self createLoadingUI];
}

- (void)createLoadingUI
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    imageView.image = self.starImageArr[1];
    [self.view addSubview:imageView];
    
    // 加载页
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(loadingImage:) userInfo:nil repeats:YES];
}

#pragma mark  loading轮播
- (void)loadingImage:(NSTimer *)timer
{
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    MasterViewController *master = [[MasterViewController alloc] init];
    PMNavigationController *navi = [[PMNavigationController alloc] initWithRootViewController:master];
    [UIApplication sharedApplication].statusBarHidden = NO;
    window.rootViewController = navi;
    [self.myTimer invalidate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
