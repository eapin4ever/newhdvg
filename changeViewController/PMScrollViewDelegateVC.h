//
//  PMScrollViewDelegateVC.h
//  changeViewController
//
//  Created by pmit on 15/1/8.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMScrollViewDelegateVC : NSObject<UIScrollViewDelegate>
@property(nonatomic,weak)UIPageControl *pageControl;
@property(nonatomic,strong)UIView *viewForZoom;
@end
