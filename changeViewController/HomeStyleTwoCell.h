//
//  HomeStyleTwoCell.h
//  changeViewController
//
//  Created by pmit on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeStyleTwoCell : UITableViewCell

@property (strong,nonatomic) UIImageView *upBigIV;
@property (strong,nonatomic) UIImageView *upOneIV;
@property (strong,nonatomic) UIImageView *upTwoIV;
@property (strong,nonatomic) UIImageView *upThreeIV;
@property (strong,nonatomic) UIImageView *upFourIV;
@property (strong,nonatomic) UIImageView *upFiveIV;
@property (strong,nonatomic) UIImageView *upSixIV;
@property (strong,nonatomic) UIImageView *downLeftIV;
@property (strong,nonatomic) UIImageView *downRightIV;
@property (strong,nonatomic) UILabel *upBigTitleLB;
@property (strong,nonatomic) UILabel *upOneTitleLB;
@property (strong,nonatomic) UILabel *upTwoTitleLB;
@property (strong,nonatomic) UILabel *upThreeTitleLB;
@property (strong,nonatomic) UILabel *upFourTitleLB;
@property (strong,nonatomic) UILabel *upFiveTitleLB;
@property (strong,nonatomic) UILabel *upSixTitleLB;
@property (strong,nonatomic) UILabel *downLeftTitleLB;
@property (strong,nonatomic) UILabel *downRightTitleLB;

@property (strong,nonatomic) UILabel *upBigPriceLB;
@property (strong,nonatomic) UILabel *downLeftPriceLB;
@property (strong,nonatomic) UILabel *downRightPriceLB;

@property (strong,nonatomic) UIView *upBigView;
@property (strong,nonatomic) UIView *upOneView;
@property (strong,nonatomic) UIView *upTwoView;
@property (strong,nonatomic) UIView *upThreeView;
@property (strong,nonatomic) UIView *upFourView;
@property (strong,nonatomic) UIView *upFiveView;
@property (strong,nonatomic) UIView *upSixView;
@property (strong,nonatomic) UIView *downLeftView;
@property (strong,nonatomic) UIView *downRightView;


- (void)createUI;
- (void)setCellWithUpBigIV:(NSString *)bigString AndOneIV:(NSString *)oneString AndTwoIV:(NSString *)twoString AndThreeIV:(NSString *)threeString AndFourIV:(NSString *)fourString AndFiveIV:(NSString *)fiveString AndSixIV:(NSString *)sixString AndDownLeftIV:(NSString *)downLeftString AndDownRightIV:(NSString *)downRightString AndBigTitle:(NSString *)bigTitle AndOneTitle:(NSString *)oneTitle AndTwoTitle:(NSString *)twoTitle AndThreeTitle:(NSString *)threeTitle AndFoutTitle:(NSString *)fourTitle AndFiveTitle:(NSString *)fiveTitle AndSixTitle:(NSString *)sixTitle AndLeftTitle:(NSString *)leftTitle AndRightTitle:(NSString *)rightTitle AndBigPrice:(NSString *)bigPrice AndDownLeftPrice:(NSString *)downLeftPrice AndDownRightPrice:(NSString *)downRightPrice;

@end
