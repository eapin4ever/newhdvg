//
//  TurnBackSendViewController.h
//  changeViewController
//
//  Created by ZhangEapin on 15/5/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TranSelectViewController.h"

@interface TurnBackSendViewController : UIViewController

@property (copy,nonatomic) NSString *tranName;
@property (copy,nonatomic) NSString *tranValue;
@property (strong,nonatomic) UITextField *companyText;
@property (copy,nonatomic) NSString *backOrderId;

@end
