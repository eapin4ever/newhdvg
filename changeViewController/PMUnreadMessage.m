//
//  PMUnreadMessage.m
//  changeViewController
//
//  Created by pmit on 14/11/17.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMUnreadMessage.h"
#import "MasterViewController.h"
#import "PMNetworking.h"
#import "PMUserInfos.h"
#import "UnReadMessageCell.h"

@interface PMUnreadMessage()<UIGestureRecognizerDelegate,UINavigationBarDelegate>

@property (strong,nonatomic) NSArray *notiArray;
@property (strong,nonatomic) UIView *noDataView;

@end

@implementation PMUnreadMessage

static NSString *cellIdentifier = @"unreadCell";

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"消息通知";
        
        self.view.backgroundColor = RGBA(245, 245, 245, 1);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.timesArray  = [NSMutableArray array];
    
    
    
    // 初始化未读消息的tableView
    self.unReadTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64)];
    [self.unReadTableView setShowsVerticalScrollIndicator:NO];
    self.unReadTableView.delegate = self;
    self.unReadTableView.dataSource = self;
    //[self.unReadTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:@"timeCell"];
    [self.unReadTableView registerClass:[UnReadMessageCell class] forCellReuseIdentifier:cellIdentifier];
    [self.view addSubview:self.unReadTableView];
    
    self.unReadTableView.tableFooterView = [[UIView alloc] init];
    
    [self createNoDataView];
    [self getHistoryMessage];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notiArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *notiDic = self.notiArray[indexPath.row];
    NSString *contentzStr = notiDic[@"content"];
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    NSDictionary *sizeDic = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName, nil];
    CGSize sizeText = [contentzStr boundingRectWithSize:CGSizeMake(WIDTH - 20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:sizeDic context:nil].size;
    //NSLog(@"高度 === %lf", sizeText.height);
    return sizeText.height + 50;
}

// 设置 footer 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 1)];
    footerView.backgroundColor = RGBA(231, 231, 231, 1);
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UnReadMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UnReadMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
//    NSInteger row = [indexPath row];
//
    NSDictionary *notiDic = self.notiArray[indexPath.row];
//    cell.textLabel.text = notiDic[@"title"];
//    cell.detailTextLabel.text = notiDic[@"content"];
//    cell.detailTextLabel.numberOfLines = 0;
//    cell.detailTextLabel.textColor = [UIColor grayColor];
//    
//    [cell setUnreadMessagesTimeUI];
    
//    NSDictionary *dict = self.timesArray[indexPath.row];
//
    // 获取下来的时间数据
    NSString *timeType = self.timesArray[indexPath.row];
//    [cell setUnreadMessagesTime:timeType];
    NSString *messageTitle = [notiDic objectNullForKey:@"title"];
    NSString *messageContent = [notiDic objectNullForKey:@"content"];
    NSString *messageTime = timeType;
    cell.messageDic = @{@"title":messageTitle,@"content":messageContent,@"time":messageTime};
    [cell createUI];
    [cell getData];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - 添加未读消息假数据
- (void)initUnreadMessagesTimeData
{
    for (NSDictionary *notiDic in self.notiArray)
    {
        NSString *timeType = @"";
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"EEE MMM dd HH:mm:ss zzzz yyyy";
        fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[notiDic objectNullForKey:@"sendTime"] doubleValue]];
        NSDate *now = [NSDate date];
        
        NSTimeInterval delta = [now timeIntervalSinceDate:date];
        if (delta <= 60)
        {
            timeType = @"刚刚";
        }
        else if (delta <= 3600 && delta > 60)
        {
            timeType = [NSString stringWithFormat:@"%.0f分钟前",delta / 60];
        }
        else if (delta <= 24 * 60 * 60 && delta > 3600)
        {
            timeType = [NSString stringWithFormat:@"%.0f小时前",delta / 3600];
        }else
        {
            fmt.dateFormat = @"MM-dd HH:mm:ss";
            timeType = [fmt stringFromDate:date];
        }
        
        [self.timesArray addObject:timeType];
       
    }
}

#pragma mark - 获取历史通知
- (void)getHistoryMessage
{
    PMNetworking *netWorking = [PMNetworking defaultNetworking];
    [netWorking request:PMRequestStateGetPushMessageList WithParameters:@{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID} callBackBlock:^(NSDictionary *dic) {
        if ([[dic objectNullForKey:@"success"] boolValue])
        {
            self.notiArray = [dic objectNullForKey:@"data"];
            if (self.notiArray.count == 0) {
                self.noDataView.hidden = NO;
            }
            else
            {
                self.noDataView.hidden = YES;
                [self initUnreadMessagesTimeData];
                [self.unReadTableView reloadData];
            }
        }
        
    } showIndicator:YES];
}

#pragma mark - 没消息视图
- (void)createNoDataView
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    noDataView.backgroundColor = [UIColor whiteColor];
    self.noDataView = noDataView;
    
    UIImageView *noIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, noDataView.frame.size.height / 2 - HeightRate(300), WIDTH, HeightRate(400))];
    noIV.image = [UIImage imageNamed:@"noData"];
    noIV.contentMode = UIViewContentModeScaleAspectFit;
    [noDataView addSubview:noIV];
    
    UILabel *noLB = [[UILabel alloc] initWithFrame:CGRectMake(0, noDataView.frame.size.height / 2 + HeightRate(100) , WIDTH, HeightRate(100))];
    noLB.text = @"您还没收到任何通知";
    noLB.textColor = HDVGFontColor;
    noLB.font = [UIFont systemFontOfSize:17.0f];
    noLB.numberOfLines = 0;
    noLB.textAlignment = NSTextAlignmentCenter;
    [noDataView addSubview:noLB];
    
    [self.view addSubview:noDataView];
    
}


@end
