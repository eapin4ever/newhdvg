//
//  EditAddressViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/16.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "HZAreaPickerView.h"
#import "PMNetworking.h"
#import "PMUserInfos.h"
#import "PMGetAreaID.h"
#import "PMToastHint.h"

@class AddressManageTableViewController;

@interface EditAddressViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate, HZAreaPickerDelegate>
@property (nonatomic,strong)NSDictionary *addressDic;

@property (strong, nonatomic) UITextField *nameTextField;
@property (strong, nonatomic) UITextField *mobileTextField;
@property (strong, nonatomic) UITextField *phoneTextField;
@property (strong, nonatomic) UITextField *regionTextField;
@property (strong, nonatomic) UITextField *addDetailTextField;
@property (assign,nonatomic) BOOL isDefault;

@property (copy, nonatomic) NSString *areaValue;
@property (copy, nonatomic) NSString *areaID;
@property (strong, nonatomic) HZAreaPickerView *locatePicker;

@property (strong, nonatomic) UIView *shippingAddView;

@property (strong, nonatomic) PMNetworking *networking;
@property (weak,nonatomic) AddressManageTableViewController *addressManagerVC;

- (instancetype)initWithAddressDetail:(NSDictionary *)dic;

- (void)cancelEditLocatePicker;

@end
