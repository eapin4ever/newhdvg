//
//  WithdrawalDetailViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  提现详情控制器
 */
#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface WithdrawalDetailViewController : UIViewController

@property (strong, nonatomic) UIView *firstView;
@property (strong, nonatomic) UIView *secondView;

@property (strong, nonatomic) NSMutableDictionary *detailDict;

@end
