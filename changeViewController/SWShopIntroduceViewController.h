//
//  SWShopIntroduceViewController.h
//  changeViewController
//
//  Created by P&M on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "DistributionSettingViewController.h"

@protocol SWShopIntroduceViewControllerDelegate <NSObject>

- (void)passRemark:(NSString *)remark;

@end

@interface SWShopIntroduceViewController : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) UIView *introduceView;
@property (strong, nonatomic) UITextView *introduceTV;
@property (strong, nonatomic) UILabel *placeholderLB;
@property (strong, nonatomic) UILabel *tipLabel;

@property (strong, nonatomic) NSDictionary *shopDataDic;
@property (weak, nonatomic) DistributionSettingViewController *shopSettingVC;
@property (weak,nonatomic) id<SWShopIntroduceViewControllerDelegate> remarkDelegate;

@end
