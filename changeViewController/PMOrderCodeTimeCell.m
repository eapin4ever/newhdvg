//
//  PMOrderCodeTimeCell.m
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMOrderCodeTimeCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMOrderCodeTimeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.orderCodeLB)
    {
        NSString *orderTitleString = @"订单编号：";
        CGSize titleSize = [orderTitleString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0f]} context:nil].size;
        UILabel *orderCodeTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, titleSize.width, 20)];
        orderCodeTitleLB.textColor = [UIColor blackColor];
        orderCodeTitleLB.textAlignment = NSTextAlignmentLeft;
        orderCodeTitleLB.font = [UIFont systemFontOfSize:16.0f];
        orderCodeTitleLB.text = @"订单编号：";
        [self.contentView addSubview:orderCodeTitleLB];
        
        self.orderCodeLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(orderCodeTitleLB.frame) + 2, 10, WIDTH - CGRectGetMaxX(orderCodeTitleLB.frame) - 17, 20)];
        self.orderCodeLB.textColor = [UIColor blackColor];
        self.orderCodeLB.textAlignment = NSTextAlignmentLeft;
        self.orderCodeLB.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:self.orderCodeLB];
    
        
        UILabel *orderTimeTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 40, titleSize.width, 20)];
        orderTimeTitleLB.textColor = [UIColor blackColor];
        orderTimeTitleLB.textAlignment = NSTextAlignmentLeft;
        orderTimeTitleLB.font = [UIFont systemFontOfSize:16.0f];
        orderTimeTitleLB.text = @"下单时间：";
        [self.contentView addSubview:orderTimeTitleLB];
        
        self.orderTimeLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(orderTimeTitleLB.frame) + 2, 40, WIDTH - CGRectGetMaxX(orderTimeTitleLB.frame) - 17, 20)];
        self.orderTimeLB.textColor = [UIColor blackColor];
        self.orderTimeLB.textAlignment = NSTextAlignmentLeft;
        self.orderTimeLB.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:self.orderTimeLB];
        
        
    }
}

- (void)setcellData:(NSString *)orderCodeString Time:(NSString *)orderTimeString
{
    self.orderCodeLB.text = orderCodeString;
    self.orderTimeLB.text = orderTimeString;
}

@end
