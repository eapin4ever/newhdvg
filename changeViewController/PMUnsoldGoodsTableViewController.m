//
//  PMUnsoldGoodsTableViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMUnsoldGoodsTableViewController.h"
#import "PMCurrentGoodsTableViewController.h"
#import "PMDistributionsGoodsViewController.h"
#import "PMMyGoodsViewController.h"
#import "PMToastHint.h"
#import "PMDistributionGoodsCell.h"

@interface PMUnsoldGoodsTableViewController ()<UITextFieldDelegate>
@property(nonatomic,strong)UITextField *searchTF;

@property(nonatomic,assign)NSInteger totalPages;
@property(nonatomic,assign)NSInteger currentPage;
@property (strong,nonatomic) NSMutableArray *hasRefreshArr;
@property (strong,nonatomic) NSMutableArray *selectedArr;

@end

static PMUnsoldGoodsTableViewController *_currentVC;
@implementation PMUnsoldGoodsTableViewController
+ (PMUnsoldGoodsTableViewController *)currentVC
{
    return _currentVC;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"上架选中商品";
        self.view.frame = CGRectMake(0, 40, WIDTH, HEIGHT - 40 - 50 - 64);
        _currentVC = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(seeSearchView:) name:@"mySearchView" object:nil];
//    [self.tableView registerClass:[PMDistributionsGoodsTableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.hasRefreshArr = [NSMutableArray array];
    self.selectedArr = [NSMutableArray array];
    [self.tableView registerClass:[PMDistributionGoodsCell class] forCellReuseIdentifier:@"Cell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = RGBA(231, 231, 231, 1);
    [self addRefreshAction];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ((PMDistributionsGoodsViewController *)self.parentViewController).noResultView.hidden = YES;
    
    self.isSelectAll = NO;
    
    [self getModelsBySearchString:nil finish:nil error:nil];
    [self.tableView reloadData];
    [self refreshAllIndexPathArray];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PMDistributionGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [cell createUI];
    
    NSDictionary *dic = self.listArray[indexPath.section];
    CGFloat inputPrice = [[dic objectNullForKey:@"input"] doubleValue];
    
    [cell.iv sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    NSString *upStatusString = [dic objectNullForKey:@"productStatus"];
    if ([upStatusString isEqualToString:@"UP"])
    {
        cell.isDown = NO;
    }
    else
    {
        cell.isDown = YES;
    }
    
    [cell setContentTitle:[dic objectNullForKey:@"productName"] unitPrice:[dic objectNullForKey:@"productPrice"] percent:inputPrice];
    
    cell.shareBtn.hidden = YES;
    cell.deleteBtn.hidden = NO;
    cell.goDetailBtn.tag = indexPath.section;
    cell.deleteBtn.tag = indexPath.section;
    cell.seletedBtn.tag = indexPath.section * 100000 + 10000;
    [cell.goDetailBtn addTarget:self action:@selector(showProductDetail:) forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.seletedBtn addTarget:self action:@selector(seletedUnsold:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([self.selectedArr containsObject:dic])
    {
        cell.seletedBtn.selected = YES;
    }
    else
    {
        cell.seletedBtn.selected = NO;
    }
    
    if (indexPath.section == self.listArray.count - 17)
    {
        if (![self.hasRefreshArr containsObject:@(indexPath.section)])
        {
            [self.hasRefreshArr addObject:@(indexPath.section)];
            
            if(self.currentPage < self.totalPages)//如果小于总页数则刷新
            {
                self.isLoad = YES;
                //获取数据
                [self getModelsBySearchString:self.searchTF.text finish:^{
                    
                    [self.tableView reloadData];
                    
                } error:^{
                    
                }];
            }
            
        }
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedIndexPaths addObject:indexPath];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedIndexPaths removeObject:indexPath];
    self.isSelectAll = NO;
    [((PMDistributionsGoodsViewController *)self.parentViewController).selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
    
}

#pragma mark - 点击事件
#pragma mark 点击跳转到商品详情
- (void)showProductDetail:(UIButton *)sender
{
    NSDictionary *dic = self.listArray[sender.tag];
    PMMyGoodsViewController *vc = [[PMMyGoodsViewController alloc] init];
//    vc.shopId = [dic objectNullForKey:@"shopId"];
    vc.productId = [dic objectNullForKey:@"productId"];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];
}

#pragma mark - 表头和表尾
- (void)initTableHeaderView
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    headerView.backgroundColor = RGBA(231, 231, 231, 1);
    
    UILabel *searchLB1 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(100), 10, WidthRate(120), 10)];
    searchLB1.text = @"搜索商品";
    searchLB1.font = [UIFont systemFontOfSize:9.0f];
    searchLB1.textAlignment = NSTextAlignmentCenter;
    searchLB1.textColor = RGBA(83, 83, 83, 1);
    [headerView addSubview:searchLB1];
    
    UILabel *searchLB2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(100), 20, WidthRate(120), 10)];
    searchLB2.text = @"SEARCH";
    searchLB2.font = [UIFont systemFontOfSize:9.0f];
    searchLB2.textAlignment = NSTextAlignmentCenter;
    searchLB2.textColor = RGBA(135, 135, 135, 1);
    [headerView addSubview:searchLB2];
    
    
    //输入框
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH/2 - WidthRate(160), 8, WidthRate(320), 24)];
    self.searchTF = tf;
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:12.0f];
    tf.placeholder = @" 请输入商品名称";
    
    [tf setValue:[UIFont boldSystemFontOfSize:12.0f] forKeyPath:@"_placeholderLabel.font"];
    [tf setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, tf.bounds.size.width, tf.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    
    tf.returnKeyType = UIReturnKeySearch;
    //使用leftView让输入的文字缩进
    tf.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 1)];
    tf.leftViewMode = UITextFieldViewModeAlways;
    tf.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:tf];
    
    
    UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(tf.frame.origin.x + tf.bounds.size.width, 8, WidthRate(80), 24)];
    searchBtn.backgroundColor = RGBA(218, 67, 80, 1);
    [headerView addSubview:searchBtn];
    
    //放大镜
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(40) - 7, 5, 14, 14)];
    iv.image = [UIImage imageNamed:@"search_white"];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    
    [searchBtn addSubview:iv];
    
    
    self.tableView.tableHeaderView = headerView;
    self.tableView.tableFooterView = [[UIView alloc] init];
}


#pragma mark - 全选  上架
- (void)selectAllRows:(UIButton *)sender
{
    //视图上的全选与全不选效果
    if(self.isSelectAll)
    {
        self.isSelectAll = NO;
        [sender setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
        self.selectedArr = [NSMutableArray array];
        for (NSInteger i = 0; i < self.listArray.count; i++)
        {
            PMDistributionGoodsCell *cell = (PMDistributionGoodsCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
            cell.seletedBtn.selected = NO;
        }
        [self.tableView reloadData];
    }
    else
    {
        self.isSelectAll = YES;
        [sender setImage:[UIImage imageNamed:@"selectAll_1"] forState:UIControlStateNormal];
        for (NSInteger i = 0; i < self.listArray.count; i++)
        {
            NSDictionary *carProductDic = self.listArray[i];
            if ([[carProductDic objectNullForKey:@"productStatus"] isEqualToString:@"UP"])
            {
                if ([self.selectedArr containsObject:carProductDic])
                {
                    continue;
                }
                else
                {
                    [self.selectedArr addObject:carProductDic];
                }
                
                PMDistributionGoodsCell *cell = (PMDistributionGoodsCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
                cell.seletedBtn.selected = YES;
            }
        }
        [self.tableView reloadData];
    }
}


- (void)unsoldSelectedGoods
{
    PMCurrentGoodsTableViewController *vc = [PMCurrentGoodsTableViewController currentVC];
    
//    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
//    for (NSIndexPath *indexPath in self.selectedIndexPaths)
//    {
//        [indexSet addIndex:indexPath.section];
//    }
    
    //判断是否有选中商品
    if(self.selectedArr.count == 0)
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请选择要上架商品"];
        return;
    }
    
    //想服务器发送请求，请求成功后下架
    [self unsoldSelectedGoods:self.selectedArr FinishAction:^{
        [vc.listArray addObjectsFromArray:self.selectedArr];
    
        [self.listArray removeObjectsInArray:self.selectedArr];
        
        //删除对应的indexPath
//        [self refreshAllIndexPathArray];

        self.selectedArr = [NSMutableArray array];
        
        [self.tableView reloadData];
        
        if(self.isSelectAll)
        {
            //重新请求数据
            self.isLoad = NO;
            [self getModelsBySearchString:self.searchTF.text finish:nil error:nil];
            //取消全选
            [((PMDistributionsGoodsViewController *)self.parentViewController).selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
            self.isSelectAll = NO;
        }
        
    }];
}

#pragma mark - 搜索
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self getModelsBySearchString:textField.text finish:nil error:nil];
    return YES;
}

#pragma mark 收键盘
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchTF resignFirstResponder];
    if (scrollView == self.tableView)
    {
        CGFloat sectionHeaderHeight = 5;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}


#pragma mark  刷新选择的区行
- (void)refreshAllIndexPathArray
{
    [self.allIndexPaths removeAllObjects];
    
    for(int i = 0;i < self.listArray.count;i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:i];
        [self.allIndexPaths addObject:indexPath];
    }
}

#pragma mark - 添加刷新机制
- (void)addRefreshAction
{
    __weak PMUnsoldGoodsTableViewController *weakSelf = self;
    //使用普通的下拉刷新
    [self.tableView addRefreshHeaderViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        weakSelf.isLoad = NO;
        //重新请求
        [weakSelf getModelsBySearchString:weakSelf.searchTF.text finish:^{
            weakSelf.hasRefreshArr = [NSMutableArray array];
            [weakSelf.tableView reloadData];
            //请求成功,显示刷新成功
            [weakSelf.tableView headerEndRefreshingWithResult:JHRefreshResultSuccess];
        } error:^{
            //请求失败,显示刷新失败
            [weakSelf.tableView headerEndRefreshingWithResult:JHRefreshResultFailure];
        }];
        
    }];
    
    
    //使用普通的上拉加载

    
}


#pragma mark - 获取代言中已下架的商品的数据
- (void)getModelsBySearchString:(NSString *)searchString finish:(myFinishBlock)finish error:(myFinishBlock)error
{
    
    //显示代言中的商品/下架的商品
    /*
     PM_SID         Y	String		Token
     currentPage	Y	Int         当前页
     productName	N	String		商品名称，用于搜索
     flag           Y	String		1: 代言中的商品  -1: 下架的商品
     */
    
    if (self.isSearch)
    {
        self.hasRefreshArr = [NSMutableArray array];
        self.isSearch = NO;
    }
    
    if(self.isLoad)
    {
        self.currentPage++;
    }
    else
    {
        self.currentPage = 1;
    }
    
    NSDictionary *param;
    if(searchString == nil || [searchString isEqual:@""])
    {
        param = @{PMSID,@"currentPage":@(self.currentPage),@"flag":@"-1",@"pageSize":@(20)};
    }
    else
    {
        param = @{PMSID,@"currentPage":@(self.currentPage),@"flag":@"-1",@"productName":searchString,@"pageSize":@(20)};
    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateShareShowSellsProducts WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            if(self.isLoad == NO)
            {
                self.listArray = [NSMutableArray array];
                self.listArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
            }
            else
            {
                [self.listArray addObjectsFromArray: [[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"]];
            }
            //获得总页数
            self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
            [self.tableView reloadData];
            //初始化并刷新所有行的indexPath
//            self.selectedIndexPaths = [NSMutableArray array];
//            self.allIndexPaths = [NSMutableArray array];
//            [self refreshAllIndexPathArray];
            
            if(finish != nil)
                finish();
            [((PMDistributionsGoodsViewController *)self.parentViewController) showNoResultViewWithtitle:nil AndIsSold:4];
            if (self.listArray.count == 0)
            {
                [((PMDistributionsGoodsViewController *)self.parentViewController) showNoResultViewWithtitle:@"没有下架的商品哦" AndIsSold:2];
            }
        }
        else
        {
            if(error)
                error();
            [((PMDistributionsGoodsViewController *)self.parentViewController) showNoResultViewWithtitle:@"没有下架的商品哦" AndIsSold:2];
        }
    }showIndicator:!self.isLoad];
}


#pragma mark - 将选中的商品上架
- (void)unsoldSelectedGoods:(NSArray *)productArr FinishAction:(finishAction)finish
{
    //上架、下架
    /*
     PM_SID         Y	String		Token
     status         Y	String		标志  1：上架   -1：下架
     productIds     N	String		多个商品Id的拼接，如04652eb5e5fa4d1a9153730d7238857f,04855d5313af43df80d4766707def575
     */
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dic in productArr)
    {
        [arr addObject:[dic objectNullForKey:@"productId"]];
    }
    NSString *productIds = [arr componentsJoinedByString:@","];
    
    
    NSDictionary *param = @{PMSID,@"status":@"1",@"productIds":productIds};
    [[PMNetworking defaultNetworking] request:PMRequestStateShareUpdateBatchDown WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            if(finish)
                finish();
        }
        else
        {
            showRequestFailAlertView;
        }
    }showIndicator:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = HDVGPageBGGray;
    return headerView;
}

- (void)seeSearchView:(NSNotification *)notification
{
    BOOL isViewShow = [[notification object] boolValue];
    if (isViewShow)
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.view.frame = CGRectMake(0, 80, WIDTH, HEIGHT - 64 - 80 - 50);
        }];
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.view.frame = CGRectMake(0, 40, WIDTH, HEIGHT - 64 - 40 - 50);
        }];
    }
}

- (void)clickAction:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    NSInteger index = sender.tag;
    if ([[self.listArray objectAtIndex:index] isKindOfClass:[NSDictionary class]])
    {
        NSString *homeProId = [[self.listArray objectAtIndex:index] objectNullForKey:@"homeProId"];
        [[PMNetworking defaultNetworking] request:PMRequestStateDeleteHomeProduct WithParameters:@{@"homeProId":homeProId} callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view ByToast:@"删除成功"];
                [self.listArray removeObjectAtIndex:sender.tag];
                NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:sender.tag];
                [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView reloadData];
                self.view.userInteractionEnabled = YES;
                if (self.listArray.count == 0)
                {
                    [((PMDistributionsGoodsViewController *)self.parentViewController) showNoResultViewWithtitle:@"没有下架的商品哦" AndIsSold:2];
                }
            }
            else
            {
                self.view.userInteractionEnabled = YES;
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"删除失败了!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        } showIndicator:NO];
        
    }
    
}

- (void)seletedUnsold:(UIButton *)sender
{
    NSInteger index = (sender.tag - 10000) / 100000;
    NSDictionary *dic = [self.listArray objectAtIndex:index];
    sender.selected = !sender.selected;
    if (sender.selected)
    {
        if (![self.selectedArr containsObject:dic])
        {
            [self.selectedArr addObject:dic];
        }
    }
    else
    {
        [self.selectedArr removeObject:dic];
    }
    
    if (self.selectedArr.count == self.listArray.count)
    {
        [((PMDistributionsGoodsViewController *)self.parentViewController).selectAllBtn setImage:[UIImage imageNamed:@"selectAll_1"] forState:UIControlStateNormal];
        self.isSelectAll = YES;
    }
    else
    {
        [((PMDistributionsGoodsViewController *)self.parentViewController).selectAllBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
        self.isSelectAll = NO;
    }
}


@end
