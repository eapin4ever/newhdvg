//
//  PMApplication.m
//  changeViewController
//
//  Created by pmit on 15/7/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMApplication.h"
#import "PMUserInfos.h"

@implementation PMApplication

- (void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    
    if(!_idleTimer) {
        [self resetIdleTimer];
    }
    
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan) {
            [self resetIdleTimer];
        }
    }
}

- (void)resetIdleTimer
{
    if (_idleTimer) {
        [_idleTimer invalidate];
    }
    
    // Schedule a timer to fire in kApplicationTimeoutInMinutes * 60
    double timeout = kApplicationTimeoutInMinutes * 60;
    _idleTimer = [NSTimer scheduledTimerWithTimeInterval:timeout
                                                   target:self
                                                 selector:@selector(idleTimerExceeded)
                                                 userInfo:nil 
                                                  repeats:NO];
    
}

- (void)idleTimerExceeded {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kApplicationDidTimeoutNotification object:nil];
}

@end
