//
//  ProductScrollCell.h
//  changeViewController
//
//  Created by pmit on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductScrollCellDelegate <NSObject>

- (void)goToHomeDetail:(NSString *)productId;

@end

@interface ProductScrollCell : UITableViewCell

@property (strong,nonatomic) UIScrollView *productScroll;
@property (strong,nonatomic) NSArray *contentArr;
@property (weak,nonatomic) id<ProductScrollCellDelegate> cellDelegate;

- (void)createUI;
- (void)setScrollViewContent:(NSArray *)contentArr;

@end

