//
//  MobileAuthcodeViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "MobileAuthcodeViewController.h"
#import "BindingMobileViewController.h"
#import "PMToastHint.h"

@interface MobileAuthcodeViewController ()
{
    NSInteger timerCount;
}

@property (strong, nonatomic) NSTimer *timer;  // 定时器
@property (strong,nonatomic) UILabel *resendCodeLabel;

@end

@implementation MobileAuthcodeViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"验证手机号";
        
        self.view.backgroundColor = HDVGPageBGGray;  // 背景色
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self createChangeMobileAuthcodeViewUI]; 
    [self createConfirmationButtonUI];
}

// 创建修改绑定手机号 view
- (void)createChangeMobileAuthcodeViewUI
{
    // 提示用户输入用户手机号码
    self.authcodeView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 88)];
    self.authcodeView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.authcodeView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.authcodeView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 43, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.authcodeView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(0, 88 - 0.5, WIDTH, 0.5);
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.authcodeView.layer addSublayer:layer3];
    
    /**
     *  从服务器获取用户数据
     */
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    NSDictionary *dict = userInfos.userDataDic;
    
    // 手机号码
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(220), 30)];
    mobileLabel.backgroundColor = [UIColor clearColor];
    mobileLabel.text = @"手 机 号 码：";
    mobileLabel.textColor = HDVGFontColor;
    mobileLabel.textAlignment = NSTextAlignmentLeft;
    mobileLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.authcodeView addSubview:mobileLabel];
    
    self.mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(270), 7, WIDTH / 3, 30)];
    self.mobileLabel.backgroundColor = [UIColor clearColor];
    self.mobileLabel.text = [[dict objectNullForKey:@"clientUser"] objectNullForKey:@"mobile"];
    self.mobileLabel.textColor = RGBA(129, 129, 129, 1);
    self.mobileLabel.textAlignment = NSTextAlignmentLeft;
    self.mobileLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.authcodeView addSubview:self.mobileLabel];
    
    // 获取验证码按钮
    CGFloat X = WIDTH - (WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30));
    CGFloat widtd = WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30) - WidthRate(46);
    UIButton *authcodeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    authcodeButton.frame = CGRectMake(X, 51, widtd, 30);
    [authcodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [authcodeButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [authcodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [authcodeButton addTarget:self action:@selector(getAuthcodeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [authcodeButton setBackgroundColor:ButtonBgColor];
    [authcodeButton.layer setCornerRadius:4.0];
    [self.authcodeView addSubview:authcodeButton];
    
    self.resendCodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(X, 51, widtd, 30)];
    self.resendCodeLabel.textAlignment = NSTextAlignmentCenter;
    self.resendCodeLabel.hidden = YES;
    self.resendCodeLabel.text = @"重发(60)";
    self.resendCodeLabel.textColor = [UIColor darkGrayColor];
    self.resendCodeLabel.font = [UIFont systemFontOfSize:12.0f];
    self.resendCodeLabel.textAlignment = NSTextAlignmentCenter;
    [self.authcodeView addSubview:self.resendCodeLabel];
    
    
    // 短信验证码
    UILabel *authcodeLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 51, WidthRate(220), 30)];
    authcodeLab.backgroundColor = [UIColor clearColor];
    authcodeLab.text = @"短信验证码：";
    authcodeLab.textColor = HDVGFontColor;
    authcodeLab.textAlignment = NSTextAlignmentLeft;
    authcodeLab.font = [UIFont systemFontOfSize:15.0f];
    [self.authcodeView addSubview:authcodeLab];
    
    self.mobileAuthcodeTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(270), 51, WidthRate(220), 30)];
    self.mobileAuthcodeTF.borderStyle = UITextBorderStyleNone;
    self.mobileAuthcodeTF.delegate = self;
    self.mobileAuthcodeTF.tag = 1;
    self.mobileAuthcodeTF.textAlignment = NSTextAlignmentLeft;
    self.mobileAuthcodeTF.font = [UIFont systemFontOfSize:15.0f];
    self.mobileAuthcodeTF.placeholder = @"请输入验证码";
    self.mobileAuthcodeTF.returnKeyType = UIReturnKeyDone;
    self.mobileAuthcodeTF.keyboardType = UIKeyboardTypeNumberPad;
    [self.authcodeView addSubview:self.mobileAuthcodeTF];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.mobileAuthcodeTF resignFirstResponder];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.mobileAuthcodeTF.tag == 1 && textField == self.mobileAuthcodeTF && textField.text.length - range.length + string.length > 6) {
        return NO;
    }
    
    return YES;
}

// 创建确认验证按钮 UI
- (void)createConfirmationButtonUI
{
    // 完成设置按钮
    UIButton *confirmationButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    confirmationButton.frame = CGRectMake(WidthRate(46), self.authcodeView.frame.origin.y + 88 + HeightRate(120), WIDTH - WidthRate(46) * 2, 44);
    [confirmationButton setTitle:@"确认验证" forState:UIControlStateNormal];
    [confirmationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [confirmationButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [confirmationButton addTarget:self action:@selector(confirmationButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [confirmationButton setBackgroundColor:ButtonBgColor];
    [confirmationButton.layer setCornerRadius:6.0];
    [self.view addSubview:confirmationButton];
}

#pragma mark - 获取验证码按钮响应事件
- (void)getAuthcodeButtonClick:(UIButton *)sender
{
    /**
     * 用户输入手机号码获取短信验证码
     * 参数: sendSMS:(string)验证码
     */
    NSDictionary *phoneSendSMS = @{@"mobile":self.mobileLabel.text};
    self.networking = [PMNetworking defaultNetworking];
    
    
    [self.networking request:PMRequestStateSendSMS WithParameters:phoneSendSMS callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSInteger messageCode = [[dic objectNullForKey:@"messageCode"] integerValue];
            if (messageCode != 1)
            {
                [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该号超出每天获取验证码条数"];
            }
            else
            {
                // 按钮变灰，倒数，“重发”
                if(!self.timer.isValid)//正在倒数
                {
                    sender.backgroundColor = [UIColor lightGrayColor];
                    sender.userInteractionEnabled = NO;
                    timerCount = 60;
                    [sender setTitle:@"重发(60)" forState:UIControlStateNormal];
                    [sender setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(resendAuthcodeClick:) userInfo:sender repeats:YES];
                }
            }
        }
        
    }showIndicator:NO];
    
}

#pragma mark 获取验证码按钮倒数
- (void)resendAuthcodeClick:(NSTimer *)timer
{
    UIButton *btn = timer.userInfo;
    
    if(timerCount == 0)
    {
        //停止运行timer
        [timer invalidate];
        //按钮恢复成红色
        
        [btn setBackgroundColor:RGBA(218, 67, 80, 1)];
        self.resendCodeLabel.hidden = YES;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:@"重发" forState:UIControlStateNormal];
        btn.userInteractionEnabled = YES;
    }
    else
    {
        timerCount --;
        self.resendCodeLabel.hidden = NO;
        self.resendCodeLabel.text = [NSString stringWithFormat:@"重发(%ld)",(long)timerCount];
        [btn setTitle:@"" forState:UIControlStateNormal];
    }
}

#pragma mark 确认验证按钮响应事件
- (void)confirmationButtonClick:(id)sender
{
    if ([self.mobileAuthcodeTF.text isEqual:@""]) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"验证码不能为空"];
    }
    // 从服务器上获得的PM_SID
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    NSDictionary *dict = userInfos.userDataDic;
    
    NSString *PM_SID = [[dict objectNullForKey:@"token"] substringFromIndex:7];
    
    if (![self.mobileAuthcodeTF.text isEqual:@""]) {
        
        /**
         *  用户修改手机号码
         *  参数：updateMobile:(string)
         */
//        NSDictionary *mobile = @{@"PM_SID":PM_SID, @"mobile":self.mobileLabel.text, @"validateCode":self.mobileAuthcodeTF.text};
//        self.networking = [PMNetworking defaultNetworking];
//        
//        [self.networking request:PMRequestStateSafeUpdateMobile WithParameters:mobile callBackBlock:^(NSDictionary *dict) {
//            
//            if ([[dict objectNullForKey:@"success"] boolValue] == 1) {
//                
//                if ([self.mobileOrBankAuthcode isEqualToString:@"changeMobileAuthcode"]) {
//                    
//                    BindingMobileViewController *newMobileVC = [[BindingMobileViewController alloc] init];
//                    [self.navigationController pushViewController:newMobileVC animated:YES];
//                }
//                if ([self.mobileOrBankAuthcode isEqualToString:@"unbundlingBankCard"]) {
//                    
//                    
//                }
//            }
//            else {
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//                [alertView show];
//            }
//        }showIndicator:NO];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"你确定解绑银行卡！" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alertView.tag = 1004;
        [alertView show];

        
       
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1 && alertView.tag == 1004) {
        
        PMNetworking *networking = [PMNetworking defaultNetworking];
        
        NSDictionary *para = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID,@"bank":@"",@"bankCard":@"",@"bankDetail":@"",@"validateCode":self.mobileAuthcodeTF.text};
        
        [networking request:PMRequestStateCancelBank WithParameters:para callBackBlock:^(NSDictionary *dict) {
            //已绑定
            if ([[dict objectNullForKey:@"success"] integerValue] == 1)
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"解绑银行卡号成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 1005;
                [alertView show];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
        }showIndicator:NO];
    }
    
    if (alertView.tag == 1005) {
        //[self.navigationController popToRootViewControllerAnimated:YES];
        
        NSArray *viewControllers = [self.navigationController viewControllers];
        UIViewController *controller = [viewControllers objectAtIndex:1];
        [self.navigationController popToViewController:controller animated:YES];
        
        //[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 2] animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
