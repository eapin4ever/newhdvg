    //
//  main.m
//  changeViewController
//
//  Created by wallace on 14/11/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "PMApplication.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
