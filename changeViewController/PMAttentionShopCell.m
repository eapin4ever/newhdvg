//
//  PMAttentionShopCell.m
//  changeViewController
//
//  Created by pmit on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMAttentionShopCell.h"
#define grayColor RGBA(123, 123, 123, 1)
#define itemX WidthRate(250)
#define itemY HeightRate(50)
#define itemWidth WidthRate(280)
#define itemHeight HeightRate(50)

@implementation PMAttentionShopCell
{
    UILabel *shopNameLB;
    UILabel *accessoryLB;
}

- (void)createUI
{
    if(!_iv)
    {
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(30), HeightRate(180), HeightRate(120))];
//        [iv.layer setCornerRadius:6.0];
//        [iv.layer setBorderColor: RGBA(223, 223, 223, 1).CGColor];
//        [iv.layer setMasksToBounds:YES];
//        [iv.layer setBorderWidth:0.5];
        _iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv];
        
        shopNameLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, HeightRate(60), itemWidth, itemHeight)];
        shopNameLB.textColor = grayColor;
        shopNameLB.numberOfLines = 1;
        shopNameLB.font = [UIFont boldSystemFontOfSize:16.0f];
        [self.contentView addSubview:shopNameLB];
        
//        //这是作为辅助视图显示的版本
//        self.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
//        
//        accessoryLB = [[UILabel alloc] initWithFrame:CGRectMake(0,(self.accessoryView.bounds.size.height - 20)/2, self.accessoryView.bounds.size.width, 20)];
//        accessoryLB.textColor = grayColor;
//        accessoryLB.numberOfLines = 1;
//        accessoryLB.textAlignment = NSTextAlignmentRight;
//        accessoryLB.font = [UIFont systemFontOfSize:10];
//        [self.accessoryView addSubview:accessoryLB];
        
        //这是普通版本
        accessoryLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(560), HeightRate(60), WidthRate(120), HeightRate(50))];
        accessoryLB.textColor = grayColor;
        accessoryLB.numberOfLines = 1;
        accessoryLB.textAlignment = NSTextAlignmentRight;
        accessoryLB.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:accessoryLB];
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //使选择时背景色透明
        self.selectedBackgroundView = [[UIView alloc] init];
    }
}

- (void)setContentShopName:(NSString *)shopName accessoryTitle:(NSString *)title
{
    shopNameLB.text = shopName;
    accessoryLB.text = title;
}


//更改删除按钮的颜色
- (void)layoutSubviews
{
    [super layoutSubviews];
    for (UIView *subView in self.subviews) {
        if ([NSStringFromClass([subView class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"]) {
            ((UIView *)[subView.subviews firstObject]).backgroundColor = HDVGRed;
        }
    }
}


@end
