//
//  tradeButton.m
//  changeViewController
//
//  Created by ZhangEapin on 15/4/23.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "tradeButton.h"

@implementation tradeButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height * 0.7)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:imageView];
        self.tradeIV = imageView;
        
        UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height * 0.6, frame.size.width, frame.size.height * 0.2)];
        lable.textAlignment = NSTextAlignmentCenter;
        lable.font = [UIFont systemFontOfSize:13.0f];
        [self addSubview:lable];
        self.titleLB = lable;
    }
    
    return self;
}

- (void)setImage:(UIImage *)image AndTitle:(NSString *)title
{
    self.tradeIV.image = image;
    self.titleLB.text = title;
}

@end
