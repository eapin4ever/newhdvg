//
//  PMUnreadMessage.h
//  changeViewController
//
//  Created by pmit on 14/11/17.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "MyTradeTableViewCell.h"

@interface PMUnreadMessage : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *titleArray;
@property (strong, nonatomic) NSMutableArray *detailArray;
@property (strong, nonatomic) NSMutableArray *timesArray;

@property (strong, nonatomic) UITableView *unReadTableView;

@end
