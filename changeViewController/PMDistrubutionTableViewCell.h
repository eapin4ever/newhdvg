//
//  PMDistrubutionTableViewCell.h
//  changeViewController
//
//  Created by P&M on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDistrubutionTableViewCell : UITableViewCell

@property (strong, nonatomic) UIButton *checkLogisticsBtn;

- (void)setDistributionOrderDataUI;
- (void)setDistributionProfitDetailDataUI;
- (void)setWithdrawalDetailDataUI;

// 设置代言订单管理的成交时间、状态、订单金额、佣金、买家、订单号
- (void)setTime:(NSString *)time setState:(NSString *)state setOrderMoney:(NSString *)orderMoney setBrokerage:(NSString *)brokerage setBuyer:(NSString *)buyer setOrderNumber:(NSString *)orderNumber;

// 设置代言获利详情的时间、订单金额、佣金金额数据
- (void)setTime:(NSString *)time setOrderMoney:(double)orderMoney setMoney:(double)money;

// 设置提现明细的时间、提款金额、状态数据
- (void)setTime:(NSString *)time setWithdrawalMoney:(double)withdrawalMoney setState:(NSString *)state;

@end
