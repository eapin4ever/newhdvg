//
//  PMShoppingCarReuseCell.m
//  changeViewController
//
//  Created by pmit on 14/11/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMShoppingCarReuseCell.h"

@implementation PMShoppingCarReuseCell
{
    UILabel *classifyLB1;
    UILabel *classifyLB2;
    UILabel *distanceLB;
    UILabel *locationLB;
    UITextField *numberTF;
    UILabel *priceLB;
}

- (void)createUI
{
    if(!_iv)
    {
        //图片
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(0), HeightRate(30), HeightRate(180), HeightRate(180))];
        _iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv];
        
        //商品标题或介绍
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(210), HeightRate(34), WidthRate(410), HeightRate(68))];
        self.titleLB.font = [UIFont systemFontOfSize:HeightRate(28)];
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        self.titleLB.textColor = HDVGFontColor;
        self.titleLB.numberOfLines = 2;
        [self.contentView addSubview:self.titleLB];
        
        //类目
        classifyLB1 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(210), HeightRate(110), WidthRate(270), HeightRate(50))];
        classifyLB1.font = [UIFont systemFontOfSize: HeightRate(24)];
        classifyLB1.textColor = [UIColor grayColor];
        [self.contentView addSubview:classifyLB1];
        
//        classifyLB2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(190), HeightRate(144), titleLB.bounds.size.width, HeightRate(25))];
//        classifyLB2.font = [UIFont systemFontOfSize: HeightRate(24)];
//        classifyLB2.textColor = [UIColor grayColor];
//        [self.contentView addSubview:classifyLB2];
        
        //距离
        distanceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(250), HeightRate(180), WidthRate(160), HeightRate(30))];
        distanceLB.font = [UIFont systemFontOfSize:HeightRate(24)];
        distanceLB.textAlignment = NSTextAlignmentLeft;
        distanceLB.textColor = [UIColor grayColor];
        [self.contentView addSubview:distanceLB];
        
        //距离图标
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(210), HeightRate(174), HeightRate(36), HeightRate(36))];
        icon.image = [UIImage imageNamed:@"location"];
        [self.contentView addSubview:icon];
        
        //隐藏
        icon.hidden = YES;
        distanceLB.hidden = YES;
        
        //添加大的背景按钮，用于点击时跳转到商品详情
//        UIButton *detailBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, HeightRate(240))];
//        self.goDetailBtn = detailBtn;
//        [self.contentView addSubview:detailBtn];
        
        
        //加减按钮 和中间显示框
        self.minusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.minusBtn.frame = CGRectMake(WidthRate(410), HeightRate(160), WidthRate(65), HeightRate(65));
        [self.minusBtn setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
        [self.minusBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.minusBtn.layer setBorderWidth:0.5];
        [self.minusBtn.layer setCornerRadius:5.0];
        [self.contentView addSubview:self.minusBtn];
        
        numberTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(470), HeightRate(160), WidthRate(80), HeightRate(55))];
        numberTF.userInteractionEnabled = NO;
        numberTF.textAlignment = NSTextAlignmentCenter;
        numberTF.font = [UIFont systemFontOfSize:HeightRate(30)];
        numberTF.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:numberTF];
        
        self.plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.plusBtn.frame = CGRectMake(WidthRate(550), HeightRate(160), WidthRate(65), HeightRate(65));
        [self.plusBtn setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
        [self.plusBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.plusBtn.layer setBorderWidth:0.5];
        [self.plusBtn.layer setCornerRadius:5.0];
        [self.contentView addSubview:self.plusBtn];
        
        //价格
        priceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(210), HeightRate(175), WidthRate(164), HeightRate(36))];
        priceLB.textColor = RGBA(189, 24, 41, 1);
        priceLB.textAlignment = NSTextAlignmentLeft;
        priceLB.font = [UIFont systemFontOfSize:HeightRate(32)];
        [self.contentView addSubview:priceLB];
        
        self.selectedBackgroundView = [[UIView alloc] init];
    }
}

- (void)setContentTitle:(NSString *)title classify1:(NSString *)classify1 classify2:(NSString *)classify2 distanceOrLocation:(NSString *)distance number:(NSString *)num price:(double)price
{
    self.titleLB.text = title;
    if(isNull(classify2) && !isNull(classify1))
        classifyLB1.text = [NSString stringWithFormat:@"%@",classify1];
    else if(!isNull(classify1) && !isNull(classify2))
        classifyLB1.text = [NSString stringWithFormat:@"%@,%@",classify1,classify2];

    distanceLB.text = distance;
    numberTF.text = num;
    priceLB.text = [NSString stringWithFormat:@"￥%.2lf",price];
}



- (void)numberPlus
{
    NSInteger temp = [numberTF.text integerValue];
    if (temp < self.maxCount)
    {
        temp ++;
    }
    else
    {
        temp = self.maxCount;
    }
    numberTF.text = [NSString stringWithFormat:@"%ld",(long)temp];
}

- (void)numberMinus
{
    NSInteger temp = [numberTF.text integerValue];
    if(temp > 1)
    {
        temp --;
        numberTF.text = [NSString stringWithFormat:@"%ld",(long)temp];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
