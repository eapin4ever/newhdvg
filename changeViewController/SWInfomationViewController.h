//
//  SWInfomationViewController.h
//  changeViewController
//
//  Created by P&M on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

// 个人信息控制器
#import <UIKit/UIKit.h>
#import "PMNetworking.h"
#import "HZAreaPickerView.h"
#import "PMGetAreaID.h"

@class SWContactAddViewController;

@interface SWInfomationViewController : UIViewController <UITextFieldDelegate, UIActionSheetDelegate, HZAreaPickerDelegate>

@property (strong, nonatomic) UIView *infomationView;
@property (strong, nonatomic) UILabel *statusLab;
@property (strong, nonatomic) NSString *certifyType;
@property (strong, nonatomic) UITextField *studenTF;
@property (strong, nonatomic) UITextField *nameTF;
@property (strong, nonatomic) UITextField *areaTF;
@property (strong, nonatomic) CALayer *layer4;
@property (strong, nonatomic) UIView *studenView;

@property (strong, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSMutableDictionary *reSellerDataDict;

@property (assign, nonatomic) BOOL isResult;

@property (strong, nonatomic) PMNetworking *networking;

@property (strong, nonatomic) NSString *areaValue;
@property (copy, nonatomic)   NSString *areaID;
@property (strong, nonatomic) HZAreaPickerView *locatePicker;

@property (strong, nonatomic) NSString *applyStatus;
@property (strong, nonatomic) NSString *shopCertifyId;

@property (strong, nonatomic) NSString *filterMessage;
@property (strong, nonatomic) NSString *filterString;
@property (copy,nonatomic) NSString *cityString;
@property (copy,nonatomic) NSString *cityId;
@property (weak,nonatomic) SWContactAddViewController *fatherVC;

- (void)cancelLocatePicker;

@end
