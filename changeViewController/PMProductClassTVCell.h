//
//  PMProductClassTVCell.h
//  changeViewController
//
//  Created by pmit on 15/1/6.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMProductCellImageView.h"

@interface PMProductClassTVCell : UITableViewCell
@property(nonatomic,strong)PMProductCellImageView *iv1;
@property(nonatomic,strong)PMProductCellImageView *iv2;
@property(nonatomic,strong)PMProductCellImageView *iv3;

- (void)createUI;
- (void)setContentLb1Text:(NSString *)text1 Lb2Text:(NSString *)text2 Lb3Text:(NSString *)text3;

- (void)setBtnTarget:(id)target selector:(SEL)sel indexPath:(NSIndexPath *)myIndexPath;

@end
