//
//  tableDelegate.h
//  changeViewController
//
//  Created by P&M on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class SearchResultViewController;

@interface tableDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *screenDetailsTV;
@property (strong,nonatomic) NSArray *titleArray;
@property (copy,nonatomic) NSString *titleType;
@property (copy,nonatomic) NSString *searchKey;
@property (strong,nonatomic) NSMutableArray *seletedArr;
@property (strong,nonatomic) NSMutableArray *openArr;
@property (strong,nonatomic) NSIndexPath *currentIndexPath;
@property (weak,nonatomic) SearchResultViewController *searchVC;
@property (copy,nonatomic) NSString *titleId;

@end
