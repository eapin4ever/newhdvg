//
//  Goods.h
//  changeViewController
//
//  Created by pmit on 14/11/20.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMGoodsReusableView : UICollectionViewCell
@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,assign)BOOL showDistance;
@property(nonatomic,assign)BOOL showDiscount;

- (void)createUI;

- (void)setContentTitle:(NSString *)title price:(double)price discount:(NSString *)discount;


@end
