//
//  PMMusicTableViewCell.h
//  changeViewController
//
//  Created by P&M on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMMusicTableViewCell : UITableViewCell

@property (strong, nonatomic) UIButton *selectedBtn;
@property (strong, nonatomic) UILabel *musicTitle;
@property (strong, nonatomic) UIButton *onAndOffBtn;

- (void)createUI;

- (void)setShopBgMusicTitle:(NSString *)title;

@end
