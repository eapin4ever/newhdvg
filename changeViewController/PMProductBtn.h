//
//  PMProductBtn.h
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

typedef NS_ENUM(NSInteger, PMGridStyle)
{
    PMGridStyleBig = 0,
    PMGridStyleSmallImageOnLeft,
    PMGridStyleSmallImageOnRight
};

@interface PMProductBtn : UIButton
@property(nonatomic,strong)UILabel *titleLB;
@property(nonatomic,strong)UILabel *priceLB;
@property(nonatomic,strong)UIImageView *productIV;

- (PMProductBtn *)initWithStyle:(PMGridStyle)style frame:(CGRect)frame;

@end
