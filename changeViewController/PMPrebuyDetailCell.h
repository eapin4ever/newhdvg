//
//  PMPrebuyDetailCell.h
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMPrebuyDetailCell : UITableViewCell
@property(nonatomic,strong)UIImageView *iv;
@property(nonatomic,strong)UILabel *timeLB;
- (void)createUI;

- (void)setTitle:(NSString *)title time:(NSString *)time price:(NSString *)price prepay:(NSString *)prepay;

@end
