//
//  PMOrderCodeTimeCell.h
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMOrderCodeTimeCell : UITableViewCell

@property (strong,nonatomic) UILabel *orderCodeLB;
@property (strong,nonatomic) UILabel *orderTimeLB;

- (void)createUI;
- (void)setcellData:(NSString *)orderCodeString Time:(NSString *)orderTimeString;

@end
