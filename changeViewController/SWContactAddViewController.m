//
//  SWContactAddViewController.m
//  changeViewController
//
//  Created by P&M on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "SWContactAddViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMUserInfos.h"
#import "PMToastHint.h"
#import "PMPublicClass.h"
#import "SWInfomationViewController.h"

@interface SWContactAddViewController ()

@end

@implementation SWContactAddViewController
{
    NSInteger _timerCount;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"验证手机号";
        
        self.view.backgroundColor = HDVGPageBGGray; // 设置背景色
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.reSellerDataDict = [NSMutableDictionary dictionary];
    
    [self createMobileViewUI];
    [self createButtonControllerUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 从登录数据中获得用户手机号
    self.mobileTF.text = [NSString stringWithFormat:@"%@", [[[PMUserInfos shareUserInfo].userDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"mobile"]];
    
    self.applyStatus = [self.reSellerDataDict objectNullForKey:@"applyStatus"];
    
    // 申请代言用户还没支付加盟费
    if (![self.applyStatus isEqualToString:@""] && [self.applyStatus integerValue] == 0) {
        
        self.mobileTF.text = [NSString stringWithFormat:@"%@", [self.reSellerDataDict objectNullForKey:@"mobile"]];
        
        self.authcodeView.hidden = YES;
        self.layer2.frame = CGRectMake(0, 44 - 0.5f, WIDTH, HeightRate(1));
        self.authcodeBtn.enabled = NO;
        self.authcodeBtn.backgroundColor = [UIColor lightGrayColor];
        self.authcodeTF.enabled = NO;
    }
}

- (void)createMobileViewUI
{
    // 创建手机号码 view
    self.mobileView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 44)];
    self.mobileView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.mobileView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.mobileView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(0, 44 - 0.5f, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    self.layer2 = layer2;
    [self.mobileView.layer addSublayer:layer2];
    
    // 手机号码
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(160), 30)];
    mobileLabel.backgroundColor = [UIColor clearColor];
    mobileLabel.text = @"手机号码:";
    mobileLabel.textColor = HDVGFontColor;
    mobileLabel.textAlignment = NSTextAlignmentLeft;
    mobileLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.mobileView addSubview:mobileLabel];
    
    UITextField *mobileTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(250), 7, WIDTH - WidthRate(250) - WidthRate(46), 30)];
    mobileTF.borderStyle = UITextBorderStyleNone;
    mobileTF.delegate = self;
    mobileTF.tag = 1;
    mobileTF.textColor = RGBA(100, 100, 100, 1);
    mobileTF.textAlignment = NSTextAlignmentLeft;
    mobileTF.font = [UIFont systemFontOfSize:15.0f];
    mobileTF.placeholder = @"请输入手机号码";
    mobileTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    mobileTF.returnKeyType = UIReturnKeyDone;
    mobileTF.keyboardType = UIKeyboardTypeNumberPad;
    self.mobileTF = mobileTF;
    [self.mobileView addSubview:mobileTF];
    //userAreaTF.text = [self.addressDic objectNullForKey:@"areas"];
    
    
    // 创建验证码 view
    self.authcodeView = [[UIView alloc] initWithFrame:CGRectMake(0, self.mobileView.frame.origin.y + self.mobileView.frame.size.height, WIDTH, 44)];
    self.authcodeView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.authcodeView];
    self.authcodeView.hidden = YES;
    
    // 短信验证码
    UILabel *authcodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(160), 30)];
    authcodeLabel.backgroundColor = [UIColor clearColor];
    authcodeLabel.text = @"验  证  码:";
    authcodeLabel.textColor = HDVGFontColor;
    authcodeLabel.textAlignment = NSTextAlignmentLeft;
    authcodeLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.authcodeView addSubview:authcodeLabel];
    
    UITextField *authcodeTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(250), 7, WIDTH - WidthRate(270) - HeightRate(210), 30)];
    authcodeTF.borderStyle = UITextBorderStyleNone;
    authcodeTF.delegate = self;
    authcodeTF.tag = 2;
    authcodeTF.textColor = RGBA(100, 100, 100, 1);
    authcodeTF.textAlignment = NSTextAlignmentLeft;
    authcodeTF.font = [UIFont systemFontOfSize:14.0f];
    authcodeTF.placeholder = @"请输入验证码";
    authcodeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    authcodeTF.returnKeyType = UIReturnKeyDone;
    authcodeTF.keyboardType = UIKeyboardTypeNumberPad;
    self.authcodeTF = authcodeTF;
    [self.authcodeView addSubview:authcodeTF];
    
    // 获取验证码按钮
    CGFloat X = WIDTH - (WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30));
    CGFloat widtd = WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30) - WidthRate(46);
    self.authcodeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.authcodeBtn.frame = CGRectMake(X, 7, widtd, 30);
    [self.authcodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.authcodeBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.authcodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.authcodeBtn addTarget:self action:@selector(authcodeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.authcodeBtn setBackgroundColor:ButtonBgColor];
    [self.authcodeBtn.layer setCornerRadius:4.0];
    [self.authcodeView addSubview:self.authcodeBtn];
    
    self.retryLab = [[UILabel alloc] initWithFrame:CGRectMake(X, 7, widtd, 30)];
    self.retryLab.textAlignment = NSTextAlignmentCenter;
    self.retryLab.hidden = YES;
    self.retryLab.text = @"重发(60)";
    self.retryLab.textColor = [UIColor darkGrayColor];
    self.retryLab.font = [UIFont systemFontOfSize:12.0f];
    self.retryLab.textAlignment = NSTextAlignmentCenter;
    [self.authcodeView addSubview:self.retryLab];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(0, 44 - 0.5, WIDTH, 0.5);
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.authcodeView.layer addSublayer:layer3];
}

// 创建按钮控件 UI
- (void)createButtonControllerUI
{
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:@"下一步" style:UIBarButtonItemStylePlain target:self action:@selector(addForNextButtonClick:)];
    
    [submitButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR,NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = submitButton;
    
//    // 初始化完成设置按钮
//    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    nextButton.frame = CGRectMake(WidthRate(46), HEIGHT - 64 - 60, WIDTH - WidthRate(46) * 2, 44);
//    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
//    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [nextButton addTarget:self action:@selector(addForNextButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [nextButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
//    [nextButton setBackgroundColor:ButtonBgColor];
//    [nextButton.layer setCornerRadius:6.0f];
//    [self.view addSubview:nextButton];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    [self.mobileTF resignFirstResponder];
    [self.authcodeTF resignFirstResponder];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *mobile = nil;
    NSString *mobile = [self.reSellerDataDict objectNullForKey:@"mobile"];
    
    // 申请代言用户还没支付加盟费
    if (![self.applyStatus isEqualToString:@""] && [self.applyStatus integerValue] == 0) {
        // 如果从申请代言商中获取的手机号与输入框输入的手机号不相同，可重新获取验证码
        mobile = [self.reSellerDataDict objectNullForKey:@"mobile"];
    }
    
    if (self.mobileTF.tag == 1 && textField == self.mobileTF && textField.text.length - range.length + string.length == 11 && ![self.mobileTF.text isEqualToString:mobile]) {
        
        self.authcodeView.hidden = NO;
        self.layer2.frame = CGRectMake(WidthRate(26), 44 - 0.5, WIDTH, HeightRate(1));
        self.authcodeBtn.enabled = YES;
        [self.authcodeBtn setBackgroundColor:ButtonBgColor];
        self.authcodeTF.enabled = YES;
    }
    
        
    if (self.mobileTF.tag == 1 && textField == self.mobileTF && textField.text.length - range.length + string.length > 11) {
        return NO;
    }
    
    if (self.authcodeTF.tag == 2 && textField == self.authcodeTF && textField.text.length - range.length + string.length > 6) {
        return NO;
    }
    
    //第一步,对组件增加监听器
    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    return YES;
}


//- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;

//第二步,实现回调函数
- (void) textFieldDidChange:(id) sender {
//    UITextField *_field = (UITextField *)sender;
//    NSLog(@"%@",[_field text]);
    
    NSString *mobile = [self.reSellerDataDict objectNullForKey:@"mobile"];
    
    if ([self.mobileTF.text isEqualToString:mobile]) {
        self.authcodeView.hidden = YES;
        self.layer2.frame = CGRectMake(0, 44 - 0.5f, WIDTH, HeightRate(1));
        self.authcodeBtn.enabled = NO;
    }
}





- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // 申请代言还没付款
    NSString *applyStatus = [self.reSellerDataDict objectNullForKey:@"applyStatus"];
    NSString *mobile = [self.reSellerDataDict objectNullForKey:@"mobile"];
    
    if ([self.mobileTF.text isEqualToString:mobile]) {
        self.authcodeView.hidden = YES;
        self.layer2.frame = CGRectMake(0, 44 - 0.5f, WIDTH, HeightRate(1));
        self.authcodeBtn.enabled = NO;
    }
    
    if (![applyStatus isEqualToString:@""] && [applyStatus integerValue] == 0 && [self.mobileTF.text isEqualToString:mobile]) {
        
        self.authcodeView.hidden = YES;
        self.layer2.frame = CGRectMake(0, 44 - 0.5f, WIDTH, HeightRate(1));
        self.authcodeBtn.enabled = NO;
        self.authcodeBtn.backgroundColor = [UIColor lightGrayColor];
        self.authcodeTF.enabled = NO;
    }
}

- (void)authcodeButtonClick:(UIButton *)sender
{
    if (self.mobileTF.text.length != 11 || ![self.mobileTF.text hasPrefix:@"1"] || ![PMPublicClass checkMobile:self.mobileTF.text])
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的手机号码"];
    }
    else {
        /**
         * 用户输入手机号码获取短信验证码
         * 参数: sendSMS:(string)验证码
         */
        NSDictionary *phoneSendSMS = @{@"mobile":self.mobileTF.text};
        self.networking = [PMNetworking defaultNetworking];
        
        [self.networking request:PMRequestStateSendSMS WithParameters:phoneSendSMS callBackBlock:^(NSDictionary *dic) {
            
            if ([[dic objectNullForKey:@"success"] integerValue] == 1) {
                
                NSInteger messageCode = [[dic objectNullForKey:@"messageCode"] integerValue];
                if (messageCode != 1)
                {
                    [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该号超出每天获取验证码条数"];
                }
                else
                {
                    // 按钮变灰，倒数，“重发”
                    if(!self.timer.isValid)//正在倒数
                    {
                        sender.backgroundColor = [UIColor lightGrayColor];
                        sender.userInteractionEnabled = NO;
                        _timerCount = 60;
                        [sender setTitle:@"重发(60)" forState:UIControlStateNormal];
                        [sender setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCountDown:) userInfo:sender repeats:YES];
                    }
                }
                
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dic objectNullForKey:@"message"] message: [dic objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
        }showIndicator:NO];
    }
}

#pragma mark  获取验证码按钮倒数
- (void)timerCountDown:(NSTimer *)timer
{
    UIButton *btn = timer.userInfo;
    
    if(_timerCount == 0)
    {
        //停止运行timer
        [timer invalidate];
        //按钮恢复成红色
        
        [btn setBackgroundColor:RGBA(218, 67, 80, 1)];
        self.retryLab.hidden = YES;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:@"重发" forState:UIControlStateNormal];
        btn.userInteractionEnabled = YES;
    }
    else
    {
        _timerCount --;
        self.retryLab.hidden = NO;
        self.retryLab.text = [NSString stringWithFormat:@"重发(%ld)",(long)_timerCount];
        [btn setTitle:@"" forState:UIControlStateNormal];
    }
    
}


#pragma mark - 下一步按钮响应事件
- (void)addForNextButtonClick:(UIButton *)sender
{
    // 申请代言还没付款
    NSString *applyStatus = [self.reSellerDataDict objectNullForKey:@"applyStatus"];
    NSString *mobile = [self.reSellerDataDict objectNullForKey:@"mobile"];
    
    if (![applyStatus isEqualToString:@""] && [applyStatus integerValue] == 0 && [self.mobileTF.text isEqualToString:mobile]) {
        
        SWInfomationViewController *infosVC = [[SWInfomationViewController alloc] init];
        infosVC.reSellerDataDict = self.reSellerDataDict;//传代言商信息
        infosVC.fatherVC = self;
        infosVC.cityString = self.cityName;
        infosVC.cityId = self.cityId;
        [self.navigationController pushViewController:infosVC animated:YES];
        
        return;
    }
    
    // 用户第一次申请，手机号码从后台获取，如果没有改变手机号码，不需验证，直接下一步
    if ([self.mobileTF.text isEqualToString:mobile]) {
        
        NSMutableDictionary *mobileData = [@{@"mobile":self.mobileTF.text} mutableCopy];
        
        SWInfomationViewController *infosVC = [[SWInfomationViewController alloc] init];
        [mobileData setObject:@"" forKey:@"applyStatus"];
        infosVC.reSellerDataDict = mobileData;//传申请代言商的手机号
        infosVC.fatherVC = self;
        infosVC.cityString = self.cityName;
        infosVC.cityId = self.cityId;
        [self.navigationController pushViewController:infosVC animated:YES];
    }
    else {
        // 判断是否按要求输入手机号或者验证码
        if (self.mobileTF.text.length != 11 || ![self.mobileTF.text hasPrefix:@"1"] || ![PMPublicClass checkMobile:self.mobileTF.text])
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的手机号码"];
            return;
        }
        if (self.authcodeTF.text.length == 0) {
            
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入获取到手机验证码"];
            return;
        }
        
        if (self.mobileTF.text.length != 0 && [self.mobileTF.text hasPrefix:@"1"] && [PMPublicClass checkMobile:self.mobileTF.text] && self.authcodeTF.text.length != 0) {
            
            // 申请代言用户手机号
            //[self.reSellerDataDict setObject:self.mobileTF.text forKey:@"mobile"];
            NSMutableDictionary *mobileData = [@{@"mobile":self.mobileTF.text} mutableCopy];
            
            //        SWInfomationViewController *infosVC = [[SWInfomationViewController alloc] init];
            //        [mobileData setObject:@"" forKey:@"applyStatus"];
            //        infosVC.reSellerDataDict = mobileData;//传申请代言商的手机号
            //        [self.navigationController pushViewController:infosVC animated:YES];
            
            
            NSDictionary *authcodeData = @{@"validateCode":self.authcodeTF.text};
            self.networking = [PMNetworking defaultNetworking];
            
            __block NSDictionary *callBackDict;
            
            [self.networking request:PMRequestStateCheckValidCode WithParameters:authcodeData callBackBlock:^(NSDictionary *dict) {
                
                callBackDict = dict;
                
                // 如果返回的信息中，success的值是true则注册成功
                if ([[callBackDict objectNullForKey:@"success"] integerValue] == 1) {
                    
                    SWInfomationViewController *infosVC = [[SWInfomationViewController alloc] init];
                    
                    // 申请代言用户还没支付加盟费
                    if (![self.applyStatus isEqualToString:@""] && [self.applyStatus integerValue] == 0) {
                        NSString *shopCertifyId = [self.reSellerDataDict objectNullForKey:@"shopCertifyId"];
                        NSString *areaName = [self.reSellerDataDict objectNullForKey:@"areaName"];
                        NSString *name = [self.reSellerDataDict objectNullForKey:@"name"];
                        NSString *areaId = [self.reSellerDataDict objectNullForKey:@"areaId"];
                        NSString *certifyNum = [self.reSellerDataDict objectNullForKey:@"certifyNum"];
                        NSString *certifyType = [self.reSellerDataDict objectNullForKey:@"certifyType"];
                        
                        NSMutableDictionary *userInfoData = [@{@"shopCertifyId":shopCertifyId,@"mobile":self.mobileTF.text,@"name":name,@"areaId":areaId,@"areaName":areaName,@"applyStatus":applyStatus,@"certifyType":certifyType,@"certifyNum":certifyNum} mutableCopy];
                        infosVC.reSellerDataDict = userInfoData;//传申请代言商的信息
                    }
                    else {
                        [mobileData setObject:@"" forKey:@"applyStatus"];
                        infosVC.reSellerDataDict = mobileData;//传申请代言商手机号
                    }
                    
                    [self.navigationController pushViewController:infosVC animated:YES];
                }
                else {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView show];
                }
                
            }showIndicator:NO];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
