//
//  PMShopSettingTableViewCell.h
//  changeViewController
//
//  Created by P&M on 15/6/30.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMShopSettingTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *listTitleLab;

- (void)createShopSettingUI;

- (void)setShopSettingListTitle:(NSString *)listTitle;

@end
