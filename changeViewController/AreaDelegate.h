//
//  AreaDelegate.h
//  changeViewController
//
//  Created by pmit on 15/9/15.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol AreaDelegateDelegate <NSObject>

- (void)goNextData:(NSString *)areaId AreaName:(NSString *)areaName CityName:(NSString *)cityName CityId:(NSString *)cityId;

@end

@interface AreaDelegate : NSObject <UITableViewDelegate,UITableViewDataSource>

@property (strong,nonatomic) NSArray *areaArr;
@property (weak,nonatomic) id<AreaDelegateDelegate> areaDDelegate;
@property (copy,nonatomic) NSString *cityId;
@property (copy,nonatomic) NSString *cityName;


@end
