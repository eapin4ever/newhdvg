//
//  PMShoppingCarCell.h
//  changeViewController
//
//  Created by pmit on 15/6/27.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMShoppingCarCell : UITableViewCell

@property (nonatomic,strong)UIButton *minusBtn;
@property (nonatomic,strong)UIButton *plusBtn;
@property (nonatomic,strong)UIImageView *iv;
@property (assign,nonatomic) NSInteger maxCount;
@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UIButton *selectedBtn;
@property (strong,nonatomic) UILabel *classifyLB1;
@property (assign,nonatomic) BOOL isDown;
@property (assign,nonatomic) BOOL isFirst;
@property (strong,nonatomic) UILabel *discountLB;
@property (strong,nonatomic) UIImageView *activeIV;

- (void)createUI;

- (void)setContentTitle:(NSString *)title classify1:(NSString *)classify1 classify2:(NSString *)classify2 distanceOrLocation:(NSString *)distance number:(NSString *)num price:(double)price;
- (void)numberPlus:(UIButton *)sender;
- (void)numberMinus:(UIButton *)sender;

@end
