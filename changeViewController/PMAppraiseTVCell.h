//
//  PMAppraiseTVCell.h
//  changeViewController
//
//  Created by pmit on 14/12/15.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMAppraiseTVCell : UITableViewCell

@property (strong, nonatomic) NSDictionary *remarkDict;

@property (strong, nonatomic) UILabel *userLB;
@property (strong, nonatomic) UILabel *appraiseLB;
@property (strong, nonatomic) UILabel *timeLB;
@property (assign,nonatomic) BOOL isBigVC;
@property (strong,nonatomic) UIButton *startBtn1;
@property (strong,nonatomic) UIButton *startBtn2;
@property (strong,nonatomic) UIButton *startBtn3;
@property (strong,nonatomic) UIButton *startBtn4;
@property (strong,nonatomic) UIButton *startBtn5;

- (void)createUI;
- (void)setContentUserName:(NSString *)name detail:(NSString *)detail time:(NSString *)time;

@end
