//
//  PMStoreScreenTwoDelegate.m
//  changeViewController
//
//  Created by P&M on 15/7/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMStoreScreenTwoDelegate.h"
#import "PMMyPhoneInfo.h"
#import "PMStoreScreenDetailCell.h"
#import "PMProductStoreViewController.h"
#import "PMDistubitionStoreViewController.h"

@implementation PMStoreScreenTwoDelegate

#pragma mark - tableView data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    self.screenTwoArray = [self.screenTwoDict objectNullForKey:@"childrens"];
    return self.screenTwoArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.screenTwoArray[section] objectNullForKey:@"childrens"] count] > 0)
    {
        return [[self.screenTwoArray[section] objectNullForKey:@"childrens"] count] + 1;
    }
    else
    {
        return 0;
    }
}

// 每个行的头的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *twoClassLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(400), 30)];
    twoClassLB.font = [UIFont systemFontOfSize:15.0f];
    twoClassLB.textAlignment = NSTextAlignmentLeft;
    twoClassLB.textColor = HDVGFontColor;
    [headerView addSubview:twoClassLB];
    
    NSDictionary *dict = self.screenTwoArray[section];
    
    twoClassLB.text = [dict objectNullForKey:@"name"];
    
    return headerView;
    
}

// 设置 footer 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return HeightRate(1);
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(1))];
    footerView.backgroundColor = RGBA(200, 200, 200, 1);
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMStoreScreenDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:@"detailsCell"];
    
    [detailCell createScreenDetailUI];
    detailCell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = self.screenTwoArray[indexPath.section];
    self.screenDetailArray = [dict objectNullForKey:@"childrens"];
    
    if (indexPath.row == 0)
    {
        NSArray *thirdChildrenArr = [[[self.screenTwoDict objectNullForKey:@"childrens"] objectAtIndex:indexPath.section] objectNullForKey:@"childrens"];
        BOOL isAll = YES;
        for (NSDictionary *thirdDic in thirdChildrenArr)
        {
            if ([self.productStoreVC.thirdClassIdArray containsObject:[thirdDic objectNullForKey:@"id"]]) {
                
            }
            else
            {
                isAll = NO;
                break;
            }
        }
        
        if (isAll)
        {
            detailCell.checkIV.hidden = NO;
        }
        else
        {
            detailCell.checkIV.hidden = YES;
        }
    }
    else
    {
        NSDictionary *dic = self.screenDetailArray[indexPath.row - 1];
        NSString *thirdClassId = [dic objectNullForKey:@"id"];
        if ([self.productStoreVC.thirdClassIdArray containsObject:thirdClassId])
        {
            detailCell.checkIV.hidden = NO;
        }
        else
        {
            detailCell.checkIV.hidden = YES;
        }
    }
    
    if (indexPath.row == 0)
    {
        [detailCell setStoreScreenDetails:@"全部"];
    }
    else
    {
        NSDictionary *dic = self.screenDetailArray[indexPath.row - 1];
        [detailCell setStoreScreenDetails:[dic objectNullForKey:@"name"]];
    }
    
    return detailCell;
}


#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0)
    {
        PMStoreScreenDetailCell *cell = (PMStoreScreenDetailCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSString *firstClassId = [self.screenTwoDict objectNullForKey:@"id"];
        NSString *secondClassId = [[[self.screenTwoDict objectNullForKey:@"childrens"] objectAtIndex:indexPath.section] objectNullForKey:@"id"];
        NSArray *thirdChildrenArr = [[[self.screenTwoDict objectNullForKey:@"childrens"] objectAtIndex:indexPath.section] objectNullForKey:@"childrens"];
         NSArray *secondClassArr = [self.screenTwoDict objectNullForKey:@"childrens"];
        
        if (cell.checkIV.hidden)
        {
            cell.checkIV.hidden = NO;
            
            if (![self.productStoreVC.firstClassIdArray containsObject:firstClassId])
            {
                [self.productStoreVC.firstClassIdArray addObject:firstClassId];
            }
            
            
            if (![self.productStoreVC.secondClassIdArray containsObject:secondClassId])
            {
                [self.productStoreVC.secondClassIdArray addObject:secondClassId];
            }

            for (NSDictionary *thirdDic in thirdChildrenArr)
            {
                if ([self.productStoreVC.thirdClassIdArray containsObject:[thirdDic objectNullForKey:@"id"]])
                {
                    
                }
                else
                {
                    [self.productStoreVC.thirdClassIdArray addObject:[thirdDic objectNullForKey:@"id"]];
                }
            }
        }
        else
        {
            cell.checkIV.hidden = YES;
            for (NSDictionary *thirdDic in thirdChildrenArr)
            {
                [self.productStoreVC.thirdClassIdArray removeObject:[thirdDic objectNullForKey:@"id"]];
            }
            
            [self.productStoreVC.secondClassIdArray removeObject:secondClassId];
            
            
            BOOL isHasSecond = NO;
            for (NSDictionary *secondDic in secondClassArr)
            {
                NSString *dicSecondId = [secondDic objectNullForKey:@"id"];
                if ([self.productStoreVC.secondClassIdArray containsObject:dicSecondId])
                {
                    isHasSecond = YES;
                    break;
                }
            }
            
            if (!isHasSecond)
            {
                [self.productStoreVC.firstClassIdArray removeObject:firstClassId];
            }
        }
        
        NSIndexSet *indexSet=[[NSIndexSet alloc] initWithIndex:indexPath.section];
        [tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else
    {
        NSString *firstClassId = [self.screenTwoDict objectNullForKey:@"id"];
        NSArray *secondClassArr = [self.screenTwoDict objectNullForKey:@"childrens"];
        NSString *secondClassId = [[[self.screenTwoDict objectNullForKey:@"childrens"] objectAtIndex:indexPath.section] objectNullForKey:@"id"];
        NSArray *thirdChildrenArr = [[[self.screenTwoDict objectNullForKey:@"childrens"] objectAtIndex:indexPath.section] objectNullForKey:@"childrens"];
        NSString *thirdClassId = [[[[[self.screenTwoDict objectNullForKey:@"childrens"] objectAtIndex:indexPath.section] objectNullForKey:@"childrens"] objectAtIndex:indexPath.row - 1] objectNullForKey:@"id"];
        PMStoreScreenDetailCell *cell = (PMStoreScreenDetailCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (cell.checkIV.hidden)
        {
            if (![self.productStoreVC.firstClassIdArray containsObject:firstClassId])
            {
                [self.productStoreVC.firstClassIdArray addObject:firstClassId];
            }
            
            if (![self.productStoreVC.secondClassIdArray containsObject:secondClassId])
            {
                [self.productStoreVC.secondClassIdArray addObject:secondClassId];
            }
            
            [self.productStoreVC.thirdClassIdArray addObject:thirdClassId];
            
            BOOL isAll = YES;
            for (NSDictionary *thirdDic in thirdChildrenArr)
            {
                if (![self.productStoreVC.thirdClassIdArray containsObject:[thirdDic objectNullForKey:@"id"]]) {
                    isAll = NO;
                    break;
                }
            }
            
            PMStoreScreenDetailCell *zeroCell = (PMStoreScreenDetailCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section]];
            if (isAll)
            {
                zeroCell.checkIV.hidden = NO;
            }
            else
            {
                zeroCell.checkIV.hidden = YES;
            }
            
            cell.checkIV.hidden = NO;
        }
        else
        {
            [self.productStoreVC.thirdClassIdArray removeObject:thirdClassId];
            
            PMStoreScreenDetailCell *zeroCell = (PMStoreScreenDetailCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section]];
            zeroCell.checkIV.hidden = YES;
            
            BOOL isHasThird = NO;
            for (NSDictionary *thirdDic in thirdChildrenArr)
            {
                NSString *dicThirdId = [thirdDic objectNullForKey:@"id"];
                if ([self.productStoreVC.thirdClassIdArray containsObject:dicThirdId])
                {
                    isHasThird = YES;
                    break;
                }
            }
            
            if (!isHasThird)
            {
                [self.productStoreVC.secondClassIdArray removeObject:secondClassId];
            }
            
            BOOL isHasSecond = NO;
            for (NSDictionary *secondDic in secondClassArr)
            {
                NSString *dicSecondId = [secondDic objectNullForKey:@"id"];
                if ([self.productStoreVC.secondClassIdArray containsObject:dicSecondId])
                {
                    isHasSecond = YES;
                    break;
                }
            }
            
            if (!isHasSecond)
            {
                [self.productStoreVC.firstClassIdArray removeObject:firstClassId];
            }
            
            cell.checkIV.hidden = YES;
        }
    }
}


@end
