//
//  PMMyUnreadViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/19.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMMyUnreadViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "YBMessageCell.h"

@interface PMMyUnreadViewController () <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *messageTableView;
@property (strong,nonatomic) NSArray *messageListArr;
@property (strong,nonatomic) UIView *noDataView;
@property (strong,nonatomic) NSMutableArray *timesArray;
@property (strong,nonatomic) UIView *detailView;
@property (strong,nonatomic) UILabel *detailTitleLB;
@property (strong,nonatomic) UILabel *detailTimeLB;
@property (strong,nonatomic) UILabel *detailContentLB;
@property (strong,nonatomic) UIView *messageDetailView;

@end

@implementation PMMyUnreadViewController

static NSString *const messageCell = @"messageCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息通知";
    self.timesArray = [NSMutableArray array];
    [self createUI];
    [self createNoDataView];
    [self getMessageByNet];
    [self buildDetailView];
    [self createNoDataView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createUI
{
    self.messageTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.messageTableView.delegate = self;
    self.messageTableView.dataSource = self;
    self.messageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.messageTableView.backgroundColor = RGBA(231, 231, 231, 1);
    [self.messageTableView registerClass:[YBMessageCell class] forCellReuseIdentifier:messageCell];
    [self.view addSubview:self.messageTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.messageListArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YBMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:messageCell];
    [cell createUI];
    
    NSDictionary *notiDic = self.messageListArr[indexPath.section];
    // 获取下来的时间数据
    NSString *timeType = self.timesArray[indexPath.section];
    //    [cell setUnreadMessagesTime:timeType];
    NSString *messageTitle = [notiDic objectNullForKey:@"title"];
    NSString *messageContent = [notiDic objectNullForKey:@"content"];
    NSString *messageTime = timeType;
    cell.messageDic = @{@"title":messageTitle,@"content":messageContent,@"time":messageTime};
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)getMessageByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateGetPushMessageList WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.messageListArr = [dic objectNullForKey:@"data"];
            if (self.messageListArr.count > 0)
            {
                [self initUnreadMessagesTimeData];
                [self.messageTableView reloadData];
                self.noDataView.hidden = YES;
            }
            else
            {
                self.noDataView.hidden = NO;
            }
        }
        else
        {
            self.noDataView.hidden = NO;
        }
        
        
    } showIndicator:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = RGBA(231, 231, 231, 1);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *messageContent = [self.messageListArr[indexPath.section] objectNullForKey:@"content"];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[self.messageListArr[indexPath.section] objectNullForKey:@"sendTime"] doubleValue]];
    NSString *sendTimeString = [fmt stringFromDate:date];
    [self showMessageDetail:messageContent andTitle:[self.messageListArr[indexPath.section] objectNullForKey:@"title"] AndTime:sendTimeString];
    self.detailView.hidden = NO;
}

#pragma mark - 没消息视图
- (void)createNoDataView
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    noDataView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *noIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noDataView.frame.size.height)];
    noIV.image = [UIImage imageNamed:@"info_null"];
    noIV.contentMode = UIViewContentModeScaleAspectFit;
    [noDataView addSubview:noIV];
    
    [self.view insertSubview:noDataView atIndex:1000];
    self.noDataView = noDataView;
    self.noDataView.hidden = YES;
}

#pragma mark - 添加未读消息假数据
- (void)initUnreadMessagesTimeData
{
    for (NSDictionary *notiDic in self.messageListArr)
    {
        NSString *timeType = @"";
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"EEE MMM dd HH:mm:ss zzzz yyyy";
        fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[notiDic objectNullForKey:@"sendTime"] doubleValue]];
        NSDate *now = [NSDate date];
        
        NSTimeInterval delta = [now timeIntervalSinceDate:date];
        if (delta <= 60)
        {
            timeType = @"刚刚";
        }
        else if (delta <= 3600 && delta > 60)
        {
            timeType = [NSString stringWithFormat:@"%.0f分钟前",delta / 60];
        }
        else if (delta <= 24 * 60 * 60 && delta > 3600)
        {
            timeType = [NSString stringWithFormat:@"%.0f小时前",delta / 3600];
        }else
        {
            fmt.dateFormat = @"MM-dd HH:mm:ss";
            timeType = [fmt stringFromDate:date];
        }
        
        [self.timesArray addObject:timeType];
        
    }
}

- (void)buildDetailView
{
    self.detailView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.detailView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.detailView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:self.detailView.frame];
    alphaView.backgroundColor = [UIColor darkGrayColor];
    alphaView.alpha = 0.8;
    [self.detailView addSubview:alphaView];
    
    UITapGestureRecognizer *dismissTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(detailDismiss:)];
    [self.detailView addGestureRecognizer:dismissTap];
    
    self.messageDetailView = [[UIView alloc] init];
    self.messageDetailView.center = CGPointMake(self.detailView.bounds.size.width / 2, self.detailView.bounds.size.height / 2 - 30);
    self.messageDetailView.backgroundColor = [UIColor whiteColor];
    [self.messageDetailView.layer setCornerRadius:5.0f];
    [self.detailView addSubview:self.messageDetailView];
    
    CALayer *titleLine = [CALayer layer];
    titleLine.backgroundColor = RGBA(231, 231, 231, 1).CGColor;
    titleLine.frame = CGRectMake(0, 40, WIDTH - WidthRate(100), 0.8);
    [self.messageDetailView.layer addSublayer:titleLine];
    
    self.detailTitleLB = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - WidthRate(100)) * 0.05, 0, (WIDTH - WidthRate(100)) * 0.5, 40)];
    self.detailTitleLB.font = [UIFont systemFontOfSize:14.0f];
    self.detailTitleLB.textAlignment = NSTextAlignmentLeft;
    self.detailTitleLB.textColor = [UIColor darkGrayColor];
    [self.messageDetailView addSubview:self.detailTitleLB];
    
    self.detailTimeLB = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - WidthRate(100)) * 0.5, 0, (WIDTH - WidthRate(100)) * 0.5, 40)];
    self.detailTimeLB.font = [UIFont systemFontOfSize:13.0f];
    self.detailTimeLB.textAlignment = NSTextAlignmentCenter;
    self.detailTimeLB.textColor = [UIColor darkGrayColor];
    [self.messageDetailView addSubview:self.detailTimeLB];
    
    self.detailContentLB = [[UILabel alloc] init];
    self.detailContentLB.textAlignment = NSTextAlignmentLeft;
    self.detailContentLB.font = [UIFont systemFontOfSize:15.0f];
    self.detailContentLB.textColor = HDVGFontColor;
    self.detailContentLB.numberOfLines = 0;
    [self.messageDetailView addSubview:self.detailContentLB];
    
    self.detailView.hidden = YES;
}

- (void)detailDismiss:(UITapGestureRecognizer *)tap
{
    self.detailView.hidden = YES;
}

- (void)showMessageDetail:(NSString *)content andTitle:(NSString *)title AndTime:(NSString *)times
{
    CGSize contentSize = [content sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(WIDTH - WidthRate(100), MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    self.messageDetailView.bounds = (CGRect){CGPointZero,{WIDTH - WidthRate(100),contentSize.height + 100}};
    self.detailTitleLB.text = title;
    self.detailTimeLB.text = times;
    self.detailContentLB.frame = CGRectMake((WIDTH - WidthRate(100)) * 0.05, 42, (WIDTH - WidthRate(100)) * 0.9, contentSize.height + 30);
    self.detailContentLB.text = content;
    
}


@end
