//
//  PMSearchFilterCell.m
//  changeViewController
//
//  Created by pmit on 14/12/10.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMSearchFilterCell.h"

#define lightGray RGBA(200, 200, 200, 1)

@implementation PMSearchFilterCell
{
    UIImageView *checkMark;
}

- (void)createUI
{
    if(!checkMark)
    {
        checkMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        checkMark.image = [UIImage imageNamed:@"check"];
        self.accessoryView = checkMark;
    }
}


@end
