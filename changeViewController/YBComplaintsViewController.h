//
//  YBComplaintsViewController.h
//  changeViewController
//
//  Created by ZhangEapin on 15/5/13.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YBComplaintsViewControllerDelegate <NSObject>

- (void)finishComplain:(NSArray *)productArr;

@end

@interface YBComplaintsViewController : UIViewController

@property (strong,nonatomic) NSArray *productArr;
@property (weak,nonatomic) id<YBComplaintsViewControllerDelegate> complainDelegate;
@property (assign,nonatomic) BOOL isHasDiscuss;
@property (copy,nonatomic) NSString *orderCode;
@property (assign,nonatomic) BOOL isFromDetail;

@end
