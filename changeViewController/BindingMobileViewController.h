//
//  BindingMobileViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  绑定新手机号控制器
 */

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "PMUserInfos.h"

@interface BindingMobileViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIView *bindingMobileView;
@property (strong, nonatomic) UITextField *mobileTextField;
@property (strong, nonatomic) UITextField *authcodeTextField;

@property (strong, nonatomic) PMNetworking *networking;

@end
