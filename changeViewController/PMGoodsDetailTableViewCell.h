//
//  PMGoodsDetailTableViewCell.h
//  changeViewController
//
//  Created by pmit on 14/11/25.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMGoodsDetailTableViewCell : UITableViewCell
@property(nonatomic,strong)UIButton *attentionBtn;



- (void)createUI;

//- (void)setContentTitle:(NSString *)title sales:(NSString *)sales stock:(NSString *)stock distance:(NSString *)distance price:(NSString *)price;

- (void)setContentTitle:(NSString *)title distance:(NSString *)distance;

- (void)setPrice:(NSString *)price sales:(NSString *)sales stock:(NSString *)stock;

@end
