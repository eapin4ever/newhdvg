//
//  NSDictionary+PMDictionaryCategory.h
//  changeViewController
//
//  Created by pmit on 14/12/31.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PMDictionaryCategory)
- (id)objectNullForKey:(id)aKey;

@end
