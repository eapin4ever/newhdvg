//
//  SWPictureViewController.h
//  changeViewController
//
//  Created by P&M on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMNetworking.h"

@interface SWPictureViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) UIView *firstView;
@property (strong, nonatomic) UIView *secondView;

@property (strong, nonatomic) NSDictionary *userDataDict;
@property (strong, nonatomic) NSDictionary *reSellerDataDict;

@property (strong, nonatomic) NSData *userAndCardPhotoId;
@property (strong, nonatomic) NSData *userCardPhotoId;

@property (strong, nonatomic) NSString *pictureStr1;
@property (strong, nonatomic) NSString *pictureStr2;

@property (strong, nonatomic) NSString *shopCertifyIdStr;

@property (strong, nonatomic) UIView *bgView;
@property (strong, nonatomic) UIActivityIndicatorView *activityView;

@property (strong, nonatomic) PMNetworking *networking;

@end
