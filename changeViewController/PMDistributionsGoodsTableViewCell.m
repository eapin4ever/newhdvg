//
//  PMDistributionsGoodsTableViewCell.m
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMDistributionsGoodsTableViewCell.h"

#define itemYUp 80
#define itemYDown 115
#define titleFont 12
#define titleFont2 10

@implementation PMDistributionsGoodsTableViewCell
{
    UILabel *goodsTitleLB;
    UILabel *hintLB;
    UILabel *commissionLB;
    double percentNum;
    
    UILabel *numLB1;
    UILabel *numLB2;
    UILabel *numLB3;
}

- (void)createUI
{
    if (!_iv)
    {
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, WidthRate(200), WidthRate(200))];
        _iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv];
        
        goodsTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(200) + 15,20, WIDTH-180,40)];
        goodsTitleLB.numberOfLines = 2;
        goodsTitleLB.font = [UIFont systemFontOfSize:12.0f];
        goodsTitleLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:goodsTitleLB];
        
        self.shareBtn = [[YBShareBtn alloc] initWithFrame:CGRectMake(WIDTH - 85, 0, 50, 150)];
        [self.shareBtn setImage:[UIImage imageNamed:@"share_02"] forState:UIControlStateNormal];
        [self.shareBtn setTitle:@"分享到" forState:UIControlStateNormal];
        self.shareBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.shareBtn];
        
        self.deleteBtn = [[YBShareBtn alloc] initWithFrame:CGRectMake(WIDTH - 85, 0, 50, 150)];
        [self.deleteBtn setImage:[UIImage imageNamed:@"distruDelete"] forState:UIControlStateNormal];
        self.deleteBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.deleteBtn];
        
        UIView *messageView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(200) + 10, 70, WIDTH -WidthRate(200) - 85, WidthRate(200) - 10)];
        self.messageView = messageView;
        
        UILabel *unitPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, messageView.bounds.size.width / 2 - 10, 20)];
        unitPriceLB.text = @"单价";
        unitPriceLB.font = [UIFont systemFontOfSize:titleFont];
        unitPriceLB.textColor = RGBA(135, 135, 135, 1);
        unitPriceLB.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:unitPriceLB];
        
        numLB1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, messageView.bounds.size.width / 2 - 10, 30)];
        numLB1.textColor = HDVGRed;
        numLB1.font = [UIFont systemFontOfSize:HeightRate(33)];
        numLB1.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:numLB1];
        
        
        
        UILabel *moneyLB = [[UILabel alloc] initWithFrame:CGRectMake(messageView.bounds.size.width / 2 - 10, 0, messageView.bounds.size.width / 2 + 10, 20)];
        moneyLB.text = @"佣金";
        moneyLB.font = [UIFont systemFontOfSize:titleFont];
        moneyLB.textColor = RGBA(135, 135, 135, 1);
        moneyLB.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:moneyLB];
        
        numLB3 = [[UILabel alloc] initWithFrame:CGRectMake(messageView.bounds.size.width / 2 - 10, 20, messageView.bounds.size.width / 2 + 10, 30)];
        numLB3.font = [UIFont systemFontOfSize:titleFont];
        numLB3.textColor = RGBA(189, 24, 41, 1);
        numLB3.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:numLB3];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:messageView];
        
        self.goDetailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.goDetailBtn.frame = CGRectMake(0, 0, WIDTH - 85, WidthRate(200));
        [self.contentView addSubview:self.goDetailBtn];
    }
}

- (void)addLineToPoint:(CGPoint)point
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(point.x, point.y, 1, HeightRate(60));
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor whiteColor].CGColor,
                       (id)[UIColor lightGrayColor].CGColor,
                       (id)[UIColor whiteColor].CGColor,nil];
    [self.contentView.layer insertSublayer:gradient atIndex:0];
}

- (void)setContentTitle:(NSString *)title unitPrice:(NSString *)price percent:(CGFloat)percent
{
    goodsTitleLB.text = title;
    
    NSMutableAttributedString *noteStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.2lf",[price doubleValue]]];
    NSRange litteRange = NSMakeRange([[noteStr1 string] rangeOfString:@"."].location,3);
    [noteStr1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:titleFont] range:NSMakeRange(0, 1)];
    [noteStr1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:titleFont] range:litteRange];
    numLB1.attributedText = noteStr1;
    NSString *inputPriceString = [NSString stringWithFormat:@"￥%.2lf",percent];
    NSMutableAttributedString *noteStr3 = [[NSMutableAttributedString alloc] initWithString:inputPriceString];
    NSRange litteRange3 = NSMakeRange(1,[[noteStr3 string] rangeOfString:@"."].location);
    [noteStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:titleFont] range:NSMakeRange(0, 1)];
    [noteStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:HeightRate(33)] range:litteRange3];
    numLB3.attributedText = noteStr3;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
