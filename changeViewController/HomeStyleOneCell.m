//
//  HomeStyleOneCell.m
//  changeViewController
//
//  Created by pmit on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "HomeStyleOneCell.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define kCellWidth (WIDTH - 20)


@implementation HomeStyleOneCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.upBigIV)
    {
        self.upBigIV = [self buildStyleOneFirstWithFrame:CGRectMake(5, 5, kCellWidth * 0.46, kCellWidth * 0.465)];
        [self.contentView addSubview:self.upBigIV];
    
        UIView *middleView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.46 + 8, 5, kCellWidth * 0.3 + 5, kCellWidth * 0.465) isMiddle:YES AndIsUp:NO];
        [self.contentView addSubview:middleView];
        self.upMiddleView = middleView;
        
        UIView *upSmallView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.77 + 12, 5, kCellWidth * 0.23, kCellWidth * 0.16) isMiddle:NO AndIsUp:YES];
        [self.contentView addSubview:upSmallView];
        self.upSmallView = upSmallView;
        
        UIView *downSmallView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.77 + 12, kCellWidth * 0.23 + 7, kCellWidth * 0.23, kCellWidth * 0.16) isMiddle:NO AndIsUp:NO];
        [self.contentView addSubview:downSmallView];
        self.downSmallView = downSmallView;
        
        UIView *downLeftView = [self buildStyleOneThreeWithFrame:CGRectMake(5, kCellWidth * 0.48 + 5, (kCellWidth + 5) * 0.5, kCellWidth * 0.25) AndDirect:NO];
        [self.contentView addSubview:downLeftView];
        self.downLeftView = downLeftView;
        
        UIView *donwRightView = [self buildStyleOneThreeWithFrame:CGRectMake((kCellWidth + 5) * 0.5 + 10,  kCellWidth * 0.48 + 5, (kCellWidth + 5) * 0.5 , kCellWidth * 0.25) AndDirect:YES];
        [self.contentView addSubview:donwRightView];
        self.downRightView = donwRightView;
    }
}

- (UIImageView *)buildStyleOneFirstWithFrame:(CGRect)frame
{
    self.upBigIV = [[UIImageView alloc] initWithFrame:frame];
    self.upBigIV.contentMode = UIViewContentModeScaleAspectFit;
    UIView *clearView = [[UIView alloc] initWithFrame:CGRectMake(0, self.upBigIV.bounds.size.height * 0.85, self.upBigIV.bounds.size.width, self.upBigIV.bounds.size.height * 0.15)];
    clearView.backgroundColor = [UIColor clearColor];
    [self.upBigIV addSubview:clearView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, clearView.bounds.size.width, clearView.bounds.size.height)];
    alphaView.backgroundColor = [UIColor whiteColor];
    alphaView.alpha = 0.7f;
    [clearView addSubview:alphaView];
    
    self.upBigTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, alphaView.bounds.size.width * 0.7, alphaView.bounds.size.height)];
    self.upBigTitleLB.lineBreakMode = NSLineBreakByClipping;
    self.upBigTitleLB.font = [UIFont systemFontOfSize:HeightRate(26)];
    self.upBigTitleLB.textColor = kTextColor;
    [clearView addSubview:self.upBigTitleLB];
    
    self.upBigPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(alphaView.bounds.size.width * 0.7, 0, alphaView.bounds.size.width * 0.3 - 4, alphaView.bounds.size.height)];
    self.upBigPriceLB.textColor = kTextColor;
    self.upBigPriceLB.font = [UIFont boldSystemFontOfSize:13.0f];
    self.upBigPriceLB.textAlignment = NSTextAlignmentRight;
    [clearView addSubview:self.upBigPriceLB];
    
    return self.upBigIV;
}

- (UIView *)buildStyleOneSecondWithFrame:(CGRect)frame isMiddle:(BOOL)isMiddle AndIsUp:(BOOL)isUp
{
    UIView *bgWhiteView = [[UIView alloc] initWithFrame:frame];
    bgWhiteView.backgroundColor = [UIColor whiteColor];
    UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, bgWhiteView.bounds.size.width, bgWhiteView.bounds.size.width)];
    [bgWhiteView addSubview:productIV];
    if (isMiddle)
    {
        self.upMiddleIV = productIV;
        self.upMiddleTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(2, productIV.frame.origin.y + productIV.frame.size.height + (kCellWidth * 0.02), bgWhiteView.bounds.size.width - 4, 20)];
        self.upMiddleTitleLB.textColor = RGBA(51, 51, 51, 1);
        self.upMiddleTitleLB.textAlignment = NSTextAlignmentRight;
        self.upMiddleTitleLB.lineBreakMode = NSLineBreakByClipping;
        self.upMiddleTitleLB.font = [UIFont systemFontOfSize:WidthRate(24)];
        [bgWhiteView addSubview:self.upMiddleTitleLB];
        self.upMiddlePriceLB = [[UILabel alloc] initWithFrame:CGRectMake(2, self.upMiddleTitleLB.frame.origin.y + 20, bgWhiteView.bounds.size.width - 4, 20)];
        self.upMiddlePriceLB.textColor = kTextColor;
        self.upMiddlePriceLB.font = [UIFont boldSystemFontOfSize:13.0f];
        self.upMiddlePriceLB.textAlignment = NSTextAlignmentRight;
        [bgWhiteView addSubview:self.upMiddlePriceLB];
    }
    else
    {
        
        productIV.frame = CGRectMake(0, 0, bgWhiteView.bounds.size.width, bgWhiteView.bounds.size.width);
        UIView *shadeView = [[UIView alloc] initWithFrame:CGRectMake(0, productIV.bounds.size.height * 0.8, productIV.bounds.size.width, productIV.bounds.size.height * 0.2)];
        shadeView.backgroundColor = kShadeColor;
        UILabel *smallTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,shadeView.bounds.size.width , shadeView.bounds.size.height)];
        smallTitleLB.textColor = kTextColor;
        smallTitleLB.lineBreakMode = NSLineBreakByClipping;
        smallTitleLB.font = [UIFont systemFontOfSize:WidthRate(18)];
        [shadeView addSubview:smallTitleLB];
        [productIV addSubview:shadeView];
        if (isUp)
        {
            self.upSmallIV = productIV;
            self.upSmallTitleLB = smallTitleLB;
            self.upSmallTitleLB.textAlignment = NSTextAlignmentCenter;

        }
        else
        {
            self.downSmallIV = productIV;
            self.downSmallTitleLB = smallTitleLB;
            self.downSmallTitleLB.textAlignment = NSTextAlignmentCenter;
        }
    }

    return bgWhiteView;
}

- (UIView *)buildStyleOneThreeWithFrame:(CGRect)frame AndDirect:(BOOL)isRight
{
    UIView *bgWhiteView = [[UIView alloc] initWithFrame:frame];
    bgWhiteView.backgroundColor = [UIColor whiteColor];
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(5, bgWhiteView.bounds.size.height * 0.3 - 3, bgWhiteView.bounds.size.width * 0.5 - 10, 20)];
    titleLB.lineBreakMode = NSLineBreakByClipping;
    [bgWhiteView addSubview:titleLB];
    titleLB.font = [UIFont systemFontOfSize:WidthRate(24)];
    titleLB.textColor = kTextColor;
    UILabel *priceLB = [[UILabel alloc] initWithFrame:CGRectMake(5, titleLB.frame.origin.y + 20, bgWhiteView.bounds.size.width * 0.5 - 10, 20)];
    priceLB.font = [UIFont boldSystemFontOfSize:13.0f];
    priceLB.textColor = kTextColor;
    [bgWhiteView addSubview:priceLB];
    UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(bgWhiteView.bounds.size.width * 0.5 + 5, 0, bgWhiteView.bounds.size.width * 0.5 - 5, bgWhiteView.bounds.size.height)];
    [bgWhiteView addSubview:productIV];
    
    if (isRight)
    {
        self.downRightIV = productIV;
        self.downRightPriceLB = priceLB;
        self.downRightTitleLB = titleLB;
    }
    else
    {
        self.downLeftIV = productIV;
        self.downLeftPriceLB = priceLB;
        self.downLeftTitleLB = titleLB;
    }
    
    return bgWhiteView;
}

- (void)setBigIV:(NSString *)bigUrlString AndMiddleIV:(NSString *)middleString AndSmallOneIV:(NSString *)smallOneString AndSmallTwo:(NSString *)SmallTowString AndDownLeft:(NSString *)downLeftString AndDownRight:(NSString *)downRightString AndbigTitle:(NSString *)bigTitle AndmiddleTitle:(NSString *)middleTitle AndSmallOneTitle:(NSString *)smallOneTitle AndSmallTwoTitle:(NSString *)smallTwoTitle AndDownLeftTitle:(NSString *)downLeftTitle AndDownRightTitle:(NSString *)downRightTitle AndBigPrice:(NSString *)bigPrice AndMiddlePrice:(NSString *)middlePrice AndDwonLeftPrice:(NSString *)downLeftPrice AndDownRightPrice:(NSString *)downRightPrice
{
    [self.upBigIV sd_setImageWithURL:[NSURL URLWithString:bigUrlString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upMiddleIV sd_setImageWithURL:[NSURL URLWithString:middleString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upSmallIV sd_setImageWithURL:[NSURL URLWithString:smallOneString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.downSmallIV sd_setImageWithURL:[NSURL URLWithString:SmallTowString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.downLeftIV sd_setImageWithURL:[NSURL URLWithString:downLeftString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.downRightIV sd_setImageWithURL:[NSURL URLWithString:downRightString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    
    if (bigTitle.length > 9) {
        bigTitle = [bigTitle substringWithRange:NSMakeRange(0, 9)];
    }
    
    if (smallOneTitle.length > 8) {
        smallOneTitle = [smallOneTitle substringWithRange:NSMakeRange(0, 8)];
    }
    
    if (smallTwoTitle.length > 8)
    {
        smallTwoTitle = [smallTwoTitle substringWithRange:NSMakeRange(0, 8)];
    }
    
    if (downLeftTitle.length > 6) {
        downLeftTitle = [downLeftTitle substringWithRange:NSMakeRange(0, 6)];
    }
    
    if (downRightTitle.length > 6)
    {
        downRightTitle = [downRightTitle substringWithRange:NSMakeRange(0, 6)];
    }
    
    CGSize upbgSize = [bigTitle boundingRectWithSize:CGSizeMake(MAXFLOAT, self.upBigTitleLB.bounds.size.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:HeightRate(26)]} context:nil].size;
    self.upBigTitleLB.frame = CGRectMake(self.upBigTitleLB.frame.origin.x, self.upBigTitleLB.frame.origin.y, upbgSize.width, self.upBigTitleLB.bounds.size.height);
    
    self.upBigTitleLB.text = bigTitle;
    self.upBigTitleLB.lineBreakMode = NSLineBreakByClipping;
    self.upMiddleTitleLB.text = middleTitle;
    self.upMiddleTitleLB.lineBreakMode = NSLineBreakByClipping;
    self.upSmallTitleLB.text = smallOneTitle;
    self.upSmallTitleLB.lineBreakMode = NSLineBreakByClipping;
    self.downSmallTitleLB.text = smallTwoTitle;
    self.downSmallTitleLB.lineBreakMode = NSLineBreakByClipping;
    self.downLeftTitleLB.text = downLeftTitle;
    self.downLeftTitleLB.lineBreakMode = NSLineBreakByClipping;
    self.downRightTitleLB.text = downRightTitle;
    self.downRightTitleLB.lineBreakMode = NSLineBreakByClipping;
    
    self.upBigPriceLB.frame = CGRectMake(CGRectGetMaxX(self.upBigTitleLB.frame), self.upBigPriceLB.frame.origin.y, self.upBigIV.bounds.size.width - CGRectGetMaxX(self.upBigTitleLB.frame), self.upBigPriceLB.bounds.size.height);
    self.upBigPriceLB.text = [NSString stringWithFormat:@"%@",bigPrice];
    self.upMiddlePriceLB.text = [NSString stringWithFormat:@"%@",middlePrice];
    self.downLeftPriceLB.text = [NSString stringWithFormat:@"%@",downLeftPrice];
    self.downRightPriceLB.text = [NSString stringWithFormat:@"%@",downRightPrice];
}

@end
