//
//  CityDelegate.h
//  changeViewController
//
//  Created by pmit on 15/9/15.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CityDelegateDelegate <NSObject>

- (void)successGetNextData:(NSArray *)areaArr CityId:(NSString *)cityId CityName:(NSString *)cityName;
- (void)noNextData:(NSString *)cityId CityName:(NSString *)cityName;

@end

@interface CityDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *cityArr;
@property (weak,nonatomic) id<CityDelegateDelegate> cityDelegateDelegate;

@end
