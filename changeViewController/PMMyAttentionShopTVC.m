//
//  PMMyAttentionShopTVC.m
//  changeViewController
//
//  Created by pmit on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMMyAttentionShopTVC.h"
#import "MyAttentionViewController.h"
#import "WebViewViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PMMyGoodsViewController.h"
#import "PMToastHint.h"
#import "ZSTChatroomLoadMoreView.h"


@interface PMMyAttentionShopTVC () <UIAlertViewDelegate>

@property(nonatomic,assign)NSInteger currentPage;
@property (assign,nonatomic) BOOL isHasMore;
@property (assign,nonatomic) BOOL isLoding;
@property (strong,nonatomic) ZSTChatroomLoadMoreView *loadMoreView;

@end

static NSString *const reuseID = @"reuseCell";
static NSString *const noResultTitle = @"暂时没有关注店铺,\n快去添加吧";

@implementation PMMyAttentionShopTVC
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.view.frame = CGRectMake(0, 40, WIDTH, HEIGHT-40);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentPage = 1;
    [self.tableView registerClass:[PMAttentionShopCell class] forCellReuseIdentifier:reuseID];
    //允许多行编辑
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 0.2)];
    self.tableView.tableFooterView.backgroundColor = [UIColor lightGrayColor];
    self.tableView.backgroundColor = RGBA(244, 244, 244, 1);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getModels];
}

#pragma mark - Table view data source UITableViewCellEditingStyleDelete

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dicArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PMAttentionShopCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID forIndexPath:indexPath];
    [cell createUI];
    
    NSDictionary *dic = self.dicArray[indexPath.row];
    [cell.iv sd_setImageWithURL:[dic objectNullForKey:@"shopLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    [cell setContentShopName:[dic objectNullForKey:@"shopName"] accessoryTitle:[dic objectNullForKey:@"userName"]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightRate(170);
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    return YES;
}


//进入编辑模式，按下出现的编辑按钮后
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        [self cancelCareShop:@[[self.dicArray[indexPath.row] objectNullForKey:@"chId"]] finish:^{
            
            [self.dicArray removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:
             UITableViewRowAnimationFade];
        }];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)deleteSelectedRows
{
    NSArray *arr = [self.tableView indexPathsForSelectedRows];
    if (arr.count == 0)
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"您还没有选择要删除的店铺"];
    }
    else
    {
        NSMutableArray *shopIds = [NSMutableArray array];
    
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
        for (NSIndexPath *indexPath in arr)
        {
            [indexSet addIndex:indexPath.row];
            NSString *shopId = [self.dicArray[indexPath.row] objectNullForKey:@"chId"];
            [shopIds addObject:shopId];
        }
    
        [self cancelCareShop:shopIds finish:^{
            //删除数据源对应的数据
            [self.dicArray removeObjectsAtIndexes:indexSet];
        
            //删除行
            [self.tableView deleteRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationAutomatic];

        }];
    }
    

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.tableView.isEditing)
    {
        
    }
    else
    {
        NSDictionary *dic = self.dicArray[indexPath.row];
        
        WebViewViewController *webVC = [[WebViewViewController alloc] initWithWebType:webTypeShop];
//        WebViewViewController *webVC = [WebViewViewController defaultWebView];
//        webVC.webViewType = webTypeShop;
        webVC.newsShopId = [dic objectNullForKey:@"shopId"];
        webVC.isCare = YES;
        [self.navigationController pushViewController:webVC animated:YES];
    }

}


#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - 从服务器请求数据
- (void)getModels
{
    [[PMNetworking defaultNetworking] request:PMRequestStateCareShowCareHome WithParameters:@{@"currentPage":@(self.currentPage),@"pageSize":@(10)} callBackBlock:^(NSDictionary *dic) {
        
        MyAttentionViewController *pVC = (MyAttentionViewController *)self.parentViewController;

        if(intSuccess == 1)
        {
            self.dicArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
            
            [self.tableView reloadData];

            if (self.dicArray.count == 0)
            {
                [pVC showNoResultViewWithIndex:2];
            }
            else
            {
                pVC.noResultView.hidden = YES;
                if ([[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue] >= self.currentPage + 1) {
                    self.isHasMore = YES;
                    if (!self.loadMoreView)
                    {
                        self.loadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                        self.tableView.tableFooterView = self.loadMoreView;
                    }
                }
                else
                {
                    self.isHasMore = NO;
                }
                
            }
            
        }
        else
        {
            self.tableView.scrollEnabled = NO;
            
            [pVC showNoResultViewWithIndex:2];

            [self.dicArray removeAllObjects];
            [self.tableView reloadData];
        }
    }showIndicator:NO];
}

#pragma mark 取消关注
- (void)cancelCareShop:(NSArray *)shopIds finish:(finishAction)finish
{
    NSString *shopId = [shopIds componentsJoinedByString:@","];
    [[PMNetworking defaultNetworking] request:PMRequestStateCareCancelBatchCare WithParameters:@{@"ids":shopId} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            if(finish)
                finish();
            
            if (self.dicArray.count == 0)
            {
                MyAttentionViewController *pVC = ((MyAttentionViewController *)self.parentViewController);
                [pVC showNoResultViewWithIndex:2];
            }
        }
        else
        {
            showRequestFailAlertView
        }
    }showIndicator:NO];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.isHasMore && !self.isLoding) {
        CGFloat scrollPosition = scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y;
        if (scrollPosition < [self footerLoadMoreHeight]) {
            //                self.isAllOrderShow = NO;
            [self loadMore];
        }
    }
}

- (CGFloat)footerLoadMoreHeight
{
    if (self.loadMoreView)
        return self.loadMoreView.frame.size.height;
    else
        return 10;
}

- (BOOL) loadMore
{
    if (self.isLoding)
        return NO;
    
    [self willBeginLoadingMore];
    self.isLoding = YES;
    return YES;
}

#pragma mark - 开始加载更多
- (void)willBeginLoadingMore
{
    ZSTChatroomLoadMoreView *fv = (ZSTChatroomLoadMoreView *)self.loadMoreView;
    [fv becomLoading];
    self.currentPage++;
    [self getMoreByNet];
}

- (void)getMoreByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateCareShowCareHome WithParameters:@{@"currentPage":@(self.currentPage),@"pageSize":@(10)} callBackBlock:^(NSDictionary *dic) {
        
        if(intSuccess == 1)
        {
            NSArray *shopArr = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
            [self.dicArray addObjectsFromArray:shopArr];
            [self loadMoreCompleted];
            [self.tableView reloadData];

            if ([[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue] >= self.currentPage + 1) {
                self.isHasMore = YES;
                if (!self.loadMoreView)
                {
                    self.loadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                    self.tableView.tableFooterView = self.loadMoreView;
                }
            }
            else
            {
                self.isHasMore = NO;
                if (self.loadMoreView)
                {
                    self.tableView.tableFooterView = nil;
                }
            }
                
        }
        
    }showIndicator:NO];
}

#pragma mark - 结束加载更多
- (void) loadMoreCompleted
{
    ZSTChatroomLoadMoreView *fv = self.loadMoreView;
    self.isLoding = NO;
    [fv endLoading];
}

@end
