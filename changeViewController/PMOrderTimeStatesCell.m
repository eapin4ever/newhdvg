//
//  PMOrderTimeStatesCell.m
//  changeViewController
//
//  Created by pmit on 15/10/29.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMOrderTimeStatesCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMOrderTimeStatesCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.orderCodeLB)
    {
        self.orderCodeLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, (WIDTH - 30) / 2, 20)];
        self.orderCodeLB.textAlignment = NSTextAlignmentLeft;
        self.orderCodeLB.font = [UIFont systemFontOfSize:HeightRate(24)];
        self.orderCodeLB.textColor = RGBA(117, 116, 121, 1);
        [self.contentView addSubview:self.orderCodeLB];
        
        self.orderStatesLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.orderCodeLB.frame), 10, (WIDTH - 30) / 2, 20)];
        self.orderStatesLB.textAlignment = NSTextAlignmentRight;
        self.orderStatesLB.font = [UIFont systemFontOfSize:HeightRate(24)];
        self.orderStatesLB.textColor = RGBA(117, 116, 121, 1);
        [self.contentView addSubview:self.orderStatesLB];
        
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        line.frame = CGRectMake(0, 39.5, WIDTH, 0.5);
        [self.contentView.layer addSublayer:line];
    }
}

- (void)setCellData:(NSString *)orderCodeString State:(NSString *)orderStateString
{
    NSString *orderCode = [NSString stringWithFormat:@"下单时间：%@",orderCodeString];
    CGSize orderCodeSize = [orderCode boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:HeightRate(24)]} context:nil].size;
    self.orderCodeLB.frame = CGRectMake(self.orderCodeLB.frame.origin.x, self.orderCodeLB.frame.origin.y, orderCodeSize.width, 20);
    self.orderCodeLB.text = orderCode;
    
    self.orderStatesLB.frame = CGRectMake(CGRectGetMaxX(self.orderCodeLB.frame), 10, WIDTH - CGRectGetMaxX(self.orderCodeLB.frame) - 15 , 20);
    self.orderStatesLB.text = orderStateString;
}


@end
