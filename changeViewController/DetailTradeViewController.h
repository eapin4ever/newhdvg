//
//  DetailTradeViewController.h
//  changeViewController
//
//  Created by pmit on 15/11/3.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    
    tradeStatusWaitPay,
    tradeStatusWaitGet,
    tradeStatusReceive,
    tradeStatusTurnBack
    
}tradeStatus;

@interface DetailTradeViewController : UIViewController


@property (assign,nonatomic) tradeStatus tradeStatus;
@property (assign,nonatomic) BOOL isFromPay;

@end
