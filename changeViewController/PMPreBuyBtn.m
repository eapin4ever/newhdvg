//
//  PMPreBuyBtn.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMPreBuyBtn.h"

#define titleY 100
#define imageY 18

@implementation PMPreBuyBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UILabel *discountLB = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width/2 - 20, 245.0/2, 40, 15)];
        self.discountLB = discountLB;
        discountLB.layer.cornerRadius = 2.0;
        discountLB.clipsToBounds = NO;
        discountLB.backgroundColor = RGBA(244, 162, 163, 1);
        discountLB.textAlignment = NSTextAlignmentCenter;
        discountLB.textColor = [UIColor whiteColor];
        discountLB.font = [UIFont systemFontOfSize:12];
        [self addSubview:discountLB];
        
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        [self setTitleColor:HDVGRed forState:UIControlStateNormal];
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted
{
    
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    //图片75*75
    return CGRectMake(0, imageY, self.bounds.size.width, 75);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, titleY, self.bounds.size.width, 20);
}


@end
