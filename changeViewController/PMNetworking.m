//
//  PMNetworking.m
//  changeViewController
//
//  Created by pmit on 14/11/11.
//  Copyright (c) 2014年 wallace. All rights reserved.
//
//恒大服务器：120.24.209.109
//本地测试服务器：192.168.1.29
#ifndef ip
#define webName @"http://hdvg.me"
//#define webName @"http://192.168.1.29"
//#define webName @"http://192.168.1.3:8085"
//#define webName @"http://120.24.234.76"

#define ip(address) [NSString stringWithFormat:@"%@/api/%@",webName,address]
//#define ipHttps(address) [NSString stringWithFormat:@"https://192.168.1.29/api/%@",address]
#define ipHttps(address) [NSString stringWithFormat:@"https://hdvg.me/api/%@",address]
//#define ipHttps(address) [NSString stringWithFormat:@"http://120.24.234.76/api/%@",address]
//#define ipHttps(address) [NSString stringWithFormat:@"https://192.168.1.3:8085/wei/api/%@",address]

//#define ip(address) [NSString stringWithFormat:@"http://192.168.1.3:8080/wei/api/%@", address]
//#define ip(address) [NSString stringWithFormat:@"http://192.168.1.26:8123/api/%@", address]
//#define ip(address) [NSString stringWithFormat:@"http://192.168.1.15/api/%@", address]

#define ipwx(address) [NSString stringWithFormat:@"%@/weixin/%@",webName,address]
//#define ipwx(address) [NSString stringWithFormat:@"http://192.168.1.26:8123/weixin/%@", address]
//#define ipwx(address) [NSString stringWithFormat:@"http://192.168.1.29/wei/weixin/%@", address]

#define shopAddress(shopId) [NSString stringWithFormat:@"%@/html/store_default.html?shopId=%@", webName ,shopId]

#define shareAddress @"http://hdvg.me/html/m_details.html?"
//#define shareAddress @"http://120.24.234.76/html/m_details.html?"
//#define shareAddress @"http://192.168.1.3:8085/wei/html/m_details.html?"

#endif

#import "PMNetworking.h"
#import "PMBrowser.h"
#import "PMNormalActivityIndicator.h"
#import "YTGifActivityIndicator.h"
#import "WXApi.h"
#import "WXApiObject.h"
#import "MasterViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "PMMyPhoneInfo.h"
#import "WebViewViewController.h"
#import <AFHTTPSessionManager.h>

@interface PMNetworking()
@property(nonatomic,strong)AFHTTPRequestOperationManager *manager;
@property(nonatomic,strong)YTGifActivityIndicator *indicator;
@end


static PMNetworking *_networking;
////百度API 使用经纬度获取详细地址
//static NSString *const getLocationDetail = @"http://api.map.baidu.com/geocoder/v2/?ak=E4805d16520de693a3fe707cdc962045&location=23.123769,113.395305&output=json&pois=1";

@implementation PMNetworking

+ (PMNetworking *)defaultNetworking
{
    if(!_networking)
    {
        _networking = [[PMNetworking alloc] init];
    }
    return _networking;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.manager = [AFHTTPRequestOperationManager manager];
        self.manager.securityPolicy.allowInvalidCertificates = YES;
        //   去掉响应头的智能转换
        [self.manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
        
        _indicator = [YTGifActivityIndicator defaultIndicator];
    }
    return self;
}

+ (UIImage *)getVerifyCode
{
    //验证码请求 -- 该url是测试地址
    //NSString *urlString = [NSString stringWithFormat:@"http://www.hdvg.tv/api/getVerifyCode?verifyCodeToken=%d",arc4random()];
    NSString *urlString = [NSString stringWithFormat:@"%@%d", ip(@"getVerifyCode?verifyCodeToken="), arc4random()];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    return [UIImage imageWithData:data];
}

#pragma mark - 各种网络请求
- (void)request:(PMRequestState)requestState WithParameters:(id)parameter callBackBlock:(callBackDicBlock)callback showIndicator:(BOOL)show
{
    //根据请求的状态不同，链接不同
    NSString *requestURL;
    switch (requestState)
    {
            /*注册登录*/
        case PMRequestStateLogin:
//            requestURL = ipHttps(@"ios/login");
            requestURL = ip(@"ios/login");
            break;
            
        case PMRequestStateLogout:
            requestURL = ipHttps(@"ios/logout");
//            requestURL = ip(@"ios/logout");
            break;
            
        case PMRequestStateRegister:
//            requestURL = ip(@"ios/register");
            requestURL = ipHttps(@"ios/register");
            break;
            
        case PMRequestStateCheckClientUserName:
//            requestURL = ip(@"ios/checkUserName");
            requestURL = ipHttps(@"ios/checkUserName");
            break;
            
        case PMRequestStateGetRememberMe:
            requestURL = ip(@"getRememberMe");
            break;
            
            //发送手机验证码
        case PMRequestStateSendSMS:
//            requestURL = ipHttps(@"sendSMS");
            requestURL = ip(@"sendSMS");
            break;
            
            //验证手机是否注册过申请代言并且获取短信验证码
        case PMRequestStateGetMobileCode:
            requestURL = ip(@"client/getMobileCode");
            break;
            
            //验证短信验证码是否正确
        case PMRequestStateCheckValidCode:
            requestURL = ip(@"check/validCode");
            break;
            
            //获取代言商信息（申请代言）
        case PMRequestStateGetApplyReSeller:
            requestURL = ip(@"share/getApplyReSeller");
            break;
            
            //申请代言 (最终提交申请审核全部数据)dd
        case PMRequestStateApplyReSeller:
            requestURL = ip(@"ios/share/applyReSeller");
            break;
            
            
            //忘记密码
        case PMRequestStateGetUserInfo:
//            requestURL = ipHttps(@"ios/getUserInfo");
            requestURL = ip(@"ios/getUserInfo");
            break;
            //忘记密码发送邮箱验证
        case PMRequestStateSendMail:
            requestURL = ip(@"sendMail");
            break;
            //设置新密码
        case PMRequestStateSetPassword:
//            requestURL = ipHttps(@"reset/pwd");
            requestURL = ip(@"reset/pwd");
            break;
            
            //我的资料
        case PMRequestStateMyInfo:
            requestURL = ip(@"my/info");
            break;
            //修改我的资料
        case PMRequestStateMyUpdate:
            requestURL = ip(@"my/update");
            break;
            
            //收货地址
        case PMRequestStateMyGetAddress:
            requestURL = ip(@"my/get/address");
            break;
            //新增收货地址
        case PMRequestStateMyAddAddress:
            requestURL = ip(@"my/add/address");
            break;
            //更新收货地址
        case PMRequestStateMyUpdateAddress:
            requestURL = ip(@"my/update/address");
            break;
            //删除收货地址
        case PMRequestStateMyDelAddress:
            requestURL = ip(@"my/del/address");
            break;
            //设置默认地址
        case PMRequestStateSetDefaultAddress:
            requestURL = ip(@"set/default/address");
            break;
            
            //账号安全
        case PMRequestStateSafeInfo:
            requestURL = ip(@"account/safe/info");
            break;
            //绑定邮箱
        case PMRequestStateSafeUpdateEmail:
            requestURL = ip(@"safe/updateEmail");
            break;
            //绑定手机
        case PMRequestStateSafeUpdateMobile:
            requestURL = ip(@"safe/updateMobile");
            break;
            //修改密码
        case PMRequestStateSafeModifyPassword:
            requestURL = ip(@"safe/modifyPwd");
            break;
            
            /*订单确认*/
            //显示订单（从购物车进入）
        case PMRequestStateShowOrder:
            requestURL = ip(@"confirmOrder/showOrder");
            break;
            //显示订单（商品详情立即购买进入）
        case PMRequestStateShowOrderOne:
            requestURL = ip(@"confirmOrder/showOrderOne");
            break;
            //提交订单
        case PMRequestStateSubmitOrder:
            requestURL = ip(@"confirmOrder/submitOrder");
            break;
            //立即支付
        case PMRequestStateAlipay:
            requestURL = ip(@"ios/alipay/pay");
            break;
            
            /*我的交易*/
            //我的订单
        case PMRequestStateShopOrderShowMyOrderList:
            requestURL = ip(@"shopOrder/showMyOrderList");
            break;
            //取消订单
        case PMRequestStateUpdateOrderStatus:
            requestURL = ip(@"order/updateOrderStatus");
            break;
            //查看物流
        case PMRequestStateShopOrderLogistics:
            requestURL = ip(@"shopOrder/logistics");
            break;
            //确认收货验证检查
        case PMRequestStateCheckIsGetProduct:
            requestURL = ip(@"shopProduct/checkIsGetProduct");
            break;
            //确认收货
        case PMRequestStateSureGetProduct:
            requestURL = ip(@"shopProduct/sureGetProduct");
            break;
            
            /*退换货管理*/
            //退换货验证检查
        case PMRequestStateCheckReturnBack:
            requestURL = ip(@"shopProduct/checkReturnBack");
            break;
            //退换货
        case PMRequestStateShowReturn:
            requestURL = ip(@"return/showReturn");
            break;
            
            /*商品评价*/
            //获取商品评价信息
        case PMRequestStateShowMyOrderProductDiscussList:
            requestURL = ip(@"shopOrder/showMyOrderProductDiscussList");
            break;
            //评价商品信息
        case PMRequestStateAddOrderDiscuss:
            requestURL = ip(@"orderDiscuss/addOrderDiscuss");
            break;
            //申请退换货
        case PMRequestStateApplyReturn:
            requestURL = ip(@"return/applyReturn");
            break;
            
            /*代言店铺*/
            //检查是否已经绑定银行卡
        case PMRequestStateShareCheckBank:
            requestURL = ip(@"share/checkBank");
            break;
            //绑定银行卡/解绑银行卡
        case PMRequestStateShareUpdateBank:
            requestURL = ip(@"share/updateBank");
            break;
            //我的客户
        case PMRequestStateShareShowMyClient:
            requestURL = ip(@"share/showMyClient");
            break;
            //代言获利
        case PMRequestStateShareShowMySell:
            requestURL = ip(@"share/showMySell");
            break;
            //获利详情
        case PMRequestStateShareShowProfit:
            requestURL = ip(@"share/showProfit");
            break;
            //提现申请
        case PMRequestStateShareApplyGetMoney:
            requestURL = ip(@"share/applyGetMoney");
            break;
            //提现明细
        case PMRequestStateShareShowGetMoney:
            requestURL = ip(@"share/showGetMoney");
            break;
            //取消/删除提现明细
        case PMRequestStateShareCancelGetMoney:
            requestURL = ip(@"share/cancelGetMoney");
            break;
            
            //获取店铺信息
        case PMRequestStateShareShowShopHome:
            requestURL = ip(@"share/showShopHome");
            break;
            //设置店铺信息
        case PMRequestStateShareUpdateShopHome:
            requestURL = ip(@"share/updateShopHome");
            break;
            
            /*使用百度api根据经纬度得到地址名称*/
//        case PMRequestStateGetLocationDetail:
//            requestURL = getLocationDetail;
//            break;
            
            /*商品*/
        case PMRequestStateProductIndex:
            requestURL = ip(@"ios/product/index_new");
//            requestURL = ip(@"ios/product/index");
            break;
            //搜索
        case PMRequestStateProductSearch:
            requestURL = ip(@"ios/product/search");
            break;
            
            /*购物车*/
        case PMRequestStateShopCartAddTo:
            requestURL = ip(@"shopCart/addTo");
            break;
        case PMRequestStateSafeShopCartShowBuyCart:
            requestURL = ip(@"shopCart/showBuyCart");
            break;
        case PMRequestStateSafeShopCartModifyCount:
            requestURL = ip(@"shopCart/modifyCount");
            break;
        case PMRequestStateSafeShopCartDelProduct:
            requestURL = ip(@"shopCart/delProduct");
            break;
        case PMRequestStateSafeShopCartDelBatchProduct:
            requestURL = ip(@"shopCart/delBatchProduct");
            break;
            
            /*我的关注*/
            //关注的商品
        case PMRequestStateCareShowCareProduct:
            requestURL = ip(@"care/showCareProduct");
            break;
            //关注的店铺
        case PMRequestStateCareShowCareHome:
            requestURL = ip(@"care/showCareHome");
            break;
            //取消关注商品
        case PMRequestStateCareCancelCareProduct:
            requestURL = ip(@"care/cancelCareProduct");
            break;
            //批量取消关注商品
        case PMRequestStateCareCancelBatchCareProduct:
            requestURL = ip(@"care/cancelBatchCareProduct");
            break;
             //批量取消关注店铺
        case PMRequestStateCareCancelBatchCare:
            requestURL = ip(@"care/cancelBatchCare");
            break;
            
            
            /*图片上传*/
        case PMRequestStateUploadImg:
            requestURL = ip(@"ios/share/uploadImg");
            break;
            
            /*意见反馈*/
        case PMRequestStateUserFeedBack:
            requestURL = ip(@"ios/user/feedBack");
            break;
            
            /*商品详情*/
        case PMRequestStateProductInfo:
            requestURL = ip(@"ios/product/info");
            break;
            
            //商品评价
        case PMRequestStateShopShowShopDisucss:
            requestURL = ip(@"shop/showShopDisucss");
            break;
            
        case PMRequestStateCheckSysVersion:
            requestURL = ip(@"checkSysVersion");
            break;
            
            //分类
        case PMRequestStateSelectClass:
            requestURL = ip(@"class/selectClass");
            break;
        case PMRequestStateSelectPreAndChildClass:
            requestURL = ip(@"class/selectPreAndChildClass");
            break;
            
            //智能搜
        case PMRequestStateSearchProductInCountInfo:
            requestURL = ip(@"product/searchProductInCountInfo");
            break;
            
            //+我的代言
        case PMRequestStateShareAddToShop:
            requestURL = ip(@"share/addToShop");
            break;
            //关注商品
        case PMRequestStateCareAddCareProduct:
            requestURL = ip(@"care/addCareProduct");
            break;
            //取消关注商品
        case PMRequestStateCareDeleteCareProduct:
            requestURL = ip(@"care/deleteCareProduct");
            break;
            //关注店铺
        case PMRequestStateCareAddCareHome:
            requestURL = ip(@"care/addCareHome");
            break;
            
            //代言的商品
        case PMRequestStateShareSelectAllShopProduct:
            requestURL = ip(@"share/selectAllShopProduct");
            break;
            //显示代言中的商品/已下架的商品
        case PMRequestStateShareShowSellsProducts:
            requestURL = ip(@"share/showSellsProducts");
            break;
            //上架下架商品
        case PMRequestStateShareUpdateBatchDown:
            requestURL = ip(@"share/updateBatchDown");
            break;
            //显示商品库
        case PMRequestStateShareShowProductStoreInfo:
            requestURL = ip(@"share/showProductStoreInfo");
            break;
            //验证购买的活动或折扣商品数量
        case PMRequestStateCheckLimitBuyNum:
            requestURL = ip(@"checkLimitBuyNum");
            break;
            //检查商品库存
        case PMRequestStateCheckStockTotal:
            requestURL = ip(@"product/checkStockTotal");
            break;
            
            //是否登录
        case PMRequestStateIsLogin:
            requestURL = ip(@"isLogin");
            break;
            
            //获取商品库存和价格等
        case PMRequestStateProductCalcStockProduct:
            requestURL = ip(@"product/calcStockProduct");
            break;
            
        case PMRequestStateGetPushLog:
            requestURL = ip(@"ios/bindBaiduyunpush");
            break;
        
        case PMRequestStateGetWXPay:
            requestURL = ipwx(@"ios/wxpayv3");
            break;
            
        case PMRequestStateGetOrderByOrderCode:
            requestURL = ip(@"order/getOrderInfo");
            
            break;
            
        case PMRequestStateGetOptTradeDetail:
            requestURL = ip(@"optTradeDetail/getOptTradeDetail");
            break;
            
        case PMRequestStateGetPushMessageList:
            requestURL = ip(@"pushMessage/getPushMessageList");
            break;
            
        case PMRequestStateShowZtList:
            requestURL = ip(@"ios/showZtList");
            break;
            
            //预售商品
        case PMRequestStateList:
            requestURL = ip(@"presell/list");
            break;
            
        case PMRequestStateGetPushMessageDetail:
            requestURL = ip(@"");
            break;
            
        case PMRequestStateZKProductList:
            requestURL = ip(@"");
            break;
        
        case PMRequestStateYSProductInfo:
            requestURL = ip(@"ios/product/ys/info");
            break;
        
        //折扣商品详情
        case PMRequestStateZKProductInfo:
            requestURL = ip(@"ios/product/zk/info");
            break;
        
        case PMRequestStateGetBan:
            requestURL = @"http://192.168.1.8/banner";
            break;
            
        case PMRequestStateGetCarrige:
            requestURL = ip(@"confirmOrder/getCarriage");
            break;
        
        case PMRequestStateGetIsUseRed:
            requestURL = ip(@"confirmOrder/getRedPackAndOrderMoney");
            break;
        
        case PMRequestStateGetAdvertising:
            requestURL = ip(@"ios/advertising/list");
            break;
        
        case PMRequestStateShowReturnDetail:
            requestURL = ip(@"return/showReturnDetail");
            break;
            
        case PMRequestStateShowBackAddress:
            requestURL = ip(@"return/backAddr");
            break;
            
        case PMRequestStatesCancelApply:
            requestURL = ip(@"return/cancelApply");
            break;
        
        case PMRequestStateUpdateLogistics:
            requestURL = ip(@"return/updateLogistics");
            break;
        
        case PMRequestStateLogCompany:
            requestURL = ip(@"logCompany");
            break;
            
        case PMRequestStateCheckOrderIsPay:
            requestURL = ip(@"order/checkOrderIsPay");
            break;
            
        case PMRequestStateGetShopFile:
            requestURL = ip(@"ios/getShopFile"); 
            break;
            
        case PMRequestStateUpdateShopHome:
            requestURL = ip(@"share/updateShopHome");
            break;
        
        case PMRequestStateCheckIndexVersion:
            requestURL = ip(@"ios/getIndexVersion");
            break;
            
        case PMRequestStateSearchKey:
            requestURL = ip(@"searchKeywords");
            break;
            
        case PMRequestStateClassGetAllClassByTree:
            requestURL = ip(@"class/getAllClassByTree");
            break;
        
        case PMRequestStateDeleteHomeProduct:
            requestURL = ip(@"share/deleteHomeProduct");
            break;
        
        case PMRequestStateDoShopByCard:
            requestURL = ip(@"ios/sysShopCard/doShopByCardNum");
            break;
            
        case PMRequestStateCheckOrderStatus:
            requestURL = ip(@"ios/validateProduct");
            break;
            
        case PMRequestStateGetProvinceData:
            requestURL = ip(@"confirmOrder/selectProvince");
            break;
        
        case PMRequestStateGetCityData:
            requestURL = ip(@"confirmOrder/selectChildren");
            break;
        
        case PMRequestStateCancelBank:
            requestURL = ip(@"share/cancelBank");
            break;
            
        case PMRequestStateShowMyLogistics:
            requestURL = ip(@"share/showMyLogistics");
            break;
            
    }
    
    //如果需要显示gif才显示
    if(show)
    {
        UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
        [window addSubview:_indicator];
        [_indicator start];
        window.userInteractionEnabled = NO;
    }
    
    @try {
        __block NSDictionary *dic;
        [self.manager GET:requestURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"请求成功");
            
            //转换成JSON格式--》字典
            if (responseObject)
            {
                dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                
                NSLog(@"operation:%@",operation);
                NSLog(@"dic ==> %@",dic);
                
                //用block返回字典
                callback(dic);
                
                if(show)
                    [_indicator stop];
                
                AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
                BOOL isConnect = [app isNetConnect];
                if (isConnect)
                {
                    [[MasterViewController defaultMasterVC] hideNoNetView];
                }
            }
            else
            {
                //            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"抱歉,与服务哥哥暂时失去联系,稍后请重新尝试" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                //            [alertView show];
            }
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            BOOL isConnect = [app isNetConnect];
            if (!isConnect)
            {
                [[MasterViewController defaultMasterVC] showNoNetView];
            }
            else
            {
                [[MasterViewController defaultMasterVC] hideNoNetView];
            }
            
            
            self.retry = ^{
                [_networking request:requestState WithParameters:parameter callBackBlock:callback showIndicator:show];
            };
            
            
            NSLog(@"请求失败");
            
            dic = @{@"message":@"与服务器哥哥的沟通出现问题了，可以尝试联系客服哟"};
            
            NSLog(@"operation:%@",operation);
            
            NSLog(@"error ==> %@",error);
            //用block返回字典
            //        callback(dic);
            
            if(show)
                [_indicator stop];
        }];

    }
    @catch (NSException *exception) {
        
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        app.crashView.hidden = NO;
        [app.window bringSubviewToFront:app.crashView];
        
    }
}

//图片上传
- (void)uploadImage:(NSDictionary *)params imageData:(NSData *)imageData success:(callBackDicBlock)successAction
{
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.responseSerializer.acceptableContentTypes = [self.manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    //直接拼服务器需要的参数
    NSMutableString *urlString = [ip(@"ios/share/uploadImg?") mutableCopy];
    [urlString appendFormat:@"PM_SID=%@&flag=%@",[params objectNullForKey:@"PM_SID"],[params objectNullForKey:@"flag"]];
    
    //parameters传nil，跟GET不一样
    [self.manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"file" fileName:@"potrait.png" mimeType:@"image/png"];
        
        
    } success:^(AFHTTPRequestOperation *operation,id responseObject) {
        NSLog(@"operation: %@",operation);
        
        //转换成JSON格式--》字典
        if (responseObject)
        {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            
            NSLog(@"operation:%@",operation);
            //
            NSLog(@"dic ==> %@",dic);
            //用block返回字典
            if(successAction)
                successAction(dic);

        }
        
    } failure:^(AFHTTPRequestOperation *operation,NSError *error) {
        NSLog(@"operation: %@",operation);
        NSLog(@"Error: %@", error);
    }];
}

//跳转支付宝手机网页支付
- (void)alipay:(NSDictionary *)params target:(id)target Index:(NSInteger)index
{
    NSMutableString *address = [NSMutableString stringWithString:ip(@"android/alipay/pay?")];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if(![address hasSuffix:@"?"])
            [address appendString:@"&"];
        [address appendString:[NSString stringWithFormat:@"%@=%@",key,obj]];
    }];
    NSString *urlString = [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"address == %@",urlString);
    
    WebViewViewController *webView;
    if (index == 1) {
        webView = [[WebViewViewController alloc] initWithWebType:webTypeJoin];
    }
    else {
        webView = [[WebViewViewController alloc] initWithWebType:webTypePay];
    }
    
    webView.urlString = urlString;

    [((UIViewController *)target).navigationController pushViewController:webView animated:YES];
}

//使用web跳转进入店铺h5页面
- (void)getInShopWithShopId:(NSString *)shopId target:(id)target
{
    PMBrowser *browser = [PMBrowser defaultWebBrowserWithController:target];
    NSLog(@"%@",shopAddress(shopId));
    
    [browser getInShop:shopAddress(shopId) ByPushController:target];
}

#pragma mark - 代言分享商品
- (NSString *)shareShopProductWithProductId:(NSString *)productId shopId:(NSString *)shopId
{
    NSMutableString *urlString = [shareAddress mutableCopy];
    [urlString appendFormat:@"productId=%@&shopId=%@",productId,shopId];
    NSLog(@"%@",urlString);
    return urlString;
}

#pragma mark - post网络请求
#pragma mark - 各种网络请求
- (void)requestWithPost:(PMRequestState)requestState WithParameters:(id)parameter callBackBlock:(callBackDicBlock)callback showIndicator:(BOOL)show
{
    NSString *urlString = @"";
    switch (requestState) {
        case PMRequestStateGetPushMessageList:
            urlString = ip(@"pushMessage/getPushMessageDetail");
            break;
        case PMRequestStateLogin:
//            urlString = ipHttps(@"ios/login");
            urlString = ip(@"ios/login");
        default:
            break;
    }
    
    if(show)
    {
        UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
        [window addSubview:_indicator];
        [_indicator start];
    }
    
     __block NSDictionary *dic;
    [self.manager POST:urlString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (responseObject)
        {
            dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            
            NSLog(@"operation:%@",operation);
            
            NSLog(@"dic ==> %@",dic);
            //用block返回字典
            callback(dic);
            
            if(show)
                [_indicator stop];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"抱歉,与服务哥哥暂时失去联系,稍后请重新尝试" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        BOOL isConnect = [app isNetConnect];
        if (!isConnect)
        {
            app.noNetView.hidden = NO;
        }
        
        
        self.retry = ^{
            [_networking request:requestState WithParameters:parameter callBackBlock:callback showIndicator:show];
        };
        
        
        NSLog(@"请求失败");
        
        dic = @{@"message":@"与服务器哥哥的沟通出现问题了，可以尝试联系客服哟"};
        
        NSLog(@"operation:%@",operation);
        
        NSLog(@"error ==> %@",error);
        //用block返回字典
        callback(dic);
        
        if(show)
            [_indicator stop];
        
        
    }];
    
}




@end
