//
//  PMMyGoodsViewController.h
//  changeViewController
//
//  Created by pmit on 15/6/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

typedef void (^finishAction)(void);

#import <UIKit/UIKit.h>

@interface PMMyGoodsViewController : UIViewController

@property (strong,nonatomic) UITableView *detailTableView;
@property (copy,nonatomic) NSString *productId;
@property (copy,nonatomic) NSString *shopId;
@property (assign,nonatomic) BOOL isPrePay; //是否是预购进来的
@property (assign,nonatomic) BOOL isDiscount; //是否是折扣进来

- (instancetype)initWIthGoodsData:(NSDictionary *)goodsData;


@end
