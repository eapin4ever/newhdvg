//
//  PMFilterSideView.h
//  testTableView
//
//  Created by pmit on 14/12/10.
//  Copyright (c) 2014年 pmit. All rights reserved.
//
typedef void (^finishActionBlock)();

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMSearchFilterCell.h"

@protocol PMFilterSideViewDelegate <NSObject>

- (void)filterHide;

@end

@interface PMFilterSideView : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *filterView;
@property(nonatomic,strong)NSArray *sectionTitleArray;
/*
 allDataArray = @[sectionTitleArray[section]:@[(该section的所有row)],sectionTitleArray[section]:@[(该section的所有row)]];
 
 
 */
@property(nonatomic,strong)NSDictionary *allDataDic;

//按钮
@property(nonatomic,strong)UIButton *sureBtn;
@property(nonatomic,strong)UIButton *recoverBtn;
//选定的筛选条件
@property(nonatomic,copy)NSString *filter;
@property(nonatomic,strong)NSMutableArray *selectedRowArr;
@property (weak,nonatomic) id<PMFilterSideViewDelegate> filterDelegate;


//显示/隐藏 筛选栏
- (void)showFilterSideView;
- (void)hideFilterSideViewFinish:(finishActionBlock)action;

@end
