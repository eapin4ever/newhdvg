//
//  NearSearchViewController.m
//  changeViewController
//
//  Created by pmit on 15/7/30.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "NearSearchViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMGoodsReusableView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MasterViewController.h"
#import "PMMyGoodsViewController.h"
#import "JHRefresh.h"

#define originY HEIGHT - 50
#define moveDistance WidthRate(100)

@interface NearSearchViewController () <UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong,nonatomic) UICollectionView *resultCollectionView;
@property (strong,nonatomic) NSMutableArray *dicArray;
@property (assign,nonatomic) NSInteger totalPages;
@property (strong,nonatomic) NSMutableArray *loadArr;
@property (strong,nonatomic) NSMutableDictionary *params;
@property (strong,nonatomic) UIImageView *scope;
@property (assign,nonatomic) CGRect oldFrame;
@property (assign,nonatomic) NSInteger frameType;
@property (assign,nonatomic) CGRect currentFrame;
@property (assign,nonatomic) NSTimer *aniTimer;

@end

@implementation NearSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"智能搜索";
    self.view.backgroundColor = [UIColor whiteColor];
    self.dicArray = [NSMutableArray array];
    self.loadArr = [NSMutableArray array];
    self.params = [NSMutableDictionary dictionary];
    [self buildBackIVView];
    [self buildResultView];
    self.isRefresh = YES;
    [self startSearch];
    [self addRefreshAction];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [UIView animateWithDuration:0.3 animations:^{
        [[MasterViewController defaultMasterVC].tabBar setAlpha:1];
    }];
    
    [MasterViewController defaultMasterVC].nearbySearchAgain = ^{
      
        [self searchAgain];
        
    };
}

- (void)buildBackIVView
{
    UIImageView *mapImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/1.5, WIDTH/1.5)];
    mapImage.center = CGPointMake(WIDTH/2, HEIGHT/2 - 64);
    mapImage.image = [UIImage imageNamed:@"wall"];
    mapImage.clipsToBounds = YES;
    mapImage.contentMode = UIViewContentModeScaleAspectFill;
    
    [mapImage.layer setCornerRadius:WIDTH/3];
    [mapImage.layer setBorderColor:RGBA(134, 215, 253, 1).CGColor];
    [mapImage.layer setBorderWidth:1.0];
    [self.view addSubview:mapImage];
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(300), HeightRate(400), WidthRate(120), HeightRate(120))];
    iv.image = [UIImage imageNamed:@"scope"];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    iv.alpha = 0.95;
    self.scope = iv;
    self.oldFrame = iv.frame;
    self.currentFrame = iv.frame;
    self.frameType = 0;
    [self.view addSubview:iv];
    
    self.aniTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(startAnimation) userInfo:nil repeats:YES];
}

- (void)buildResultView
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    [flowLayout setMinimumInteritemSpacing:5];
    [flowLayout setMinimumLineSpacing:HeightRate(20)];//上下间距
    [flowLayout setItemSize:CGSizeMake(WidthRate(345), WidthRate(490))];//宽度、高度
    [flowLayout setSectionInset:UIEdgeInsetsMake(HeightRate(10), WidthRate(20), 0, WidthRate(20))];
    
    self.resultCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, originY , WIDTH, HEIGHT - 64) collectionViewLayout:flowLayout];
    [self.resultCollectionView setBackgroundColor:HDVGPageBGGray];
    [self.resultCollectionView setDataSource:self];
    [self.resultCollectionView setDelegate:self];
    [self.resultCollectionView registerClass:[PMGoodsReusableView class] forCellWithReuseIdentifier:@"reuseItem"];
    
    [self.resultCollectionView setContentInset:UIEdgeInsetsMake(0, 0, 60, 0)];
    
    [self.view addSubview:self.resultCollectionView];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dicArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMGoodsReusableView *item = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseItem" forIndexPath:indexPath];
    
    NSDictionary *dic = self.dicArray[indexPath.row];
    NSString *discount;
    BOOL isHasDiscount = NO;
    
    // ---------------------------- wei ----------------------------
    // 折扣
    id discountDic = [dic objectNullForKey:@"zkMap"];
    if ([discountDic isKindOfClass:[NSDictionary class]] && !isNull(discountDic))
    {
        discount = [discountDic objectNullForKey:@"zkblShow"];
        if (discount.length != 0) {
            item.showDiscount = YES;
            isHasDiscount = YES;
        }
        else {
            item.showDiscount = NO;
            isHasDiscount = NO;
        }
    }
    else
    {
        item.showDiscount = NO;
        isHasDiscount = NO;
        discount = @"10";
    }
    // ---------------------------- wei ----------------------------
    
    item.showDistance = YES;
    
    [item createUI];
    
    [item.imageView sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    item.showDiscount = NO;
    
    [item setContentTitle:[dic objectNullForKey:@"productName"] price:isHasDiscount ? [[discountDic objectNullForKey:@"discountPrice"] doubleValue] : [[dic objectNullForKey:@"productPrice"] doubleValue] discount:discount];
    
    if (indexPath.row == self.dicArray.count - 5)
    {
        if (![self.loadArr containsObject:@(indexPath.row)])
        {
            self.isRefresh = NO;
            NSInteger currentPage = [[self.params objectNullForKey:@"currentPage"] integerValue];
            if(currentPage < self.totalPages)//如果小于总页数则刷新
            {
                //增加当前页
                currentPage++;
                //获取数据
                [self.params setValue:@(currentPage) forKey:@"currentPage"];
                [self getModelsWithParam:self.params finish:^{
                    [self.loadArr addObject:@(indexPath.row)];
                    [self.resultCollectionView reloadData];
                    
                } error:^{
                    
                }];
            }
        }
    }
    return item;
}

#pragma mark - 从服务器获得数据
- (void)getModelsWithParam:(NSDictionary *)param finish:(myFinishBlock)finish error:(myFinishBlock)error
{
    /*
     currentPage	Y	int         当前页
     pageSize       N	int         一页显示条数（默认20条）
     longitude      N	String		经度
     latitude       N	String		纬度
     */
    
    if(param == nil)
    {
        param = @{@"currentPage":@(1),@"pageSize":@(20)};
        self.params = [param mutableCopy];
    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateSearchProductInCountInfo WithParameters:self.params callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            if(_isRefresh)
            {
                //刷新数组
                self.dicArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                //得到总页数
                self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
                self.loadArr = [NSMutableArray array];
            }
            else
            {
                //添加objs
                [self.dicArray addObjectsFromArray:[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"]];
            }
            //刷新表单
            [self.resultCollectionView reloadData];
            if(finish)//如果不是nil的话，执行block
                finish();
        }
        else
        {
            if(error)
                error();
            showRequestFailAlertView;
        }
    }showIndicator:NO];
}

- (void)startSearch
{
    [self getModelsWithParam:nil finish:^{
        
        [UIView animateWithDuration:0.3f animations:^{
            [NSThread sleepForTimeInterval:0.5f];
            self.resultCollectionView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
            [self stopAnimation];
        }];
    } error:^{
        
    }];
}

- (void)startAnimation
{
    CGRect newsFrame = CGRectZero;
    switch (self.frameType)
    {
        case 0:
        {
            newsFrame = CGRectMake(self.currentFrame.origin.x + moveDistance, self.currentFrame.origin.y + moveDistance, self.oldFrame.size.width, self.oldFrame.size.height);
            self.currentFrame = newsFrame;
            self.frameType = 1;
        }
            break;
        case 1:
        {
            newsFrame = CGRectMake(self.currentFrame.origin.x - moveDistance, self.currentFrame.origin.y + moveDistance, self.oldFrame.size.width, self.oldFrame.size.height);
            self.currentFrame = newsFrame;
            self.frameType = 2;
        }
            break;
        case 2:
        {
            newsFrame = CGRectMake(self.currentFrame.origin.x - moveDistance, self.currentFrame.origin.y - moveDistance, self.oldFrame.size.width, self.oldFrame.size.height);
            self.currentFrame = newsFrame;
            self.frameType = 3;
        }
            
            break;
        case 3:
        {
            newsFrame = CGRectMake(self.currentFrame.origin.x + moveDistance, self.currentFrame.origin.y - moveDistance, self.oldFrame.size.width, self.oldFrame.size.height);
            self.currentFrame = newsFrame;
            self.frameType = 0;
        }
            break;
            
        default:
            break;
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        self.scope.frame = self.currentFrame;
    }];
}

- (void)stopAnimation
{
    [self.aniTimer setFireDate:[NSDate distantFuture]];
}

- (void)searchAgain
{
    self.isRefresh = YES;
    [self.aniTimer setFireDate:[NSDate distantPast]];
    [self hideResult];
}

- (void)hideResult
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.resultCollectionView.contentOffset = CGPointZero;
        self.resultCollectionView.frame = CGRectMake(0, originY , WIDTH, HEIGHT - 64);
        
    } completion:^(BOOL finished) {
        
        [self startSearch];
        [self.aniTimer setFireDate:[NSDate distantPast]];
        
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopAnimation];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //显示加载提示视图
    //    [self lodingViewShow:YES];
    NSDictionary *dic = self.dicArray[indexPath.row];
    //    YBGoodDetailViewController *detailVC = [[YBGoodDetailViewController alloc] init];
    PMMyGoodsViewController *detailVC = [[PMMyGoodsViewController alloc] init];
    //请求数据
    detailVC.productId = [dic objectNullForKey:@"id"];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:detailVC];
}

- (void)addRefreshAction
{
    __weak NearSearchViewController *weakSelf = self;
    
    //使用普通的下拉刷新
    [self.resultCollectionView addRefreshHeaderViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        weakSelf.isRefresh = YES;
        [weakSelf hideResult];
        [weakSelf.resultCollectionView headerEndRefreshingWithResult:JHRefreshResultSuccess];
        
    }];
}

@end
