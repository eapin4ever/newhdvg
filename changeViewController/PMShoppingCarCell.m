//
//  PMShoppingCarCell.m
//  changeViewController
//
//  Created by pmit on 15/6/27.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMShoppingCarCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMShoppingCarCell
{
    UITextField *numberTF;
    UILabel *priceLB;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.selectedBtn)
    {
        UIView *myContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 120)];
        myContentView.backgroundColor = [UIColor whiteColor];
        
        self.selectedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.selectedBtn.frame = CGRectMake(WidthRate(20), 40, WIDTH * 0.08 - WidthRate(10), 40);
        self.selectedBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.selectedBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
        [self.selectedBtn setImage:[UIImage imageNamed:@"selectAll_1"] forState:UIControlStateSelected];
        [myContentView addSubview:self.selectedBtn];
        
        self.iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(15) + WIDTH * 0.1, 10, 100, 100)];
        self.iv.contentMode = UIViewContentModeScaleAspectFit;

        [myContentView addSubview:self.iv];
        
        self.activeIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        self.activeIV.image = [UIImage imageNamed:@"activeTip"];
        self.activeIV.contentMode = UIViewContentModeScaleAspectFit;
        self.activeIV.hidden = YES;
        [self.iv addSubview:self.activeIV];
        
        
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH * 0.1 + WidthRate(35) + 100, 10, WIDTH - WIDTH * 0.1 - WidthRate(60) - 100, 40)];
        self.titleLB.backgroundColor = [UIColor clearColor];
        self.titleLB.font = [UIFont systemFontOfSize:14.0f];
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        self.titleLB.textColor = HDVGFontColor;
        self.titleLB.numberOfLines = 2;
        [myContentView addSubview:self.titleLB];
        
        self.classifyLB1 = [[UILabel alloc] initWithFrame:CGRectMake(self.titleLB.frame.origin.x, self.titleLB.frame.origin.y + self.titleLB.bounds.size.height, self.titleLB.bounds.size.width, 20)];
        self.classifyLB1.backgroundColor = [UIColor clearColor];
        self.classifyLB1.font = [UIFont systemFontOfSize: 12.0f];
        self.classifyLB1.textColor = [UIColor grayColor];
        [myContentView addSubview:self.classifyLB1];
        
        priceLB = [[UILabel alloc] initWithFrame:CGRectMake(self.classifyLB1.frame.origin.x, self.classifyLB1.frame.origin.y + self.classifyLB1.bounds.size.height, self.classifyLB1.bounds.size.width * 0.45, 40)];
        priceLB.backgroundColor = [UIColor clearColor];
        priceLB.textColor = RGBA(189, 24, 41, 1);
        priceLB.textAlignment = NSTextAlignmentLeft;
        priceLB.font = [UIFont systemFontOfSize:16.0f];
        [myContentView addSubview:priceLB];
        
        self.minusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.minusBtn.frame = CGRectMake(WIDTH - WidthRate(90) - 60, priceLB.frame.origin.y + 5, 30, 30);
        [self.minusBtn setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
        [self.minusBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.minusBtn.layer setBorderWidth:0.5];
        [self.minusBtn.layer setCornerRadius:5.0];
        [myContentView addSubview:self.minusBtn];
        
        numberTF = [[UITextField alloc] initWithFrame:CGRectMake(self.minusBtn.frame.origin.x + self.minusBtn.bounds.size.width, priceLB.frame.origin.y + 5, WidthRate(60), 30)];
        numberTF.userInteractionEnabled = NO;
        numberTF.textAlignment = NSTextAlignmentCenter;
        numberTF.font = [UIFont systemFontOfSize:HeightRate(30)];
        numberTF.textColor = [UIColor lightGrayColor];
        [myContentView addSubview:numberTF];
        
        self.plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.plusBtn.frame = CGRectMake(numberTF.frame.origin.x + numberTF.bounds.size.width, priceLB.frame.origin.y + 5, 30, 30);
        [self.plusBtn setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
        [self.plusBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.plusBtn.layer setBorderWidth:0.5];
        [self.plusBtn.layer setCornerRadius:5.0];
        [myContentView addSubview:self.plusBtn];
        
        [self.contentView addSubview:myContentView];
        
    }
}

- (void)setContentTitle:(NSString *)title classify1:(NSString *)classify1 classify2:(NSString *)classify2 distanceOrLocation:(NSString *)distance number:(NSString *)num price:(double)price
{
    
    self.titleLB.text = title;
    if((isNull(classify2) || [classify2 isEqualToString:@""])&& (!isNull(classify1) && ![classify1 isEqualToString:@""]))
        self.classifyLB1.text = [NSString stringWithFormat:@"%@",classify1];
    else if((!isNull(classify1) && ![classify1 isEqualToString:@""]) && (!isNull(classify2) && ![classify2 isEqualToString:@""]))
        self.classifyLB1.text = [NSString stringWithFormat:@"%@,%@",classify1,classify2];
    numberTF.text = num;
    NSString *priceString = [NSString stringWithFormat:@"￥%.2lf",price];
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} range:NSMakeRange(0, 1)];
    priceLB.attributedText = nodeString;
}

- (void)numberPlus:(UIButton *)sender
{
    NSInteger temp = [numberTF.text integerValue];
    if (temp < self.maxCount)
    {
        temp ++;
    }
    else
    {
        
    }
    numberTF.text = [NSString stringWithFormat:@"%ld",(long)temp];
}

- (void)numberMinus:(UIButton *)sender
{
    NSInteger temp = [numberTF.text integerValue];
    if(temp > 1)
    {
        temp --;
        numberTF.text = [NSString stringWithFormat:@"%ld",(long)temp];
    }
    else
    {
        
    }
}

@end
