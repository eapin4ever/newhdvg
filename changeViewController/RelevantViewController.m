//
//  RelevantViewController.m
//  changeViewController
//
//  Created by P&M on 14/12/2.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "RelevantViewController.h"
#import "PMToastHint.h"

@interface RelevantViewController () <UITextFieldDelegate>

@property (strong, nonatomic) NSMutableArray *buttonArray;
@property (strong, nonatomic) NSMutableArray *addAndSubArr;
@property (strong, nonatomic) UITextField *countText;
@property (assign, nonatomic) NSInteger countNum;

@end

@implementation RelevantViewController
{
    NSInteger _textView;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"我要退货";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.countNum = 1;
    _textView = 0;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    // 注册通知，监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];
    // 注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
    
    [super viewWillAppear:animated];
    
    [self createRelevantTypeUI];
    [self createRelevantContentUI];
}

- (void)createRelevantTypeUI
{
    self.relevantView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, HeightRate(250))];
    self.relevantView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.relevantView];
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.relevantView .layer addSublayer:layer];
    
    // 类型
    UILabel *typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50), HeightRate(36), WidthRate(110), HeightRate(40))];
    typeLabel.backgroundColor = [UIColor clearColor];
    typeLabel.text = @"我要:";
    typeLabel.textColor = RGBA(118, 118, 118, 1);
    typeLabel.textAlignment = NSTextAlignmentLeft;
    typeLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.relevantView addSubview:typeLabel];
    
//    NSArray *array = [NSArray arrayWithObjects:@"退货",@"换货", nil];
    NSArray *array = [NSArray arrayWithObjects:@"退货", nil];
    self.buttonArray = [NSMutableArray array];
    for(int i = 0; i < array.count; i++) {
        
        self.relevantBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.relevantBtn.frame = CGRectMake(WidthRate(160) + WidthRate(180) * (i%3), HeightRate(30) + HeightRate(80) * (i/3), WidthRate(160), HeightRate(56));
        [self.relevantBtn setBackgroundImage:[UIImage imageNamed:@"01normal@2x.png"] forState:UIControlStateNormal];
        [self.relevantBtn setBackgroundImage:[UIImage imageNamed:@"01selected@2x.png"] forState:UIControlStateSelected];
        [self.relevantBtn setTitle:array[i] forState:UIControlStateNormal];
        [self.relevantBtn setTitleColor:RGBA(189, 189, 189, 1) forState:UIControlStateNormal];
        [self.relevantBtn setTitleColor:HDVGRed forState:UIControlStateSelected];
        [self.relevantBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
        if (i == 0)
        {
            self.relevantBtn.selected = YES;
        }
        
        // 设置tag，主要是为了添加点击事件时可以正确的知道是那个按钮触发的
        self.relevantBtn.tag = i;
        [self.relevantBtn addTarget:self action:@selector(returnButtonTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.relevantView addSubview:self.relevantBtn];
        
        [self.buttonArray addObject:self.relevantBtn];   // 将按钮添加到数组里
    }
    
    UILabel *countTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50), HeightRate(100), WidthRate(110), HeightRate(40))];
    countTitleLB.backgroundColor = [UIColor clearColor];
    countTitleLB.text = @"数量:";
    countTitleLB.textColor = RGBA(118, 118, 118, 1);
    countTitleLB.textAlignment = NSTextAlignmentLeft;
    countTitleLB.font = [UIFont systemFontOfSize:15.0f];
    [self.relevantView addSubview:countTitleLB];
    
    UIButton *minusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    minusBtn.frame = CGRectMake(WidthRate(160), HeightRate(100), WidthRate(40), WidthRate(40));
    [minusBtn setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
    minusBtn.layer.borderColor = RGBA(118, 118, 118, 1).CGColor;
    minusBtn.layer.borderWidth = 0.5;
    minusBtn.adjustsImageWhenHighlighted = NO;
    minusBtn.tag = 1000;
    [minusBtn addTarget:self action:@selector(changeNum:) forControlEvents:UIControlEventTouchUpInside];
    [self.relevantView addSubview:minusBtn];
    
    self.countText = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(210), HeightRate(100), WidthRate(70), WidthRate(40))];
    self.countText.layer.borderWidth = 0.5;
    self.countText.layer.borderColor = RGBA(118, 118, 118, 1).CGColor;
    self.countText.delegate = self;
    self.countText.font = [UIFont systemFontOfSize:15.0f];
    self.countText.textAlignment = NSTextAlignmentCenter;
    self.countText.text = [NSString stringWithFormat:@"%ld",(long)self.countNum];
    [self.relevantView addSubview:self.countText];
    
    UIButton *plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    plusBtn.frame = CGRectMake(WidthRate(290), HeightRate(100), WidthRate(40), WidthRate(40));
    [plusBtn setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
    plusBtn.layer.borderColor = RGBA(118, 118, 118, 1).CGColor;
    plusBtn.layer.borderWidth = 0.5;
    plusBtn.adjustsImageWhenHighlighted = NO;
    plusBtn.tag = 1001;
    [plusBtn addTarget:self action:@selector(changeNum:) forControlEvents:UIControlEventTouchUpInside];
    [self.relevantView addSubview:plusBtn];
    
    // 描述
    UILabel *describeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50), HeightRate(164), WidthRate(110), HeightRate(40))];
    describeLabel.backgroundColor = [UIColor clearColor];
    describeLabel.text = @"描述：";
    describeLabel.textColor = RGBA(118, 118, 118, 1);
    describeLabel.textAlignment = NSTextAlignmentLeft;
    describeLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.relevantView addSubview:describeLabel];
}

- (void)createRelevantContentUI
{
    self.describeView = [[UIView alloc] initWithFrame:CGRectMake(0, self.relevantView.frame.origin.y + HeightRate(250), WIDTH, HeightRate(200))];
    self.describeView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.describeView];
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.describeView .layer addSublayer:layer];
    
    // 初始化退换货描述 textView
    self.reasonTextView = [[UITextView alloc] initWithFrame:CGRectMake(WidthRate(2), HeightRate(1), WIDTH - WidthRate(4), HeightRate(238))];
    self.reasonTextView.delegate = self;
    self.reasonTextView.textColor = RGBA(79, 79, 79, 1);
    self.reasonTextView.font = [UIFont systemFontOfSize:14.0f];
    self.reasonTextView.layer.cornerRadius = 0.0f;   // 设置圆角值
    self.reasonTextView.layer.masksToBounds = YES;
    self.reasonTextView.layer.borderWidth = 0.5f;    // 设置边框大小
    [self.reasonTextView.layer setBorderColor:[[UIColor clearColor] CGColor]];
    self.reasonTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.reasonTextView.returnKeyType = UIReturnKeyDone;
    self.reasonTextView.keyboardType = UIKeyboardTypeDefault;
    self.reasonTextView.scrollEnabled = YES;
    [self.describeView  addSubview:self.reasonTextView];
    
    // 分隔线
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(0, HeightRate(239), WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.describeView .layer addSublayer:layer2];

    
    // 提示输入字数
    self.tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(140), self.describeView.frame.origin.y + HeightRate(245), WidthRate(100), HeightRate(48))];
    self.tipLabel.backgroundColor = [UIColor clearColor];
    self.tipLabel.text = [NSString stringWithFormat:@"%lu/50", (unsigned long)self.reasonTextView.text.length];
    self.tipLabel.textColor = [UIColor grayColor];
    self.tipLabel.textAlignment = NSTextAlignmentRight;
    self.tipLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.view addSubview:self.tipLabel];
    
    // 初始化申请提交按钮
    UIButton *submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    submitButton.frame = CGRectMake(WidthRate(50), self.describeView.frame.origin.y + HeightRate(350), WIDTH - WidthRate(50) * 2, 40);
    [submitButton setTitle:@"提交申请" forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(submitForRelevantButton:) forControlEvents:UIControlEventTouchUpInside];
    [submitButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [submitButton setBackgroundColor:ButtonBgColor];
    [submitButton.layer setCornerRadius:6.0f];
    [self.view addSubview:submitButton];
}

#pragma mark - 退换货 button 触发的方法
- (void)returnButtonTypeClick:(UIButton *)button
{
    button.selected = YES;
    if (button.tag == 0)
    {
        self.flagLabel = [[UILabel alloc] init];
        self.flagLabel.text = @"1";
    }
}

#pragma mark - 解决textView输入时键盘遮挡问题
// 监听事件
- (void)handleKeyboardDidShow:(NSNotification *)paramNotification
{
//    [UIView animateWithDuration:0.3 animations:^{
//        self.view.frame = CGRectMake(0, (iPhone4s ? -30 : 0), self.view.frame.size.width, self.view.frame.size.height);
//    }];
    
    // 让出现键盘只执行一次，当_textView ＝ 0 时，才向上移动30像素
    if (_textView == 0) {
        // 只针对3.5屏幕向上移动30像素
        [self moveView:(iPhone4s ? -30 : 0)];
        
        _textView = 1;
    }
}

- (void)handleKeyboardDidHidden
{
//    [UIView animateWithDuration:0.3 animations:^{
//        self.view.frame = CGRectMake(0, (iPhone4s ? 64 : 0), self.view.frame.size.width, self.view.frame.size.height);
//    }];
    
    [self moveView:(iPhone4s ? 30 : 0)];
    
    // 当键盘消失时，向下移动30像素 ,把_textView置0，
    _textView = 0;
}

- (void)moveView:(CGFloat)move
{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y += move;//view的y轴上移
    self.view.frame = frame;
    [UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.reasonTextView resignFirstResponder];
}

// 隐藏软键盘
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    // 判断输入是否已经超出最大可输入长度
    if ([text isEqualToString:@""] && range.length > 0) {

        //删除字符肯定是安全的
        return YES;
    }
    else {
        if (self.reasonTextView.text.length - range.length + text.length > 50) {
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"超出最大可输入长度" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alert show];
            return NO;
        }
        else {
            return YES;
        }
    }
    return YES;
}

// 改变输入字数多少，显示几分之几
- (void)textViewDidChange:(UITextView *)textView
{
    self.tipLabel.text = [NSString stringWithFormat:@"%lu/50", (unsigned long)self.reasonTextView.text.length];
}

- (void)submitForRelevantButton:(id)sender
{
    if (self.reasonTextView.text.length == 0) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请填写退货原因" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
    }
    
    if (self.reasonTextView.text.length != 0)
    {
        /**
         参数：
         PM_SID	       Y	String		Token
         orderId	   Y	String		订单Id
         productId	   Y	String		商品Id
         productNum	   Y	String		商品数量
         reason	       Y	String		退换货原因（默认传1给后台） 1：收到商品破损  2：商品错发/漏发 3：收到商品与描述不符  4：收到假货
         remark	       N	String		退换货说明
         flag	       Y	Integer		标志退换货 1：退货，  -1：换货
         */
        PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
        
        NSString *orderId = [self.relevantDict objectNullForKey:@"orderId"];
        NSString *productId = [self.relevantDict objectNullForKey:@"productId"];
        NSString *productNum = [NSString stringWithFormat:@"%@",self.countText.text];
        //NSString *productNum = [NSString stringWithFormat:@"%@",[self.relevantDict objectNullForKey:@"productNum"]];
        NSString *reason = @"1";
        NSString *remark = [NSString stringWithFormat:@"%@", self.reasonTextView.text];
        
        if ([self.countText.text integerValue] > [productNum integerValue])
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"数量不能大于购买数量"];
            self.countText.text = productNum;
        }
        else
        {
            NSDictionary *applyReturn = @{@"PM_SID":userInfos.PM_SID,@"orderId":orderId,@"productId":productId,@"productNum":productNum,@"reason":reason,@"remark":remark,@"flag":@(1)};
            self.networking = [PMNetworking defaultNetworking];
            [self.networking request:PMRequestStateApplyReturn WithParameters:applyReturn callBackBlock:^(NSDictionary *dict) {
            
                if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
                
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提交申请退换货成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                    alertView.tag = 1;
                    [alertView show];
                }
                else {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }showIndicator:YES];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        //[self.navigationController popToRootViewControllerAnimated:YES];
        NSArray *controllers = [self.navigationController viewControllers];
        UIViewController *viewController = [controllers objectAtIndex:1];
        [self.navigationController popToViewController:viewController animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeNum:(UIButton *)sender
{
    NSInteger maxCount = [[self.relevantDict objectNullForKey:@"productNum"] integerValue];
    if (sender.tag == 1000)
    {
        if (self.countNum == 1)
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"数量至少为1"];
        }
        else
        {
            self.countNum --;
            self.countText.text = [NSString stringWithFormat:@"%ld",(long)self.countNum];
        }
    }
    else
    {
        if (self.countNum == maxCount)
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"数量不能大于购买数量"];
        }
        else
        {
            self.countNum ++;
            self.countText.text = [NSString stringWithFormat:@"%ld",(long)self.countNum];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text integerValue] < 1)
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入一个大于0的数"];
        self.countText.text = @"1";
    }
    else if (![self isPureInteger:textField.text])
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入一个大于0的整数"];
        self.countText.text = @"1";
    }
}

- (BOOL)isPureInteger:(NSString*)string{
    NSScanner *scan = [NSScanner scannerWithString:string];
    NSInteger val;
    return[scan scanInteger:&val] && [scan isAtEnd];
}

@end
