//
//  SafeManageTableViewCell.h
//  changeViewController
//
//  Created by P&M on 14/12/12.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface SafeManageTableViewCell : UITableViewCell

@property (strong, nonatomic) UIImageView *defaultMark;
@property (strong, nonatomic) UIImageView *imaView;
@property (strong, nonatomic) UIButton *exitButton;

- (void)setMyAccountCellUI;
- (void)setNewAddressDataUI;

// 设置我的账户图片、标题
- (void)setImage:(UIImage *)image setTitle:(NSString *)title;

// 设置新建地址的姓名、电话、地址数据
- (void)setName:(NSString *)name setMobile:(NSString *)mobile setAddress:(NSString *)address;

@end
