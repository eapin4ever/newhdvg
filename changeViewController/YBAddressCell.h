//
//  YBAddressCell.h
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBAddressCell : UITableViewCell

@property (strong,nonatomic) NSDictionary *addressDic;
@property (strong,nonatomic) UIButton *chooseAddressBtn;
@property (assign,nonatomic) BOOL isTradeBack;

- (void)createAddressUI;

@end
