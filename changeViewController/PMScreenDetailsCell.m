//
//  PMScreenDetailsCell.m
//  changeViewController
//
//  Created by P&M on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMScreenDetailsCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMScreenDetailsCell

- (void)createScreenDetailsUI
{
    if (!self.titleLabel) {
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 7, WidthRate(350), 30)];
        self.titleLabel.textColor = [UIColor lightGrayColor];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.titleLabel];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.checkIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        self.checkIV.image = [UIImage imageNamed:@"item_selected"];
        self.checkIV.hidden = YES;
        self.accessoryView = self.checkIV;
    }
}

- (void)setScreenDetailsTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

@end
