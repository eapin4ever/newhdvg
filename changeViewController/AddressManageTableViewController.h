//
//  AddressManageTableViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/16.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "SafeManageTableViewCell.h"
#import "ShippingAddViewController.h"
#import "EditAddressViewController.h"
#import "PMNetworking.h"
#import "PMUserInfos.h"

@interface AddressManageTableViewController : UITableViewController


@property (strong, nonatomic) NSMutableArray *addressSectionArray;

- (void)hideAccessory;
- (void)initNewAddressDataList;

@end
