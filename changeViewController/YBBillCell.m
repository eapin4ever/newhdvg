//
//  YBBillCell.m
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//账单单元格
//
#define textGrayColor RGBA(135, 135, 135, 1)
#define lineGrayColor RGBA(193, 193, 193, 1).CGColor

#import "YBBillCell.h"
#import "PMMyPhoneInfo.h"
#import "YBBillInfos.h"
#import "YBSureOrderViewController.h"

@interface YBBillCell() <UITextFieldDelegate>

@property(nonatomic,strong)NSMutableArray *billTypeBtnArray;
@property(nonatomic,strong)NSMutableArray *billContentBtnArray;
    
@property(nonatomic,assign)NSInteger billType;
@property(nonatomic,assign)NSInteger billContent;




@end

@implementation YBBillCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //分割线
//    CALayer *line1 = [[CALayer alloc] init];
//    line1.frame = CGRectMake(0, 44, self.contentView.bounds.size.width, 0.5);
//    line1.backgroundColor = lineGrayColor;
//    [self.contentView.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(0, 44*2, self.contentView.bounds.size.width, 0.5);
    line2.backgroundColor = lineGrayColor;
    [self.contentView.layer addSublayer:line2];
    
    CALayer *line3 = [[CALayer alloc] init];
    line3.frame = CGRectMake(0, 44*3, self.contentView.bounds.size.width, 0.5);
    line3.backgroundColor = lineGrayColor;
    [self.contentView.layer addSublayer:line3];
    
    UILabel *lb1 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 10, WidthRate(180), 24)];
    lb1.textColor = textGrayColor;
    lb1.text = @"发票抬头：";
    lb1.font = [UIFont systemFontOfSize:14.0f];
    [self.contentView addSubview:lb1];
    //发票抬头
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(250), 10, WidthRate(450), 24)];
    tf.returnKeyType = UIReturnKeyDone;
    self.tf = tf;
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:15.0f];
    tf.delegate = self;
    [self.contentView addSubview:tf];
    
    //预留字
    tf.placeholder = @"请填写";
    tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    [tf setValue:[UIFont boldSystemFontOfSize:14.0f] forKeyPath:@"_placeholderLabel.font"];
    [tf setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, tf.bounds.size.width, tf.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [tf setValue:RGBA(227, 227, 227, 1) forKeyPath:@"_placeholderLabel.textColor"];
    if (!isNull([YBBillInfos shareInstance].billNum) && ![[YBBillInfos shareInstance].billNum isEqualToString:@"null"])
    {
        tf.text = [YBBillInfos shareInstance].billNum;
    }
    else
    {
        tf.text = @"";
    }
    
    
    UILabel *lb2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 44, WidthRate(690), 44)];
    lb2.textColor = textGrayColor;
    lb2.text = @"发票类型：";
    lb2.font = [UIFont systemFontOfSize:15.0f];
//    [self.contentView addSubview:lb2];
    
    
    NSArray *arr1 = @[@"个人",@"单位"];
    self.billTypeBtnArray = [NSMutableArray array];
    //类型只有两种 使用按钮
    for(int i = 0;i<arr1.count;i++)
    {
        UIButton *typeBtn = [[UIButton alloc] initWithFrame:CGRectMake(WidthRate(250) + WidthRate(200)*i, 55, WidthRate(150), 24)];
        [typeBtn setTitle:arr1[i] forState:UIControlStateNormal];
        [typeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [typeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        typeBtn.layer.cornerRadius = 6.0;
        typeBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        typeBtn.layer.borderWidth = 0.5;
        typeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.self.contentView addSubview:typeBtn];
        [self.billTypeBtnArray addObject:typeBtn];
        
        typeBtn.tag = i+100;
        [typeBtn addTarget:self action:@selector(changeBtn:) forControlEvents:UIControlEventTouchDown];
    }
    
    UILabel *lb3 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 88, WidthRate(690), 44)];
    lb3.textColor = textGrayColor;
    lb3.text = @"发票内容：";
    lb3.font = [UIFont systemFontOfSize:14.0f];
    [self.contentView addSubview:lb3];
    //内容只有两种 使用按钮
    NSArray *arr2 = @[@"商品明细",@"办公用品"];
    self.billContentBtnArray = [NSMutableArray array];
    //类型只有两种 使用按钮
    for(int i = 0;i<arr2.count;i++)
    {
        UIButton *contentBtn = [[UIButton alloc] initWithFrame:CGRectMake(WidthRate(250) + WidthRate(200)*i, 99, WidthRate(150), 24)];
        [contentBtn setTitle:arr2[i] forState:UIControlStateNormal];
        [contentBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [contentBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        contentBtn.layer.cornerRadius = 6.0;
        contentBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        contentBtn.layer.borderWidth = 0.5;
        contentBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self.contentView addSubview:contentBtn];
        [self.billContentBtnArray addObject:contentBtn];
        
        contentBtn.tag = i;
        [contentBtn addTarget:self action:@selector(changeBtn:) forControlEvents:UIControlEventTouchDown];
    }
    
    //如果YBBillInfos存储了发票类型的值，那么将这个tag对应的button作为当前选中的button
    if ([YBBillInfos shareInstance].billType && [YBBillInfos shareInstance].billType != 0)
    {
        NSInteger billType = [YBBillInfos shareInstance].billType;
        NSInteger typeTag = (billType + 1) / 2 + 100;
        for (UIButton *button  in self.billTypeBtnArray)
        {
            if (typeTag == button.tag)
            {
                self.typeCurrentButton = button;
                break;
            }
        }
    }
    
    //如果YBBillInfos存储了发票内容的值，那么将这个tag对应的button作为当前选中的button
    if ([YBBillInfos shareInstance].billContent && [YBBillInfos shareInstance].billContent != 0)
    {
        NSInteger billcontent = [YBBillInfos shareInstance].billContent;
        NSInteger contentTag = (billcontent + 1) / 2;
        for (UIButton *button  in self.billContentBtnArray)
        {
            if (contentTag == button.tag)
            {
                self.contentCurrentButton = button;
                break;
            }
        }
    }
    
    if (self.isTradeBack)
    {
        tf.userInteractionEnabled = NO;
        tf.placeholder = @"";
        for (UIButton *button in self.billContentBtnArray)
        {
            button.userInteractionEnabled = NO;
        }
        
        for (UIButton *button in self.billTypeBtnArray)
        {
            button.userInteractionEnabled = NO;
        }
    }
    
    [self.typeCurrentButton setBackgroundColor:HDVGRed];
    self.typeCurrentButton.layer.borderColor = HDVGRed.CGColor;
    self.typeCurrentButton.selected = YES;
    [self.contentCurrentButton setBackgroundColor:HDVGRed];
    self.contentCurrentButton.layer.borderColor = HDVGRed.CGColor;
    self.contentCurrentButton.selected = YES;


    
}

#pragma mark - 点击切换选中按钮
- (void)changeBtn:(UIButton *)sender
{
    [self.tf resignFirstResponder];
    //大于100属于发票类型
    if(sender.tag>=100)
    {
        if (self.typeCurrentButton != sender)
        {
            self.typeCurrentButton.selected = NO;
            [self.typeCurrentButton setBackgroundColor:[UIColor clearColor]];
            self.typeCurrentButton = sender;
            self.typeCurrentButton.selected = YES;
            [self.typeCurrentButton setBackgroundColor:HDVGRed];
            
            self.billType = (sender.tag - 100)*2-1;
            
            [YBBillInfos shareInstance].billType = self.billType;

        }
        
        
    }
    else//发票内容
    {
        if (self.contentCurrentButton != sender)
        {
            self.contentCurrentButton.selected = NO;
            [self.contentCurrentButton setBackgroundColor:[UIColor clearColor]];
            self.contentCurrentButton = sender;
            [self.contentCurrentButton setBackgroundColor:HDVGRed];
            self.contentCurrentButton.selected = YES;
            
            self.billContent = sender.tag*2-1;
            [YBBillInfos shareInstance].billContent = self.billContent;
        }
        
        
        
    }
}

//发票抬头写完后往单例中保存
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (![textField.text isEqualToString:@""])
    {
        [YBBillInfos shareInstance].billNum = textField.text;
    }
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.tf resignFirstResponder];
    return YES;
}


@end
