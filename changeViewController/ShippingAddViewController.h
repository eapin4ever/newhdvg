//
//  ShippingAddViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "HZAreaPickerView.h"
#import "PMNetworking.h"
#import "PMUserInfos.h"
#import "PMGetAreaID.h"

@interface ShippingAddViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate, HZAreaPickerDelegate>

@property (strong, nonatomic) UITextField *nameTextField;
@property (strong, nonatomic) UITextField *mobileTextField;
@property (strong, nonatomic) UITextField *phoneTextField;
@property (strong, nonatomic) UITextField *regionTextField;
@property (strong, nonatomic) UITextField *addDetailTextField;

@property (strong, nonatomic) NSString *areaValue;
@property (copy, nonatomic) NSString *areaID;
@property (copy, nonatomic) NSString *provinceID;
@property (copy, nonatomic) NSString *cityID;
@property (copy, nonatomic) NSString *districtID;
@property (strong, nonatomic) HZAreaPickerView *locatePicker;

@property (strong, nonatomic) PMNetworking *networking;

- (void)cancelLocatePicker;

@end
