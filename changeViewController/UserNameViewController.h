//
//  UserNameViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/3.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  设置用户昵称和性别控制器
 */

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMUserInfos.h"
#import "PMNetworking.h"

@interface UserNameViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate, UIActionSheetDelegate, UITextViewDelegate>

@property (strong, nonatomic) UIView *userInfoView;
@property (strong, nonatomic) UITextField *nicknameTextField;
@property (strong, nonatomic) UITextField *sexTextField;
@property (strong, nonatomic) NSString *sexString;
@property (strong, nonatomic) UILabel *tipLabel;
@property (strong, nonatomic) UILabel *mobileNumberLab;
@property (strong, nonatomic) UITextView *remarkTextView;

@property (strong, nonatomic) PMNetworking *networking;

@end
