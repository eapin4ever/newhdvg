//
//  PMBillInfoCell.m
//  changeViewController
//
//  Created by pmit on 15/11/4.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMBillInfoCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMBillInfoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.titleLB)
    {
        NSString *tempSting = @"发票抬头：";
        CGSize tempSize = [tempSting boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0f]} context:nil].size;
        self.titleLB = [[UILabel alloc] init];
        self.titleLB.frame = CGRectMake(15, 0, tempSize.width, 20);
        self.titleLB.font = [UIFont systemFontOfSize:16.0f];
        self.titleLB.textColor = [UIColor lightGrayColor];
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.titleLB];
        
        self.contentLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.titleLB.frame) + 2, 0, WIDTH - CGRectGetMaxX(self.titleLB.frame) - 17, 20)];
        self.contentLB.font = [UIFont systemFontOfSize:16.0f];
        self.contentLB.textColor = [UIColor lightGrayColor];
        self.contentLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.contentLB];
        [self.contentView addSubview:self.contentLB];
    }
}

- (void)setCellData:(NSString *)billTitleString Content:(NSString *)billContentString
{
    if (![billTitleString isEqualToString:@"发票类型"])
    {
        self.titleLB.text = [NSString stringWithFormat:@"%@：",billTitleString];
    }
    
    self.contentLB.text = billContentString;
}

@end
