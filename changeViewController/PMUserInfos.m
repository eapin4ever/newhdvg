//
//  PMUserInfos.m
//  changeViewController
//
//  Created by pmit on 14/12/16.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMUserInfos.h"
#import "PMNetworking.h"

static PMUserInfos *_userInfos;
@implementation PMUserInfos
{
    NSInteger _getlocationErrorCount;
}

+ (PMUserInfos *)shareUserInfo
{
    if(!_userInfos)
    {
        _userInfos = [[PMUserInfos alloc] init];
    }
    return _userInfos;
}

- (void)setUserDataDic:(NSMutableDictionary *)userDataDic
{
    _userDataDic = userDataDic;
    
    NSDictionary *clientUserDic = [userDataDic objectForKey:@"clientUser"];
    
    _userId = [clientUserDic objectForKey:@"id"];
    
    double newsTime = [[NSDate new] timeIntervalSince1970];
    
    self.loginTime = newsTime;
}


- (void)logout
{
    //清除userDefault数据
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:@"codeAndPass"];
    
    self.userDataDic = nil;
    self.userId = nil;
    self.isReseller = NO;
    self.PM_SID = nil;
    self.loginTime = 0;
}

- (BOOL)checkPMSIDTime
{
    double nowTime = [[NSDate new] timeIntervalSince1970];
    if (nowTime - self.loginTime >= 30 * 60)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

@end
