//
//  HomeTitleCell.m
//  changeViewController
//
//  Created by pmit on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "HomeTitleCell.h"
#import "PMMyPhoneInfo.h"

@implementation HomeTitleCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.titleLB)
    {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.colorView = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 15, 44)];
        [self.contentView addSubview:self.colorView];
        
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 80 , 44)];
        self.titleLB.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
        self.titleLB.textColor = RGBA(51, 51, 51, 1);
        [self.contentView addSubview:self.titleLB];
        
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBA(51, 51, 51, 1).CGColor;
        line.frame = CGRectMake(120, 21.5, WIDTH -120, 1);
        [self.contentView.layer addSublayer:line];
    }
}

- (void)setCellTitle:(NSString *)titleString AndColor:(UIColor *)cellColor
{
    NSString *newsString = [NSString stringWithFormat:@"%@·%@",[titleString substringWithRange:NSMakeRange(0, 2)],[titleString substringWithRange:NSMakeRange(2, 2)]];
    self.titleLB.text = newsString;
    self.colorView.backgroundColor = RGBA(51, 51, 51, 1);
}

@end
