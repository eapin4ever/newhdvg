//
//  OrderListViewController.m
//  changeViewController
//
//  Created by ZhangEapin on 15/5/6.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "OrderListViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PMSureOrderTVCell.h"
#import "PMMyGoodsViewController.h"

@interface OrderListViewController () <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *listTableView;

@end

@implementation OrderListViewController

static NSString *const tvCell = @"tvCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"购物清单";
    self.view.backgroundColor = HDVGPageBGGray;
    
    [self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

- (void)createUI
{
    UITableView *listTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 69) style:UITableViewStylePlain];
    self.listTableView = listTableView;
    listTableView.backgroundColor = HDVGPageBGGray;
    listTableView.delegate = self;
    listTableView.dataSource = self;
    [listTableView registerClass:[PMSureOrderTVCell class] forCellReuseIdentifier:tvCell];
    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:listTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.productArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMSureOrderTVCell *cell = [tableView dequeueReusableCellWithIdentifier:tvCell];
    NSDictionary *dic = self.productArr[indexPath.section];
    
    if (!self.isFromOrderDetail)
    {
        NSDictionary *productDic = [dic objectNullForKey:@"product"];
        NSString *zkString = @"";
        if (self.isTradeBack)
        {
            zkString = [productDic objectNullForKey:@"productType"];
        }
        else
        {
            zkString = [dic objectForKey:@"productType"];
        }
        
        // 剪头图片
        UIImageView *arrowIma = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, HeightRate(105) - (iPhone4s?-3:-1), 20, 20)];
        arrowIma.image = [UIImage imageNamed:@"arrowRight"];
        [cell.contentView addSubview:arrowIma];
        
        
        [cell createUI];
        
        if ([zkString isEqualToString:@"zk"])
        {
            cell.activeIV.hidden = NO;
        }
        else
        {
            cell.activeIV.hidden = YES;
        }
        
        [cell.iv sd_setImageWithURL:[productDic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
        if (self.isTradeBack)
        {
            [cell setContentTitle:[productDic objectNullForKey:@"productName"] classify1:[productDic objectNullForKey:@"specValueA"] classify2:[productDic objectNullForKey:@"specValueB"] Classify3:[productDic objectNullForKey:@"specValueC"] Classify4:[productDic objectNullForKey:@"specValueD"] Classify5:[productDic objectNullForKey:@"specValueE"] distanceOrLocation:[NSString stringWithFormat:@"%dkm",arc4random()%20] number:[productDic objectNullForKey:@"productNum"] price:[[productDic objectNullForKey:@"productPrice"] doubleValue]];
        }
        else
        {
            [cell setContentTitle:[productDic objectNullForKey:@"productName"] classify1:[dic objectNullForKey:@"aSpecVal"] classify2:[dic objectNullForKey:@"bSpecVal"] Classify3:[dic objectNullForKey:@"cSpecVal"] Classify4:[dic objectNullForKey:@"dSpecVal"] Classify5:[dic objectNullForKey:@"eSpecVal"] distanceOrLocation:[NSString stringWithFormat:@"%dkm",arc4random()%20] number:[dic objectNullForKey:@"productCount"] price:[[productDic objectNullForKey:@"productPrice"] doubleValue]];
        }
    }
    else
    {
        UIImageView *arrowIma = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, HeightRate(105) - (iPhone4s?-3:-1), 20, 20)];
        arrowIma.image = [UIImage imageNamed:@"arrowRight"];
        [cell.contentView addSubview:arrowIma];
        [cell createUI];
        
        [cell.iv sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
        [cell setContentTitle:[dic objectNullForKey:@"productName"] classify1:[dic objectNullForKey:@"aSpecVal"] classify2:[dic objectNullForKey:@"bSpecVal"] Classify3:[dic objectNullForKey:@"cSpecVal"] Classify4:[dic objectNullForKey:@"dSpecVal"] Classify5:[dic objectNullForKey:@"eSpecVal"] distanceOrLocation:[NSString stringWithFormat:@"%dkm",arc4random()%20] number:[dic objectNullForKey:@"productNum"] price:[[dic objectNullForKey:@"productPrice"] doubleValue]];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightRate(240);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMMyGoodsViewController *goodVC = [[PMMyGoodsViewController alloc] init];
    NSString *productId = self.isFromOrderDetail ? [self.productArr[indexPath.section] objectNullForKey:@"productId"] : self.isTradeBack ? [[self.productArr[indexPath.section] objectNullForKey:@"product"]objectNullForKey:@"productId"]:[[self.productArr[indexPath.section] objectNullForKey:@"product"] objectNullForKey:@"id"];
    goodVC.productId = productId;
    [self.navigationController pushViewControllerWithNavigationControllerTransition:goodVC];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return 5;
}

@end
