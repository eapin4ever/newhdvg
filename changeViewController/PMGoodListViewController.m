//
//  PMGoodListViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/25.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMGoodListViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "PMShowClass.h"
#import "PMShowClass.h"
#import "MasterViewController.h"
#import "SearchResultViewController.h"

@interface PMGoodListViewController () <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *typeNameTableView;
@property (strong,nonatomic) NSArray *typeNameArr;
@property (strong,nonatomic) PMShowClass *showClass;
@property (strong,nonatomic) NSMutableArray *parentClassArray;
@property (strong,nonatomic) UIView *noDataView;
@property (strong,nonatomic) UIView *detailView;
@property (assign,nonatomic) NSInteger currentIndex;
@property (assign,nonatomic) BOOL isSecond;

@end

@implementation PMGoodListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商品分类";
    self.currentIndex = 0;
    [self buildTypeNameTableView];
    [self buildDetailView];
    [self createNoDataView:self.detailView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[MasterViewController defaultMasterVC].tabBar setAlpha:1];
    
    [self getTypeNameByNet];
    if (self.typeNameArr && self.typeNameArr.count != 0)
    {
        NSString *classId = [self.typeNameArr[self.currentIndex] objectNullForKey:@"preParentClassId"];
        [self getDetailByNet:classId];
        [self.detailTableView reloadData];
    }
    
    self.isSecond = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildTypeNameTableView
{
    self.typeNameTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, -64, WidthRate(220), HEIGHT) style:UITableViewStylePlain];
    self.typeNameTableView.delegate = self;
    self.typeNameTableView.dataSource = self;
    self.typeNameTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.typeNameTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.typeNameTableView setContentInset:UIEdgeInsetsMake(64, 0, 49, 0)];
    self.typeNameTableView.showsHorizontalScrollIndicator = NO;
    self.typeNameTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.typeNameTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.typeNameArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *selectedBackGroundView = [[UIView alloc] initWithFrame:cell.frame];
    selectedBackGroundView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [[CALayer alloc] init];
    line.frame = CGRectMake(0, 0, WidthRate(8), 51);
    line.backgroundColor = RGBA(188, 24, 40, 1).CGColor;
    [selectedBackGroundView.layer addSublayer:line];
    
    cell.selectedBackgroundView = selectedBackGroundView;
    cell.textLabel.text = [self.typeNameArr[indexPath.row] objectNullForKey:@"preParentClassName"];
    cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
    cell.textLabel.highlightedTextColor = RGBA(188, 24, 40, 1);
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)getTypeNameByNet
{
//    NSDictionary *param;
//    param = @{@"areaId":@"440100"};
    
    [[PMNetworking defaultNetworking] request:PMRequestStateSelectClass WithParameters:nil callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.typeNameArr = [dic objectNullForKey:@"data"];
            
            [self.typeNameTableView reloadData];
            
            if (self.typeNameArr.count > 0)
            {
                NSIndexPath *ip=[NSIndexPath indexPathForRow:self.currentIndex inSection:0];
                [self.typeNameTableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionBottom];
                [self.typeNameTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                [self getDetailByNet:[self.typeNameArr[self.currentIndex] objectNullForKey:@"preParentClassId"]];
                [self.detailTableView reloadData];
            }
        }
      
    }showIndicator:!self.isSecond];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentIndex = indexPath.row;
    [self getDetailByNet:[self.typeNameArr[indexPath.row] objectNullForKey:@"preParentClassId"]];
    if (indexPath.row < 7)
    {
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else
    {
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}


- (void)buildDetailView
{
    UIView *detaiView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(220), 0, WIDTH - WidthRate(220), HEIGHT - 49 )];
    detaiView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:detaiView];
    self.detailView = detaiView;
    
    self.detailTableView = [[UITableView alloc] initWithFrame:CGRectMake(WidthRate(20), -64 , detaiView.bounds.size.width, detaiView.bounds.size.height)];
    [self.detailTableView registerClass:[PMProductClassTVCell class] forCellReuseIdentifier:@"productClassCell"];
    self.detailTableView.backgroundColor = [UIColor whiteColor];
    self.detailTableView.separatorColor = [UIColor clearColor];
    self.detailTableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    self.detailTableView.showsVerticalScrollIndicator = NO;
    
    PMShowClass *showClass = [[PMShowClass alloc] init];
    showClass.goodsListVC = self;
    self.showClass = showClass;
    self.detailTableView.dataSource = showClass;
    self.detailTableView.delegate = showClass;
    [detaiView addSubview:self.detailTableView];
}

- (void)getDetailByNet:(NSString *)classId
{
    [[PMNetworking defaultNetworking] request:PMRequestStateSelectPreAndChildClass WithParameters:@{@"classId":classId} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            NSArray *dicArray = [dic objectNullForKey:@"data"];
            if (dicArray.count == 0)
            {
                self.noDataView.hidden = NO;
            }
            else
            {
                self.noDataView.hidden = YES;
                
                BOOL isGet = NO;
                self.parentClassArray = [NSMutableArray array];
                //将得到的数据分类，以二级id为准
                for (NSDictionary *parentClassDic in dicArray)
                {
                    //筛选有图片的 -- 如果有图片的话
                    if([[[parentClassDic objectNullForKey:@"classLogo"] lastPathComponent] rangeOfString:@"null"].length == 0)
                    {
                        
                        NSString *parentClassId = [parentClassDic objectNullForKey:@"parentClassId"];
                        for (NSDictionary *classDic in self.parentClassArray)
                        {
                            //如果是与已经存在的二级类目id相同，则加入到该类目中
                            if([[classDic objectNullForKey:@"parentClassId"] isEqualToString:parentClassId])
                            {
                                
                                //加入到该数组中
                                NSMutableArray *arr = [classDic objectNullForKey:@"class"];
                                [arr addObject:parentClassDic];
                                //标记该字典已经使用了
                                isGet = YES;
                                break;
                                
                            }
                        }
                        
                        //遍历完都没有加入到数组，说明没有一个相同的id，新增一个二级类目
                        if(isGet == NO)
                        {
                            //新字典
                            NSMutableDictionary *newDic = [NSMutableDictionary dictionary];
                            
                            NSMutableArray *newArr = [NSMutableArray array];
                            [newArr addObject:parentClassDic];
                            
                            [newDic setValuesForKeysWithDictionary:@{@"parentClassId":parentClassId,@"parentClassName":[parentClassDic objectNullForKey:@"parentClassName"],@"class":newArr}];
                            
                            //将字典加入到属性的数组中
                            [self.parentClassArray addObject:newDic];
                        }
                        else
                            isGet = NO;
                    }
                }
                
                //得到数据并且处理完成后，刷新右侧页面
                self.showClass.parentClassArray = self.parentClassArray;
                [self.detailTableView reloadData];
            } 
        }
        else
        {
            showRequestFailAlertView;
        }
    }showIndicator:NO];
}

#pragma mark  点击进行搜索
- (void)clickBtn:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    NSInteger section = sender.tag/1000;
    NSInteger row = sender.tag%1000/100;
    //一行有三个按钮  0  1  2
    //              3  4  5
    NSInteger arrIndex = sender.tag % 10 + row * 3;
    
    NSDictionary *dic = self.parentClassArray[section];
    NSArray *arr = [dic objectNullForKey:@"class"];
    NSDictionary *productDic = arr[arrIndex];
    
    SearchResultViewController *searchVC = [[SearchResultViewController alloc] init];
    NSString *areaName = [NSString stringWithFormat:@"%@,%@",[productDic objectNullForKey:@"areaId"],[productDic objectNullForKey:@"areaName"]];
    searchVC.areaName = areaName;
    searchVC.searchString = [productDic objectNullForKey:@"className"];
    searchVC.secondClassId = [productDic objectNullForKey:@"classId"];
    
    
    [self.navigationController pushViewControllerWithNavigationControllerTransition:searchVC];
    self.view.userInteractionEnabled = YES;
}

- (void)createNoDataView:(UIView *)superView
{
    self.noDataView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(20), 0 , superView.bounds.size.width - WidthRate(40), superView.bounds.size.height)];
    self.noDataView.backgroundColor = [UIColor whiteColor];
    UIImageView *noDataIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.noDataView.bounds.size.width, self.noDataView.bounds.size.height)];
    noDataIV.contentMode = UIViewContentModeScaleAspectFit;
    noDataIV.image = [UIImage imageNamed:@"productStore_null"];
    [self.noDataView addSubview:noDataIV];
    
    self.noDataView.hidden = YES;
    [superView addSubview:self.noDataView];
}


@end
