//
//  YBSureOrderViewController.m
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define grayColor RGBA(231, 231, 231, 1)

#define itemX WidthRate(30)

#import "YBSureOrderViewController.h"
#import "PMMyPhoneInfo.h"
#import "YBAddressCell.h"
#import "YBPayCell.h"
#import "YBBillCell.h"
#import "PMSureOrderTVCell.h"
#import "PMChooseAddressVC.h"
#import "YBBillInfos.h"
#import "PMShowAllGoodsWillBuyVC.h"
#import "PMBrowser.h"
#import "MasterViewController.h"
#import "WXApi.h"
#import "AppDelegate.h"
#import "OrderListViewController.h"
#import "DetailTradeViewController.h"
#import <UIImageView+PlayGIF.h>

@interface YBSureOrderViewController () <UIAlertViewDelegate,MyAppDelegate>

@property (assign,nonatomic) BOOL isOpen;
@property (assign,nonatomic) BOOL goodIsOpen;
@property (strong,nonatomic) UIImageView *arrowImageView;
@property (strong,nonatomic) UIImageView *goodArrowImageView;
@property (assign,nonatomic) BOOL isPay;
@property (assign,nonatomic) BOOL isReback;
@property (strong,nonatomic) NSArray *payArray;
@property (strong,nonatomic) UISwitch *packageSwitch;
@property (assign,nonatomic) BOOL isUserPackage; //是否使用钱包
@property (strong,nonatomic) UILabel *moneyLabel;
@property (assign,nonatomic) double transportationMoney;//运费
@property (assign,nonatomic) double packageMoney;//钱包余额
@property (assign,nonatomic) double changePackageMoney; //变化的钱包余额
@property (assign,nonatomic) double changeGoldMoney; //变化的红包余额
@property (assign,nonatomic) double goldCount;//金条数量
@property (assign,nonatomic) int goldTicket; //优惠劵面值
@property (strong,nonatomic) UISwitch *goldSwitch;
@property (strong,nonatomic) UISwitch *ticketSwitch;
@property (assign,nonatomic) BOOL isUseGold;
@property (assign,nonatomic) BOOL isUseTicket;
@property (strong,nonatomic) YBBillCell *billCell;
@property (assign,nonatomic) double userFulRedMoney;
@property (assign,nonatomic) double ydUserFulRedMoney;
@property (strong,nonatomic) UIImageView *addressArrowImageView;
@property (assign,nonatomic) BOOL isAddressOpen;
@property (assign,nonatomic) BOOL isUserRed;
@property (assign,nonatomic) double myHBMoney;
@property (strong,nonatomic) UIImageView *gifIV;
@property (strong,nonatomic) UIView *noTouchView;


@end

@implementation YBSureOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.isOpen = NO;//判断发表行状态是否是打开的
    self.goodIsOpen = NO;//判断购买物品行状态是否打开
    self.isReback = NO;//判断是否还未完成付款就回到账单界面
    self.isWX = NO;
    self.isUserPackage = NO;
    self.transportationMoney = 0;
    self.packageMoney = 100.00;
    self.changePackageMoney = self.packageMoney;
    //self.goldCount = 20;
    self.goldTicket = 10;
    self.isUseGold = NO;
    self.isUseTicket = NO;
//    self.isPrePay = YES;
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.myDelegate = self;
    
    self.title = @"订单确认";
    [self createUI];
    
    [self.view setBackgroundColor:grayColor];
    [self.tableView registerClass:[YBAddressCell class] forCellReuseIdentifier:@"addressCell"];
    [self.tableView registerClass:[YBPayCell class] forCellReuseIdentifier:@"payCell"];
    [self.tableView registerClass:[YBBillCell class] forCellReuseIdentifier:@"billCell"];
    [self.tableView registerClass:[PMSureOrderTVCell class] forCellReuseIdentifier:@"tvCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    //[self createNoTouchView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(self.isReback)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getRedData:@"" AndMon:@""];
    self.noTouchView.hidden = YES;
    [self.gifIV stopGIF];
}

- (void)createUI
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 50 - 64)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    self.payArray = @[@"商品总价",@"商品运费",@"红包抵扣"];
    [self initTabBar];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 3)
    {
        if (self.isOpen)
        {
            return 2;
        }
        else if (self.isTradeBack && [YBBillInfos shareInstance].billNum && [YBBillInfos shareInstance].billType && [YBBillInfos shareInstance].billContent)
        {
            return 2;
        }
        else if (self.isTradeBack && (![YBBillInfos shareInstance].billNum || ![YBBillInfos shareInstance].billType || ![YBBillInfos shareInstance].billContent))
        {
            return 1;
        }
        else
        {
            return 1;
        }
            
    }
    else if(section == 1)
    {
//        if (self.goodIsOpen)
//        {
//            return 1 + self.productsArray.count;
//        }
//        else
//        {
//            return 1;
//        }
        return 1;
    }
    else if (section == 4)
    {
        //return 2;
        return 1;
    }
    else if (section == 5)
    {
        return 3;
    }
    else if (section == 0)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row != 0)
    {
        YBAddressCell *cell = [[YBAddressCell alloc] init];
        if ([self.addressDic isKindOfClass:[NSDictionary class]] && !isNull(self.addressDic))
        {
            cell.addressDic = self.addressDic;
        }
        cell.isTradeBack = self.isTradeBack;
        cell.contentView.userInteractionEnabled = YES;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 0 && indexPath.row == 0)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addressCell"];
        cell.textLabel.text = @"收货人信息";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(indexPath.section == 2)
    {
        YBPayCell *cell = [[YBPayCell alloc] init];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.myDelegate = self;
        return cell;
    }
    else if (indexPath.section == 3 && indexPath.row == 0)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"billCell"];
        cell.textLabel.text = @"发票信息";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UISwitch *billSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        if (self.isTradeBack)
        {
            if ([YBBillInfos shareInstance].billNum && [YBBillInfos shareInstance].billType && [YBBillInfos shareInstance].billContent) {
                billSwitch.on = YES;
                billSwitch.enabled = NO;
            }
            else
            {
                billSwitch.on = NO;
                billSwitch.enabled = NO;
            }
        }
        else
        {
            billSwitch.on = self.isOpen;
            [billSwitch addTarget:self action:@selector(billShow:) forControlEvents:UIControlEventValueChanged];
        }
        cell.accessoryView = billSwitch;
        
        return cell;
    }
    else if (indexPath.section == 3 && indexPath.row == 1)
    {
        YBBillCell *cell = [[YBBillCell alloc] init];
        self.billCell = cell;
        cell.isTradeBack = self.isTradeBack;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(indexPath.section == 1 && indexPath.row == 0)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"goodTitleCell"];
        
        CGFloat lastX = 0;
        for (NSInteger i = 0; i < self.productsArray.count; i++)
        {
            UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(lastX + WidthRate(30), HeightRate(30), HeightRate(140), HeightRate(140))];
            productIV.contentMode = UIViewContentModeScaleAspectFit;
            NSString *imageUrl = @"";
            if (self.isTradeBack)
            {
                imageUrl = [[self.productsArray[i] objectNullForKey:@"product"] objectNullForKey:@"productLogo"];
            }
            else
            {
                imageUrl = [[self.productsArray[i] objectNullForKey:@"product"] objectNullForKey:@"productLogo"];
            }
            [productIV sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"loading_image"]];
            lastX += HeightRate(160);
            if (lastX > WIDTH - WidthRate(140))
            {
                UILabel *saveLB = [[UILabel alloc] initWithFrame:CGRectMake(lastX + WidthRate(20), HeightRate(60), WIDTH - WidthRate(140) - lastX - HeightRate(160), HeightRate(80))];
                saveLB.font = [UIFont systemFontOfSize:30.0f];
                saveLB.textColor = [UIColor darkGrayColor];
                saveLB.text = @"· · ·";
                [cell.contentView addSubview:saveLB];
                
                break;
            }
            else
            {
                [cell.contentView addSubview:productIV];
            }
        }
        
        
        UIView *acView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WidthRate(180), HeightRate(35))];
        UILabel *countLabel = [[UILabel  alloc] initWithFrame:CGRectMake(0, 0, acView.frame.size.width - WidthRate(50), HeightRate(35))];
        NSInteger productCount = 0;
        if (self.isTradeBack)
        {
            for (NSDictionary *productDic in self.productsArray)
            {
                NSInteger aProductCount = [[[productDic objectNullForKey:@"product"] objectNullForKey:@"productNum" ]integerValue];
                productCount += aProductCount;
            }
        }
        else
        {
            for (NSDictionary *productDic in self.productsArray)
            {
                NSInteger aProductCount = [[productDic objectNullForKey:@"productCount"] integerValue];
                productCount += aProductCount;
            }
        }
        countLabel.text = [NSString stringWithFormat:@"共%ld件",(unsigned long)productCount];
        countLabel.textColor = GoodsClassifyColor;
        countLabel.font = [UIFont systemFontOfSize:14.0f];
        countLabel.textAlignment = NSTextAlignmentRight;
        [acView addSubview:countLabel];
        
        UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(acView.frame.size.width - 20, iPhone4s ? -3 : -1, 20, 20)];
        arrow.image = [UIImage imageNamed:@"arrowRight"];
        self.goodArrowImageView = arrow;
        CGFloat angle = self.goodIsOpen ? M_PI_2 : 0;
        [self.goodArrowImageView setTransform:CGAffineTransformMakeRotation(angle)];
        [acView addSubview:arrow];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = acView;
        
        return cell;
    }
    else if (indexPath.section == 4)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"goldCell"];
        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
        cell.textLabel.textColor = HDVGFontColor;
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        if (indexPath.row == 0)
        {
            if (self.isTradeBack)
            {
                cell.textLabel.text = @"使用红包";
                cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
                cell.textLabel.textColor = [UIColor blackColor];
                cell.detailTextLabel.text = @"该订单已生成, 无法再使用红包抵扣";
                cell.detailTextLabel.textColor = HDVGRed;
                UISwitch *goldSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
                goldSwitch.on = self.isUseGold;
                [goldSwitch addTarget:self action:@selector(userDiscount:) forControlEvents:UIControlEventValueChanged];
                goldSwitch.tag = 300;
                self.goldSwitch = goldSwitch;
                cell.accessoryView = goldSwitch;
                goldSwitch.enabled = NO;
            }
            else if (self.isHasActivityProduct)
            {
                cell.textLabel.text = @"使用红包";
                cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
                cell.textLabel.textColor = [UIColor blackColor];
                cell.detailTextLabel.text = @"订单中包含活动商品,无法使用红包抵扣";
                cell.detailTextLabel.textColor = HDVGRed;
                UISwitch *goldSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
                goldSwitch.on = self.isUseGold;
                [goldSwitch addTarget:self action:@selector(userDiscount:) forControlEvents:UIControlEventValueChanged];
                goldSwitch.tag = 300;
                self.goldSwitch = goldSwitch;
                cell.accessoryView = goldSwitch;
                goldSwitch.enabled = NO;
            }
            else
            {
                cell.textLabel.text = @"使用红包";
                cell.detailTextLabel.text = [NSString stringWithFormat:@"红包余额%.2lf元,本次可抵%.2lf元",self.changeGoldMoney,self.userFulRedMoney];
                UISwitch *goldSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
                goldSwitch.on = self.isUseGold;
                [goldSwitch addTarget:self action:@selector(userDiscount:) forControlEvents:UIControlEventValueChanged];
                goldSwitch.tag = 300;
                self.goldSwitch = goldSwitch;
                cell.accessoryView = goldSwitch;
            }
            
        }
//        else
//        {
//            cell.textLabel.text = @"使用微购代金卷";
//            cell.detailTextLabel.text = [NSString stringWithFormat:@"本次可使用%d元的代金卷,可优惠%d元",self.goldTicket,self.goldTicket];
//            UISwitch *ticketSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
//            ticketSwitch.tag = 400;
//            ticketSwitch.on = NO;
//            [ticketSwitch addTarget:self action:@selector(userDiscount:) forControlEvents:UIControlEventValueChanged];
//            self.ticketSwitch = ticketSwitch;
//            cell.accessoryView = ticketSwitch;
//        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"moneyCountCell"];
        cell.textLabel.text = self.payArray[indexPath.row];
        cell.textLabel.textColor = HDVGFontColor;
        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, HeightRate(60))];
        label.font = [UIFont systemFontOfSize:14.0f];
        label.textColor = [UIColor redColor];
        if (indexPath.row == 0)
        {
            label.text = [NSString stringWithFormat:@"  ￥%.2lf",[self.totalPrice doubleValue]];
        }
        else if (indexPath.row == 1)
        {
            label.text = [NSString stringWithFormat:@"+  ￥%.2lf",self.carriagePrice];
        }
        else if (indexPath.row == 2)
        {
            if (self.isTradeBack)
            {
                
                label.text = [NSString stringWithFormat:@"- ￥%.2lf",[self.totalPrice doubleValue] - [self.actuallyPay doubleValue]];
            }
            else
            {
                if (self.isUseGold)
                {
                    label.text = [NSString stringWithFormat:@"-  ￥%.2lf",self.userFulRedMoney];
                }
                else
                {
                    label.text = @"-  ￥0.00";
                }
            }
//            CALayer *bottomLine = [CALayer layer];
//            bottomLine.backgroundColor = RGBA(236, 236, 236, 1).CGColor;
//            bottomLine.frame = CGRectMake(0, cell.frame.size.height, WIDTH, 1);
//            [cell.layer addSublayer:bottomLine];
        }
//        else if (indexPath.row == 3)
//        {
//            double trueTotalMoney = [self.totalPrice doubleValue] + self.transportationMoney;
//            
//            
//            if (self.isUseGold)
//            {
//                trueTotalMoney -= self.goldCount;
//            }
//            double trueMoney = self.packageMoney > trueTotalMoney ? trueTotalMoney : self.packageMoney;
//            
//            label.text = self.isUserPackage ? [NSString stringWithFormat:@"-  ￥%.2lf",trueMoney] : @"-  ￥0.00";
//        }
        
        label.textAlignment = NSTextAlignmentRight;
        cell.accessoryView = label;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && indexPath.row == 0)
    {
        return HeightRate(90);
    }
    else if (indexPath.section == 0 && indexPath.row != 0)
    {
//        return HeightRate(150);
        if ([self.addressDic isKindOfClass:[NSDictionary class]] && !isNull(self.addressDic))
        {
            NSString *area = [NSString stringWithFormat:@"%@ %@",[self.addressDic objectNullForKey:@"areas"],[self.addressDic objectNullForKey:@"address"]];
            CGSize areaSize = [area sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(WIDTH - itemX * 2, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
            
            return areaSize.height + HeightRate(80);
        }
        else
        {
            return HeightRate(150);
        }
       
        
    }
    else if(indexPath.section == 2)
    {
        return HeightRate(178);
    }
    else if(indexPath.section == 3 && indexPath.row == 0)
    {
        return HeightRate(90);
    }
    else if(indexPath.section == 3 && indexPath.row == 1)
    {
        return 44*3;
    }
    else if (indexPath.section == 1 && indexPath.row == 0)
    {
        return HeightRate(200);
    }
    else if (indexPath.section == 4)
    {
        return WidthRate(120);
    }
    else if (indexPath.section == 5)
    {
        return HeightRate(60);
    }
    else
    {
        return HeightRate(290);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    return HeightRate(10);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row != 0)
    {
        if (!self.isTradeBack)
        {
            PMChooseAddressVC *vc = [[PMChooseAddressVC alloc] init];
            vc.productArr = self.productsArray;
            [vc hideAccessory];
            vc.sureOrderVC = self;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else if (indexPath.section == 0 && indexPath.row == 0)
    {
        self.isAddressOpen = !self.isAddressOpen;
        [self refreshData:indexPath.section];
    }
    else if (indexPath.section == 1 && indexPath.row == 0)
    {
        OrderListViewController *orderVC = [[OrderListViewController alloc] init];
        orderVC.productArr = self.productsArray;
        orderVC.isTradeBack = self.isTradeBack;
        orderVC.isFromOrderDetail = NO;
        [self.navigationController pushViewController:orderVC animated:YES];
    }
}

- (void)initTabBar
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50, WIDTH, 50)];
    view.backgroundColor = RGBA(71, 70, 70, 1);
    [self.view addSubview:view];
    
    //    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(itemX, HeightRate(20), WidthRate(140), HeightRate(40))];
    //    lb.text = @"实付款:";
    //    lb.textColor = [UIColor whiteColor];
    //    lb.font = [UIFont boldSystemFontOfSize:HeightRate(28)];
    //    [view addSubview:lb];
    
    UILabel *allTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, 0, WidthRate(100), 48)];
    allTitleLB.text = @"总价:";
    allTitleLB.font = [UIFont systemFontOfSize:17.0f];
    allTitleLB.textColor = [UIColor redColor];
    [view addSubview:allTitleLB];
    
    
    UILabel *money = [[UILabel alloc] initWithFrame:CGRectMake(itemX + WidthRate(100), 0, WidthRate(300), 50)];
    if (self.isTradeBack)
    {
        money.text = [NSString stringWithFormat:@"￥%.2lf",[self.actuallyPay doubleValue] + self.carriagePrice];
    }
    else
    {
        money.text = [NSString stringWithFormat:@"￥%.2lf",[self.totalPrice doubleValue] + self.carriagePrice];
    }
    
    self.moneyLabel = money;
    money.textColor = [UIColor whiteColor];
    money.font = [UIFont boldSystemFontOfSize:18];
    [view addSubview:money];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(WidthRate(500), 0,view.frame.size.width - WidthRate(500), view.frame.size.height);
    btn.backgroundColor = RGBA(202, 34, 50, 1);
//    [btn.layer setCornerRadius:6.0];
    [btn setTitle:@"确认支付" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [btn addTarget:self action:@selector(surePay:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    
}

- (void)surePay:(UIButton *)sender
{
//    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"系统升级，暂时无法支付!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [av show];
    
    [self.billCell.tf resignFirstResponder];
    if(isNull(self.addressDic))
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请选择收货地址" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [av show];
    }
    else if (self.isOpen && (![YBBillInfos shareInstance].billNum || ![YBBillInfos shareInstance].billType || ![YBBillInfos shareInstance].billContent))
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"发票信息不完整" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [av show];
    }
    else
    {
        self.view.userInteractionEnabled = NO;
        if (!self.isTradeBack)
        {
            self.noTouchView.hidden = NO;
            [self.gifIV startGIF];
        }
        NSLog(@"productArr --> %@",self.productsArray);
        if (!self.isTradeBack)
        {
            NSMutableArray *idsArray = [NSMutableArray array];
            //向服务器发送 '提交订单' 请求
            NSMutableString *products = [NSMutableString string];
            for (NSInteger i = 0; i < self.productsArray.count; i++)
            {
                NSDictionary *productDic = self.productsArray[i];
                NSString *productId = [[productDic objectNullForKey:@"product"] objectNullForKey:@"id"];
//                NSString *aSpecValId = [productDic objectNullForKey:@"aSpecValId"];
//                NSString *bSpecValId = [productDic objectNullForKey:@"bSpecValId"];
//                NSString *cSpecValId = [productDic objectNullForKey:@"cSpecValId"];
//                NSString *dSpecValId = [productDic objectNullForKey:@"dSpecValId"];
//                NSString *eSpecValId = [productDic objectNullForKey:@"eSpecValId"];
                NSString *stockId = [productDic objectForKey:@"stockId"];
                NSString *buyNum = [productDic objectNullForKey:@"productCount"];
                if (i == 0)
                {
                    [products appendString:@"[{"];
                }
                else
                {
                    [products appendString:@",{"];
                }
                NSString *productString = [NSString stringWithFormat:@"\"productId\":\"%@\",\"stockId\":\"%@\",\"buyNum\":\"%@\"",productId,stockId,buyNum];
                [products appendString:productString];
                
                if (i == self.productsArray.count - 1)
                {
                    [products appendString:@"}]"];
                }
                else
                {
                    [products appendString:@"}"];
                }
            }
            NSDictionary *parameter = @{@"products":products};
            [[PMNetworking defaultNetworking] request:PMRequestStateCheckStockTotal WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    if ([[dic objectNullForKey:@"messageCode"] integerValue] == 0)
                    {
                        for (NSDictionary *idsDic in self.productsArray)
                        {
                            NSLog(@"idsDic -- >%@",idsDic);
                            NSString *productId;
                            NSString *areaId;
//                            NSString *aSpecValId;
//                            NSString *bSpecValId;
//                            NSString *cSpecValId;
//                            NSString *dSpecValId;
//                            NSString *eSpecValId;
                            NSString *shopId;
                            NSString *productNum;
                            NSString *stockId;
                            if (!self.isTradeBack)
                            {
                                productId = [[idsDic objectNullForKey:@"product"] objectNullForKey:@"id"];
                                areaId = [idsDic objectNullForKey:@"areaId"];
                                stockId = [idsDic objectNullForKey:@"stockId"];
//                                shopId = [idsDic objectNullForKey:@"shopId"];
                                if ([[idsDic objectNullForKey:@"shopHome"] isKindOfClass:[NSDictionary class]])
                                {
                                    shopId = [[idsDic objectNullForKey:@"shopHome"] objectNullForKey:@"id"];
                                }
                                else
                                {
                                    shopId = @"";
                                }
                                productNum = [idsDic objectNullForKey:@"productCount"];
                            }
                            
                            NSString *idsString = [NSString stringWithFormat:@"%@_%@_%@_%@_%@",productId,areaId,stockId,shopId,productNum];
                            [idsArray addObject:idsString];
                        }
                        
                        NSString *ids = [idsArray componentsJoinedByString:@","];
                        NSDictionary *param = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID,@"ids":ids};
                        NSLog(@"ids --> %@",param);
                        [[PMNetworking defaultNetworking] request:PMRequestStateShowOrder WithParameters:param callBackBlock:^(NSDictionary *dic) {
                            if([[dic objectNullForKey:@"success"] integerValue] == 1)
                            {
                                //跳转到支付页面,并标记
                                self.isReback = YES;
                                self.carVC.carSeletedArr = [NSMutableArray array];
                                [self submitOrder];
                                self.view.userInteractionEnabled = YES;
                            }
                            else {
                                self.view.userInteractionEnabled = YES;
                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dic objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                                [av show];
                                
                            }
                        }showIndicator:YES];
                        
                    }
                    else
                    {
                        self.view.userInteractionEnabled = YES;
                        NSMutableString *noEnoughString = [NSMutableString string];
                        NSArray *dataArr = [dic objectNullForKey:@"data"];
                        for (NSDictionary *dataDic in dataArr)
                        {
                            NSString *productName = [dataDic objectNullForKey:@"productName"];
                            [noEnoughString appendString:productName];
                        }
                        
                        NSString *alertMessage = [NSString stringWithFormat:@"%@\n库存不足",noEnoughString];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:alertMessage delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
                else
                {
                    self.view.userInteractionEnabled = YES;
                    self.noTouchView.hidden = YES;
                    [self.gifIV stopGIF];
                    showRequestFailAlertView;
                    
                }
                
                
            } showIndicator:NO];
        }
        else
        {
            //跳转到支付页面,并标记
            self.isReback = YES;
            
            [self submitOrder];
        }
    }
   
}

#pragma mark - webBrowser代理方法
//支付完成跳转到根视图控制器
- (void)webBrowser:(KINWebBrowserViewController *)webBrowser didFinishLoadingURL:(NSURL *)URL
{
    NSString *urlString = [NSString stringWithFormat:@"%@",URL];
    
    //点击返回商户时，链接会有returnPay这个字段，识别到这个字段就退出页面。
    if([urlString rangeOfString:@"returnPay"].length >0)
    {
        if (self.isPrePay)
        {
            //标记预售支付成功
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:PREPAY_STATE];
            [webBrowser.navigationController popViewControllerAnimated:YES];
        }
        else {
            //动画为NO防止出现空白页面
            [webBrowser.navigationController popToRootViewControllerAnimated:NO];
            //跳转到订单页面
            [[MasterViewController defaultMasterVC] transitionToMyTrade];
        }
    }
}


#pragma mark - 数据处理
//提交订单并跳转到支付页面
- (void)submitOrder
{
    PMNetworking *nw = [PMNetworking defaultNetworking];
    
    //微支付和支付宝支付参数
    NSMutableDictionary *param = nil;
    NSString *addressId = @"";
    if (!self.isTradeBack)
    {
        if ([self.addressDic isKindOfClass:[NSDictionary class]] && !isNull(self.addressDic))
        {
            addressId = [self.addressDic objectNullForKey:@"id"];
        }
        else
        {
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"您还没有地址呢!"];
            return;
        }
        
    
        if (self.isWX)
        {
            param = [@{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID,@"defaultAddressId":addressId,@"payWay":@"alipay"} mutableCopy];
        }
        else
        {
            param = [@{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID,@"defaultAddressId":[self.addressDic objectNullForKey:@"id"],@"payWay":@"alipay"} mutableCopy];
        }
        
        if ([YBBillInfos shareInstance].billNum && [YBBillInfos shareInstance].billType && [YBBillInfos shareInstance].billContent)
        {
            [param setValue:[YBBillInfos shareInstance].billNum forKey:@"receiptTitle"];
            [param setValue:[NSString stringWithFormat:@"%ld",(long)[YBBillInfos shareInstance].billType] forKey:@"receiptType"];
            [param setValue:[NSString stringWithFormat:@"%ld",(long)[YBBillInfos shareInstance].billContent] forKey:@"receiptContent"];
        }
        
        NSString *userRedPack = self.isUseGold ? @"yes" : @"no";
        [param setValue:userRedPack forKey:@"isUseRedPack"];
        [param setValue:[NSString stringWithFormat:@"%.2lf",([self.totalPrice doubleValue] - self.userFulRedMoney)] forKey:@"actuallyPay"];
        [param setValue:[NSString stringWithFormat:@"%.2lf",self.userFulRedMoney] forKey:@"redPackUsedMoney"];
        [param setValue:[NSString stringWithFormat:@"%.2lf",self.goldCount - self.userFulRedMoney] forKey:@"redPackLeaveTotalMoney"];
        
        [nw request:PMRequestStateSubmitOrder WithParameters:param callBackBlock:^(NSDictionary *dic) {
            if([[dic objectNullForKey:@"success"] integerValue] == 1)
            {
                NSMutableArray *namesArr = [NSMutableArray array];
                //得到所有商品名称
                for (NSDictionary *dic in self.productsArray)
                {
                    NSDictionary *productDic = [dic objectNullForKey:@"product"];
                    [namesArr addObject:[productDic objectNullForKey:@"productName"]];
                }
                NSString *names = [namesArr componentsJoinedByString:@","];
                
                //得到订单号
                NSString *orderCode = [dic objectNullForKey:@"data"];
                
                
                NSDictionary *param = nil;
                
                if (self.isWX)
                {
                    param = @{PMSID,@"orderCode":orderCode};
                    [[PMNetworking defaultNetworking] request:PMRequestStateGetWXPay WithParameters:param callBackBlock:^(NSDictionary *adic) {
            
                        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
                        [app weixinPayWithRequsetData:adic];
                        self.noTouchView.hidden = YES;
                    } showIndicator:YES];
                }
                else
                {
                    self.noTouchView.hidden = YES;
                    [self.gifIV stopGIF];
                    param = @{PMSID,@"orderCode":orderCode,@"totalPrice":@(self.isTradeBack ? [self.actuallyPay doubleValue] + self.carriagePrice : self.isUseGold ? ([self.totalPrice doubleValue] + self.carriagePrice - self.userFulRedMoney) : [self.totalPrice doubleValue] + self.carriagePrice),@"totalNum":@(self.totalProductNum),@"pro":names};
                    [[PMNetworking defaultNetworking] alipay:param target:self Index:0];
                    
                }
                
                
                _isPay = YES;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"changeNum" object:nil userInfo:nil];
            }
            else
            {
                self.noTouchView.hidden = YES;
                [self.gifIV stopGIF];
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dic objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [av show];
            }
        }showIndicator:YES];
    }
    else
    {//我的交易付款  项进入
        
        
        
       __block NSDictionary *param = nil;
        
        NSMutableArray *namesArr = [NSMutableArray array];
        for (NSDictionary *dic in self.productsArray)
        {
            NSDictionary *productDic = [dic objectNullForKey:@"product"];
            [namesArr addObject:[productDic objectNullForKey:@"productName"]];
        }
        NSString *names = [namesArr componentsJoinedByString:@","];
        
        NSDictionary *dic = self.productsArray[0];
        NSDictionary *productDic = [dic objectNullForKey:@"product"];
        NSString *orderCode = productDic[@"orderCode"];
        
        if (self.isWX)
        {
            
            param = @{PMSID,@"orderCode":orderCode};
            [[PMNetworking defaultNetworking] request:PMRequestStateGetWXPay WithParameters:param callBackBlock:^(NSDictionary *dic) {
                
                if ([dic isKindOfClass:[NSDictionary class]])
                {
                    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
                    [app weixinPayWithRequsetData:dic];
                    self.noTouchView.hidden = YES;
                }
                else
                {
                    self.noTouchView.hidden = YES;
                }
                
                
            } showIndicator:YES];
        }
        else
        {
            param = @{PMSID,@"orderCode":orderCode,@"totalPrice":@([self.totalPrice doubleValue]),@"totalNum":@(self.totalProductNum),@"pro":names};
            [[PMNetworking defaultNetworking] alipay:param target:self Index:0];
        }
        
//        [[PMNetworking defaultNetworking] request:PMRequestStateCheckOrderStatus WithParameters:@{PMSID,@"orderCode":orderCode} callBackBlock:^(NSDictionary *dic) {
//            if (intSuccess == 1)
//            {
//                if (self.isWX)
//                {
//                    
//                    param = @{PMSID,@"orderCode":orderCode};
//                    [[PMNetworking defaultNetworking] request:PMRequestStateGetWXPay WithParameters:param callBackBlock:^(NSDictionary *dic) {
//                        
//                        if ([dic isKindOfClass:[NSDictionary class]])
//                        {
//                            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//                            [app weixinPayWithRequsetData:dic];
//                            self.noTouchView.hidden = YES;
//                        }
//                        else
//                        {
//                            self.noTouchView.hidden = YES;
//                        }
//                        
//                        
//                    } showIndicator:YES];
//                }
//                else
//                {
//                    param = @{PMSID,@"orderCode":orderCode,@"totalPrice":@([self.totalPrice doubleValue]),@"totalNum":@(self.totalProductNum),@"pro":names};
//                    [[PMNetworking defaultNetworking] alipay:param target:self Index:0];
//                }
//            }
//            else
//            {
//                NSString *errorMsg = [dic objectNullForKey:@"message"];
//                [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:errorMsg];
//            }
//            
//            
//        } showIndicator:YES];
        
        
    }
   
}

#pragma mark - alertView代理方法
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        self.isReback = YES;
        
        [self submitOrder];
    }
    else if ([alertView.message hasPrefix:@"付款"] || [alertView.title hasPrefix:@"订金"])
    {
        if (buttonIndex == 0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)successPay
{
    DetailTradeViewController *detail = [[DetailTradeViewController alloc] init];
    detail.isFromPay = YES;
    detail.tradeStatus = tradeStatusWaitGet;
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)failPay:(int)errorCode
{
    DetailTradeViewController *detail = [[DetailTradeViewController alloc] init];
    detail.isFromPay = YES;
    detail.tradeStatus = tradeStatusWaitPay;
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma mark - 钱包开关值改变
- (void)valueChange:(UISwitch *)switchButton
{
    self.isUserPackage = switchButton.isOn;
    
    double trueTotleMoney = 0;
    if (self.isTradeBack)
    {
        trueTotleMoney  = [self.actuallyPay doubleValue] + self.transportationMoney;
    }
    else
    {
        trueTotleMoney  = [self.totalPrice doubleValue] + self.transportationMoney;
    }
    
    if (self.isUseGold)
    {
        trueTotleMoney -= self.goldCount;
    }
    
    if (self.isUserPackage)
    {
        if (self.packageMoney >= trueTotleMoney)
        {
            self.moneyLabel.text = [NSString stringWithFormat:@"￥0.00"];
            self.changePackageMoney = self.packageMoney - trueTotleMoney;
        }
        else
        {
            self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2lf",trueTotleMoney - self.packageMoney];
            self.changePackageMoney = 0;
        }
    }
    else
    {
        self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2lf",trueTotleMoney];
        self.changePackageMoney = self.packageMoney;
    }
    
    //[self refreshData:6];
    
    [self refreshData:5];
}

#pragma mark - 使用金条和优惠劵的开关
- (void)userDiscount:(UISwitch *)mySwitch
{
    if (mySwitch.tag == 300)
    {
        self.isUseGold = mySwitch.isOn;
        double trueTotleMoney = 0;
        if (self.isTradeBack)
        {
           trueTotleMoney = [self.actuallyPay doubleValue] + self.carriagePrice;
        }
        else
        {
            trueTotleMoney = [self.totalPrice doubleValue] + self.carriagePrice;
        }
        if (self.isUseGold)
        {
            self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2lf",trueTotleMoney - self.userFulRedMoney];
            self.changeGoldMoney -= self.userFulRedMoney;
            
        }
        else
        {
            self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2lf",trueTotleMoney];
            self.changeGoldMoney = self.goldCount;
        }
        [self refreshData:4];
        [self refreshData:5];
        
        
    }
    else
    {
        self.ticketSwitch.on = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"该功能尚未开放" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - 重载特定section数据
- (void)refreshData:(NSUInteger)index
{
    NSIndexSet *indexSet=[[NSIndexSet alloc] initWithIndex:index];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    if (self.isTradeBack)
    {
        self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2lf",[_actuallyPay doubleValue] + self.carriagePrice];
    }
    else
    {
        if (self.isUseGold)
        {
            self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2lf",[_totalPrice doubleValue] + self.carriagePrice - self.userFulRedMoney];
        }
        else
        {
            self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2lf",[_totalPrice doubleValue] + self.carriagePrice];
        }
    }
    
}

#pragma mark - 请求网络获取红包详细情况
- (void)getRedData:(NSString *)year AndMon:(NSString *)month
{
    PMNetworking *netWorking = [PMNetworking defaultNetworking];
    NSDictionary *parameter = @{@"year":@"",@"month":@"",@"PM_SID":[PMUserInfos shareUserInfo].PM_SID};
    [netWorking request:PMRequestStateGetOptTradeDetail WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
        
        
        if ([[dic objectNullForKey:@"data"] isKindOfClass:[NSString class]])
        {
            
        }
        else
        {
//            NSArray *redArray = [dic objectNullForKey:@"data"];
//            NSDictionary *detailDic = redArray[0];
            NSDictionary *detailDic = [[dic objectForKey:@"data"] objectForKey:@"total"];
            double myHdMoney = [[detailDic objectNullForKey:@"myHdMoney"] doubleValue];
            self.goldCount = myHdMoney;
            self.changeGoldMoney = myHdMoney;
//            self.ydUserFulRedMoney = [self.totalPrice doubleValue] * self.orderRedLimit;
            self.ydUserFulRedMoney = [self.redPackageTotalPrice doubleValue] * self.orderRedLimit;
            self.userFulRedMoney = (self.ydUserFulRedMoney > self.goldCount) ? self.goldCount : self.ydUserFulRedMoney;
        
        [self refreshData:4];
        }
        
    } showIndicator:YES];
}

- (void)billShow:(UISwitch *)sender
{
    sender.on = !sender.isOn;
    self.isOpen = !self.isOpen;
    [self refreshData:3];
}

- (void)createNoTouchView
{
    UIView *noTouchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    noTouchView.backgroundColor = [UIColor clearColor];
    
    UIView *noTouchAlphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    noTouchAlphaView .backgroundColor = [UIColor whiteColor];
    noTouchAlphaView.alpha = 0.6;
    [noTouchView addSubview:noTouchAlphaView];
    
    UIImageView *gifIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    gifIV.center = CGPointMake(noTouchView.bounds.size.width / 2, noTouchView.bounds.size.height / 2);
    self.gifIV = gifIV;
    gifIV.gifPath = [[NSBundle mainBundle] pathForResource:@"loading" ofType:@"gif"];
    gifIV.contentMode = UIViewContentModeScaleAspectFit;
    [gifIV startGIF];
    [noTouchView addSubview:gifIV];
    self.noTouchView = noTouchView;
    noTouchView.hidden = YES;
    noTouchView.tag = 920109;
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    [window addSubview:noTouchView];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + 40, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
//    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
//        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
//        [self.tableView setContentOffset:scrollPoint animated:YES];
//    }
}

- (void)keyboardWillHide:(NSNotification *)notify
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.view.userInteractionEnabled = YES;
}

@end
