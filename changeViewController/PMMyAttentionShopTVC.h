//
//  PMMyAttentionShopTVC.h
//  changeViewController
//
//  Created by pmit on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMAttentionShopCell.h"
#import "PMBrowser.h"

@interface PMMyAttentionShopTVC : UITableViewController<KINWebBrowserDelegate>
@property(nonatomic,strong)NSMutableArray *dicArray;

- (void)deleteSelectedRows;

@end
