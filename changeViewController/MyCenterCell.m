//
//  MyCenterCell.m
//  changeViewController
//
//  Created by pmit on 15/7/3.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "MyCenterCell.h"
#import "PMMyPhoneInfo.h"

@implementation MyCenterCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.iconIV)
    {
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 20, 20)];
        self.iconIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.iconIV];
        
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, WIDTH - 120, 50)];
        self.titleLB.textColor = RGBA(51, 51, 51, 1);
        self.titleLB.font = [UIFont systemFontOfSize:16.0f];
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.titleLB];
        
        UIView *accessView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH - 170, 15, 160, 20)];
        
        self.tipLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 130, 20)];
        self.tipLB.font = [UIFont systemFontOfSize:12.0f];
        self.tipLB.textColor = RGBA(153, 153, 153, 1);
        self.tipLB.textAlignment = NSTextAlignmentRight;
        [accessView addSubview:self.tipLB];
        
        UIImageView *bottomArrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(130, 0, 20, 20)];
        bottomArrowIV.image = [UIImage imageNamed:@"arrowRight"];
        [accessView addSubview:bottomArrowIV];
        
        [self.contentView addSubview:accessView];
        
        CALayer *line = [CALayer layer];
        line.backgroundColor = HDVGPageBGGray.CGColor;
        line.frame = CGRectMake(40, 49, WIDTH - 40, 1);
        [self.layer addSublayer:line];
    }
}

@end
