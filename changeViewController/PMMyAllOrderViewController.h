//
//  PMMyAllOrderViewController.h
//  changeViewController
//
//  Created by pmit on 15/10/29.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTChatroomLoadMoreView.h"

@interface PMMyAllOrderViewController : UIViewController

@property (strong,nonatomic) UITableView *waitPayTableView;
@property (strong,nonatomic) UITableView *waitGetTableView;
@property (strong,nonatomic) UITableView *waitDiscussTableView;

@property (assign,nonatomic) BOOL isWaitPayLoadMore;
@property (strong,nonatomic) ZSTChatroomLoadMoreView *waitPayLoadMoreView;
@property (assign,nonatomic) BOOL isWaitPayLoading;
@property (assign,nonatomic) BOOL isWaitPayShow;

@property (assign,nonatomic) BOOL isWaitGetLoadMore;
@property (strong,nonatomic) ZSTChatroomLoadMoreView *waitGetLoadMoreView;
@property (assign,nonatomic) BOOL isWaitGetLoading;
@property (assign,nonatomic) BOOL isWaitGetShow;

@property (assign,nonatomic) BOOL isWaitDiscussLoadMore;
@property (strong,nonatomic) ZSTChatroomLoadMoreView *waitDiscussLoadMoreView;
@property (assign,nonatomic) BOOL isWaitDiscussLoading;
@property (assign,nonatomic) BOOL isWaitDiscussShow;

- (void)getOtherOrderByNet;

@end
