//
//  LogisticsDetailViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/27.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

// 物流详情控制器
#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"

@interface LogisticsDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *detailTableView;

@property (copy, nonatomic) NSDictionary *logisticsDict;

@property (strong, nonatomic) PMNetworking *networking;

@end
