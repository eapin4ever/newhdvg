//
//  MasterViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMSideBarCell.h"
#import "GetMyLocation.h"
#import "PMNetworking.h"
#import "PMNavigationController.h"
#import "LoginViewController.h"
#import "PMUnreadMessage.h"
#import "MyAttentionViewController.h"
#import "DistributionShopViewController.h"
#import "GoodsListViewController.h"
#import "SettingViewController.h"
#import "PMImageCroperSingleton.h"
#import "PMNearbySearchViewController.h"

@interface MasterViewController : UIViewController
//用户位置
@property(nonatomic,strong)NSString *userLocation;

@property(nonatomic,strong)UITabBar *tabBar;
@property(nonatomic,strong)UIImageView *tabBarIV;
@property (strong,nonatomic) CLLocation *location;
@property (nonatomic,strong) UIButton *midBtn;

@property(nonatomic,strong)UIViewController *currentViewController;
@property(nonatomic,copy)myFinishBlock nearbySearchAgain;
@property(nonatomic,assign)BOOL isSearching;//判断智能搜是否正在搜索

@property(nonatomic,copy)myFinishBlock afterLoginAction;
@property (assign,nonatomic) BOOL isNoFinish;
@property (strong,nonatomic) UIImageView *noNetTipView;


+ (MasterViewController *)defaultMasterVC;


- (void)transitionToLoginVC;
- (void)transitionToMyTrade;
- (void)transitionToHomePage;
- (void)transitionToMyTrade2;
- (void)transitionToGetCashVC;

- (void)transitionToMyClass;

- (void)checkLogin;

- (void)userLogout;
- (void)showNoNetView;
- (void)hideNoNetView;
@end
