//
//  NewAccountViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "NewAccountViewController.h"
#import "LoginViewController.h"

@interface NewAccountViewController ()

@end

@implementation NewAccountViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"设置新账户";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createNewUserAndPassUI];
    
    [self createButtonControllerUI];
}

// 创建新用户账户和密码
- (void)createNewUserAndPassUI
{
    // 创建注册验证码 view
    self.accountAndPassView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(50), WIDTH, HeightRate(176))];
    self.accountAndPassView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.accountAndPassView];
    
    // 注册新账户昵称
    UILabel *newNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), HeightRate(20), WidthRate(120), HeightRate(48))];
    newNameLabel.backgroundColor = [UIColor clearColor];
    newNameLabel.text = @"昵称：";
    newNameLabel.textColor = RGBA(129, 129, 129, 1);
    newNameLabel.textAlignment = NSTextAlignmentLeft;
    newNameLabel.font = [UIFont systemFontOfSize:17.0f];
    [self.accountAndPassView addSubview:newNameLabel];
    
    self.aNewNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(170), HeightRate(20), WIDTH - WidthRate(170) - WidthRate(46) * 2, HeightRate(48))];
    self.aNewNameTextField.borderStyle = UITextBorderStyleNone;
    self.aNewNameTextField.delegate = self;
    self.aNewNameTextField.textAlignment = NSTextAlignmentLeft;
    self.aNewNameTextField.font = [UIFont systemFontOfSize:15.0f];
    self.aNewNameTextField.placeholder = @"请输入新昵称";
    self.aNewNameTextField.returnKeyType = UIReturnKeyDone;
    self.aNewNameTextField.keyboardType = UIReturnKeyDefault;
    [self.accountAndPassView addSubview:self.aNewNameTextField];
    
    // 画线
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(87), self.accountAndPassView.frame.size.width - WidthRate(30), HeightRate(1))];
    line.backgroundColor = RGBA(224, 224, 224, 1);
    [self.accountAndPassView addSubview:line];
    
    
    // 注册新账户密码
    UILabel *newPassLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), HeightRate(108), WidthRate(120), HeightRate(48))];
    newPassLabel.backgroundColor = [UIColor clearColor];
    newPassLabel.text = @"密码：";
    newPassLabel.textColor = RGBA(129, 129, 129, 1);
    newPassLabel.textAlignment = NSTextAlignmentLeft;
    newPassLabel.font = [UIFont systemFontOfSize:17.0f];
    [self.accountAndPassView addSubview:newPassLabel];
    
    self.aNewAccountPassTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(170), HeightRate(108), WIDTH - WidthRate(170) - WidthRate(10), HeightRate(48))];
    self.aNewAccountPassTF.borderStyle = UITextBorderStyleNone;
    self.aNewAccountPassTF.delegate = self;
    self.aNewAccountPassTF.textAlignment = NSTextAlignmentLeft;
    self.aNewAccountPassTF.font = [UIFont systemFontOfSize:14.0f];
    self.aNewAccountPassTF.placeholder = @"长度为5～16位的数字、字母、符号";
    self.aNewAccountPassTF.returnKeyType = UIReturnKeyDone;
    self.aNewAccountPassTF.keyboardType = UIReturnKeyDefault;
    [self.accountAndPassView addSubview:self.aNewAccountPassTF];
}

// 按下Done按钮的调用方法，让软键盘隐藏
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.aNewNameTextField resignFirstResponder];
    [self.aNewAccountPassTF resignFirstResponder];
}

// 创建按钮控件 UI
- (void)createButtonControllerUI
{
    // 初始化显示密码按钮
    UIButton *showButton = [UIButton buttonWithType:UIButtonTypeCustom];
    showButton.frame = CGRectMake(WIDTH - WidthRate(240), self.accountAndPassView.frame.origin.y + HeightRate(196), WidthRate(34), WidthRate(34));
    [showButton setImage:[UIImage imageNamed:@"showpass_yes.png"] forState:UIControlStateNormal];
    [showButton setImage:[UIImage imageNamed:@"showpass_no.png"] forState:UIControlStateSelected];
    [showButton addTarget:self action:@selector(showPassButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:showButton];
    
    UILabel *showPassLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(190), self.accountAndPassView.frame.origin.y + HeightRate(186), WidthRate(150), HeightRate(48))];
    showPassLab.backgroundColor = [UIColor clearColor];
    showPassLab.text = @"显示密码";
    showPassLab.textColor = RGBA(129, 129, 129, 1);
    showPassLab.textAlignment = NSTextAlignmentLeft;
    showPassLab.font = [UIFont systemFontOfSize:15.0f];
    [self.view addSubview:showPassLab];
    
    
    // 初始化完成设置按钮
    self.finishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.finishButton.frame = CGRectMake(WidthRate(46), self.accountAndPassView.frame.origin.y + HeightRate(256), WIDTH - WidthRate(46) * 2, HeightRate(90));
    [self.finishButton setTitle:@"完成设置" forState:UIControlStateNormal];
    [self.finishButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.finishButton addTarget:self action:@selector(finishButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.finishButton setBackgroundColor:RGBA(218, 67, 80, 1)];
    [self.finishButton.layer setCornerRadius:6.0f];
    [self.view addSubview:self.finishButton];
}

- (void)showPassButtonClick:(UIButton *)button
{
    // 改变按钮的选中状态
    button.selected = !button.selected;
    if (button.selected) {
        self.aNewAccountPassTF.secureTextEntry = NO;
    }
    if (!button.selected) {
        self.aNewAccountPassTF.secureTextEntry = YES;
    }
}

- (void)finishButtonClick:(id)sender
{
    // 姓名、密码不能为空
    if (self.aNewNameTextField.text.length == 0 || self.aNewAccountPassTF.text.length == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请按要求填写完整注册信息！" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
    }
    if (![self.aNewNameTextField.text isEqual:@""] && ![self.aNewAccountPassTF.text isEqual:@""]) {
        
        // 完成注册
        NSDictionary *nameAndPassString = @{@"name":self.aNewNameTextField.text,@"password":self.aNewAccountPassTF.text};
        
        
        [self.networking request:PMRequestStateRegister WithParameters:nameAndPassString callBackBlock:^(NSDictionary *dict) {
            
            //LoginViewController *loginVC = [[LoginViewController alloc] init];
            //LoginViewController *loginVC = [LoginViewController shareInstance];
            [self.navigationController pushViewController:[LoginViewController shareInstance] animated:YES];
            
        }showIndicator:NO];
        

    }
}

#pragma mark - textField delegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    /**
     * 检查用户名是否存在
     * 参数: userName:(string)用户名
     */
    NSDictionary *userNameString = @{@"userName":self.aNewNameTextField.text};
    self.networking = [PMNetworking defaultNetworking];
    
    __block NSDictionary *callBackDict;
    
    [self.networking request:PMRequestStateCheckClientUserName WithParameters:userNameString callBackBlock:^(NSDictionary *dict) {
        callBackDict = dict;
        
        //如果返回的信息中，success的值是true则用户已存在
        if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
            
            self.tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.aNewNameTextField.frame.size.width -  WidthRate(200), HeightRate(0), WidthRate(200), HeightRate(48))];
            self.tipLabel.backgroundColor = [UIColor clearColor];
            self.tipLabel.text = @"* 用户名已存在";
            self.tipLabel.font = [UIFont systemFontOfSize:12.0f];
            self.tipLabel.textAlignment = NSTextAlignmentRight;
            self.tipLabel.textColor = [UIColor redColor];
            [self.aNewNameTextField addSubview:self.tipLabel];
            [self.finishButton setUserInteractionEnabled:NO];
        }
        else if ([[callBackDict objectNullForKey:@"success"] boolValue] != 1) {
            
            //self.tipLabel.hidden = YES;
            for (id obj in self.aNewNameTextField.subviews) {
                if ([obj isKindOfClass:[UILabel class]]) {
                    [obj removeFromSuperview];
                    break;
                }
            };
        }
        if ([[callBackDict objectNullForKey:@"success"] boolValue] == 0) {
            [self.finishButton setUserInteractionEnabled:YES];
        }
    }showIndicator:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
