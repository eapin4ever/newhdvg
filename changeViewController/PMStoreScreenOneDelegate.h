//
//  PMStoreScreenOneDelegate.h
//  changeViewController
//
//  Created by P&M on 15/7/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PMStoreScreenTwoDelegate.h"

@class PMProductStoreViewController;

@protocol PMStoreScreenOneDelegate <NSObject>

- (void)passDictionary:(NSDictionary *)dict;

@end

@interface PMStoreScreenOneDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *storeScreenOneTV;
@property (strong, nonatomic) NSMutableArray *screenOneArray;
@property (strong, nonatomic) NSDictionary *dataDict;

@property (weak, nonatomic) id<PMStoreScreenOneDelegate> screenOneDelegate;

@end
