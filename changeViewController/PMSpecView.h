//
//  PMSpecView.h
//  changeViewController
//
//  Created by pmit on 15/6/11.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMSpecView : UIView

@property (strong,nonatomic) NSMutableArray *aBtnArray;
@property (strong,nonatomic) NSMutableArray *bBtnArray;
@property (strong,nonatomic) NSMutableArray *cBtnArray;
@property (strong,nonatomic) NSMutableArray *dBtnArray;
@property (strong,nonatomic) NSMutableArray *eBtnArray;
@property (copy,nonatomic) NSString *defaultAValueId;
@property (copy,nonatomic) NSString *defaultBValueId;
@property (copy,nonatomic) NSString *defaultCValueId;
@property (copy,nonatomic) NSString *defaultDValueId;
@property (copy,nonatomic) NSString *defaultEValueId;

- (void)createUIWithASpecName:(NSString *)aName BSpecName:(NSString *)bName CSpecName:(NSString *)cName DSpecName:(NSString *)dName ESpecName:(NSString *)eName ASpecArr:(NSArray *)ASpecArr BSpecArr:(NSArray *)BSpecArr CSpecArr:(NSArray *)CSpecArr DSpecArr:(NSArray *)DSpecArr ESpecArr:(NSArray *)ESpecArr;

@end
