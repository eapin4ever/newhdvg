//
//  PMPrebuyDetailCell.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define CELLHI 256/2
#define itemX WidthRate(240)
#define downItemY 174/2

#import "PMPrebuyDetailCell.h"

@implementation PMPrebuyDetailCell
{
    UILabel *_titleLB;
    UILabel *_priceLB;
    UILabel *_prepayLB;
}
- (void)createUI
{
    if(!_iv)
    {
        
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(32), CELLHI/2 - 40, 80, 80)];
        [self.contentView addSubview:_iv];
        
        _titleLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, 25, WidthRate(470), 35)];
        _titleLB.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_titleLB];
        
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(itemX, downItemY, 20, 20)];
        icon.image = [UIImage imageNamed:@"time"];
        [self.contentView addSubview:icon];
        
        _timeLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX + 25, downItemY, WidthRate(120), 20)];
        _timeLB.font = [UIFont systemFontOfSize:12];
        _timeLB.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_timeLB];
        
        _priceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(456), downItemY, WidthRate(140), 20)];
        _priceLB.textAlignment = NSTextAlignmentRight;
        _priceLB.font = [UIFont systemFontOfSize:16];
        _priceLB.textColor = HDVGRed;
        [self.contentView addSubview:_priceLB];
        
        _prepayLB = [[UILabel alloc] initWithFrame:CGRectMake(_priceLB.frame.origin.x+_priceLB.bounds.size.width+2, downItemY, WidthRate(135), 20)];
        _prepayLB.font = [UIFont systemFontOfSize:10];
        _prepayLB.textColor = [UIColor grayColor];
        [self.contentView addSubview:_prepayLB];
        
    }
}

- (void)setTitle:(NSString *)title time:(NSString *)time price:(NSString *)price prepay:(NSString *)prepay
{
    _titleLB.text = title;
    _timeLB.text = time;
    _priceLB.text = price;
    _prepayLB.text = [NSString stringWithFormat:@"(定金%@)",prepay];
}

@end
