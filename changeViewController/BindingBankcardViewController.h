//
//  BindingBankcardViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  选择开户银行控制器
 */
#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@class BankcardViewController;

@interface BindingBankcardViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *bankTableView;
@property (strong, nonatomic) NSArray *bankArray;

@property (weak, nonatomic) BankcardViewController *bankcardVC;

@end
