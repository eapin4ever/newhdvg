//
//  PMChooseAddressVC.h
//  changeViewController
//
//  Created by pmit on 14/12/18.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "AddressManageTableViewController.h"
@class YBSureOrderViewController;

@interface PMChooseAddressVC : AddressManageTableViewController

@property (strong,nonatomic) NSArray *productArr;

@property(nonatomic,weak)YBSureOrderViewController *sureOrderVC;

@end
