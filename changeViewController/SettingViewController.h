//
//  SettingViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/7.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "PMUserInfos.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface SettingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
{
    NSIndexPath *lastIndexPath;
}

@property (strong, nonatomic) UIImageView *logoImage;

@property (strong, nonatomic) NSMutableArray *sectionOneArray;
@property (strong, nonatomic) NSMutableArray *sectionTwoArray;
@property (strong, nonatomic) NSMutableArray *sectionThreeArray;
@property (strong, nonatomic) NSMutableArray *sectionFourArray;

@property (strong, nonatomic) NSMutableDictionary *versionDict;

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *lastIndexPath;

//@property (copy, nonatomic) NSString *token;  // 用于退出登录
@property (strong, nonatomic) PMNetworking *networking;

@end
