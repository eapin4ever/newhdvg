//
//  PMProductStoreViewController.h
//  changeViewController
//
//  Created by pmit on 15/1/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

// 商品库
#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMProductStoreCell.h"
#import "JHRefresh.h"//下拉刷新
#import <SDWebImage/UIImageView+WebCache.h>

@protocol PMStoreScreenDelegate <NSObject>

- (void)passIDString:(NSString *)string;

@end

@interface PMProductStoreViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *firstClassIdArray;
@property (strong, nonatomic) NSMutableArray *secondClassIdArray;
@property (strong, nonatomic) NSMutableArray *thirdClassIdArray;

@property (weak, nonatomic) id<PMStoreScreenDelegate> screenDelegate;

- (void)getModelsWithParam:(NSDictionary *)param finish:(myFinishBlock)finish error:(myFinishBlock)error;

@end
