//
//  ComplaintsViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/27.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 * 商品选择控制器
 */
 
#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMResizeImage.h"
#import "PMNetworking.h"
#import "MyTradeTableViewCell.h"
#import "LogisticsDetailViewController.h"
#import "RelevantViewController.h"

typedef NS_ENUM(NSInteger, orderState)
{
    PMOrderStateLogistics = 0,//查物流
    PMOrderStateReturnBack,//退换货
    PMOrderStateSureOrder//确认收货
};

@protocol ComplaintsViewControllerDelegate <NSObject>

- (void)finishSureOrder:(NSArray *)productArr;

@end

@interface ComplaintsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (strong, nonatomic) UITableView *returnGoodsTableView;

@property(nonatomic,strong) NSArray *productsArray;
@property(nonatomic,assign) orderState state;

@property (strong, nonatomic) PMNetworking *networking;
@property (weak,nonatomic) id<ComplaintsViewControllerDelegate> complainDelegate;
@property (assign,nonatomic) BOOL isFromDetail;

@end
