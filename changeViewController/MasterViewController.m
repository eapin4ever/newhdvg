//
//  MasterViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//
//
//
//

#define OLD_FRAME CGRectMake(-WidthRate(280), 0, WidthRate(280), HEIGHT)
#define LineSpace WidthRate(70);
#define InerSpace 0;
#define ItemSize CGSizeMake(WidthRate(260), WidthRate(70))

#import "MasterViewController.h"
#import "DistributionShopViewController.h"
#import "PMUserInfos.h"
#import "HZAreaPickerView.h"
#import "PMGetAreaID.h"
#import "PMNavigationController.h"
#import "AppDelegate.h"
#import "WebViewViewController.h"
#import "BPush.h"
#import "DetailTradeViewController.h"
#import "PMGetCashTableViewController.h"
#import <UIImageView-PlayGIF/UIImageView+PlayGIF.h>
#import "PMGoodListViewController.h"
#import "PMHomePageViewController.h"
#import "PMShoppingCarViewController.h"
#import "MyCenterViewController.h"
#import "NearSearchViewController.h"


@interface MasterViewController ()<UITabBarDelegate,UINavigationBarDelegate,UIAlertViewDelegate>
//当前视图控制器

//头像底视图
@property(nonatomic,strong)UIView *headerView;

@property(nonatomic,copy)NSString *locationName;

@property (strong,nonatomic) NSMutableArray *listArray;
@property (strong,nonatomic) UIView *numView;
@property (strong,nonatomic) UILabel *numLab;
@property (strong,nonatomic) UIView *locationView;

@property (assign,nonatomic) int errorCount;
@property (assign,nonatomic) int rErrorCount;
@property (copy,nonatomic) NSString *areaValue;
@property (copy,nonatomic) NSString *areaID;
@property (strong,nonatomic) HZAreaPickerView *locatePicker;
@property (strong,nonatomic) UIView *backView;
@property (strong,nonatomic) UIButton *numBtn;



@end

//单例，方便调用
static MasterViewController *_masterVC;
@implementation MasterViewController

+ (MasterViewController *)defaultMasterVC
{
    return _masterVC;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [((AppDelegate *)[UIApplication sharedApplication].delegate) checkClientVersionNotUpdate:nil];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    _masterVC = self;
    
    self.isSearching = NO;
    
    self.listArray = [NSMutableArray array];
    self.errorCount = 0;
    self.rErrorCount = 0;
    
    [self addChildViewControllers];
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if ([app isNetConnect])
    {
    }
    
    [self tabbarUI2];
    
    [self checkLogin];
}





#pragma mark - 添加子视图控制器
- (void)addChildViewControllers
{
    //tabBar上调出的Controller
    //移除在侧边栏的标题
    //以一个没用的vc作为第一个vc，用作推出首页
    UIViewController *vc = [[UIViewController alloc] init];
    vc.title = @"temp";
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
    self.currentViewController = vc;
    
    //首页
    PMHomePageViewController *homePageVC = [[PMHomePageViewController alloc] init];
    PMNavigationController *homePageNav = [[PMNavigationController alloc] initWithRootViewController:homePageVC];
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goToNextController:)];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    homePageVC.view.tag = 0;
    [homePageVC.view addGestureRecognizer:leftSwipe];
    [self addChildViewController:homePageNav];

    
    //商品列表 -- 主题卖场 -- 分类
    PMGoodListViewController *goodsListVC = [[PMGoodListViewController alloc] init];
    PMNavigationController *nav = [[PMNavigationController alloc] initWithRootViewController:goodsListVC];
    UISwipeGestureRecognizer *listSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(listGoTo:)];
    [listSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    [goodsListVC.view addGestureRecognizer:listSwipe];
    
    listSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(listGoTo:)];
    [listSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [goodsListVC.view addGestureRecognizer:listSwipe];
    
    [self addChildViewController:nav];
    
    
    //智能搜索
//    PMNearbySearchViewController *nearbySearchVC = [[PMNearbySearchViewController alloc] init];
    NearSearchViewController *nearbySearchVC = [[NearSearchViewController alloc] init];
    PMNavigationController *nearbySearchNav = [[PMNavigationController alloc] initWithRootViewController:nearbySearchVC];
    
    UISwipeGestureRecognizer *nearbySwiper = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nearGoTo:)];
    [nearbySwiper setDirection:UISwipeGestureRecognizerDirectionLeft];
    [nearbySearchVC.view addGestureRecognizer:nearbySwiper];
    
    nearbySwiper = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nearGoTo:)];
    [nearbySwiper setDirection:UISwipeGestureRecognizerDirectionRight];
    [nearbySearchVC.view addGestureRecognizer:nearbySwiper];
    
    [self addChildViewController:nearbySearchNav];
    
    
    //购物车
    PMShoppingCarViewController *shoppingCarVC = [[PMShoppingCarViewController alloc] init];
    shoppingCarVC.isFromDetai = NO;
    PMNavigationController *shoppingCarNav = [[PMNavigationController alloc] initWithRootViewController:shoppingCarVC];
    UISwipeGestureRecognizer *carSwiper = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(carGoTo:)];
    [carSwiper setDirection:UISwipeGestureRecognizerDirectionLeft];
    [shoppingCarVC.view addGestureRecognizer:carSwiper];
    
    carSwiper = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(carGoTo:)];
    [carSwiper setDirection:UISwipeGestureRecognizerDirectionRight];
    [shoppingCarVC.view addGestureRecognizer:carSwiper];
    
    [self addChildViewController:shoppingCarNav];
    
    
    //我的个人中心
    MyCenterViewController *myCenter = [[MyCenterViewController alloc] init];
    myCenter.isFromWeb = NO;
    PMNavigationController *myCenterNav = [[PMNavigationController alloc] initWithRootViewController:myCenter];
    UISwipeGestureRecognizer *centerSwiper = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(centerGoto:)];
    [centerSwiper setDirection:UISwipeGestureRecognizerDirectionRight];
    [myCenter.view addGestureRecognizer:centerSwiper];
    [self addChildViewController:myCenterNav];
    
    
    [self transitionFromViewController:self.currentViewController toViewController:homePageNav duration:0.5 options:UIViewAnimationOptionTransitionFlipFromTop animations:nil completion:^(BOOL finished) {
        self.currentViewController = homePageNav;
    }];
    
    [self bringSomeViewToFront];
}

- (void)tabbarUI2
{
    UIImage *image = [UIImage imageNamed:@"talk"];
    UIImageView *noNetIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(90), HEIGHT - 49 - 100, WIDTH - WidthRate(180), 100)];
    noNetIV.contentMode = UIViewContentModeScaleAspectFit;
    noNetIV.image = image;
    noNetIV.hidden = YES;
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    [window addSubview:noNetIV];
    self.noNetTipView = noNetIV;
    
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 13, noNetIV.bounds.size.width, 60)];
    tipLB.backgroundColor = [UIColor clearColor];
    tipLB.textColor = [UIColor whiteColor];
    tipLB.textAlignment = NSTextAlignmentCenter;
    tipLB.font = [UIFont systemFontOfSize:13.0f];
    tipLB.numberOfLines = 2;
    tipLB.text = @"世界上最遥远的距离就是没网,\n快去开启网络吧";
    [noNetIV addSubview:tipLB];
    
    UITabBar *tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, HEIGHT - 49, WIDTH, 49)];
    self.tabBar = tabBar;
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(231, 231, 231, 1).CGColor;
    line.frame = CGRectMake(0, -1, WIDTH, 1);
    [tabBar.layer addSublayer:line];
    
    tabBar.shadowImage = [[UIImage alloc] init];
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, RGBA(247, 247, 247, 1).CGColor);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    tabBar.backgroundImage = theImage;
    tabBar.delegate = self;
    [self.view addSubview:tabBar];
    
    
    UITabBarItem * tbi0 = [[UITabBarItem alloc] initWithTitle:@"首页" image:[[UIImage imageNamed:@"tabBar60_01"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:[self.childViewControllers count]-5];
    
    UITabBarItem * tbi1 = [[UITabBarItem alloc] initWithTitle:@"分类" image:[[UIImage imageNamed:@"tabBar60_02"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:[self.childViewControllers count]-4];
    
    UITabBarItem *nearbySearch = [[UITabBarItem alloc] initWithTitle:nil image:nil tag:[self.childViewControllers count]-3];
    [self initMidBtnImage];
    
    UITabBarItem * tbi2 = [[UITabBarItem alloc] initWithTitle:@"购物车" image:[[UIImage imageNamed:@"tabBar60_03"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:[self.childViewControllers count]-2];
    
    UITabBarItem * tbi3 = [[UITabBarItem alloc] initWithTitle:@"我的" image:[[UIImage imageNamed:@"tabBar60_04"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:[self.childViewControllers count]-1];
    
    
    [self setTabBarItemTitleColor:tbi0];
    [self setTabBarItemTitleColor:tbi1];
    [self setTabBarItemTitleColor:tbi2];
    [self setTabBarItemTitleColor:tbi3];
    
    if(SYSTEM_VERSION >= 7.0)
    {
        //禁止半透明
        tabBar.translucent = NO;
        //添加选中按钮的图片
        [tbi0 setSelectedImage:[[UIImage imageNamed:@"tabBar_01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tbi1 setSelectedImage:[[UIImage imageNamed:@"tabBar_02"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tbi2 setSelectedImage:[[UIImage imageNamed:@"tabBar_03"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tbi3 setSelectedImage:[[UIImage imageNamed:@"tabBar_04"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    
    tabBar.items = @[tbi0,tbi1,nearbySearch,tbi2,tbi3];
    
    //默认显示首页
    tabBar.selectedItem = tbi0;
    [self bringSomeViewToFront];
}


#pragma mark - 自定义tabBar
- (void)tabBarUI
{
    //tabBar系统默认高度49
    UITabBar *tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, HEIGHT - 49, WIDTH, 49)];
    self.tabBar = tabBar;
    
    //去掉tabBar边框黑线
    tabBar.shadowImage = [[UIImage alloc] init];
    tabBar.backgroundImage = [[UIImage alloc] init];
    tabBar.tintColor = RGBA(53, 54, 71, 1);
    tabBar.backgroundColor = NAVGray;
    
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, -12, WIDTH, 61)];
    self.tabBarIV = iv;
    iv.image = [UIImage imageNamed:@"bg__TAB_02"];
    iv.contentMode = UIViewContentModeScaleAspectFill;
    iv.backgroundColor = [UIColor clearColor];
    [tabBar addSubview:iv];
    


    //设置代理，当点击tabBarItem时会调用某协议方法
    tabBar.delegate = self;
    [self.view addSubview:tabBar];
    
    UITabBarItem * tbi0 = [[UITabBarItem alloc] initWithTitle:@"首页" image:[[UIImage imageNamed:@"homePage01"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:[self.childViewControllers count]-5];
    
    
    
    UITabBarItem * tbi1 = [[UITabBarItem alloc] initWithTitle:@"分类" image:[[UIImage imageNamed:@"classify01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:[self.childViewControllers count]-4];
    

    
    UITabBarItem *nearbySearch = [[UITabBarItem alloc] initWithTitle:nil image:nil tag:[self.childViewControllers count]-3];
    [self initMidBtnImage];
    
    
    UITabBarItem * tbi2 = [[UITabBarItem alloc] initWithTitle:@"购物车" image:[[UIImage imageNamed:@"shoppingCar01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:[self.childViewControllers count]-2];
    

    
    UITabBarItem * tbi3 = [[UITabBarItem alloc] initWithTitle:@"我的" image:[[UIImage imageNamed:@"my01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] tag:[self.childViewControllers count]-1];
    
    

    //修改字体颜色
    [self setTabBarItemTitleColor:tbi0];
    [self setTabBarItemTitleColor:tbi1];
    [self setTabBarItemTitleColor:tbi2];
    [self setTabBarItemTitleColor:tbi3];
    
    
    
    if(SYSTEM_VERSION >= 7.0)
    {
        //禁止半透明
        tabBar.translucent = NO;
        //添加选中按钮的图片
        [tbi0 setSelectedImage:[[UIImage imageNamed:@"homePage02"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tbi1 setSelectedImage:[[UIImage imageNamed:@"classify02"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tbi2 setSelectedImage:[[UIImage imageNamed:@"shoppingCar02"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tbi3 setSelectedImage:[[UIImage imageNamed:@"my02"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

        
    }
    
    tabBar.items = @[tbi0,tbi1,nearbySearch,tbi2,tbi3];
    
    //默认显示首页
    tabBar.selectedItem = tbi0;
    [self bringSomeViewToFront];
    
}

- (void)setTabBarItemTitleColor:(UITabBarItem *)tbi
{
    //修改tabBar上的字体颜色
    [tbi setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:RGBA(53, 54, 71, 1), NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:10], NSFontAttributeName, nil] forState:UIControlStateNormal];
    //选中时的颜色
    [tbi setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:RGBA(255, 61, 61, 1), NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:10], NSFontAttributeName, nil] forState:UIControlStateSelected];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    //检查是否已经登录
    if([PMUserInfos shareUserInfo].PM_SID == nil && (item.tag == [self.childViewControllers count]-2 ))
    {
        __weak MasterViewController *weakSelf = self;
        self.afterLoginAction = ^{
            [weakSelf tabBar:tabBar didSelectItem:item];
        };
        
        showAlertViewLogin;
        return;
    }
    
    
    //选中智能搜的效果
    if(item.tag == [self.childViewControllers count]-3)
    {
        self.midBtn.selected = YES;
    }
    else if (self.midBtn.selected == YES)
    {
        self.midBtn.selected = NO;
    }
    
    
    //如果不是当前页面
    if(self.childViewControllers[item.tag] != self.currentViewController)
    {
        
        [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[item.tag] duration:0 options:UIViewAnimationOptionTransitionNone animations:^
         {
             [self bringSomeViewToFront];
         }completion:^(BOOL finished)
         {
             self.currentViewController = self.childViewControllers[item.tag];
         }];
    }
    else if(item.tag == [self.childViewControllers count]-5)//如果是首页
    {
        
    }
    else if(item.tag == [self.childViewControllers count]-3)//智能搜-防止多次操作
    {
        if(self.nearbySearchAgain)
        {
            self.nearbySearchAgain();
        }
        

    }
}

- (void)goSearchBy
{
    
}


#pragma mark  中间按钮图片
- (void)initMidBtnImage
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 55, 55)];
    imageView.center = CGPointMake(self.tabBar.center.x + .2, 15);
    imageView.image = [UIImage imageNamed:@"tabbar_robot"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.tabBar addSubview:imageView];
    
//    UIImageView *gifIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 55, 55)];
//    gifIV.center = CGPointMake(self.tabBar.center.x + .2, 15);
//    gifIV.gifPath = [[NSBundle mainBundle] pathForResource:@"around" ofType:@"gif"];
//    gifIV.contentMode = UIViewContentModeScaleAspectFit;
//    [gifIV startGIF];
//    [self.tabBar addSubview:gifIV];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 55, 55)];
    self.midBtn = btn;
    btn.center = CGPointMake(self.tabBar.center.x + .2, 15);
//    [btn.layer setCornerRadius:21];
    btn.clipsToBounds = YES;
    [self.tabBar addSubview:btn];

    btn.imageView.contentMode = UIViewContentModeScaleAspectFit;

}


#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



#pragma mark - 某些视图需要一直前置
- (void)bringSomeViewToFront
{
    [self.view bringSubviewToFront:self.tabBar];
}

#pragma mark - 如果保存了账号密码，则自动登录
- (void)checkLogin
{
    //用NSUserDefaults中取值，如果有账号密码，则直接使用登录
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *params = [ud objectForKey:@"codeAndPass"];
    if(params != nil)
    {
//        [[PMNetworking defaultNetworking] request:PMRequestStateLogin WithParameters:params callBackBlock:^(NSDictionary *dic) {
//            
//            //如果返回的信息中，success的值是true则登陆成功
//            if([[dic objectForKey:@"success"] boolValue] == 1)
//            {
//                
//                NSDictionary *callBackDataDic = [dic objectForKey:@"data"];
//                
//                //将数据保存到 UserInfo单例中
//                [PMUserInfos shareUserInfo].userDataDic = [callBackDataDic mutableCopy];
//                //保存是否 代言商
//                [PMUserInfos shareUserInfo].isReseller = [[[callBackDataDic objectForKey:@"clientUser"] objectForKey:@"isReseller"] integerValue];
//                NSString *token = [callBackDataDic objectForKey:@"token"];
//                
//                //9.取子字符串，从第n个开始（参数），取到字符串结尾
//                [PMUserInfos shareUserInfo].PM_SID = [token substringFromIndex:7];
//                
////                PMMyCenterViewController *myCenter = [[[self.childViewControllers lastObject] childViewControllers] firstObject];
//                MyCenterViewController *myCenter = [[[self.childViewControllers lastObject] childViewControllers] firstObject];
//                
//                //传头像地址
//                myCenter.photoUrl = [[callBackDataDic objectForKey:@"clientUser"] objectForKey:@"photo"];
//                AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//                [app bindBaiduyun];
//                
//            }
//            else
//            {
//                // 用户没有登录成功的提示原因
////                showRequestFailAlertView;
//            }
//        }showIndicator:YES];
        [[PMNetworking defaultNetworking] requestWithPost:PMRequestStateLogin WithParameters:params callBackBlock:^(NSDictionary *dic) {
            
            if([[dic objectForKey:@"success"] boolValue] == 1)
            {
                
                NSDictionary *callBackDataDic = [dic objectForKey:@"data"];
                
                //将数据保存到 UserInfo单例中
                [PMUserInfos shareUserInfo].userDataDic = [callBackDataDic mutableCopy];
                //保存是否 代言商
                [PMUserInfos shareUserInfo].isReseller = [[[callBackDataDic objectForKey:@"clientUser"] objectForKey:@"isReseller"] integerValue];
                NSString *token = [callBackDataDic objectForKey:@"token"];
                
                //9.取子字符串，从第n个开始（参数），取到字符串结尾
                [PMUserInfos shareUserInfo].PM_SID = [token substringFromIndex:7];
                
                //                PMMyCenterViewController *myCenter = [[[self.childViewControllers lastObject] childViewControllers] firstObject];
                MyCenterViewController *myCenter = [[[self.childViewControllers lastObject] childViewControllers] firstObject];
                
                //传头像地址
                myCenter.photoUrl = [[callBackDataDic objectForKey:@"clientUser"] objectForKey:@"photo"];
                AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [app bindBaiduyun];
                
            }
            else
            {
                // 用户没有登录成功的提示原因
                //                showRequestFailAlertView;
            }
            
        } showIndicator:NO];
    }
}



#pragma mark - 跳转到登陆页面
- (void)transitionToLoginVC
{
    // 就当前页显示登录页面
    // LoginViewController *vc = [[LoginViewController alloc] init];
    LoginViewController *vc = [LoginViewController shareInstance];
    PMNavigationController *nav = [[PMNavigationController alloc] initWithRootViewController:vc];
    
    UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithTitle:@"   取消" style:UIBarButtonItemStyleDone target:vc action:@selector(cancelLogin:)];
    vc.navigationItem.leftBarButtonItem = bbi;
    
    [bbi setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR, NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:16], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    
    vc.isPresented = YES;
    
    [[self.currentViewController.childViewControllers lastObject] presentViewController:nav animated:YES completion:^{
        
    }];
}

#pragma mark - 跳转到绑定银行卡页面
- (void)transitionToGetCashVC
{
    // 就当前页显示绑定银行卡页面
    PMGetCashTableViewController *getCashVC = [PMGetCashTableViewController shareInstance];
    PMNavigationController *nav = [[PMNavigationController alloc] initWithRootViewController:getCashVC];
    
    UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithTitle:@"  返回" style:UIBarButtonItemStyleDone target:getCashVC action:@selector(backClick:)];
    getCashVC.navigationItem.leftBarButtonItem = bbi;
    
    [bbi setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR, NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:16], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    //getCashVC.isPresented = YES;
    
    [[self.currentViewController.childViewControllers lastObject] presentViewController:nav animated:YES completion:^{
        
    }];
}

#pragma mark - 跳转到我的交易页面
- (void)transitionToMyTrade
{
    [self transitionToHomePage];
    DetailTradeViewController *vc = [[DetailTradeViewController alloc] init];
    vc.tradeStatus = tradeStatusWaitPay;
    [(PMNavigationController *)self.currentViewController pushViewController:vc animated:NO];
    
}

#pragma mark - 跳转到我的交易页面
- (void)transitionToMyTrade2
{
    [self transitionToMyPage];
    DetailTradeViewController *vc = [[DetailTradeViewController alloc] init];
    vc.tradeStatus = tradeStatusWaitPay;
    [(PMNavigationController *)self.currentViewController pushViewController:vc animated:NO];
    
}

#pragma mark - 提示登录后，跳转到登录页面
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //如果是提示登录的，而且当前页不是登录页面的，则跳转到登录页面
    if([alertView.message isEqual:@"亲，请先登录哦"] && self.currentViewController != self.childViewControllers[0])
    {
        [self transitionToLoginVC];
    }
    else if([alertView.message isEqualToString:@"亲，网络似乎有问题,请重试"])
    {
        
    }
}

#pragma mark 跳转到首页
- (void)transitionToHomePage
{
    if(self.currentViewController != self.childViewControllers[1])
    {
        [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[1] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
         {
             [self bringSomeViewToFront];
         }completion:^(BOOL finished)
         {
             self.currentViewController = self.childViewControllers[1];
             self.tabBar.selectedItem = self.tabBar.items[0];
         }];
    }
}

#pragma mark 跳转到首页
- (void)transitionToMyClass
{
    if(self.currentViewController != self.childViewControllers[2])
    {
        [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[2] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
         {
             [self bringSomeViewToFront];
         }completion:^(BOOL finished)
         {
             self.currentViewController = self.childViewControllers[2];
             self.tabBar.selectedItem = self.tabBar.items[1];
         }];
    }
}

#pragma mark 跳转到我的微购
- (void)transitionToMyPage
{
    if(self.currentViewController != self.childViewControllers[5])
    {
        [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[5] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
         {
             [self bringSomeViewToFront];
         }completion:^(BOOL finished)
         {
             self.currentViewController = self.childViewControllers[5];
             self.tabBar.selectedItem = self.tabBar.items[4];
         }];
    }
}

#pragma mark - 用户退出登录，所有页面重新刷新
- (void)userLogout
{
    //我的账户页面返回登录页面
    
    
    for (id obj in self.childViewControllers)
    {
        NSInteger index = [self.childViewControllers indexOfObject:obj];
        if([obj isKindOfClass:[UINavigationController class]] && index != 1)//除了首页之外，都清除掉页面数据
        {
            [self.numView setHidden:YES];
            UINavigationController *nav = obj;
            UIViewController *vc = nav.viewControllers[0];
            vc.view = nil;
        }
    }
    
    self.numBtn.hidden = YES;
    [self.numBtn setTitle:@"0" forState:UIControlStateNormal];
    [self transitionToHomePage];
}


- (void)showNoNetView
{
    if (self.tabBar.alpha == 0)
    {
        
    }
    else
    {
        [UIView animateWithDuration:1.0f animations:^{
            self.noNetTipView.hidden = NO;
        }];
        
    }
}

- (void)hideNoNetView
{
    [UIView animateWithDuration:0.3f animations:^{
        self.noNetTipView.hidden = YES;
    }];
    
}

- (void)goToNextController:(UISwipeGestureRecognizer *)swipe
{
    if(self.currentViewController != self.childViewControllers[2])
    {
        [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[2] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
         {
             [self bringSomeViewToFront];
         }completion:^(BOOL finished)
         {
             self.currentViewController = self.childViewControllers[2];
             self.tabBar.selectedItem = self.tabBar.items[1];
         }];
    }
}

- (void)listGoTo:(UISwipeGestureRecognizer *)swipe
{
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if(self.currentViewController != self.childViewControllers[3])
        {
            [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[3] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
             {
                 [self bringSomeViewToFront];
             }completion:^(BOOL finished)
             {
                 self.currentViewController = self.childViewControllers[3];
                 self.tabBar.selectedItem = self.tabBar.items[2];
             }];
        }
    }
    else
    {
        [self transitionToHomePage];
    }
}

- (void)carGoTo:(UISwipeGestureRecognizer *)swipe
{
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        if(self.currentViewController != self.childViewControllers[5])
        {
            [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[5] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
             {
                 [self bringSomeViewToFront];
             }completion:^(BOOL finished)
             {
                 self.currentViewController = self.childViewControllers[5];
                 self.tabBar.selectedItem = self.tabBar.items[4];
             }];
        }
    }
    else
    {
        if(self.currentViewController != self.childViewControllers[3])
        {
            [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[3] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
             {
                 [self bringSomeViewToFront];
             }completion:^(BOOL finished)
             {
                 self.currentViewController = self.childViewControllers[3];
                 self.tabBar.selectedItem = self.tabBar.items[2];
             }];
        }
    }
}

- (void)centerGoto:(UISwipeGestureRecognizer *)swiper
{
    if(self.currentViewController != self.childViewControllers[4])
    {
        [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[4] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
         {
             [self bringSomeViewToFront];
         }completion:^(BOOL finished)
         {
             self.currentViewController = self.childViewControllers[4];
             self.tabBar.selectedItem = self.tabBar.items[3];
         }];
    }
}

- (void)nearGoTo:(UISwipeGestureRecognizer *)swipe
{
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight)
    {
        if(self.currentViewController != self.childViewControllers[2])
        {
            [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[2] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
             {
                 [self bringSomeViewToFront];
             }completion:^(BOOL finished)
             {
                 self.currentViewController = self.childViewControllers[2];
                 self.tabBar.selectedItem = self.tabBar.items[1];
             }];
        }
    }
    else
    {
        if(self.currentViewController != self.childViewControllers[4])
        {
            [self transitionFromViewController:self.currentViewController toViewController:self.childViewControllers[4] duration:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^
             {
                 [self bringSomeViewToFront];
             }completion:^(BOOL finished)
             {
                 self.currentViewController = self.childViewControllers[4];
                 self.tabBar.selectedItem = self.tabBar.items[3];
             }];
        }
    }
}

- (void) timeEnough
{
    UIButton *btn=(UIButton*)[self.tabBar viewWithTag:1000];
    btn.selected=NO;
//    [timer invalidate];
//    timer = nil;
}


@end
