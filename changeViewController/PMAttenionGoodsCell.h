//
//  PMAttenionGoodsCell.h
//  changeViewController
//
//  Created by pmit on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMAttenionGoodsCell : UITableViewCell
@property(nonatomic,strong)UIImageView *iv;

- (void)createUI;
- (void)setContentTitle:(NSString *)title classify:(NSString *)classify distance:(NSString *)distance price:(NSString *)price;
@end
