//
//  WebViewViewController.h
//  changeViewController
//
//  Created by EapinZhang on 15/3/27.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WebViewViewController;

typedef enum{
    
    webTypeHongBao,
    webTypeDiscount,
    webTypePreBuy,
    webTypeTheme,
    webTypeShop,
    webTypeJoin,
    webTypePay,
    webTypeHot,
    
}webType;

@protocol WebViewControllerDelegate <NSObject>

- (void)jumpToTrade:(WebViewViewController *)webView;

@end

@interface WebViewViewController : UIViewController

@property (copy,nonatomic) NSString *productId;
@property (copy,nonatomic) NSString *newsShopId;
@property (assign,nonatomic) BOOL isCare;
@property (assign,nonatomic) BOOL isPrepay;
@property (copy,nonatomic) NSString *urlString;
@property (assign,nonatomic) webType webViewType;
@property (assign,nonatomic) BOOL isFromCar;
@property (weak,nonatomic) id<WebViewControllerDelegate> webDelegate;
@property (copy,nonatomic) NSString *themeId;
@property (copy,nonatomic) NSString *hotUrlString;
@property (copy,nonatomic) NSString *imgUrl;

- (instancetype)initWithWebType:(webType)webType;
//+ (WebViewViewController *)defaultWebView;

@end
