//
//  PMShowAllGoodsWillBuyVC.m
//  changeViewController
//
//  Created by pmit on 14/12/18.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMShowAllGoodsWillBuyVC.h"

@interface PMShowAllGoodsWillBuyVC ()

@end

@implementation PMShowAllGoodsWillBuyVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"查看所有商品";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[PMSureOrderTVCell class] forCellReuseIdentifier:@"Cell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.listArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMSureOrderTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSDictionary *dic = self.listArray[indexPath.row];
    NSDictionary *productDic = [dic objectNullForKey:@"product"];
    
    
    [cell createUI];
    [cell.iv sd_setImageWithURL:[productDic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
//不显示距离
    //[cell setContentTitle:[productDic objectNullForKey:@"productName"] classify1:[dic objectNullForKey:@"aspecVal"] classify2:[dic objectNullForKey:@"bspecVal"] distanceOrLocation:nil number:[dic objectNullForKey:@"productCount"] price:[[productDic objectNullForKey:@"productPrice"] doubleValue]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightRate(240);

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2;
}


@end
