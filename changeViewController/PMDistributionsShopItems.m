//
//  PMDistributionsShopItems.m
//  changeViewController
//
//  Created by pmit on 14/11/27.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMDistributionsShopItems.h"

@implementation PMDistributionsShopItems

- (instancetype)initWithFrame:(CGRect)frame imageName:(NSString *)imageName title:(NSString *)myTitle;
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height - 20)];
        iv.image = [UIImage imageNamed:imageName];
        [self addSubview:iv];
        
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 20, self.bounds.size.width, 40)];
        lb.text = myTitle;
        lb.font = [UIFont systemFontOfSize:15];
        lb.numberOfLines = 2;
        lb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:lb];
        
        
    }
    return self;
}



@end
