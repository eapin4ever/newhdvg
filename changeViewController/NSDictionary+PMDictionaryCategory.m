//
//  NSDictionary+PMDictionaryCategory.m
//  changeViewController
//
//  Created by pmit on 14/12/31.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "NSDictionary+PMDictionaryCategory.h"

@implementation NSDictionary (PMDictionaryCategory)
- (id)objectNullForKey:(id)aKey
{
    if([self isKindOfClass:[NSNull class]])
    {
        return @"null";
    }
    else
    {
        id obj = [self objectForKey:aKey];
        
        if([obj isKindOfClass:[NSNull class]])
            obj = @"";
        
        return obj;
    }
}
@end
