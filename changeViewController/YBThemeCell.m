//
//  YBThemeCell.m
//  changeViewController
//
//  Created by ZhangEapin on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBThemeCell.h"
#import "PMMyPhoneInfo.h"
#import "PMMyPhoneInfo.h"

#define kCellWidth (WIDTH - 15)

@implementation YBThemeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    self.backgroundColor = HDVGPageBGGray;
    if (!self.upLeftIV)
    {
        self.upLeftIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, kCellWidth * 0.5, kCellWidth * 0.5 / 1.5)];
        self.upLeftIV.image = [UIImage imageNamed:@"banner_Moren2"];
        self.upLeftIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.upLeftIV];
        
        self.upLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.upLeftBtn.frame = self.upLeftIV.frame;
        self.upLeftBtn.tag = 1;
        [self.contentView addSubview:self.upLeftBtn];
        
        self.upRightIV = [[UIImageView alloc] initWithFrame:CGRectMake(kCellWidth * 0.5 + 10, 5, kCellWidth * 0.5, kCellWidth * 0.5 / 1.5)];
        self.upRightIV.image = [UIImage imageNamed:@"banner_Moren2"];
        self.upRightIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.upRightIV];
        
        self.upRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.upRightBtn.frame = self.upRightIV.frame;
        self.upRightBtn.tag = 2;
        [self.contentView addSubview:self.upRightBtn];
        
        self.downLeftIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, self.upLeftIV.bounds.size.height + 10, kCellWidth * 0.5, kCellWidth * 0.5 / 1.5)];
        self.downLeftIV.image = [UIImage imageNamed:@"banner_Moren2"];
        self.downLeftIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.downLeftIV];
        
        self.downLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.downLeftBtn.frame = self.downLeftIV.frame;
        self.downLeftBtn.tag = 3;
        [self.contentView addSubview:self.downLeftBtn];
        
        self.downRightIV = [[UIImageView alloc] initWithFrame:CGRectMake(kCellWidth * 0.5 + 10, self.upLeftIV.bounds.size.height + 10, kCellWidth * 0.5, kCellWidth * 0.5 / 1.5)];
        self.downRightIV.image = [UIImage imageNamed:@"banner_Moren2"];
        self.downRightIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.downRightIV];
        
        self.downRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.downRightBtn.frame = self.downRightIV.frame;
        self.downRightBtn.tag = 4;
        [self.contentView addSubview:self.downRightBtn];
        
    }
   
}

- (void)addTarget:(id)target selector:(SEL)sel
{
    [self.upLeftBtn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [self.upRightBtn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [self.downLeftBtn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [self.downRightBtn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    
    
}

@end
