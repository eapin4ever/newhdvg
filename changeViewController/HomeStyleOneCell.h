//
//  HomeStyleOneCell.h
//  changeViewController
//
//  Created by pmit on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeStyleOneCell : UITableViewCell

@property (strong,nonatomic) UIImageView *upBigIV;
@property (strong,nonatomic) UIImageView *upMiddleIV;
@property (strong,nonatomic) UIImageView *upSmallIV;
@property (strong,nonatomic) UIImageView *downSmallIV;
@property (strong,nonatomic) UIImageView *downLeftIV;
@property (strong,nonatomic) UIImageView *downRightIV;
@property (strong,nonatomic) UILabel *upBigTitleLB;
@property (strong,nonatomic) UILabel *upMiddleTitleLB;
@property (strong,nonatomic) UILabel *upSmallTitleLB;
@property (strong,nonatomic) UILabel *downSmallTitleLB;
@property (strong,nonatomic) UILabel *downLeftTitleLB;
@property (strong,nonatomic) UILabel *downRightTitleLB;
@property (strong,nonatomic) UILabel *upBigPriceLB;
@property (strong,nonatomic) UILabel *upMiddlePriceLB;
@property (strong,nonatomic) UILabel *upSmallPriceLB;
@property (strong,nonatomic) UILabel *downLeftPriceLB;
@property (strong,nonatomic) UILabel *downRightPriceLB;

@property (strong,nonatomic) UIView *upBigView;
@property (strong,nonatomic) UIView *upMiddleView;
@property (strong,nonatomic) UIView *upSmallView;
@property (strong,nonatomic) UIView *downSmallView;
@property (strong,nonatomic) UIView *downRightView;
@property (strong,nonatomic) UIView *downLeftView;

- (void)createUI;
- (void)setBigIV:(NSString *)bigUrlString AndMiddleIV:(NSString *)middleString AndSmallOneIV:(NSString *)smallOneString AndSmallTwo:(NSString *)SmallTowString AndDownLeft:(NSString *)downLeftString AndDownRight:(NSString *)downRightString AndbigTitle:(NSString *)bigTitle AndmiddleTitle:(NSString *)middleTitle AndSmallOneTitle:(NSString *)smallOneTitle AndSmallTwoTitle:(NSString *)smallTwoTitle AndDownLeftTitle:(NSString *)downLeftTitle AndDownRightTitle:(NSString *)downRightTitle AndBigPrice:(NSString *)bigPrice AndMiddlePrice:(NSString *)middlePrice AndDwonLeftPrice:(NSString *)downLeftPrice AndDownRightPrice:(NSString *)downRightPrice;

@end
