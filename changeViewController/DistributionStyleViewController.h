//
//  DistributionStyleViewController.h
//  changeViewController
//
//  Created by pmit on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DistributionStyleViewControllerDelegate <NSObject>

- (void)passStyleUrlString:(NSString *)styleUrlString;

@end

@interface DistributionStyleViewController : UIViewController

@property (strong,nonatomic) NSArray *styleArr;
@property (copy,nonatomic) NSString *shopId;
@property (copy,nonatomic) NSString *styleUrlString;
@property (strong,nonatomic) NSDictionary *shopDic;
@property (weak,nonatomic) id<DistributionStyleViewControllerDelegate> styleDelegate;

@end
