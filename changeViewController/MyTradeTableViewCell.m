//
//  MyTradeTableViewCell.m
//  changeViewController
//
//  Created by P&M on 14/12/2.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "MyTradeTableViewCell.h"

@implementation MyTradeTableViewCell
{
    UILabel *titleLabel;
    UILabel *classifyLabel;
    UILabel *specificationLab;
    UILabel *priceLabel;
    UILabel *timeLabel;
    
    UILabel *numberLabel;
}

#pragma mark - 我的交易订单商品数据
// 设置我的交易订单商品数据 UI
- (void)setMyTradeOrderDataUI
{
    if (!_iv) {
        
        // 商品图片
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(30), WidthRate(150), WidthRate(150))];
        _iv.layer.cornerRadius = 6.0f;
        _iv.layer.masksToBounds = YES;
        [self.contentView addSubview:_iv];
        
        // 商品标题
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(200), HeightRate(30), WidthRate(350), iPhone6_Plus || iPhone6 ? HeightRate(90) : HeightRate(110))];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont systemFontOfSize:14.0f];
        titleLabel.numberOfLines = 0;
        [self.contentView addSubview:titleLabel];
        
        // 商品数量
        classifyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(200), titleLabel.frame.origin.y + HeightRate(130), WidthRate(140), HeightRate(40))];
        classifyLabel.backgroundColor = [UIColor clearColor];
        classifyLabel.textColor = [UIColor grayColor];
        classifyLabel.textAlignment = NSTextAlignmentLeft;
        classifyLabel.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:classifyLabel];
        
        // 商品规格
        specificationLab = [[UILabel alloc] initWithFrame:CGRectMake(classifyLabel.frame.origin.x + WidthRate(160), titleLabel.frame.origin.y + HeightRate(130), WidthRate(180), HeightRate(40))];
        specificationLab.backgroundColor = [UIColor clearColor];
        specificationLab.textColor = [UIColor grayColor];
        specificationLab.textAlignment = NSTextAlignmentLeft;
        specificationLab.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:specificationLab];
        
        // 商品价格
        priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(190), (HeightRate(230) - HeightRate(40)) / 2, WidthRate(150), HeightRate(40))];
        priceLabel.backgroundColor = [UIColor clearColor];
        priceLabel.textAlignment = NSTextAlignmentRight;
        priceLabel.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:priceLabel];
        
        // 如果是客户服务的退换货，就是商品数量
        numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(180), (HeightRate(230) - HeightRate(40)) / 2, WidthRate(130), HeightRate(40))];
        numberLabel.backgroundColor = [UIColor clearColor];
        numberLabel.textAlignment = NSTextAlignmentCenter;
        numberLabel.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:numberLabel];
        
        self.turnBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.turnBackBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.turnBackBtn.layer.borderWidth = 1;
        self.turnBackBtn.frame = CGRectMake(WIDTH - WidthRate(150), titleLabel.frame.origin.y + HeightRate(130), WidthRate(130), WidthRate(50));
        [self.turnBackBtn setTitle:@"退货" forState:UIControlStateNormal];
        [self.turnBackBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.turnBackBtn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
        [self.turnBackBtn.layer setCornerRadius:3.0f];
        [self.contentView addSubview:self.turnBackBtn];
        
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        line.frame = CGRectMake(0, (iPhone6_Plus || iPhone6 ? 110 : 90) - 0.5, WIDTH, 0.5);
        [self.contentView.layer addSublayer:line];
    }
}

// 设置我的交易详情的商品图片、商品标题、商品数量、商品价格数据
- (void)setTitle:(NSString *)goodsTitle setSpecification:(NSString *)specification setNumber:(NSInteger)goodsNumber setPrice:(double)goodsPrice
{
    titleLabel.text = goodsTitle;
    specificationLab.text = specification;
    classifyLabel.text = [NSString stringWithFormat:@"数量: %ld", (long)goodsNumber];
    priceLabel.text = [NSString stringWithFormat:@"¥%.2lf", goodsPrice];
}

// 设置交易详情的商品图片、商品标题、商品分类、商品数量数据
- (void)setTitle:(NSString *)goodsTitle setClssify:(NSString *)goodsClassify setNumber:(NSInteger)goodsNumber
{
    titleLabel.text = goodsTitle;
    classifyLabel.text = goodsClassify;
    numberLabel.text = [NSString stringWithFormat:@"数量:%ld", (long)goodsNumber];
}

- (void)setImage:(NSString *)urlString setTitle:(NSString *)goodsTitle setClssify:(NSString *)goodClassify setNumber:(NSInteger)goodsNumber
{
    [_iv sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"loading_image"]] ;
    titleLabel.text = goodsTitle;
    classifyLabel.text = goodClassify;
    numberLabel.text = [NSString stringWithFormat:@"数量:%ld", (long)goodsNumber];
}

#pragma mark - 未读消息
// 设置未读消息时间 UI
- (void)setUnreadMessagesTimeUI
{
    if (!timeLabel) {
        // 未读消息时间
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(200), 10, WidthRate(140), 20)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor grayColor];
        timeLabel.textAlignment = NSTextAlignmentRight;
        timeLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:timeLabel];
    }
}

// 设置未读消息时间数据
- (void)setUnreadMessagesTime:(NSString *)time
{
    timeLabel.text = time;
}


@end
