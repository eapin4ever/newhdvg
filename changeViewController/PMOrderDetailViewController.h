//
//  PMOrderDetailViewController.h
//  changeViewController
//
//  Created by pmit on 15/10/27.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PMOrderDetailViewControllerDelegate <NSObject>

- (void)detailFinishSureOrder:(NSArray *)orderArr;
- (void)detailFinishComments:(NSArray *)orderArr;
- (void)detailFinishCancelOrder:(NSArray *)orderArr;

@end

@interface PMOrderDetailViewController : UIViewController

@property (strong,nonatomic) NSString *orderCode;
@property (copy,nonatomic) NSString *orderStatus;
@property (assign,nonatomic) BOOL isHasDiscuss;
@property (strong,nonatomic) NSArray *productArr;
@property (weak,nonatomic) id<PMOrderDetailViewControllerDelegate> detailViewDelegate;

@end
