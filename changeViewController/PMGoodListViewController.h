//
//  PMGoodListViewController.h
//  changeViewController
//
//  Created by pmit on 15/6/25.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMGoodListViewController : UIViewController

@property (strong,nonatomic) UITableView *detailTableView;

@end
