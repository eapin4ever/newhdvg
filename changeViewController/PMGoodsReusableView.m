//
//  Goods.m
//  changeViewController
//
//  Created by pmit on 14/11/20.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMGoodsReusableView.h"

@interface PMGoodsReusableView()

@end

@implementation PMGoodsReusableView
{
    UILabel *discountLB;
    UILabel *discriptionLB;
    UILabel *distanceLB;
    UILabel *priceLB;
    UIButton *distanceBtn;
}

- (void)createUI
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    if(!_imageView)
    {
        //商品图片
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.width)];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_imageView];
        
            //折扣
        discountLB = [[UILabel alloc] initWithFrame:CGRectMake(-WidthRate(60), HeightRate(10), WidthRate(200), HeightRate(70))];
        discountLB.backgroundColor = HDVGRed;
        discountLB.transform = CGAffineTransformMakeRotation(-M_PI_4);//逆时针旋转45度
        discountLB.font = [UIFont systemFontOfSize:14.0f];
        discountLB.textColor = [UIColor whiteColor];
        discountLB.textAlignment = NSTextAlignmentCenter;
//        discountLB.hidden = YES;
        [self.contentView insertSubview:discountLB atIndex:10000];
        self.contentView.clipsToBounds = YES;//去掉超出部分
        
        //商品描述
        discriptionLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), self.contentView.bounds.size.width + 5, WidthRate(325), HeightRate(80))];
        discriptionLB.backgroundColor = [UIColor clearColor];
        discriptionLB.numberOfLines = 2;
        discriptionLB.font = [UIFont systemFontOfSize:WidthRate(28)];
        discriptionLB.textColor = [UIColor darkGrayColor];
        discriptionLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:discriptionLB];
        
        
        //价格
        priceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(5), discriptionLB.frame.origin.y + discriptionLB.frame.size.height, WidthRate(305), HeightRate(46))];
        priceLB.backgroundColor = [UIColor clearColor];
        priceLB.font = [UIFont boldSystemFontOfSize:16];
        priceLB.textColor = HDVGRed;
        priceLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:priceLB];
        
    }
    
    if (self.showDiscount)
    {
        discountLB.hidden = NO;
    }
    else
    {
        discountLB.hidden = YES;
    }
    
    
}

- (void)setContentTitle:(NSString *)title price:(double)price discount:(NSString *)discount
{
    discriptionLB.text = title;
    priceLB.text = [NSString stringWithFormat:@"￥%.2lf", price];
    discountLB.text = [NSString stringWithFormat:@"%@折",discount];
}



@end
