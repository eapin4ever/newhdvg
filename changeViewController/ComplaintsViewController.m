//
//  ComplaintsViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/27.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "ComplaintsViewController.h"
#import "PMMyAllOrderViewController.h"

@interface ComplaintsViewController ()

@end

@implementation ComplaintsViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"商品选择";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = HDVGPageBGGray;
    self.networking = [PMNetworking defaultNetworking];
    self.returnGoodsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.returnGoodsTableView setShowsVerticalScrollIndicator:NO];
    self.returnGoodsTableView.delegate = self;
    self.returnGoodsTableView.dataSource = self;
    [self.returnGoodsTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:@"returnGoodsCell"];
    [self.view addSubview:self.returnGoodsTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 初始化我的交易tableView
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.productsArray.count;
}

// 设置 footer 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.state == PMOrderStateSureOrder)
        return 35;
    else
        return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] init];
    
    if (self.state == PMOrderStateSureOrder) {
        
        footerView.frame = CGRectMake(0, 0, WIDTH, 35);
        footerView.backgroundColor = [UIColor clearColor];
        
//        NSString *orderId = [self.productsArray[section] objectNullForKey:@"orderId"];
//        NSDictionary *param = @{@"orderId":orderId};
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(WIDTH - 100, 1, 100, 30);
        [button setTitle:@"确认收货" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [button.layer setCornerRadius:4.0f];
        [button.layer setBorderWidth:0.5f];
        [button.layer setBorderColor:[UIColor grayColor].CGColor];
        [footerView addSubview:button];
        
        
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(0, 35 - 0.5f, WIDTH, HeightRate(1));
        layer.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
        [footerView.layer addSublayer:layer];
        
        return footerView;
    }
    else {
        footerView.frame = CGRectMake(0, 0, WIDTH, 1);
        footerView.backgroundColor = RGBA(245, 245, 245, 1);
        
        return footerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightRate(220);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"returnGoodsCell"];
    
    [cell setMyTradeOrderDataUI];
    
    NSDictionary *dic = self.productsArray[indexPath.row];

    [cell setImage:[dic objectNullForKey:@"productLogo"] setTitle:[dic objectNullForKey:@"productName"] setClssify:[dic objectNullForKey:@"aSpecVal"] setNumber:[[dic objectNullForKey:@"productNum"] integerValue]];
    
    if (self.state != PMOrderStateSureOrder) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.turnBackBtn.hidden = YES;
    
    return cell;
}

#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *orderId = [self.productsArray[indexPath.row] objectNullForKey:@"orderId"];
    
    NSDictionary *param = @{@"orderId":orderId};
    
    // 查看物流
    if (self.state == PMOrderStateLogistics)
    {
        
        [self.networking request:PMRequestStateShopOrderLogistics WithParameters:param callBackBlock:^(NSDictionary *dict) {
            
            if([[dict objectForKey:@"success"] integerValue] == 1)
            {
                LogisticsDetailViewController *logisticsVC = [[LogisticsDetailViewController alloc] init];
                logisticsVC.logisticsDict = dict;
                [self.navigationController pushViewController:logisticsVC animated:YES];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alertView.tag = 2;
                [alertView show];
            }
        }showIndicator:YES];
    }
    // 检查确认收货 （提示用户是否确认收货）
    else if (self.state == PMOrderStateSureOrder)
    {
        // 检查确认收货
    }
    // 退换货
    else if (self.state == PMOrderStateReturnBack)
    {
        
        [self.networking request:PMRequestStateCheckReturnBack WithParameters:param callBackBlock:^(NSDictionary *dict) {
            
            if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
            
                RelevantViewController *relevantVC = [[RelevantViewController alloc] init];
                relevantVC.relevantDict = self.productsArray[indexPath.row];
                [self.navigationController pushViewController:relevantVC animated:YES];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"你已经申请过退换货了！" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 2;
                [alertView show];
            }
        }showIndicator:YES];
    }
}

#pragma mark - 确认收货
- (void)buttonAction:(UIButton *)sender
{
    NSInteger section = sender.tag;
    
    NSString *orderId = [self.productsArray[section] objectNullForKey:@"orderId"];
    NSDictionary *param = @{@"orderId":orderId};
    
    [self.networking request:PMRequestStateCheckIsGetProduct WithParameters:param callBackBlock:^(NSDictionary *dict) {
        
        if([[dict objectNullForKey:@"success"] integerValue] == 1)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"你确定要收货吗？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alertView.tag = 1;
            [alertView show];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            alertView.tag = 2;
            [alertView show];
        }
    }showIndicator:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1 && alertView.tag == 1) {
        
        NSString *orderId = [self.productsArray[0] objectNullForKey:@"orderId"];
        
        NSDictionary *goodsDict = @{@"orderId":orderId};
        
        self.networking = [PMNetworking defaultNetworking];
        [self.networking request:PMRequestStateSureGetProduct WithParameters:goodsDict callBackBlock:^(NSDictionary *dict) {
            
            if([[dict objectNullForKey:@"success"] integerValue] == 1)
            {
                if ([self.complainDelegate respondsToSelector:@selector(finishSureOrder:)])
                {
                    [self.complainDelegate finishSureOrder:self.productsArray];
                }
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dict objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alertView.tag = 2;
                [alertView show];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dict objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }showIndicator:YES];
    }
    if (alertView.tag == 2) {
        if (self.isFromDetail)
        {
            for (UIViewController *subVC in self.navigationController.viewControllers)
            {
                if ([subVC isKindOfClass:[PMMyAllOrderViewController class]])
                {
                    [self.navigationController popToViewController:subVC animated:YES];
                }
                else
                {
                    continue;
                }
            }
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
}


@end
