//
//  PMMyProfitViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/5.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  代言获利控制器
 */

#import <UIKit/UIKit.h>
#import "PMNetworking.h"

@interface PMMyProfitViewController : UIViewController

@property (strong, nonatomic) UIView *todayView;
@property (strong, nonatomic) UIView *moneyView;
@property (strong, nonatomic) UIView *bottomView;

@property (strong, nonatomic) PMNetworking *networking;

@end
