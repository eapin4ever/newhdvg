//
//  PMOrderBtnCell.h
//  changeViewController
//
//  Created by pmit on 15/10/30.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PMOrderBtnType)
{
    PMOrderBtnTypeWaitPay,
    PMOrderBtnTypeWaitGet,
    PMOrderBtnTypeWaitSend,
    PMOrderBtnTypeWaitDiscuss,
    PMOrderBtnTypeHasCancel,
    PMOrderBtnTypeTurnBack
};

@protocol PMOrderBtnCellDelegate <NSObject>

- (void)cellBtnClickAction:(UIButton *)sender;

@end

@interface PMOrderBtnCell : UITableViewCell

@property (strong,nonatomic) UIButton *goToPayBtn;
@property (strong,nonatomic) UIButton *cancelPayBtn;
@property (strong,nonatomic) UIButton *waitSendBtn;
@property (strong,nonatomic) UIButton *discussBtn;
@property (strong,nonatomic) UIButton *checkLogBtn;
@property (strong,nonatomic) UIButton *sureGetBtn;
@property (strong,nonatomic) UIButton *deleteBtn;
@property (strong,nonatomic) UIButton *buyAgainBtn;
@property (strong,nonatomic) UILabel *truePayLB;
@property (strong,nonatomic) UIButton *hasDiscussBtn;
@property (assign,nonatomic) PMOrderBtnType orderBtnType;
@property (weak,nonatomic) id<PMOrderBtnCellDelegate> orderBtnDelegate;

- (void)createUI;
- (void)setCellData:(NSString *)truePayString OrderBtnType:(PMOrderBtnType)orderBtnType DiscussStatus:(BOOL)hasDiscuss;


@end
