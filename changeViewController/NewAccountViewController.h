//
//  NewAccountViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"

@interface NewAccountViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) UIView *accountAndPassView;
@property (strong, nonatomic) UITextField *aNewNameTextField;
@property (strong, nonatomic) UITextField *aNewAccountPassTF;

@property (strong, nonatomic) UILabel *tipLabel;
@property (strong, nonatomic) UIButton *finishButton;

@property (strong, nonatomic) PMNetworking *networking;

@end
