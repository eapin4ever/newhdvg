//
//  WithdrawalDetailViewController.m
//  changeViewController
//
//  Created by P&M on 14/12/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "WithdrawalDetailViewController.h"

@interface WithdrawalDetailViewController ()

@end

@implementation WithdrawalDetailViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"提现详情";
        
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self createWithdrawalDetailFirstUI];
    [self createWithdrawalDetailSecondUI];
}

- (void)createWithdrawalDetailFirstUI
{
    self.firstView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(476))];
    self.firstView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.firstView];
    
    CALayer *line = [[CALayer alloc] init];
    line.frame = CGRectMake(0, HeightRate(300), WIDTH, HeightRate(1));
    line.backgroundColor = [RGBA(225, 225, 225, 1) CGColor];
    [self.firstView.layer addSublayer:line];
    
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(0, HeightRate(388), WIDTH, HeightRate(1));
    line1.backgroundColor = [RGBA(225, 225, 225, 1) CGColor];
    [self.firstView.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(0, HeightRate(475), WIDTH, HeightRate(1));
    line2.backgroundColor = [RGBA(225, 225, 225, 1) CGColor];
    [self.firstView.layer addSublayer:line2];
    
    // 提款金额
    UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(90), HeightRate(60), WidthRate(200), HeightRate(60))];
    moneyLabel.backgroundColor = [UIColor clearColor];
    moneyLabel.text = @"提款金额：";
    moneyLabel.textColor = RGBA(98, 98, 98, 1);
    moneyLabel.font = [UIFont systemFontOfSize:16.0f];
    moneyLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:moneyLabel];
    
    // 金额数额
    UILabel *moneyNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(90), HeightRate(150), WIDTH - WidthRate(180), HeightRate(100))];
    moneyNumLabel.backgroundColor = [UIColor clearColor];
    moneyNumLabel.text = [NSString stringWithFormat:@"%.2lf",[[self.detailDict objectNullForKey:@"money"] doubleValue]];
    moneyNumLabel.textColor = RGBA(98, 98, 98, 1);
    moneyNumLabel.font = [UIFont systemFontOfSize:48.0f];
    moneyNumLabel.textAlignment = NSTextAlignmentCenter;
    [self.firstView addSubview:moneyNumLabel];
    
    // 状态
    UILabel *stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(90), HeightRate(314), WIDTH - WidthRate(180), HeightRate(60))];
    stateLabel.backgroundColor = [UIColor clearColor];
    stateLabel.text = [NSString stringWithFormat:@"状       态：%@", [self.detailDict objectNullForKey:@"statusStr"]];
    stateLabel.textColor = RGBA(98, 98, 98, 1);
    stateLabel.font = [UIFont systemFontOfSize:16.0f];
    stateLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:stateLabel];
    
    // 创建时间
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(90), HeightRate(402), WIDTH - WidthRate(180), HeightRate(60))];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.text = [NSString stringWithFormat:@"创建时间：%@",[self.detailDict objectNullForKey:@"createDt"]];
    timeLabel.textColor = RGBA(98, 98, 98, 1);
    timeLabel.font = [UIFont systemFontOfSize:16.0f];
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:timeLabel];
}

- (void)createWithdrawalDetailSecondUI
{
    self.secondView = [[UIView alloc] initWithFrame:CGRectMake(0, self.firstView.frame.origin.y + HeightRate(476), WIDTH, HeightRate(355))];
    self.secondView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.secondView];
    
    CALayer *line = [[CALayer alloc] init];
    line.frame = CGRectMake(0, HeightRate(175), WIDTH, HeightRate(1));
    line.backgroundColor = [RGBA(225, 225, 225, 1) CGColor];
    [self.secondView.layer addSublayer:line];
    
//    CALayer *line1 = [[CALayer alloc] init];
//    line1.frame = CGRectMake(0, HeightRate(350), WIDTH, HeightRate(1));
//    line1.backgroundColor = [RGBA(225, 225, 225, 1) CGColor];
//    [self.secondView.layer addSublayer:line1];
    
    // 提现银行
    UILabel *bankLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(90), HeightRate(40), WIDTH - WidthRate(180), HeightRate(60))];
    bankLabel.backgroundColor = [UIColor clearColor];
    bankLabel.text = [NSString stringWithFormat:@"提现银行：%@",[self.detailDict objectNullForKey:@"bank"]];
    bankLabel.textColor = RGBA(98, 98, 98, 1);
    bankLabel.font = [UIFont systemFontOfSize:16.0f];
    bankLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:bankLabel];
    
    // 银行卡号
    UILabel *bankCardLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(90), HeightRate(100), WIDTH - WidthRate(150), HeightRate(60))];
    bankCardLab.backgroundColor = [UIColor clearColor];
    bankCardLab.text = [NSString stringWithFormat:@"                   %@",[self.detailDict objectNullForKey:@"bankCard"]];
    bankCardLab.textColor = RGBA(98, 98, 98, 1);
    bankCardLab.font = [UIFont systemFontOfSize:15.0f];
    bankCardLab.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:bankCardLab];
    
//    // 流水号
//    UILabel *serialLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(90), HeightRate(215), WidthRate(200), HeightRate(60))];
//    serialLabel.backgroundColor = [UIColor clearColor];
//    serialLabel.text = @"流  水  号：";
//    serialLabel.textColor = RGBA(98, 98, 98, 1);
//    serialLabel.font = [UIFont systemFontOfSize:16.0f];
//    serialLabel.textAlignment = NSTextAlignmentLeft;
//    [self.secondView addSubview:serialLabel];
//    
//    UILabel *serialNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(280), HeightRate(215), WIDTH - WidthRate(300), HeightRate(60))];
//    serialNumLabel.backgroundColor = [UIColor clearColor];
//    serialNumLabel.text = [self.detailDict objectNullForKey:@"serialNumber"];
//    serialNumLabel.textColor = RGBA(98, 98, 98, 1);
//    serialNumLabel.font = [UIFont systemFontOfSize:15.0f];
//    serialNumLabel.textAlignment = NSTextAlignmentLeft;
//    [self.secondView addSubview:serialNumLabel];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
