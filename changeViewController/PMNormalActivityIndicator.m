//
//  PMNormalActivityIndicator.m
//  changeViewController
//
//  Created by pmit on 15/1/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMNormalActivityIndicator.h"

@implementation PMNormalActivityIndicator

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        
        UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(WIDTH/2 - 50, HEIGHT/2 - 25 - 64, 100, 100)];
        bg.backgroundColor = [UIColor blackColor];
        bg.alpha = 0.6;
        bg.layer.cornerRadius = 8.0;
        [self addSubview:bg];
        
        UIActivityIndicatorView *ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.activityIndicatorView = ai;
        ai.frame = CGRectMake(30, 30, 40, 40);
        [ai startAnimating];
        [bg addSubview:ai];
        
    }
    return self;
}

- (void)stop
{
    [self.activityIndicatorView stopAnimating];
    [self removeFromSuperview];
}

@end
