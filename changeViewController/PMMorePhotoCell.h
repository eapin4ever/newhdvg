//
//  PMMorePhotoCell.h
//  changeViewController
//
//  Created by pmit on 15/1/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMMorePhotoCell : UITableViewCell
- (void)createUI;
- (void)setContentImage:(UIImage *)image height:(CGFloat)height;
@end
