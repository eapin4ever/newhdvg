//
//  PMHomePageViewController.m
//  changeViewController
//
//  Created by pmit on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMHomePageViewController.h"
#import "PMMyPhoneInfo.h"
#import "AppDelegate.h"
#import "MasterViewController.h"
#import "HomeData.h"
#import "YBSearchViewController.h"
#import "QRCodeScannerViewController.h"
#import "YBThemeBtn.h"
#import "DetailTradeViewController.h"
#import "WebViewViewController.h"
#import "YBThemeCell.h"
#import "YBProductCellOne.h"
#import "YBProductCellTwo.h"
#import "HomeStyleOneCell.h"
#import "HomeStyleTwoCell.h"
#import "HomeStyleThreeCell.h"
#import "HomeTitleCell.h"
#import "HomeHeaderImageCell.h"
#import "HomeData.h"
#import "HomeStyleFourCell.h"
#import "PMSearchResultViewController.h"
#import "PMMyGoodsViewController.h"
#import "ProductScrollCell.h"
#import "JHRefresh.h"
#import "PMToastHint.h"
#import "SearchResultViewController.h"
#import "SecondViewController.h"

@interface PMHomePageViewController () <UITableViewDataSource,UITableViewDelegate,ProductScrollCellDelegate>

@property (strong,nonatomic) UITableView *homeTableView;
@property (strong,nonatomic) UIScrollView *advertisementSV;
@property (strong,nonatomic) AppDelegate *app;
@property (strong,nonatomic) NSMutableArray *scrollProductArr;
@property (strong,nonatomic) NSMutableArray *productDicArr;
@property (strong,nonatomic) NSMutableArray *floorAdverArr;
@property (strong,nonatomic) NSMutableDictionary *gridDataDic;
@property (strong,nonatomic) NSMutableArray *productList;
@property (strong,nonatomic) NSString *urlHeaderString;
@property (strong,nonatomic) NSMutableArray *themeArr;
@property (strong,nonatomic) NSArray *colorArr;
@property (strong,nonatomic) NSArray *bannerArr;
@property (strong,nonatomic) UIPageControl *pageControl;
@property (strong,nonatomic) NSArray *titleArr;

@property (strong,nonatomic) UIView *buttonView;
@property (strong,nonatomic) YBThemeBtn *themeBtn;
@property (strong,nonatomic) NSMutableArray *bgArray;

@property (strong,nonatomic) NSArray *titleCellArray;//临时使用
@property (assign,nonatomic) NSInteger productSectionNum;
@property (strong,nonatomic) NSArray *floorArr;

@end

@implementation PMHomePageViewController

static NSString *const themeCell = @"themeCell";
static NSString *const productOneCell = @"oneCell";
static NSString *const productTwoCell = @"twoCell";
static NSString *const productStyleOneCell = @"styleOne";
static NSString *const productStyleTwoCell = @"styleTwo";
static NSString *const productStyleThreeCell = @"styleThree";
static NSString *const titleCell = @"sectionTitleCell";
static NSString *const homeHeaderImageCell = @"homeHeaderCell";
static NSString *const productStyleFourCell = @"styleFour";
static NSString *const scrollCell = @"scrollCell";
static NSString *const scrollCellTwo = @"scrollCellTwo";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = HDVGPageBGGray;
    self.productSectionNum = 0;
    [self setTitles];
    
    self.bgArray = [NSMutableArray array];
    
    self.productDicArr = [NSMutableArray array];
    self.scrollProductArr = [NSMutableArray array];
    self.scrollProductArr = [NSMutableArray array];
    self.colorArr = @[RGBA(120, 152, 192, 1),RGBA(208, 50, 82, 1),RGBA(95, 176, 251, 1),RGBA(231, 96, 178, 1),RGBA(237, 131, 92, 1),RGBA(167, 218, 84, 1),RGBA(233, 100, 114, 1),RGBA(79, 206, 209, 1)];
    [self buildNavigationBar];
    [self buildTableView];
    [self buildScrollViewHeader];
    [self getThemeByNet];
    [self getBannerByNet];
    [self getFloorByNet];

    [self checkUpdateClientByNet];
    

    
    
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app checkClientVersionNotUpdate:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIView animateWithDuration:0.3 animations:^{
        [[MasterViewController defaultMasterVC].tabBar setAlpha:1];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildNavigationBar
{
    UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 44)];
    self.navigationItem.titleView = topBarView;
    
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 54, 7, 30, 30)];
    [rightButton setImage:[UIImage imageNamed:@"QRCode"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(scanQRCode) forControlEvents:UIControlEventTouchUpInside];
    [topBarView addSubview:rightButton];
    
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(8, 7, 30, 30)];
    [leftButton setImage:[UIImage imageNamed:@"Logo_icon"] forState:UIControlStateNormal];
    leftButton.userInteractionEnabled = NO;
    [topBarView addSubview:leftButton];
    
    
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(105), 7, WIDTH - WidthRate(250), 30)];
    tf.enabled = NO;
    tf.userInteractionEnabled = YES;
    [tf setBorderStyle:UITextBorderStyleRoundedRect];
    //搜索框背景色
    [tf setBackgroundColor:[UIColor whiteColor]];
    [tf.layer setCornerRadius:5.0];
    [tf.layer setBorderWidth:0];
    
    //预留字
    tf.placeholder = @"输入关键字搜索商品";
    [tf setValue:[UIFont boldSystemFontOfSize:11] forKeyPath:@"_placeholderLabel.font"];
    [tf setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, tf.bounds.size.width, tf.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [tf setValue:NAVTEXTCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    
    tf.returnKeyType = UIReturnKeySearch;
    tf.textColor = [UIColor darkGrayColor];
    tf.font = [UIFont systemFontOfSize:12];
    
    [topBarView addSubview:tf];
    
    UIButton *jumpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    jumpBtn.frame = tf.frame;
    [jumpBtn addTarget:self action:@selector(searchViewShow:) forControlEvents:UIControlEventTouchUpInside];
    [topBarView addSubview:jumpBtn];
    
    //搜索栏中的放大镜
    UIImage * image = [UIImage imageNamed:@"search"];
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    searchIcon.image = image;
    searchIcon.contentMode = UIViewContentModeScaleAspectFit;
    
    tf.leftView = searchIcon;
    tf.leftViewMode = UITextFieldViewModeAlways;
}

- (void)buildTableView
{
    self.homeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT- 64 - 50)];
//    self.homeTableView.contentInset = UIEdgeInsetsMake(0, 0, 49, 0);
    self.homeTableView.backgroundColor = HDVGPageBGGray;
    self.homeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.homeTableView.delegate = self;
    self.homeTableView.dataSource = self;
    [self.homeTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.homeTableView registerClass:[YBThemeCell class] forCellReuseIdentifier:themeCell];
    [self.homeTableView registerClass:[YBProductCellOne class] forCellReuseIdentifier:productOneCell];
    [self.homeTableView registerClass:[YBProductCellTwo class] forCellReuseIdentifier:productTwoCell];
    [self.homeTableView registerClass:[HomeStyleOneCell class] forCellReuseIdentifier:productStyleOneCell];
    [self.homeTableView registerClass:[HomeStyleTwoCell class] forCellReuseIdentifier:productStyleTwoCell];
    [self.homeTableView registerClass:[HomeStyleThreeCell class] forCellReuseIdentifier:productStyleThreeCell];
    [self.homeTableView registerClass:[HomeTitleCell class] forCellReuseIdentifier:titleCell];
    [self.homeTableView registerClass:[HomeHeaderImageCell class] forCellReuseIdentifier:homeHeaderImageCell];
    [self.homeTableView registerClass:[HomeStyleFourCell class] forCellReuseIdentifier:productStyleFourCell];
    [self.homeTableView registerClass:[ProductScrollCell class] forCellReuseIdentifier:scrollCell];
    [self.homeTableView registerClass:[ProductScrollCell class] forCellReuseIdentifier:scrollCellTwo];
    [self.view addSubview:self.homeTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.productDicArr.count == 0)
    {
        return 2;
    }
    else
    {
        return self.productSectionNum + 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    else if (section == 1)
    {
        if (self.themeArr.count == 0)
        {
            return 0;
        }
        return 2;
    }
    else if (section == 2 || section == 6)
    {
        return 3;
    }
    else
    {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"quick"];
        cell.backgroundColor = HDVGPageBGGray;
        
        NSArray *classArr = @[@"品牌折扣",@"领取红包",@"我的关注",@"物流查询"];
        NSArray *classImageArr = @[@"icon60_01",@"icon60_02",@"icon60_03",@"icon60_04"];
        NSArray *classSelectedImageArr = @[@"icon_01",@"icon_02",@"icon_03",@"icon_04"];
        NSArray *englishArr = @[@"DISCOUNT",@"ENVELOPE",@"FOCUS",@"LOGISTICS"];
        
        UIView *superBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 90)];
        superBtnView.backgroundColor = [UIColor whiteColor];
        
        for (NSInteger i = 0; i < classArr.count; i++)
        {
            UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH / 4 * i, 5, WIDTH / 4, 85)];
            buttonView.backgroundColor = [UIColor whiteColor];
            self.buttonView = buttonView;
            
            CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
            UIGraphicsBeginImageContext(rect.size);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetFillColorWithColor(context, RGBA(71, 65, 66, 1).CGColor);
            CGContextFillRect(context, rect);
            UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.themeBtn = [[YBThemeBtn alloc] initWithFrame:CGRectMake(WidthRate(20), 0, WIDTH / 4 - WidthRate(50), 90)];
            [self.themeBtn setImage:[UIImage imageNamed:classImageArr[i]] forState:UIControlStateNormal];
            [self.themeBtn setTitle:classArr[i] forState:UIControlStateNormal];
            [self.themeBtn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
            [self.themeBtn setBackgroundImage:theImage forState:UIControlStateHighlighted];
            [self.themeBtn setImage:[UIImage imageNamed:classSelectedImageArr[i]] forState:UIControlStateHighlighted];
            [self.themeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
            self.themeBtn.tag = i;
            self.themeBtn.englishLB.text = englishArr[i];
            [self.themeBtn addTarget:self action:@selector(classSelected:) forControlEvents:UIControlEventTouchUpInside];
            [buttonView addSubview:self.themeBtn];
            [self.bgArray addObject:self.themeBtn];
            [superBtnView addSubview:buttonView];
            
        }
        [cell.contentView addSubview:superBtnView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"titleCell"];
            cell.backgroundColor = HDVGPageBGGray;
            
            CALayer *line1 = [CALayer layer];
            line1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            line1.frame = CGRectMake(0, cell.bounds.size.height / 2 - 13, WIDTH / 2 - WidthRate(125), 0.5);
            [cell.layer addSublayer:line1];
            
            UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 2 - WidthRate(125), 0, WidthRate(230), 20)];
            titleLB.textColor = HDVGFontColor;
            titleLB.textAlignment = NSTextAlignmentCenter;
            titleLB.font = [UIFont systemFontOfSize:11.0f];
            NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:@"THEME·主题卖场"];
            NSRange litteRange = NSMakeRange(0, [[noteStr string] rangeOfString:@"·"].location);
            [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:litteRange];
            titleLB.attributedText = noteStr;
            
            [cell.contentView addSubview:titleLB];
            
            
            CALayer *line2 = [CALayer layer];
            line2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            line2.frame = CGRectMake(WIDTH / 2 + WidthRate(105), cell.bounds.size.height / 2 - 13, WIDTH / 2 - WidthRate(100), 0.5);
            [cell.layer addSublayer:line2];
            
            return cell;
        }
        else
        {
            YBThemeCell *cell = [tableView dequeueReusableCellWithIdentifier:themeCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell createUI];
            NSDictionary *dic1;
            NSDictionary *dic2;
            NSDictionary *dic3;
            NSDictionary *dic4;
            
            if (self.themeArr.count == 1)
            {
                dic1 = self.themeArr[0];
                NSString *oneUrlString = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic1 objectNullForKey:@"themePhoto"]];
                [cell.upLeftIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                cell.upRightIV.image = [UIImage imageNamed:@"theme_default_null"];
                cell.downLeftIV.hidden = YES;
                cell.downLeftIV.hidden = YES;
                
                cell.upLeftBtn.enabled = YES;
                cell.upRightBtn.enabled = NO;
                cell.downLeftBtn.enabled = NO;
                cell.downRightBtn.enabled = NO;
                
                cell.downLeftIV.hidden = YES;
                cell.downRightIV.hidden = YES;
                cell.downLeftBtn.hidden = YES;
                cell.downRightBtn.hidden = YES;
                
                
            }
            else if (self.themeArr.count == 2)
            {
                dic1 = self.themeArr[0];
                dic2 = self.themeArr[1];
                NSString *oneUrlString1 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic1 objectNullForKey:@"themePhoto"]];
                NSString *oneUrlString2 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic2 objectNullForKey:@"themePhoto"]];
                [cell.upLeftIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString1] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                [cell.upRightIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString2] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                cell.downLeftIV.hidden = YES;
                cell.downLeftIV.hidden = YES;
                
                cell.upLeftBtn.enabled = YES;
                cell.upRightBtn.enabled = YES;
                cell.downLeftBtn.enabled = NO;
                cell.downRightBtn.enabled = NO;
                
                cell.downLeftIV.hidden = YES;
                cell.downRightIV.hidden = YES;
                cell.downLeftBtn.hidden = YES;
                cell.downRightBtn.hidden = YES;
            }
            else if (self.themeArr.count == 3)
            {
                dic1 = self.themeArr[0];
                dic2 = self.themeArr[1];
                dic3 = self.themeArr[2];
                NSString *oneUrlString1 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic1 objectNullForKey:@"themePhoto"]];
                NSString *oneUrlString2 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic2 objectNullForKey:@"themePhoto"]];
                 NSString *oneUrlString3 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic3 objectNullForKey:@"themePhoto"]];
                [cell.upLeftIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString1] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                [cell.upRightIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString2] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                [cell.downLeftIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString3] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                cell.downRightIV.image = [UIImage imageNamed:@"theme_default_null"];
                
                cell.upLeftBtn.enabled = YES;
                cell.upRightBtn.enabled = YES;
                cell.downLeftBtn.enabled = YES;
                cell.downRightBtn.enabled = NO;
    
            }
            else if (self.themeArr.count == 4)
            {
                dic1 = self.themeArr[0];
                dic2 = self.themeArr[1];
                dic3 = self.themeArr[2];
                dic4 = self.themeArr[3];
                NSString *oneUrlString1 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic1 objectNullForKey:@"themePhoto"]];
                NSString *oneUrlString2 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic2 objectNullForKey:@"themePhoto"]];
                NSString *oneUrlString3 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic3 objectNullForKey:@"themePhoto"]];
                NSString *oneUrlString4 = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[dic4 objectNullForKey:@"themePhoto"]];
                [cell.upLeftIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString1] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                [cell.upRightIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString2] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                [cell.downLeftIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString3] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                [cell.downRightIV sd_setImageWithURL:[NSURL URLWithString:oneUrlString4] placeholderImage:[UIImage imageNamed:@"theme_default_null"]];
                
                cell.upLeftBtn.enabled = YES;
                cell.upRightBtn.enabled = YES;
                cell.downLeftBtn.enabled = YES;
                cell.downRightBtn.enabled = YES;
            }
            [cell addTarget:self selector:@selector(goToThemeDetail:)];
            
            return cell;
        }
    }
    else if ((indexPath.section == 2 && indexPath.row == 1) || (indexPath.section == 5 && indexPath.row == 1))
    {
        HomeStyleOneCell *cell = [tableView dequeueReusableCellWithIdentifier:productStyleOneCell];
        NSArray *thisSectionArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeProduct];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = HDVGPageBGGray;
        HomeData *homeData0;
        HomeData *homeData1;
        HomeData *homeData2;
        HomeData *homeData3;
        HomeData *homeData4;
        HomeData *homeData5;
        @try {
            homeData0 = thisSectionArr[0];
            homeData1 = thisSectionArr[1];
            homeData2 = thisSectionArr[2];
            homeData3 = thisSectionArr[3];
            homeData4 = thisSectionArr[4];
            homeData5 = thisSectionArr[5];
        }
        @catch (NSException *exception) {
            
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            app.crashView.hidden = NO;
            [app.window bringSubviewToFront:app.crashView];
            
        }
        @finally
        {
            
        }
        
        
        [cell createUI];
        [cell setBigIV:homeData0.homeImage AndMiddleIV:homeData1.homeImage AndSmallOneIV:homeData2.homeImage AndSmallTwo:homeData3.homeImage AndDownLeft:homeData4.homeImage AndDownRight:homeData5.homeImage AndbigTitle:homeData0.productName AndmiddleTitle:homeData1.productName AndSmallOneTitle:homeData2.productName AndSmallTwoTitle:homeData3.productName AndDownLeftTitle:homeData4.productName AndDownRightTitle:homeData5.productName AndBigPrice:[NSString stringWithFormat:@"￥%@",homeData0.productPrice] AndMiddlePrice:[NSString stringWithFormat:@"￥%@",homeData1.productPrice] AndDwonLeftPrice:[NSString stringWithFormat:@"￥%@",homeData4.productPrice] AndDownRightPrice:[NSString stringWithFormat:@"￥%@",homeData5.productPrice]];
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        cell.upBigIV.tag = indexPath.section * 100 + 1;
        cell.upMiddleView.tag = indexPath.section * 100 + 2;
        cell.upSmallView.tag = indexPath.section * 100 + 3;
        cell.downSmallView.tag = indexPath.section * 100 + 4;
        cell.downLeftView.tag = indexPath.section * 100 + 5;
        cell.downRightView.tag = indexPath.section * 100 + 6;
        
        cell.upBigIV.userInteractionEnabled = YES;
        cell.upMiddleView.userInteractionEnabled = YES;
        cell.upSmallView.userInteractionEnabled = YES;
        cell.downSmallView.userInteractionEnabled = YES;
        cell.downLeftView.userInteractionEnabled = YES;
        cell.downRightView.userInteractionEnabled = YES;
        
        [cell.upBigIV addGestureRecognizer:tap1];
        [cell.upMiddleView addGestureRecognizer:tap2];
        [cell.upSmallView addGestureRecognizer:tap3];
        [cell.downSmallView addGestureRecognizer:tap4];
        [cell.downLeftView addGestureRecognizer:tap5];
        [cell.downRightView addGestureRecognizer:tap6];
        
        return cell;
    }
    else if ((indexPath.section == 3 && indexPath.row == 1 ) || (indexPath.section == 6 && indexPath.row == 1))
    {
        HomeStyleTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:productStyleTwoCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = HDVGPageBGGray;
        NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeProduct];
        HomeData *homeData0;
        HomeData *homeData1;
        HomeData *homeData2;
        HomeData *homeData3;
        HomeData *homeData4;
        HomeData *homeData5;
        HomeData *homeData6;
        HomeData *homeData7;
        HomeData *homeData8;
        @try {
            homeData0 = homeDataArr[0];
            homeData1 = homeDataArr[1];
            homeData2 = homeDataArr[2];
            homeData3 = homeDataArr[3];
            homeData4 = homeDataArr[4];
            homeData5 = homeDataArr[5];
            homeData6 = homeDataArr[6];
            homeData7 = homeDataArr[7];
            homeData8 = homeDataArr[8];
        }
        @catch (NSException *exception) {
            
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            app.crashView.hidden = NO;
            [app.window bringSubviewToFront:app.crashView];
            
        }
        @finally
        {
            
        }
        
        [cell createUI];
        [cell setCellWithUpBigIV:homeData0.homeImage AndOneIV:homeData1.homeImage AndTwoIV:homeData2.homeImage AndThreeIV:homeData3.homeImage AndFourIV:homeData4.homeImage AndFiveIV:homeData5.homeImage AndSixIV:homeData6.homeImage AndDownLeftIV:homeData7.homeImage AndDownRightIV:homeData8.homeImage AndBigTitle:homeData0.productName AndOneTitle:homeData1.productName AndTwoTitle:homeData2.productName AndThreeTitle:homeData3.productName AndFoutTitle:homeData4.productName AndFiveTitle:homeData5.productName AndSixTitle:homeData6.productName AndLeftTitle:homeData7.productName AndRightTitle:homeData8.productName AndBigPrice:[NSString stringWithFormat:@"￥%@",homeData0.productPrice] AndDownLeftPrice:[NSString stringWithFormat:@"￥%@",homeData7.productPrice] AndDownRightPrice:[NSString stringWithFormat:@"￥%@",homeData8.productPrice]];
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap9 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        
        cell.upBigView.tag = indexPath.section * 100 + 1;
        cell.upOneView.tag = indexPath.section * 100 + 2;
        cell.upTwoView.tag = indexPath.section * 100 + 3;
        cell.upThreeView.tag = indexPath.section * 100 + 4;
        cell.upFourView.tag = indexPath.section * 100 + 5;
        cell.upFiveView.tag = indexPath.section * 100 + 6;
        cell.upSixView.tag = indexPath.section * 100 + 7;
        cell.downLeftView.tag = indexPath.section * 100 + 8;
        cell.downRightView.tag = indexPath.section * 100 + 9;
        
        cell.upBigView.userInteractionEnabled = YES;
        cell.upOneView.userInteractionEnabled = YES;
        cell.upTwoView.userInteractionEnabled = YES;
        cell.upThreeView.userInteractionEnabled = YES;
        cell.upFourView.userInteractionEnabled = YES;
        cell.upFiveView.userInteractionEnabled = YES;
        cell.upSixView.userInteractionEnabled = YES;
        cell.downLeftView.userInteractionEnabled = YES;
        cell.downRightView.userInteractionEnabled = YES;
        
        [cell.upBigView addGestureRecognizer:tap1];
        [cell.upOneView addGestureRecognizer:tap2];
        [cell.upTwoView addGestureRecognizer:tap3];
        [cell.upThreeView addGestureRecognizer:tap4];
        [cell.upFourView addGestureRecognizer:tap5];
        [cell.upFiveView addGestureRecognizer:tap6];
        [cell.upSixView addGestureRecognizer:tap7];
        [cell.downLeftView addGestureRecognizer:tap8];
        [cell.downRightView addGestureRecognizer:tap9];

        
        return cell;
    }
    else if ((indexPath.section == 4 && indexPath.row == 1) || (indexPath.section == 8 && indexPath.row == 1))
    {
        HomeStyleThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:productStyleThreeCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = HDVGPageBGGray;
        NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeProduct];
        HomeData *homeData0;
        HomeData *homeData1;
        HomeData *homeData2;
        HomeData *homeData3;
        HomeData *homeData4;
        
        @try {
            homeData0 = homeDataArr[0];
            homeData1 = homeDataArr[1];
            homeData2 = homeDataArr[2];
            homeData3 = homeDataArr[3];
            homeData4 = homeDataArr[4];
        }
        @catch (NSException *exception) {
            
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            app.crashView.hidden = NO;
            [app.window bringSubviewToFront:app.crashView];
            
        }
        @finally
        {
            
        }
        
        [cell createStyleThreeUI];
        [cell setCellWithUpOneIV:homeData0.homeImage AndUpTwoIV:homeData1.homeImage AndUpThreeIV:homeData2.homeImage AndDwonLeftIV:homeData3.homeImage AndDwonRightIV:homeData4.homeImage AndUpOneTitle:homeData0.productName AndUpTwoTitle:homeData1.productName AndUpThreeTitle:homeData2.productName AndDownLeftTitle:homeData3.productName AndDownRightTitle:homeData4.productName AndOnePrice:[NSString stringWithFormat:@"￥%@",homeData0.productPrice] AndUpTwoPrice:[NSString stringWithFormat:@"￥%@",homeData1.productPrice] AndUpThreePrice:[NSString stringWithFormat:@"￥%@",homeData2.productPrice] AndDwonLeftPrice:[NSString stringWithFormat:@"￥%@",homeData3.productPrice] AndDownRightPrice:[NSString stringWithFormat:@"￥%@",homeData4.productPrice]];
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        cell.upOneView.tag = indexPath.section * 100 + 1;
        cell.upTwoView.tag = indexPath.section * 100 + 2;
        cell.upThreeView.tag = indexPath.section * 100 + 3;
        cell.downLeftView.tag = indexPath.section * 100 + 4;
        cell.downRightView.tag = indexPath.section * 100 + 5;
        
        [cell.upOneView addGestureRecognizer:tap1];
        [cell.upTwoView addGestureRecognizer:tap2];
        [cell.upThreeView addGestureRecognizer:tap3];
        [cell.downLeftView addGestureRecognizer:tap4];
        [cell.downRightView addGestureRecognizer:tap5];
        
        cell.upOneView.userInteractionEnabled = YES;
        cell.upTwoView.userInteractionEnabled = YES;
        cell.upThreeView.userInteractionEnabled = YES;
        cell.downLeftView.userInteractionEnabled = YES;
        cell.downRightView.userInteractionEnabled = YES;
        
        return cell;
    }
    else if ((indexPath.section == 7 && indexPath.row == 1) || (indexPath.section == 9 && indexPath.row == 1))
    {
        HomeStyleFourCell *cell = [tableView dequeueReusableCellWithIdentifier:productStyleFourCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        [cell createUI];
        NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeProduct];
        HomeData *homeData0 = homeDataArr[0];
        HomeData *homeData1 = homeDataArr[1];
        HomeData *homeData2 = homeDataArr[2];
        HomeData *homeData3 = homeDataArr[3];
        HomeData *homeData4 = homeDataArr[4];
        HomeData *homeData5 = homeDataArr[5];
        [cell setCellWithOneIV:homeData0.homeImage AndTwoIV:homeData1.homeImage AndThreeIV:homeData2.homeImage AndFourIV:homeData3.homeImage AndFiveIV:homeData4.homeImage AndSixIV:homeData5.homeImage AndOneTitle:homeData0.productName AndTwoTitle:homeData1.productName AndThreeTitle:homeData2.productName AndFourTitle:homeData3.productName AndFiveTitle:homeData4.productName AndSixTitle:homeData5.productName AndOnePrice:[NSString stringWithFormat:@"￥%@",homeData0.productPrice] AndTwoPrice:[NSString stringWithFormat:@"￥%@",homeData1.productPrice] AndThreePrice:[NSString stringWithFormat:@"￥%@",homeData2.productPrice] AndFourPrice:[NSString stringWithFormat:@"￥%@",homeData3.productPrice] AndFivePrice:[NSString stringWithFormat:@"￥%@",homeData4.productPrice] AndSixPrice:[NSString stringWithFormat:@"￥%@",homeData5.productPrice]];
        
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        cell.upOneView.tag = indexPath.section * 100 + 1;
        cell.upTwoView.tag = indexPath.section * 100 + 2;
        cell.upThreeView.tag = indexPath.section * 100 + 3;
        cell.upFourView.tag = indexPath.section * 100 + 4;
        cell.upFiveView.tag = indexPath.section * 100 + 5;
        cell.upSixView.tag = indexPath.section * 100 + 6;
        
        cell.upOneView.userInteractionEnabled = YES;
        cell.upTwoView.userInteractionEnabled = YES;
        cell.upThreeView.userInteractionEnabled = YES;
        cell.upFourView.userInteractionEnabled = YES;
        cell.upFiveView.userInteractionEnabled = YES;
        cell.upSixView.userInteractionEnabled = YES;
        
        [cell.upOneView addGestureRecognizer:tap1];
        [cell.upTwoView addGestureRecognizer:tap2];
        [cell.upThreeView addGestureRecognizer:tap3];
        [cell.upFourView addGestureRecognizer:tap4];
        [cell.upFiveView addGestureRecognizer:tap5];
        [cell.upSixView addGestureRecognizer:tap6];
        
        return cell;
        
    }
    else if (indexPath.section == 2 && indexPath.row == 2)
    {
        NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeScroll];
        ProductScrollCell *cell = [tableView dequeueReusableCellWithIdentifier:scrollCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.cellDelegate = self;
        cell.contentArr = homeDataArr;
        [cell createUI];
        [cell setScrollViewContent:homeDataArr];
        return cell;
        
    }
    else if (indexPath.section == 6 && indexPath.row == 2)
    {
        NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeScroll];
        ProductScrollCell *cell = [tableView dequeueReusableCellWithIdentifier:scrollCellTwo];
        cell.cellDelegate = self;
        cell.contentArr = homeDataArr;
        [cell createUI];
        cell.productScroll.contentSize = CGSizeMake(homeDataArr.count * WIDTH / 3, 0);
        [cell setScrollViewContent:homeDataArr];
        return cell;
    }
    else if (indexPath.row == 0)
    {
        HomeHeaderImageCell *cell = [tableView dequeueReusableCellWithIdentifier:homeHeaderImageCell];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        HomeData *homeData;
        if ([[self getSectionArr:indexPath.section AndKey:PredicateTypeHeader] count] > 0) {
            homeData = [self getSectionArr:indexPath.section AndKey:PredicateTypeHeader][0];
        }
        [cell createUI];
        cell.headerImageView.userInteractionEnabled = YES;
        cell.headerImageView.tag = indexPath.section;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToMore:)];
        [cell.headerImageView addGestureRecognizer:tap];
        [cell setCellHeaderImageView:homeData.homeImage];
        return cell;
    }
    else
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cells"];
        cell.textLabel.text = @"123";
        return cell;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2)
    {
        return 44;
    }
    else if (section == 0 || section == 1)
    {
        return 0;
    }
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 10, 44)];
    view.backgroundColor = [UIColor whiteColor];
    UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 15, 44)];
    colorView.backgroundColor = RGBA(51, 51, 51, 1);
    [view addSubview:colorView];
    
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 80 , 44)];
    titleLB.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];
    titleLB.textColor = RGBA(51, 51, 51, 1);
    [view addSubview:titleLB];
    
//    NSString * titleString = [self.titleCellArray[section - 2] objectNullForKey:@"title"];
    NSArray *homeDataArr = [self getSectionArr:section AndKey:PredicateTypeProduct];
    HomeData *homeData = [homeDataArr firstObject];
    NSString *titleString = homeData.productTitle;
    titleString = [NSString stringWithFormat:@"%@·%@",[titleString substringWithRange:NSMakeRange(0, 2)],[titleString substringWithRange:NSMakeRange(2, 2)]];
    titleLB.text = titleString;
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(51, 51, 51, 1).CGColor;
    line.frame = CGRectMake(120, 21.5, WIDTH -120, 1);
    [view.layer addSublayer:line];
    
    CALayer *shadowLine = [CALayer layer];
    shadowLine.backgroundColor = RGBA(231, 231, 231, 1).CGColor;
    shadowLine.frame = CGRectMake(5, 43.5, WIDTH - 10, 0.5);
    [view.layer addSublayer:shadowLine];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 90;
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            return 20;
        }
        else
        {
            if (self.themeArr.count == 1 || self.themeArr.count == 2)
            {
                return WIDTH / 3 + 10;
            }
            return WIDTH / 1.5 + 10;
        }
    }
    else if (indexPath.section == 3)
    {
        if (indexPath.row == 1)
        {
            return WIDTH * 0.75 + 10;
        }
        else if (indexPath.row == 0)
        {
            NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeHeader];
            if (homeDataArr.count == 0)
            {
                return 0;
            }
            return (WIDTH - 10) / 3;
        }
    }
    else if (indexPath.section == 4)
    {
        if (indexPath.row == 1)
        {
            return WIDTH * 0.75;
        }
        else if (indexPath.row == 0)
        {
            NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeHeader];
            if (homeDataArr.count == 0)
            {
                return 0;
            }
            return (WIDTH - 10) / 3;
        }
    }
    else if (indexPath.section == 5)
    {
        if (indexPath.row == 1)
        {
            return WIDTH * 0.75;
        }
        else if (indexPath.row == 0)
        {
            NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeHeader];
            if (homeDataArr.count == 0)
            {
                return 0;
            }
            return (WIDTH - 10) / 3;
        }
    }
    else if (indexPath.section == 7 || indexPath.section == 9)
    {
        if (indexPath.row == 1)
        {
            return WIDTH * 0.93 + 15;
        }
        else if (indexPath.row == 0)
        {
            NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeHeader];
            if (homeDataArr.count == 0)
            {
                return 0;
            }
            return (WIDTH - 10) / 3;
        }
    }
    else if (indexPath.section == 2)
    {
        if (indexPath.row == 1)
        {
            return WIDTH * 0.72;
        }
        else if (indexPath.row == 0)
        {
            NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeHeader];
            if (homeDataArr.count == 0)
            {
                return 0;
            }
            return (WIDTH - 10) / 3;
        }
        else if (indexPath.row == 2)
        {
            return 110;
        }
    }
    else if (indexPath.section == 6)
    {
        if (indexPath.row == 1)
        {
            return (WIDTH - 10) * 0.75 + 10;
        }
        else if (indexPath.row == 0)
        {
            NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeHeader];
            if (homeDataArr.count == 0)
            {
                return 0;
            }
            return (WIDTH - 10) / 3;
        }
        else if (indexPath.row == 2)
        {
            return 110;
        }
    }
    else if (indexPath.section == 8)
    {
        if (indexPath.row == 1)
        {
            return WIDTH * 0.75;
        }
        else if (indexPath.row == 0)
        {
            NSArray *homeDataArr = [self getSectionArr:indexPath.section AndKey:PredicateTypeHeader];
            if (homeDataArr.count == 0)
            {
                return 0;
            }
            return (WIDTH - 10) / 3;
        }
    }
    else if (indexPath.row == 1)
    {
        return 125;
    }
    else if (indexPath.row % 2 != 0)
    {
        return 228;
    }
    
    return 44;
}



- (void)buildScrollViewHeader
{
    self.advertisementSV = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH / 2 - 5)];
    UIImageView *defaultAd = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH / 2 - 5)];
    defaultAd.image = [UIImage imageNamed:@"banner_Moren"];
    [self.advertisementSV addSubview:defaultAd];
    self.advertisementSV.showsHorizontalScrollIndicator = NO;
    self.advertisementSV.showsVerticalScrollIndicator = NO;
    self.advertisementSV.pagingEnabled = YES;
    self.homeTableView.tableHeaderView = self.advertisementSV;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 44)];
    footerView.backgroundColor = HDVGPageBGGray;
    self.homeTableView.tableFooterView = footerView;
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(self.advertisementSV.bounds.size.width/4 *3 + self.advertisementSV.frame.origin.x - 5, self.advertisementSV.bounds.size.height/5 *4 + self.advertisementSV.frame.origin.y, self.advertisementSV.bounds.size.width/4, self.advertisementSV.bounds.size.height/4)];
    [self.pageControl setPageIndicatorTintColor:[UIColor lightGrayColor]];
    [self.pageControl setCurrentPageIndicatorTintColor:[UIColor whiteColor]];
    [self.homeTableView.tableHeaderView addSubview:self.pageControl];
    
    __weak PMHomePageViewController *weakSelf = self;
    [self.homeTableView addRefreshHeaderViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        
        [weakSelf getProductByNetWithAni:NO];
        [weakSelf.homeTableView headerEndRefreshingWithResult:JHRefreshResultSuccess];
    }];
    
}

- (NSArray *)getDataArr:(PredicateType)predicateType
{
    NSString *predicateString = @"";
    switch (predicateType) {
        case PredicateTypeProduct:
            predicateString = @"isProduct";
            break;
        case PredicateTypeHeader:
            predicateString = @"isHeadeAd";
            break;
        case PredicateTypeScroll:
            predicateString = @"isScroll";
            break;
        default:
            break;
    }
    
    self.app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"HomeData" inManagedObjectContext:self.app.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K=1",predicateString,[NSNumber numberWithBool:YES]];
    [request setEntity:entity];
    request.predicate = predicate;
    NSArray *array = [self.app.managedObjectContext executeFetchRequest:request error:nil];
    return array;
}

- (void)removeAllData
{
    self.app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"HomeData" inManagedObjectContext:self.app.managedObjectContext];
    [request setEntity:entity];
    NSArray *array = [self.app.managedObjectContext executeFetchRequest:request error:nil];
    for (NSManagedObject *obj in array)
    {
        [self.app.managedObjectContext deleteObject:obj];
    }
    
    [self.app.managedObjectContext save:nil];
}

- (void)makeDataRefresh
{
    NSTimeInterval nowTime = [[NSDate new] timeIntervalSince1970];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (![ud objectForKey:@"oldTime"])
    {
        [ud setValue:@(nowTime) forKey:@"oldTime"];
    }
    else
    {
        NSTimeInterval oldTime = [[ud objectForKey:@"oldTime"] doubleValue];
        if (nowTime - oldTime >= 2 * 60 * 60)
        {
            [ud setValue:@(nowTime) forKey:@"oldTime"];
        }
    }
    
    [ud synchronize];
}

- (void)getProductByNetWithAni:(BOOL)hasAni
{
    self.productSectionNum = 0;
    PMNetworking *nw = [PMNetworking defaultNetworking];

    NSDictionary *firstDic = @{@"logo":[UIImage imageNamed:@"phone"],@"title":@"手机数码",@"color":RGBA(223,251,241,1)};
    NSDictionary *secondDic = @{@"logo":[UIImage imageNamed:@"bag"],@"title":@"家用电器",@"color":RGBA(252, 235, 238, 1)};
    NSDictionary *thirdDic = @{@"logo":[UIImage imageNamed:@"electric"],@"title":@"服饰鞋帽",@"color":RGBA(254, 238, 222, 1)};
    NSDictionary *fourthDic = @{@"logo":[UIImage imageNamed:@"colothes"],@"title":@"食品饮料",@"color":RGBA(226, 251, 247, 1)};
    NSDictionary *fifthDic = @{@"logo":[UIImage imageNamed:@"Healthcare"],@"title":@"箱包手表",@"color":RGBA(222, 245, 231, 1)};
    NSDictionary *sixthDic = @{@"logo":[UIImage imageNamed:@"sports"],@"title":@"户外健康",@"color":RGBA(241, 235, 222, 1)};
    NSDictionary *seventhDic = @{@"logo":[UIImage imageNamed:@"food"],@"title":@"家居生活",@"color":RGBA(249, 237, 247, 1)};
    NSDictionary *eighthDic = @{@"logo":[UIImage imageNamed:@"book"],@"title":@"图书音像",@"color":RGBA(254, 236, 233, 1)};
    
    
    NSDictionary *param;
    param = @{@"latitude":@"23.123769",@"longitude":@"113.395305"};
    [nw request:PMRequestStateProductIndex WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1 && [[dic objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
        {
            [self removeAllData];
            
            self.productDicArr = [NSMutableArray array];
            self.scrollProductArr = [NSMutableArray array];
            self.floorAdverArr = [NSMutableArray array];
            self.productList = [NSMutableArray array];
            [PMUserInfos shareUserInfo].defaultArea = [[dic objectNullForKey:@"data"] objectNullForKey:@"defaultArea"];
            self.gridDataDic = [[[dic objectNullForKey:@"data"] objectNullForKey:@"productMap"] mutableCopy];
    
            self.floorAdverArr = [[dic objectNullForKey:@"data"] objectNullForKey:@"floorAdverInfo"];
            
            for (NSDictionary *floorAdDic in self.floorAdverArr)
            {
                NSInteger orderNo = [[floorAdDic objectNullForKey:@"orderNo"] integerValue];
                [self insertData:floorAdDic AndType:PredicateTypeHeader AndSection:orderNo AndTitle:@""];
            }
            
            @try {
                [self dureData:@"firstProductList" AndDic:firstDic AndSection:0 AndStyle:1 AndTitle:@"手机数码"];
                [self dureData:@"thirdProductList" AndDic:secondDic AndSection:1 AndStyle:2 AndTitle:@"家用电器"];
                [self dureData:@"fourthProductList" AndDic:thirdDic AndSection:2 AndStyle:3 AndTitle:@"服饰鞋帽"];
                [self dureData:@"seventhProductList" AndDic:fourthDic AndSection:3 AndStyle:1 AndTitle:@"食品饮料"];
                [self dureData:@"secondProductList" AndDic:fifthDic AndSection:4 AndStyle:2 AndTitle:@"箱包礼品"];
                [self dureData:@"sixthProductList" AndDic:sixthDic AndSection:5 AndStyle:4 AndTitle:@"户外健康"];
                [self dureData:@"fifthProductList" AndDic:seventhDic AndSection:6 AndStyle:3 AndTitle:@"家居生活"];
                [self dureData:@"eighthProductList" AndDic:eighthDic AndSection:7 AndStyle:4 AndTitle:@"图书音像"];
            }
            @catch (NSException *exception) {
                
            }
            
            self.scrollProductArr = [[self getDataArr:PredicateTypeScroll] mutableCopy];
            
            [self.homeTableView reloadData];
        }
        else
        {
            if (![self.app isNetConnect])
            {
                [[MasterViewController defaultMasterVC] showNoNetView];
            }
            else
            {
                [[MasterViewController defaultMasterVC] hideNoNetView];
            }
        }
        
    } showIndicator:NO];
}

- (void)getThemeByNet
{
    self.themeArr = [NSMutableArray array];
    [[PMNetworking defaultNetworking] request:PMRequestStateShowZtList WithParameters:nil callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.themeArr = [dic objectNullForKey:@"data"];
            self.urlHeaderString = [dic objectNullForKey:@"message"];
            
            [self.homeTableView reloadData];
        }
        else
        {
            if (![self.app isNetConnect])
            {
                [[MasterViewController defaultMasterVC] showNoNetView];
            }
            else
            {
                [[MasterViewController defaultMasterVC] hideNoNetView];
            }
        }
        
    } showIndicator:NO];
}

- (void)dureData:(NSString *)key AndDic:(NSDictionary *)sectionDic AndSection:(NSInteger)section AndStyle:(NSInteger)styleInteger AndTitle:(NSString *)title
{
    NSInteger keyCount = 3;
    switch (styleInteger)
    {
        case 1:
            keyCount = 6;
            break;
        case 2:
            keyCount = 9;
            break;
        case 3:
            keyCount = 5;
            break;
        case 4:
            keyCount = 6;
            break;
            
        default:
            break;
    }
    
    if ([[self.gridDataDic objectNullForKey:key] count] > 0)
    {
        self.productSectionNum++;
        [self.productList addObject:sectionDic];
        NSMutableArray *groupArr = [NSMutableArray array];
        NSMutableArray *otherArr = [NSMutableArray array];
        if ([[self.gridDataDic objectNullForKey:key] count] < keyCount)
        {
            for (NSInteger i = 0; i < [[self.gridDataDic objectNullForKey:key] count]; i++)
            {
                [groupArr addObject:[[self.gridDataDic objectNullForKey:key] objectAtIndex:i]];
                [self insertData:[[self.gridDataDic objectNullForKey:key] objectAtIndex:i] AndType:PredicateTypeProduct AndSection:section AndTitle:title];
            }
        }
        else
        {
            for (NSInteger i = 0; i < keyCount; i++)
            {
                [groupArr addObject:[[self.gridDataDic objectNullForKey:key] objectAtIndex:i]];
                [self insertData:[[self.gridDataDic objectNullForKey:key] objectAtIndex:i] AndType:PredicateTypeProduct AndSection:section AndTitle:title];
            }
            
            for (NSInteger i = keyCount; i < [[self.gridDataDic objectNullForKey:key] count]; i++){
                [otherArr addObject:[[self.gridDataDic objectNullForKey:key] objectAtIndex:i]];
                [self insertData:[[self.gridDataDic objectNullForKey:key] objectAtIndex:i] AndType:PredicateTypeScroll AndSection:section AndTitle:title];
            }
        }
        
        [self.scrollProductArr addObject:otherArr];
        [self.productDicArr addObject:groupArr];
    }
}


- (void)insertData:(NSDictionary *)dic AndType:(PredicateType)preType AndSection:(NSInteger)section AndTitle:(NSString *)title
{
    self.app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    HomeData *newsData = [NSEntityDescription insertNewObjectForEntityForName:@"HomeData" inManagedObjectContext:self.app.managedObjectContext];
    if (preType == PredicateTypeProduct)
    {
        newsData.productId = [dic objectNullForKey:@"id"];
        newsData.productName = [dic objectNullForKey:@"productName"];
        newsData.isProduct = [NSNumber numberWithBool:YES];
        newsData.isScroll = [NSNumber numberWithBool:NO];
        newsData.isHeadeAd = [NSNumber numberWithBool:NO];
        newsData.homeImage = [dic objectNullForKey:@"productLogo"];
        newsData.rawPrice = @([[dic objectNullForKey:@"productRawPrice"] doubleValue]);
        newsData.productPrice = @([[dic objectNullForKey:@"productPrice"] doubleValue]);
        id zkMap = [dic objectNullForKey:@"zkMap"];
        if ([zkMap isKindOfClass:[NSDictionary class]] && !isNull(zkMap))
        {
            newsData.isDiscount = [NSNumber numberWithBool:YES];
            newsData.discount = @([[zkMap objectNullForKey:@"discountScale"] doubleValue]);
            newsData.productPrice = @([[zkMap objectNullForKey:@"discountPrice"] doubleValue]);
        }
        else
        {
            newsData.isDiscount = [NSNumber numberWithBool:NO];
            newsData.discount = @(0);
        }
        newsData.headerADName = @"";
        newsData.productSection = @(section + 2);
        newsData.productTitle = title;
    }
    else if (preType == PredicateTypeScroll)
    {
        newsData.productId = [dic objectNullForKey:@"id"];
        newsData.productName = [dic objectNullForKey:@"productName"];
        newsData.isProduct = [NSNumber numberWithBool:NO];
        newsData.isScroll = [NSNumber numberWithBool:YES];
        newsData.isHeadeAd = [NSNumber numberWithBool:NO];
        newsData.homeImage = [dic objectNullForKey:@"productLogo"];
        newsData.rawPrice = @([[dic objectNullForKey:@"productRawPrice"] doubleValue]);
        newsData.productPrice = @([[dic objectNullForKey:@"productPrice"] doubleValue]);
        id zkMap = [dic objectNullForKey:@"zkMap"];
        if ([zkMap isKindOfClass:[NSDictionary class]] && !isNull(zkMap))
        {
            newsData.isDiscount = [NSNumber numberWithBool:YES];
            newsData.discount = @([[zkMap objectNullForKey:@"discountScale"] doubleValue]);
            newsData.productPrice = @([[zkMap objectNullForKey:@"discountPrice"] doubleValue]);
        }
        else
        {
            newsData.isDiscount = [NSNumber numberWithBool:NO];
            newsData.discount = @(0);
        }
        newsData.headerADName = @"";
        newsData.productSection = @(section + 2);
        newsData.productTitle = title;
    }
    else if (preType == PredicateTypeHeader)
    {
        newsData.productId = [dic objectNullForKey:@"id"];
        newsData.homeImage = [dic objectNullForKey:@"clientPhotoUrl"];
        newsData.isHeadeAd = @(YES);
        newsData.isDiscount = @(NO);
        newsData.isProduct = @(NO);
        newsData.rawPrice = @(0);
        newsData.productPrice = @(0);
        newsData.productSection = @(section + 1);
        newsData.discount = @(0);
        newsData.isScroll = @(NO);
        newsData.headerADName = [dic objectNullForKey:@"name"];
        newsData.productName = @"";
        newsData.productTitle = title;
    }
    
    if([self.app.managedObjectContext save:nil])
    {
    }
    else
    {
    }
}

- (void)searchViewShow:(UIButton *)sender
{
    YBSearchViewController *searchView = [[YBSearchViewController alloc] init];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:searchView];
}

- (void)scanQRCode
{
    QRCodeScannerViewController *scannerVC = [QRCodeScannerViewController QRCodeScanner];
    [self.navigationController pushViewController:scannerVC animated:YES];
}

- (void)classSelected:(UIButton *)sender
{
    UIViewController *vc;
    switch (sender.tag)
    {
        case 0://品牌折扣
        {
            [self setButton:sender buttonTag:0];
        }
            break;
        case 1://抢红包
        {
            [self setButton:sender buttonTag:1];
        }
            break;
        case 2://我的关注
        {
            [self setButton:sender buttonTag:2];
            
        }
            return;
        case 3://查物流;
        {
            [self setButton:sender buttonTag:3];
        }
            return;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)setButton:(UIButton *)button buttonTag:(NSInteger)tag
{
    self.view.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect frame = CGRectMake(WIDTH / 4, 80, WIDTH / 4, 85);
        frame.origin.x = WIDTH / 4;
        
    } completion:^(BOOL finished) {
        
        NSInteger index = 0;
        UIButton *button;
        for (UIButton *btn in self.bgArray) {
            
            if (index == tag) {
                button = btn;
                UIGraphicsBeginImageContext(button.frame.size);
                CGContextRef context = UIGraphicsGetCurrentContext();
                [button.layer renderInContext:context];
            }
            index++;
        }
        
        UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // 图片开始变大的起始位置
        UIImageView *photoIV = [[UIImageView alloc] initWithFrame:CGRectMake((WIDTH - (WIDTH / 4)) / 2, 150, WIDTH / 4, 85)];
        [photoIV setImage:theImage];
        photoIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:photoIV];
        photoIV.alpha = 0.5f;
        
        [UIView animateWithDuration:0.5f animations:^{
            
            // 图片变大最终位置
            photoIV.frame = CGRectMake(0, 80, WIDTH, WIDTH);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.3f animations:^{
                
                photoIV.alpha = 0;
                
            } completion:^(BOOL finished) {
                
                [photoIV removeFromSuperview];
                
                UIViewController *vc;
                
                switch (tag) {
                    case 0:
                        vc = [[WebViewViewController alloc] initWithWebType:webTypeDiscount];
                        [self.navigationController pushViewController:vc animated:YES];
                        break;
                    case 1:
                    {
                         vc = [[WebViewViewController alloc] initWithWebType:webTypeHongBao];
                        if ([PMUserInfos shareUserInfo].PM_SID)
                        {
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                        else
                        {
                            [MasterViewController defaultMasterVC].afterLoginAction = ^{
                                [self.navigationController pushViewController:vc animated:YES];
                            };
                            showAlertViewLogin
                        }
                        
                    }
                        break;
                    case 2:
                        
                        if([PMUserInfos shareUserInfo].PM_SID)
                        {
                            vc = [[MyAttentionViewController alloc] init];
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                        else
                        {
                            [MasterViewController defaultMasterVC].afterLoginAction = ^{
                                MyAttentionViewController *vc = [[MyAttentionViewController alloc] init];
                                [self.navigationController pushViewController:vc animated:YES];
                            };
                            showAlertViewLogin
                        }
                        break;
                    case 3:
                         
                        if([PMUserInfos shareUserInfo].PM_SID)
                        {
                            vc = [[DetailTradeViewController alloc] init];
                            ((DetailTradeViewController *)vc).tradeStatus = tradeStatusWaitGet;
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                        else
                        {
                            [MasterViewController defaultMasterVC].afterLoginAction = ^{
                                DetailTradeViewController *vc = [[DetailTradeViewController alloc] init];
                                vc.tradeStatus = tradeStatusWaitGet;
                                [self.navigationController pushViewController:vc animated:YES];
                            };
                            showAlertViewLogin
                        }
                        break;
                        
                    default:
                        break;
                }
                
            }];
        }];
    }];
}


- (NSArray *)getSectionArr:(NSInteger)sectionIndex AndKey:(PredicateType)preType
{
    NSString *predicateString = @"";
    switch (preType) {
        case PredicateTypeProduct:
            predicateString = @"isProduct";
            break;
        case PredicateTypeHeader:
            predicateString = @"isHeadeAd";
            break;
        case PredicateTypeScroll:
            predicateString = @"isScroll";
            break;
        default:
            break;
    }
    self.app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"HomeData" inManagedObjectContext:self.app.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K=1 and productSection=%@",predicateString,@(sectionIndex)];
    [request setEntity:entity];
    request.predicate = predicate;
    NSArray *array = [self.app.managedObjectContext executeFetchRequest:request error:nil];
    return array;
    
}

- (void)goToMore:(UITapGestureRecognizer *)tap
{
    self.view.userInteractionEnabled = NO;
    HomeData *homeData = [self getSectionArr:tap.view.tag AndKey:PredicateTypeHeader][0];
    NSString *title = homeData.headerADName;
    if ([title isEqualToString:@"箱包手表"])
    {
        title = @"箱包";
    }
    else if ([title isEqualToString:@"户外健康"])
    {
        title = @"运动户外";
    }
    else if ([title isEqualToString:@"家居生活"])
    {
        title = @"家居";
    }
    
    [self searchWithString:title];
    
}

- (void)searchWithString:(NSString *)string
{
    SearchResultViewController *searchReult = [[SearchResultViewController alloc] init];
    searchReult.searchString = string;
    searchReult.secondClassId = @"";
    [self.navigationController pushViewControllerWithNavigationControllerTransition:searchReult];
    
}

- (void)goToDetail:(UITapGestureRecognizer *)tap
{
    NSInteger section = tap.view.tag / 100;
    NSInteger index = tap.view.tag % 100;
    NSArray *homeDataArr = [self getSectionArr:section AndKey:PredicateTypeProduct];
    HomeData *homeData = homeDataArr[index - 1];
    NSString *productId = homeData.productId;
    
    PMMyGoodsViewController *detailVC = [[PMMyGoodsViewController alloc] init];
    detailVC.productId = productId;
    [self.navigationController pushViewControllerWithNavigationControllerTransition:detailVC];

}

- (void)getBannerByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateGetAdvertising WithParameters:@{@"typeName":@"banner"} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.bannerArr = [dic objectNullForKey:@"data"];
            [self refreshBanner];
        }
        else
        {
            if (![self.app isNetConnect])
            {
                [[MasterViewController defaultMasterVC] showNoNetView];
            }
            else
            {
                [[MasterViewController defaultMasterVC] hideNoNetView];
            }
        }
        
    } showIndicator:NO];
    
}

- (void)refreshBanner
{
    if (self.bannerArr.count > 0)
    {
        [self.advertisementSV.subviews[0] removeFromSuperview];
        for (NSInteger i = 0; i < self.bannerArr.count; i++)
        {
            UIImageView *adIV = [[UIImageView alloc] initWithFrame:CGRectMake(i * WIDTH, 0, WIDTH, self.advertisementSV.bounds.size.height)];
            adIV.tag = i;
            NSDictionary *bannerDic = self.bannerArr[i];
            [adIV sd_setImageWithURL:[NSURL URLWithString:[bannerDic objectNullForKey:@"photoUrl"]] placeholderImage:[UIImage imageNamed:@"banner_Moren"]];
            [self.advertisementSV addSubview:adIV];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goBannerDetail:)];
            adIV.userInteractionEnabled = YES;
            [adIV addGestureRecognizer:tap];
        }
        [self.advertisementSV setContentSize:CGSizeMake(self.bannerArr.count * WIDTH, 0)];
        [self.pageControl setNumberOfPages:self.bannerArr.count];
        
        [NSTimer scheduledTimerWithTimeInterval:4.0f target:self selector:@selector(changeAdv:) userInfo:nil repeats:YES];
    }
}

- (void)goToHomeDetail:(NSString *)productId
{
    PMMyGoodsViewController *goodVC = [[PMMyGoodsViewController alloc] init];
    goodVC.productId = productId;
    [self.navigationController pushViewControllerWithNavigationControllerTransition:goodVC];
}

- (void)goBannerDetail:(UITapGestureRecognizer *)tap
{
    NSInteger index = tap.view.tag;
    NSDictionary *bannerDic = self.bannerArr[index];
    NSString *urlString = [bannerDic objectNullForKey:@"linkUrl"];
    if ([[bannerDic objectNullForKey:@"mold"] integerValue] == 0)
    {
        if ([urlString rangeOfString:@"productId="].length > 0)
        {
            NSInteger location = [urlString rangeOfString:@"productId="].location + @"productId=".length;
            NSString *productId = [urlString substringFromIndex:location];
            [self getInToProductDetailWithProductId:productId];
        }
        else
        {
            if ([urlString isEqualToString:@"#"])
            {
                
            }
            else if ([urlString rangeOfString:@"key="].length > 0)
            {
                NSString *keys = [urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSInteger location = [keys rangeOfString:@"key="].location + @"key=".length;
                NSString *key = [keys substringFromIndex:location];
                NSLog(@"keys --> %@",key);
                SearchResultViewController *searchVC = [[SearchResultViewController alloc] init];
                searchVC.searchString = key;
                searchVC.secondClassId = @"";
                [self.navigationController pushViewControllerWithNavigationControllerTransition:searchVC];
            }
            else
            {
//                WebViewViewController *webView = [[WebViewViewController alloc] initWithWebType:webTypeDiscount];
                NSInteger location = [urlString rangeOfString:@"?id="].location + @"?id=".length;
                NSString *webId = [urlString substringFromIndex:location];
                NSString *webUrlString = [NSString stringWithFormat:@"http://hdvg.me/html/Brdstore.html?id=%@",webId];
//                NSString *webUrlString = [NSString stringWithFormat:@"http://120.24.234.76/html/Brdstore.html?id=%@",webId];
                SecondViewController *webView = [[SecondViewController alloc] init];
                webView.urlString = webUrlString;
                [self.navigationController pushViewController:webView animated:YES];
            }
        }
    }
    else
    {
        
        NSInteger location = [urlString rangeOfString:@"?id="].location + @"?id=".length;
        NSString *webId = [urlString substringFromIndex:location];
        NSString *webUrlString = [NSString stringWithFormat:@"http://hdvg.me/html/Brdstore.html?id=%@",webId];
//        NSString *webUrlString = [NSString stringWithFormat:@"http://120.24.234.76/html/Brdstore.html?id=%@",webId];
        SecondViewController *webView = [[SecondViewController alloc] init];
        webView.urlString = webUrlString;
        [self.navigationController pushViewController:webView animated:YES];
    }
}

- (void)getInToProductDetailWithProductId:(NSString *)productId
{
    PMMyGoodsViewController *detailVC = [[PMMyGoodsViewController alloc] init];
    detailVC.productId = productId;
    [self.navigationController pushViewControllerWithNavigationControllerTransition:detailVC];
 
}

- (void)checkUpdateClientByNet
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *versionString = [ud objectForKey:@"historyVersion"] ;
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckIndexVersion WithParameters:nil callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSString *newsVersionString = [dic objectNullForKey:@"data"];
            if ([versionString isKindOfClass:[NSString class]] && ![versionString isEqualToString:@""])
            {
                if ([versionString isEqualToString:newsVersionString])
                {
                    NSArray *array = [self getDataArr:PredicateTypeProduct];
                    
                    if (array.count == 0)
                    {
                        [self getProductByNetWithAni:YES];
                    }
                    else
                    {
                        [self getProductSectionNumBySql];
                        self.productDicArr = [[self getDataArr:PredicateTypeProduct] mutableCopy];
                    }
                    
                    NSArray *headerArr = [self getDataArr:PredicateTypeHeader];
                    if (headerArr.count == 0)
                    {
                        
                    }
                    else
                    {
                        self.scrollProductArr = [[self getDataArr:PredicateTypeScroll] mutableCopy];
                    }
                    
                    [self.homeTableView reloadData];
                }
                else
                {
                    [self getProductByNetWithAni:YES];
                    [ud setValue:newsVersionString forKey:@"historyVersion"];
                    [ud synchronize];
                }
            }
            else
            {
                [self getProductByNetWithAni:YES];
                [ud setValue:newsVersionString forKey:@"historyVersion"];
                [ud synchronize];
            }
        }
        
    } showIndicator:NO];
}

- (void)goToThemeDetail:(UIButton *)sender
{
    WebViewViewController *themeWebView = [[WebViewViewController alloc] initWithWebType:webTypeTheme];
    NSDictionary *themeDic = self.themeArr[sender.tag - 1];
    NSString *themeId = [themeDic objectNullForKey:@"id"];
    NSString *imgUrl = [NSString stringWithFormat:@"%@%@",self.urlHeaderString,[themeDic objectNullForKey:@"themePhoto"]];
    themeWebView.themeId = themeId;
    themeWebView.imgUrl = imgUrl;
    [self.navigationController pushViewController:themeWebView animated:YES];
}

- (void)changeAdv:(NSTimer *)timer
{
    CGPoint point = self.advertisementSV.contentOffset;
    point.x += WIDTH;
    if(point.x == (self.advertisementSV.subviews.count-1) * WIDTH)
    {
        point.x = 0;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.advertisementSV.contentOffset = point;
    }];
}

- (void)setTitles
{
    NSDictionary *firstDic = @{@"logo":[UIImage imageNamed:@"phone"],@"title":@"手机数码",@"color":RGBA(223,251,241,1)};
    NSDictionary *secondDic = @{@"logo":[UIImage imageNamed:@"bag"],@"title":@"家用电器",@"color":RGBA(252, 235, 238, 1)};
    NSDictionary *thirdDic = @{@"logo":[UIImage imageNamed:@"electric"],@"title":@"服饰鞋帽",@"color":RGBA(254, 238, 222, 1)};
    NSDictionary *fourthDic = @{@"logo":[UIImage imageNamed:@"colothes"],@"title":@"食品饮料",@"color":RGBA(226, 251, 247, 1)};
    NSDictionary *fifthDic = @{@"logo":[UIImage imageNamed:@"Healthcare"],@"title":@"箱包礼品",@"color":RGBA(222, 245, 231, 1)};
    NSDictionary *sixthDic = @{@"logo":[UIImage imageNamed:@"sports"],@"title":@"户外健康",@"color":RGBA(241, 235, 222, 1)};
    NSDictionary *seventhDic = @{@"logo":[UIImage imageNamed:@"food"],@"title":@"家居生活",@"color":RGBA(249, 237, 247, 1)};
    NSDictionary *eighthDic = @{@"logo":[UIImage imageNamed:@"book"],@"title":@"图书音像",@"color":RGBA(254, 236, 233, 1)};
    
    self.titleCellArray = [NSArray arrayWithObjects:firstDic,secondDic,thirdDic,fourthDic,fifthDic,sixthDic,seventhDic,eighthDic, nil];
}

- (void)getProductSectionNumBySql
{
    self.productSectionNum = 0;
    NSArray * homeDataArr = [self getDataArr:PredicateTypeProduct];
    NSMutableArray *sectionArr = [NSMutableArray array];
    for (HomeData *homeData in homeDataArr)
    {
        BOOL isHas = NO;
        for (NSInteger i = 0; i < sectionArr.count; i++)
        {
            if ([homeData.productSection integerValue] == [sectionArr[i] integerValue])
            {
                isHas = YES;
                break;
            }
        }
        
        if (!isHas)
        {
            [sectionArr addObject:homeData.productSection];
        }
    }
    
    self.productSectionNum = sectionArr.count;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.view.userInteractionEnabled = YES;
}

- (void)getFloorByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateGetAdvertising WithParameters:@{@"typeName":@"floor"} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            if ([[dic objectForKey:@"data"] isKindOfClass:[NSArray class]])
            {
                self.floorArr = [dic objectForKey:@"data"];
                [self.homeTableView reloadData];
            }
        }
    } showIndicator:NO];
}

@end
