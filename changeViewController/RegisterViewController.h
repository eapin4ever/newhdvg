//
//  RegisterViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "MD5Util.h"

@interface RegisterViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) UIView *registerView;
@property (strong, nonatomic) UITextField *aNewNameTextField;
@property (strong, nonatomic) UITextField *aNewAccountPassTF;
@property (strong, nonatomic) UITextField *mobileTextField;
@property (strong, nonatomic) UITextField *registerAuthcodeTF;

@property (strong, nonatomic) UILabel *nicknameLabel;
@property (strong, nonatomic) UILabel *mobileLabel;
@property (strong, nonatomic) UIButton *authcodeButton;
//@property (strong, nonatomic) UIButton *registerButton;
@property (strong,nonatomic) UIButton *nextBtn;

@property (strong, nonatomic) PMNetworking *networking;

@end
