//
//  PMOrderBtnCell.m
//  changeViewController
//
//  Created by pmit on 15/10/30.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMOrderBtnCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMOrderBtnCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.goToPayBtn)
    {
        self.truePayLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH - WidthRate(200) - WidthRate(200), 20)];
        self.truePayLB.font = [UIFont systemFontOfSize:14.0f];
        self.truePayLB.textAlignment = NSTextAlignmentLeft;
        self.truePayLB.textColor = HDVGRed;
        [self.contentView addSubview:self.truePayLB];
        
        self.goToPayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.goToPayBtn.frame = CGRectMake(WIDTH - WidthRate(200), 10, WidthRate(200), 20);
        [self.goToPayBtn setTitle:@"去付款" forState:UIControlStateNormal];
        [self.goToPayBtn setTitleColor:HDVGRed forState:UIControlStateNormal];
        self.goToPayBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [self.goToPayBtn addTarget:self action:@selector(cellBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.goToPayBtn];
        
        self.cancelPayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.cancelPayBtn.frame = CGRectMake(WIDTH - WidthRate(200) - WidthRate(200), 10, WidthRate(200), 20);
        [self.cancelPayBtn setTitle:@"取消订单" forState:UIControlStateNormal];
        [self.cancelPayBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.cancelPayBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.cancelPayBtn addTarget:self action:@selector(cellBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.cancelPayBtn];
        
        self.waitSendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.waitSendBtn.frame = CGRectMake(WIDTH - WidthRate(250), 10, WidthRate(250), 20);
        [self.waitSendBtn setTitle:@"商家备货中" forState:UIControlStateNormal];
        [self.waitSendBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.waitSendBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.contentView addSubview:self.waitSendBtn];
        
        self.checkLogBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.checkLogBtn.frame = CGRectMake(WIDTH - WidthRate(200), 10, WidthRate(200), 20);
        [self.checkLogBtn setTitle:@"查物流" forState:UIControlStateNormal];
        [self.checkLogBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.checkLogBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.checkLogBtn addTarget:self action:@selector(cellBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.checkLogBtn];
        
        self.sureGetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.sureGetBtn.frame = CGRectMake(WIDTH - WidthRate(200) - WidthRate(200), 10, WidthRate(200), 20);
        [self.sureGetBtn setTitle:@"确认收货" forState:UIControlStateNormal];
        [self.sureGetBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.sureGetBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.sureGetBtn addTarget:self action:@selector(cellBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.sureGetBtn];
        
        self.discussBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.discussBtn.frame = CGRectMake(WIDTH - WidthRate(200), 10, WidthRate(200), 20);
        [self.discussBtn setTitle:@"评价" forState:UIControlStateNormal];
        [self.discussBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.discussBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.discussBtn addTarget:self action:@selector(cellBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.discussBtn];
        
        self.hasDiscussBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.hasDiscussBtn.frame = CGRectMake(WIDTH - WidthRate(200), 10, WidthRate(200), 20);
        [self.hasDiscussBtn setTitle:@"查看评价" forState:UIControlStateNormal];
        [self.hasDiscussBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.hasDiscussBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.hasDiscussBtn addTarget:self action:@selector(cellBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.hasDiscussBtn];
        
        self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.deleteBtn.frame = CGRectMake(WIDTH - WidthRate(200), 10, WidthRate(200), 20);
        [self.deleteBtn setTitle:@"已取消" forState:UIControlStateNormal];
        [self.deleteBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.deleteBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.contentView addSubview:self.deleteBtn];
        
    }
}

- (void)setCellData:(NSString *)truePayString OrderBtnType:(PMOrderBtnType)orderBtnType DiscussStatus:(BOOL)hasDiscuss
{
    switch (orderBtnType)
    {
        case PMOrderBtnTypeWaitPay:
        {
            self.discussBtn.hidden = YES;
            self.hasDiscussBtn.hidden = YES;
            self.goToPayBtn.hidden = NO;
            self.cancelPayBtn.hidden = NO;
            self.waitSendBtn.hidden = YES;
            self.sureGetBtn.hidden = YES;
            self.checkLogBtn.hidden = YES;
            self.deleteBtn.hidden = YES;
        }
            break;
        case PMOrderBtnTypeWaitSend:
        {
            self.discussBtn.hidden = YES;
            self.hasDiscussBtn.hidden = YES;
            self.goToPayBtn.hidden = YES;
            self.cancelPayBtn.hidden = YES;
            self.waitSendBtn.hidden = YES;
            self.sureGetBtn.hidden = YES;
            self.checkLogBtn.hidden = YES;
            self.deleteBtn.hidden = YES;
        }
            break;
        case PMOrderBtnTypeWaitGet:
        {
            self.discussBtn.hidden = YES;
            self.hasDiscussBtn.hidden = YES;
            self.goToPayBtn.hidden = YES;
            self.cancelPayBtn.hidden = YES;
            self.waitSendBtn.hidden = YES;
            self.sureGetBtn.hidden = NO;
            self.checkLogBtn.hidden = NO;
            self.deleteBtn.hidden = YES;
        }
            break;
        case PMOrderBtnTypeWaitDiscuss:
        {
            if (hasDiscuss)
            {
                self.discussBtn.hidden = YES;
                self.hasDiscussBtn.hidden = NO;
            }
            else
            {
                self.discussBtn.hidden = NO;
                self.hasDiscussBtn.hidden = YES;
            }
            
            self.goToPayBtn.hidden = YES;
            self.cancelPayBtn.hidden = YES;
            self.waitSendBtn.hidden = YES;
            self.sureGetBtn.hidden = YES;
            self.checkLogBtn.hidden = YES;
            self.deleteBtn.hidden = YES;
        }
            break;
        case PMOrderBtnTypeHasCancel:
        {
            self.discussBtn.hidden = YES;
            self.hasDiscussBtn.hidden = YES;
            self.goToPayBtn.hidden = YES;
            self.cancelPayBtn.hidden = YES;
            self.waitSendBtn.hidden = YES;
            self.sureGetBtn.hidden = YES;
            self.checkLogBtn.hidden = YES;
            self.deleteBtn.hidden = YES;
        }
            break;
        case PMOrderBtnTypeTurnBack:
        {
            self.discussBtn.hidden = YES;
            self.hasDiscussBtn.hidden = YES;
            self.goToPayBtn.hidden = YES;
            self.cancelPayBtn.hidden = YES;
            self.waitSendBtn.hidden = YES;
            self.sureGetBtn.hidden = YES;
            self.checkLogBtn.hidden = YES;
            self.deleteBtn.hidden = YES;
        }
        default:
            break;
    }
    
    NSString *showPriceString = [NSString stringWithFormat:@"实付款：%@",truePayString];
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:showPriceString];
    [nodeString addAttributes:@{UITextAttributeTextColor:[UIColor grayColor]} range:NSMakeRange(0, 4)];
    self.truePayLB.attributedText = nodeString;
}

- (void)cellBtnAction:(UIButton *)sender
{
    if ([self.orderBtnDelegate respondsToSelector:@selector(cellBtnClickAction:)])
    {
        [self.orderBtnDelegate cellBtnClickAction:sender];
    }
}

@end
