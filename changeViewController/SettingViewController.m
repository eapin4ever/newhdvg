//
//  SettingViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/7.
//  Copyright (c) 2014年 wallace. All rights reserved.
//
//

#import "SettingViewController.h"
#import "FeedbackViewController.h"//意见反馈
#import "MasterViewController.h"
#import "SafeManageTableViewCell.h"
#import "AppDelegate.h"

#define TEL @"400-091-8114"


@interface SettingViewController ()<UIGestureRecognizerDelegate,UINavigationBarDelegate>
@property(nonatomic,strong) UIButton *exitBtn;
@property (assign,nonatomic) BOOL isPush;
@end

@implementation SettingViewController
@synthesize lastIndexPath;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"设置";
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sectionThreeArray = [NSMutableArray arrayWithObjects:@"清除缓存",@"客服电话",@"意见反馈", nil];
    self.sectionFourArray = [NSMutableArray arrayWithObjects:@"", nil];
    
    
    // 初始化设置 tableView
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [[UIScreen mainScreen] bounds].size.height - 64)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //self.tableView.scrollEnabled = NO;
    [self.tableView registerClass:[SafeManageTableViewCell class] forCellReuseIdentifier:@"moreCell"];
    [self.view addSubview:self.tableView];
    
    self.tableView.backgroundColor = HDVGPageBGGray;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
    
    // 添加logo
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(350))];
    headerView.backgroundColor = HDVGPageBGGray;
    
    self.logoImage = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(100), HeightRate(100), WIDTH - WidthRate(100) * 2, HeightRate(200))];
    self.logoImage.image = [UIImage imageNamed:@"LOGO_image.png"];
    [headerView addSubview:self.logoImage];
    self.logoImage.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *versionLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(100), HeightRate(300), WIDTH - WidthRate(100) * 2, HeightRate(50))];
    NSString *appVersionStr = [[[NSBundle mainBundle] infoDictionary] objectNullForKey:@"CFBundleVersion"];
    versionLB.textAlignment = NSTextAlignmentCenter;
    versionLB.font = [UIFont systemFontOfSize:17.0f];
    versionLB.text = [NSString stringWithFormat:@"V%@",appVersionStr];
    versionLB.textColor = HDVGFontColor;
    [headerView addSubview:versionLB];
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(300))];
    footerView.backgroundColor = HDVGPageBGGray;
    
    // 初始化退出账号按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(WidthRate(30), HeightRate(120), WIDTH - WidthRate(60), 40);
    [button setTitle:@"退出账号" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [button addTarget:self action:@selector(exitButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [button.layer setCornerRadius:5.0f];
    [button setBackgroundColor:ButtonBgColor];
    [footerView addSubview:button];
    self.exitBtn = button;
    self.exitBtn.hidden = YES;
    
    self.tableView.tableHeaderView = headerView;
    self.tableView.tableFooterView = footerView;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([PMUserInfos shareUserInfo].PM_SID != nil) {
        self.exitBtn.hidden = NO;
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(100);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(100))];
    headerView.backgroundColor = HDVGPageBGGray;
    
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, headerView.frame.size.height - HeightRate(1), WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [headerView.layer addSublayer:layer1];
    
    return headerView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [self.sectionThreeArray count];
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SafeManageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"moreCell"];
    
    NSInteger row = [indexPath row];
    
    // First section
    if (indexPath.section == 0) {
        cell.textLabel.text = [self.sectionThreeArray objectAtIndex:row];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        cell.textLabel.textColor = RGBA(86, 86, 86, 1);
        
        if (indexPath.row == 0) {
            
        }
        else if (indexPath.row == 1) {
            
            // 客服电话
            UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(320), 7, WidthRate(260), 30)];
            phoneLabel.backgroundColor = [UIColor clearColor];
            phoneLabel.text = TEL;
            phoneLabel.textColor = RGBA(86, 86, 86, 1);
            phoneLabel.textAlignment = NSTextAlignmentRight;
            phoneLabel.font = [UIFont systemFontOfSize:14.0f];
            
            [cell.contentView addSubview:phoneLabel];
        }
//        else if (indexPath.row == 2) {
//            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//            // 检测更新按钮
//            UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
//            checkButton.frame = CGRectMake(0, 1, WIDTH, 42);
////            [checkButton addTarget:self action:@selector(checkButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//            [cell.contentView addSubview:checkButton];
//            
//            // 当前使用的版本号
//            NSString *appVersionStr = [[[NSBundle mainBundle] infoDictionary] objectNullForKey:@"CFBundleVersion"];
//            
//            // 创建 label 显示版本号
//            UILabel *versionLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(320), 7, WidthRate(260), 30)];
//            versionLab.backgroundColor = [UIColor clearColor];
//            versionLab.text = [NSString stringWithFormat:@"V%@", appVersionStr];
//            versionLab.textColor = RGBA(86, 86, 86, 1);
//            versionLab.textAlignment = NSTextAlignmentRight;
//            versionLab.font = [UIFont systemFontOfSize:14.0f];
//            [cell.contentView addSubview:versionLab];
//            
//        }
        else if (indexPath.row == 2) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            CALayer *layer = [[CALayer alloc] init];
            layer.frame = CGRectMake(0, 44 - HeightRate(1), WIDTH, HeightRate(1));
            layer.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            [cell.contentView.layer addSublayer:layer];
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone; // 去掉cell被选中时的颜色
    
    return cell;
}


#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"是否清除商品信息和图片缓存？" message:nil delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        alertView.tag = 1;
        [alertView show];
    }
    else if (indexPath.section == 0 && indexPath.row == 2) {
        if([PMUserInfos shareUserInfo].PM_SID == nil)
        {
            [MasterViewController defaultMasterVC].afterLoginAction = ^{
                self.isPush = YES;
                [self tableView:tableView didSelectRowAtIndexPath:indexPath];
            };
            showAlertViewLogin
        }
        else
        {
            FeedbackViewController *feedbackVC = [[FeedbackViewController alloc] init];
            [self.navigationController pushViewController:feedbackVC animated:YES];
        }
    }
    else if (indexPath.section == 0 && indexPath.row == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",TEL]]];
    }
}


#pragma mark - 版本判断
- (void)checkButtonClick:(id)sender
{
/*
    //获取版本号
    NSString *appVersionStr = [[[NSBundle mainBundle] infoDictionary] objectNullForKey:@"CFBundleVersion"];
    
    NSString *versionNo = [NSString stringWithFormat:@"V_%@", appVersionStr];
    NSString *type = @"2-1-1";
    
    NSDictionary *versionDict = @{@"versionNo":versionNo,@"type":type};
    
    self.networking = [PMNetworking defaultNetworking];
    
    [self.networking request:PMRequestStateCheckSysVersion WithParameters:versionDict callBackBlock:^(NSDictionary *dict) {
        
        if ([[[dict objectNullForKey:@"data"] objectNullForKey:@"isUpdate"] isEqualToString:@"YES"]) {
            
            // 获得服务器版本全部数据数组
            self.versionDict = [[dict objectNullForKey:@"data"] mutableCopy];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dict objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"更新" otherButtonTitles:nil, nil];
            alertView.tag = 2;
            [alertView show];
            
        }
        else {
            [self noNewVersion];
        }
    }];
 */
//    [[AppDelegate shareAppDelegate] checkClientVersionNotUpdate:^{
//        [self noNewVersion];
//    }];
    
}

- (void)noNewVersion
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"此版本已是最新版本！" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
    [alertView show];
}

- (void)downLoadMessage:(NSDictionary *)appVersion
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"更新提示" message:[appVersion objectNullForKey:@"introduce"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"更新", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1 && alertView.tag == 1)
    {
        // 清除缓存
        NSURLCache *cache = [NSURLCache sharedURLCache];
        
        
        [cache removeAllCachedResponses];
        
        //清除图片缓存
        [[SDImageCache sharedImageCache] clearDisk];
        
        [[SDImageCache sharedImageCache] clearMemory];
        
    }
    else if(alertView.tag == 2)
    {
        //清除所有页面数据
        [[MasterViewController defaultMasterVC] userLogout];
        
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    else if (alertView.tag == 202)
    {
        [[MasterViewController defaultMasterVC] transitionToLoginVC];
    }
}

#pragma mark - 退出账号按钮事件方法
- (void)exitButtonClick:(id)sender
{
    if ([PMUserInfos shareUserInfo].PM_SID == nil) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"用户还没有登录" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
        return;
    }
    else
    {
        [[PMNetworking defaultNetworking] request:PMRequestStateLogout WithParameters:@{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID} callBackBlock:^(NSDictionary *dict){
            
            if ([dict objectNullForKey:@"success"]) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"成功退出登录" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 2;
                [alertView show];
                
                
                //清空用户数据
                [[PMUserInfos shareUserInfo] logout];
                
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"退出登录失败,请重试" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }showIndicator:YES];
    }
}


@end
