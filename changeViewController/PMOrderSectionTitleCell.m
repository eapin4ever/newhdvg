//
//  PMOrderSectionTitleCell.m
//  changeViewController
//
//  Created by pmit on 15/11/4.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMOrderSectionTitleCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMOrderSectionTitleCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.sectionIV)
    {
        self.sectionIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 20, 20)];
        self.sectionIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.sectionIV];
        
        NSString *tempString = @"商品信息";
        CGSize tempSize = [tempString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17.0f]} context:nil].size;
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, tempSize.width, 20)];
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        self.titleLB.font = [UIFont systemFontOfSize:17.0f];
        self.titleLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.titleLB];
        
        self.moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.moreBtn.frame = CGRectMake(WIDTH - 100, 10, 60, 20);
        [self.moreBtn setTitle:@"查看全部" forState:UIControlStateNormal];
        [self.moreBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.moreBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.moreBtn addTarget:self action:@selector(goToMoreProduct:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.moreBtn];
        
        
    }
}

- (void)setCellData:(NSString *)imgName TitleString:(NSString *)titleString IsMoreBtnShow:(BOOL)isMoreBtnShow
{
    self.sectionIV.image = [UIImage imageNamed:imgName];
    self.titleLB.text = titleString;
    if (isMoreBtnShow)
    {
        self.moreBtn.hidden = NO;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        self.moreBtn.hidden = YES;
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (void)goToMoreProduct:(UIButton *)sender
{
    if ([self.titleCellDelegate respondsToSelector:@selector(goToMore)])
    {
        [self.titleCellDelegate goToMore];
    }
}

@end
