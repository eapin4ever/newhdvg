//
//  PMCurrentGoodsTableViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMDistributionsGoodsTableViewCell.h"
#import <ShareSDK/ShareSDK.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface PMCurrentGoodsTableViewController : UITableViewController
@property(nonatomic,strong)NSMutableArray *listArray;

@property(nonatomic,strong)NSMutableArray *allRowsIndexPath;
@property(nonatomic,assign)BOOL isSelectAll;
@property(nonatomic,assign)BOOL isLoad;//判断是不是加载
@property (assign,nonatomic) BOOL isSearch;

//下面两个数组用于数组的多行选择，全选等
@property(nonatomic,strong)NSMutableArray *allIndexPaths;
@property(nonatomic,strong)NSMutableArray *selectedIndexPaths;

+ (PMCurrentGoodsTableViewController *)currentVC;
- (void)unsoldSelectedGoods;
- (void)selectAllRows:(UIButton *)sender;

- (void)getModelsBySearchString:(NSString *)searchString finish:(myFinishBlock)finish error:(myFinishBlock)error;
@end
