//
//  PMNearbySearchViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/26.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMNearbySearchViewController : UIViewController

@property(nonatomic,strong)UIImageView *scope;

- (void)moveItem:(UIView *)item changeX:(CGFloat)x changeY:(CGFloat)y finishAction:(myFinishBlock)finish;
- (void)hideResultVC;
- (void)showResultVC;
@end
