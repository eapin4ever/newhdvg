//
//  HomeTitleCell.h
//  changeViewController
//
//  Created by pmit on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTitleCell : UITableViewCell

@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UIView *colorView;

- (void)createUI;
- (void)setCellTitle:(NSString *)titleString AndColor:(UIColor *)cellColor;

@end
