//
//  YBQueueUtils.m
//  changeViewController
//
//  Created by ZhangEapin on 15/5/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBQueueUtils.h"
#import "Reachability.h"

@implementation YBQueueUtils

static YBQueueUtils *_queueUtil;

+ (YBQueueUtils *)defaultQueue
{
    if (!_queueUtil)
    {
        _queueUtil = [[YBQueueUtils alloc] init];
        _queueUtil.queue = [[NSOperationQueue alloc] init];
    }
    
    return _queueUtil;
}

- (void)checkNetStaus
{
    BOOL isWifi = NO;
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    switch ([reachability currentReachabilityStatus]) {
        case ReachableViaWiFi:
            isWifi = YES;
            break;
        default:
            isWifi = NO;
            break;
    }
    if (!isWifi)
    {
        _queueUtil.queue.maxConcurrentOperationCount = 6;
    }
    
}



@end
