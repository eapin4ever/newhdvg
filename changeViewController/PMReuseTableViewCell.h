//
//  PMReuseTableViewCell.h
//  changeViewController
//
//  Created by pmit on 14/11/22.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMReuseTableViewCell : UITableViewCell
@property(nonatomic,strong)UICollectionView *cv;

- (void)initCollectionViewWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout dataSource:(id<UICollectionViewDataSource>)dataSource delegate:(id<UICollectionViewDelegate>)delegate registerClass:(Class)registerClass reuseIdentify:(NSString *)identify;


@end
