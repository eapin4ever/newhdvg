//
//  QRCodeScannerViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/7.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#define originY HeightRate(295)
#define targetY HeightRate(725)

#import "QRCodeScannerViewController.h"
#import <ZBarSDK.h>

@interface QRCodeScannerViewController ()<ZBarReaderViewDelegate>
@property(nonatomic,strong)ZBarReaderView *readerView;
@property(nonatomic,copy)NSString *scanResult;

@end

static QRCodeScannerViewController *_scanner;
@implementation QRCodeScannerViewController
{
    BOOL _isUp;
}

+ (QRCodeScannerViewController *)QRCodeScanner
{
    if(!_scanner)
    {
        _scanner = [[QRCodeScannerViewController alloc] init];
    }
    _scanner.scanResult = nil;
    return _scanner;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"扫描二维码";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self QRCodeScanner];
}

#pragma mark - 初始化二维码扫码器，并且开始扫描
- (void)QRCodeScanner
{
//    UIButton *exitQRCodeSannerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    exitQRCodeSannerBtn.frame = CGRectMake(20, 20, self.view.bounds.size.width - 40, 36);
//    [exitQRCodeSannerBtn setTitle:@"退 出 扫 描" forState:UIControlStateNormal];
//    [exitQRCodeSannerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [exitQRCodeSannerBtn setBackgroundColor:[UIColor lightGrayColor]];
//    [exitQRCodeSannerBtn.layer setCornerRadius:8.0];
//    [exitQRCodeSannerBtn addTarget:self action:@selector(clickToExitScan:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:exitQRCodeSannerBtn];
    
    ZBarReaderView *readerView = [[ZBarReaderView alloc] init];
    self.readerView = readerView;
    [self.view addSubview:readerView];
    
    //设置大小
    readerView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    self.view.backgroundColor = [UIColor blackColor];
    
    //设置外观
    readerView.layer.cornerRadius = 8.0f;
    readerView.layer.borderWidth = 1.0f;
    readerView.layer.borderColor = [HDVGRed CGColor];
    
    //设置代理，当扫描到二维码时，会调用代理方法
    readerView.readerDelegate = self;
    
    //关闭闪光灯
    readerView.torchMode = 0;
    
    
    //如果是模拟器
    if(TARGET_IPHONE_SIMULATOR)
    {
        ZBarCameraSimulator *cameraSimulator = [[ZBarCameraSimulator alloc] initWithViewController:self];
        cameraSimulator.readerView = readerView;
    }
    
    //添加遮罩
    UIImageView *iv = [[UIImageView alloc] initWithFrame:readerView.bounds];
    iv.image = [UIImage imageNamed:@"QRCode_bg"];
    iv.alpha = 0.6;
    [readerView addSubview:iv];
    
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(134), originY, WidthRate(485), HeightRate(20))];
    lineIV.image = [UIImage imageNamed:@"QRCode_Line"];
    [iv addSubview:lineIV];
    
    [self lineMove:lineIV];
    //设置扫描区域，默认是readerView的整个区域都是扫描区域
    //CGRect readerMaskRect = CGRectMake(60, 60, self.view.bounds.size.width - 120 , self.view.bounds.size.width - 120);//居中正方形
    
    //readerView.scanCrop = [self getScanCropWithScanRect:readerMaskRect andReaderViewBounds:readerView.bounds];
    
    [readerView start];
}

- (CGRect)getScanCropWithScanRect:(CGRect)rect andReaderViewBounds:(CGRect)rvBounds
{
    CGFloat x,y,width,height;
    
    x = rect.origin.y / rvBounds.size.height;
    y = 1 - (rect.origin.x + rect.size.width) / rvBounds.size.width;
    width = rect.size.height / rvBounds.size.height;
    height = rect.size.width / rvBounds.size.width;
    
    return CGRectMake(x, y, width, height);
    
}


- (void)lineMove:(UIImageView *)iv
{
    [UIView animateWithDuration:2 animations:^{
        CGRect rect = iv.frame;
        if(_isUp)
        {
            rect.origin.y = originY;
        }
        else
        {
            rect.origin.y = targetY;
        }
        iv.frame = rect;
    } completion:^(BOOL finished) {
        _isUp = !_isUp;
        [self lineMove:iv];
    }];
}



#pragma mark - ZBarReaderViewDelegate方法
//当扫描到二维码的时候，会调用此方法
- (void) readerView: (ZBarReaderView*) readerView
     didReadSymbols: (ZBarSymbolSet*) symbols
          fromImage: (UIImage*) image
{
    NSLog(@"didReadSymbols");
    for (ZBarSymbol *symbol in symbols)
    {
        NSLog(@"%@",symbol.data);
        self.scanResult = symbol.data;
    }
    
    [self.readerView stop];

//    if([self.scanResult hasPrefix:@"http://"] || [self.scanResult hasPrefix:@"HTTP://"] || [self.scanResult hasPrefix:@"https://"] || [self.scanResult hasPrefix:@"HTTPS://"])
//        [self.readerView stop];
}


- (void) readerViewDidStart: (ZBarReaderView*) readerView
{
    
}

- (void) readerView: (ZBarReaderView*) readerView
   didStopWithError: (NSError*) error
{
    
    NSLog(@"self.scanResult == %@",self.scanResult);
    
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    PMBrowser *browser = [PMBrowser defaultWebBrowserWithController:nil];
    [self.navigationController popViewControllerAnimated:YES];
    [browser getInShop:self.scanResult ByPushController:self];
    
//    if([self.scanResult hasPrefix:@"http://"] || [self.scanResult hasPrefix:@"HTTP://"] || [self.scanResult hasPrefix:@"https://"] || [self.scanResult hasPrefix:@"HTTPS://"])
//    {
//        PMBrowser *browser = [PMBrowser defaultWebBrowserWithController:nil];
//        [self.navigationController popViewControllerAnimated:YES];
//        [browser getInShop:self.scanResult ByPushController:self];
//    }
    
    
}

#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
