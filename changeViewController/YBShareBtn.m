//
//  YBShareBtn.m
//  changeViewController
//
//  Created by ZhangEapin on 15/5/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBShareBtn.h"

@implementation YBShareBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:11.0f];
    }
    return self;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, contentRect.size.height * 0.45, contentRect.size.width, contentRect.size.height * 0.15);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, contentRect.size.height * 0.6, contentRect.size.width, contentRect.size.height * 0.2);
}

@end
