//
//  SafeManageViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/12.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "BindingMobileViewController.h"
#import "NewPassViewController.h"
#import "PMUserInfos.h"

@interface SafeManageViewController : UIViewController

@property (strong, nonatomic) UIView *safeManageView;
@property (strong, nonatomic) NSMutableArray *safeManageArray;
@property (strong, nonatomic) UILabel *mobileNumberLab;

@end
