//
//  PMProductCellImageView.m
//  changeViewController
//
//  Created by pmit on 15/3/11.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMProductCellImageView.h"

@implementation PMProductCellImageView

- (void)setImage:(UIImage *)image
{
    [super setImage:image];
    if(_btn)
        [_btn setImage:image forState:UIControlStateNormal];
}
@end
