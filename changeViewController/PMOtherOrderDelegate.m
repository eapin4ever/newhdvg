//
//  PMOtherOrderDelegate.m
//  changeViewController
//
//  Created by pmit on 15/10/29.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMOtherOrderDelegate.h"
#import "PMOrderTimeStatesCell.h"
#import "MyTradeTableViewCell.h"
#import "PMOrderBtnCell.h"
#import "PMMyAllOrderViewController.h"

@interface PMOtherOrderDelegate() <PMOrderBtnCellDelegate>

@end

@implementation PMOtherOrderDelegate

static NSString *const orderTimeCell = @"orderTimeCell";
static NSString *const tradeCellIdetify = @"tradeCell";
static NSString *const orderBtnCell = @"orderBtnCell";

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.orderArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.orderArr[section] count] + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *oneDic = [self.orderArr[indexPath.section] firstObject];
    if (indexPath.row == 0)
    {
        NSDictionary *oneDic = [self.orderArr[indexPath.section] firstObject];
        PMOrderTimeStatesCell *cell = [tableView dequeueReusableCellWithIdentifier:orderTimeCell];
        [cell createUI];
        NSString *orderStateString = @"";
        switch (self.orderType)
        {
            case PMOrderTypeWaitPay:
                orderStateString = @"等待买家付款";
                break;
            case PMOrderTypeWaitGet:
                orderStateString = @"等待买家收货";
                break;
            case PMOrderTypeWaitDiscuss:
                orderStateString = @"";
                break;
            default:
                break;
        }
        [cell setCellData:[oneDic objectForKey:@"createDt"] State:orderStateString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == [self.orderArr[indexPath.section] count] + 1)
    {
        NSInteger status = 0;
        NSString *statusString = [oneDic objectForKey:@"tradeStatus"];
        PMOrderBtnCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderBtnCell"];
        [cell createUI];
        if ([statusString isEqualToString:@"待付款"])
        {
            status = -11;
        }
        else if ([statusString isEqualToString:@"待发货"])
        {
            status = 3;
        }
        else if ([statusString isEqualToString:@"待收货"] || [statusString isEqualToString:@"已发货"])
        {
            status = 0;
        }
        else if ([statusString isEqualToString:@"已收货"])
        {
            status = 1;
        }
        
        cell.orderBtnDelegate = self;
        cell.goToPayBtn.tag = indexPath.section * 10000 + 1001;
        cell.cancelPayBtn.tag = indexPath.section * 10000 + 1002;
        cell.waitSendBtn.tag = indexPath.section * 10000 + 1003;
        cell.checkLogBtn.tag = indexPath.section * 10000 + 1004;
        cell.sureGetBtn.tag = indexPath.section * 10000 + 1005;
        cell.discussBtn.tag = indexPath.section * 10000 + 1006;
        cell.hasDiscussBtn.tag = indexPath.section * 10000 + 1007;
        
        switch (status)
        {
            case 3:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitSend DiscussStatus:NO];
                break;
            case -11:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitPay DiscussStatus:NO];
                break;
            case 0:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitGet DiscussStatus:NO];
                break;
            case 1:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitDiscuss DiscussStatus:[[oneDic objectForKey:@"discussStatus"] integerValue] == 0 ? NO : YES];
                break;
            case 2:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitGet DiscussStatus:NO];
                break;
                
            default:
                break;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        MyTradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tradeCellIdetify];
        [cell setMyTradeOrderDataUI];
        
        NSArray *arr = self.orderArr[indexPath.section];
        NSDictionary *dict = arr[indexPath.row - 1];
        
        //给一张默认图片，先使用默认图片，当图片加载完成后再替换
        [cell.iv sd_setImageWithURL:[dict objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
        
        NSString *aSpecVal = [dict objectNullForKey:@"aSpecVal"];
        NSString *bSpecVal = [dict objectNullForKey:@"bSpecVal"];
        NSString *cSpecVal = [dict objectNullForKey:@"cSpecVal"];
        NSString *dSpecVal = [dict objectNullForKey:@"dSpecVal"];
        NSString *eSpecVal = [dict objectNullForKey:@"eSpecVal"];
        
        NSMutableString *specificationStr = [NSMutableString string];
        
        if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", aSpecVal];
        }
        else if (bSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", bSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", cSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", dSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", eSpecVal];
        }
        
        [cell setTitle:[dict objectNullForKey:@"productName"] setSpecification:specificationStr setNumber:[[dict objectNullForKey:@"productNum"] integerValue] setPrice:[[dict objectNullForKey:@"productPrice"] doubleValue]];
        
        cell.turnBackBtn.hidden = YES;
        
        return cell;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 40;
    }
    else if (indexPath.row != [self.orderArr[indexPath.section] count] + 1)
    {
        return (iPhone6_Plus || iPhone6 ? 110 : 90);
    }
    
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 10;//设置你footer高度
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
    
    BOOL isShouldLoadMore = NO;
    PMMyAllOrderViewController *orderVC = (PMMyAllOrderViewController *)self.doubleDelegate;
    switch (self.orderType)
    {
        case PMOrderTypeWaitPay:
            
            isShouldLoadMore = orderVC.isWaitPayLoadMore && !orderVC.isWaitPayLoading;
            
            break;
        
        case PMOrderTypeWaitGet:
            
            isShouldLoadMore = orderVC.isWaitGetLoadMore && !orderVC.isWaitGetLoading;
            
            break;
            
        case PMOrderTypeWaitDiscuss:
            
            isShouldLoadMore = orderVC.isWaitDiscussLoadMore && !orderVC.isWaitDiscussLoading;
            
            break;
            
        default:
            break;
    }
    
    if (isShouldLoadMore) {
        CGFloat scrollPosition = scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y;
        if (scrollPosition < [self footerLoadMoreHeight]) {
//            self.isAllOrderShow = NO;
            switch (self.orderType)
            {
                case PMOrderTypeWaitPay:
                    
                    orderVC.isWaitPayShow = NO;
                    
                    break;
                    
                case PMOrderTypeWaitGet:
                    
                    orderVC.isWaitGetShow = NO;
                    
                    break;
                    
                case PMOrderTypeWaitDiscuss:
                    
                    orderVC.isWaitDiscussShow = NO;
                    
                    break;
                    
                default:
                    break;
            }
            [self loadMore];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = RGBA(243, 243, 243, 1);
    return sectionHeaderView;
}

- (void)cellBtnClickAction:(UIButton *)sender
{
    if ([self.doubleDelegate respondsToSelector:@selector(delegateBtnClick:)])
    {
        [self.doubleDelegate delegateBtnClick:sender];
    }
}

- (CGFloat)footerLoadMoreHeight
{
    switch (self.orderType) {
        case PMOrderTypeWaitPay:
        {
            if (((PMMyAllOrderViewController *)self.doubleDelegate).waitPayLoadMoreView)
                return ((PMMyAllOrderViewController *)self.doubleDelegate).waitPayLoadMoreView.frame.size.height;
            else
                return 10;
        }
            break;
        case PMOrderTypeWaitGet:
        {
            if (((PMMyAllOrderViewController *)self.doubleDelegate).waitGetLoadMoreView)
                return ((PMMyAllOrderViewController *)self.doubleDelegate).waitGetLoadMoreView.frame.size.height;
            else
                return 10;
        }
            break;
        case PMOrderTypeWaitDiscuss:
        {
            if (((PMMyAllOrderViewController *)self.doubleDelegate).waitDiscussLoadMoreView)
                return ((PMMyAllOrderViewController *)self.doubleDelegate).waitDiscussLoadMoreView.frame.size.height;
            else
                return 10;
        }
            break;
            
        default:
            break;
    }
}

- (BOOL) loadMore
{
    BOOL isShouldLoading = NO;
    PMMyAllOrderViewController *orderVC = (PMMyAllOrderViewController *)self.doubleDelegate;
    switch (self.orderType)
    {
        case PMOrderTypeWaitPay:
            
            isShouldLoading = orderVC.isWaitPayLoading;
            
            break;
            
        case PMOrderTypeWaitGet:
            
            isShouldLoading = orderVC.isWaitGetLoading;
            
            break;
            
        case PMOrderTypeWaitDiscuss:
            
            isShouldLoading = orderVC.isWaitDiscussLoading;
            
            break;
            
        default:
            break;
    }
    
    if (isShouldLoading)
        return NO;
    
    [self willBeginLoadingMore];
    
    switch (self.orderType)
    {
        case PMOrderTypeWaitPay:
            
            orderVC.isWaitPayLoading = YES;
            
            break;
            
        case PMOrderTypeWaitGet:
            
            orderVC.isWaitGetLoading = YES;
            
            break;
            
        case PMOrderTypeWaitDiscuss:
            
            orderVC.isWaitDiscussLoading = YES;
            
            break;
            
        default:
            break;
    }
    
    return YES;
}

- (void)willBeginLoadingMore
{
    PMMyAllOrderViewController *orderVC = (PMMyAllOrderViewController *)self.doubleDelegate;
    ZSTChatroomLoadMoreView *fv;
    
    switch (self.orderType)
    {
        case PMOrderTypeWaitPay:
            
           fv = (ZSTChatroomLoadMoreView *)orderVC.waitPayLoadMoreView;
            
            break;
            
        case PMOrderTypeWaitGet:
            
            fv = (ZSTChatroomLoadMoreView *)orderVC.waitGetLoadMoreView;
            
            break;
            
        case PMOrderTypeWaitDiscuss:
            
            fv = (ZSTChatroomLoadMoreView *)orderVC.waitDiscussLoadMoreView;
            
            break;
            
        default:
            break;
    }
    [fv becomLoading];
    [orderVC getOtherOrderByNet];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 0 && indexPath.row != [self.orderArr[indexPath.section] count] + 1 )
    {
        NSArray *arr = self.orderArr[indexPath.section];
        if ([self.doubleDelegate respondsToSelector:@selector(goToDetailOrder:)])
        {
            [self.doubleDelegate goToDetailOrder:arr];
        }
    }
}

@end
