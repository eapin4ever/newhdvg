//
//  DistributionStyleViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "DistributionStyleViewController.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DistributionStyleViewController () <UIAlertViewDelegate>

@property (strong,nonatomic) UIButton *currentSeletedBtn;
@property (assign,nonatomic) NSInteger defaultIndex;
@property (weak,nonatomic) NSString *seletedStyleUrlString;

@end

@implementation DistributionStyleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"样式选择";
    [self buildNavigationBar];
    [self createDefaultIndex];
    [self buildStyleUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)buildStyleUI
{
    UIScrollView *styleScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    styleScrollView.backgroundColor = [UIColor whiteColor];
    CGFloat sWidth = (WIDTH - 30) / 2;
    CGFloat sHeight = sWidth * 1.5;
    for (NSInteger i = 0; i < self.styleArr.count; i++)
    {
        NSInteger widthIndex = i % 2;
        NSInteger heightIndex = i / 2;
        UIImageView *styleIV = [[UIImageView alloc] initWithFrame:CGRectMake(widthIndex * sWidth + 15 * (widthIndex + 1), heightIndex * sHeight + 10, sWidth - 5, sHeight)];
        styleIV.contentMode = UIViewContentModeScaleAspectFit;
        [styleIV sd_setImageWithURL:[NSURL URLWithString:[self.styleArr[i] objectAtIndex:1]]];
        [styleScrollView addSubview:styleIV];
        UIButton *seletedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        seletedBtn.frame = CGRectMake(styleIV.frame.origin.x - 5, styleIV.frame.origin.y, sWidth, sHeight);
        seletedBtn.tag = i;
        
        UIView *seletedView = [[UIView alloc] initWithFrame:CGRectMake(seletedBtn.bounds.size.width - 20, 0, 20, 20)];
        seletedView.backgroundColor = [UIColor whiteColor];
        UIImageView *seletedIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        seletedView.tag = 10000;
        seletedIV.image = [UIImage imageNamed:@"selectAll_1"];
        [seletedView addSubview:seletedIV];
        [seletedBtn addSubview:seletedView];
        seletedView.hidden = YES;
        
        [seletedBtn addTarget:self action:@selector(showSeleted:) forControlEvents:UIControlEventTouchUpInside];
        [styleScrollView addSubview:seletedBtn];
        if (i == self.defaultIndex)
        {
            self.currentSeletedBtn = seletedBtn;
            self.currentSeletedBtn.layer.borderColor = RGBA(28, 126, 251, 1).CGColor;
            self.currentSeletedBtn.layer.borderWidth = 1;
            seletedView.hidden = NO;
        }
    }
    [self.view addSubview:styleScrollView];
}

- (void)buildNavigationBar
{
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveStyle)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)showSeleted:(UIButton *)sender
{
    if (self.currentSeletedBtn != sender)
    {
        UIView *oldSeletedIV = (UIView *)[self.currentSeletedBtn viewWithTag:10000];
        oldSeletedIV.hidden = YES;
        self.currentSeletedBtn.layer.borderColor = [UIColor clearColor].CGColor;
        self.currentSeletedBtn.layer.borderWidth = 0;
        self.currentSeletedBtn = sender;
        UIView *newsSeletedIV = (UIView *)[self.currentSeletedBtn viewWithTag:10000];
        newsSeletedIV.hidden = NO;
        self.currentSeletedBtn.layer.borderColor = RGBA(28, 126, 251, 1).CGColor;
        self.currentSeletedBtn.layer.borderWidth = 1;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if([_styleDelegate respondsToSelector:@selector(passStyleUrlString:)])
        {
            [_styleDelegate passStyleUrlString:self.seletedStyleUrlString];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)saveStyle
{
    NSInteger index = self.currentSeletedBtn.tag;
    NSString *styleString = [self.styleArr[index] objectAtIndex:2];
    self.seletedStyleUrlString = styleString;
    [[PMNetworking defaultNetworking] request:PMRequestStateUpdateShopHome WithParameters:@{PMSID,@"id":self.shopId,@"shopStyle":styleString} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dic objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
    } showIndicator:YES];
}

- (void)createDefaultIndex
{
    if ([self.styleUrlString isKindOfClass:[NSString class]] && self.styleUrlString && ![self.styleUrlString isEqualToString:@""])
    {
        for (NSInteger i = 0; i < self.styleArr.count; i++)
        {
            NSString *styleNameString = [self.styleArr[i] lastObject];
            if ([self.styleUrlString isEqualToString:styleNameString])
            {
                self.defaultIndex = i;
                break;
            }
        }
    }
    else if ([self.shopDic isKindOfClass:[NSDictionary class]] && self.shopDic && [[self.shopDic objectNullForKey:@"shopStyle"] isKindOfClass:[NSString class]] && ![[self.shopDic objectNullForKey:@"shopStyle"] isEqualToString:@""] )
    {
        NSString *seletedStyleString = [self.shopDic objectNullForKey:@"shopStyle"];
        for (NSInteger i = 0; i < self.styleArr.count; i++)
        {
            NSString *styleNameString = [self.styleArr[i] lastObject];
            if ([seletedStyleString isEqualToString:styleNameString])
            {
                self.defaultIndex = i;
                break;
            }
        }
    }
    else
    {
        self.defaultIndex = 0;
    }
}


@end
