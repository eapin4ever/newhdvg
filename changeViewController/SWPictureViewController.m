//
//  SWPictureViewController.m
//  changeViewController
//
//  Created by P&M on 15/4/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "SWPictureViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMUserInfos.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SWPictureViewController () <UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (strong,nonatomic) UIImageView *firstIma;
@property (strong,nonatomic) UIImageView *secondIma;
@property (assign,nonatomic) BOOL isFirst;
@property (assign,nonatomic) BOOL pictureOne;
@property (assign,nonatomic) BOOL pictureTwo;
@property (assign,nonatomic) BOOL isLoadingOne;
@property (assign,nonatomic) BOOL isLoadingTwo;
@property (strong,nonatomic) UILabel *handLB;
@property (strong,nonatomic) UILabel *faceLB;
@property (assign,nonatomic) NSInteger firstCount;
@property (assign,nonatomic) NSInteger secondCount;
@property (strong,nonatomic) NSTimer *firstTimer;
@property (strong,nonatomic) NSTimer *secondTimer;

@end

@implementation SWPictureViewController
{
    NSString *filePath;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"拍摄照片";
        
        self.view.backgroundColor = RGBA(231, 231, 231, 1); // 设置背景色
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createPictureUI];
    [self createSubmitControlUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *isResult = [self.reSellerDataDict objectNullForKey:@"result"];
    
    // 申请代言不通过
    if ([isResult isEqualToString:@"NO"] && self.pictureOne == NO && self.pictureTwo == NO) {
        
        [self.firstIma sd_setImageWithURL:[self.reSellerDataDict objectNullForKey:@"userCardPhoto"] placeholderImage:[UIImage imageNamed:@"add_userid_picture"]];//手持照
        [self.secondIma sd_setImageWithURL:[self.reSellerDataDict objectNullForKey:@"certifyFile"] placeholderImage:[UIImage imageNamed:@"add_userid_picture"]];//身份证正面照
        
        self.shopCertifyIdStr = [NSString stringWithFormat:@"%@", [self.reSellerDataDict objectNullForKey:@"shopCertifyId"]];
        
        NSString *firstIma = [self.reSellerDataDict objectNullForKey:@"userCardPhoto"];
        NSString *secondIma = [self.reSellerDataDict objectNullForKey:@"certifyFile"];
        
        self.pictureStr1 = [firstIma substringFromIndex:46];
        self.pictureStr2 = [secondIma substringFromIndex:49];
    }
}

- (void)createPictureUI
{
    // 创建第一个 view
    UIView *firstView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(120), (iPhone4s ? HeightRate(50) : HeightRate(100)), WIDTH - WidthRate(240), (iPhone4s ? WidthRate(330) : WidthRate(350)))];
    firstView.backgroundColor = [UIColor whiteColor];
    [firstView.layer setCornerRadius:8.0f];
    [firstView.layer setBorderWidth:0.5f];
    [firstView.layer setBorderColor:[UIColor grayColor].CGColor];
    self.firstView = firstView;
    [self.view addSubview:firstView];
    
    
    UIImageView *firstIV = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, firstView.frame.size.width - 4, firstView.frame.size.height - 4)];
    [firstIV setImage:[UIImage imageNamed:@"add_userid_picture"]];
    firstIV.contentMode = UIViewContentModeScaleAspectFit;
    self.firstIma = firstIV;
    [firstView addSubview:firstIV];
    
    UITapGestureRecognizer *firstTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeFirstImage)];
    [firstIV addGestureRecognizer:firstTap];
    firstIV.userInteractionEnabled = YES;
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(120), firstView.frame.origin.y + firstView.frame.size.height, WIDTH - WidthRate(240), 25)];
    label1.text = @"手持照";
    label1.textColor = GoodsClassifyColor;
    label1.textAlignment = NSTextAlignmentCenter;
    label1.font = [UIFont systemFontOfSize:15.0f];
    self.handLB = label1;
    [self.view addSubview:label1];
    
    
    // 创建第二个 view
    UIView *secondView = [[UIView alloc] initWithFrame:CGRectMake(firstView.frame.origin.x, firstView.frame.origin.y + firstView.frame.size.height + (iPhone4s ? HeightRate(70) : HeightRate(100)), WIDTH - WidthRate(240), (iPhone4s ? WidthRate(330) : WidthRate(350)))];
    secondView.backgroundColor = [UIColor whiteColor];
    [secondView.layer setCornerRadius:8.0f];
    [secondView.layer setBorderWidth:0.5f];
    [secondView.layer setBorderColor:[UIColor grayColor].CGColor];
    self.secondView = secondView;
    [self.view addSubview:secondView];
    
    
    UIImageView *secondIV = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, secondView.frame.size.width - 4, secondView.frame.size.height - 4)];
    [secondIV setImage:[UIImage imageNamed:@"add_userid_picture"]];
    secondIV.contentMode = UIViewContentModeScaleAspectFit;
    self.secondIma = secondIV;
    [secondView addSubview:secondIV];
    
    UITapGestureRecognizer *secondTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeSecondImage)];
    [secondIV addGestureRecognizer:secondTap];
    secondIV.userInteractionEnabled = YES;
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(120), secondView.frame.origin.y + secondView.frame.size.height, WIDTH - WidthRate(240), 25)];
    label2.text = @"身份证正面";
    self.faceLB = label2;
    label2.textColor = GoodsClassifyColor;
    label2.textAlignment = NSTextAlignmentCenter;
    label2.font = [UIFont systemFontOfSize:15.0f];
    [self.view addSubview:label2];
}

// 创建提交 view 控件
- (void)createSubmitControlUI
{
    // 登陆按钮
    UIButton *submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    submitButton.frame = CGRectMake(WidthRate(46), HEIGHT - 64 - 60, WIDTH - WidthRate(46) * 2, 44);
    [submitButton setTitle:@"提交" forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [submitButton addTarget:self action:@selector(submitButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [submitButton setBackgroundColor:ButtonBgColor];
    [submitButton.layer setCornerRadius:6.0];
    [self.view addSubview:submitButton];
}

#pragma mark - 添加第一张照片按钮响应事件
- (void)changeFirstImage
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册中选取", nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

#pragma mark - 添加第二张照片按钮响应事件
- (void)changeSecondImage
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册中选取", nil];
    actionSheet.tag = 2;
    [actionSheet showInView:self.view];
}


#pragma mark - 提交按钮响应事件
- (void)submitButtonClick:(UIButton *)sender
{
    if ((self.pictureOne == NO || self.pictureTwo == NO) && (!self.isLoadingOne || !self.isLoadingTwo)) {
        
        NSString *isResult = [self.reSellerDataDict objectNullForKey:@"result"];
        
        // 申请代言不通过
        if ([isResult isEqualToString:@"NO"]) {
            
            NSString *remark = [self.reSellerDataDict objectNullForKey:@"remark"];
            NSString *message = [NSString stringWithFormat:@"%@，请重新上传", remark];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            [alertView show];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请上传代言认证照片" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            [alertView show];
        }
    }
    else if ((self.pictureOne == NO || self.pictureTwo == NO) && (self.isLoadingOne || self.isLoadingTwo))
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"照片正在上传中，请稍候再试" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
    }
    else {
        
        NSMutableDictionary *userAllDict = [self.userDataDict mutableCopy];//姓名、身份证、手机、区域
        
        [userAllDict setValue:[PMUserInfos shareUserInfo].PM_SID forKey:@"PM_SID"];//用户PM_SID
        [userAllDict setValue:self.shopCertifyIdStr forKey:@"shopCertifyId"];//用户店铺资质表
        [userAllDict setValue:@"" forKey:@"email"];//用户邮箱
        [userAllDict setValue:@"" forKey:@"cityId"];//用户市级id
        [userAllDict setValue:@"" forKey:@"provinceId"];//用户省级id
        [userAllDict setValue:self.pictureStr1 forKey:@"userAndCardPhotoId"];//用户手持身份证照
        [userAllDict setValue:self.pictureStr2 forKey:@"userCardPhotoId"];//用户身份证正面照
        [userAllDict setValue:@"YES" forKey:@"isValidate"];
        
        self.networking = [PMNetworking defaultNetworking];
        
        [self.networking request:PMRequestStateApplyReSeller WithParameters:userAllDict callBackBlock:^(NSDictionary *dict) {
            
            if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"您的代言申请提交成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 100;
                [alertView show];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
        }showIndicator:YES];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
    case 0:  //打开照相机拍照
        {
            if (actionSheet.tag == 1)
            {
                self.isFirst = YES;
            }
            else
            {
                self.isFirst = NO;
            }
            [self takePhoto:actionSheet.tag];
        }
        break;
    case 1:  //打开本地相册
        {
            if (actionSheet.tag == 1)
            {
                self.isFirst = YES;
            }
            else
            {
                self.isFirst = NO;
            }
            [self LocalPhoto:actionSheet.tag];
        }
        break;
    }
}


//开始拍照
-(void)takePhoto:(NSInteger)acIndex
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //设置拍照后的图片可被编辑
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
    }else
    {
        NSLog(@"模拟其中无法打开照相机,请在真机中使用");
    }
}

//打开本地相册
-(void)LocalPhoto:(NSInteger)acIndex
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    //设置选择后的图片可被编辑
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

//当选择一张图片后进入这里
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"])
    {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        //图片压缩，因为原图都是很大的，不必要传原图
        //UIImage *image = [self scaleImage:originImage toScale:0.3];
        
        if (UIImagePNGRepresentation(image) == nil)
        {
            self.userCardPhotoId = UIImageJPEGRepresentation(image, 1.0);
        }
        else
        {
            if (self.isFirst) {
                self.userAndCardPhotoId = UIImagePNGRepresentation(image);
            }
            else{
                self.userCardPhotoId = UIImagePNGRepresentation(image);
            }
        }
        //图片保存的路径
        //这里将图片放在沙盒的documents文件夹中
        NSString *DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        //文件管理器
        NSFileManager *fileManager = [NSFileManager defaultManager];
        //把刚刚图片转换的data对象拷贝至沙盒中 并保存为image.png
        [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
        if (self.isFirst) {
            [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:@"/firstImage.png"] contents:self.userAndCardPhotoId attributes:nil];
            //得到选择后沙盒中图片的完整路径
            filePath = [[NSString alloc]initWithFormat:@"%@%@", DocumentsPath, @"/firstImage.png"];
        }
        else {
            [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:@"/secondImage.png"] contents:self.userCardPhotoId attributes:nil];
            //得到选择后沙盒中图片的完整路径
            filePath = [[NSString alloc]initWithFormat:@"%@%@", DocumentsPath, @"/secondImage.png"];
        }
        
        //关闭相册界面
        [picker dismissViewControllerAnimated:YES completion:^{
            
            //加在视图中
            if (self.isFirst)
            {
                [self.firstIma setImage:image];//手持照
        
                self.firstCount = 0;
                self.firstTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(wordChangeOne:) userInfo:nil repeats:YES];
                self.isLoadingOne = YES;
                
                self.navigationItem.backBarButtonItem.enabled = NO;
                
                // 上传第一张图片
                NSDictionary *param1 = @{PMSID,@"flag":@"userAndCardPhoto"};
                self.networking = [PMNetworking defaultNetworking];
                [self.networking uploadImage:param1 imageData:self.userAndCardPhotoId success:^(NSDictionary *dic) {
                    
                    if ([[dic objectNullForKey:@"success"] integerValue] == 1) {
                        
                        self.pictureStr1 = [dic objectNullForKey:@"messageCode"];
                        self.pictureOne = YES;
                        [self.firstTimer invalidate];
                        self.handLB.text = @"手持照(上传成功)";
                        self.navigationItem.backBarButtonItem.enabled = YES;
                    }
                    else {
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dic objectNullForKey:@"message"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                        [alertView show];
                    }
                }];
            }
            else
            {
                [self.secondIma setImage:image];//身份证正面照
                
                self.secondCount = 0;
                self.secondTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(wordChangeTwo:) userInfo:nil repeats:YES];
                
                self.faceLB.text = @"身份证正面(正在上传中···)";
                
                self.isLoadingTwo = YES;
                
                self.navigationItem.backBarButtonItem.enabled = NO;
//                 上传第二张图片
                NSDictionary *param2 = @{PMSID,@"flag":@"userCardPhoto"};
                self.networking = [PMNetworking defaultNetworking];
                [self.networking uploadImage:param2 imageData:self.userCardPhotoId success:^(NSDictionary *dict){
                    
                    if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
                        
                        self.pictureStr2 = [dict objectNullForKey:@"messageCode"];
                        
                        [self.secondTimer invalidate];
                        self.pictureTwo = YES;
                        self.faceLB.text = @"身份证正面(上传成功)";
                        
                        self.navigationItem.backBarButtonItem.enabled = YES;
                    }
                    else {
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                        [alertView show];
                    }
                }];
            }
            
        }];
        
    }
}

#pragma mark- 缩放图片
- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width * scaleSize, image.size.height * scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height * scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (void)createTipsViewUI
{
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    //[bgView setBackgroundColor:[UIColor blackColor]];
    bgView.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
    self.bgView = bgView;
    [self.view insertSubview:bgView atIndex:10000];
    
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake((WIDTH - 180) / 2, (HEIGHT - 80) / 2 - 64, 180, 120)];
    [grayView setBackgroundColor:RGBA(60, 60, 60, 1)];
    [grayView.layer setCornerRadius:8.0f];
    [bgView addSubview:grayView];
    
    self.activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((grayView.frame.size.width - 30) / 2, 20, 30, 30)];
    [self.activityView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [grayView addSubview:self.activityView];
    
    UILabel *tipsLB = [[UILabel alloc] initWithFrame:CGRectMake(5, 70, 170, 30)];
    tipsLB.text = @"图片正在上传中...";
    tipsLB.textColor = [UIColor whiteColor];
    tipsLB.font = [UIFont systemFontOfSize:16.0f];
    tipsLB.textAlignment = NSTextAlignmentCenter;
    [grayView addSubview:tipsLB];
    
    [self.activityView startAnimating];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
//        NSArray *viewControllers = [self.navigationController viewControllers];
//        UIViewController *controller = [viewControllers objectAtIndex:1];
//        [self.navigationController popToViewController:controller animated:YES];
    }
}

- (void)wordChangeOne:(NSTimer *)timer
{
    if (self.firstCount % 3 == 0)
    {
        self.handLB.text = @"手持照(正在上传中·)";
    }
    else if (self.firstCount % 3 == 1)
    {
        self.handLB.text = @"手持照(正在上传中··)";
    }
    else if (self.firstCount % 3 == 2)
    {
        self.handLB.text = @"手持照(正在上传中···)";
    }
    
    self.firstCount++;
}

- (void)wordChangeTwo:(NSTimer *)timer
{
    if (self.secondCount % 3 == 0)
    {
        self.faceLB.text = @"身份证正面(正在上传中·)";
    }
    else if (self.secondCount % 3 == 1)
    {
        self.faceLB.text = @"身份证正面(正在上传中··)";
    }
    else if (self.secondCount % 3 == 2)
    {
        self.faceLB.text = @"身份证正面(正在上传中···)";
    }
    
    self.secondCount++;
}

//- (void)viewWillDisappear:(BOOL)animated
//{
//    if (self.firstTimer)
//    {
//        [self.firstTimer invalidate];
//    }
//    
//    if (self.secondTimer)
//    {
//        [self.secondTimer invalidate];
//    }
//}


@end
