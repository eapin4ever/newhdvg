//
//  YBQueueUtils.h
//  changeViewController
//
//  Created by ZhangEapin on 15/5/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface YBQueueUtils : NSObject

+ (YBQueueUtils *)defaultQueue;

@property (strong,nonatomic) NSOperationQueue *queue;

@end
