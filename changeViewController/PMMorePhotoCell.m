//
//  PMMorePhotoCell.m
//  changeViewController
//
//  Created by pmit on 15/1/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMMorePhotoCell.h"

@implementation PMMorePhotoCell
{
    UIImageView *_iv;
}
- (void)createUI
{
    if(!_iv)
    {
        _iv = [[UIImageView alloc] init];
        [self.contentView addSubview:_iv];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

- (void)setContentImage:(UIImage *)image height:(CGFloat)height
{
    _iv.frame = CGRectMake(20, 0, WIDTH-40, height);
    
    _iv.image = image;
}


@end
