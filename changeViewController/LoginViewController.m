//
//   .m
//  changeViewController
//
//  Created by P&M on 14/11/22.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "ChangePassViewController.h"
#import "MasterViewController.h"
#import "AppDelegate.h"
#import "MyCenterViewController.h"
#import "PMApplication.h"

@interface LoginViewController ()<UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIView *loginView;

@property (strong, nonatomic) UITextField *accountTF;
@property (strong, nonatomic) UITextField *passwordTF;
@property (strong, nonatomic) UITextField *verifyTF;

@property (strong, nonatomic) UITextField *passwordForSureTF;

@property (strong, nonatomic) UIButton *registerButton;

@property (assign, nonatomic) NSInteger errorCount;

@property (strong, nonatomic) PMNetworking *networking;
@property (strong,nonatomic) UIButton *loginBtn;
@property (strong,nonatomic) NSMutableArray *loginNameArr;


@end

@implementation LoginViewController

static LoginViewController *_loginVC;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"我的";
        
        
    }
    return self;
}

+ (LoginViewController *)shareInstance
{
    if (!_loginVC)
    {
        _loginVC = [[LoginViewController alloc] init];
        _loginVC.loginNameArr = [NSMutableArray array];
    }
    return _loginVC;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"用户登录";
    
    if(!self.isPresented)
    {
        [MasterViewController defaultMasterVC].tabBar.alpha = 1;
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = HDVGPageBGGray;  // 背景色
    [self createUserLoginUI];
    [self createUserLoginControlUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChange) name:UITextFieldTextDidChangeNotification object:self.passwordTF];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChangeAnother) name:UITextFieldTextDidChangeNotification object:self.accountTF];
}

// 创建用户登录 view 输入框
- (void)createUserLoginUI
{
    //暂定是这样初始化，正式版会从服务器请求输入错误的次数
    self.errorCount = 0;
    
    // 初始化一个UIView
    self.loginView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 88)];
    self.loginView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.loginView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.loginView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 43, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.loginView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(0, 88 - 0.5, WIDTH, 0.5);
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.loginView.layer addSublayer:layer3];
    
    
    // 账号输入框
    UIImageView *userIma = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(46), 10, 20, 24)];
    [userIma setImage:[UIImage imageNamed:@"account_image.png"]];
    [self.loginView addSubview:userIma];
    
    self.accountTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(110), 7, WIDTH - WidthRate(150), 30)];
    self.accountTF.borderStyle = UITextBorderStyleNone;
    self.accountTF.delegate = self;
    //self.accountTF.text = @"hdvip";
    self.accountTF.font = [UIFont systemFontOfSize:15.0f];
    self.accountTF.placeholder = @"请输入账号/手机号码/邮箱";
    self.accountTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.accountTF.returnKeyType = UIReturnKeyDone;
    self.accountTF.keyboardType = UIKeyboardTypeDefault;
    self.accountTF.textAlignment = NSTextAlignmentNatural; // 输入框内对齐位置调整
    [self.loginView addSubview:self.accountTF];
    
    // 提示用户输入密码
    UIImageView *passIma = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(46), 54, 20, 24)];
    [passIma setImage:[UIImage imageNamed:@"password_image.png"]];
    [self.loginView addSubview:passIma];
    
    self.passwordTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(110), 51, WIDTH - WidthRate(150), 30)];
    self.passwordTF.clearsOnBeginEditing = YES;
    self.passwordTF.secureTextEntry = YES;  // 使密码输入后变成点点
    self.passwordTF.borderStyle = UITextBorderStyleNone;
    self.passwordTF.delegate = self;
    //self.passwordTF.text = @"a123456";
    self.passwordTF.font = [UIFont systemFontOfSize:15.0f];
    self.passwordTF.placeholder = @"请输入密码";
    self.passwordTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.passwordTF.returnKeyType = UIReturnKeyDone;
    self.passwordTF.keyboardType = UIKeyboardTypeDefault;
    [self.loginView addSubview:self.passwordTF];
}

// 创建用户登录 view 控件
- (void)createUserLoginControlUI
{
    // 登陆按钮
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    loginButton.frame = CGRectMake(WidthRate(46), self.loginView.frame.origin.y + 88 + HeightRate(120), WIDTH - WidthRate(46) * 2, 44);
    [loginButton setTitle:@"登录" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [loginButton addTarget:self action:@selector(loginClick:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *enableImage = [self buttonImageFromColor:ButtonBgColor];
    UIImage *unableImage = [self buttonImageFromColor:RGBA(252, 157, 154, 1)];
    [loginButton setTitleColor:RGBA(232, 232, 232, 1) forState:UIControlStateDisabled];
    [loginButton.layer setCornerRadius:6.0];
    [loginButton setBackgroundImage:enableImage forState:UIControlStateNormal];
    [loginButton setBackgroundImage:unableImage forState:UIControlStateDisabled];
    self.loginBtn = loginButton;
    if ([self isAllIn])
    {
        loginButton.enabled = YES;
    }
    else
    {
        loginButton.enabled = NO;
    }
//    [loginButton.layer setCornerRadius:6.0];
//    [loginButton setBackgroundColor:ButtonBgColor];
    [self.view addSubview:loginButton];
    
    // 忘记密码
    UIButton *forgetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    forgetButton.frame = CGRectMake(WIDTH / 2 - WidthRate(240), loginButton.frame.origin.y + 44 + HeightRate(40), WidthRate(164), HeightRate(48));
    [forgetButton setTitle:@"忘记密码" forState:UIControlStateNormal];
    [forgetButton.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [forgetButton setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [forgetButton addTarget:self action:@selector(forgetPassButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgetButton];
    
    UILabel *lineImage = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 2, loginButton.frame.origin.y + 44 + HeightRate(44), 1, HeightRate(32))];
    lineImage.backgroundColor = HDVGFontColor;
    [self.view addSubview:lineImage];
    
    // 用户注册
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    registerButton.frame = CGRectMake(WIDTH / 2 + WidthRate(76), loginButton.frame.origin.y + 44 + HeightRate(40), WidthRate(132), HeightRate(48));
    [registerButton setTitle:@"新用户" forState:UIControlStateNormal];
    [registerButton.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [registerButton setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [registerButton addTarget:self action:@selector(registerClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerButton];
}

// 按下Done按钮的调用方法，让软键盘隐藏
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.accountTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
}

// 登陆按钮点击事件
- (void)loginClick:(UIButton *)button
{
    [self.accountTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
    
    NSDictionary *parameter = @{@"code":self.accountTF.text,@"password":[MD5Util md5:self.passwordTF.text]};
    
    //向服务器发送登陆请求
    self.networking = [PMNetworking defaultNetworking];
    
    __block NSDictionary *callBackDic;
    
    __block MasterViewController *masterVC = [MasterViewController defaultMasterVC];
    NSLog(@"start -- > %@",[NSDate new]);
    [self.networking requestWithPost:PMRequestStateLogin WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
        
         NSLog(@"end -- > %@",[NSDate new]);
        callBackDic = dic;
        //如果返回的信息中，success的值是true则登陆成功
        if([[callBackDic objectNullForKey:@"success"] boolValue] == 1)
        {
            //将账号密码保存到NSUserDefault，下次打开app时自动登录
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:@{@"code":self.accountTF.text,@"password":[MD5Util md5:self.passwordTF.text]} forKey:@"codeAndPass"];
            
            
            //[(PMApplication *)[UIApplication sharedApplication] resetIdleTimer];
            
            NSDictionary *callBackDataDic = [callBackDic objectNullForKey:@"data"];
            
            
            //将数据保存到 UserInfo单例中
            [PMUserInfos shareUserInfo].userDataDic = [callBackDataDic mutableCopy];
            //保存是否代言商
            [PMUserInfos shareUserInfo].isReseller = [[[callBackDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"isReseller"] integerValue];
            NSString *token = [callBackDataDic objectNullForKey:@"token"];
            
            //9.取子字符串，从第n个开始（参数），取到字符串结尾
            [PMUserInfos shareUserInfo].PM_SID = [token substringFromIndex:7];
            
            //            PMMyCenterViewController *myCenter = [[[masterVC.childViewControllers lastObject] childViewControllers] firstObject];
            MyCenterViewController *myCenter = [[[masterVC.childViewControllers lastObject] childViewControllers] firstObject];
            
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [app bindBaiduyun];
            
            
            //传头像地址
            myCenter.photoUrl = [[callBackDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"photo"];
            
            //如果是present出来的
            if(self.isPresented == YES)
            {
                
                [self dismissViewControllerAnimated:YES completion:^{
                    //如果有登录前的动作，则执行
                    if(masterVC.afterLoginAction)
                    {
                        masterVC.afterLoginAction();
                        //清空动作
                        masterVC.afterLoginAction = nil;
                    }
                }];
            }
        }
        else
        {
            // 用户没有登录成功的提示原因
            showRequestFailAlertView;
        }
        
    } showIndicator:YES];
    
//    [self.networking request:PMRequestStateLogin WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
//        
//        callBackDic = dic;
//        //如果返回的信息中，success的值是true则登陆成功
//        if([[callBackDic objectNullForKey:@"success"] boolValue] == 1)
//        {
//            //将账号密码保存到NSUserDefault，下次打开app时自动登录
//            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//            [ud setObject:@{@"code":self.accountTF.text,@"password":[MD5Util md5:self.passwordTF.text]} forKey:@"codeAndPass"];
//            
//            
//            //[(PMApplication *)[UIApplication sharedApplication] resetIdleTimer];
//            
//            NSDictionary *callBackDataDic = [callBackDic objectNullForKey:@"data"];
//            
//            
//            //将数据保存到 UserInfo单例中
//            [PMUserInfos shareUserInfo].userDataDic = [callBackDataDic mutableCopy];
//            //保存是否代言商
//            [PMUserInfos shareUserInfo].isReseller = [[[callBackDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"isReseller"] integerValue];
//            NSString *token = [callBackDataDic objectNullForKey:@"token"];
//            
//            //9.取子字符串，从第n个开始（参数），取到字符串结尾
//            [PMUserInfos shareUserInfo].PM_SID = [token substringFromIndex:7];
//            
////            PMMyCenterViewController *myCenter = [[[masterVC.childViewControllers lastObject] childViewControllers] firstObject];
//            MyCenterViewController *myCenter = [[[masterVC.childViewControllers lastObject] childViewControllers] firstObject];
//            
//            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//            [app bindBaiduyun];
//            
//            
//            //传头像地址
//            myCenter.photoUrl = [[callBackDataDic objectNullForKey:@"clientUser"] objectNullForKey:@"photo"];
//            
//            //如果是present出来的
//            if(self.isPresented == YES)
//            {
//                
//                [self dismissViewControllerAnimated:YES completion:^{
//                    //如果有登录前的动作，则执行
//                    if(masterVC.afterLoginAction)
//                    {
//                        masterVC.afterLoginAction();
//                        //清空动作
//                        masterVC.afterLoginAction = nil;
//                    }
//                }];
//            }
//        }
//        else
//        {
//            // 用户没有登录成功的提示原因
//            showRequestFailAlertView;
//        }
//    }showIndicator:YES];

}

//注册按钮点击事件
- (void)registerClick:(UIButton *)button
{
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

//忘记密码按钮点击事件
- (void)forgetPassButtonClick:(UIButton *)button
{
    ChangePassViewController *changePassVC = [[ChangePassViewController alloc] init];
    [self.navigationController pushViewController:changePassVC animated:YES];
}

#pragma mark - textField delegate
//输入框代理方法，提示用户输入正确的手机号和密码
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //输入框为空的情况
    if([textField.text isEqualToString:@""] && [textField.subviews count] == 2)
    {
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(textField.bounds.size.width, 0, textField.bounds.size.width, textField.bounds.size.height)];
        lb.textColor = [UIColor redColor];
        lb.text = @" * 不能为空";
        [textField addSubview:lb];
        [self.registerButton setUserInteractionEnabled:NO];
    }
    else if(![textField.text isEqualToString:@""])
    {
        for (id obj in textField.subviews)
        {
            if([obj isKindOfClass:[UILabel class]])
            {
                [obj removeFromSuperview];
                break;
            }
        };
    }
    
    //手机号长度小于11位
    if(textField == self.accountTF && ([self.accountTF.text length] != 11 || ![self.accountTF.text hasPrefix:@"1"]) && [textField.subviews count] == 2)
    {
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(textField.bounds.size.width, 0, textField.bounds.size.width, textField.bounds.size.height)];
        lb.textColor = [UIColor redColor];
        lb.text = @" * 请输入正确的手机号";
        [textField addSubview:lb];
        [self.registerButton setUserInteractionEnabled:NO];
    }
    else if(textField == self.accountTF && ([self.accountTF.text length] == 11 || [self.accountTF.text hasPrefix:@"1"]))
    {
        for (id obj in textField.subviews)
        {
            if([obj isKindOfClass:[UILabel class]])
            {
                [obj removeFromSuperview];
                break;
            }
        };
    }
    
    //如果是输入完确认密码，并且有字符，并且两次输入的密码不相同，并且没有子视图
    if(textField == self.passwordForSureTF && ![self.passwordForSureTF.text isEqualToString: @""] && ![self.passwordForSureTF.text isEqualToString:self.passwordTF.text] && [self.passwordForSureTF.subviews count] == 2)
    {
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(textField.bounds.size.width, 0, textField.bounds.size.width, textField.bounds.size.height)];
        lb.textColor = [UIColor redColor];
        lb.text = @" * 两次输入的密码不相同";
        [textField addSubview:lb];
        [self.registerButton setUserInteractionEnabled:NO];
    }
    else if(textField == self.passwordForSureTF && ![self.passwordForSureTF.text isEqualToString: @""] && [self.passwordForSureTF.text isEqualToString:self.passwordTF.text])
    {
        for (id obj in textField.subviews)
        {
            if([obj isKindOfClass:[UILabel class]])
            {
                [obj removeFromSuperview];
                break;
            }
        };
    }
    
    if([self.passwordTF.subviews count] == 2 && [self.passwordForSureTF.subviews count] == 2 && [self.accountTF.subviews count] == 2)
    {
        [self.registerButton setUserInteractionEnabled:YES];
    }
}

-(void)textChange
{
    if (self.accountTF.text.length > 0)
    {
        self.loginBtn.enabled = self.passwordTF.text.length > 0;
    }
    else
    {
        self.loginBtn.enabled = NO;
    }
}

- (void)textChangeAnother
{
    if (self.passwordTF.text.length > 0)
    {
        self.loginBtn.enabled = self.accountTF.text.length > 0;
    }
    else
    {
        self.loginBtn.enabled = NO;
        ;
    }
}


#pragma mark - 取消登录 消除掉当前页面
- (void)cancelLogin:(UIBarButtonItem *)bbi
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIImage *)buttonImageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, WIDTH, HEIGHT);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (BOOL)isAllIn
{
    BOOL isNoEmpty = NO;
    if (![self.passwordTF.text isEqualToString:@""] && ![self.accountTF.text isEqualToString:@""]) {
        isNoEmpty = YES;
    }
    else
    {
        isNoEmpty = NO;
    }
    
    return isNoEmpty;
}


@end
