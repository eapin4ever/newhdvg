//
//  BankcardViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 * 修改银行卡号控制器
 */

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@protocol BankcardViewControllerDelegate <NSObject>

@end

@interface BankcardViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIView *changeBankcardView;
@property (strong, nonatomic) UITextField *bankTextField;
@property (strong, nonatomic) UITextField *bankcardTextField;
@property (strong, nonatomic) UITextField *mobileTextField;
@property (strong, nonatomic) UITextField *userNameTextField;
@property (strong, nonatomic) UITextField *userIDTextField;
@property (strong,nonatomic) UITextField *bankDetailTF;

@property (strong, nonatomic) UIButton *setBankcardButton;
@property (strong, nonatomic) NSString *bankString;

@property(nonatomic,strong)NSDictionary *bankCardDataDic;

@property (weak, nonatomic) id<BankcardViewControllerDelegate> bankcardDelegate;
@property (assign,nonatomic) BOOL isFromGet;

@end
