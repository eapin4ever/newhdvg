//
//  HomeStyleThreeCell.h
//  changeViewController
//
//  Created by pmit on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeStyleThreeCell : UITableViewCell

@property (strong,nonatomic) UIImageView *upOneIV;
@property (strong,nonatomic) UIImageView *upTwoIV;
@property (strong,nonatomic) UIImageView *upThreeIV;
@property (strong,nonatomic) UIImageView *downLeftIV;
@property (strong,nonatomic) UIImageView *downRightIV;

@property (strong,nonatomic) UILabel *upOneTitleLB;
@property (strong,nonatomic) UILabel *upTwoTitleLB;
@property (strong,nonatomic) UILabel *upThreeTitleLB;
@property (strong,nonatomic) UILabel *downLeftTitleLB;
@property (strong,nonatomic) UILabel *downRightTitleLB;

@property (strong,nonatomic) UILabel *upOnePriceLB;
@property (strong,nonatomic) UILabel *upTwoPriceLB;
@property (strong,nonatomic) UILabel *upThreePriceLB;
@property (strong,nonatomic) UILabel *downLeftPriceLB;
@property (strong,nonatomic) UILabel *downRightPriceLB;

@property (strong,nonatomic) UIView *upOneView;
@property (strong,nonatomic) UIView *upTwoView;
@property (strong,nonatomic) UIView *upThreeView;
@property (strong,nonatomic) UIView *downLeftView;
@property (strong,nonatomic) UIView *downRightView;

- (void)createStyleThreeUI;
- (void)setCellWithUpOneIV:(NSString *)upOneString AndUpTwoIV:(NSString *)upTwoString AndUpThreeIV:(NSString *)upThreeString AndDwonLeftIV:(NSString *)downLeftString AndDwonRightIV:(NSString *)DownRightString AndUpOneTitle:(NSString *)upOneTitle AndUpTwoTitle:(NSString *)upTwoTitle AndUpThreeTitle:(NSString *)upThreeTitle AndDownLeftTitle:(NSString *)downLeftTitle AndDownRightTitle:(NSString *)downRightTitle AndOnePrice:(NSString *)onePrice AndUpTwoPrice:(NSString *)upTwoPrice AndUpThreePrice:(NSString *)upThreePrice AndDwonLeftPrice:(NSString *)downLeftPrice AndDownRightPrice:(NSString *)downRightPrice;


@end
