//
//  PMSearchViewDelegate.h
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PMSerchViewMyDelegate <NSObject>

- (void)searchWithWord:(NSString *)word;
- (void)clearHistoryWord;

@end

@interface PMSearchViewDelegate : NSObject<UITableViewDataSource,UITableViewDelegate>

//搜索框
@property(nonatomic,strong)UITableView *searchView;
@property (weak,nonatomic) id<PMSerchViewMyDelegate> myDelegate;
@property (nonatomic,strong) NSMutableArray *searchHistory;


@end
