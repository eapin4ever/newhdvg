//
//  PMNormalActivityIndicator.h
//  changeViewController
//
//  Created by pmit on 15/1/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMNormalActivityIndicator : UIView
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicatorView;

- (void)stop;
@end
