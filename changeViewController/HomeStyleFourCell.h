//
//  HomeStyleFourCell.h
//  changeViewController
//
//  Created by pmit on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeStyleFourCell : UITableViewCell

@property (strong,nonatomic) UIImageView *upOneIV;
@property (strong,nonatomic) UIImageView *upTwoIV;
@property (strong,nonatomic) UIImageView *upThreeIV;
@property (strong,nonatomic) UIImageView *upFourIV;
@property (strong,nonatomic) UIImageView *upFiveIV;
@property (strong,nonatomic) UIImageView *upSixIV;

@property (strong,nonatomic) UILabel *upOneTitleLB;
@property (strong,nonatomic) UILabel *upTwoTitleLB;
@property (strong,nonatomic) UILabel *upThreeTitleLB;
@property (strong,nonatomic) UILabel *upFourTitleLB;
@property (strong,nonatomic) UILabel *upFiveTitleLB;
@property (strong,nonatomic) UILabel *upSixTitleLB;

@property (strong,nonatomic) UILabel *onePriceLB;
@property (strong,nonatomic) UILabel *twoPriceLB;
@property (strong,nonatomic) UILabel *threePriceLB;
@property (strong,nonatomic) UILabel *fourPriceLB;
@property (strong,nonatomic) UILabel *fivePriceLB;
@property (strong,nonatomic) UILabel *sixPriceLB;

@property (strong,nonatomic) UIView *upOneView;
@property (strong,nonatomic) UIView *upTwoView;
@property (strong,nonatomic) UIView *upThreeView;
@property (strong,nonatomic) UIView *upFourView;
@property (strong,nonatomic) UIView *upFiveView;
@property (strong,nonatomic) UIView *upSixView;

- (void)createUI;
- (void)setCellWithOneIV:(NSString *)oneString AndTwoIV:(NSString *)twoIV AndThreeIV:(NSString *)threeIV AndFourIV:(NSString *)fourIV AndFiveIV:(NSString *)fiveIV AndSixIV:(NSString *)sixIV AndOneTitle:(NSString *)oneTitle AndTwoTitle:(NSString *)twoTitle AndThreeTitle:(NSString *)threeTitle AndFourTitle:(NSString *)fourTitle AndFiveTitle:(NSString *)fiveTitle AndSixTitle:(NSString *)sixTitle AndOnePrice:(NSString *)onePrice AndTwoPrice:(NSString *)twoPrice AndThreePrice:(NSString *)threePrice AndFourPrice:(NSString *)fourPrice AndFivePrice:(NSString *)fivePrice AndSixPrice:(NSString *)sixPrice;
@end
