//
//  SearchResultViewController.m
//  changeViewController
//
//  Created by pmit on 15/7/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "SearchResultViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMGoodsReusableView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PMScreenTableViewCell.h"
#import "PMMyGoodsViewController.h"
#import "tableDelegate.h"
#import "PMScreenDetailsCell.h"

#define originY 40

@interface SearchResultViewController () <UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>

@property(nonatomic,strong)UICollectionView *resultCollectionView;
@property(nonatomic,strong)UICollectionViewFlowLayout *flowLayout;

@property (assign,nonatomic) NSInteger totalPages;
@property (assign,nonatomic) BOOL isLoad;
@property (assign,nonatomic) BOOL isSearch;
@property (assign,nonatomic) BOOL isAscending;
@property (assign,nonatomic) BOOL isClickSell;
@property (assign,nonatomic) BOOL isClickAll;
@property (assign,nonatomic) BOOL hasLoad;
@property (assign,nonatomic) NSInteger currentPage;
@property (assign,nonatomic) NSInteger sellCount;

@property (strong,nonatomic) UIView *noResultView;
@property (strong,nonatomic) NSMutableArray *typeNameArray;//一级类别标题数组
@property (strong,nonatomic) NSMutableArray *brandNameArray;//二级类别品牌标题
@property (strong,nonatomic) NSMutableArray *classNameArray;//二级类别分类标题
@property (strong,nonatomic) NSMutableArray *specNameArray;//二级类别规格标题

@property (strong,nonatomic) UIImageView *priceImage;
@property (strong,nonatomic) UILabel *redLine;
@property (strong,nonatomic) UIView *darkView;
@property (strong,nonatomic) UIView *filterView;
@property (strong,nonatomic) UILabel *secondTitleLB;
@property (strong,nonatomic) tableDelegate *secondDelegate;
@property (strong,nonatomic) UIView *firstView;
@property (strong,nonatomic) UIView *secondView;
@property (strong,nonatomic) NSMutableArray *hasLoadIndexArr;

@end

@implementation SearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentPage = 1;
    self.view.backgroundColor = HDVGPageBGGray;
    
    self.brandNameArray = [NSMutableArray array];
    self.specNameArray = [NSMutableArray array];
    self.classNameArray = [NSMutableArray array];
    self.brandIds = [NSMutableArray array];
    self.specIds = [NSMutableArray array];
    self.brandNameStringArr = [NSMutableArray array];
    self.specDic = [NSMutableDictionary dictionary];
    self.hasLoadIndexArr = [NSMutableArray array];
    self.className = @"";
    self.classIdString = @"";
    self.manager = [AFHTTPRequestOperationManager manager];
    [self.manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [self initSegmentedControl];
    [self buildCollectionView];
    [self createFilter];
    [self searchWithString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _isClickAll = YES;
    _isClickSell = NO;
    
    _sellCount = 0;
}

- (void)initSegmentedControl
{
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:@[@"综合",@"销量",@"价格",@"筛选"]];
    segment.frame = CGRectMake(0, 0, WIDTH, 40);
    segment.selectedSegmentIndex = 0;
    
    [segment addTarget:self action:@selector(switchFilter:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segment];
    
    //添加tap手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [segment addGestureRecognizer:tap];
    
    [segment setTintColor:[UIColor clearColor]];
    //选中时字体颜色
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HDVGRed,NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateSelected];
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor lightGrayColor],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    segment.backgroundColor = RGBA(244, 244, 244, 1);
    
    //分割线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(WIDTH/4, 7.5, 1, 25);
    line1.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
    [segment.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(WIDTH/4*2, 7.5, 1, 25);
    line2.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
    [segment.layer addSublayer:line2];
    
    CALayer *line3 = [[CALayer alloc] init];
    line3.frame = CGRectMake(WIDTH/4*3, 7.5, 1, 25);
    line3.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
    [segment.layer addSublayer:line3];
    
    // 价格走向图标
    UIImageView *priceIma = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH/4*3 - WidthRate(60), 15, 7, 12)];
    [priceIma setImage:[UIImage imageNamed:@"arrow_price"]];
    self.priceImage = priceIma;
    [segment addSubview:priceIma];
    
    // 筛选图标
    UIImageView *screenIma = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(60), 14, 10, 12)];
    [screenIma setImage:[UIImage imageNamed:@"screen_ima"]];
    [segment addSubview:screenIma];
    
    
    //指示条
    self.redLine = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/5/segment.numberOfSegments - 7, 38, WIDTH/5*4/segment.numberOfSegments, 2)];
    self.redLine.backgroundColor = HDVGRed;
    [segment addSubview:self.redLine];
}

- (void)tapAction:(UITapGestureRecognizer *)tap
{
    UISegmentedControl *segment = (UISegmentedControl *)tap.view;
    CGPoint point = [tap locationInView:segment];
    if(point.x < WIDTH/4)
    {
        segment.selectedSegmentIndex = 0;
    }
    else if(point.x > WIDTH/4*2 && point.x < WIDTH/4*3)
    {
        segment.selectedSegmentIndex = 2;
    }
    else if(point.x > WIDTH/4*3)
    {
        segment.selectedSegmentIndex = 3;
    }
    else
    {
        segment.selectedSegmentIndex = 1;
    }
    [self switchFilter:segment];
}

- (void)switchFilter:(UISegmentedControl *)sender
{
    //指示条滑动的动画
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.redLine.frame;
        rect.origin.x = WIDTH/5/sender.numberOfSegments + WIDTH / sender.numberOfSegments * sender.selectedSegmentIndex - 7;
        self.redLine.frame = rect;
    }];
    
    _isSearch = NO;//不是搜索
    _isLoad = NO;//不是加载
    
    NSString *query = @"";
    if(sender.selectedSegmentIndex == 0)//综合排序
    {
        query = @"all";
        _isAscending = YES;
    }
    else if (sender.selectedSegmentIndex == 1)//销量排序
    {
        query = @"sellNum";
        _isAscending = YES;
        _sellCount++;
        if (_sellCount >= 2) {
            _isClickSell = YES;
        }
    }
    else if(sender.selectedSegmentIndex == 2)//价格排序
    {
        CGFloat angle = 0;
        if(_isAscending == YES) {
            query = @"priceDown";
            angle = _isAscending ? M_PI : 0;
            [self.priceImage setTransform:CGAffineTransformMakeRotation(angle)];
        }
        else {
            query = @"priceUp";
            angle = _isAscending ? M_PI : 0;
            [self.priceImage setTransform:CGAffineTransformMakeRotation(angle)];
        }
        _isAscending = !_isAscending;
    }
    else if(sender.selectedSegmentIndex == 3)//筛选
    {
        self.darkView.hidden = NO;
        [UIView animateWithDuration:0.3f animations:^{
           
            self.filterView.frame = CGRectMake(40, 0, WIDTH - 40, HEIGHT);
            
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
    //排序/筛选请求
    if ((sender.selectedSegmentIndex == 0 && _isClickAll == YES) || (sender.selectedSegmentIndex == 1 && _isClickSell == YES)) {
        
        [self filterRequestWithQuery:query isBool:NO];// 不显示刷新动画
    }
    else
    {
        [self filterRequestWithQuery:query isBool:YES];
    }
}

#pragma mark 筛选请求
- (void)filterRequestWithQuery:(NSString *)query isBool:(BOOL)isbool
{
    self.currentPage = 1;
    if([query isKindOfClass:[NSString class]] && query != nil && ![query isEqualToString:@""])
    {
        _isSearch = NO;
        NSString *paramString = @"";
        if ([self.secondClassId isKindOfClass:[NSString class]] && ![self.secondClassId isEqualToString:@""])
        {
            paramString = [NSString stringWithFormat:@"?orderBy=%@&currentPage=%@&key=%@&classId=%@",query,@(self.currentPage),@"",self.secondClassId];
        }
        else
        {
            paramString = [NSString stringWithFormat:@"?orderBy=%@&currentPage=%@&key=%@&classId=%@",query,@(self.currentPage),self.searchString,@""];
        }
        
        if (![self.classIdString isEqualToString:@""])
        {
            paramString = [paramString stringByAppendingString:[NSString stringWithFormat:@"&classId3=%@",self.classIdString]];
        }
        
        for (NSString *brandId in self.brandIds)
        {
            paramString = [paramString stringByAppendingString:[NSString stringWithFormat:@"&brandIds=%@",brandId]];
        }
        
//        for (NSString *specId in self.specIds)
//        {
//            paramString = [paramString stringByAppendingString:[NSString stringWithFormat:@"&specLists=%@",specId]];
//        }
        for (NSDictionary *thisSpecDic in self.specIds)
        {
            NSString *thisKey = [[thisSpecDic allKeys] firstObject];
            NSArray *thisSpecArr = [thisSpecDic objectForKey:thisKey];
            NSString *arrString = [thisSpecArr componentsJoinedByString:@","];
            NSString *thisSpecString = [NSString stringWithFormat:@"%@",arrString];
            paramString = [paramString stringByAppendingString:[NSString stringWithFormat:@"&specLists=%@",thisSpecString]];
        }
        
        
        [self searchWithStringOrderByString:paramString];
//        [self searchWithStringOrderByParam:param isBool:isbool];
    }
}

- (void)buildCollectionView
{
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    [self.flowLayout setMinimumInteritemSpacing:5];
    [self.flowLayout setMinimumLineSpacing:HeightRate(20)];
    [self.flowLayout setItemSize:CGSizeMake(WidthRate(345), WidthRate(490))];
    [self.flowLayout setSectionInset:UIEdgeInsetsMake(0, WidthRate(20), 0, WidthRate(20))];
    
    self.resultCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, originY , WIDTH, HEIGHT - originY - 64) collectionViewLayout:self.flowLayout];
    [self.resultCollectionView setBackgroundColor:HDVGPageBGGray];
    [self.resultCollectionView setDataSource:self];
    [self.resultCollectionView setDelegate:self];
    [self.resultCollectionView registerClass:[PMGoodsReusableView class] forCellWithReuseIdentifier:@"reuseItem"];
    
    [self.resultCollectionView setContentInset:UIEdgeInsetsMake(5, 0, 5, 0)];
    
    [self.view insertSubview:self.resultCollectionView atIndex:0];
    
    UIView *noResultView = [[UIView alloc] initWithFrame:CGRectMake(0, originY, WIDTH, HEIGHT - originY - 64)];
    noResultView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *noResultIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noResultView.frame.size.height)];
    noResultIV.image = [UIImage imageNamed:@"search_null"];
    noResultIV.contentMode = UIViewContentModeScaleAspectFit;
    [noResultView addSubview:noResultIV];
    
    [self.view insertSubview:noResultView atIndex:1];
    self.noResultView = noResultView;
    self.noResultView.hidden = YES;
    
}

- (void)searchWithString
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if (![self.secondClassId isEqualToString:@""])
    {
        params = [@{@"key":@" ",@"currentPage":@(self.currentPage),@"pageSize":@(20),@"classId":self.secondClassId} mutableCopy];
    }
    else
    {
        params = [@{@"key":self.searchString,@"currentPage":@(self.currentPage),@"pageSize":@(20),@"classId":@""} mutableCopy];
    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:params callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            if (self.isLoad)
            {
                [self.dicArray addObjectsFromArray:[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"]];
                [self.resultCollectionView reloadData];
            }
            else if ([[dic objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
            {
                self.brandNameArray = [NSMutableArray array];
                self.specNameArray = [NSMutableArray array];
                self.classNameArray = [NSMutableArray array];
                self.brandIds = [NSMutableArray array];
                self.specIds = [NSMutableArray array];
                self.brandNameStringArr = [NSMutableArray array];
                self.specDic = [NSMutableDictionary dictionary];
                self.className = @"";
                self.classIdString = @"";
                
                self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
                self.totalPages = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"totalPages"] integerValue];
                NSDictionary *allClassDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allClassMap"];
                NSArray *classKeyArr = [allClassDic allKeys];
                for (NSString *classKey in classKeyArr)
                {
                    NSDictionary *oneClassDic = [allClassDic objectNullForKey:classKey];
                    [self.classNameArray addObject:oneClassDic];
                }

                NSDictionary *allBrandDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allBrandMap"];
                NSArray *brandKeyArr = [allBrandDic allKeys];
                for (NSString *brandKey in brandKeyArr)
                {
                    NSDictionary *oneBrandDic = [allBrandDic objectNullForKey:brandKey];
                    [self.brandNameArray addObject:oneBrandDic];
                }
            
                NSDictionary *allSpecDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allSpecMap"];
                NSArray *specKeyArr = [allSpecDic allKeys];
                for (NSString *specKey in specKeyArr)
                {
                    NSDictionary *oneSpecDic = [allSpecDic objectNullForKey:specKey];
                    [self.specNameArray addObject:oneSpecDic];
                }
                
                [self.firstClassTableView reloadData];
                
                if (self.dicArray.count == 0)
                {
                    self.noResultView.hidden = NO;
                }
                else
                {
                    self.noResultView.hidden = YES;
                }

                [self.resultCollectionView reloadData];

            }
        }
        
    } showIndicator:!self.isLoad];
}

- (void)searchWithStringOrderByString:(NSString *)paramString
{
//     NSString *urlString = [NSString stringWithFormat:@"http://192.168.1.26:8123/api/ios/product/search"];
    
    NSString *urlString = [NSString stringWithFormat:@"http://hdvg.me/api/ios/product/search"];
//    NSString *urlString = [NSString stringWithFormat:@"http://120.24.234.76/api/ios/product/search"];
    urlString = [urlString stringByAppendingString:paramString];
    NSString *newsString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.manager GET:newsString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (responseObject)
        {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            if (intSuccess == 1)
            {
                if (self.isLoad)
                {
                    [self.dicArray addObjectsFromArray:[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"]];
                    [self.resultCollectionView reloadData];
                }
                else if ([[dic objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
                {
                    self.brandNameArray = [NSMutableArray array];
                    self.specNameArray = [NSMutableArray array];
                    self.classNameArray = [NSMutableArray array];
                    self.hasLoadIndexArr = [NSMutableArray array];
                    
                    self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
                    self.totalPages = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"totalPages"] integerValue];
                    NSDictionary *allClassDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allClassMap"];
                    NSArray *classKeyArr = [allClassDic allKeys];
                    for (NSString *classKey in classKeyArr)
                    {
                        NSDictionary *oneClassDic = [allClassDic objectNullForKey:classKey];
                        [self.classNameArray addObject:oneClassDic];
                    }
                    
                    NSDictionary *allBrandDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allBrandMap"];
                    NSArray *brandKeyArr = [allBrandDic allKeys];
                    for (NSString *brandKey in brandKeyArr)
                    {
                        NSDictionary *oneBrandDic = [allBrandDic objectNullForKey:brandKey];
                        [self.brandNameArray addObject:oneBrandDic];
                    }
                    
                    NSDictionary *allSpecDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allSpecMap"];
                    NSArray *specKeyArr = [allSpecDic allKeys];
                    for (NSString *specKey in specKeyArr)
                    {
                        NSDictionary *oneSpecDic = [allSpecDic objectNullForKey:specKey];
                        [self.specNameArray addObject:oneSpecDic];
                    }
                    
                    [self.firstClassTableView reloadData];
                    
                    if (self.dicArray.count == 0)
                    {
                        self.noResultView.hidden = NO;
                    }
                    else
                    {
                        self.noResultView.hidden = YES;
                    }
                    
                    [self.resultCollectionView reloadData];
                    
                }
            }
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)searchWithStringOrderByParam:(NSDictionary *)param isBool:(BOOL)isbool
{
    
    [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:param callBackBlock:^(NSDictionary *dic) {
        
        
    } showIndicator:isbool];
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dicArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMGoodsReusableView *item = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseItem" forIndexPath:indexPath];
    
    NSDictionary *dic = self.dicArray[indexPath.row];
    NSString *discount;
    BOOL isHasDiscount = NO;
    // ---------------------------- wei ----------------------------
    // 折扣
    id discountDic = [dic objectNullForKey:@"zkMap"];
    if ([discountDic isKindOfClass:[NSDictionary class]] && !isNull(discountDic))
    {
        discount = [discountDic objectNullForKey:@"zkblShow"];
        if (discount.length != 0) {
            item.showDiscount = YES;
            isHasDiscount = YES;
        }
        else {
            item.showDiscount = NO;
            isHasDiscount = NO;
        }
    }
    else
    {
        item.showDiscount = NO;
        isHasDiscount = NO;
        discount = @"10";
    }
    // ---------------------------- wei ----------------------------
    
    [item createUI];
    
    
    [item.imageView sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    [item setContentTitle:[dic objectNullForKey:@"productName"] price:isHasDiscount ? [[discountDic objectNullForKey:@"discountPrice"] doubleValue] : [[dic objectNullForKey:@"productPrice"] doubleValue] discount:discount];
    
    
    if (indexPath.row == self.dicArray.count - 7)
    {
        if ([self.hasLoadIndexArr containsObject:@(indexPath.row)])
        {
            
        }
        else
        {
            self.isLoad = YES;
            [self.hasLoadIndexArr addObject:@(indexPath.row)];
            self.hasLoad = YES;
            self.currentPage++;
            if (self.currentPage <= self.totalPages)
            {
                [self searchWithString];
            }
            
        }
        
    }
    
    return item;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMMyGoodsViewController *detailVC = [[PMMyGoodsViewController alloc] init];
    detailVC.productId = [self.dicArray[indexPath.row] objectNullForKey:@"id"];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:detailVC];
}

- (void)createFilter
{
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    
    self.darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.darkView.backgroundColor = [UIColor clearColor];
    [window addSubview:self.darkView];
    
    self.darkView.hidden = YES;
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.8;
    [self.darkView addSubview:alphaView];
    
    self.filterView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH - 40, HEIGHT)];
    self.filterView.backgroundColor = HDVGPageBGGray;
    [window addSubview:self.filterView];
    
    UISwipeGestureRecognizer *swiper = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(darkViewDismiss:)];
    [swiper setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.filterView addGestureRecognizer:swiper];

    UIView *firstView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.filterView.bounds.size.width, self.filterView.bounds.size.height)];
    self.firstView = firstView;
    firstView.backgroundColor = HDVGPageBGGray;
    [self.filterView addSubview:firstView];
    
    UIView *firstHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, firstView.bounds.size.width, 30)];
    UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissBtn.frame = CGRectMake(13, 0, 40, 30);
    [dismissBtn setTitle:@"收起" forState:UIControlStateNormal];
    [dismissBtn addTarget:self action:@selector(dismissFilterView:) forControlEvents:UIControlEventTouchUpInside];
    dismissBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [dismissBtn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [firstHeaderView addSubview:dismissBtn];
    [firstView addSubview:firstHeaderView];
    
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(53, 0, firstHeaderView.bounds.size.width - 108, 30)];
    titleLB.font = [UIFont systemFontOfSize:17.0f];
    titleLB.textColor = HDVGFontColor;
    titleLB.text = @"筛选";
    titleLB.textAlignment = NSTextAlignmentCenter;
    [firstHeaderView addSubview:titleLB];
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(firstHeaderView.bounds.size.width - 55, 0, 40, 30);
    [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveFirst:) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [saveBtn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [firstHeaderView addSubview:saveBtn];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = [UIColor lightGrayColor].CGColor;
    line.frame = CGRectMake(10, 29.5, firstView.bounds.size.width - 10, 0.5);
    [firstHeaderView.layer addSublayer:line];
    
    
    self.firstClassTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, firstView.bounds.size.width, firstView.bounds.size.height - 60) style:UITableViewStylePlain];
    self.firstClassTableView.backgroundColor = HDVGPageBGGray;
    self.firstClassTableView.delegate = self;
    self.firstClassTableView.dataSource = self;
    [self.firstClassTableView registerClass:[PMScreenTableViewCell class] forCellReuseIdentifier:@"screenCell"];
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, WIDTH - 40, 40)];
    self.firstClassTableView.tableFooterView = footView;
    
    UIButton *clearHistoryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearHistoryBtn setTitle:@"清除记录" forState:UIControlStateNormal];
    [clearHistoryBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [clearHistoryBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    clearHistoryBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    clearHistoryBtn.layer.cornerRadius = 8.0;
    clearHistoryBtn.frame = CGRectMake(0, 0, WidthRate(240), 30);
    clearHistoryBtn.center = footView.center;
    clearHistoryBtn.layer.borderWidth = 1.5;
    clearHistoryBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [clearHistoryBtn addTarget:self action:@selector(clickToClearSearchHistory:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:clearHistoryBtn];
    
    footView.backgroundColor = [UIColor clearColor];
    
    [firstView addSubview:self.firstClassTableView];
    
    self.secondDelegate = [[tableDelegate alloc] init];
    self.secondDelegate.searchVC = self;
    
    UIView *secondView = [[UIView alloc] initWithFrame:CGRectMake(self.filterView.bounds.size.width, 0, self.filterView.bounds.size.width, self.filterView.bounds.size.height)];
    self.secondView = secondView;
    
    UIView *secondHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, secondView.bounds.size.width, 30)];
    
    UIButton *secondDismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    secondDismissBtn.frame = CGRectMake(13, 0, 40, 30);
    [secondDismissBtn setTitle:@"返回" forState:UIControlStateNormal];
    [secondDismissBtn addTarget:self action:@selector(dismissSecondView:) forControlEvents:UIControlEventTouchUpInside];
    secondDismissBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [secondDismissBtn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [secondHeaderView addSubview:secondDismissBtn];
    
    self.secondTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(53, 0, secondHeaderView.bounds.size.width - 108, 30)];
    self.secondTitleLB.font = [UIFont systemFontOfSize:17.0f];
    self.secondTitleLB.textColor = HDVGFontColor;
    self.secondTitleLB.textAlignment = NSTextAlignmentCenter;
    [secondHeaderView addSubview:self.secondTitleLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(secondHeaderView.bounds.size.width - 55, 0, 40, 30);
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureSecond:) forControlEvents:UIControlEventTouchUpInside];
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [sureBtn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [secondHeaderView addSubview:sureBtn];
    
    self.secondClassTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, secondView.bounds.size.width, secondView.bounds.size.height - 60) style:UITableViewStylePlain];
    self.secondClassTableView.backgroundColor = HDVGPageBGGray;
    self.secondClassTableView.delegate = self.secondDelegate;
    self.secondClassTableView.dataSource = self.secondDelegate;
    [self.secondClassTableView registerClass:[PMScreenDetailsCell class] forCellReuseIdentifier:@"detailsCell"];
    [secondView addSubview:secondHeaderView];
    [secondView addSubview:self.secondClassTableView];
    [self.filterView addSubview:secondView];
    
    CALayer *secondLine = [CALayer layer];
    secondLine.backgroundColor = [UIColor lightGrayColor].CGColor;
    secondLine.frame = CGRectMake(10, 29.5, firstView.bounds.size.width - 10, 0.5);
    [secondHeaderView.layer addSublayer:line];
    
    UISwipeGestureRecognizer *secondSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(secondDismiss:)];
    [secondSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.secondView addGestureRecognizer:secondSwipe];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger hasInteger = 0;
    if (self.classNameArray.count > 0)
    {
        hasInteger += 1;
    }
    
    if (self.brandNameArray.count > 0)
    {
        hasInteger += 1;
    }
    
    return hasInteger + self.specNameArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMScreenTableViewCell *screenCell = [tableView dequeueReusableCellWithIdentifier:@"screenCell"];
    [screenCell createScreenUI];
    screenCell.backgroundColor = HDVGPageBGGray;
    
    // 取出数据
    if (indexPath.row == 0)
    {
        [screenCell setScreenTitle:@"分类"];
        if (self.className)
        {
            screenCell.tipLB.text = self.className;
        }
        
    }
    else if (indexPath.row == 1)
    {
        [screenCell setScreenTitle:@"品牌"];
        if (self.brandNameStringArr)
        {
            NSString *brandName = [self.brandNameStringArr componentsJoinedByString:@","];
            screenCell.tipLB.text = brandName;
        }
    }
    else
    {
        NSLog(@"specNameArr --> %@",self.specNameArray);
        NSDictionary *oneSpecDic = self.specNameArray[indexPath.row - 2];
        NSString *specName = [oneSpecDic objectNullForKey:@"name"];
        NSString *keyName = [NSString stringWithFormat:@"规格 -- %@",specName];
        NSArray *specArr = [self.specDic objectNullForKey:keyName];
        NSString *thisSpecString = [specArr componentsJoinedByString:@","];
        screenCell.tipLB.text = thisSpecString;
        
        [screenCell setScreenTitle:specName];
    }
    
    return screenCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = @"";
    
    if (indexPath.row == 0)
    {
        title = @"分类";
        self.secondDelegate.titleArray = self.classNameArray;
    }
    else if (indexPath.row == 1)
    {
        title = @"品牌";
        self.secondDelegate.titleArray = self.brandNameArray;
    }
    else
    {
        title = [NSString stringWithFormat:@"规格 -- %@",[self.specNameArray[indexPath.row - 2] objectNullForKey:@"name"]];
        NSArray *specValueList = [self.specNameArray[indexPath.row - 2] objectNullForKey:@"specValueList"];
        self.secondDelegate.titleArray = specValueList;
        NSString *specTitleId = [self.specNameArray[indexPath.row - 2] objectNullForKey:@"id"];
        self.secondDelegate.titleId = specTitleId;
    }
    
    self.secondTitleLB.text = title;
    self.secondDelegate.titleType = title;
    self.secondDelegate.searchKey = self.searchString;
    [self.secondClassTableView reloadData];
    
    
    [UIView animateWithDuration:0.3f animations:^{
        self.firstView.frame = CGRectMake(self.filterView.bounds.size.width, 0, self.firstView.bounds.size.width, self.firstView.bounds.size.height);
        self.secondView.frame = CGRectMake(0, 0, self.secondView.bounds.size.width, self.secondView.bounds.size.height);
    }];
}

- (void)darkViewDismiss:(UISwipeGestureRecognizer *)swipe
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.filterView.frame = CGRectMake(WIDTH, 0, WIDTH - 40, HEIGHT);
        
    } completion:^(BOOL finished) {
//        self.navigationController.navigationBarHidden = NO;
//        self.resultCollectionView.frame = CGRectMake(0, originY, WIDTH, HEIGHT - originY - 64);
        self.darkView.hidden = YES;
        
    }];
}

- (void)dismissFilterView:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
    
        self.filterView.frame = CGRectMake(WIDTH, 0, WIDTH - 40, HEIGHT);
        
    } completion:^(BOOL finished) {
//        self.navigationController.navigationBarHidden = NO;
//        self.resultCollectionView.frame = CGRectMake(0, originY, WIDTH, HEIGHT - originY - 64);
        self.darkView.hidden = YES;
        
    }];
}

- (void)secondDismiss:(UISwipeGestureRecognizer *)swiper
{
    [UIView animateWithDuration:0.3f animations:^{
        self.firstView.frame = CGRectMake(0, 0, self.firstView.bounds.size.width, self.firstView.bounds.size.height);
        self.secondView.frame = CGRectMake(self.filterView.bounds.size.width, 0, self.secondView.bounds.size.width, self.secondView.bounds.size.height);
    }];
}

- (void)dismissSecondView:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        self.firstView.frame = CGRectMake(0, 0, self.firstView.bounds.size.width, self.firstView.bounds.size.height);
        self.secondView.frame = CGRectMake(self.filterView.bounds.size.width, 0, self.secondView.bounds.size.width, self.secondView.bounds.size.height);
    }];
}

- (void)sureSecond:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.firstView.frame = CGRectMake(0, 0, self.firstView.bounds.size.width, self.firstView.bounds.size.height);
        self.secondView.frame = CGRectMake(self.filterView.bounds.size.width, 0, self.secondView.bounds.size.width, self.secondView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        if ([self.secondDelegate.titleType isEqualToString:@"分类"])
        {
            [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:@{@"key":self.searchString,@"currentPage":@(1),@"pageSize":@(20),@"classId3":self.classIdString} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    NSDictionary *dataDic = [dic objectNullForKey:@"data"];
                    NSDictionary *allBrandMap = [dataDic objectNullForKey:@"allBrandMap"];
                    NSDictionary *allSpecMap = [dataDic objectNullForKey:@"allSpecMap"];
                    
                    NSMutableArray *brandArr = [NSMutableArray array];
                    NSArray *brandKeyArr = [allBrandMap allKeys];
                    for (NSString *brandKey in brandKeyArr)
                    {
                        NSDictionary *oneBrandDic = [allBrandMap objectNullForKey:brandKey];
                        [brandArr addObject:oneBrandDic];
                    }
                    
                    NSMutableArray *specArr = [NSMutableArray array];
                    NSArray *specKeyArr = [allSpecMap allKeys];
                    for (NSString *specKey in specKeyArr)
                    {
                        NSDictionary *oneSpecDic = [allSpecMap objectNullForKey:specKey];
                        [specArr addObject:oneSpecDic];
                    }
                    
                    self.brandNameArray = [brandArr mutableCopy];
                    self.specNameArray = [specArr mutableCopy];
                    self.brandIds = [NSMutableArray array];
                    self.specIds = [NSMutableArray array];
                    self.brandNameStringArr = [NSMutableArray array];
                    [self.firstClassTableView reloadData];
                }
            } showIndicator:NO];
        }
        else if ([self.secondDelegate.titleType isEqualToString:@"品牌"])
        {
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:1 inSection:0];
            [self.firstClassTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        else
        {
            [self.firstClassTableView reloadData];
        }
    }];
}

- (void)saveFirst:(UIButton *)sender
{
//    NSString *urlString = [NSString stringWithFormat:@"http://192.168.1.26:8123/api/ios/product/search"];
    NSString *urlString = [NSString stringWithFormat:@"http://hdvg.me/api/ios/product/search"];
//    NSString *urlString = [NSString stringWithFormat:@"http://120.24.234.76/api/ios/product/search"];
    NSString *params = [NSString stringWithFormat:@"?key=%@&currentPage=%@&pageSize=%@",self.searchString,@(1),@(20)];
    if (![self.classIdString isEqualToString:@""])
    {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&classId3=%@",self.classIdString]];
    }
    
    for (NSString *brandId in self.brandIds)
    {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&brandIds=%@",brandId]];
    }
    
    for (NSDictionary *thisSpecDic in self.specIds)
    {
        NSString *thisKey = [[thisSpecDic allKeys] firstObject];
        NSArray *thisSpecArr = [thisSpecDic objectForKey:thisKey];
        NSString *arrString = [thisSpecArr componentsJoinedByString:@","];
        NSString *thisSpecString = [NSString stringWithFormat:@"%@",arrString];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&specLists=%@",thisSpecString]];
    }
    urlString = [urlString stringByAppendingString:params];
    NSString *newsUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    self.darkView.hidden = YES;
    
    [self.manager GET:newsUrlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (responseObject)
        {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            if (intSuccess == 1)
            {
                self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
                [self.resultCollectionView reloadData];
                
                if (self.dicArray.count == 0)
                {
                    self.noResultView.hidden = NO;
                }
                else
                {
                    self.noResultView.hidden = YES;
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error --> %@",[error description]);
    }];

    
    [UIView animateWithDuration:0.3f animations:^{
        self.filterView.frame = CGRectMake(WIDTH, 0, WIDTH - 40, HEIGHT);
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)clickToClearSearchHistory:(UIButton *)sender
{
    [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:@{@"key":self.searchString,@"pageSize":@(20),@"currentPage":@(1)} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            self.brandNameArray = [NSMutableArray array];
            self.specNameArray = [NSMutableArray array];
            self.classNameArray = [NSMutableArray array];
            self.brandIds = [NSMutableArray array];
            self.specIds = [NSMutableArray array];
            self.brandNameStringArr = [NSMutableArray array];
            self.specDic = [NSMutableDictionary dictionary];
            self.className = @"";
            self.classIdString = @"";
            //是不是加载请求
            if(self.isLoad)
            {
                [self.dicArray addObjectsFromArray:[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"]];
            }
            else if([[dic objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
            {
                self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
                self.totalPages = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"totalPages"] integerValue];
                NSDictionary *allClassDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allClassMap"];
                NSArray *classKeyArr = [allClassDic allKeys];
                NSLog(@"allClass --> %@",allClassDic);
                for (NSString *classKey in classKeyArr)
                {
                    NSDictionary *oneClassDic = [allClassDic objectNullForKey:classKey];
                    [self.classNameArray addObject:oneClassDic];
                }
                
                NSDictionary *allBrandDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allBrandMap"];
                NSArray *brandKeyArr = [allBrandDic allKeys];
                for (NSString *brandKey in brandKeyArr)
                {
                    NSDictionary *oneBrandDic = [allBrandDic objectNullForKey:brandKey];
                    [self.brandNameArray addObject:oneBrandDic];
                }
                
                NSDictionary *allSpecDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allSpecMap"];
                NSArray *specKeyArr = [allSpecDic allKeys];
                for (NSString *specKey in specKeyArr)
                {
                    NSDictionary *oneSpecDic = [allSpecDic objectNullForKey:specKey];
                    [self.specNameArray addObject:oneSpecDic];
                }
                
                [self.firstClassTableView reloadData];
            }
            
            [self.resultCollectionView reloadData];
            
            if (self.dicArray.count == 0)
            {
                self.noResultView.hidden = NO;
            }
            else
            {
                self.noResultView.hidden = YES;
            }
        }
        else
        {
            
        }
    }showIndicator:NO];//YES为显示刷新动画，NO不显示
}

@end
