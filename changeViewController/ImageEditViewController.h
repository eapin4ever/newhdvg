//
//  ImageEditViewController.h
//  changeViewController
//
//  Created by pmit on 15/7/1.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageEditViewControllerDelegate <NSObject>

- (void)passImageData:(NSData *)imageData;

@end

@interface ImageEditViewController : UIViewController

@property (weak,nonatomic) id<ImageEditViewControllerDelegate> editDelegate;

- (instancetype)initWithOriginalImage:(UIImage *)image;

@end
