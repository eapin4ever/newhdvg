//
//  NearSearchViewController.h
//  changeViewController
//
//  Created by pmit on 15/7/30.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearSearchViewController : UIViewController

@property (assign,nonatomic) BOOL isRefresh;
@property (assign,nonatomic) BOOL isLoad;

@end
