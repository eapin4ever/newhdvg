//
//  PMAppraiseVC.m
//  changeViewController
//
//  Created by pmit on 14/12/15.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMAppraiseVC.h"
#import "PMToastHint.h"
#import "JHRefresh.h"

@interface PMAppraiseVC ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray *titleArray;
@property(nonatomic,strong)UILabel *redLine;

@property(nonatomic,strong)UITableView *appraiseDetailTV;

@property(nonatomic,strong)NSArray *currentAppraiseArray;
@property(nonatomic,strong)NSMutableDictionary *appraiseCount;
@property (copy,nonatomic) NSString *productId;
@property (assign,nonatomic) NSInteger currentPage;
@property (assign,nonatomic) NSInteger totalPages;
@property (assign,nonatomic) BOOL isLoading;
@property (strong,nonatomic) NSMutableArray *goodArr;
@property (strong,nonatomic) NSMutableArray *midArr;
@property (strong,nonatomic) NSMutableArray *badArr;
@property (strong,nonatomic) NSArray *gmbArr;

@property (strong,nonatomic) UIView *noDataView;

@end

static NSString *const normalCell = @"Cell";
@implementation PMAppraiseVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"商品评价";
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isLoading = NO;
    
    
    
    [self initAppraiseDetailTableView];
    
    [self createGoodsEvaluateNoData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initSegmentItemsTitle];
    
    [self initSegmentedControl];
}

- (void)initSegmentItemsTitle
{
    NSString *title1;
    NSString *title2;
    NSString *title3;
    if (self.gmbArr.count == 0)
    {
        title1 = [NSString stringWithFormat:@"好评(%@)",@"0"];
        title2 = [NSString stringWithFormat:@"中评(%@)",@"0"];
        title3 = [NSString stringWithFormat:@"差评(%@)",@"0"];
    }
    else
    {
        title1 = [NSString stringWithFormat:@"好评(%@)",self.gmbArr[0]];
        title2 = [NSString stringWithFormat:@"中评(%@)",self.gmbArr[1]];
        title3 = [NSString stringWithFormat:@"差评(%@)",self.gmbArr[2]];
//        title1 = [NSString stringWithFormat:@"好评(%@)",@"0"];
//        title2 = [NSString stringWithFormat:@"中评(%@)",@"0"];
//        title3 = [NSString stringWithFormat:@"差评(%@)",@"0"];
    }
    
    self.titleArray = @[title1,title2,title3];
}

- (void)initSegmentedControl
{
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:self.titleArray];
    segment.frame = CGRectMake(0, 0, WIDTH, HeightRate(80));
    segment.selectedSegmentIndex = 0;
    
    [segment addTarget:self action:@selector(switchAppraise:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segment];
    
    [segment setTintColor:[UIColor clearColor]];
    //选中时字体颜色
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HDVGRed,NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:HeightRate(32)], NSFontAttributeName, nil] forState:UIControlStateSelected];
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor lightGrayColor],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:HeightRate(32)], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    segment.backgroundColor = RGBA(244, 244, 244, 1);
    
    //分割线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(WIDTH/3, HeightRate(15), 1, HeightRate(50));
    line1.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
    [segment.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(WIDTH/3*2, HeightRate(15), 1, HeightRate(50));
    line2.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
    [segment.layer addSublayer:line2];
   
    //指示条
    self.redLine = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/5/segment.numberOfSegments, HeightRate(75), WIDTH/5*3/segment.numberOfSegments, HeightRate(5))];
    self.redLine.backgroundColor = HDVGRed;
    [segment addSubview:self.redLine];
}


//综合、附近、价格筛选按钮
- (void)switchAppraise:(UISegmentedControl *)sender
{
    //指示条滑动的动画
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.redLine.frame;
        rect.origin.x = WIDTH/5/sender.numberOfSegments + WIDTH/sender.numberOfSegments*sender.selectedSegmentIndex;
        self.redLine.frame = rect;
    }];
    
    NSString *key;
    
    if (sender.selectedSegmentIndex == 0)
    {
        key = @"good";
    }
    else if(sender.selectedSegmentIndex == 1)
    {
        key = @"mid";
    }
    else
    {
        key = @"bad";
    }
    
    self.currentAppraiseArray = [self.appraiseCount objectNullForKey:key];
    [self.appraiseDetailTV reloadData];
}

- (void)initAppraiseDetailTableView
{
    self.appraiseDetailTV = [[UITableView alloc] initWithFrame:CGRectMake(0, HeightRate(80), WIDTH, HEIGHT - 64 - HeightRate(80)) style:UITableViewStylePlain];
    self.appraiseDetailTV.delegate = self;
    self.appraiseDetailTV.dataSource = self;
    [self.appraiseDetailTV registerClass:[PMAppraiseTVCell class] forCellReuseIdentifier:normalCell];
    self.appraiseDetailTV.rowHeight = HeightRate(240);
//    self.appraiseDetailTV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.appraiseDetailTV];
    
    self.appraiseDetailTV.tableFooterView = [[UIView alloc] init];
    
    [self addRefreshAction];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.currentAppraiseArray.count == 0)
        self.noDataView.hidden = NO;
    else
        self.noDataView.hidden = YES;
    
    return self.currentAppraiseArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *notiDic = self.currentAppraiseArray[indexPath.row];
    NSString *contentzStr = notiDic[@"remark"];
    
    UIFont *font1 = [UIFont systemFontOfSize:HeightRate(30)];
    NSDictionary *sizeDict1 = [NSDictionary dictionaryWithObjectsAndKeys:font1, NSFontAttributeName, nil];
    
    CGSize sizeText1 = [contentzStr boundingRectWithSize:CGSizeMake(WIDTH - 20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:sizeDict1 context:nil].size;
    
    return sizeText1.height + 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMAppraiseTVCell *cell = [tableView dequeueReusableCellWithIdentifier:normalCell];
    
    NSDictionary *dic = self.currentAppraiseArray[indexPath.row];
    
    NSString *remarkString = [dic objectNullForKey:@"remark"];
    cell.remarkDict = @{@"remark":remarkString};
    cell.isBigVC = NO;
    [cell createUI];
    [cell setContentUserName:[NSString stringWithFormat:@"%@",[dic objectNullForKey:@"level"]] detail:([[dic objectNullForKey:@"remark"] isKindOfClass:[NSNull class]] ||[[dic objectNullForKey:@"remark"] isEqualToString:@""])?@"这家伙好懒，什么评论也没写。":[dic objectNullForKey:@"remark"] time:[dic objectNullForKey:@"createDt"]];
    
    return cell;
}


#pragma mark - 获取数据
- (void)getModelsWithProductId:(NSString *)productID finishAction:(finishAction)finishAction
{
    /*
     PM_SID         Y	String		Token
     currentPage	Y	Int         当前页
     productId      Y	String		商品id
     */
    self.productId = productID;
    if (!self.isLoading)
    {
        self.currentPage = 1;
    }
    NSDictionary *param = @{PMSID,@"currentPage":@(self.currentPage),@"productId":productID,@"pageSize":@(20)};
    [[PMNetworking defaultNetworking] request:PMRequestStateShopShowShopDisucss WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            self.noDataView.hidden = YES;
            
            if (!self.isLoading) {
                self.goodArr = [NSMutableArray array];
                self.midArr = [NSMutableArray array];
                self.badArr = [NSMutableArray array];
            }
            
            NSString *messageCode = [dic objectNullForKey:@"messageCode"];
            self.gmbArr = [messageCode componentsSeparatedByString:@","];
    
            self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
            if (self.currentPage <= self.totalPages)
            {
                for (NSDictionary *dict in [[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"])
                {
                    switch ([[dict objectNullForKey:@"level"] integerValue])
                    {
                        case 5://5星，好评
                        case 4:
                            [self.goodArr addObject:dict];
                            break;
                        case 2://1星，差评
                        case 1:
                            [self.badArr addObject:dict];
                            break;
                        default://其他，中评
                            [self.midArr addObject:dict];
                            break;
                    }
                }
            }
            
            self.appraiseCount = [@{@"good":self.goodArr,@"mid":self.midArr,@"bad":self.badArr} mutableCopy];
            //默认显示好评
            if (!self.isLoading)
            {
                self.currentAppraiseArray = self.goodArr;
            }
            
            
            if(finishAction)
                finishAction();
            [self.appraiseDetailTV reloadData];
        }
        else
        {
//            showRequestFailAlertView;
            //[[PMToastHint defaultToastWithRight:NO] showHintToView:self.goodsVC.view ByToast:@"这个商品暂时没有评价哦"];
            self.noDataView.hidden = NO;
        }
    }showIndicator:NO];
}

- (void)addRefreshAction
{
    __weak PMAppraiseVC *weakSelf = self;
    [self.appraiseDetailTV addRefreshFooterViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        
        weakSelf.isLoading = YES;
        if (weakSelf.currentPage < weakSelf.totalPages)
        {
            weakSelf.currentPage += 1;
            [weakSelf getModelsWithProductId:weakSelf.productId finishAction:^{
                [weakSelf.appraiseDetailTV reloadData];
                //加载刷新
                [weakSelf.appraiseDetailTV footerEndRefreshing];
            }];
        }
        
        [weakSelf.appraiseDetailTV footerEndRefreshing];
    }];
}


#pragma mark - 创建商品评价没有数据的界面
- (void)createGoodsEvaluateNoData
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(80), WIDTH, HEIGHT - 64 - HeightRate(80))];
    noDataView.backgroundColor = [UIColor whiteColor];
    self.noDataView = noDataView;
    
    UIImageView *noDataIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noDataView.frame.size.height)];
    noDataIV.image = [UIImage imageNamed:@"evaluate_null"];
    noDataIV.contentMode = UIViewContentModeScaleAspectFit;
    [noDataView addSubview:noDataIV];
    
    [self.view insertSubview:noDataView atIndex:1000];
    self.noDataView.hidden = YES;
}


@end
