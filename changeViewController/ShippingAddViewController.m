//
//  ShippingAddViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "ShippingAddViewController.h"
#import "PMToastHint.h"
#import "PMPublicClass.h"
#import "AddressPickerViewController.h"

@interface ShippingAddViewController () <AddressPickerViewControllerDelegate>

@property (strong,nonatomic) UIButton *defaultBtn;

@end

@implementation ShippingAddViewController
@synthesize areaValue = _areaValue;
@synthesize locatePicker = _locatePicker;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"新增地址";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)setAreaValue:(NSString *)areaValue
{
    if (![_areaValue isEqualToString:areaValue]) {
        
        self.regionTextField.text = areaValue;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createShippingAddUI];
    [self cancelLocatePicker];
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveAndUseButtonClick:)];
    
    [saveButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR, NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = saveButton;
}

- (void)createShippingAddUI
{
    // 创建收货地址 view
    UIView *shippingAddView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 160)];
    shippingAddView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:shippingAddView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [shippingAddView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 40, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [shippingAddView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(WidthRate(26), 80, WIDTH, HeightRate(1));
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [shippingAddView.layer addSublayer:layer3];
    
    CALayer *layer4 = [[CALayer alloc] init];
    layer4.frame = CGRectMake(WidthRate(26), 120, WIDTH, HeightRate(1));
    layer4.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [shippingAddView.layer addSublayer:layer4];
    
    CALayer *layer5 = [[CALayer alloc] init];
    layer5.frame = CGRectMake(0, 160 - 0.5, WIDTH, 0.5);
    layer5.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [shippingAddView.layer addSublayer:layer5];
//    
//    CALayer *layer6 = [[CALayer alloc] init];
//    layer6.frame = CGRectMake(0, 200, WIDTH, HeightRate(1));
//    layer6.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
//    [shippingAddView.layer addSublayer:layer6];
    
    
    // 收货人姓名
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 5, WidthRate(150), 30)];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = @"收  货  人:";
    nameLabel.textColor = HDVGFontColor;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [UIFont systemFontOfSize:14.0f];
    [shippingAddView addSubview:nameLabel];
    
    self.nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(190), 5, WidthRate(530), 30)];
    self.nameTextField.borderStyle = UITextBorderStyleNone;
    self.nameTextField.delegate = self;
    self.nameTextField.tag = 1;
    self.nameTextField.textColor = RGBA(112, 112, 112, 1);
    self.nameTextField.textAlignment = NSTextAlignmentLeft;
    self.nameTextField.font = [UIFont systemFontOfSize:15.0f];
    self.nameTextField.placeholder = @"请输入姓名";
    self.nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.nameTextField.returnKeyType = UIReturnKeyDone;
    self.nameTextField.keyboardType = UIReturnKeyDefault;
    [shippingAddView addSubview:self.nameTextField];
    
    
    // 手机号码
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 45, WidthRate(150), 30)];
    mobileLabel.backgroundColor = [UIColor clearColor];
    mobileLabel.text = @"手机号码:";
    mobileLabel.textColor = HDVGFontColor;
    mobileLabel.textAlignment = NSTextAlignmentCenter;
    mobileLabel.font = [UIFont systemFontOfSize:14.0f];
    [shippingAddView addSubview:mobileLabel];
    
    self.mobileTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(190), 45, WidthRate(530), 30)];
    self.mobileTextField.borderStyle = UITextBorderStyleNone;
    self.mobileTextField.delegate = self;
    self.mobileTextField.tag = 2;
    self.mobileTextField.textColor = RGBA(112, 112, 112, 1);
    self.mobileTextField.textAlignment = NSTextAlignmentLeft;
    self.mobileTextField.font = [UIFont systemFontOfSize:15.0f];
    self.mobileTextField.placeholder = @"请输入手机号码";
    self.mobileTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.mobileTextField.returnKeyType = UIReturnKeyDone;
    self.mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    [shippingAddView addSubview:self.mobileTextField];
    
    
//    // 固定电话
//    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 85, WidthRate(150), 30)];
//    phoneLabel.backgroundColor = [UIColor clearColor];
//    phoneLabel.text = @"固定电话:";
//    phoneLabel.textColor = HDVGFontColor;
//    phoneLabel.textAlignment = NSTextAlignmentCenter;
//    phoneLabel.font = [UIFont systemFontOfSize:14.0f];
//    [shippingAddView addSubview:phoneLabel];
    
//    self.phoneTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(190), 85, WidthRate(400), 30)];
//    self.phoneTextField.borderStyle = UITextBorderStyleNone;
//    self.phoneTextField.delegate = self;
//    self.phoneTextField.textColor = RGBA(112, 112, 112, 1);
//    self.phoneTextField.textAlignment = NSTextAlignmentLeft;
//    self.phoneTextField.font = [UIFont systemFontOfSize:15.0f];
//    self.phoneTextField.placeholder = @"固定电话（选填）";
//    self.phoneTextField.returnKeyType = UIReturnKeyDone;
//    self.phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
//    [shippingAddView addSubview:self.phoneTextField];
    
    // 所在地区
    UILabel *regionLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 85, WidthRate(150), 30)];
    regionLabel.backgroundColor = [UIColor clearColor];
    regionLabel.text = @"所在地区:";
    regionLabel.textColor = HDVGFontColor;
    regionLabel.textAlignment = NSTextAlignmentCenter;
    regionLabel.font = [UIFont systemFontOfSize:14.0f];
    [shippingAddView addSubview:regionLabel];
    
    self.regionTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(190), 85, WidthRate(400), 30)];
    self.regionTextField.borderStyle = UITextBorderStyleNone;
    self.regionTextField.delegate = self;
    self.regionTextField.textColor = RGBA(112, 112, 112, 1);
    self.regionTextField.textAlignment = NSTextAlignmentLeft;
    self.regionTextField.font = [UIFont systemFontOfSize:15.0f];
    self.regionTextField.placeholder = @"请选择地区";
    self.regionTextField.returnKeyType = UIReturnKeyDone;
    self.regionTextField.keyboardType = UIReturnKeyDefault;
    [shippingAddView addSubview:self.regionTextField];
    
    
    // 详细地址
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 125, WidthRate(150), 30)];
    addressLabel.backgroundColor = [UIColor clearColor];
    addressLabel.text = @"详细地址:";
    addressLabel.textColor = HDVGFontColor;
    addressLabel.textAlignment = NSTextAlignmentCenter;
    addressLabel.font = [UIFont systemFontOfSize:14.0f];
    [shippingAddView addSubview:addressLabel];
    
    self.addDetailTextField =[[UITextField alloc] initWithFrame:CGRectMake(WidthRate(190), 125, WidthRate(530), 30)];
    self.addDetailTextField.borderStyle = UITextBorderStyleNone;
    self.addDetailTextField.delegate = self;
    self.addDetailTextField.tag = 3;
    self.addDetailTextField.textColor = RGBA(112, 112, 112, 1);
    self.addDetailTextField.textAlignment = NSTextAlignmentLeft;
    self.addDetailTextField.font = [UIFont systemFontOfSize:15.0f];
    self.addDetailTextField.placeholder = @"请输入详细地址";
    self.addDetailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.addDetailTextField.returnKeyType = UIReturnKeyDone;
    self.addDetailTextField.keyboardType = UIReturnKeyDefault;
    [shippingAddView addSubview:self.addDetailTextField];
    
    // 设为默认地址
    UIButton *defaultBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    defaultBtn.frame = CGRectMake(WIDTH - WidthRate(270), shippingAddView.frame.origin.y + 160 + HeightRate(10), WidthRate(240), 18);
    [defaultBtn setImage:[UIImage imageNamed:@"showpass_yes.png"] forState:UIControlStateNormal];
    [defaultBtn setImage:[UIImage imageNamed:@"showpass_no.png"] forState:UIControlStateSelected];
    [defaultBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [defaultBtn.titleLabel setFont:[UIFont systemFontOfSize:HeightRate(30)]];
    [defaultBtn setTitle:@"设为默认地址" forState:UIControlStateNormal];
    [defaultBtn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [defaultBtn addTarget:self action:@selector(setDefaultAddress:) forControlEvents:UIControlEventTouchUpInside];
    self.defaultBtn = defaultBtn;
    [self.view addSubview:defaultBtn];
}

- (void)setDefaultAddress:(UIButton *)sender
{
    sender.selected = !sender.isSelected;
}

// 按下Done按钮的调用方法，让软键盘隐藏
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    [self.nameTextField resignFirstResponder];
    [self.mobileTextField resignFirstResponder];
    [self.phoneTextField resignFirstResponder];
    [self.regionTextField resignFirstResponder];
    [self.addDetailTextField resignFirstResponder];
    
    [self cancelLocatePicker];
}

#pragma mark - HZAreaPicker delegate
- (void)pickerDidChaneStatus:(HZAreaPickerView *)picker
{
    self.areaValue = [NSString stringWithFormat:@"%@%@%@", picker.locate.state, picker.locate.city, picker.locate.district];
    //获取地区ID
    self.areaID = picker.locate.areaId;
    
    //省、市、区的ID
//    self.provinceID = [PMGetAreaID getAreaIDByArea:[NSString stringWithFormat:@"%@", picker.locate.state]];
//    self.cityID = [PMGetAreaID getAreaIDByArea:[NSString stringWithFormat:@"%@", picker.locate.city]];
//    self.districtID = [PMGetAreaID getAreaIDByArea:[NSString stringWithFormat:@"%@", picker.locate.district]];
    self.provinceID = picker.locate.provinceId;
    self.cityID = picker.locate.cityId;
    self.districtID = picker.locate.areasId;
}

- (void)cancelLocatePicker
{
    [self.locatePicker cancelPicker];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.nameTextField.tag == 1 && textField == self.nameTextField && textField.text.length - range.length + string.length > 12) {
        return NO;
    }
    
    if (self.mobileTextField.tag == 2 && textField == self.mobileTextField && textField.text.length - range.length + string.length > 11) {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // 如果详细地址输入框被键盘遮挡了，就向上移动50个像素
    if (self.addDetailTextField.tag == 3 && textField == self.addDetailTextField) {
        // 输入框监听事件
        [textField addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventEditingDidBegin];
        [textField addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    }
    
    if ([textField isEqual:self.regionTextField]) {
        [self.nameTextField resignFirstResponder];
        [self.mobileTextField resignFirstResponder];
        [self.phoneTextField resignFirstResponder];
        [self.regionTextField resignFirstResponder];
        [self.addDetailTextField resignFirstResponder];
//        if(!self.locatePicker)
//            self.locatePicker = [[HZAreaPickerView alloc] initWithStyle:HZAreaPickerWithStateAndCityAndDistrict delegate:self];
//        [self.locatePicker showInView:self.view];
//        if ([self.regionTextField.text isEqualToString:@""])
//        {
//            self.regionTextField.text = @"北京市市辖区东城区";
//            self.provinceID = @"110000";
//            self.cityID = @"110100";
//            self.areaID = @"110101";
//        }
        
        AddressPickerViewController *addPickerVC = [[AddressPickerViewController alloc] init];
        addPickerVC.addressDelegate = self;
        [self.navigationController pushViewController:addPickerVC animated:YES];
        
        return NO;
    }
    else {
        [self cancelLocatePicker];
        return YES;
    }
}

// 开始编辑时，整体上移
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.addDetailTextField.tag == 3 && textField == self.addDetailTextField) {
        [self moveView:(iPhone4s ? -50 : 0)];
    }
}
// 结束编辑时，整体下移
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.addDetailTextField.tag == 3 && textField == self.addDetailTextField) {
        [self moveView:(iPhone4s ? 50 : 0)];
    }
}

- (void)moveView:(CGFloat)move
{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y += move;//view的y轴上移
    self.view.frame = frame;
    [UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}


#pragma mark - 保存并使用按钮响应事件
- (void)saveAndUseButtonClick:(id)sender
{
    [self.nameTextField resignFirstResponder];
    [self.mobileTextField resignFirstResponder];
    [self.regionTextField resignFirstResponder];
    [self.addDetailTextField resignFirstResponder];
    
    // 判断是否按要求编辑收货地址
    if (self.nameTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入收货人姓名"];
        return;
    }
    if (self.mobileTextField.text.length == 0 || ![PMPublicClass checkMobile:self.mobileTextField.text]) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的手机号"];
        return;
    }
    if (self.regionTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请选择收货地址区域"];
        return;
    }
    if (self.addDetailTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请填写收货人详细地址"];
        return;
    }
    
    // 从服务器获取用户 token
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    
    if (self.nameTextField.text.length != 0 && self.mobileTextField.text.length != 0 && self.regionTextField.text.length != 0 && self.addDetailTextField.text.length != 0 && [PMPublicClass checkMobile:self.mobileTextField.text]) {
        
        /**
         * 用户收货地址填写
         * 参数: seesionId:(string)会话ID
         */
        NSInteger isDefault = 1;
        if (self.defaultBtn.selected)
        {
            isDefault = -1;
        }
        
        NSString *districtIds = @"";
        if (!self.districtID || [self.districtID isEqualToString:@""])
        {
            districtIds = @"null";
        }
        else
        {
            districtIds = self.districtID;
        }
        
        NSDictionary *getUserInfo = @{@"PM_SID":userInfos.PM_SID, @"name":self.nameTextField.text, @"mobile":self.mobileTextField.text, @"areaId":self.areaID, @"newProvince":self.provinceID, @"newCity":self.cityID, @"newArea":districtIds, @"address":self.addDetailTextField.text,@"isDefault":@(isDefault)};
        self.networking = [PMNetworking defaultNetworking];
        
        __block NSDictionary *callBackDict;
        
        [self.networking request:PMRequestStateMyAddAddress WithParameters:getUserInfo callBackBlock:^(NSDictionary *dict) {
            
            callBackDict = dict;
            if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"保存并使用地址成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 1;
                [alertView show];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }showIndicator:NO];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)finishPick:(NSString *)provinceId CityId:(NSString *)cityId AreaId:(NSString *)areaId ProvinceValue:(NSString *)provinceName CityName:(NSString *)cityName AreaName:(NSString *)areaName
{
    self.areaValue = [NSString stringWithFormat:@"%@%@%@",provinceName,cityName,areaName];
    self.provinceID = provinceId;
    self.cityID = cityId;
    if (areaId && ![areaId isEqualToString:@""])
    {
        self.areaID = areaId;
        self.districtID = areaId;
    }
    else if (cityId && ![cityId isEqualToString:@""])
    {
        self.areaID = cityId;
        self.districtID = cityId;
    }
    else
    {
        self.areaID = provinceId;
        self.districtID = provinceId;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
