//
//  SecondViewController.h
//  changeViewController
//
//  Created by EapinZhang on 15/4/1.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (copy,nonatomic) NSString *urlString;
@property (copy,nonatomic) NSString *webTitle;
@property (copy,nonatomic) NSString *webType;

@end
