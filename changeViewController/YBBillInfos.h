//
//  YBBillInfos.h
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YBBillInfos : NSObject

@property (copy,nonatomic) NSString *billNum;
@property (assign,nonatomic) NSInteger billType;
@property (assign,nonatomic) NSInteger billContent;

+ (instancetype)shareInstance;
- (void)initWithBillNum:(NSString *)billNum andType:(NSInteger)type andBillContent:(NSInteger)billContent;

@end
