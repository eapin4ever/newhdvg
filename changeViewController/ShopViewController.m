//
//  ShopViewController.m
//  changeViewController
//
//  Created by EapinZhang on 15/3/26.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "ShopViewController.h"
#import "PMMyPhoneInfo.h"
#import "MasterViewController.h"
#import "PMMyGoodsViewController.h"

@interface ShopViewController () <UIWebViewDelegate>
{
    NSString *_shopId;
    NSString *_productId;
}

@property (strong,nonatomic) UIWebView *webVC;

@end

@implementation ShopViewController


- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = @"店铺首页";
    
    [self buildWebView];
}

- (void)buildWebView
{
    UIWebView *webVC = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    NSURL *url = [NSURL URLWithString:self.loadAddress];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    webVC.delegate = self;
    self.webVC = webVC;
    [webVC loadRequest:request];
    [self.view addSubview:webVC];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = request.URL;
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    if ([urlString rangeOfString:@"shopId"].length > 0 && [urlString rangeOfString:@"details"].length <= 0)
    {
        NSInteger index = [urlString rangeOfString:@"shopId"].location + @"shopId=".length;
        _shopId = [urlString substringFromIndex:index];
        return YES;
    }
    else if ([urlString rangeOfString:@"productId"].length>0)
    {
        NSInteger index = [urlString rangeOfString:@"productId"].location + @"productId=".length;
        NSInteger shopIndex = [urlString rangeOfString:@"&shopId="].location;
        NSRange range = NSMakeRange(index, (shopIndex - index));
        _productId = [urlString substringWithRange:range];
        PMMyGoodsViewController *detailVC = [[PMMyGoodsViewController alloc] init];
        detailVC.productId = _productId;
        [self.navigationController pushViewControllerWithNavigationControllerTransition:detailVC];
        return NO;
    }
    
    return YES;
}

@end
