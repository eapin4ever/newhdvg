//
//  PMProductCellTwo.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define CELLHI 425.0/2

#import "PMProductCellTwo.h"
#import "PMProductBtn.h"

@implementation PMProductCellTwo
{
    PMProductBtn *_btn1;
    PMProductBtn *_btn2;
    PMProductBtn *_btn3;
}

- (void)createUI
{
    if(!_btn1)
    {
        CGFloat btnY = arc4random()%2==0 ? 0:CELLHI/2;
        
        _btn1 = [[PMProductBtn alloc] initWithStyle:PMGridStyleBig frame:CGRectMake(WIDTH/2, 0, WIDTH/2, CELLHI)];
        _btn2 = [[PMProductBtn alloc] initWithStyle:PMGridStyleSmallImageOnLeft frame:CGRectMake(0, btnY, WIDTH/2, CELLHI/2)];
        _btn3 = [[PMProductBtn alloc] initWithStyle:PMGridStyleSmallImageOnRight frame:CGRectMake(0, btnY==0?CELLHI/2:0, WIDTH/2, CELLHI/2)];
        
        
        self.iv1 = _btn1.productIV;
        self.iv2 = _btn2.productIV;
        self.iv3 = _btn3.productIV;
    
        [self.contentView addSubview:_btn1];
        [self.contentView addSubview:_btn2];
        [self.contentView addSubview:_btn3];
    
        CALayer *line1 = [CALayer layer];
        line1.frame = CGRectMake(WIDTH/2, 30, 1, CELLHI - 60);
        line1.backgroundColor = HDVGPageBGGray.CGColor;
        [self.contentView.layer addSublayer:line1];
        
        CALayer *line2 = [CALayer layer];
        line2.frame = CGRectMake(30, CELLHI/2, WIDTH/2 - 30, 1);
        line2.backgroundColor = HDVGPageBGGray.CGColor;
        [self.contentView.layer addSublayer:line2];
    }
}

- (void)setProductOnetitle1:(NSString *)title1 price1:(double)price1 title2:(NSString *)title2 price2:(double)price2 title3:(NSString *)title3 price3:(double)price3
{
    _btn1.titleLB.text = title1;
    NSString *text = @"";
    if (price1 != 0)
    {
        text = [NSString stringWithFormat:@"￥%.2lf",price1];
    }
    
    _btn1.priceLB.text = text;
    
    _btn2.titleLB.text = title2;
    NSString *text2 = @"";
    if (price2 != 0)
    {
        text2 = [NSString stringWithFormat:@"￥%.2lf",price2];
    }
    _btn2.priceLB.text = text2;
    
    _btn3.titleLB.text = title3;
    NSString *text3 = @"";
    if (price3 != 0)
    {
        text3 = [NSString stringWithFormat:@"￥%.2lf",price3];
    }
    _btn3.priceLB.text = text3;
}

- (void)addTarget:(id)target selector:(SEL)sel row:(NSInteger)row
{
    _btn1.tag = row*100 + 0;
    _btn2.tag = row*100 + 1;
    _btn3.tag = row*100 + 2;
    
    [_btn1 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [_btn2 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [_btn3 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    
}

@end
