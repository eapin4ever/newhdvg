//
//  PMGoodPostCell.h
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMGoodPostCell : UITableViewCell

@property (strong,nonatomic) UILabel *postLB;

- (void)createUI;
- (void)setCellData:(NSString *)globelPostString Pro:(NSString *)globelProString;

@end
