//
//  FeedbackViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/19.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMUserInfos.h"
#import "PMNetworking.h"

@interface FeedbackViewController : UIViewController <UITextViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UITextView *feedView;
@property (strong, nonatomic) UILabel *tipsLabel;

@property (strong, nonatomic) PMNetworking *networking;

@end
