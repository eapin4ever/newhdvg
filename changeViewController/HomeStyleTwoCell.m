//
//  HomeStyleTwoCell.m
//  changeViewController
//
//  Created by pmit on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "HomeStyleTwoCell.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define kCellWidth (WIDTH - 20)

@implementation HomeStyleTwoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.upBigIV)
    {
        UIView *upBigView = [self buildStyleOneSecondWithFrame:CGRectMake(5, 5, kCellWidth * 0.4 - 10, kCellWidth * 0.5) isMiddle:YES AndIsUp:NO AndStation:@""];
        [self.contentView addSubview:upBigView];
        self.upBigView = upBigView;
        
        UIView *upOneView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.4, 5, kCellWidth * 0.2, kCellWidth * 0.24) isMiddle:NO AndIsUp:YES AndStation:@"one"];
        [self.contentView addSubview:upOneView];
        self.upOneView = upOneView;
        
        UIView *upTwoView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.6 + 7, 5, kCellWidth * 0.2, kCellWidth * 0.24) isMiddle:NO AndIsUp:YES AndStation:@"two"];
        [self.contentView addSubview:upTwoView];
        self.upTwoView = upTwoView;
        
        UIView *upThreeView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.8 + 12, 5, kCellWidth * 0.2, kCellWidth * 0.24) isMiddle:NO AndIsUp:YES AndStation:@"three"];
        [self.contentView addSubview:upThreeView];
        self.upThreeView = upThreeView;
        
        UIView *upFourView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.4, kCellWidth * 0.24 + 11, kCellWidth * 0.2, kCellWidth * 0.24) isMiddle:NO AndIsUp:YES AndStation:@"four"];
        [self.contentView addSubview:upFourView];
        self.upFourView = upFourView;
        
        UIView *upFiveView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.6 + 7, kCellWidth * 0.24 + 11, kCellWidth * 0.2, kCellWidth * 0.24) isMiddle:NO AndIsUp:YES AndStation:@"five"];
        [self.contentView addSubview:upFiveView];
        self.upFiveView = upFiveView;
        
        UIView *upSixView = [self buildStyleOneSecondWithFrame:CGRectMake(kCellWidth * 0.8 + 12, kCellWidth * 0.24 + 11, kCellWidth * 0.2, kCellWidth * 0.24) isMiddle:NO AndIsUp:YES AndStation:@"six"];
        [self.contentView addSubview:upSixView];
        self.upSixView = upSixView;
        
        UIView *donwLeftView = [self buildStyleOneThreeWithFrame:CGRectMake(5, kCellWidth * 0.5 + 10, (kCellWidth + 5) * 0.5, kCellWidth * 0.25) AndDirect:NO];
        [self.contentView addSubview:donwLeftView];
        self.downLeftView = donwLeftView;
        
        UIView *downRightView = [self buildStyleOneThreeWithFrame:CGRectMake((kCellWidth + 5) * 0.5 + 10, kCellWidth * 0.5 + 10, (kCellWidth + 5) * 0.5, kCellWidth * 0.25) AndDirect:YES];
        [self.contentView addSubview:downRightView];
        self.downRightView = downRightView;
    }
}

- (UIView *)buildStyleOneSecondWithFrame:(CGRect)frame isMiddle:(BOOL)isMiddle AndIsUp:(BOOL)isUp AndStation:(NSString *)station
{
    UIView *bgWhiteView = [[UIView alloc] initWithFrame:frame];
    bgWhiteView.backgroundColor = [UIColor whiteColor];
   
    if (isMiddle)
    {
        UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, bgWhiteView.bounds.size.width, bgWhiteView.bounds.size.width)];
        [bgWhiteView addSubview:productIV];
        self.upBigIV = productIV;
        self.upBigTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(2, productIV.frame.origin.y + productIV.frame.size.height + (kCellWidth * 0.015), bgWhiteView.bounds.size.width - 4, 20)];
        self.upBigTitleLB.lineBreakMode = NSLineBreakByClipping;
        self.upBigTitleLB.textAlignment = NSTextAlignmentRight;
        self.upBigTitleLB.textColor = kTextColor;
        self.upBigTitleLB.font = [UIFont systemFontOfSize:WidthRate(24)];
        [bgWhiteView addSubview:self.upBigTitleLB];
        self.upBigPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(2, self.upBigTitleLB.frame.origin.y + 20, bgWhiteView.bounds.size.width - 4, 20)];
        self.upBigPriceLB.textColor = kTextColor;
        self.upBigPriceLB.font = [UIFont boldSystemFontOfSize:13.0f];
        self.upBigPriceLB.textAlignment = NSTextAlignmentRight;
        [bgWhiteView addSubview:self.upBigPriceLB];
    }
    else
    {
        bgWhiteView.backgroundColor = [UIColor whiteColor];
        UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, bgWhiteView.bounds.size.width, bgWhiteView.bounds.size.width)];
        UIView *shadeView = [[UIView alloc] initWithFrame:CGRectMake(0, bgWhiteView.bounds.size.height * 0.8, bgWhiteView.bounds.size.width, bgWhiteView.bounds.size.height * 0.2)];
        shadeView.backgroundColor = kShadeColor;
        UILabel *smallTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, shadeView.bounds.size.width, shadeView.bounds.size.height)];
        smallTitleLB.lineBreakMode = NSLineBreakByClipping;
        smallTitleLB.textColor = kTextColor;
        smallTitleLB.textAlignment = NSTextAlignmentCenter;
        smallTitleLB.font = [UIFont systemFontOfSize:WidthRate(18)];
        [shadeView addSubview:smallTitleLB];
        [bgWhiteView addSubview:productIV];
        [bgWhiteView addSubview:shadeView];
        if ([station isEqualToString:@"one"])
        {
            self.upOneTitleLB = smallTitleLB;
            self.upOneIV = productIV;
        }
        else if ([station isEqualToString:@"two"])
        {
            self.upTwoTitleLB = smallTitleLB;
            self.upTwoIV = productIV;
        }
        else if ([station isEqualToString:@"three"])
        {
            self.upThreeIV = productIV;
            self.upThreeTitleLB = smallTitleLB;
        }
        else if ([station isEqualToString:@"four"])
        {
            self.upFourTitleLB = smallTitleLB;
            self.upFourIV = productIV;
        }
        else if ([station isEqualToString:@"five"])
        {
            self.upFiveTitleLB = smallTitleLB;
            self.upFiveIV = productIV;
        }
        else
        {
            self.upSixTitleLB = smallTitleLB;
            self.upSixIV = productIV;
        }
    }

    return bgWhiteView;
}

- (UIView *)buildStyleOneThreeWithFrame:(CGRect)frame AndDirect:(BOOL)isRight
{
    UIView *bgWhiteView = [[UIView alloc] initWithFrame:frame];
    bgWhiteView.backgroundColor = [UIColor whiteColor];
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(5, bgWhiteView.bounds.size.height * 0.3 - 5, bgWhiteView.bounds.size.width * 0.5 - 10, 20)];
    titleLB.font = [UIFont systemFontOfSize:WidthRate(24)];
    titleLB.lineBreakMode = NSLineBreakByClipping;
    titleLB.textColor = kTextColor;
    [bgWhiteView addSubview:titleLB];
    UILabel *priceLB = [[UILabel alloc] initWithFrame:CGRectMake(5, titleLB.frame.origin.y + 20, bgWhiteView.bounds.size.width * 0.5 - 10, 20)];
    priceLB.font = [UIFont boldSystemFontOfSize:13.0f];
    priceLB.textColor = kTextColor;
    [bgWhiteView addSubview:priceLB];
    UIImageView *productIV = [[UIImageView alloc] initWithFrame:CGRectMake(bgWhiteView.bounds.size.width * 0.5 + 5, 0, bgWhiteView.bounds.size.width * 0.5 - 5, bgWhiteView.bounds.size.height)];
    [bgWhiteView addSubview:productIV];
    
    if (isRight)
    {
        self.downRightIV = productIV;
        self.downRightPriceLB = priceLB;
        self.downRightTitleLB = titleLB;
        self.downRightTitleLB.numberOfLines = 2;
    }
    else
    {
        self.downLeftIV = productIV;
        self.downLeftPriceLB = priceLB;
        self.downLeftTitleLB = titleLB;
        self.downLeftTitleLB.numberOfLines = 2;
    }
    
    return bgWhiteView;
}

- (void)setCellWithUpBigIV:(NSString *)bigString AndOneIV:(NSString *)oneString AndTwoIV:(NSString *)twoString AndThreeIV:(NSString *)threeString AndFourIV:(NSString *)fourString AndFiveIV:(NSString *)fiveString AndSixIV:(NSString *)sixString AndDownLeftIV:(NSString *)downLeftString AndDownRightIV:(NSString *)downRightString AndBigTitle:(NSString *)bigTitle AndOneTitle:(NSString *)oneTitle AndTwoTitle:(NSString *)twoTitle AndThreeTitle:(NSString *)threeTitle AndFoutTitle:(NSString *)fourTitle AndFiveTitle:(NSString *)fiveTitle AndSixTitle:(NSString *)sixTitle AndLeftTitle:(NSString *)leftTitle AndRightTitle:(NSString *)rightTitle AndBigPrice:(NSString *)bigPrice AndDownLeftPrice:(NSString *)downLeftPrice AndDownRightPrice:(NSString *)downRightPrice
{
    [self.upBigIV sd_setImageWithURL:[NSURL URLWithString:bigString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upOneIV sd_setImageWithURL:[NSURL URLWithString:oneString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upTwoIV sd_setImageWithURL:[NSURL URLWithString:twoString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upThreeIV sd_setImageWithURL:[NSURL URLWithString:threeString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upFourIV sd_setImageWithURL:[NSURL URLWithString:fourString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upFiveIV sd_setImageWithURL:[NSURL URLWithString:fiveString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.upSixIV sd_setImageWithURL:[NSURL URLWithString:sixString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.downLeftIV sd_setImageWithURL:[NSURL URLWithString:downLeftString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    [self.downRightIV sd_setImageWithURL:[NSURL URLWithString:downRightString] placeholderImage:[UIImage imageNamed:@"loading_image"] options:SDWebImageLowPriority | SDWebImageProgressiveDownload | SDWebImageContinueInBackground];
    
    if (oneTitle.length > 7)
    {
        oneTitle = [oneTitle substringWithRange:NSMakeRange(0, 7)];
    }
    
    if (twoTitle.length > 7)
    {
        twoTitle = [twoTitle substringWithRange:NSMakeRange(0, 7)];
    }
    
    if (threeTitle.length > 7)
    {
        threeTitle = [threeTitle substringWithRange:NSMakeRange(0, 7)];
    }
    
    if (fourTitle.length > 7)
    {
        fourTitle = [fourTitle substringWithRange:NSMakeRange(0, 7)];
    }
    
    if (fiveTitle.length > 7)
    {
        fiveTitle = [fiveTitle substringWithRange:NSMakeRange(0, 7)];
    }
    
    if (sixTitle.length > 7)
    {
        sixTitle = [sixTitle substringWithRange:NSMakeRange(0, 7)];
    }
    
    if (bigTitle.length > 13)
    {
        bigTitle = [bigTitle substringWithRange:NSMakeRange(0, 13)];
    }
    
    if (leftTitle.length > 6) {
        leftTitle = [leftTitle substringWithRange:NSMakeRange(0, 6)];
    }
    
    if (rightTitle.length > 6) {
        rightTitle = [rightTitle substringWithRange:NSMakeRange(0, 6)];
    }
    
    self.upBigTitleLB.text = bigTitle;
    self.upOneTitleLB.text = oneTitle;
    self.upTwoTitleLB.text = twoTitle;
    self.upThreeTitleLB.text = threeTitle;
    self.upFourTitleLB.text = fourTitle;
    self.upFiveTitleLB.text = fiveTitle;
    self.upSixTitleLB.text = sixTitle;
    self.downLeftTitleLB.text = leftTitle;
    self.downRightTitleLB.text = rightTitle;
    
    self.upBigPriceLB.text = bigPrice;
    self.downLeftPriceLB.text = downLeftPrice;
    self.downRightPriceLB.text = downRightPrice;
    
}

@end
