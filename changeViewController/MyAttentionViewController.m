//
//  MyAttentionViewController.m
//  changeViewController
//
//  Created by wallace on 14/11/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "MyAttentionViewController.h"
#import "MasterViewController.h"


@interface MyAttentionViewController () <UIGestureRecognizerDelegate>
@property(nonatomic,strong)UIViewController *currentViewController;
@property(nonatomic,strong)PMMyAttentionGoodsTVC *goodsVC;
@property(nonatomic,strong)PMMyAttentionShopTVC *shopVC;

@property(nonatomic,strong)UILabel *redLine;

@property(nonatomic,strong)UIButton *deleteBtn;


@end

@implementation MyAttentionViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"我的关注";
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = HDVGPageBGGray;
    //初始化商品和店铺的子视图控制器
    self.goodsVC = [[PMMyAttentionGoodsTVC alloc] init];
    [self addChildViewController:self.goodsVC];
    [self.view addSubview:self.goodsVC.view];
    self.currentViewController = self.goodsVC;
    
    self.shopVC = [[PMMyAttentionShopTVC alloc] init];
    [self addChildViewController:self.shopVC];
    
    [self initEditBtn];
    
    [self initSegmentedControl];
    
    [self createNoResult];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //检查是否已经登陆
    [self checkIsLogin];
    
}

#pragma mark - 创建没有关注界面
- (void)createNoResult
{
    UIView *noResultView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, WIDTH, HEIGHT - 64 - 40)];
    UIImageView *noResultIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noResultView.frame.size.height)];
    self.tipImage = noResultIV;
    noResultIV.contentMode = UIViewContentModeScaleAspectFit;
    [noResultView addSubview:noResultIV];
    
//    UILabel *noResultLB = [[UILabel alloc] initWithFrame:CGRectMake(0, noResultView.bounds.size.height / 2, WIDTH, HeightRate(100))];
//    self.noResultLB = noResultLB;
//    noResultLB.numberOfLines = 0;
//    noResultLB.font = [UIFont systemFontOfSize:16.0f];
//    noResultLB.textAlignment = NSTextAlignmentCenter;
//    noResultLB.textColor = HDVGFontColor;
//    [noResultView addSubview:noResultLB];
    
    noResultView.backgroundColor = [UIColor whiteColor];
    self.noResultView = noResultView;
    [self.view insertSubview:noResultView atIndex:100];
    self.noResultView.hidden = YES;
}



//tableView左划删除功能与侧边栏的侧滑方法有冲突，这个方法解决
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


#pragma mark - 自定义导航栏
- (void)initEditBtn
{
//    UIBarButtonItem *editBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(clickToEdit:)];
    UIBarButtonItem *editBtn = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStyleDone target:self action:@selector(clickToEdit:)];
    [editBtn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR,NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = editBtn;
    
    
    
    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.deleteBtn = deleteBtn;
    deleteBtn.frame = CGRectMake(WidthRate(30), 10, WidthRate(100), 24);
    [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    [deleteBtn setTitleColor:NAVTEXTCOLOR forState:UIControlStateNormal];
    deleteBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [deleteBtn addTarget:self action:@selector(clickToDelete:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:deleteBtn];
    deleteBtn.hidden = YES;
}

- (void)clickToEdit:(UIBarButtonItem *)sender
{
    UITableView *tableView = ((UITableViewController *)self.currentViewController).tableView;
    [tableView setEditing:!tableView.editing animated:YES];
    
    if(tableView.isEditing)
    {
        sender.title = @"完成";
        [self.navigationItem setHidesBackButton:YES animated:YES];
        self.deleteBtn.hidden = NO;
    }
    else
    {
        sender.title = @"编辑";
        [self.navigationItem setHidesBackButton:NO animated:YES];
        self.deleteBtn.hidden = YES;
    }
}

- (void)clickToDelete:(UIButton *)sender
{
    PMMyAttentionGoodsTVC *vc = (PMMyAttentionGoodsTVC *)self.currentViewController;
    [vc deleteSelectedRows];
    
}

#pragma mark - 切换商品/店铺
- (void)initSegmentedControl
{
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:@[@"商品",@"店铺"]];
    segment.frame = CGRectMake(0, 0, WIDTH, 40);
    segment.selectedSegmentIndex = 0;
    
    [segment addTarget:self action:@selector(switchGoodsOrShops:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segment];
    
    [segment setTintColor:[UIColor clearColor]];
    //选中时字体颜色
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HDVGRed,NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:15], NSFontAttributeName, nil] forState:UIControlStateSelected];
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor lightGrayColor],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:15], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    segment.backgroundColor = RGBA(244, 244, 244, 1);
    //中间分割线
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(segment.center.x - 0.5, 0, 1, 40)];
    line.backgroundColor = RGBA(228, 228, 228, 1);
    [segment addSubview:line];
    
    //指示条
    self.redLine = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/5/segment.numberOfSegments, 37.5, WIDTH/5*3/segment.numberOfSegments, 2.5)];
    self.redLine.backgroundColor = HDVGRed;
    [segment addSubview:self.redLine];
    
    if (self.isShop)
    {
        segment.selectedSegmentIndex = 1;
        self.redLine.frame = CGRectMake(WIDTH/5/2 + WIDTH/2, self.redLine.frame.origin.y, self.redLine.bounds.size.width, self.redLine.bounds.size.height);
        [self transitionFromViewController:self.currentViewController toViewController:self.shopVC duration:0.3 options:UIViewAnimationOptionLayoutSubviews animations:nil completion:^(BOOL finished) {
            self.goodsVC.tableView.editing = NO;
            self.currentViewController = self.shopVC;
        }];
    }
}

- (void)switchGoodsOrShops:(UISegmentedControl *)sender
{
    UITableView *tableView = ((UITableViewController *)self.currentViewController).tableView;
    if ([tableView isEditing])
    {
        [tableView setEditing:NO];
        self.deleteBtn.hidden = YES;
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
    //指示条滑动的动画
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.redLine.frame;
        rect.origin.x = WIDTH/5/sender.numberOfSegments + WIDTH/sender.numberOfSegments*sender.selectedSegmentIndex;
        self.redLine.frame = rect;
    }];
    
    if (sender.selectedSegmentIndex == 0)
    {
        [self transitionFromViewController:self.currentViewController toViewController:self.goodsVC duration:0.3 options:UIViewAnimationOptionLayoutSubviews animations:nil completion:^(BOOL finished) {
            self.shopVC.tableView.editing = NO;
            self.currentViewController = self.goodsVC;
        }];
        
        
    }
    else
    {
        [self transitionFromViewController:self.currentViewController toViewController:self.shopVC duration:0.3 options:UIViewAnimationOptionLayoutSubviews animations:nil completion:^(BOOL finished) {
            self.goodsVC.tableView.editing = NO;
            self.currentViewController = self.shopVC;
        }];
        
        
    }
    
    [self.view bringSubviewToFront:self.noResultView];
    self.deleteBtn.hidden = YES;
    [self.navigationItem.rightBarButtonItem setTitle:@"编辑"];
}


#pragma mark - 检查是否已经登陆
- (void)checkIsLogin
{
    NSString *pmsid = [PMUserInfos shareUserInfo].PM_SID;
    if(pmsid == nil)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请先登录" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        av.tag = 101;
        [av show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101)
    {
        NSLog(@"alertView----clickedButtonAtIndex");
        
        [[MasterViewController defaultMasterVC] transitionToLoginVC];
    }
}

- (void)showNoResultViewWithIndex:(NSInteger)index
{
    if (index == 1) {
        [self.view bringSubviewToFront:self.noResultView];
        self.noResultView.hidden = NO;
        //self.noResultLB.text = title;
        self.tipImage.image = [UIImage imageNamed:@"goods_null"];
    }
    
    if (index == 2) {
        [self.view bringSubviewToFront:self.noResultView];
        self.noResultView.hidden = NO;
        //self.noResultLB.text = title;
        self.tipImage.image = [UIImage imageNamed:@"shop_null"];
    }
}


#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
