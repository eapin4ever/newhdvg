//
//  PMHomePageViewController.h
//  changeViewController
//
//  Created by pmit on 15/7/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    PredicateTypeProduct,
    PredicateTypeHeader,
    PredicateTypeScroll
    
}PredicateType;

@interface PMHomePageViewController : UIViewController


@property(nonatomic,assign)BOOL isNeedRefresh;//是否需要重新刷新数据
@property(nonatomic,assign)BOOL isGetCurrentModel;//是否使用了当前经纬度获取当前位置的商品了

@end
