//
//  PMDistrubutionTableViewCell.m
//  changeViewController
//
//  Created by P&M on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMDistrubutionTableViewCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMDistrubutionTableViewCell
{
    // 代言订单管理
    UILabel *stateLabel;
    UILabel *buyerLabel;
    UILabel *orderNumberLab;
    
    // 代言获利详情
    UILabel *timeLabel;
    UILabel *orderMoneyLab;
    UILabel *amountLabel;
    UILabel *moneyLabel;
    
    // 提现明细
    UILabel *channelLabel;
    UILabel *serialNumberLab;
}

#pragma mark - 代言订单
- (void)setDistributionOrderDataUI
{
    if (!timeLabel) {
        
        // 画线
        UILabel *lineImage = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(22), WIDTH - WidthRate(30) * 2, HeightRate(1))];
        lineImage.backgroundColor = RGBA(200, 200, 200, 1);
        [self.contentView addSubview:lineImage];
        
        UILabel *lineImage2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(82), WIDTH - WidthRate(30) * 2, HeightRate(1))];
        lineImage2.backgroundColor = RGBA(200, 200, 200, 1);
        [self.contentView addSubview:lineImage2];
        
        // 成交时间
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(25), HeightRate(27), WidthRate(170), HeightRate(50))];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = RGBA(150, 150, 150, 1);
        timeLabel.textAlignment = NSTextAlignmentCenter;
        timeLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:timeLabel];
        
        // 交易状态
        stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(190), HeightRate(27), WidthRate(120), HeightRate(50))];
        stateLabel.backgroundColor = [UIColor clearColor];
        stateLabel.textColor = RGBA(150, 150, 150, 1);
        stateLabel.textAlignment = NSTextAlignmentCenter;
        stateLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:stateLabel];
        
        // 订单金额
        orderMoneyLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(320), HeightRate(27), WidthRate(130), HeightRate(50))];
        orderMoneyLab.backgroundColor = [UIColor clearColor];
        orderMoneyLab.textColor = RGBA(186, 0, 14, 1);
        orderMoneyLab.textAlignment = NSTextAlignmentCenter;
        orderMoneyLab.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:orderMoneyLab];
        
        // 佣金金额
        moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(450), HeightRate(27), WidthRate(100), HeightRate(50))];
        moneyLabel.backgroundColor = [UIColor clearColor];
        moneyLabel.textColor = RGBA(186, 0, 14, 1);
        moneyLabel.textAlignment = NSTextAlignmentCenter;
        moneyLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:moneyLabel];
        
        // 买家
        buyerLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(550), HeightRate(27), WidthRate(180), HeightRate(50))];
        buyerLabel.backgroundColor = [UIColor clearColor];
        buyerLabel.textColor = RGBA(150, 150, 150, 1);
        buyerLabel.textAlignment = NSTextAlignmentCenter;
        buyerLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:buyerLabel];
        
        // 交易订单号
        orderNumberLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(640), HeightRate(97), WidthRate(450), HeightRate(50))];
        orderNumberLab.backgroundColor = [UIColor clearColor];
        orderNumberLab.textColor = RGBA(95, 95, 95, 1);
        orderNumberLab.textAlignment = NSTextAlignmentRight;
        orderNumberLab.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:orderNumberLab];
        
        // 查看物流按钮
        self.checkLogisticsBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.checkLogisticsBtn.frame = CGRectMake(WIDTH - WidthRate(186), lineImage2.frame.origin.y + HeightRate(10), WidthRate(150), HeightRate(60));
        [self.checkLogisticsBtn setTitle:@"查看物流" forState:UIControlStateNormal];
        [self.checkLogisticsBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.checkLogisticsBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
        [self.checkLogisticsBtn setBackgroundColor:RGBA(218, 67, 80, 1)];
        [self.checkLogisticsBtn.layer setCornerRadius:5.0];
        [self.contentView addSubview:self.checkLogisticsBtn];
    }
}

// 设置代言订单管理的成交时间、状态、订单金额、佣金、买家、订单号
- (void)setTime:(NSString *)time setState:(NSString *)state setOrderMoney:(NSString *)orderMoney setBrokerage:(NSString *)brokerage setBuyer:(NSString *)buyer setOrderNumber:(NSString *)orderNumber
{
    timeLabel.text = time;
    stateLabel.text = state;
    orderMoneyLab.text = orderMoney;
    moneyLabel.text = brokerage;
    buyerLabel.text = buyer;
    orderNumberLab.text = [NSString stringWithFormat:@"订单号：%@", orderNumber];
}


#pragma mark - 代言获利
// 设置代言获利详情数据 UI
- (void)setDistributionProfitDetailDataUI
{
    if (!timeLabel) {
        
        // 获利时间
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 7, WidthRate(230), 30)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = RGBA(150, 150, 150, 1);
        timeLabel.textAlignment = NSTextAlignmentCenter;
        timeLabel.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:timeLabel];
        
        // 订单金额
        orderMoneyLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(260), 7, WidthRate(230), 30)];
        orderMoneyLab.backgroundColor = [UIColor clearColor];
        orderMoneyLab.textColor = RGBA(186, 0, 14, 1);
        orderMoneyLab.textAlignment = NSTextAlignmentCenter;
        orderMoneyLab.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:orderMoneyLab];
        
        // 佣金金额
        moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(490), 7, WidthRate(230), 30)];
        moneyLabel.backgroundColor = [UIColor clearColor];
        moneyLabel.textColor = RGBA(186, 0, 14, 1);
        moneyLabel.textAlignment = NSTextAlignmentCenter;
        moneyLabel.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:moneyLabel];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

// 设置代言获利详情的时间、订单金额、佣金金额数据
- (void)setTime:(NSString *)time setOrderMoney:(double)orderMoney setMoney:(double)money
{
    timeLabel.text = time;
    orderMoneyLab.text = [NSString stringWithFormat:@"%.2lf", orderMoney];
    moneyLabel.text = [NSString stringWithFormat:@"%.2lf", money];
//    orderMoneyLab.text = orderMoney;
//    moneyLabel.text = money;
}

#pragma mark - 提现明细
- (void)setWithdrawalDetailDataUI
{
    if (!timeLabel) {
        
        // 画线
//        UILabel *lineImage = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(22), WIDTH - WidthRate(30) * 2, HeightRate(1))];
//        lineImage.backgroundColor = RGBA(200, 200, 200, 1);
//        [self.contentView addSubview:lineImage];
//        
//        UILabel *lineImage2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(82), WIDTH - WidthRate(30) * 2, HeightRate(1))];
//        lineImage2.backgroundColor = RGBA(200, 200, 200, 1);
//        [self.contentView addSubview:lineImage2];
        
        // 创建时间
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 7, WidthRate(260), 30)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor grayColor];
        timeLabel.textAlignment = NSTextAlignmentCenter;
        timeLabel.font = [UIFont systemFontOfSize:11.0f];
        [self.contentView addSubview:timeLabel];
        
        //----------------------版面需要，将这两个label互换
        // 提款金额
        moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(300), 7, WidthRate(150), 30)];
        moneyLabel.backgroundColor = [UIColor clearColor];
        moneyLabel.textColor = [UIColor grayColor];
        //moneyLabel.textColor = RGBA(190, 0, 32, 1);
        moneyLabel.textAlignment = NSTextAlignmentCenter;
        moneyLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:moneyLabel];
        
        // 交易状态
        stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(490), 7, WidthRate(230), 30)];
        stateLabel.backgroundColor = [UIColor clearColor];
        stateLabel.textColor = [UIColor grayColor];
        //stateLabel.textColor = RGBA(190, 0, 32, 1);
        stateLabel.textAlignment = NSTextAlignmentCenter;
        stateLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:stateLabel];
    }
}

// 设置提现明细的时间、提款金额、状态数据
- (void)setTime:(NSString *)time setWithdrawalMoney:(double)withdrawalMoney setState:(NSString *)state
{
//    NSInteger timeCuo = [time doubleValue] / 1000;
//    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:timeCuo];
//    //实例化一个NSDateFormatter对象
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    //设定时间格式,这里可以设置成自己需要的格式
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSString *newsTime = [dateFormatter stringFromDate:detaildate];
    timeLabel.text = time;
    moneyLabel.text = [NSString stringWithFormat:@"%.2lf", withdrawalMoney];
    stateLabel.text = state;
    if ([stateLabel.text isEqualToString:@"处理中"]) {
        stateLabel.textColor = RGBA(190, 0, 32, 1);
    }
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
