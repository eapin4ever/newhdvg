//
//  PMShopSettingTableViewCell.m
//  changeViewController
//
//  Created by P&M on 15/6/30.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMShopSettingTableViewCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMShopSettingTableViewCell

- (void)createShopSettingUI
{
    if (!self.listTitleLab)
    {
        self.listTitleLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(200), 40)];
        self.listTitleLab.font = [UIFont systemFontOfSize:15.0f];
        self.listTitleLab.textAlignment = NSTextAlignmentLeft;
        self.listTitleLab.textColor = HDVGFontColor;
        [self.contentView addSubview:self.listTitleLab];
    }
}

- (void)setShopSettingListTitle:(NSString *)listTitle
{
    self.listTitleLab.text = listTitle;
}

@end
