//
//  PMMyCenterMyTradeCell.m
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMMyCenterMyTradeCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMMyCenterMyTradeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.tradeView)
    {
        self.tradeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 80)];
        self.tradeView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.tradeView];
        
        NSArray *titleArr = @[@"待付款",@"待收货",@"待评价",@"退货"];
        NSArray *titleImageArr = @[@"my_01",@"my_02",@"my_03",@"my_04"];
        
        for (NSInteger i = 0; i < titleArr.count; i++)
        {
            UIView *tradeSubView = [[UIView alloc] init];
            tradeSubView.tag = 1000 + i;
            tradeSubView.frame = CGRectMake(WIDTH / 4 * i, 0, WIDTH / 4 - 1, 50);
            [self.tradeView addSubview:tradeSubView];
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(0, 0, WIDTH / 4 - 1, tradeSubView.frame.size.height);
            btn.tag = 100 + i;
            [btn addTarget:self action:@selector(goToMyTrade:) forControlEvents:UIControlEventTouchUpInside];

            switch (i) {
                case 0:
                {
                    self.payBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    self.payBtn.center = CGPointMake(tradeSubView.bounds.size.width / (iPhone6_Plus || iPhone6 ? 4.4 : 4.2) * 3 + 2, 10);
                    self.payBtn.bounds = (CGRect){CGPointZero,{14,14}};
                    self.payBtn.layer.cornerRadius = 7;
                    [self.payBtn setBackgroundColor:[UIColor redColor]];
                    self.payBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
                    self.payBtn.hidden = YES;
                    [tradeSubView addSubview:self.payBtn];
                }
                    break;
                    
                case 1:
                {
                    self.waitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    self.waitBtn.center = CGPointMake(tradeSubView.bounds.size.width / (iPhone6_Plus || iPhone6 ? 4.4 : 4.2) * 3 + 2, 10);
                    self.waitBtn.bounds = (CGRect){CGPointZero,{14,14}};
                    self.waitBtn.layer.cornerRadius = 7;
                    [self.waitBtn setBackgroundColor:[UIColor redColor]];
                    self.waitBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
                    self.waitBtn.hidden = YES;
                    [tradeSubView addSubview:self.waitBtn];
                }
                    break;
                    
                case 2:
                    
                {
                    self.discussBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    self.discussBtn.center = CGPointMake(tradeSubView.bounds.size.width / (iPhone6_Plus || iPhone6 ? 4.4 : 4.2) * 3 + 2, 10);
                    self.discussBtn.bounds = (CGRect){CGPointZero,{14,14}};
                    self.discussBtn.layer.cornerRadius = 7;
                    [self.discussBtn setBackgroundColor:[UIColor redColor]];
                    self.discussBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
                    self.discussBtn.hidden = YES;
                    [tradeSubView addSubview:self.discussBtn];
                }
                    break;
                case 3:
                {
                    self.relevantBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    self.relevantBtn.center = CGPointMake(tradeSubView.bounds.size.width / (iPhone6_Plus || iPhone6 ? 4.4 : 4.2) * 3 + 2, 10);
                    self.relevantBtn.bounds = (CGRect){CGPointZero,{14,14}};
                    self.relevantBtn.layer.cornerRadius = 7;
                    self.relevantBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
                    [self.relevantBtn setBackgroundColor:[UIColor redColor]];
                    self.relevantBtn.hidden = YES;
                    [tradeSubView addSubview:self.relevantBtn];
                }
                    break;
                default:
                    
                    break;
            }
            // 图片
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, tradeSubView.frame.size.height * 0.1, WIDTH / 4, tradeSubView.frame.size.height * 0.5)];
            imageView.image = [UIImage imageNamed:titleImageArr[i]];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            [tradeSubView addSubview:imageView];
            
            // 标题
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, tradeSubView.frame.size.height * 0.7, WIDTH / 4 - 1, tradeSubView.frame.size.height * 0.2)];
            label.text = titleArr[i];
            label.font = [UIFont systemFontOfSize:10.0f];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentCenter;
            [tradeSubView addSubview:label];
            [tradeSubView addSubview:btn];
            
        }
        
    }
}

- (void)setCellData:(NSInteger)payCount GetCount:(NSInteger)getCount DiscussCount:(NSInteger)discussCount BackCount:(NSInteger)backCount
{
    [self showOrHideByCount:self.payBtn AndCount:payCount];
    [self showOrHideByCount:self.waitBtn AndCount:getCount];
    [self showOrHideByCount:self.discussBtn AndCount:discussCount];
    [self showOrHideByCount:self.relevantBtn AndCount:backCount];
    
}

- (void)goToMyTrade:(UIButton *)sender
{
    if ([self.myCenterCellDelegate respondsToSelector:@selector(goToOutTradeDetail:)])
    {
        [self.myCenterCellDelegate goToOutTradeDetail:sender];
    }
}

- (void)showOrHideByCount:(UIButton *)sender AndCount:(NSInteger)countString
{
    sender.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    if (countString == 0)
    {
        sender.hidden = YES;
    }
    else if (countString > 0 &&countString < 10)
    {
        sender.hidden = NO;
        [sender setTitle:[NSString stringWithFormat:@"%@",@(countString)] forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        sender.hidden = NO;
        [sender setTitle:@"···" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

@end
