//
//  PMPublicClass.h
//  changeViewController
//
//  Created by P&M on 15/6/24.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPublicClass : NSObject

// 判断手机号码是否正确
+ (BOOL)checkMobile:(NSString *)string;

@end
