//
//  CatchObject.m
//  catchException
//
//  Created by pmit on 15/7/29.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "CatchObject.h"
#import "AppDelegate.h"

@implementation CatchObject

void uncaughtExceptionHandler(NSException *exception)
{
    //异常的堆栈信息
    NSArray *stackArray = [exception callStackSymbols];
    //出现异常的原因
    NSString *reason = [exception reason];
    //异常名称
    NSString *name = [exception name];
    NSString *exceptionInfo = [NSString stringWithFormat:@"Exceptionreason：%@nExceptionname：%@nExceptionstack：%@",name,reason,stackArray];
    NSLog(@"exception --> %@",exceptionInfo);
    
    NSMutableArray *tmpArr=[NSMutableArray arrayWithArray:stackArray];
    [tmpArr insertObject:reason atIndex:0];
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.crashView.hidden = NO;
    [app.window bringSubviewToFront:app.crashView];
}
@end
