//
//  NewPassViewController.h
//  changeViewController
//
//  Created by P&M on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 * 修改登录密码控制器
 */

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "MD5Util.h"
#import "PMNetworking.h"
#import "PMUserInfos.h"

@interface NewPassViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIView *setNewPassView;
@property (strong, nonatomic) UITextField *oldPassTextField;
@property (strong, nonatomic) UITextField *passwordTextField;

@property (strong, nonatomic) PMNetworking *networking;

@end