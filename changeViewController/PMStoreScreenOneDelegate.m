//
//  PMStoreScreenOneDelegate.m
//  changeViewController
//
//  Created by P&M on 15/7/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMStoreScreenOneDelegate.h"
#import "PMMyPhoneInfo.h"
#import "PMStoreScreenTitleCell.h"

@implementation PMStoreScreenOneDelegate


#pragma mark - tableView data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.screenOneArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

// 设置 footer 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(1))];
    footerView.backgroundColor = RGBA(200, 200, 200, 1);
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMStoreScreenTitleCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
    
    [titleCell createScreenTitleUI];
    
    titleCell.backgroundColor = [UIColor clearColor];
    
    NSDictionary *dic = self.screenOneArray[indexPath.section];
    
    [titleCell setStoreScreenTitle:[dic objectNullForKey:@"name"]];
    
    return titleCell;
}

#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.screenOneArray[indexPath.section];
    [self.screenOneDelegate passDictionary:dic];
}

@end
