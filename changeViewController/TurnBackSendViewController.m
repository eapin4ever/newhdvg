//
//  TurnBackSendViewController.m
//  changeViewController
//
//  Created by ZhangEapin on 15/5/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "TurnBackSendViewController.h"
#import "PMMyPhoneInfo.h"
#import "TranSelectViewController.h"
#import "PMNetworking.h"

@interface TurnBackSendViewController () <UIAlertViewDelegate>

@property (strong,nonatomic) NSArray *tranArr;
@property (strong,nonatomic) NSArray *tranValueArr;
@property (strong,nonatomic) UIView *backView;
@property (strong,nonatomic) UITextField *numText;


@end

@implementation TurnBackSendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"填写物流信息";
    
    [self createUI];
}

- (void)createUI
{
    self.view.backgroundColor = RGBA(236, 236, 236, 1);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyBoard:)];
    [self.view addGestureRecognizer:tap];
    
    UIView *infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, WIDTH, 100)];
    infoView.backgroundColor = [UIColor whiteColor];
    
    NSString *exampleTitle = @"物流公司";
    CGSize titleSize = [exampleTitle sizeWithFont:[UIFont systemFontOfSize:16.0f]];
    
    UILabel *companyTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, titleSize.width + 10, 49.5)];
    companyTitleLB.font = [UIFont systemFontOfSize:15.0f];
    companyTitleLB.textColor = [UIColor lightGrayColor];
    companyTitleLB.text = @"物流公司:";
    [infoView addSubview:companyTitleLB];
    
    UITextField *companyText = [[UITextField alloc] initWithFrame:CGRectMake(titleSize.width + 30, 0, WIDTH - titleSize.width, 49.5)];
    companyText.enabled = NO;
    companyText.placeholder = @"点击选择物流公司";
    self.companyText = companyText;
    UIButton *tranBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    tranBtn.frame = CGRectMake(titleSize.width + 30, 0, WIDTH - titleSize.width, 49.5);
    [tranBtn addTarget:self action:@selector(goTranSeleted:) forControlEvents:UIControlEventTouchUpInside];
    [infoView addSubview:tranBtn];
    
    [companyText setValue:[UIFont boldSystemFontOfSize:12] forKeyPath:@"_placeholderLabel.font"];
    companyText.contentHorizontalAlignment = UIControlContentVerticalAlignmentCenter;
    [companyText setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, companyText.bounds.size.width,companyText.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [companyText setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [companyText.layer setBorderWidth:0];
    companyText.textColor = [UIColor blackColor];
    companyText.font = [UIFont systemFontOfSize:14];
    
    [infoView addSubview:companyText];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = [UIColor lightGrayColor].CGColor;
    line.frame = CGRectMake(0, 49.5, WIDTH, 0.5);
    [infoView.layer addSublayer:line];
    
    UILabel *numTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 50.5, titleSize.width + 10, 49.5)];
    numTitleLB.font = [UIFont systemFontOfSize:15.0f];
    numTitleLB.textColor = [UIColor lightGrayColor];
    numTitleLB.text = @"物流单号:";
    [infoView addSubview:numTitleLB];
    
    UITextField *numText = [[UITextField alloc] initWithFrame:CGRectMake(titleSize.width + 30, 50.5, WIDTH - titleSize.width, 49.5)];
    numText.placeholder = @"请填写物流单号";
    
    [numText setValue:[UIFont boldSystemFontOfSize:12] forKeyPath:@"_placeholderLabel.font"];
    numText.contentHorizontalAlignment = UIControlContentVerticalAlignmentCenter;
    [numText setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, numText.bounds.size.width,numText.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [numText setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [numText.layer setBorderWidth:0];
    numText.textColor = [UIColor blackColor];
    numText.font = [UIFont systemFontOfSize:14];
    numText.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.numText = numText;
    [infoView addSubview:numText];
    [self.view addSubview:infoView];
    
    UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame = CGRectMake(WidthRate(46), HEIGHT - 64 - 60, WIDTH - WidthRate(46) * 2, 44);
    [submitBtn setBackgroundColor:ButtonBgColor];
    [submitBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    submitBtn.layer.cornerRadius = 6.0f;
    [submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitInfo:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)goTranSeleted:(UIButton *)sender
{
    TranSelectViewController *tranSelectedVC = [[TranSelectViewController alloc] init];
    tranSelectedVC.myDelegate = self;
    [self.navigationController pushViewController:tranSelectedVC animated:YES];
}

- (void)submitInfo:(UIButton *)sender
{
    if ([self.tranValue isKindOfClass:[NSNull class]] || [self.tranValue isEqualToString:@""] || !self.tranValue)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请选择物流公司" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([self.numText.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请填写订单号" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSDictionary *parameter = @{@"expressCompany":self.tranValue,@"expressNo":self.numText.text,@"id":self.backOrderId,PMSID};
        
        [[PMNetworking defaultNetworking] request:PMRequestStateUpdateLogistics WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"提交成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alert.tag = 100;
                [alert show];
            }
        } showIndicator:YES];
    }
}

- (void)resignKeyBoard:(UITapGestureRecognizer *)tap
{
    [self.numText resignFirstResponder];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100 && buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
