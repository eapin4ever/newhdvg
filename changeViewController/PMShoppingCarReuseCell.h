//
//  PMShoppingCarReuseCell.h
//  changeViewController
//
//  Created by pmit on 14/11/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMShoppingCarReuseCell : UITableViewCell
@property(nonatomic,strong)UIButton *minusBtn;
@property(nonatomic,strong)UIButton *plusBtn;
@property(nonatomic,strong)UIImageView *iv;
//@property(nonatomic,strong)UIButton *goDetailBtn;
@property (assign,nonatomic) NSInteger maxCount;
@property (strong,nonatomic) UILabel *titleLB;



- (void)createUI;

- (void)setContentTitle:(NSString *)title classify1:(NSDictionary *)classify1 classify2:(NSDictionary *)classify2 distanceOrLocation:(NSString *)distance number:(NSString *)num price:(double)price;

- (void)numberPlus;
- (void)numberMinus;
@end
