//
//  DistributionSettingViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "DistributionSettingViewController.h"
#import "PMMyPhoneInfo.h"
#import "SWShopNameViewController.h"
#import "SWShopIntroduceViewController.h"
#import "DistrubitionBackground.h"
#import "PMImageCroperSingleton.h"
#import "PMNetworking.h"
#import "DistributionStyleViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PMShopMusicViewController.h"
#import "WebViewViewController.h"
#import "PMShopSettingTableViewCell.h"

@interface DistributionSettingViewController () <UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,DistrubitionBackgroundDelegate,DistributionStyleViewControllerDelegate,SWShopNameViewControllerDelegate,SWShopIntroduceViewControllerDelegate>

@property (strong,nonatomic) UITableView *distributionTableView;
@property (strong,nonatomic) NSArray *distributionArr;
@property (strong,nonatomic) NSArray *backgroundIVArr;
@property (strong,nonatomic) NSArray *styleArr;
@property (strong,nonatomic) UIImageView *bgIV;
@property (strong,nonatomic) UIImageView *backgroundIV;
@property (strong,nonatomic) UIImageView *headerIV;
@property (strong,nonatomic) NSArray *musicArr;
@property (copy,nonatomic) NSString *bgUrlString;
@property (copy,nonatomic) NSString *styleUrlString;
@property (copy,nonatomic) NSString *newsShopName;
@property (copy,nonatomic) NSString *introString;
@property (strong,nonatomic) UILabel *nameLB;
@property (strong,nonatomic) UILabel *remarkLB;

@end

@implementation DistributionSettingViewController

static NSString *const settingCell = @"settingCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"店铺设置";
    [self getMyDistubtionFileByNet];
    self.distributionArr = @[@[@"店铺名称",@"店铺介绍",@"店铺认证"],@[@"店铺头像",@"店铺背景",@"店铺音乐",@"店铺样式"]];
    [self buildTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getShopData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    self.shopDataDic = nil;
    self.distributionTableView = nil;
    self.distributionArr = nil;
    self.backgroundIVArr = nil;
    self.styleArr = nil;
    self.bgIV = nil;
    self.backgroundIV = nil;
    self.headerIV = nil;
    self.musicArr = nil;
}

- (void)buildTableView
{
    self.distributionTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStyleGrouped];
    self.distributionTableView.backgroundColor = HDVGPageBGGray;
    self.distributionTableView.delegate = self;
    self.distributionTableView.dataSource = self;
    [self.distributionTableView registerClass:[PMShopSettingTableViewCell class] forCellReuseIdentifier:settingCell];
    self.distributionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.distributionTableView];
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    footView.backgroundColor = HDVGPageBGGray;
    
    UIButton *preLookBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    preLookBtn.frame = CGRectMake(WidthRate(36), 5, WIDTH - WidthRate(72), 44);
    [preLookBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [preLookBtn addTarget:self action:@selector(preLookAction:) forControlEvents:UIControlEventTouchUpInside];
    [preLookBtn setTitle:@"预览店铺" forState:UIControlStateNormal];
    [preLookBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [preLookBtn setBackgroundColor:ButtonBgColor];
    [preLookBtn.layer setCornerRadius:6.0f];
    [footView addSubview:preLookBtn];
    
    self.distributionTableView.tableFooterView = footView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.distributionArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.distributionArr[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMShopSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingCell];
    [cell createShopSettingUI];
    cell.listTitleLab.text = [self.distributionArr[indexPath.section] objectAtIndex:indexPath.row];

    if (indexPath.section == 0)
    {
        
        if (indexPath.row == 0)
        {
            if (!self.nameLB)
            {
                CALayer *line = [CALayer layer];
                line.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
                line.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
                [cell.contentView.layer addSublayer:line];
                
                UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 160, 15, 130, 20)];
                tipLB.font = [UIFont systemFontOfSize:14.0f];
                tipLB.textColor = [UIColor lightGrayColor];
                tipLB.textAlignment = NSTextAlignmentRight;
                [cell.contentView addSubview:tipLB];
                self.nameLB = tipLB;
            }
            
            if ([[self.shopDataDic objectNullForKey:@"shopName"] isKindOfClass:[NSString class]] && ![[self.shopDataDic objectNullForKey:@"shopName"] isEqualToString:@""])
            {
                self.nameLB.text = [self.shopDataDic objectNullForKey:@"shopName"];
            }
            
        }
        else if (indexPath.row == 1)
        {
            if (!self.remarkLB)
            {
                CALayer *line = [CALayer layer];
                line.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
                line.frame = CGRectMake(WidthRate(26), 0, WIDTH, HeightRate(1));
                [cell.contentView.layer addSublayer:line];
                
                UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 160, 15, 130, 20)];
                tipLB.font = [UIFont systemFontOfSize:14.0f];
                tipLB.textColor = [UIColor lightGrayColor];
                tipLB.textAlignment = NSTextAlignmentRight;
                [cell.contentView addSubview:tipLB];
                
                self.remarkLB = tipLB;
            }
            
 
            NSString *remarkString = @"";
            if (![[self.shopDataDic objectNullForKey:@"shopAbout"] isKindOfClass:[NSString class]])
            {
                remarkString = @"";
            }
            else
            {
                remarkString = [self.shopDataDic objectNullForKey:@"shopAbout"];
            }
                
            self.remarkLB.text = remarkString;
        }
        else
        {
            CALayer *line = [CALayer layer];
            line.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            line.frame = CGRectMake(WidthRate(26), 0, WIDTH, HeightRate(1));
            [cell.contentView.layer addSublayer:line];
            
            CALayer *line2 = [CALayer layer];
            line2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            line2.frame = CGRectMake(0, 50 - 0.5, WIDTH, HeightRate(1));
            [cell.contentView.layer addSublayer:line2];
            
            UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 160, 15, 130, 20)];
            tipLB.font = [UIFont systemFontOfSize:14.0f];
            tipLB.textColor = [UIColor lightGrayColor];
            tipLB.textAlignment = NSTextAlignmentRight;
            [cell.contentView addSubview:tipLB];
            
            tipLB.text = @"已认证";
            tipLB.font = [UIFont systemFontOfSize:14.0f];
            tipLB.textColor = [UIColor greenColor];
        }
        
        UIImageView *arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 15, 20, 20)];
        arrowIV.contentMode = UIViewContentModeScaleAspectFit;
        arrowIV.image = [UIImage imageNamed:@"arrowRight"];
        
        if (indexPath.row == 2)
        {
            
        }
        else
        {
            [cell.contentView addSubview:arrowIV];
        }
    }
    // 第二个分组
    else
    {
        if (indexPath.row == 1)
        {
            CALayer *line = [CALayer layer];
            line.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            line.frame = CGRectMake(WidthRate(26), 0, WIDTH, HeightRate(1));
            [cell.contentView.layer addSublayer:line];
            
//            UIImageView *backGroundIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 120, 10, 90, 30)];
//            backGroundIV.layer.cornerRadius = 15.0f;
////            backGroundIV.contentMode = UIViewContentModeScaleAspectFit;
//            NSString *backgroundUrlString = @"";
//            if ([[self.shopDataDic objectNullForKey:@"shopPhoto"] isKindOfClass:[NSString class]] && ![[self.shopDataDic objectNullForKey:@"shopPhoto"] isEqualToString:@""])
//            {
//                backgroundUrlString = [self.shopDataDic objectNullForKey:@"shopPhoto"];
//                [self.backgroundIV sd_setImageWithURL:[NSURL URLWithString:backgroundUrlString] placeholderImage:[UIImage imageNamed:@"backiv"]];
//            }
//            else
//            {
//                self.backgroundIV.frame = CGRectMake(WIDTH - 60, 10, 30, 30);
//                self.backgroundIV.contentMode = UIViewContentModeScaleAspectFit;
//                [self.backgroundIV setImage:[UIImage imageNamed:@"backiv"]];
//            }
//            [cell.contentView addSubview:backGroundIV];
//            self.backgroundIV = backGroundIV;
        }
        else if (indexPath.row == 0)
        {
            if (!self.headerIV)
            {
                CALayer *line = [CALayer layer];
                line.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
                line.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
                [cell.contentView.layer addSublayer:line];
                
                
                UIImageView *headerIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 60, 10, 30, 30)];
                headerIV.layer.cornerRadius = 15.0f;
                self.headerIV = headerIV;
                
                [cell.contentView addSubview:headerIV];
            }
            
            NSString *headerPhotoUrlString = @"";
            if ([[self.shopDataDic objectNullForKey:@"shopLogo"] isKindOfClass:[NSString class]] && ![[self.shopDataDic objectNullForKey:@"shopLogo"] isEqualToString:@""])
            {
                headerPhotoUrlString = [self.shopDataDic objectNullForKey:@"shopLogo"];
                
                [self.headerIV sd_setImageWithURL:[NSURL URLWithString:headerPhotoUrlString] placeholderImage:[UIImage imageNamed:@"myheader"]];
            }
            else
            {
                [self.headerIV setImage:[UIImage imageNamed:@"myheader"]];
            }
            
            
        }
        else if (indexPath.row == 2) {
            CALayer *line = [CALayer layer];
            line.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            line.frame = CGRectMake(WidthRate(26), 0, WIDTH, HeightRate(1));
            [cell.contentView.layer addSublayer:line];
        }
        else {
            CALayer *line = [CALayer layer];
            line.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            line.frame = CGRectMake(WidthRate(26), 0, WIDTH, HeightRate(1));
            [cell.contentView.layer addSublayer:line];
            
            CALayer *line2 = [CALayer layer];
            line2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
            line2.frame = CGRectMake(0, 50 - 0.5, WIDTH, HeightRate(1));
            [cell.contentView.layer addSublayer:line2];
        }
        
        UIImageView *arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 15, 20, 20)];
        arrowIV.contentMode = UIViewContentModeScaleAspectFit;
        arrowIV.image = [UIImage imageNamed:@"arrowRight"];
        [cell.contentView addSubview:arrowIV];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            SWShopNameViewController *shopNameVC = [[SWShopNameViewController alloc] init];
            shopNameVC.nameDelegate = self;
            shopNameVC.shopDataDic = self.shopDataDic;
            shopNameVC.shopSettingVC = self;
            [self.navigationController pushViewController:shopNameVC animated:YES];
        }
        else if (indexPath.row == 1) {
            SWShopIntroduceViewController *introduceVC = [[SWShopIntroduceViewController alloc] init];
            introduceVC.remarkDelegate = self;
            introduceVC.shopDataDic = self.shopDataDic;
            introduceVC.shopSettingVC = self;
            [self.navigationController pushViewController:introduceVC animated:YES];
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 1)
        {
            DistrubitionBackground *backgroundVC = [[DistrubitionBackground alloc] init];
            backgroundVC.backgroundIVArr = self.backgroundIVArr;
            backgroundVC.shopId = [self.shopDataDic objectNullForKey:@"id"];
            backgroundVC.bgDelegate = self;
            backgroundVC.shopDic = self.shopDataDic;
            [self.navigationController pushViewController:backgroundVC animated:YES];
        }
        else if (indexPath.row == 0)
        {
            UIActionSheet *headerPhotoAS = [[UIActionSheet alloc] initWithTitle:@"头像上传" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"拍照" otherButtonTitles:@"从相册中选择", nil];
            [headerPhotoAS showInView:self.view];
        }
        else if (indexPath.row == 2)
        {
            PMShopMusicViewController *bgMusicVC = [[PMShopMusicViewController alloc] init];
            bgMusicVC.musicArr = self.musicArr;
            bgMusicVC.muiscName = [self.shopDataDic objectNullForKey:@"shopMusic"];
            bgMusicVC.shopId = [self.shopDataDic objectNullForKey:@"id"];
            [self.navigationController pushViewController:bgMusicVC animated:YES];
        }
        else if (indexPath.row == 3)
        {
            DistributionStyleViewController *styleVC = [[DistributionStyleViewController alloc] init];
            styleVC.styleDelegate = self;
            styleVC.styleArr = self.styleArr;
            styleVC.shopId = [self.shopDataDic objectNullForKey:@"id"];
            styleVC.shopDic = self.shopDataDic;
            if (self.styleUrlString && ![self.styleUrlString isEqualToString:@""])
            {
                styleVC.styleUrlString = self.styleUrlString;
            }
            [self.navigationController pushViewController:styleVC animated:YES];
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)preLookAction:(UIButton *)sender
{
    WebViewViewController *webVC = [[WebViewViewController alloc] initWithWebType:webTypeShop];
    webVC.newsShopId = [self.shopDataDic objectNullForKey:@"id"];
    [self.navigationController pushViewController:webVC animated:YES];
}

- (void)getMyDistubtionFileByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateGetShopFile WithParameters:nil callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.backgroundIVArr = [[dic objectNullForKey:@"data"] objectNullForKey:@"background"];
            self.styleArr = [[dic objectNullForKey:@"data"] objectNullForKey:@"style"];
            self.musicArr = [[dic objectNullForKey:@"data"] objectNullForKey:@"music"];
        }
    } showIndicator:NO];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                //设置拍照后的图片可被编辑
                picker.allowsEditing = YES;
                picker.sourceType = sourceType;
                [self presentViewController:picker animated:YES completion:nil];
            }
            else {
                NSLog(@"模拟其中无法打开照相机,请在真机中使用");
            }
        }
             break;
        case 1:
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            //设置选择后的图片可被编辑
            picker.allowsEditing = YES;
            [self presentViewController:picker animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"])
    {
        //先把图片转成NSData
        UIImage *originImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        
        //图片压缩，因为原图都是很大的，不必要传原图
        UIImage *scaleImage = [self scaleImage:originImage toScale:0.3];
        
        
        NSData *data;
        if (UIImagePNGRepresentation(scaleImage) == nil)
        {
            data = UIImageJPEGRepresentation(scaleImage, 1.0);
        }
        else
        {
            data = UIImagePNGRepresentation(scaleImage);
        }
        //将二进制数据生成UIImage
        UIImage *image = [UIImage imageWithData:data];
        self.headerIV.image = image;
        
        
        //self.headerIV.image = [UIImage imageWithData:data];
        [self uploadImageView:data];
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)uploadImageView:(NSData *)imageData
{
    NSDictionary *param = @{PMSID,@"flag":@"shopLogo"};
    
    //上传头像
    [[PMNetworking defaultNetworking] uploadImage:param imageData:imageData success:^(NSDictionary *dic) {
        NSLog(@"dic === %@", dic);
        if (intSuccess == 1)
        {
            [self.headerIV sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"data"]]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"上传成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}


- (void)changeBgPhoto:(NSString *)urlString AndData:(NSData *)bgData
{
    self.bgUrlString = urlString;
    
    self.backgroundIV.image = [UIImage imageWithData:bgData];
}

- (void)passStyleUrlString:(NSString *)styleUrlString
{
    self.styleUrlString = styleUrlString;
}

- (void)passShopName:(NSString *)shopName
{
    self.nameLB.text = shopName;
}

- (void)passRemark:(NSString *)remark
{
    self.remarkLB.text = remark;
}

#pragma mark- 缩放图片
-(UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width*scaleSize,image.size.height*scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height *scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

- (void)getShopData
{
    [[PMNetworking defaultNetworking] request:PMRequestStateShareShowShopHome WithParameters:@{PMSID} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            self.shopDataDic = [dic objectNullForKey:@"data"];
            [self.distributionTableView reloadData];
        }
    }showIndicator:NO];
}

@end
