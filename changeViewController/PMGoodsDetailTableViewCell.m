//
//  PMGoodsDetailTableViewCell.m
//  changeViewController
//
//  Created by pmit on 14/11/25.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#define textFont 12
#define itemWidth WidthRate(170)

#import "PMGoodsDetailTableViewCell.h"

@implementation PMGoodsDetailTableViewCell
{
    UILabel *_goodTitleLB;
    UILabel *_salesVolumeLB;
    UILabel *_stockLB;
    UITextField*_distanceLB;
    UILabel *_priceLB;

}

- (void)createUI
{
    if(!_goodTitleLB)
    {
        _goodTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(45), 7, WidthRate(670), 40)];
        _goodTitleLB.font = [UIFont systemFontOfSize:14];
        _goodTitleLB.numberOfLines = 2;
        [self.contentView addSubview:_goodTitleLB];
        
        _distanceLB = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(45), 50, itemWidth, 15)];
        _distanceLB.font = [UIFont systemFontOfSize:textFont];
        _distanceLB.textColor = RGBA(142, 141, 146,1);
        UIImageView *leftIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location"]];
        leftIV.frame = CGRectMake(2, 2, 11, 11);
        leftIV.contentMode = UIViewContentModeScaleAspectFit;
        _distanceLB.leftView = leftIV;
        _distanceLB.leftViewMode = UITextFieldViewModeAlways;
        _distanceLB.userInteractionEnabled = NO;
        [self.contentView addSubview:_distanceLB];
        
        
        _salesVolumeLB = [[UILabel alloc] initWithFrame:CGRectMake(_distanceLB.frame.origin.x + _distanceLB.bounds.size.width, 50, WidthRate(160), 15)];
        _salesVolumeLB.font = [UIFont systemFontOfSize:textFont];
        _salesVolumeLB.textColor = RGBA(142, 141, 146,1);
        [self.contentView addSubview:_salesVolumeLB];
        
        _stockLB = [[UILabel alloc] initWithFrame:CGRectMake(_salesVolumeLB.frame.origin.x + _salesVolumeLB.bounds.size.width, 50, itemWidth, 15)];
        _stockLB.font = [UIFont systemFontOfSize:textFont];
        _stockLB.textColor = RGBA(142, 141, 146,1);
        [self.contentView addSubview:_stockLB];
        

        
        
        _priceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(540), 47, WidthRate(180), 15)];
        _priceLB.font = [UIFont boldSystemFontOfSize:16];
        _priceLB.textColor = HDVGRed;
        _priceLB.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_priceLB];
        
//        _attentionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        _attentionBtn.frame = CGRectMake(WidthRate(540), -43, WidthRate(180), 30);
//        _attentionBtn.titleLabel.font = [UIFont boldSystemFontOfSize:textFont+2];
//        [_attentionBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [_attentionBtn.layer setBackgroundColor:RGBA(238, 89, 117, 1).CGColor];
//        [_attentionBtn.layer setBorderWidth:0];
//        [_attentionBtn.layer setCornerRadius:6.0];
//        [self.contentView addSubview:_attentionBtn];
        
        
        CALayer *grayLine = [[CALayer alloc] init];
        grayLine.frame = CGRectMake(0, 0, WIDTH, 1);
        grayLine.backgroundColor = RGBA(220, 220, 220, 1).CGColor;
        [self.contentView.layer addSublayer:grayLine];
    }
}

- (void)setContentTitle:(NSString *)title distance:(NSString *)distance
{
    _goodTitleLB.text = [NSString stringWithFormat:@"%@",title];
    _distanceLB.text = distance;
    _salesVolumeLB.text = @"销量:0";
    _stockLB.text = @"库存:0";
}

- (void)setPrice:(NSString *)price sales:(NSString *)sales stock:(NSString *)stock
{
    _salesVolumeLB.text = [NSString stringWithFormat:@"销量: %@",sales];
    _stockLB.text = [NSString stringWithFormat:@"库存: %@",stock];
    _priceLB.text = [NSString stringWithFormat:@"￥ %@",price];
}


@end
