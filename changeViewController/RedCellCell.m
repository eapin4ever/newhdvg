//
//  RedCellCell.m
//  changeViewController
//
//  Created by EapinZhang on 15/3/23.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "RedCellCell.h"
#import "PMMyPhoneInfo.h"

@implementation RedCellCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //self.redIV = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(0), 10, WidthRate(100), 40)];
    self.redMoneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), HeightRate(10), self.bounds.size.width, self.bounds.size.height / 2)];
    self.redMoneyLabel.font = [UIFont systemFontOfSize:20.0f];
    self.redMoneyLabel.textColor = [UIColor redColor];
    self.redMoneyLabel.text = [NSString stringWithFormat:@"￥%@",self.perRedMoney];
    [self.contentView addSubview:self.redMoneyLabel];
    
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), self.bounds.size.height / 2, self.bounds.size.width, self.bounds.size.height / 2)];
    self.timeLabel.font = [UIFont systemFontOfSize:13.0f];
    self.timeLabel.textColor = [UIColor lightGrayColor];
    self.timeLabel.text = [NSString stringWithFormat:@"到期时间:%@",@"2015-12-31"];
    [self.contentView addSubview:self.timeLabel];
}

@end
