//
//  PMHongBaoCell.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//
#define BARHI 30

#import "PMHongBaoCell.h"

@implementation PMHongBaoCell
{
    UILabel *_titleLB;
    UILabel *_peopleLB;
    
}

- (void)createUI
{
    if(!_iv)
    {
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(320)- BARHI)];
        [self.contentView addSubview:_iv];
        
        UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, WidthRate(320) - BARHI, WIDTH, BARHI)];
        barView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:barView];
        
        _titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(25), 0, WidthRate(300), BARHI)];
        _titleLB.textColor = [UIColor darkGrayColor];
        _titleLB.font = [UIFont systemFontOfSize:12];
        [barView addSubview:_titleLB];
        
        
        _peopleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(350), 0, WidthRate(200), BARHI)];
        _peopleLB.textColor = [UIColor darkGrayColor];
        _peopleLB.font = [UIFont systemFontOfSize:12];
        [barView addSubview:_peopleLB];
        
        _timeBtn = [[UIButton alloc] initWithFrame:CGRectMake(WidthRate(555), 5, WidthRate(140), 20)];
        _timeBtn.layer.cornerRadius = 2.0;
        _timeBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _timeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:12];
        [_timeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        _timeBtn.userInteractionEnabled = NO;
        [barView addSubview:_timeBtn];
    }
}


- (void)setTitle:(NSString *)title people:(NSString *)people
{
    _titleLB.text = title;
    _peopleLB.text = [NSString stringWithFormat:@"已有%@人参与",people];
}
@end
