//
//  PMGetCashTableViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/28.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

/**
 *  提现明细控制器
 */

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMUserInfos.h"
#import "PMNetworking.h"
#import "PMDistrubutionTableViewCell.h"
#import "WithdrawalDetailViewController.h"

@interface PMGetCashTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *withdrawalDetailArray;

@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *orderMoneyLab;
@property (strong, nonatomic) UILabel *stateLabel;

@property (assign, nonatomic) BOOL withdrawalMoney;

@property (strong, nonatomic) PMNetworking *networking;

// 用于判断是不是被present出来的
@property (assign, nonatomic) BOOL isPresented;
// 用于present时的右侧取消绑定按钮事件
- (void)backClick:(UIBarButtonItem *)bbi;

// 单列方便调用
+ (PMGetCashTableViewController *)shareInstance;

@end
