//
//  AddressManageTableViewController.m
//  changeViewController
//
//  Created by P&M on 14/12/16.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "AddressManageTableViewController.h"
#import "MasterViewController.h"

@interface AddressManageTableViewController ()<UIAlertViewDelegate,UINavigationBarDelegate>
@property(nonatomic,strong)PMNetworking *netWorking;
@end

@implementation AddressManageTableViewController
{
    BOOL _showAccessory;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"收货地址";
        
        self.tableView.backgroundColor = HDVGPageBGGray;
        _showAccessory = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[SafeManageTableViewCell class] forCellReuseIdentifier:@"addressCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableView setEditing:NO animated:YES];
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 45 + WidthRate(80))];
    headerView.backgroundColor = HDVGPageBGGray;
    
    // 添加地址按钮
    UIButton *createAddressButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    createAddressButton.frame = CGRectMake(WidthRate(40), WidthRate(40), WIDTH - WidthRate(80), 45);
    [createAddressButton setTitle:@"+ 新增地址" forState:UIControlStateNormal];
    [createAddressButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [createAddressButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [createAddressButton addTarget:self action:@selector(createAddressButton:) forControlEvents:UIControlEventTouchUpInside];
    [createAddressButton setBackgroundColor:ButtonBgColor];
    [createAddressButton.layer setCornerRadius:6.0];
    [headerView addSubview:createAddressButton];
    
    self.tableView.tableHeaderView = headerView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initNewAddressDataList];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
// 根据数组去创建多少个section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.addressSectionArray count];
}

// 每个section里有多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// 每个行的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

// 每个行的头的高度
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 10;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 1)];
//    headerView.backgroundColor = HDVGPageBGGray;
//    
//    return headerView;
//}

// 每个行尾的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 10)];
    footerView.backgroundColor = HDVGPageBGGray;
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SafeManageTableViewCell *newAddressCell = [tableView dequeueReusableCellWithIdentifier:@"addressCell"];
    newAddressCell.backgroundColor = HDVGPageBGGray;
    [newAddressCell setNewAddressDataUI];
    
    NSDictionary *dict = self.addressSectionArray[indexPath.section];
    
    // 获取下来的数据
    [newAddressCell setName:[dict objectNullForKey:@"name"] setMobile:[dict objectNullForKey:@"mobile"] setAddress:[[dict objectNullForKey:@"areas"] stringByAppendingString:[dict objectNullForKey:@"address"]]];
    
    newAddressCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //是否默认地址
    if([[dict objectNullForKey:@"isDefault"] integerValue] == 1)
    {
        newAddressCell.defaultMark.hidden = NO;

    }
    else
    {
        newAddressCell.defaultMark.hidden = YES;
    }
    
    
    if(_showAccessory)
        newAddressCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return newAddressCell;
}

#pragma mark - 点击cell触发的事件响应方法

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EditAddressViewController *editAddressVC = [[EditAddressViewController alloc] initWithAddressDetail:self.addressSectionArray[indexPath.section]];
    editAddressVC.addressManagerVC = self;
    [self.navigationController pushViewController:editAddressVC animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.addressSectionArray[indexPath.section];
    [self deleteAddressRequestWithAddressID:[dic objectNullForKey:@"id"]];
    
    [self.addressSectionArray removeObjectAtIndex:indexPath.section];//删除数组元素所在区
    // 删除区所在位置
    [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    
}


// 新建地址响应按钮事件
- (void)createAddressButton:(id)sender
{
    ShippingAddViewController *shippingAddVC = [[ShippingAddViewController alloc] init];
    [self.navigationController pushViewController:shippingAddVC animated:YES];
}

#pragma mark - 请求地址数据
- (void)initNewAddressDataList
{
    PMUserInfos *userInfo = [PMUserInfos shareUserInfo];
    if(userInfo.PM_SID == nil)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"请先登录" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [av show];
    }
    else {
        PMNetworking *nw = [PMNetworking defaultNetworking];
        
        [nw request:PMRequestStateMyGetAddress WithParameters:@{@"PM_SID":userInfo.PM_SID} callBackBlock:^(NSDictionary *dict) {
            if ([dict isKindOfClass:[NSDictionary class]])
            {
                if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
                    
                    self.addressSectionArray = [[dict objectNullForKey:@"data"] mutableCopy];
                }
                else {
                    
                }
                [self.tableView reloadData];
            }
            
        }showIndicator:NO];
    }
}

- (void)deleteAddressRequestWithAddressID:(NSString *)addressID
{
    NSDictionary *param = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID,@"addressId":addressID};
    [[PMNetworking defaultNetworking] request:PMRequestStateMyDelAddress WithParameters:param callBackBlock:^(NSDictionary *dic) {
        
    }showIndicator:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 隐藏辅助视图
- (void)hideAccessory
{
    _showAccessory = NO;
}



@end
