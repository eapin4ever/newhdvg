//
//  DistrubitionBackground.m
//  changeViewController
//
//  Created by pmit on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "DistrubitionBackground.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageEditViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface DistrubitionBackground () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,ImageEditViewControllerDelegate,MBProgressHUDDelegate>

@property (strong,nonatomic) UIButton *currentSeletedBtn;
@property (copy,nonatomic) NSString *imageUrlString;
@property (assign,nonatomic) NSInteger defaultIndex;
@property (strong,nonatomic) MBProgressHUD *hud;
@property (strong,nonatomic) NSData *imageData;

@end

@implementation DistrubitionBackground

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"店铺背景选择";
    self.view.backgroundColor = HDVGPageBGGray;
    [self getSeletedOne];
    [self buildMyPhotoView];
    [self buildImageChooseView];
    [self buildNavigaitonBarButton];
    
}

- (void)buildHUD
{
    if (!self.hud)
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.delegate = self;
        self.hud.color = [UIColor blackColor];
        self.hud.labelText = @"努力上传中,请稍等片刻";
        self.hud.alpha = 0.8;
        self.hud.dimBackground = YES;
    }
    [self.hud show:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    self.backgroundIVArr = nil;
    self.shopDic = nil;
    self.imageData = nil;
}

- (void)buildNavigaitonBarButton
{
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(savePicture)];
    self.navigationItem.rightBarButtonItem = saveBtn;
}

- (void)buildMyPhotoView
{
    UIView *myPhotoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 100)];
    myPhotoView.backgroundColor = [UIColor whiteColor];
    UILabel *albumLB = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, WIDTH - 40, 50)];
    albumLB.font = [UIFont systemFontOfSize:15.0f];
    albumLB.text = @"从相册中选择";
    [myPhotoView addSubview:albumLB];
    
    UIImageView *arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 15, 20, 20)];
    arrowIV.contentMode = UIViewContentModeScaleAspectFit;
    arrowIV.image = [UIImage imageNamed:@"arrowRight"];
    [myPhotoView addSubview:arrowIV];
    
    UIButton *albumBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    albumBtn.frame = CGRectMake(0, 0, WIDTH, 50);
    albumBtn.tag = 10000;
    [albumBtn addTarget:self action:@selector(goToMyPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [myPhotoView addSubview:albumBtn];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(231, 231, 231, 1).CGColor;
    line.frame = CGRectMake(WidthRate(26), 50, WIDTH, 0.8);
    [myPhotoView.layer addSublayer:line];
    
    UILabel *takePhotoLB = [[UILabel alloc] initWithFrame:CGRectMake(20, 50, WIDTH - 40, 50)];
    takePhotoLB.font = [UIFont systemFontOfSize:15.0f];
    takePhotoLB.text = @"拍照";
    [myPhotoView addSubview:takePhotoLB];
    
    UIImageView *takeArrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 65, 20, 20)];
    takeArrowIV.contentMode = UIViewContentModeScaleAspectFit;
    takeArrowIV.image = [UIImage imageNamed:@"arrowRight"];
    [myPhotoView addSubview:takeArrowIV];
    
    UIButton *takeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    takeBtn.frame = CGRectMake(0, 50, WIDTH, 50);
    takeBtn.tag = 10001;
    [takeBtn addTarget:self action:@selector(goToMyPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [myPhotoView addSubview:takeBtn];
    
    [self.view addSubview:myPhotoView];
}

- (void)buildImageChooseView
{
    UIView *chooseIVView = [[UIView alloc] initWithFrame:CGRectMake(10, 120 , WIDTH - 20, HEIGHT - 64 - 140)];
    chooseIVView.backgroundColor = [UIColor whiteColor];
    chooseIVView.layer.cornerRadius = 3.0f;
    [self.view addSubview:chooseIVView];
    
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, WIDTH - 20 - 10, 30)];
    titleLB.font = [UIFont systemFontOfSize:18.0f];
    titleLB.text = @"从精选图库中选择";
    [chooseIVView addSubview:titleLB];
    
    UIScrollView *imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(5, 30, WIDTH - 20 - 10, chooseIVView.bounds.size.height - 30)];
    imageScrollView.showsVerticalScrollIndicator = NO;
    NSLog(@"arr --> %@",self.backgroundIVArr);
    CGFloat bgWidth = imageScrollView.bounds.size.width - 10;
    CGFloat bgHeight = imageScrollView.bounds.size.width / 3;
    for (NSInteger i = 0; i < self.backgroundIVArr.count; i++)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImageView *bgIV = [[UIImageView alloc] init];
        NSInteger heightIndex = i;
        bgIV.frame = CGRectMake(5, heightIndex * bgHeight + (i + 1) * 5, bgWidth, bgHeight);
        button.frame = CGRectMake(3, heightIndex * bgHeight + (i + 1) * 5 - 2, bgWidth + 4, bgHeight + 4);
        [bgIV sd_setImageWithURL:[NSURL URLWithString:[self.backgroundIVArr[i] objectAtIndex:1]]];
        button.tag = i;
        
        UIView *seletedView = [[UIView alloc] initWithFrame:CGRectMake(button.bounds.size.width - 20, 0, 20, 20)];
        seletedView.backgroundColor = [UIColor clearColor];
        UIImageView *seletedIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        seletedView.tag = 10000;
        seletedIV.image = [UIImage imageNamed:@"selectAll_1"];
        [seletedView addSubview:seletedIV];
        [button addSubview:seletedView];
        seletedView.hidden = YES;
        if (i == self.defaultIndex)
        {
            self.currentSeletedBtn = button;
            self.currentSeletedBtn.layer.borderColor = RGBA(28, 126, 251, 1).CGColor;
            self.currentSeletedBtn.layer.borderWidth = 1;
            seletedView.hidden = NO;
        }
        [button addTarget:self action:@selector(choosePicture:) forControlEvents:UIControlEventTouchUpInside];
        [imageScrollView addSubview:bgIV];
        [imageScrollView addSubview:button];
    }
    imageScrollView.contentSize = CGSizeMake(0,self.backgroundIVArr.count * (bgHeight + 5));
    [chooseIVView addSubview:imageScrollView];

    
}

- (void)goToMyPhoto:(UIButton *)sender
{
    if (sender.tag == 10000)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        //设置选择后的图片可被编辑
        picker.allowsEditing = NO;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            //设置拍照后的图片可被编辑
            picker.allowsEditing = NO;
            picker.sourceType = sourceType;
            [self presentViewController:picker animated:YES completion:nil];
        }else
        {
            NSLog(@"模拟其中无法打开照相机,请在真机中使用");
        }
    }
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
    NSString *type = [info objectNullForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"])
    {
        //先把图片转成NSData
        UIImage* image = [info objectNullForKey:@"UIImagePickerControllerOriginalImage"];
        NSData *data;
        if (UIImagePNGRepresentation(image) == nil)
        {
            data = UIImageJPEGRepresentation(image, 1.0);
        }
        else
        {
            data = UIImagePNGRepresentation(image);
        }
        
//        [self uploadImageView:data];
        ImageEditViewController *ieVC = [[ImageEditViewController alloc] initWithOriginalImage:image];
        ieVC.editDelegate = self;
        [self.navigationController pushViewController:ieVC animated:YES];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)choosePicture:(UIButton *)sender
{
    if (sender != self.currentSeletedBtn)
    {
        UIView *oldSeletedIV = (UIView *)[self.currentSeletedBtn viewWithTag:10000];
        oldSeletedIV.hidden = YES;
        self.currentSeletedBtn.layer.borderColor = [UIColor clearColor].CGColor;
        self.currentSeletedBtn.layer.borderWidth = 0;
        self.currentSeletedBtn = sender;
        UIView *newsSeletedIV = (UIView *)[self.currentSeletedBtn viewWithTag:10000];
        newsSeletedIV.hidden = NO;
        self.currentSeletedBtn.layer.borderColor = RGBA(28, 126, 251, 1).CGColor;
        self.currentSeletedBtn.layer.borderWidth = 1;
    }
}

- (void)savePicture
{
    [self savePictureToHost];
}

- (void)savePictureToHost
{
    NSString *logoString = [self.backgroundIVArr[self.currentSeletedBtn.tag] objectAtIndex:0];
    [[PMNetworking defaultNetworking] request:PMRequestStateUpdateShopHome WithParameters:@{PMSID,@"id":self.shopId,@"shopPhoto":logoString} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.imageUrlString = [self.backgroundIVArr[self.currentSeletedBtn.tag] objectAtIndex:1];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:[dic objectNullForKey:@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        
    } showIndicator:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if ([_bgDelegate respondsToSelector:@selector(changeBgPhoto:AndData:)])
        {
            [_bgDelegate changeBgPhoto:self.imageUrlString AndData:self.imageData];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)uploadImageView:(NSData *)imageData
{
    [self buildHUD];
    self.navigationItem.backBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    NSDictionary *param = @{PMSID,@"flag":@"shopPhoto"};
    //上传头像
    [[PMNetworking defaultNetworking] uploadImage:param imageData:imageData success:^(NSDictionary *dic) {
        if (intSuccess == 1) {
            [self.hud hide:YES];
            self.navigationItem.backBarButtonItem.enabled = YES;
            self.navigationItem.rightBarButtonItem.enabled = YES;
            NSString *imageString = [dic objectNullForKey:@"data"];
            self.imageUrlString = imageString;
            self.imageUrlString = [self.backgroundIVArr[self.currentSeletedBtn.tag] objectAtIndex:1];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"上传成功!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

- (void)getSeletedOne
{
    if ([self.bgUrlString isKindOfClass:[NSString class]] && self.bgUrlString && ![self.bgUrlString isEqualToString:@""])
    {
        NSString *bgString = [[self.bgUrlString componentsSeparatedByString:@"/"] lastObject];
        for (NSInteger i = 0; i < self.backgroundIVArr.count; i++)
        {
            NSString *backUrlString = [[[self.backgroundIVArr[i] objectAtIndex:1] componentsSeparatedByString:@"/"] lastObject];
            if ([bgString isEqualToString:backUrlString])
            {
                self.defaultIndex = i;
                break;
            }
        }
    }
    else if ([self.shopDic isKindOfClass:[NSDictionary class]] && [[self.shopDic objectNullForKey:@"shopPhoto"] isKindOfClass:[NSString class]] && [self.shopDic objectNullForKey:@"shopPhoto"] && ![[self.shopDic objectNullForKey:@"shopPhoto"] isEqualToString:@""])
    {
        NSString *bgString = [[[self.shopDic objectNullForKey:@"shopPhoto"] componentsSeparatedByString:@"/"] lastObject];
        for (NSInteger i = 0; i < self.backgroundIVArr.count; i++)
        {
            NSString *backUrlString = [[[self.backgroundIVArr[i] objectAtIndex:1] componentsSeparatedByString:@"/"] lastObject];
            if ([bgString isEqualToString:backUrlString])
            {
                self.defaultIndex = i;
                break;
            }
        }
    }
    else
    {
        self.defaultIndex = 0;
    }
}

- (void)passImageData:(NSData *)imageData
{
    self.imageData = imageData;
    [self uploadImageView:imageData];
}

@end
