//
//  PMAttenionGoodsCell.m
//  changeViewController
//
//  Created by pmit on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMAttenionGoodsCell.h"
#define grayColor RGBA(123, 123, 123, 1)
#define itemX WidthRate(240)
#define itemY HeightRate(40)
#define itemWidth WidthRate(450)
#define itemHeight HeightRate(70)

@implementation PMAttenionGoodsCell
{

    UILabel *titleLB;
    UILabel *distanceLB;
    UILabel *priceLB;
}

- (void)createUI
{
    if(!_iv)
    {
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(30), HeightRate(180), HeightRate(180))];
//        [_iv.layer setCornerRadius:6.0];
//        [_iv.layer setBorderColor: RGBA(223, 223, 223, 1).CGColor];
//        [_iv.layer setMasksToBounds:YES];
//        [_iv.layer setBorderWidth:0.5];
        _iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv];
        
        titleLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, itemY, WidthRate(450), HeightRate(80))];
        titleLB.textColor = grayColor;
        titleLB.numberOfLines = 2;
        titleLB.font = [UIFont systemFontOfSize:HeightRate(30)];
        [self.contentView addSubview:titleLB];
        
        distanceLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX + WidthRate(40), HeightRate(148), WidthRate(160), HeightRate(50))];
        distanceLB.textColor = grayColor;
        distanceLB.font = [UIFont systemFontOfSize:HeightRate(28)];
        distanceLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:distanceLB];
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(itemX, HeightRate(148), HeightRate(36), HeightRate(36))];
        icon.image = [UIImage imageNamed:@"location"];
        [self.contentView addSubview:icon];
        
        //隐藏
        icon.hidden = YES;
        distanceLB.hidden = YES;
        
        priceLB = [[UILabel alloc] initWithFrame:CGRectMake(itemX, HeightRate(148), WidthRate(180), HeightRate(50))];
        priceLB.textColor = HDVGRed;
        priceLB.textAlignment = NSTextAlignmentLeft;
        priceLB.font = [UIFont systemFontOfSize:HeightRate(28)];
        [self.contentView addSubview:priceLB];
        
        //使选择时背景色透明
        self.selectedBackgroundView = [[UIView alloc] init];
    }
    
}

- (void)setContentTitle:(NSString *)title classify:(NSString *)classify distance:(NSString *)distance price:(NSString *)price
{
    titleLB.text = [NSString stringWithFormat:@"%@  %@",title,classify];
//    distanceLB.text = distance; 不显示距离
    priceLB.text = [NSString stringWithFormat:@"￥%@",price];
}

//更改删除按钮的颜色
- (void)layoutSubviews
{
    [super layoutSubviews];
    for (UIView *subView in self.subviews) {
        if ([NSStringFromClass([subView class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"]) {
            ((UIView *)[subView.subviews firstObject]).backgroundColor = HDVGRed;
        }
    }
}

@end
