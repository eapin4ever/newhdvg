//
//  detailBtn.m
//  changeViewController
//
//  Created by ZhangEapin on 15/4/22.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "detailBtn.h"

@implementation detailBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return self;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 0, contentRect.size.width, contentRect.size.height * 0.7);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, contentRect.size.height * 0.7, contentRect.size.width, contentRect.size.height * 0.3);
}

@end
