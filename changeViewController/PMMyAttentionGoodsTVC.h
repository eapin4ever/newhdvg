//
//  PMMyAttentionGoodsTVC.h
//  changeViewController
//
//  Created by pmit on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMAttenionGoodsCell.h"

@interface PMMyAttentionGoodsTVC : UITableViewController
@property(nonatomic,strong)NSMutableArray *dicArray;

+ (PMMyAttentionGoodsTVC *)currentVC;
- (void)deleteSelectedRows;

@end
