//
//  RegisterPassViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/8.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "RegisterPassViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMToastHint.h"
#import "PMNetworking.h"
#import "MD5Util.h"

@interface RegisterPassViewController () <UITextFieldDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) UITextField *passTextField;
@property (strong,nonatomic) UITextField *rePassTextField;
@property (strong,nonatomic) UIView *passSettingView;
@property (strong,nonatomic) UIButton *registerBtn;
@property (strong,nonatomic) PMNetworking *networking;

@end

@implementation RegisterPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置密码";
    self.view.backgroundColor = HDVGPageBGGray;
    self.networking = [PMNetworking defaultNetworking];
    [self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)createUI
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyBoard:)];
    [self.view addGestureRecognizer:tap];
    
    self.passSettingView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 88)];
    self.passSettingView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.passSettingView];
    
    CALayer *topLine = [CALayer layer];
    topLine.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    topLine.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    [self.passSettingView.layer addSublayer:topLine];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    line.frame = CGRectMake(WidthRate(26), 44, WIDTH, HeightRate(1));
    [self.passSettingView.layer addSublayer:line];
    
    CALayer *bottomLine = [CALayer layer];
    bottomLine.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    bottomLine.frame = CGRectMake(0, 88 - 0.5, WIDTH, HeightRate(1));
    [self.passSettingView.layer addSublayer:bottomLine];
    
    UILabel *passLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 7, WidthRate(220), 30)];
    passLB.textColor = HDVGFontColor;
    passLB.text = @"密          码:";
    passLB.textAlignment = NSTextAlignmentLeft;
    passLB.font = [UIFont systemFontOfSize:15.0f];
    [self.passSettingView addSubview:passLB];
    
    self.passTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(260), 7, WIDTH - WidthRate(270) - WidthRate(40), 30)];
    self.passTextField.borderStyle = UITextBorderStyleNone;
    self.passTextField.delegate = self;
    self.passTextField.tag = 1;
    self.passTextField.textAlignment = NSTextAlignmentLeft;
    self.passTextField.font = [UIFont systemFontOfSize:14.0f];
    self.passTextField.placeholder = @"请输入密码";
    self.passTextField.returnKeyType = UIReturnKeyDone;
    self.passTextField.keyboardType = UIReturnKeyDefault;
    self.passTextField.secureTextEntry = YES;
    [self.passSettingView addSubview:self.passTextField];
    
    UILabel *rePassLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(46), 51, WidthRate(220), 30)];
    rePassLB.textColor = HDVGFontColor;
    rePassLB.text = @"确 认 密 码:";
    rePassLB.textAlignment = NSTextAlignmentLeft;
    rePassLB.font = [UIFont systemFontOfSize:15.0f];
    [self.passSettingView addSubview:rePassLB];
    
    self.rePassTextField = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(260), 51, WIDTH - WidthRate(270) - WidthRate(40), 30)];
    self.rePassTextField.borderStyle = UITextBorderStyleNone;
    self.rePassTextField.delegate = self;
    self.rePassTextField.tag = 2;
    self.rePassTextField.textAlignment = NSTextAlignmentLeft;
    self.rePassTextField.font = [UIFont systemFontOfSize:14.0f];
    self.rePassTextField.placeholder = @"请再次输入密码";
    self.rePassTextField.returnKeyType = UIReturnKeyDone;
    self.rePassTextField.keyboardType = UIReturnKeyDefault;
    self.rePassTextField.secureTextEntry = YES;
    [self.passSettingView addSubview:self.rePassTextField];
    
    self.registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.registerBtn.frame = CGRectMake(WidthRate(46), self.passSettingView.frame.origin.y + 88 + HeightRate(120), WIDTH - WidthRate(46) * 2, 44);
    [self.registerBtn setTitle:@"注册" forState:UIControlStateNormal];
    [self.registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.registerBtn addTarget:self action:@selector(registerMyCount:) forControlEvents:UIControlEventTouchUpInside];
    [self.registerBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [self.registerBtn setBackgroundColor:ButtonBgColor];
    [self.registerBtn.layer setCornerRadius:6.0f];
    [self.view addSubview:self.registerBtn];
    
//    初始化显示密码按钮
    UIButton *showButton = [UIButton buttonWithType:UIButtonTypeCustom];
    showButton.frame = CGRectMake(WIDTH - WidthRate(230), self.passSettingView.frame.origin.y + 90 + HeightRate(10), WidthRate(190), 18);
    [showButton setImage:[UIImage imageNamed:@"showpass_no.png"] forState:UIControlStateNormal];
    [showButton setImage:[UIImage imageNamed:@"showpass_yes.png"] forState:UIControlStateSelected];
    [showButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [showButton.titleLabel setFont:[UIFont systemFontOfSize:HeightRate(30)]];
    [showButton setTitle:@"显示密码" forState:UIControlStateNormal];
    [showButton setTitleColor:HDVGFontColor forState:UIControlStateNormal];
    [showButton addTarget:self action:@selector(showPassButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:showButton];

}

- (void)registerMyCount:(UIButton *)sender
{
    if ([self.passTextField.text isEqualToString:@""] || [self.rePassTextField.text isEqualToString:@""])
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入你要设置的密码"];
    }
    else if (self.passTextField.text.length < 6 && self.passTextField.text.length > 16 )
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"长度为6~16位数字/字母/符号"];
    }
    else if (![self.passTextField.text isEqualToString:self.rePassTextField.text])
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"两次输入的密码不一致"];
    }
    else
    {
        NSDictionary *nameAndPassString = @{@"name":self.userName,@"mobile":self.phoneNum,@"password":[MD5Util md5:self.passTextField.text]};
        
        __block NSDictionary *callBackDict;
        
        [self.networking request:PMRequestStateRegister WithParameters:nameAndPassString callBackBlock:^(NSDictionary *dict) {
            
            callBackDict = dict;
            
            // 如果返回的信息中，success的值是true则注册成功
            if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"注册成功，请登录" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 1;
                [alertView show];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
        }showIndicator:NO];
    }
}

- (void)resignKeyBoard:(UITapGestureRecognizer *)tap
{
    [self.passTextField resignFirstResponder];
    [self.rePassTextField resignFirstResponder];
}

- (void)showPassButtonClick:(UIButton *)sender
{
    // 改变按钮的选中状态
    sender.selected = !sender.selected;
    if (sender.selected)
    {
        self.passTextField.secureTextEntry = NO;
        self.rePassTextField.secureTextEntry = NO;
    }
    else
    {
        self.passTextField.secureTextEntry = YES;
        self.rePassTextField.secureTextEntry = YES;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
