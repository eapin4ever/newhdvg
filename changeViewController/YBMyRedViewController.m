//
//  YBMyRedViewController.m
//  changeViewController
//
//  Created by EapinZhang on 15/4/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBMyRedViewController.h"
#import "YBDatePickerView.h"
#import "PMMyPhoneInfo.h"
#import "YBDetailCell.h"
#import "JHRefresh.h"
#import "MasterViewController.h"

@interface YBMyRedViewController () <UITableViewDataSource,UITableViewDelegate,YBDatePickerViewDelegate>

@property (strong,nonatomic) UITableView *detailTableView;
@property (strong,nonatomic) UIView *pickView;
@property (strong,nonatomic) UIView *shadowView;
@property (assign,nonatomic) NSInteger year;
@property (assign,nonatomic) NSInteger month;
@property (strong,nonatomic) UILabel *yLB;
@property (strong,nonatomic) UILabel *monLB;
@property (strong,nonatomic) UILabel *oMoneyLB;
@property (strong,nonatomic) UILabel *iMoneyLB;
@property (strong,nonatomic) UILabel *allMoneyLB;
@property (strong,nonatomic) NSMutableArray *redArray;
@property (strong,nonatomic) UILabel *timeLabel;
@property (strong,nonatomic) UILabel *orderMoneyLab;
@property (strong,nonatomic) UILabel *stateLabel;
@property (strong,nonatomic) UIView *noDataView;
@property (strong,nonatomic) UIView *hasDataView;
@property (assign,nonatomic) BOOL isPush;

@end

@implementation YBMyRedViewController

static NSString *_cellIdentifier = @"redCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"红包余额";
    if ([self.navigationController.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
        NSArray *list=self.navigationController.navigationBar.subviews;
        for (id obj in list) {
            if ([obj isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView=(UIImageView *)obj;
                NSArray *list2=imageView.subviews;
                for (id obj2 in list2) {
                    if ([obj2 isKindOfClass:[UIImageView class]]) {
                        UIImageView *imageView2=(UIImageView *)obj2;
                        imageView2.hidden=YES;
                    }
                }
            }
        }
    }
    self.redArray = [NSMutableArray array];
    //[self initFake];
    [self createUI];
    [self createYMSelected];
    [self getRedData:[NSString stringWithFormat:@"%ld",(long)self.year] AndMon:[NSString stringWithFormat:@"%ld",(long)self.month]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 创建表格和表头
- (void)createUI
{
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = RGBA(236, 236, 236, 1);
    self.detailTableView = tableView;
    [tableView registerClass:[YBDetailCell class] forCellReuseIdentifier:_cellIdentifier];
    [self.view addSubview:tableView];
    
    //通过月份查看收支明细
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    headView.backgroundColor = RGBA(236, 236, 236, 1);
    tableView.tableHeaderView = headView;
    
    
    //获取当前时间
    NSDate *nowDate = [NSDate new];
    NSCalendar *calendar =  [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:nowDate];
    NSInteger year = [dateComponent year];
    NSInteger month = [dateComponent month];
    self.year = year;
    self.month = month;
    
    //年Label
    UILabel *yearLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), 5, WIDTH / 6, 15)];
    yearLB.text = [NSString stringWithFormat:@"%ld年",(long)year];
    yearLB.font = [UIFont systemFontOfSize:14.0f];
    yearLB.textColor = HDVGFontColor;
    self.yLB = yearLB;
    [headView addSubview:yearLB];
    
    UILabel *monthLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), 20, WidthRate(80), 35)];
    NSString *monthString = @"";
    if (month < 10)
    {
        monthString = [NSString stringWithFormat:@"0%ld",(long)month];
    }
    else
    {
        monthString = [NSString stringWithFormat:@"%ld",(long)month];
    }
    monthLB.text = monthString;
    monthLB.font = [UIFont systemFontOfSize:23.0f];
    monthLB.textColor = HDVGFontColor;
    self.monLB = monthLB;
    [headView addSubview:monthLB];
    
    UILabel *monthTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(170), 25, WidthRate(30), 35)];
    monthTitleLB.text = @"月";
    monthTitleLB.font = [UIFont systemFontOfSize:13.0f];
    monthTitleLB.textColor = HDVGFontColor;
    [headView addSubview:monthTitleLB];
    
    UIImageView *arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(140), 38, 10, 10)];
    arrowIV.image = [UIImage imageNamed:@"下拉"];
    arrowIV.contentMode = UIViewContentModeScaleAspectFit;
    [headView addSubview:arrowIV];
    
    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(WIDTH / 3 - WidthRate(80), 10, 1, 40);
    line.backgroundColor = [UIColor whiteColor].CGColor;
    [headView.layer addSublayer:line];
    
    //选择月份
    UIButton *monthButton = [UIButton buttonWithType:UIButtonTypeCustom];
    monthButton.frame = CGRectMake(0, 0, WIDTH / 3 - WidthRate(80), headView.bounds.size.height);
    monthButton.tag = 1;
    [monthButton addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:monthButton];
    
    UILabel *outLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(75), 5, WIDTH / 4, 15)];
    outLB.text = @"月支出(元)";
    outLB.font = [UIFont systemFontOfSize:14.0f];
    outLB.textColor = HDVGFontColor;
    outLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:outLB];
    
    UILabel *outMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(75), 20, WIDTH / 4, 40)];
    outMoneyLB.text = @"0.00";
    outMoneyLB.font = [UIFont systemFontOfSize:15.0f];
    outMoneyLB.textColor = HDVGFontColor;
    outMoneyLB.textAlignment = NSTextAlignmentCenter;
    self.oMoneyLB = outMoneyLB;
    [headView addSubview:outMoneyLB];
    
    UILabel *inLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(5) + outLB.frame.origin.x + outLB.bounds.size.width, 5, WIDTH / 4, 15)];
    inLB.text = @"月收入(元)";
    inLB.font = [UIFont systemFontOfSize:14.0f];
    inLB.textColor = HDVGFontColor;
    inLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:inLB];
    
    UILabel *inMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(outLB.frame.origin.x + outLB.bounds.size.width + WidthRate(5), 20, WIDTH / 4, 40)];
    inMoneyLB.text = @"0.00";
    inMoneyLB.font = [UIFont systemFontOfSize:15.0f];
    inMoneyLB.textColor = HDVGFontColor;
    inMoneyLB.textAlignment = NSTextAlignmentCenter;
    self.iMoneyLB = inMoneyLB;
    [headView addSubview:inMoneyLB];
    
    UILabel *allLB = [[UILabel alloc] initWithFrame:CGRectMake(inLB.frame.origin.x + inLB.bounds.size.width + WidthRate(5), 5, WIDTH / 4, 15)];
    allLB.text = @"总余额(元)";
    allLB.font = [UIFont systemFontOfSize:14.0f];
    allLB.textColor = HDVGFontColor;
    allLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:allLB];
    
    UILabel *allMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(inLB.frame.origin.x + inLB.bounds.size.width + WidthRate(5), 20, WIDTH / 4, 40)];
    allMoneyLB.text = @"0.00";
    allMoneyLB.font = [UIFont systemFontOfSize:15.0f];
    allMoneyLB.textColor = HDVGFontColor;
    allMoneyLB.textAlignment = NSTextAlignmentCenter;
    self.allMoneyLB = allMoneyLB;
    [headView addSubview:allMoneyLB];
    
    
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 300)];
    shadowView.backgroundColor = [UIColor lightGrayColor];
    shadowView.alpha = 0.5;
    shadowView.hidden = YES;
    self.shadowView = shadowView;
    [self.view addSubview:shadowView];
    
    //    UIView *typeView = [[UIView alloc] initWithFrame:CGRectMake(0, 60, WIDTH, 40)];
    //    UISegmentedControl *segControl = [[UISegmentedControl alloc] initWithItems:@[@"收支明细",@"提现明细"]];
    //    typeView.backgroundColor = [UIColor whiteColor];
    //    segControl.frame = CGRectMake(10, 5, typeView.bounds.size.width - 20, 30);
    //    segControl.segmentedControlStyle = UISegmentedControlStyleBordered;
    //    [typeView addSubview:segControl];
    //    segControl.selectedSegmentIndex = 0;
    //    [headView addSubview:typeView];
    
//    UIView *footView = [[UIView alloc] init];
//    footView.backgroundColor = [UIColor whiteColor];
//    tableView.tableFooterView = footView;
    
    
    
    //下拉刷新
    __weak YBMyRedViewController *weakSelf = self;
    
    [tableView addRefreshHeaderViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        
        //请求数据
        //添加行
        
        //事情做完了别忘了结束刷新动画~~~
        [weakSelf.detailTableView headerEndRefreshingWithResult:JHRefreshResultSuccess];
    }];
}

#pragma mark - 年月选择器
- (void)createYMSelected
{
    YBDatePickerView *datePicker = [[YBDatePickerView alloc] init];
    datePicker.myDelegate = self;
    datePicker.nowYear = [NSString stringWithFormat:@"%ld",(long)self.year];
    datePicker.nowMonth = [NSString stringWithFormat:@"%ld",(long)self.month];
    [datePicker createUIWithFrame:CGRectMake(0, HEIGHT, WIDTH, 300)];
    self.pickView = datePicker;
    [self.view addSubview:datePicker];
}


#pragma mark - 点击事件
- (void)clickBtn:(UIButton *)btn
{
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT - 300, WIDTH, 300);
    }];
}

- (void)surePickerView:(NSString *)selectedYear And:(NSString *)selectedMonth
{
    self.yLB.text = selectedYear;
    self.monLB.text = selectedMonth;
    
    [self getRedData:selectedYear AndMon:selectedMonth];
    
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

////初始化假数据
//- (void)initFake
//{
//    NSDictionary *dic1 = @{@"date":@"2015-1-1",@"content":@"抢红包",@"money":@"+ ￥10.00"};
//    NSDictionary *dic2 = @{@"date":@"2015-1-2",@"content":@"抢红包",@"money":@"+ ￥10.00"};
//    NSDictionary *dic3 = @{@"date":@"2015-1-3",@"content":@"使用红包",@"money":@"- ￥20.00"};
//    NSDictionary *dic4 = @{@"date":@"2015-1-4",@"content":@"抢红包",@"money":@"+ ￥10.00"};
//    [self.redArray addObject:dic1];
//    [self.redArray addObject:dic2];
//    [self.redArray addObject:dic3];
//    [self.redArray addObject:dic4];
//}

- (void)cancelPickerView
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.redArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YBDetailCell *cell = [self.detailTableView dequeueReusableCellWithIdentifier:_cellIdentifier];
    [cell createUI];
    
    [cell setDetailDic:self.redArray[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(100);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *typeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(100))];
    typeView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(0, HeightRate(100) - 1, WIDTH, 1);
    line.backgroundColor = RGBA(236, 236, 236, 1).CGColor;
    [typeView.layer addSublayer:line];
    
    // 创建时间
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), HeightRate(25), WidthRate(230), HeightRate(50))];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.text = @"时间";
    self.timeLabel.textColor = RGBA(65, 65, 65, 1);
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.font = [UIFont systemFontOfSize:13.0f];
    [typeView addSubview:self.timeLabel];
    
    // 提款金额
    self.orderMoneyLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(250), HeightRate(25), WidthRate(230), HeightRate(50))];
    self.orderMoneyLab.backgroundColor = [UIColor clearColor];
    self.orderMoneyLab.text = @"项目";
    self.orderMoneyLab.textColor = RGBA(65, 65, 65, 1);
    self.orderMoneyLab.textAlignment = NSTextAlignmentCenter;
    self.orderMoneyLab.font = [UIFont systemFontOfSize:13.0f];
    [typeView addSubview:self.orderMoneyLab];
    
    // 交易状态
    self.stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(500), HeightRate(25), WidthRate(230), HeightRate(50))];
    self.stateLabel.backgroundColor = [UIColor clearColor];
    self.stateLabel.text = @"金额(元)";
    self.stateLabel.textColor = RGBA(65, 65, 65, 1);
    self.stateLabel.textAlignment = NSTextAlignmentCenter;
    self.stateLabel.font = [UIFont systemFontOfSize:13.0f];
    [typeView addSubview:self.stateLabel];
    
    return typeView;
}

#pragma mark - 请求网络获取红包详细情况
- (void)getRedData:(NSString *)year AndMon:(NSString *)month
{
    self.redArray = [NSMutableArray array];
    self.iMoneyLB.text = @"0.00";
    self.oMoneyLB.text = @"0.00";
    PMNetworking *netWorking = [PMNetworking defaultNetworking];
    NSDictionary *parameter = @{@"year":year,@"month":month,@"PM_SID":[PMUserInfos shareUserInfo].PM_SID};
    [netWorking request:PMRequestStateGetOptTradeDetail WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
        if ([[dic objectNullForKey:@"success"] boolValue] == 1)
        {
            double getsMoney = 0;
            double lostMoney = 0;
            double myHdMoney = 0;
//            self.redArray = [dic objectNullForKey:@"data"];
            
            NSDictionary *dataDic = [dic objectNullForKey:@"data"];
            self.redArray = [dataDic objectForKey:@"detail"];
            NSDictionary *totalDic = [dataDic objectNullForKey:@"total"];
            getsMoney = [[totalDic objectForKey:@"monthInMoney"] doubleValue];
            lostMoney = [[totalDic objectForKey:@"monthOutMoney"] doubleValue];
            myHdMoney = [[totalDic objectForKey:@"myHdMoney"] doubleValue];
            
            self.iMoneyLB.text = [NSString stringWithFormat:@"%.2lf",getsMoney];
            self.oMoneyLB.text = [NSString stringWithFormat:@"%.2lf",lostMoney];
            self.allMoneyLB.text = [NSString stringWithFormat:@"%.2lf",myHdMoney];
            [self.detailTableView reloadData];
            if (self.redArray.count == 0)
            {
                self.detailTableView.tableFooterView = nil;
                if (!self.noDataView) {
                    
                    UIView *dataFootView = [[UIView alloc] init];
                    dataFootView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64 - HeightRate(120) - 50);
                    dataFootView.backgroundColor = [UIColor whiteColor];
                    self.noDataView = dataFootView;
                    
                    UIImageView *noDataIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, dataFootView.frame.size.height)];
                    noDataIV.image = [UIImage imageNamed:@"red_null"];
                    noDataIV.contentMode = UIViewContentModeScaleAspectFit;
                    [dataFootView addSubview:noDataIV];
                    
                    self.detailTableView.scrollEnabled = NO;
                }
                
                self.detailTableView.tableFooterView = self.noDataView;
                [self.detailTableView reloadData];
            }
            else
            {
                self.detailTableView.tableFooterView = nil;
                if (!self.hasDataView)
                {
                    self.hasDataView = [[UIView alloc] init];
                }
                
                self.detailTableView.tableFooterView = self.hasDataView;
                self.detailTableView.scrollEnabled = YES;
                [self.detailTableView reloadData];
            }
        }
        else
        {
            self.detailTableView.tableFooterView = nil;
            if (!self.noDataView) {
                
                UIView *dataFootView = [[UIView alloc] init];
                dataFootView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64 - HeightRate(120) - 50);
                dataFootView.backgroundColor = [UIColor whiteColor];
                self.noDataView = dataFootView;
                
                UIImageView *noDataIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, dataFootView.frame.size.height)];
                noDataIV.image = [UIImage imageNamed:@"red_null"];
                noDataIV.contentMode = UIViewContentModeScaleAspectFit;
                [dataFootView addSubview:noDataIV];
                
                self.detailTableView.scrollEnabled = NO;
            }
            
            self.detailTableView.tableFooterView = self.noDataView;
            [self.detailTableView reloadData];
        }
        
    } showIndicator:YES];
}


@end
