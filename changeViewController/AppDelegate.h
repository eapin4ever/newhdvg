//
//  AppDelegate.h
//  changeViewController
//
//  Created by wallace on 14/11/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "WXApi.h"
#import "WXApiObject.h"
#import "WXApi.h"
#import "BPush.h"
#import "JSONKit.h"


typedef void(^finishAction)(void);
@protocol MyAppDelegate <NSObject>

- (void)successPay;
- (void)failPay:(int)errorCode;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate,BPushDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *appId;
@property (strong, nonatomic) NSString *channelId;
@property (strong, nonatomic) NSString *userId;

@property (assign,nonatomic) BOOL isWXPay;
@property (weak,nonatomic) id<MyAppDelegate> myDelegate;
@property (strong,nonatomic) UIView *noNetView;
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong,nonatomic) UIView *crashView;

+ (AppDelegate *)shareAppDelegate;
- (void)checkClientVersionNotUpdate:(finishAction)notUpdate;
- (void)bindBaiduyun;
- (BOOL)isNetConnect;
- (void)weixinPayWithRequsetData:(NSDictionary *)dict;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

