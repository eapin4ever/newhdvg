//
//  BindingBankcardViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/24.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "BindingBankcardViewController.h"
#import "BankcardViewController.h"

@interface BindingBankcardViewController ()

@end

@implementation BindingBankcardViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"选择开户银行";
        
        //self.view.backgroundColor = RGBA(245, 245, 245, 1);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //self.bankArray = [NSArray arrayWithObjects:@"中国工商银行",@"中国农业银行",@"中国建设银行",@"中国银行",@"中信银行",@"中信实业银行",@"招商银行",@"恒丰银行",@"广发银行",@"平安银行",@"光大银行",@"兴业银行",@"交通银行",@"民生银行",@"华夏银行",@"上海浦发银行",@"浙商银行",@"深圳发展银行",@"广州银行",@"北京银行",@"天津银行",@"上海银行",@"南京银行",@"宁波银行",@"温州银行",@"重庆银行",@"哈尔滨银行",@"青岛银行",@"中国农业发展银行",@"中国人民银行",@"中国邮政储蓄银行",@"中国民生银行",@"北京农村商业银行",@"广州农村商业银行",@"上海农村商业银行",@"深圳农村商业银行",@"常熟农村商业银行",@"杭州市商业银行",@"济南市商业银行",@"成都市商业银行",@"包头市商业银行",@"南昌市商业银行",@"贵阳商业银行",@"兰州市商业银行",@"浙商银行",@"徽商银行", nil];
    
    self.bankArray = [NSArray arrayWithObjects:@"中国工商银行",@"中国农业银行",@"中国建设银行",@"中国银行",@"中信银行",@"恒丰银行",@"广发银行",@"平安银行",@"光大银行",@"兴业银行",@"交通银行",@"民生银行",@"华夏银行",@"上海浦发银行",@"浙商银行", nil];
    
    // 创建选择银行卡 tableView
    self.bankTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.view.frame.size.height - 64) style:UITableViewStylePlain];
    self.bankTableView.delegate = self;
    self.bankTableView.dataSource = self;
    [self.view addSubview:self.bankTableView];
}

// 有多少个 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// 该组有多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.bankArray count];
}

// 每一行的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"bankCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    NSInteger row = [indexPath row];
    
    cell.textLabel.text = [self.bankArray objectAtIndex:row];
    cell.textLabel.textColor = RGBA(75, 75, 75, 1);
    cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
    
    return cell;
}

#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //self.bankcardVC.bankString = self.bankArray[indexPath.row];
    NSString *string = self.bankArray[indexPath.row];
    [[NSUserDefaults standardUserDefaults] setObject:string forKey:@"bankString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
