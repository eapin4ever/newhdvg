//
//  BankcardViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "BankcardViewController.h"
#import "BindingBankcardViewController.h"
#import "MobileAuthcodeViewController.h"
#import "PMToastHint.h"
#import <CardIO/CardIO.h>
#import <CardIO/CardIOPaymentViewControllerDelegate.h>

@interface BankcardViewController () <CardIOPaymentViewControllerDelegate>

@property (assign, nonatomic) BOOL isBinding;

@end

@implementation BankcardViewController
@synthesize bankString;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"银行卡号管理";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getModels];
    [self createChangeBankcardViewUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChange) name:UITextFieldTextDidChangeNotification object:self.bankcardTextField];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 获得开户银行
    bankString = [[NSUserDefaults standardUserDefaults] objectForKey:@"bankString"];
    self.bankTextField.text = bankString;
}

// 创建修改银行卡号
- (void)createChangeBankcardViewUI
{
    // 创建修改银行卡号 view
//    self.changeBankcardView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 160)];
    self.changeBankcardView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 200)];
    self.changeBankcardView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.changeBankcardView];
    
    // 分隔线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    line1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.changeBankcardView.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(WidthRate(26), 40, WIDTH, HeightRate(1));
    line2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.changeBankcardView.layer addSublayer:line2];
    
    CALayer *line3 = [[CALayer alloc] init];
    line3.frame = CGRectMake(WidthRate(26), 80, WIDTH, HeightRate(1));
    line3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.changeBankcardView.layer addSublayer:line3];
    
    CALayer *line4 = [[CALayer alloc] init];
    line4.frame = CGRectMake(WidthRate(26), 120, WIDTH, HeightRate(1));
    line4.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.changeBankcardView.layer addSublayer:line4];
    
    CALayer *line5 = [[CALayer alloc] init];
    line5.frame = CGRectMake(0, 160 - 0.5, WIDTH, 0.5);
    line5.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.changeBankcardView.layer addSublayer:line5];
    
    // 开户银行
    UILabel *bankLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(200), 30)];
    bankLabel.backgroundColor = [UIColor clearColor];
    bankLabel.text = @"开户银行";
    bankLabel.textColor = [UIColor darkGrayColor];
    bankLabel.textAlignment = NSTextAlignmentLeft;
    bankLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.changeBankcardView addSubview:bankLabel];
    
    self.bankTextField = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(430), 5, WidthRate(360), 30)];
    self.bankTextField.borderStyle = UITextBorderStyleNone;
    self.bankTextField.enabled = NO;
    self.bankTextField.delegate = self;
    self.bankTextField.placeholder = @"请选择开户银行";
    self.bankTextField.textAlignment = NSTextAlignmentRight;
    self.bankTextField.font = [UIFont systemFontOfSize:14.0f];
    self.bankTextField.textColor = [UIColor darkGrayColor];
    self.bankTextField.returnKeyType = UIReturnKeyDone;
    self.bankTextField.keyboardType = UIReturnKeyDefault;
    [self.changeBankcardView addSubview:self.bankTextField];
    
    // 创建选择开户银行按钮
    UIButton *openBankButton = [UIButton buttonWithType:UIButtonTypeCustom];
    openBankButton.frame = CGRectMake(0, 0, WIDTH, 40);
    [openBankButton addTarget:self action:@selector(changeBankcardViewClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.changeBankcardView addSubview:openBankButton];
    // 剪头图片
    UIImageView *arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 10, 20, 20)];
    [arrowImage setImage:[UIImage imageNamed:@"arrowRight"]];
    [self.changeBankcardView addSubview:arrowImage];
    
    // 银行卡号
    UILabel *bankcardLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 45, WidthRate(200), 30)];
    bankcardLabel.backgroundColor = [UIColor clearColor];
    bankcardLabel.text = @"银行卡号";
    bankcardLabel.textColor = [UIColor darkGrayColor];
    bankcardLabel.textAlignment = NSTextAlignmentLeft;
    bankcardLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.changeBankcardView addSubview:bankcardLabel];
    
    self.bankcardTextField = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(490), 45, WidthRate(420), 30)];
    self.bankcardTextField.borderStyle = UITextBorderStyleNone;
    self.bankcardTextField.delegate = self;
    self.bankcardTextField.tag = 1;
    self.bankcardTextField.placeholder = @"请输入银行卡号";
    self.bankcardTextField.textAlignment = NSTextAlignmentRight;
    self.bankcardTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.bankcardTextField.font = [UIFont systemFontOfSize:14.0f];
    self.bankcardTextField.textColor = [UIColor darkGrayColor];
    self.bankcardTextField.returnKeyType = UIReturnKeyDone;
    self.bankcardTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self.changeBankcardView addSubview:self.bankcardTextField];
    
    UILabel *bankcardDetailLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 85, WidthRate(200), 30)];
    bankcardDetailLB.backgroundColor = [UIColor clearColor];
    bankcardDetailLB.text = @"开户支行";
    bankcardDetailLB.textColor = [UIColor darkGrayColor];
    bankcardDetailLB.textAlignment = NSTextAlignmentLeft;
    bankcardDetailLB.font = [UIFont systemFontOfSize:15.0f];
    [self.changeBankcardView addSubview:bankcardDetailLB];
    
    self.bankDetailTF = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(490), 85, WidthRate(420), 30)];
    self.bankDetailTF.borderStyle = UITextBorderStyleNone;
    self.bankDetailTF.delegate = self;
    self.bankDetailTF.tag = 4;
    self.bankDetailTF.placeholder = @"请输入开户支行";
    self.bankDetailTF.textAlignment = NSTextAlignmentRight;
    self.bankDetailTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.bankDetailTF.font = [UIFont systemFontOfSize:14.0f];
    self.bankDetailTF.textColor = [UIColor darkGrayColor];
    self.bankDetailTF.returnKeyType = UIReturnKeyDone;
    [self.changeBankcardView addSubview:self.bankDetailTF];
    
//    UIButton *carBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [carBtn setBackgroundColor:[UIColor blueColor]];
//    [carBtn addTarget:self action:@selector(goToCarScan:) forControlEvents:UIControlEventTouchUpInside];
//    carBtn.frame = CGRectMake(WIDTH - 40, 45, 30, 30);
//    [self.changeBankcardView addSubview:carBtn];
    
    
    // 开户人姓名
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 125, WidthRate(200), 30)];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = @"真实姓名";
    nameLabel.textColor = [UIColor darkGrayColor];
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.changeBankcardView addSubview:nameLabel];
    
    self.userNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(430), 125, WidthRate(360), 30)];
    self.userNameTextField.borderStyle = UITextBorderStyleNone;
    self.userNameTextField.delegate = self;
    self.userNameTextField.tag = 2;
    self.userNameTextField.placeholder = @"请输入开户人姓名";
    self.userNameTextField.textAlignment = NSTextAlignmentRight;
    self.userNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.userNameTextField.font = [UIFont systemFontOfSize:14.0f];
    self.userNameTextField.textColor = HDVGFontColor;
    self.userNameTextField.returnKeyType = UIReturnKeyDone;
    self.userNameTextField.keyboardType = UIReturnKeyDefault;
    [self.changeBankcardView addSubview:self.userNameTextField];
    //self.userNameTextField.userInteractionEnabled = NO;

    
    // 开户人身份证号
    UILabel *userIDLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 165, WidthRate(200), 30)];
    userIDLabel.backgroundColor = [UIColor clearColor];
    userIDLabel.text = @"身份证号";
    userIDLabel.textColor = HDVGFontColor;
    userIDLabel.textAlignment = NSTextAlignmentLeft;
    userIDLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.changeBankcardView addSubview:userIDLabel];
    
    self.userIDTextField = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(490), 165, WidthRate(420), 30)];
    self.userIDTextField.borderStyle = UITextBorderStyleNone;
    self.userIDTextField.delegate = self;
    self.userIDTextField.tag = 3;
    self.userIDTextField.placeholder = @"请输入开户人身份证号";
    self.userIDTextField.textAlignment = NSTextAlignmentRight;
    self.userIDTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.userIDTextField.font = [UIFont systemFontOfSize:14.0f];
    self.userIDTextField.textColor = HDVGFontColor;
    self.userIDTextField.returnKeyType = UIReturnKeyDone;
    self.userIDTextField.keyboardType = UIReturnKeyDefault;
    [self.changeBankcardView addSubview:self.userIDTextField];
    //self.userIDTextField.userInteractionEnabled = NO;

    
    // 初始化完成修改按钮
    self.setBankcardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.setBankcardButton.frame = CGRectMake(WidthRate(36), self.changeBankcardView.frame.origin.y + 200 + HeightRate(120), WIDTH - WidthRate(36) * 2, 40);
    [self.setBankcardButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.setBankcardButton setBackgroundColor:ButtonBgColor];
    [self.setBankcardButton.layer setCornerRadius:6.0f];
    self.setBankcardButton.titleLabel.font = [UIFont boldSystemFontOfSize:ButtonFont];
    [self.view addSubview:self.setBankcardButton];
    
    //提示用户
    //注意：提现绑定的银行卡持卡人姓名须与代言用户姓名一致，否则将无法打款。
    UILabel *hintLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), self.changeBankcardView.frame.origin.y + 200 + HeightRate(5), WIDTH - WidthRate(72), HeightRate(80))];
    hintLB.text = @"注意：提现绑定的银行卡持卡人姓名须与代言用户姓名一致，否则将无法打款。";
    hintLB.font = [UIFont systemFontOfSize:11];
    hintLB.numberOfLines = 2;
    hintLB.textColor = HDVGRed;
    [self.view addSubview:hintLB];
}

// 回收软键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.bankTextField resignFirstResponder];
    [self.bankcardTextField resignFirstResponder];
    [self.userNameTextField resignFirstResponder];
    [self.userIDTextField resignFirstResponder];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.bankcardTextField.tag == 1 && textField == self.bankcardTextField && textField.text.length - range.length + string.length > 19) {
        return NO;
    }
    
    if (self.userNameTextField.tag == 2 && textField == self.userNameTextField && textField.text.length - range.length + string.length > 12) {
        return NO;
    }
    
    if (self.userIDTextField.tag == 3 && textField == self.userIDTextField && textField.text.length - range.length + string.length > 18) {
        return NO;
    }
    
    return YES;
}

#pragma mark - 选择开户银行按钮响应事件
- (void)changeBankcardViewClick:(id)sender
{
    if(_isBinding == NO)
    {
        BindingBankcardViewController *bindingBankcardVC = [[BindingBankcardViewController alloc] init];
        bindingBankcardVC.bankcardVC = self;
        [self.navigationController pushViewController:bindingBankcardVC animated:YES];
    }
}

#pragma mark - 完成修改按钮响应事件

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
//        if (_isPresented == YES && _isBinding == YES) {
//            [self dismissViewControllerAnimated:YES completion:^{
//                self.bankTextField.text = nil;
//                self.bankcardTextField.text = nil;
//            }];
//            
//            _isBinding = NO;
//        }
//        else {
//            [self.navigationController popViewControllerAnimated:YES];
//        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

// 检查银行卡是否已经绑定
- (void)getModels
{
    PMNetworking *nw = [PMNetworking defaultNetworking];
    
    NSDictionary *para = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID};
    
    if([PMUserInfos shareUserInfo].PM_SID == nil)
    {
        
    }
    
    [nw request:PMRequestStateShareCheckBank WithParameters:para callBackBlock:^(NSDictionary *dic) {
        //已绑定
        if ([[dic objectNullForKey:@"success"] integerValue] == 1)
        {
            if ([dic isKindOfClass:[NSDictionary class]])
            {
                self.bankCardDataDic = [dic objectNullForKey:@"data"];
                [self setBankData];
                
                _isBinding = YES;
                self.bankcardTextField.userInteractionEnabled = NO;
                self.userNameTextField.userInteractionEnabled = NO;
                self.userIDTextField.userInteractionEnabled = NO;
                self.bankDetailTF.userInteractionEnabled = NO;
                
                // 解绑银行卡
                [self.setBankcardButton setTitle:@"解绑银行卡" forState:UIControlStateNormal];
                [self.setBankcardButton addTarget:self action:@selector(unBankcardButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            }
            
        }
        else//未绑定
        {
            if (_isFromGet == NO) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"您还没有绑定银行卡，请绑定！" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                [alertView show];
            }
            
            self.bankCardDataDic = [dic objectNullForKey:@"data"];
            
            _isBinding = NO;
            
            [self setBankData];

            [self.setBankcardButton setTitle:@"绑定银行卡" forState:UIControlStateNormal];
            [self.setBankcardButton addTarget:self action:@selector(bindingBankcardButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }showIndicator:YES];
}

- (void)setBankData
{
    //用户名和身份证必须是和用户一致
    if ([[self.bankCardDataDic objectNullForKey:@"bankCard"] isKindOfClass:[NSString class]])
    {
        self.bankcardTextField.text = [self.bankCardDataDic objectNullForKey:@"bankCard"];
    }
    
    if ([[self.bankCardDataDic objectNullForKey:@"bank"] isKindOfClass:[NSString class]])
    {
        self.bankTextField.text = [self.bankCardDataDic objectNullForKey:@"bank"];
    }
    
    if ([[self.bankCardDataDic objectNullForKey:@"name"] isKindOfClass:[NSString class]])
    {
        self.userNameTextField.text = [self.bankCardDataDic objectNullForKey:@"name"];
    }
    
    if ([[self.bankCardDataDic objectNullForKey:@"userCard"] isKindOfClass:[NSString class]])
    {
        self.userIDTextField.text = [self.bankCardDataDic objectNullForKey:@"userCard"];
    }
    
    if ([[self.bankCardDataDic objectNullForKey:@"bankDetail"] isKindOfClass:[NSString class]])
    {
        self.bankDetailTF.text = [self.bankCardDataDic objectNullForKey:@"bankDetail"];
    }
    
    
    
    
}

#pragma mark 绑定银行卡
- (void)bindingBankcardButtonClick:(id)sender
{
//    || ![self checkCardNo:self.bankcardTextField.text]
    // 判断是否为空
    if (self.bankTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请选择开户银行"];
        return;
    }
    if (self.bankcardTextField.text.length == 0 || ![self.bankcardTextField.text hasPrefix:@"6"] || self.bankcardTextField.text.length < 16 || self.bankcardTextField.text.length > 19) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请填写正确银卡号码"];
        return;
    }
    if (self.userNameTextField.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入与身份证相符的姓名"];
        return;
    }
    if (self.userIDTextField.text.length == 0 || self.userIDTextField.text.length != 18) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入正确的身份证号"];
        return;
    }
    
    if (self.bankDetailTF.text.length == 0) {
        
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"请输入银行支行名称"];
        return;
    }
    
    
    if (self.bankTextField.text.length != 0 && [self.bankcardTextField.text hasPrefix:@"6"] && self.bankcardTextField.text.length >= 16 && self.bankcardTextField.text.length <= 19 && self.userNameTextField.text.length != 0 && self.userIDTextField.text.length == 18) {
        //添加修改银行卡的接口
        PMNetworking *nw = [PMNetworking defaultNetworking];
        
        NSDictionary *para = @{@"PM_SID":[PMUserInfos shareUserInfo].PM_SID,@"name":self.userNameTextField.text,@"userCard":self.userIDTextField.text,@"bankCard":self.bankcardTextField.text,@"bank":self.bankTextField.text,@"bankDetail":self.bankDetailTF.text};
        
        [nw request:PMRequestStateShareUpdateBank WithParameters:para callBackBlock:^(NSDictionary *dic) {
            if([[dic objectNullForKey:@"success"] integerValue] == 1)
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"绑定银行卡号成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 1;
                [alertView show];
                
                _isBinding = YES;
            }
        }showIndicator:NO];
    }
    else {
        //self.setBankcardButton.enabled = NO;
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"填写信息错误，请仔细检查"];
    }
}

#pragma mark 解绑银行卡
- (void)unBankcardButtonClick:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:self.bankTextField.text forKey:@"bankString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    MobileAuthcodeViewController *mobileAuthcodeVC = [[MobileAuthcodeViewController alloc] init];
    NSString *string = @"unbundlingBankCard";
    mobileAuthcodeVC.mobileOrBankAuthcode = string;
    [self.navigationController pushViewController:mobileAuthcodeVC animated:YES];
}


#pragma mark - 取消绑定 消除掉当前页面
- (void)cancelBinding:(UIBarButtonItem *)bbi
{
    [self dismissViewControllerAnimated:YES completion:^{
        self.bankTextField.text = nil;
        self.bankcardTextField.text = nil;
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud removeObjectForKey:@"bankString"];
    }];
    
//    NSArray *viewControllers = [self.navigationController viewControllers];
//    UIViewController *controller = [viewControllers objectAtIndex:1];
//    [self.navigationController popToViewController:controller animated:NO];
}

- (void)goToCarScan:(UIButton *)sender
{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.collectExpiry = NO;
    scanViewController.collectCVV = NO;
    scanViewController.useCardIOLogo = YES;
    [self presentViewController:scanViewController animated:YES completion:nil];
}

//取消扫描

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController

{
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
    //扫描结果
    
    NSString *strTem = [info.cardNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *strTem2 = @"";
    if (strTem.length % 4 == 0)
    {
        NSInteger count = strTem.length / 4;
        for (int i = 0; i < count; i++)
        {
            NSString *str = [strTem substringWithRange:NSMakeRange(i * 4, 4)];
            strTem2 = [strTem2 stringByAppendingString:[NSString stringWithFormat:@"%@ ", str]];
        }
    }
    else
    {
        NSInteger count = strTem.length / 4;
        for (int j = 0; j <= count; j++)
        {
            if (j == count)
            {
                NSString *str = [strTem substringWithRange:NSMakeRange(j * 4, strTem.length % 4)];
                strTem2 = [strTem2 stringByAppendingString:[NSString stringWithFormat:@"%@ ", str]];
            }
            else
            {
                NSString *str = [strTem substringWithRange:NSMakeRange(j * 4, 4)];
                strTem2 = [strTem2 stringByAppendingString:[NSString stringWithFormat:@"%@ ", str]];
            }
        }
    }
    
    self.bankcardTextField.text = strTem2;
    
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)textChange
{
//    if (self.bankcardTextField.text.length % 4 == 0)
//    {
//        self.bankcardTextField.text = [self.bankcardTextField.text stringByAppendingString:@" "];
//    }
}

-(BOOL) checkCardNo:(NSString*) cardNo{
    NSInteger sum = 0;
    NSInteger len = [cardNo length];
    NSInteger i = 0;
    
    while (i < len) {
        NSString *tmpString = [cardNo substringWithRange:NSMakeRange(len - 1 - i, 1)];
        NSInteger tmpVal = [tmpString intValue];
        if (i % 2 != 0) {
            tmpVal *= 2;
            if(tmpVal>=10) {
                tmpVal -= 9;
            }
        }
        sum += tmpVal;
        i++;
    }
    
    if((sum % 10) == 0)
        return YES;
    else
        return NO;
}

@end
