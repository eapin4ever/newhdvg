//
//  tableDelegate.m
//  changeViewController
//
//  Created by P&M on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "tableDelegate.h"
#import "PMMyPhoneInfo.h"
#import "PMScreenDetailsCell.h"
#import "PMNetworking.h"
#import "SearchResultViewController.h"

@implementation tableDelegate

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    self.seletedArr = [NSMutableArray array];
    if (!self.openArr)
    {
        self.openArr = [NSMutableArray array];
    }
    if ([self.titleType isEqualToString:@"分类"])
    {
        self.currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:-1];
        NSDictionary *oneDic = self.titleArray[0];
        NSDictionary *subClassDic = [oneDic objectNullForKey:@"subClass"];
        return [[subClassDic allKeys] count];
    }
    else if ([self.titleType isEqualToString:@"品牌"])
    {
        return 1;
    }
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.titleType isEqualToString:@"分类"])
    {
        NSDictionary *oneDic = self.titleArray[0];
        NSDictionary *subClassDic = [oneDic objectNullForKey:@"subClass"];
        NSArray *subKeysArr = [subClassDic allKeys];
        NSString *thisSectionKey = subKeysArr[section];
        NSDictionary *subSubClassDic = [[subClassDic objectNullForKey:thisSectionKey] objectNullForKey:@"subClass"];
        
        if ([self.openArr containsObject:@(section)])
        {
            return 0;
        }
        else
        {
           return [[subSubClassDic allKeys] count];
        }
        
    }
    else if ([self.titleType isEqualToString:@"品牌"])
    {
        return self.titleArray.count;
    }
    else
    {
        return self.titleArray.count;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self.titleType isEqualToString:@"分类"])
    {
        return 50;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([self.titleType isEqualToString:@"分类"])
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 40, 50)];
        headerView.backgroundColor = [UIColor whiteColor];
        
        UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, headerView.bounds.size.width - 10, 50)];
        titleLB.font = [UIFont systemFontOfSize:16.0f];
        titleLB.textAlignment = NSTextAlignmentLeft;
        
        NSDictionary *oneDic = self.titleArray[0];
        NSDictionary *subClassDic = [oneDic objectNullForKey:@"subClass"];
        NSArray *subKeysArr = [subClassDic allKeys];
        NSString *thisSectionKey = subKeysArr[section];
        NSString *headTitle = [[subClassDic objectNullForKey:thisSectionKey] objectNullForKey:@"name"];
        titleLB.text = headTitle;
        [headerView addSubview:titleLB];
        
        CALayer *upLine = [CALayer layer];
        upLine.backgroundColor = RGBA(231, 231, 231, 1).CGColor;
        upLine.frame = CGRectMake(0, 0, headerView.bounds.size.width, 0.5);
        [headerView.layer addSublayer:upLine];
        
        CALayer *downLine = [CALayer layer];
        downLine.backgroundColor = RGBA(231, 231, 231, 1).CGColor;
        downLine.frame = CGRectMake(0, headerView.bounds.size.height - 0.5, headerView.bounds.size.width, 0.5);
        [headerView.layer addSublayer:downLine];
        
        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headerView.bounds.size.width - 40, 15, 20, 20)];
        if ([self.openArr containsObject:@(section)])
        {
            arrowImageView.image = [UIImage imageNamed:@"arrowDown"];
        }
        else
        {
            arrowImageView.image = [UIImage imageNamed:@"arrowUp"];
        }
        
        [headerView addSubview:arrowImageView];
        
        headerView.tag = section;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openOrClose:)];
        [headerView addGestureRecognizer:tap];
        
        return headerView;
    }
    
    return nil;
    
}

// 设置 footer 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return HeightRate(1);
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(1))];
    footerView.backgroundColor = RGBA(200, 200, 200, 1);
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"titleArr --> %@",self.titleArray);
    PMScreenDetailsCell *detailsCell = [tableView dequeueReusableCellWithIdentifier:@"detailsCell"];
    detailsCell.backgroundColor = [UIColor whiteColor];
    [detailsCell createScreenDetailsUI];
    if ([self.titleType isEqualToString:@"分类"])
    {
        NSDictionary *oneDic = self.titleArray[0];
        NSDictionary *subClassDic = [oneDic objectNullForKey:@"subClass"];
        NSArray *subKeysArr = [subClassDic allKeys];
        NSString *thisSectionKey = subKeysArr[indexPath.section];
        NSDictionary *subSubClassDic = [[subClassDic objectNullForKey:thisSectionKey] objectNullForKey:@"subClass"];
        NSArray *subSubKeys = [subSubClassDic allKeys];
        NSString *thisRowKeys = subSubKeys[indexPath.row];
        NSString *classId = [[subSubClassDic objectNullForKey:thisRowKeys] objectNullForKey:@"id"];
        [detailsCell setScreenDetailsTitle:[[subSubClassDic objectNullForKey:thisRowKeys] objectNullForKey:@"name"]];
       
        if ([self.searchVC.classIdString isEqualToString:classId])
        {
            self.currentIndexPath = indexPath;
            detailsCell.checkIV.hidden = NO;
        }
        else
        {
            detailsCell.checkIV.hidden = YES;
        }
    }
    else if ([self.titleType isEqualToString:@"品牌"])
    {
        NSString *brandId = [self.titleArray[indexPath.row] objectNullForKey:@"brandId"];
        if ([self.searchVC.brandIds containsObject:brandId])
        {
            detailsCell.checkIV.hidden = NO;
        }
        else
        {
            detailsCell.checkIV.hidden = YES;
        }
        [detailsCell setScreenDetailsTitle:[self.titleArray[indexPath.row] objectNullForKey:@"brandName"]];
    }
    else
    {
        [detailsCell setScreenDetailsTitle:[self.titleArray[indexPath.row] objectNullForKey:@"name"]];
        NSString *specId = [self.titleArray[indexPath.row] objectNullForKey:@"id"];
//        if ([self.searchVC.specIds containsObject:specId])
//        {
//            detailsCell.checkIV.hidden = NO;
//        }
//        else
//        {
//            detailsCell.checkIV.hidden = YES;
//        }
        BOOL isHas = NO;
        NSInteger hasIndex = -1;
        for (NSInteger i = 0; i < self.searchVC.specIds.count; i++)
        {
            NSDictionary *thisSpecDic = [self.searchVC.specIds objectAtIndex:i];
            if ([[[thisSpecDic allKeys] firstObject] isEqualToString:self.titleId])
            {
                isHas = YES;
                hasIndex = i;
                break;
            }
        }
        
        if (isHas)
        {
            NSMutableArray *thisSpecArr = [[self.searchVC.specIds objectAtIndex:hasIndex] objectForKey:self.titleId];
            if ([thisSpecArr containsObject:specId])
            {
                detailsCell.checkIV.hidden = NO;
            }
            else
            {
                detailsCell.checkIV.hidden = YES;
            }
        }
        else
        {
            detailsCell.checkIV.hidden = YES;
        }
        
    }
    
    return detailsCell;
}



#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.titleType isEqualToString:@"分类"])
    {
        PMScreenDetailsCell *cell = (PMScreenDetailsCell *)[tableView cellForRowAtIndexPath:indexPath];
        PMScreenDetailsCell *nowCell = (PMScreenDetailsCell *)[tableView cellForRowAtIndexPath:self.currentIndexPath];
        if (cell.checkIV.hidden)
        {
            nowCell.checkIV.hidden = YES;
            NSDictionary *oneDic = self.titleArray[0];
            NSDictionary *subClassDic = [oneDic objectNullForKey:@"subClass"];
            NSArray *subKeysArr = [subClassDic allKeys];
            NSString *thisSectionKey = subKeysArr[indexPath.section];
            NSDictionary *subSubClassDic = [[subClassDic objectNullForKey:thisSectionKey] objectNullForKey:@"subClass"];
            NSArray *subSubKeys = [subSubClassDic allKeys];
            NSString *thisRowKeys = subSubKeys[indexPath.row];
            NSString *classId = [[subSubClassDic objectNullForKey:thisRowKeys] objectNullForKey:@"id"];
            NSString *className = [[subSubClassDic objectNullForKey:thisRowKeys] objectNullForKey:@"name"];
            cell.checkIV.hidden = NO;
            self.currentIndexPath = indexPath;
            self.searchVC.classIdString = classId;
            self.searchVC.className = className;
        }
        else
        {
            cell.checkIV.hidden = YES;
            nowCell.checkIV.hidden = YES;
            self.currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:-1];
            self.searchVC.classIdString = @"";
            self.searchVC.className = @"";
        }
    }
    else if ([self.titleType isEqualToString:@"品牌"])
    {
        PMScreenDetailsCell *cell = (PMScreenDetailsCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSString *brandId = [self.titleArray[indexPath.row] objectNullForKey:@"brandId"];
        NSString *brandName = [self.titleArray[indexPath.row] objectNullForKey:@"brandName"];
        
        if (cell.checkIV.hidden)
        {
            cell.checkIV.hidden = NO;
            [self.searchVC.brandIds addObject:brandId];
            [self.searchVC.brandNameStringArr addObject:brandName];
        }
        else
        {
            cell.checkIV.hidden = YES;
            [self.searchVC.brandIds removeObject:brandId];
            [self.searchVC.brandNameStringArr removeObject:brandName];
        }
        
        
    }
    else
    {
        PMScreenDetailsCell *cell = (PMScreenDetailsCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSString *specId = [self.titleArray[indexPath.row] objectNullForKey:@"id"];
        NSString *specName = [self.titleArray[indexPath.row] objectNullForKey:@"name"];
        if (cell.checkIV.hidden)
        {
            NSArray *specKeysArr = [self.searchVC.specDic allKeys];
            if ([specKeysArr containsObject:self.titleType])
            {
                NSMutableArray *thisArr = [[self.searchVC.specDic objectNullForKey:self.titleType] mutableCopy];
                [thisArr addObject:specName];
                [self.searchVC.specDic setValue:thisArr forKey:self.titleType];
            }
            else
            {
                NSMutableArray *thisArr = [NSMutableArray array];
                [thisArr addObject:specName];
                [self.searchVC.specDic setValue:thisArr forKey:self.titleType];
            }
            
            cell.checkIV.hidden = NO;
            BOOL isHas = NO;
            NSInteger hasIndex = -1;
            for (NSInteger i = 0; i < self.searchVC.specIds.count; i++)
            {
                NSDictionary *thisSpecDic = [self.searchVC.specIds objectAtIndex:i];
                if ([[[thisSpecDic allKeys] firstObject] isEqualToString:self.titleId])
                {
                    isHas = YES;
                    hasIndex = i;
                    break;
                }
            }
            
            if (isHas)
            {
//                NSMutableArray *thisSpecArr = [[[self.searchVC.specIds objectAtIndex:hasIndex] objectForKey:self.titleId] mutableCopy];
//                [thisSpecArr addObject:specId];
                NSMutableDictionary *thisSpecDic = [[self.searchVC.specIds objectAtIndex:hasIndex] mutableCopy];
                NSMutableArray *thisSpecArr = [[thisSpecDic objectForKey:self.titleId] mutableCopy];
                [thisSpecArr addObject:specId];
                [thisSpecDic setValue:thisSpecArr forKey:self.titleId];
                [self.searchVC.specIds replaceObjectAtIndex:hasIndex withObject:thisSpecDic];
            }
            else
            {
                NSMutableArray *thisSpecArr = [NSMutableArray array];
                [thisSpecArr addObject:specId];
                NSDictionary *thisSpecDic = @{self.titleId:thisSpecArr};
                [self.searchVC.specIds addObject:thisSpecDic];
            }
        }
        else
        {
            cell.checkIV.hidden = YES;
            NSMutableArray *thisArr = [[self.searchVC.specDic objectNullForKey:self.titleType] mutableCopy];
            [thisArr removeObject:specName];
            [self.searchVC.specDic setValue:thisArr forKey:self.titleType];
            NSInteger hasIndex = -1;
            for (NSInteger i = 0; i < self.searchVC.specIds.count; i++)
            {
                NSDictionary *thisSpecDic = [self.searchVC.specIds objectAtIndex:i];
                if ([[[thisSpecDic allKeys] firstObject] isEqualToString:self.titleId])
                {
                    hasIndex = i;
                    break;
                }
            }
            
            NSMutableDictionary *thisSpecDic = [[self.searchVC.specIds objectAtIndex:hasIndex] mutableCopy];
            NSMutableArray *thisSpecArr = [[thisSpecDic objectForKey:self.titleId] mutableCopy];
            [thisSpecArr removeObject:specId];
            [thisSpecDic setValue:thisSpecArr forKey:self.titleId];
            [self.searchVC.specIds replaceObjectAtIndex:hasIndex withObject:thisSpecDic];
            
            if (thisSpecArr.count == 0)
            {
                [self.searchVC.specIds removeObjectAtIndex:hasIndex];
            }
        }
    }
}

- (void)openOrClose:(UITapGestureRecognizer *)tap
{
    NSInteger section = tap.view.tag;
    if ([self.openArr containsObject:@(section)])
    {
        [self.openArr removeObject:@(section)];
    }
    else
    {
        [self.openArr addObject:@(section)];
    }
    [self.searchVC.secondClassTableView reloadData];
}

@end
