//
//  OrderTotalPriceCell.h
//  changeViewController
//
//  Created by pmit on 15/10/29.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTotalPriceCell : UITableViewCell

@property (strong,nonatomic) UILabel *totalTitleLB;
@property (strong,nonatomic) UILabel *totalPriceLB;

- (void)createUI;
- (void)setCellData:(double)price;

@end
