//
//  PMMyAllOrderViewController.m
//  changeViewController
//
//  Created by pmit on 15/10/29.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMMyAllOrderViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMOtherOrderDelegate.h"
#import "PMNetworking.h"
#import "PMOrderTimeStatesCell.h"
#import "MyTradeTableViewCell.h"
#import "PMOrderBtnCell.h"
#import "YBBillInfos.h"
#import "YBSureOrderViewController.h"
#import "PMToastHint.h"
#import "ComplaintsViewController.h"
#import "PMOrderDetailViewController.h"
#import "YBComplaintsViewController.h"


@interface PMMyAllOrderViewController () <UITableViewDataSource,UITableViewDelegate,PMOrderBtnCellDelegate,YBComplaintsViewControllerDelegate,ComplaintsViewControllerDelegate,PMOtherOrderDelegateDelegate,PMOrderDetailViewControllerDelegate>

@property (strong,nonatomic) UIScrollView *bigScroll;
@property (strong,nonatomic) UIImageView *selectIV;
@property (strong,nonatomic) UISegmentedControl *orderTypeSeg;
@property (strong,nonatomic) UITableView *allOrderTableView;
@property (strong,nonatomic) PMOtherOrderDelegate *otherDelegate;
@property (assign,nonatomic) BOOL hasGetWaitPay; //是否已经加载过待付款
@property (assign,nonatomic) BOOL hasGetWaitGet; //是否已经加载过待收货
@property (assign,nonatomic) BOOL hasGetWaitDiscuss; //是否已经加载过待评论
@property (strong,nonatomic) NSMutableArray *waitPayArr;
@property (strong,nonatomic) NSMutableArray *waitGetArr;
@property (strong,nonatomic) NSMutableArray *waitDiscussArr;
@property (strong,nonatomic) NSMutableArray *allOrderArr;
@property (assign,nonatomic) NSInteger currentAllOrderPage;
@property (assign,nonatomic) NSInteger currentWaitPayOrderPage;
@property (assign,nonatomic) NSInteger currentWaitGetOrderPage;
@property (assign,nonatomic) NSInteger currentWaitDiscussOrderPage;
@property (assign,nonatomic) BOOL isAllOrderLoadMore;
@property (strong,nonatomic) ZSTChatroomLoadMoreView *allOrderLoadMoreView;
@property (assign,nonatomic) BOOL isAllOrderLoading;
@property (assign,nonatomic) BOOL isAllOrderShow;
@property (strong,nonatomic) UIView *allOrderNoResultView;
@property (strong,nonatomic) UIView *waitPayNoResultView;
@property (strong,nonatomic) UIView *waitGetNoResultView;
@property (strong,nonatomic) UIView *waitDiscussNoResultView;

@end

@implementation PMMyAllOrderViewController

static NSString *const orderTimeCell = @"orderTimeCell";
static NSString *const tradeCellIdetify = @"tradeCell";
static NSString *const orderBtnCell = @"orderBtnCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的订单";
    self.isAllOrderShow = YES;
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    self.otherDelegate = [[PMOtherOrderDelegate alloc] init];
    self.otherDelegate.doubleDelegate = self;
    self.waitPayArr = [NSMutableArray array];
    self.waitGetArr = [NSMutableArray array];
    self.waitDiscussArr = [NSMutableArray array];
    self.currentAllOrderPage = 1;
    self.currentWaitPayOrderPage = 1;
    self.currentWaitGetOrderPage = 1;
    self.currentWaitDiscussOrderPage = 1;
    self.isAllOrderLoadMore = NO;
    [self buildSwitchView];
    [self buildBigScrollView];
    [self buildAllTableView];
    [self getAllOrderByNet];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildAllTableView
{
    self.allOrderTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.bigScroll.bounds.size.width, self.bigScroll.bounds.size.height) style:UITableViewStylePlain];
    self.allOrderTableView.delegate = self;
    self.allOrderTableView.dataSource = self;
    [self.allOrderTableView registerClass:[PMOrderTimeStatesCell class] forCellReuseIdentifier:orderTimeCell];
    [self.allOrderTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:tradeCellIdetify];
    [self.allOrderTableView registerClass:[PMOrderBtnCell class] forCellReuseIdentifier:orderBtnCell];
    self.allOrderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.allOrderTableView.backgroundColor = RGBA(243, 243, 243, 1);
    [self.bigScroll addSubview:self.allOrderTableView];
    
    [self createNoResult:0 PView:self.allOrderNoResultView FView:self.bigScroll];
    
    self.waitPayTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH, 0, self.bigScroll.bounds.size.width, self.bigScroll.bounds.size.height) style:UITableViewStylePlain];
    self.waitPayTableView.delegate = self.otherDelegate;
    self.waitPayTableView.dataSource = self.otherDelegate;
    [self.waitPayTableView registerClass:[PMOrderTimeStatesCell class] forCellReuseIdentifier:orderTimeCell];
    [self.waitPayTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:tradeCellIdetify];
    [self.waitPayTableView registerClass:[PMOrderBtnCell class] forCellReuseIdentifier:orderBtnCell];
    self.waitPayTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.waitPayTableView.backgroundColor = RGBA(243, 243, 243, 1);
    [self.bigScroll addSubview:self.waitPayTableView];
    
    [self createNoResult:WIDTH PView:self.waitPayNoResultView FView:self.bigScroll];
    
    self.waitGetTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH * 2, 0, self.bigScroll.bounds.size.width, self.bigScroll.bounds.size.height) style:UITableViewStylePlain];
    self.waitGetTableView.delegate = self.otherDelegate;
    self.waitGetTableView.dataSource = self.otherDelegate;
    self.waitGetTableView.backgroundColor = RGBA(243, 243, 243, 1);
    [self.waitGetTableView registerClass:[PMOrderTimeStatesCell class] forCellReuseIdentifier:orderTimeCell];
    [self.waitGetTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:tradeCellIdetify];
    [self.waitGetTableView registerClass:[PMOrderBtnCell class] forCellReuseIdentifier:orderBtnCell];
    self.waitGetTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.bigScroll addSubview:self.waitGetTableView];
    
    [self createNoResult:WIDTH * 2 PView:self.waitGetNoResultView FView:self.bigScroll];
    
    self.waitDiscussTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH * 3, 0, self.bigScroll.bounds.size.width, self.bigScroll.bounds.size.height) style:UITableViewStylePlain];
    self.waitDiscussTableView.delegate = self.otherDelegate;
    self.waitDiscussTableView.dataSource = self.otherDelegate;
    [self.waitDiscussTableView registerClass:[PMOrderTimeStatesCell class] forCellReuseIdentifier:orderTimeCell];
    [self.waitDiscussTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:tradeCellIdetify];
    [self.waitDiscussTableView registerClass:[PMOrderBtnCell class] forCellReuseIdentifier:orderBtnCell];
    self.waitDiscussTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.waitDiscussTableView.backgroundColor = RGBA(243, 243, 243, 1);
    [self.bigScroll addSubview:self.waitDiscussTableView];
    
    [self createNoResult:WIDTH * 3 PView:self.waitDiscussNoResultView FView:self.bigScroll];
}

- (void)buildSwitchView
{
    UIView *switchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 45)];
    switchView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:switchView];
    
    self.orderTypeSeg = [[UISegmentedControl alloc] initWithItems:@[@"全部订单",@"待付款",@"待收货",@"待评价"]];
    self.orderTypeSeg.frame = CGRectMake(0, 0, switchView.bounds.size.width, switchView.bounds.size.height);
    [self.orderTypeSeg setTintColor:[UIColor whiteColor]];
    self.orderTypeSeg.selectedSegmentIndex = 0;
    [self.orderTypeSeg addTarget:self action:@selector(segmentChange:) forControlEvents:UIControlEventValueChanged];
    [switchView addSubview:self.orderTypeSeg];
    
    NSDictionary* selectedTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:16],
                                             NSForegroundColorAttributeName: [UIColor redColor]};
    [self.orderTypeSeg setTitleTextAttributes:selectedTextAttributes forState:UIControlStateSelected];//设置文字
    
    NSDictionary* unselectedTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:16],
                                               NSForegroundColorAttributeName: [UIColor blackColor]};
    [self.orderTypeSeg setTitleTextAttributes:unselectedTextAttributes forState:UIControlStateNormal];
    
    self.selectIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 44, WIDTH / 4, 1)];
    self.selectIV.backgroundColor = [UIColor redColor];
    [switchView addSubview:self.selectIV];
}

- (void)buildBigScrollView
{
    self.bigScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, WIDTH, HEIGHT - 64 - 45)];
    self.bigScroll.delegate = self;
    self.bigScroll.contentSize = CGSizeMake(WIDTH * 4, 0);
    self.bigScroll.pagingEnabled = YES;
    self.bigScroll.backgroundColor = RGBA(243, 243, 243, 1);
    [self.view addSubview:self.bigScroll];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.bigScroll)
    {
        NSInteger offset = scrollView.contentOffset.x / WIDTH;
        self.selectIV.frame = CGRectMake(offset * WIDTH / 4, self.selectIV.frame.origin.y, self.selectIV.bounds.size.width, self.selectIV.bounds.size.height);
        self.orderTypeSeg.selectedSegmentIndex = offset;
        [self changeSegmentOrScroll:offset];
    }
    else
    {
        CGFloat sectionHeaderHeight = 10;//设置你footer高度
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
        
        if (self.isAllOrderLoadMore && !self.isAllOrderLoading) {
            CGFloat scrollPosition = scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y;
            if (scrollPosition < [self footerLoadMoreHeight]) {
                self.isAllOrderShow = NO;
                [self loadMore];
            }
        }
    }
}

- (void)segmentChange:(UISegmentedControl *)sender
{
    NSInteger selectIndex= sender.selectedSegmentIndex;
    self.bigScroll.contentOffset = CGPointMake(WIDTH * selectIndex, 0);
    self.selectIV.frame = CGRectMake(selectIndex * WIDTH / 4, self.selectIV.frame.origin.y, self.selectIV.bounds.size.width, self.selectIV.bounds.size.height);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.allOrderArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.allOrderArr[section] count] + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *oneDic = [self.allOrderArr[indexPath.section] firstObject];
    if (indexPath.row == 0)
    {
        PMOrderTimeStatesCell *cell = [tableView dequeueReusableCellWithIdentifier:orderTimeCell];
        [cell createUI];
        NSString *statusString = [oneDic objectForKey:@"tradeStatus"];
        NSString *orderStateString = @"";
        
        if ([statusString isEqualToString:@"待付款"])
        {
            orderStateString = @"等待买家付款";
        }
        else if ([statusString isEqualToString:@"待发货"])
        {
            orderStateString = @"卖家准备发货";
        }
        else if ([statusString isEqualToString:@"待收货"] || [statusString isEqualToString:@"已发货"])
        {
            orderStateString = @"等待买家收货";
        }
        else if ([statusString isEqualToString:@"已收货"])
        {
            orderStateString = @"交易完成";
        }
        else if ([statusString isEqualToString:@"已取消"])
        {
            orderStateString = @"订单已取消";
        }
        
        [cell setCellData:[oneDic objectForKey:@"createDt"] State:orderStateString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == [self.allOrderArr[indexPath.section] count] + 1)
    {
        NSInteger status = 0;
        NSString *statusString = [oneDic objectForKey:@"tradeStatus"];
        PMOrderBtnCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderBtnCell"];
        [cell createUI];
        cell.orderBtnDelegate = self;
        cell.goToPayBtn.tag = indexPath.section * 10000 + 1001;
        cell.cancelPayBtn.tag = indexPath.section * 10000 + 1002;
        cell.waitSendBtn.tag = indexPath.section * 10000 + 1003;
        cell.checkLogBtn.tag = indexPath.section * 10000 + 1004;
        cell.sureGetBtn.tag = indexPath.section * 10000 + 1005;
        cell.discussBtn.tag = indexPath.section * 10000 + 1006;
        cell.hasDiscussBtn.tag = indexPath.section * 10000 + 1007;
        cell.deleteBtn.tag = indexPath.section * 10000 + 1008;
        
        if ([statusString isEqualToString:@"待付款"])
        {
            status = -11;
        }
        else if ([statusString isEqualToString:@"待发货"])
        {
            status = 3;
        }
        else if ([statusString isEqualToString:@"待收货"] || [statusString isEqualToString:@"已发货"])
        {
            status = 0;
        }
        else if ([statusString isEqualToString:@"已收货"])
        {
            status = 1;
        }
        else if ([statusString isEqualToString:@"已取消"])
        {
            status = 9999;
        }
        
        switch (status)
        {
            case 3:
               [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitSend DiscussStatus:NO];
                break;
            case -11:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitPay DiscussStatus:NO];
                break;
            case 0:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitGet DiscussStatus:NO];
                break;
            case 1:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitDiscuss DiscussStatus:[[oneDic objectForKey:@"discussStatus"] integerValue] == 0 ? NO : YES];
                break;
            case 2:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeWaitGet DiscussStatus:NO];
                break;
            case 9999:
                [cell setCellData:[NSString stringWithFormat:@"￥%@",@([[oneDic objectForKey:@"allPrice"] doubleValue])] OrderBtnType:PMOrderBtnTypeHasCancel DiscussStatus:NO];
                break;
            default:
                break;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        MyTradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tradeCellIdetify];
        [cell setMyTradeOrderDataUI];
        
        NSArray *arr = self.allOrderArr[indexPath.section];
        NSDictionary *dict = arr[indexPath.row - 1];
        
        //给一张默认图片，先使用默认图片，当图片加载完成后再替换
        [cell.iv sd_setImageWithURL:[dict objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
        
        NSString *aSpecVal = [dict objectNullForKey:@"aSpecVal"];
        NSString *bSpecVal = [dict objectNullForKey:@"bSpecVal"];
        NSString *cSpecVal = [dict objectNullForKey:@"cSpecVal"];
        NSString *dSpecVal = [dict objectNullForKey:@"dSpecVal"];
        NSString *eSpecVal = [dict objectNullForKey:@"eSpecVal"];
        
        NSMutableString *specificationStr = [NSMutableString string];
        
        if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", aSpecVal];
        }
        else if (bSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", bSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", cSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", dSpecVal];
        }
        else if (aSpecVal.length != 0) {
            specificationStr = [NSMutableString stringWithFormat:@"%@", eSpecVal];
        }
        
        [cell setTitle:[dict objectNullForKey:@"productName"] setSpecification:specificationStr setNumber:[[dict objectNullForKey:@"productNum"] integerValue] setPrice:[[dict objectNullForKey:@"productPrice"] doubleValue]];
        
        cell.turnBackBtn.hidden = YES;
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 40;
    }
    else if (indexPath.row != [self.allOrderArr[indexPath.section] count] + 1)
    {
        return (iPhone6_Plus || iPhone6 ? 110 : 90);
    }
    
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = RGBA(243, 243, 243, 1);
    return sectionHeaderView;
}

- (void)changeSegmentOrScroll:(NSInteger)offset
{
    switch (offset)
    {
        case 0:
            
            break;
        case 1:
        {
            if (!self.hasGetWaitPay)
            {
                [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:@{PMSID,@"currentPage":@(1),@"pageSize":@(10),@"tradeStatus":@"-11"} callBackBlock:^(NSDictionary *dic) {
                    self.hasGetWaitPay = YES;
                    if (intSuccess == 1)
                    {
                        NSInteger totalPage = [[[dic objectForKey:@"data"] objectForKey:@"totalPages"] integerValue];
                        
                        if ([[[dic objectForKey:@"data"] objectForKey:@"beanList"] count] > 0)
                        {
                            self.waitPayNoResultView.hidden = YES;
                            self.waitPayArr = [self sortArr:[[dic objectForKey:@"data"] objectForKey:@"beanList"]];
                            self.otherDelegate.orderArr = self.waitPayArr;
                            self.otherDelegate.orderType = PMOrderTypeWaitPay;
                            self.otherDelegate.waitPayPage = totalPage;
                            if (totalPage >= self.currentWaitPayOrderPage + 1)
                            {
                                self.currentWaitPayOrderPage++;
                                self.isWaitPayLoadMore = YES;
                                if (!self.waitPayLoadMoreView)
                                {
                                    self.waitPayLoadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                                    self.waitPayTableView.tableFooterView = self.waitPayLoadMoreView;
                                }
                            }
                            
                            [self.waitPayTableView reloadData];
                        }
                        else
                        {
                            self.waitPayNoResultView.hidden = NO;
                        }
                    }
                    else
                    {
                        self.waitPayNoResultView.hidden = NO;
                    }
                    
                } showIndicator:YES];
            }
            else
            {
                self.otherDelegate.orderArr = self.waitPayArr;
                self.otherDelegate.orderType = PMOrderTypeWaitPay;
                [self.waitPayTableView reloadData];
            }
        }
            break;
        case 2:
        {
            if (!self.hasGetWaitGet)
            {
                [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:@{PMSID,@"currentPage":@(1),@"pageSize":@(10),@"tradeStatus":@"3,0"} callBackBlock:^(NSDictionary *dic) {
                    self.hasGetWaitGet = YES;
                    if (intSuccess == 1)
                    {
                        NSInteger totalPage = [[[dic objectForKey:@"data"] objectForKey:@"totalPages"] integerValue];
    
                        if ([[[dic objectForKey:@"data"] objectForKey:@"beanList"] count] > 0)
                        {
                            self.waitGetNoResultView.hidden = YES;
                            self.waitGetArr = [self sortArr:[[dic objectForKey:@"data"] objectForKey:@"beanList"]];
                            self.otherDelegate.orderArr = self.waitGetArr;
                            self.otherDelegate.orderType = PMOrderTypeWaitGet;
                            self.otherDelegate.waitGetPage = totalPage;
                            if (totalPage >= self.currentWaitGetOrderPage + 1)
                            {
                                self.currentWaitGetOrderPage++;
                                self.isWaitGetLoadMore = YES;
                                if (!self.waitGetLoadMoreView)
                                {
                                    self.waitGetLoadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                                    self.waitGetTableView.tableFooterView = self.waitGetLoadMoreView;
                                }
                            }
                            
                            [self.waitGetTableView reloadData];
                        }
                        else
                        {
                            self.waitGetNoResultView.hidden = NO;
                        }
                    }
                    else
                    {
                        self.waitGetNoResultView.hidden = NO;
                    }
                    
                } showIndicator:YES];
            }
            else
            {
                self.otherDelegate.orderArr = self.waitGetArr;
                self.otherDelegate.orderType = PMOrderTypeWaitGet;
                [self.waitGetTableView reloadData];
            }
        }
            break;
        case 3:
        {
            if (!self.hasGetWaitDiscuss)
            {
                NSDictionary *parameter = @{PMSID,@"currentPage":@(1),@"isDiscussed":@"no",@"pageSize":@(20)};
                [[PMNetworking defaultNetworking] request:PMRequestStateShowMyOrderProductDiscussList WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
                    self.hasGetWaitDiscuss = YES;
                    if(intSuccess ==1)
                    {
                        NSInteger totalPage = [[[dic objectForKey:@"data"] objectForKey:@"totalPages"] integerValue];
                        
                        NSArray *beanListArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                        if (beanListArray.count > 0)
                        {
                            self.waitDiscussNoResultView.hidden = YES;
                            self.waitDiscussArr = [self sortArr:beanListArray];
                            self.otherDelegate.orderArr = self.waitDiscussArr;
                            self.otherDelegate.orderType = PMOrderTypeWaitDiscuss;
                            self.otherDelegate.waitDiscussPage = totalPage;
                            if (totalPage >= self.currentWaitDiscussOrderPage + 1)
                            {
                                self.currentWaitDiscussOrderPage++;
                                self.isWaitDiscussLoadMore = YES;
                                if (!self.waitDiscussLoadMoreView)
                                {
                                    self.waitDiscussLoadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                                    self.waitDiscussTableView.tableFooterView = self.waitDiscussLoadMoreView;
                                }
                            }
                            
                            [self.waitDiscussTableView reloadData];
                        }
                        else
                        {
                            self.waitDiscussNoResultView.hidden = NO;
                        }
                    }
                    else
                    {
                        self.waitDiscussNoResultView.hidden = NO;
                    }
                } showIndicator:YES];
            }
            else
            {
                self.otherDelegate.orderArr = self.waitDiscussArr;
                self.otherDelegate.orderType = PMOrderTypeWaitGet;
                [self.waitDiscussTableView reloadData];
            }
        }
            break;
        
        default:
            break;
    }
}

- (NSMutableArray *)sortArr:(NSArray *)resultArr
{
    NSMutableArray *tempArr = [NSMutableArray array];
    BOOL isSame = NO;
    for (NSDictionary *dic in resultArr)
    {
        isSame = NO;
        for (NSInteger i = 0; i < tempArr.count; i++)
        {
            NSMutableArray *dicArr = tempArr[i];
            NSDictionary *showArrayDic = dicArr[0];
            if ([[showArrayDic objectNullForKey:@"orderCode"] isEqualToString:[dic objectNullForKey:@"orderCode"]])
            {
                isSame = YES;
                [dicArr addObject:dic];
                break;
            }
        }
        //如果没有相同订单号的话，就新创建一个数组
        if(isSame == NO)
        {
            NSMutableArray *newArr = [NSMutableArray array];
            [newArr addObject:dic];
            [tempArr addObject:newArr];
        }
    }
    
    return tempArr;
}

- (void)getAllOrderByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:@{PMSID,@"currentPage":@(self.currentAllOrderPage),@"pageSize":@(10),@"tradeStatus":@"all",@"isDiscussed":@"yes"} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSArray *thisAllOrderArr = [[dic objectForKey:@"data"] objectForKey:@"beanList"];
            
            NSInteger totalPage = [[[dic objectForKey:@"data"] objectForKey:@"totalPages"] integerValue];
            if (!self.isAllOrderLoadMore)
            {
                if (thisAllOrderArr.count > 0)
                {
                    self.allOrderNoResultView.hidden = YES;
                    self.allOrderArr = [self sortArr:thisAllOrderArr];
                    [self.allOrderTableView reloadData];
                    if (totalPage >= self.currentAllOrderPage + 1)
                    {
                        self.isAllOrderLoadMore = YES;
                        self.currentAllOrderPage++;
                        if (!self.allOrderLoadMoreView)
                        {
                            self.allOrderLoadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                            self.allOrderTableView.tableFooterView = self.allOrderLoadMoreView;
                        }
                    }
                }
                else
                {
                    self.allOrderNoResultView.hidden = NO;
                }
                
            }
            else
            {
                NSArray *moreArr = [self sortArr:thisAllOrderArr];
                [self.allOrderArr addObjectsFromArray:moreArr];
                [self.allOrderTableView reloadData];
                [self loadMoreCompleted];
                self.currentAllOrderPage++;
                if (self.currentAllOrderPage > totalPage)
                {
                    self.isAllOrderLoadMore = NO;
                    self.allOrderTableView.tableFooterView = nil;
                }
                else
                {
                    self.isAllOrderLoadMore = YES;
                }
            }
        }
        else
        {
            self.allOrderNoResultView.hidden = NO;
        }
        
    } showIndicator:self.isAllOrderShow];
}

- (void)cellBtnClickAction:(UIButton *)sender
{
    NSInteger sectionIndex = sender.tag / 10000;
    NSInteger btnType = sender.tag % 10000;
    NSArray *orderArray;
    NSInteger offset = self.bigScroll.contentOffset.x / WIDTH;
    switch (offset)
    {
        case 0:
            orderArray = self.allOrderArr[sectionIndex];
            break;
        case 1:
            orderArray = self.waitPayArr[sectionIndex];
            break;
        case 2:
            orderArray = self.waitGetArr[sectionIndex];
            break;
        case 3:
            orderArray = self.waitDiscussArr[sectionIndex];
            break;
            
        default:
            break;
    }
    
    switch (btnType)
    {
        case 1001:
        {
            [self getParamsWithOrderArray:orderArray];
        }
            break;
        case 1002:
        {
            [self cancelTrade:orderArray Section:sectionIndex];
        }
            break;
        case 1003:
            
            break;
        case 1004:
        {
            ComplaintsViewController *complaintsVC = [[ComplaintsViewController alloc] init];
            complaintsVC.productsArray = orderArray; // 传当前section展示的数据
            complaintsVC.state = PMOrderStateLogistics;
            [self.navigationController pushViewControllerWithNavigationControllerTransition:complaintsVC];
            self.view.userInteractionEnabled = YES;
        }
            break;
        case 1005:
        {
            ComplaintsViewController *complaintsVC = [[ComplaintsViewController alloc] init];
            complaintsVC.productsArray = orderArray; // 传当前section展示的数据
            complaintsVC.state = PMOrderStateSureOrder;
            complaintsVC.isFromDetail = NO;
            complaintsVC.complainDelegate = self;
            [self.navigationController pushViewController:complaintsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            break;
        case 1006:
        {
            YBComplaintsViewController *commentsVC = [[YBComplaintsViewController alloc] init];
            commentsVC.productArr = orderArray;
            commentsVC.complainDelegate = self;
            commentsVC.isHasDiscuss = NO;
            commentsVC.isFromDetail = NO;
            [self.navigationController pushViewController:commentsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            break;
        case 1007:
        {
            YBComplaintsViewController *commentsVC = [[YBComplaintsViewController alloc] init];
            commentsVC.productArr = orderArray;
            commentsVC.complainDelegate = self;
            commentsVC.isHasDiscuss = YES;
            commentsVC.isFromDetail = NO;
            commentsVC.orderCode = [[orderArray firstObject] objectForKey:@"orderCode"];
            [self.navigationController pushViewController:commentsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            
            break;
            
        default:
            break;
    }
}

#pragma mark - 付款需要使用的参数
- (void)getParamsWithOrderArray:(NSArray *)array
{
    NSMutableString *products = [NSMutableString string];
    for (NSInteger i = 0; i < array.count; i++)
    {
        NSDictionary *productDic = array[i];
        NSString *productId = [productDic objectNullForKey:@"productId"];
        NSString *aSpecValId = [productDic objectNullForKey:@"aSpecValId"];
        NSString *bSpecValId = [productDic objectNullForKey:@"bSpecValId"];
        NSString *cSpecValId = [productDic objectNullForKey:@"cSpecValId"];
        NSString *dSpecValId = [productDic objectNullForKey:@"dSpecValId"];
        NSString *eSpecValId = [productDic objectNullForKey:@"eSpecValId"];
        NSString *buyNum = [productDic objectNullForKey:@"productNum"];
        if (i == 0)
        {
            [products appendString:@"[{"];
        }
        else
        {
            [products appendString:@",{"];
        }
        NSString *productString = [NSString stringWithFormat:@"\"productId\":\"%@\",\"aSpecValId\":\"%@\",\"bSpecValId\":\"%@\",\"cSpecValId\":\"%@\",\"dSpecValId\":\"%@\",\"eSpecValId\":\"%@\",\"buyNum\":\"%@\"",productId,aSpecValId,bSpecValId,cSpecValId,dSpecValId,eSpecValId,buyNum];
        [products appendString:productString];
        
        if (i == array.count - 1)
        {
            [products appendString:@"}]"];
        }
        else
        {
            [products appendString:@"}"];
        }
    }
    NSDictionary *parameter = @{@"products":products};
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckStockTotal WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            if ([[dic objectNullForKey:@"messageCode"] integerValue] == 0)
            {
                NSDictionary *dic = array[0];
                NSString *orderCode = dic[@"orderCode"];
                
                NSDictionary *param = @{@"orderCode":orderCode,PMSID};
                
                [[PMNetworking defaultNetworking] request:PMRequestStateGetOrderByOrderCode WithParameters:param callBackBlock:^(NSDictionary *dic) {
                    
                    NSMutableArray *newProductArray = [NSMutableArray array];
                    
                    YBSureOrderViewController *vc = [[YBSureOrderViewController alloc] init];
                    
                    
                    vc.orderCode = orderCode;
                    NSDictionary *dataDic = [dic objectNullForKey:@"data"];
                    NSString *actuallyPay = [dataDic objectNullForKey:@"actuallyPay"];
                    vc.actuallyPay = actuallyPay;
                    NSArray *oldProductArray = [dataDic objectNullForKey:@"orderProductMap"];
                    for (NSDictionary *oldProductDic in oldProductArray)
                    {
                        NSDictionary *newProductDic = @{@"product":oldProductDic};
                        [newProductArray addObject:newProductDic];
                    }
                    
                    
                    NSDictionary *oldAddressDic = [dataDic objectNullForKey:@"receiveMap"];
                    NSDictionary *newAddressDic = @{@"name":[oldAddressDic objectNullForKey:@"receiveName"],@"mobile":[oldAddressDic objectNullForKey:@"receiveMobile"],@"areas":[oldAddressDic objectNullForKey:@"receiveArea"],@"address":[oldAddressDic objectNullForKey:@"receiveAddressDetail"],@"defaultAddressId":[oldAddressDic objectNullForKey:@"receiveAreaId"]};
                    
                    
                    NSDictionary *billDic = [dataDic objectNullForKey:@"billMap"];
                    [YBBillInfos shareInstance].billNum = @"null";
                    [YBBillInfos shareInstance].billContent = 0;
                    [YBBillInfos shareInstance].billType = 0;
                    
                    if (!isNull([billDic objectNullForKey:@"billTitle"]))
                    {
                        [YBBillInfos shareInstance].billNum = [billDic objectNullForKey:@"billTitle"];
                        [YBBillInfos shareInstance].billType = [[billDic objectNullForKey:@"billType"] integerValue];
                        [YBBillInfos shareInstance].billContent = [[billDic objectNullForKey:@"billContent"] integerValue];
                    }
                    
                    vc.productsArray = newProductArray;
                    vc.addressDic = newAddressDic;
                    vc.actuallyPay = [dataDic objectNullForKey:@"actuallyPay"];
                    vc.totalPrice = [dataDic objectNullForKey:@"orderMoney"];
                    vc.totalNum = [dataDic objectNullForKey:@"totalNum"];
                    vc.carriagePrice = [[dataDic objectNullForKey:@"logisticsPrice"] doubleValue];
                    vc.isTradeBack = YES;
                    
                    
                    [vc.tableView reloadData];
                    
                    [self.navigationController pushViewController:vc animated:YES];
                    
                    self.view.userInteractionEnabled = YES;
                    
                }showIndicator:YES];
            }
            else
            {
                NSMutableString *noEnoughString = [NSMutableString string];
                NSArray *dataArr = [dic objectNullForKey:@"data"];
                for (NSDictionary *dataDic in dataArr)
                {
                    NSString *productName = [dataDic objectNullForKey:@"productName"];
                    [noEnoughString appendString:productName];
                }
                
                NSString *alertMessage = [NSString stringWithFormat:@"%@\n库存不足",noEnoughString];
                self.view.userInteractionEnabled = YES;
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:alertMessage delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else
        {
            self.view.userInteractionEnabled = YES;
            showRequestFailAlertView;
        }
        
        
    } showIndicator:NO];
    
    /**
     参数：
     PM_SID  	  Y	   String		Token
     orderCode	  Y	   String		订单编号（根据上一步提交订单取得）
     totalPrice	  Y	   doubel		总价（根据订单信息获取）
     totalNum	  Y	   int		    商品笔数（根据订单信息获取）
     pro	      Y	   String		商品名称（多个商品用","逗号隔开）
     */
    
}

#pragma mark - 取消订单
- (void)cancelTrade:(NSArray *)orderArr Section:(NSInteger)sectionIndex
{
    NSString *orderCode = [orderArr[0] objectNullForKey:@"orderCode"];
    NSString *orderId = [orderArr[0] objectNullForKey:@"orderId"];
    
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckOrderIsPay WithParameters:@{PMSID,@"orderId":orderId} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            [[PMNetworking defaultNetworking] request:PMRequestStateUpdateOrderStatus WithParameters:@{@"status":@"9999",@"orderCode":orderCode} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view ByToast:@"订单取消成功"];
                    self.view.userInteractionEnabled = YES;
                    if (self.bigScroll.contentOffset.x / WIDTH == 0)
                    {
                        NSMutableArray *replaceArr = [NSMutableArray array];
                        for (NSInteger i = 0; i < orderArr.count; i++)
                        {
                            NSMutableDictionary *oneDic = [orderArr[i] mutableCopy];
                            [oneDic setValue:@"已取消" forKey:@"tradeStatus"];
                            [oneDic setValue:@(9999) forKey:@"status"];
                            [replaceArr addObject:oneDic];
                        }
                        
                        NSInteger allIndex = [self.allOrderArr indexOfObject:orderArr];
                        [self.allOrderArr replaceObjectAtIndex:allIndex withObject:replaceArr];
                        [self.allOrderTableView reloadSections:[NSIndexSet indexSetWithIndex:allIndex] withRowAnimation:UITableViewRowAnimationNone];
                        
//                        if (self.waitPayArr && self.waitPayArr.count > 0 && [self.waitPayArr containsObject:orderArr])
//                        {
//                            NSMutableArray *payReplaceArr = [NSMutableArray array];
//                            for (NSInteger i = 0; i < orderArr.count; i++)
//                            {
//                                NSMutableDictionary *oneDic = [orderArr[i] mutableCopy];
//                                [oneDic setValue:@"已取消" forKey:@"tradeStatus"];
//                                [oneDic setValue:@(9999) forKey:@"status"];
//                                [payReplaceArr addObject:oneDic];
//                            }
//                            
//                            NSInteger oneIndex = [self.waitPayArr indexOfObject:orderArr];
//                            [self.waitPayArr replaceObjectAtIndex:oneIndex withObject:payReplaceArr];
//                            [self.waitPayTableView reloadSections:[NSIndexSet indexSetWithIndex:oneIndex] withRowAnimation:UITableViewRowAnimationNone];
//                        }
                        
                        if (self.waitPayArr && self.waitPayArr.count > 0 && [self.waitPayArr containsObject:orderArr])
                        {
                            NSInteger obIndex = [self.waitPayArr indexOfObject:orderArr];
                            [self.waitPayArr removeObject:orderArr];
                            [self.waitPayTableView deleteSections:[NSIndexSet indexSetWithIndex:obIndex] withRowAnimation:UITableViewRowAnimationNone];
                        }


                    }
                    else
                    {
//                        NSMutableArray *payReplaceArr = [NSMutableArray array];
//                        for (NSInteger i = 0; i < orderArr.count; i++)
//                        {
//                            NSMutableDictionary *oneDic = [orderArr[i] mutableCopy];
//                            [oneDic setValue:@"已取消" forKey:@"tradeStatus"];
//                            [oneDic setValue:@(9999) forKey:@"status"];
//                            [payReplaceArr addObject:oneDic];
//                        }
//                        
//                        NSInteger oneIndex = [self.waitPayArr indexOfObject:orderArr];
//                        [self.waitPayArr replaceObjectAtIndex:oneIndex withObject:payReplaceArr];
//                        [self.waitPayTableView reloadSections:[NSIndexSet indexSetWithIndex:oneIndex] withRowAnimation:UITableViewRowAnimationNone];
                        
                        if (self.waitPayArr && self.waitPayArr.count > 0 && [self.waitPayArr containsObject:orderArr])
                        {
                            NSInteger obIndex = [self.waitPayArr indexOfObject:orderArr];
                            [self.waitPayArr removeObject:orderArr];
                            [self.waitPayTableView deleteSections:[NSIndexSet indexSetWithIndex:obIndex] withRowAnimation:UITableViewRowAnimationNone];
                        }

                        if ([self.allOrderArr containsObject:orderArr])
                        {
                            NSMutableArray *replaceArr = [NSMutableArray array];
                            for (NSInteger i = 0; i < orderArr.count; i++)
                            {
                                NSMutableDictionary *oneDic = [orderArr[i] mutableCopy];
                                [oneDic setValue:@"已取消" forKey:@"tradeStatus"];
                                [oneDic setValue:@(9999) forKey:@"status"];
                                [replaceArr addObject:oneDic];
                            }
                            
                            NSInteger allIndex = [self.allOrderArr indexOfObject:orderArr];
                            [self.allOrderArr replaceObjectAtIndex:allIndex withObject:replaceArr];
                            [self.allOrderTableView reloadSections:[NSIndexSet indexSetWithIndex:allIndex] withRowAnimation:UITableViewRowAnimationNone];
                        }
                    }
                }
                else
                {
                    self.view.userInteractionEnabled = YES;
                    showRequestFailAlertView;
                }
                
            } showIndicator:YES];
        }
        else
        {
            self.view.userInteractionEnabled = YES;
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该订单已经支付"];
        }
        
    } showIndicator:NO];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 0 && indexPath.row != [self.allOrderArr[indexPath.section] count] + 1 )
    {
        NSArray *arr = self.allOrderArr[indexPath.section];
        NSDictionary *dict = arr[indexPath.row - 1];
        NSString *orderCode = [dict objectNullForKey:@"orderCode"];
        PMOrderDetailViewController *orderDetailVC = [[PMOrderDetailViewController alloc] init];
        orderDetailVC.orderCode = orderCode;
        orderDetailVC.detailViewDelegate = self;
        orderDetailVC.productArr = arr;
        orderDetailVC.orderStatus = [dict objectForKey:@"tradeStatus"];
        orderDetailVC.isHasDiscuss = [[dict objectForKey:@"discussStatus"] integerValue] == 0 ? NO : YES;
        [self.navigationController pushViewController:orderDetailVC animated:YES];
    }
}

- (void)finishComplain:(NSArray *)productArr
{
    NSMutableArray *replaceArr = [NSMutableArray array];
    for (NSInteger i = 0; i < productArr.count; i++)
    {
        NSMutableDictionary *productDic = [productArr[i] mutableCopy];
        [productDic setValue:@(1) forKey:@"discussStatus"];
        [replaceArr addObject:productDic];
    }
    
    if ([self.allOrderArr containsObject:productArr])
    {
        NSInteger sectionIndex = [self.allOrderArr indexOfObject:productArr];
        [self.allOrderArr replaceObjectAtIndex:sectionIndex withObject:replaceArr];
        [self.allOrderTableView reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if (self.hasGetWaitDiscuss && [self.waitDiscussArr containsObject:productArr])
    {
        NSInteger sectionIndex = [self.waitDiscussArr indexOfObject:productArr];
        [self.waitDiscussArr replaceObjectAtIndex:sectionIndex withObject:replaceArr];
        [self.waitDiscussTableView reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)finishSureOrder:(NSArray *)productArr
{
    NSMutableArray *replaceArr = [NSMutableArray array];
    for (NSInteger i = 0; i < productArr.count; i++)
    {
        NSMutableDictionary *productDic = [productArr[i] mutableCopy];
        [productDic setValue:@(0) forKey:@"discussStatus"];
        [productDic setValue:@"已收货" forKey:@"tradeStatus"];
        [replaceArr addObject:productDic];
    }
    
    if ([self.allOrderArr containsObject:productArr])
    {
        NSInteger sectionIndex = [self.allOrderArr indexOfObject:productArr];
        [self.allOrderArr replaceObjectAtIndex:sectionIndex withObject:replaceArr];
        [self.allOrderTableView reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if (self.hasGetWaitGet && [self.waitGetArr containsObject:productArr])
    {
        NSInteger sectionIndex = [self.waitGetArr indexOfObject:productArr];
        [self.waitGetArr removeObject:productArr];
        [self.waitGetTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if (self.hasGetWaitDiscuss)
    {
        [self.waitDiscussArr insertObject:replaceArr atIndex:0];
        [self.waitDiscussTableView insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}

- (CGFloat)footerLoadMoreHeight
{
    if (self.allOrderLoadMoreView)
        return self.allOrderLoadMoreView.frame.size.height;
    else
        return 10;
}

- (BOOL) loadMore
{
    if (self.isAllOrderLoading)
        return NO;
    
    [self willBeginLoadingMore];
    self.isAllOrderLoading = YES;
    return YES;
}

#pragma mark - 开始加载更多
- (void)willBeginLoadingMore
{
    ZSTChatroomLoadMoreView *fv = (ZSTChatroomLoadMoreView *)self.allOrderLoadMoreView;
    [fv becomLoading];
    
    [self getAllOrderByNet];
}

#pragma mark - 结束加载更多
- (void) loadMoreCompleted
{
    ZSTChatroomLoadMoreView *fv;
    NSInteger offset = self.bigScroll.contentOffset.x / WIDTH;
    switch (offset)
    {
        case 0:
            fv = (ZSTChatroomLoadMoreView *)self.allOrderLoadMoreView;
            self.isAllOrderLoading = NO;
            break;
        case 1:
            fv = (ZSTChatroomLoadMoreView *)self.waitPayLoadMoreView;
            self.isWaitPayLoading = NO;
            break;
        case 2:
            fv = (ZSTChatroomLoadMoreView *)self.waitGetLoadMoreView;
            self.isWaitGetLoading = NO;
            break;
        case 3:
            fv = (ZSTChatroomLoadMoreView *)self.waitDiscussLoadMoreView;
            self.isWaitDiscussLoading = NO;
            break;
            
        default:
            break;
    }
    
    [fv endLoading];
}

- (void)delegateBtnClick:(UIButton *)sender
{
    [self cellBtnClickAction:sender];
}

- (void)getOtherOrderByNet
{
    NSInteger offset = self.bigScroll.contentOffset.x / WIDTH;
    switch (offset)
    {
        case 1:
        {
            [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:@{PMSID,@"currentPage":@(self.currentWaitPayOrderPage),@"pageSize":@(10),@"tradeStatus":@"-11"} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    NSArray *thisAllOrderArr = [[dic objectForKey:@"data"] objectForKey:@"beanList"];
                    NSInteger totalPage = [[[dic objectForKey:@"data"] objectForKey:@"totalPages"] integerValue];
                    NSArray *moreArr = [self sortArr:thisAllOrderArr];
                    [self.waitPayArr addObjectsFromArray:moreArr];
                    [self.waitPayTableView reloadData];
                    [self loadMoreCompleted];
                    self.currentWaitPayOrderPage++;
                    if (self.currentWaitPayOrderPage > totalPage)
                    {
                        self.isWaitPayLoadMore = NO;
                        self.waitPayTableView.tableFooterView = nil;
                    }
                    else
                    {
                        self.isWaitPayLoadMore = YES;
                    }
                }
                
            } showIndicator:NO];
        }
            break;
        case 2:
        {
            [[PMNetworking defaultNetworking] request:PMRequestStateShopOrderShowMyOrderList WithParameters:@{PMSID,@"currentPage":@(self.currentWaitGetOrderPage),@"pageSize":@(10),@"tradeStatus":@"3,0"} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    NSArray *thisAllOrderArr = [[dic objectForKey:@"data"] objectForKey:@"beanList"];
                    NSInteger totalPage = [[[dic objectForKey:@"data"] objectForKey:@"totalPages"] integerValue];
                    NSArray *moreArr = [self sortArr:thisAllOrderArr];
                    [self.waitPayArr addObjectsFromArray:moreArr];
                    [self.waitPayTableView reloadData];
                    [self loadMoreCompleted];
                    self.currentWaitPayOrderPage++;
                    if (self.currentWaitPayOrderPage > totalPage)
                    {
                        self.isWaitPayLoadMore = NO;
                        self.waitPayTableView.tableFooterView = nil;
                    }
                    else
                    {
                        self.isWaitPayLoadMore = YES;
                    }
                }
                
            } showIndicator:NO];
        }
            
            break;
        case 3:
        {
            NSDictionary *parameter = @{PMSID,@"currentPage":@(self.currentWaitDiscussOrderPage),@"isDiscussed":@"yes",@"pageSize":@(20)};
            [[PMNetworking defaultNetworking] request:PMRequestStateShowMyOrderProductDiscussList WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
                
                if(intSuccess ==1)
                {
                    NSInteger totalPage = [[[dic objectForKey:@"data"] objectForKey:@"totalPages"] integerValue];
                    NSArray *beanListArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                    NSArray *moreArr = [self sortArr:beanListArray];
                    [self.waitDiscussArr addObjectsFromArray:moreArr];
                    [self.waitDiscussTableView reloadData];
                    [self loadMoreCompleted];
                    self.currentWaitDiscussOrderPage++;
                    if (self.currentWaitDiscussOrderPage > totalPage)
                    {
                        self.isWaitDiscussLoadMore = NO;
                        self.waitDiscussTableView.tableFooterView = nil;
                    }
                    else
                    {
                        self.isWaitDiscussLoadMore = YES;
                    }
                    
                    [self.waitDiscussTableView reloadData];
                }
            } showIndicator:YES];
        }
            
            break;
            
        default:
            break;
    }
}

- (void)detailFinishSureOrder:(NSArray *)orderArr
{
    [self finishSureOrder:orderArr];
}

- (void)detailFinishComments:(NSArray *)orderArr
{
    [self finishComplain:orderArr];
}

- (void)goToDetailOrder:(NSArray *)orderArr
{
    NSDictionary *dict = orderArr[0];
    NSString *orderCode = [dict objectNullForKey:@"orderCode"];
    PMOrderDetailViewController *orderDetailVC = [[PMOrderDetailViewController alloc] init];
    orderDetailVC.orderCode = orderCode;
    orderDetailVC.detailViewDelegate = self;
    orderDetailVC.productArr = orderArr;
    orderDetailVC.orderStatus = [dict objectForKey:@"tradeStatus"];
    orderDetailVC.isHasDiscuss = [[dict objectForKey:@"discussStatus"] integerValue] == 0 ? NO : YES;
    [self.navigationController pushViewController:orderDetailVC animated:YES];
}

- (void)createNoResult:(CGFloat)viewWidth PView:(UIView *)propertyView FView:(UIView *)fatherView
{
    UIView *noResultView = [[UIView alloc] initWithFrame:CGRectMake(viewWidth, 0, WIDTH, HEIGHT- 64)];
    noResultView.backgroundColor = [UIColor whiteColor];
    if (viewWidth == 0)
    {
        self.allOrderNoResultView = noResultView;
    }
    else if (viewWidth == WIDTH)
    {
        self.waitPayNoResultView = noResultView;
    }
    else if (viewWidth == WIDTH * 2)
    {
        self.waitGetNoResultView = noResultView;
    }
    else
    {
        self.waitDiscussNoResultView = noResultView;
    }
    
    UIImageView *noResultIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noResultView.frame.size.height)];
    noResultIV.image = [UIImage imageNamed:@"order_null2"];
    noResultIV.contentMode = UIViewContentModeScaleAspectFit;
    [noResultView addSubview:noResultIV];
    
    [fatherView addSubview:noResultView];
    
    noResultView.hidden = YES;
}

- (void)detailFinishCancelOrder:(NSArray *)orderArr
{
    NSMutableArray *replaceArr = [NSMutableArray array];
    for (NSInteger i = 0; i < orderArr.count; i++)
    {
        NSMutableDictionary *oneDic = [orderArr[i] mutableCopy];
        [oneDic setValue:@"已取消" forKey:@"tradeStatus"];
        [oneDic setValue:@(9999) forKey:@"status"];
        [replaceArr addObject:oneDic];
    }
    
    NSInteger allIndex = [self.allOrderArr indexOfObject:orderArr];
    [self.allOrderArr replaceObjectAtIndex:allIndex withObject:replaceArr];
    [self.allOrderTableView reloadSections:[NSIndexSet indexSetWithIndex:allIndex] withRowAnimation:UITableViewRowAnimationNone];

    if (self.waitPayArr && self.waitPayArr.count > 0 && [self.waitPayArr containsObject:orderArr])
    {
        NSInteger obIndex = [self.waitPayArr indexOfObject:orderArr];
        [self.waitPayArr removeObject:orderArr];
        [self.waitPayTableView deleteSections:[NSIndexSet indexSetWithIndex:obIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

@end
