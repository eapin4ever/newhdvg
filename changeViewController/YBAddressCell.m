//
//  YBAddressCell.m
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define grayColor RGBA(231, 231, 231, 1)

#define itemX WidthRate(30)

#import "YBAddressCell.h"
#import "PMMyPhoneInfo.h"

@interface YBAddressCell()

@property (strong,nonatomic) UILabel *name;
@property (strong,nonatomic) UILabel *mobile;
@property (strong,nonatomic) UILabel *area;
@property (strong,nonatomic) UILabel *address;
@property (strong,nonatomic) UILabel *lb;


@end

@implementation YBAddressCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if(!isNull(self.addressDic) && [self.addressDic isKindOfClass:[NSDictionary class]] && self.addressDic != nil)
    {
        if(!_name)
        {
            [self createAddressUI];
        }
    }
    else
    {
        [self createUI];
    }
}

- (void)createUI
{
    if (!self.lb)
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.chooseAddressBtn = btn;
        btn.frame = CGRectMake(0, 0, WidthRate(320), HeightRate(100));
        btn.center = self.contentView.center;
        btn.backgroundColor = [UIColor whiteColor];
        [btn.layer setCornerRadius:8.0];
        [btn setTitle:@"选择收货地址" forState:UIControlStateNormal];
        [btn setTitleColor:HDVGFontColor forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:HeightRate(30)];
        btn.userInteractionEnabled = NO;
        //[btn addTarget:self action:@selector(chooseAddress:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
    }
}

- (void)createAddressUI
{
    [self.chooseAddressBtn setHidden:YES];
    
    
    // 头像
    UIImageView *avatarImage = [[UIImageView alloc] initWithFrame:CGRectMake(itemX, HeightRate(15), 15, 15)];
    avatarImage.contentMode = UIViewContentModeScaleAspectFit;
    avatarImage.image = [UIImage imageNamed:@"address_name.png"];
    [self.contentView addSubview:avatarImage];
    
    _name = [[UILabel alloc] init];
    _name.font = [UIFont systemFontOfSize:14.0f];
    NSString *name = [self.addressDic objectNullForKey:@"name"];
    CGSize nameSize = [name sizeWithFont:[UIFont systemFontOfSize:14.0f]];
    _name.frame = CGRectMake(itemX + 15, HeightRate(10), nameSize.width, HeightRate(40));
    _name.font = [UIFont systemFontOfSize:14.0f];
    _name.textColor = HDVGFontColor;
    [self.contentView addSubview:_name];
    
    // 电话图片
    UIImageView *phoneImage = [[UIImageView alloc] initWithFrame:CGRectMake(_name.frame.origin.x + _name.frame.size.width + 20, HeightRate(15), 15, 15)];
    phoneImage.contentMode = UIViewContentModeScaleAspectFit;
    phoneImage.image = [UIImage imageNamed:@"address_phone.png"];
    [self.contentView addSubview:phoneImage];
    
    _mobile = [[UILabel alloc] init];
    _mobile.font = [UIFont systemFontOfSize:14.0f];
    NSString *mobile = [self.addressDic objectNullForKey:@"mobile"];
    CGSize mobileSize = [mobile sizeWithFont:[UIFont systemFontOfSize:14.0f]];
    _mobile.frame = CGRectMake(phoneImage.frame.origin.x + phoneImage.frame.size.width, HeightRate(10), mobileSize.width, HeightRate(40));
    _mobile.textColor = HDVGFontColor;
    [self.contentView addSubview:_mobile];
    
    _area = [[UILabel alloc] init];
    _area.font = [UIFont systemFontOfSize:14.0f];
    NSString *area = [NSString stringWithFormat:@"%@ %@",[self.addressDic objectNullForKey:@"areas"],[self.addressDic objectNullForKey:@"address"]];
    CGSize areaSize = [area sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(WIDTH - itemX * 2, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    //_area.frame = CGRectMake(itemX, HeightRate(60), areaSize.width, areaSize.height);
    _area.frame = CGRectMake(itemX, HeightRate(60), WIDTH - itemX * 3, areaSize.height);
    _area.numberOfLines = 0;
    _area.textColor = HDVGFontColor;
    [self.contentView addSubview:_area];
    
    // 箭头图片
    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 35, HeightRate(10), 20, 20)];
    arrow.image = [UIImage imageNamed:@"arrowRight"];
    if (!self.isTradeBack)
    {
        [self.contentView addSubview:arrow];
    }
    
    _name.text = name;
    _mobile.text = mobile;
    _area.text = area;
}

@end
