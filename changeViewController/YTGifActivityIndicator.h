//
//  YTGifActivityIndicator.h
//  changeViewController
//
//  Created by pmit on 15/3/27.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YTGifActivityIndicator : UIView

@property (strong,nonatomic) NSMutableArray *imageArr;

@property (assign,nonatomic) NSInteger imageCount;
@property (strong,nonatomic) NSTimer *timer;

+ (YTGifActivityIndicator *)defaultIndicator;
- (void)start;
- (void)stop;
@end
