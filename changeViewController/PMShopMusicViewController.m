//
//  PMShopMusicViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/30.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMShopMusicViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "PMMusicTableViewCell.h"
#import "PMMyPhoneInfo.h"
#import "PMToastHint.h"

@interface PMShopMusicViewController () <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) AVAudioPlayer *musicPlayer;
@property (assign,nonatomic) NSInteger seletedIndex;
@property (strong,nonatomic) UITableView *musicListTableView;
@property (assign,nonatomic) NSInteger nowPlayIndex;

@end

@implementation PMShopMusicViewController

static NSString *const musicCell = @"musicCell";

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"店铺音乐设置";
    
    [self buildNavigationBarBtn];
    self.seletedIndex = -1;
    self.nowPlayIndex = -1;
    [self buildTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildNavigationBarBtn
{
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveMusic)];
    self.navigationItem.rightBarButtonItem = saveBtn;
}

- (void)buildTableView
{
    self.musicListTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.musicListTableView.backgroundColor = HDVGPageBGGray;
    self.musicListTableView.delegate = self;
    self.musicListTableView.dataSource = self;
    [self.musicListTableView registerClass:[PMMusicTableViewCell class] forCellReuseIdentifier:musicCell];
    [self.view addSubview:self.musicListTableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.musicArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMMusicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:musicCell];
    [cell createUI];
    cell.selectedBtn.tag = indexPath.row;
    [cell.selectedBtn addTarget:self action:@selector(seletedMusic:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *urlNameString = [self.musicArr[indexPath.row] objectAtIndex:2];
    if ([self.muiscName isKindOfClass:[NSNull class]] || [self.muiscName isEqualToString:@""]) {
        
    }
    else
    {
        NSString *seletedMusicName = [[self.muiscName componentsSeparatedByString:@"/"] lastObject];
        NSString *thisMusicName = [[[self.musicArr[indexPath.row] objectAtIndex:0] componentsSeparatedByString:@"/"] lastObject];
        if ([seletedMusicName isEqualToString:thisMusicName])
        {
            cell.selectedBtn.selected = YES;
            self.seletedIndex = indexPath.row;
        }
    }
    
    
    
    cell.musicTitle.text = urlNameString;
    cell.onAndOffBtn.tag = indexPath.row * 1000;
    [cell.onAndOffBtn addTarget:self action:@selector(playOrPauseMusic:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSInteger index= indexPath.row;
//    [self changeStatue:index AndTableView:tableView];
//    
//}

- (void)changeStatue:(NSInteger)newsIndex AndTableView:(UITableView *)tableView
{
    NSIndexPath *seletedIndexPath = [NSIndexPath indexPathForRow:self.seletedIndex inSection:0];
    NSIndexPath *newsSeletedIndexPath = [NSIndexPath indexPathForRow:newsIndex inSection:0];
    if (self.seletedIndex != newsIndex)
    {
        if (self.seletedIndex != -1)
        {
            PMMusicTableViewCell *seletedCell = (PMMusicTableViewCell *)[tableView cellForRowAtIndexPath:seletedIndexPath];
            seletedCell.selectedBtn.selected = NO;
        }
        PMMusicTableViewCell *newsSeletedCell = (PMMusicTableViewCell *)[tableView cellForRowAtIndexPath:newsSeletedIndexPath];
        newsSeletedCell.selectedBtn.selected = YES;
        self.seletedIndex = newsIndex;
    }
    else
    {
        PMMusicTableViewCell *seletedCell = (PMMusicTableViewCell *)[tableView cellForRowAtIndexPath:seletedIndexPath];
        seletedCell.selectedBtn.selected = NO;
        self.seletedIndex = -1;
    }
}

- (void)seletedMusic:(UIButton *)sender
{
    NSInteger index = sender.tag;
    [self changeStatue:index AndTableView:self.musicListTableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return HeightRate(1);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(1))];
    footerView.backgroundColor = RGBA(200, 200, 200, 1);
    
    return footerView;
}

- (void)playOrPauseMusic:(UIButton *)sender
{
    NSInteger index = sender.tag / 1000;
    NSString *musicLink = [self.musicArr[sender.tag / 1000] objectAtIndex:1];
    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:musicLink]];
    
    if (self.nowPlayIndex == index)
    {
        if (sender.selected)
        {
            [self.musicPlayer pause];
        }
        else
        {
            self.musicPlayer = [[AVAudioPlayer alloc] initWithData:data error:nil];
            [self.musicPlayer prepareToPlay];
            [self.musicPlayer play];
        }
        sender.selected = !sender.selected;
    }
    else
    {
        if (self.nowPlayIndex != -1)
        {
            PMMusicTableViewCell *cell = (PMMusicTableViewCell *)[self.musicListTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.nowPlayIndex inSection:0]];
            cell.onAndOffBtn.selected = NO;
            [self.musicPlayer pause];
            [self.musicPlayer stop];
        }
        
        sender.selected = YES;
        self.nowPlayIndex = index;
        self.musicPlayer = [[AVAudioPlayer alloc] initWithData:data error:nil];
        [self.musicPlayer prepareToPlay];
        [self.musicPlayer play];
    }
   
}


- (void)saveMusic
{
    if (self.seletedIndex == -1) {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"亲，请选择歌曲再保存"];
        return;
    }
    
    NSString *musicName = [self.musicArr[self.seletedIndex] objectAtIndex:0];
    [[PMNetworking defaultNetworking] request:PMRequestStateUpdateShopHome WithParameters:@{PMSID,@"id":self.shopId,@"shopMusic":musicName} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dic objectNullForKey:@"message"] message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
    } showIndicator:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.musicPlayer)
    {
        [self.musicPlayer pause];
        [self.musicPlayer stop];
    }
}

@end
