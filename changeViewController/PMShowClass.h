//
//  PMShowClass.h
//  changeViewController
//
//  Created by pmit on 15/1/6.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMProductClassTVCell.h"
@class PMGoodListViewController;
@interface PMShowClass : NSObject<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray *parentClassArray;
@property(nonatomic,weak) PMGoodListViewController *goodsListVC;
@end
