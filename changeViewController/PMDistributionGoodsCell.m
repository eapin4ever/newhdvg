//
//  PMDistributionGoodsCell.m
//  changeViewController
//
//  Created by pmit on 15/7/28.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMDistributionGoodsCell.h"
#import "PMMyPhoneInfo.h"

#define itemYUp 80
#define itemYDown 115
#define titleFont 12
#define titleFont2 10

@implementation PMDistributionGoodsCell
{
    UILabel *hintLB;
    UILabel *commissionLB;
    double percentNum;
    
    UILabel *numLB1;
    UILabel *numLB2;
    UILabel *numLB3;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.iv)
    {
        self.seletedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.seletedBtn.frame = CGRectMake(WidthRate(20), 50, WIDTH * 0.08 - WidthRate(10), 40);
        self.seletedBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.seletedBtn setImage:[UIImage imageNamed:@"selectAll_0"] forState:UIControlStateNormal];
        [self.seletedBtn setImage:[UIImage imageNamed:@"selectAll_1"] forState:UIControlStateSelected];
        [self.contentView addSubview:self.seletedBtn];
        
        self.lossLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), 50, WIDTH * 0.08 - WidthRate(10), 40)];
        self.lossLB.text = @"失效";
        self.lossLB.font = [UIFont systemFontOfSize:10.0f];
        self.lossLB.textColor = [UIColor lightGrayColor];
        self.lossLB.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.lossLB];
        
        self.iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(20) + WIDTH * 0.08, 20, 100, 100)];
        self.iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.iv];
        
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20) + WIDTH * 0.08 + 110, 20, WIDTH - WidthRate(20) - WIDTH * 0.08 - 60 - 110, 40)];
        self.titleLB.numberOfLines = 2;
        self.titleLB.font = [UIFont systemFontOfSize:12.0f];
        self.titleLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.titleLB];
        
        UIView *messageView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(20) + WIDTH * 0.08 + 110, 70, WIDTH - WidthRate(20) - WIDTH * 0.08 - 60 - 110, 60)];
        self.messageView = messageView;
        
        UILabel *unitPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, messageView.bounds.size.width / 2 + 5, 20)];
        unitPriceLB.text = @"单价";
        unitPriceLB.font = [UIFont systemFontOfSize:titleFont];
        unitPriceLB.textColor = RGBA(135, 135, 135, 1);
        unitPriceLB.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:unitPriceLB];
        
        numLB1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, messageView.bounds.size.width / 2 + 5, 30)];
        numLB1.textColor = HDVGRed;
        numLB1.font = [UIFont systemFontOfSize:HeightRate(33)];
        numLB1.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:numLB1];
        
        UILabel *moneyLB = [[UILabel alloc] initWithFrame:CGRectMake(messageView.bounds.size.width / 2 + 5, 0, messageView.bounds.size.width / 2 + 5, 20)];
        moneyLB.text = @"佣金";
        moneyLB.font = [UIFont systemFontOfSize:titleFont];
        moneyLB.textColor = RGBA(135, 135, 135, 1);
        moneyLB.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:moneyLB];
        
        numLB3 = [[UILabel alloc] initWithFrame:CGRectMake(messageView.bounds.size.width / 2 +5, 20, messageView.bounds.size.width / 2 + 5, 30)];
        numLB3.font = [UIFont systemFontOfSize:titleFont];
        numLB3.textColor = RGBA(189, 24, 41, 1);
        numLB3.textAlignment = NSTextAlignmentCenter;
        [messageView addSubview:numLB3];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:messageView];
        
        self.shareBtn = [[YBShareBtn alloc] initWithFrame:CGRectMake(WIDTH - 50, 0, 50, 140)];
        [self.shareBtn setImage:[UIImage imageNamed:@"share_02"] forState:UIControlStateNormal];
        [self.shareBtn setTitle:@"分享到" forState:UIControlStateNormal];
        self.shareBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.shareBtn];
        
        self.deleteBtn = [[YBShareBtn alloc] initWithFrame:CGRectMake(WIDTH - 50, 0, 50, 140)];
        [self.deleteBtn setImage:[UIImage imageNamed:@"distruDelete"] forState:UIControlStateNormal];
        self.deleteBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.deleteBtn];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.goDetailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.goDetailBtn.frame = CGRectMake(WIDTH * 0.08 + WidthRate(10), 0, WIDTH - 50 - WIDTH * 0.08 - WidthRate(10), 140);
        [self.contentView addSubview:self.goDetailBtn];
    }
}

- (void)setContentTitle:(NSString *)title unitPrice:(NSString *)price percent:(CGFloat)percent
{
    self.titleLB.text = title;
    
    NSMutableAttributedString *noteStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.2lf",[price doubleValue]]];
    NSRange litteRange = NSMakeRange([[noteStr1 string] rangeOfString:@"."].location,3);
    [noteStr1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:titleFont] range:NSMakeRange(0, 1)];
    [noteStr1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:titleFont] range:litteRange];
    numLB1.attributedText = noteStr1;
    NSString *inputPriceString = [NSString stringWithFormat:@"￥%.2lf",percent];
    NSMutableAttributedString *noteStr3 = [[NSMutableAttributedString alloc] initWithString:inputPriceString];
    NSRange litteRange3 = NSMakeRange(1,[[noteStr3 string] rangeOfString:@"."].location);
    [noteStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:titleFont] range:NSMakeRange(0, 1)];
    [noteStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:HeightRate(33)] range:litteRange3];
    numLB3.attributedText = noteStr3;
    
    if (self.isDown)
    {
        self.seletedBtn.hidden = YES;
        self.lossLB.hidden = NO;
    }
    else
    {
        self.seletedBtn.hidden = NO;
        self.lossLB.hidden = YES;
    }
    
}

@end
