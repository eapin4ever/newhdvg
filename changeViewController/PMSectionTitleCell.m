//
//  PMSectionTitleCell.m
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//
#define CellHi 37
#define CellWi self.bounds.size.width

#import "PMSectionTitleCell.h"

@implementation PMSectionTitleCell
{
    UIImageView *_iv;
    UILabel *_lb;
}

- (void)createUI
{
    if(!_iv)
    {
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(14, (CellHi-20)/2, 20, 20)];
        _iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv];
        
        _lb = [[UILabel alloc] initWithFrame:CGRectMake(36, _iv.frame.origin.y, CellWi - 36, 20)];
        _lb.textColor = [UIColor darkGrayColor];
        _lb.font = [UIFont systemFontOfSize:16];
        
        [self.contentView addSubview:_lb];
        
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CALayer *line1 = [CALayer layer];
        line1.frame = CGRectMake(0, 0, WIDTH, 1);
        line1.backgroundColor = HDVGPageBGGray.CGColor;
        [self.contentView.layer addSublayer:line1];
        
        CALayer *line2 = [CALayer layer];
        line2.frame = CGRectMake(0, CellHi - 1, WIDTH, 1);
        line2.backgroundColor = HDVGPageBGGray.CGColor;
        [self.contentView.layer addSublayer:line2];
    }
}

- (void)setTitle:(NSString *)title image:(UIImage *)image
{
    _iv.image = image;
    _lb.text = title;
    
}

@end
