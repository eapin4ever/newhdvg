//
//  PMOrderSectionTitleCell.h
//  changeViewController
//
//  Created by pmit on 15/11/4.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PMOrderSectionTitleCellDelegate <NSObject>

- (void)goToMore;

@end

@interface PMOrderSectionTitleCell : UITableViewCell

@property (strong,nonatomic) UIImageView *sectionIV;
@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UIButton *moreBtn;
@property (weak,nonatomic) id<PMOrderSectionTitleCellDelegate> titleCellDelegate;

- (void)createUI;
- (void)setCellData:(NSString *)imgName TitleString:(NSString *)titleString IsMoreBtnShow:(BOOL)isMoreBtnShow;

@end
