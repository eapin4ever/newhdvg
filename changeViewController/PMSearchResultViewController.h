//
//  searchResultViewController.h
//  changeViewController
//
//  Created by pmit on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMGoodsReusableView.h"
#import "PMFilterSideView.h"
#import "JHRefresh.h"
#import "PMSearchViewDelegate.h"


@interface PMSearchResultViewController : UIViewController <PMSerchViewMyDelegate,PMFilterSideViewDelegate>
@property(nonatomic,strong)NSMutableArray *dicArray;
@property(nonatomic,copy)NSString *searchString;
@property (strong,nonatomic) NSMutableArray *brandIds;
@property (strong,nonatomic) NSMutableArray *specIds;
@property (copy,nonatomic) NSString *classIdString;
@property (copy,nonatomic) NSString *className;
@property (strong,nonatomic) NSMutableArray *brandNameStringArr;
@property (strong,nonatomic) UITableView *screenTableView;//一级类别tableview
@property (strong,nonatomic) NSMutableDictionary *specDic;
@property (strong,nonatomic) NSDictionary *paramDic;



- (instancetype)initWithSearchString:(NSString *)string;

- (void)refreshSearchResult;

//- (void)searchWithParam:(NSMutableDictionary *)param finish:(myFinishBlock)finish isBool:(BOOL)isbool error:(myFinishBlock)error;
@end



