//
//  PMOrderTimeStatesCell.h
//  changeViewController
//
//  Created by pmit on 15/10/29.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMOrderTimeStatesCell : UITableViewCell

@property (strong,nonatomic) UILabel *orderCodeLB;
@property (strong,nonatomic) UILabel *orderStatesLB;

- (void)createUI;
- (void)setCellData:(NSString *)orderCodeString State:(NSString *)orderStateString;

@end
