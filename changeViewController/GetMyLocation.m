//
//  GetMyLocation.m
//  changeViewController
//
//  Created by pmit on 14/11/7.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "GetMyLocation.h"

@interface GetMyLocation()


@end

static GetMyLocation *_getLocation;
@implementation GetMyLocation
+ (GetMyLocation *)defaultLocationManager
{
    if(!_getLocation)
    {
        _getLocation = [[GetMyLocation alloc] init];
        _getLocation.clm = [[CLLocationManager alloc] init];
    }
    return _getLocation;
}


+ (CLLocation *)getMyLocation
{
    if(!_getLocation)
    {
        _getLocation = [[GetMyLocation alloc] init];
        _getLocation.clm = [[CLLocationManager alloc] init];
    }
    
    //设置代理
    _getLocation.clm.delegate = _getLocation;
    //设置距离筛选器
    _getLocation.clm.distanceFilter = 1000.0f;
    //设置精度
    _getLocation.clm.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    //一直打开定位服务
    if(SYSTEM_VERSION >= 8.0)
        [_getLocation.clm requestWhenInUseAuthorization];
    
    //IOS 8.0以上版本，需要再plist文件中加入以下字段
    //NSLocationWhenInUsageDescription
    //NSLocationAlwaysUsageDescription
    
    //NSLog(@"authorizationStatus = %d",[CLLocationManager authorizationStatus]);
    
    [_getLocation.clm startUpdatingLocation];
    
    return _getLocation.clm.location;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if(SYSTEM_VERSION >= 8.0)
    {
        switch (status)
        {
            case kCLAuthorizationStatusNotDetermined:
                if([manager respondsToSelector:@selector(requestWhenInUseAuthorization)])
                {
                    [manager requestWhenInUseAuthorization];
                    
                }
                break;
            case kCLAuthorizationStatusDenied:
                
                
                break;
                
            default:
                break;
        }
    }
    [self.clm startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSString *errorString;
    [manager stopUpdatingLocation];
    switch([error code]) {
        case kCLErrorDenied:
            //Access denied by user
//            errorString = @"Access to Location Services denied by user";
            //Do something...
            break;
        case kCLErrorLocationUnknown:
            //Probably temporary...
//            errorString = @"Location data unavailable";
            //Do something else...
            break;
        default:
//            errorString = @"An unknown error has occurred";
            break;
    }
}

@end
