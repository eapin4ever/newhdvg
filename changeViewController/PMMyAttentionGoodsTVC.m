//
//  PMMyAttentionGoodsTVC.m
//  changeViewController
//
//  Created by pmit on 14/12/6.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMMyAttentionGoodsTVC.h"
#import "MyAttentionViewController.h"
#import "PMMyGoodsViewController.h"
#import "PMToastHint.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ZSTChatroomLoadMoreView.h"

@interface PMMyAttentionGoodsTVC ()
@property(nonatomic,assign)NSInteger currentPage;
@property(nonatomic,strong)NSDictionary *allDataDic;
@property (assign,nonatomic) BOOL isHasMore;
@property (assign,nonatomic) BOOL isLoding;
@property (strong,nonatomic) ZSTChatroomLoadMoreView *loadMoreView;

@end

static PMMyAttentionGoodsTVC *_currentVC;
static NSString *const reuseID = @"reuseCell";
static NSString *const noResultTitle = @"暂时没有关注商品,\n快去添加吧";
@implementation PMMyAttentionGoodsTVC
+ (PMMyAttentionGoodsTVC *)currentVC
{
    if(!_currentVC)
        _currentVC = [[PMMyAttentionGoodsTVC alloc] init];
    return _currentVC;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.view.frame = CGRectMake(0, 40, WIDTH, HEIGHT-40);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.currentPage = 1;
    [self.tableView registerClass:[PMAttenionGoodsCell class] forCellReuseIdentifier:reuseID];
    //允许多行编辑
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 0.2)];
    self.tableView.tableFooterView.backgroundColor = [UIColor lightGrayColor];
    self.tableView.backgroundColor = RGBA(244, 244, 244, 1);
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getModels];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dicArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightRate(240);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMAttenionGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID forIndexPath:indexPath];
    [cell createUI];
    
    cell.showsReorderControl = NO;
    
    NSDictionary *dic = self.dicArray[indexPath.row];
    
    [cell.iv sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
//关注不显示距离
    [cell setContentTitle:[dic objectNullForKey:@"productName"] classify:@"白色" distance:nil price:[dic objectNullForKey:@"productPrice"]];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.tableView.isEditing)
    {
    
    }
    else
    {
        PMMyGoodsViewController *vc = [[PMMyGoodsViewController alloc] init];
        vc.productId = [self.dicArray[indexPath.row] objectNullForKey:@"productId"];
        [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSString *productId = [self.dicArray[indexPath.row] objectNullForKey:@"scpId"];
        [self cancelCareProduct:@[productId] finish:^{
            //请求成功后，再从UI上删除
            [self.dicArray removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }];
        
       
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (void)deleteSelectedRows
{
    NSArray *arr = [self.tableView indexPathsForSelectedRows];
    if (arr.count == 0)
    {
        [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"您还没有选择要删除的商品"];
    }
    else
    {
        NSMutableArray *productIds = [NSMutableArray array];
        
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
        for (NSIndexPath *indexPath in arr)
        {
            [indexSet addIndex:indexPath.row];
            //取出对应的商品id
            NSString *productId = [self.dicArray[indexPath.row] objectNullForKey:@"scpId"];
            [productIds addObject:productId];
        }
        
        
        [self cancelCareProduct:productIds finish:^{
            //请求成功后，再从上删除
            //删除数据源对应的数据
            [self.dicArray removeObjectsAtIndexes:indexSet];
            
            //删除行
            [self.tableView deleteRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationAutomatic];
            
            if (self.dicArray.count == 0)
            {
                self.tableView.scrollEnabled = NO;
                ((MyAttentionViewController *)self.parentViewController).noResultView.hidden = NO;
            }
        }];
    }
    

    
}



#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - 从服务器请求数据
- (void)getModels
{
    [[PMNetworking defaultNetworking] request:PMRequestStateCareShowCareProduct WithParameters:@{@"currentPage":@(self.currentPage),@"pageSize":@(30)} callBackBlock:^(NSDictionary *dic) {
        
        MyAttentionViewController *pVC = (MyAttentionViewController *)self.parentViewController;

        if([[dic objectNullForKey:@"success"] integerValue]==1)
        {
            self.allDataDic = [dic objectNullForKey:@"data"];
            self.dicArray = [[self.allDataDic objectNullForKey:@"beanList"] mutableCopy];
            
            self.tableView.scrollEnabled = YES;
            pVC.noResultView.hidden = YES;
            
            if ([[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue] >= self.currentPage + 1) {
                self.isHasMore = YES;
                if (!self.loadMoreView)
                {
                    self.loadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                    self.tableView.tableFooterView = self.loadMoreView;
                }
            }
            else
            {
                self.isHasMore = NO;
            }
            
            [self.tableView reloadData];
        }
        else
        {
            
            self.tableView.scrollEnabled = NO;
            [pVC showNoResultViewWithIndex:1];
            
            [self.dicArray removeAllObjects];
            [self.tableView reloadData];
        }
    }showIndicator:YES];
}


#pragma mark 取消关注
- (void)cancelCareProduct:(NSArray *)productIds finish:(finishAction)finish
{
    NSString *productId = [productIds componentsJoinedByString:@","];
    [[PMNetworking defaultNetworking] request:PMRequestStateCareCancelBatchCareProduct WithParameters:@{@"ids":productId} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            if(finish)
                finish();
            if (self.dicArray.count == 0)
            {
                MyAttentionViewController *pVC = ((MyAttentionViewController *)self.parentViewController);
                [pVC showNoResultViewWithIndex:1];
            }
        }
        else
        {
            showRequestFailAlertView
        }
    }showIndicator:NO];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.isHasMore && !self.isLoding) {
        CGFloat scrollPosition = scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y;
        if (scrollPosition < [self footerLoadMoreHeight]) {
            //                self.isAllOrderShow = NO;
            [self loadMore];
        }
    }
}

- (CGFloat)footerLoadMoreHeight
{
    if (self.loadMoreView)
        return self.loadMoreView.frame.size.height;
    else
        return 10;
}

- (BOOL) loadMore
{
    if (self.isLoding)
        return NO;
    
    [self willBeginLoadingMore];
    self.isLoding = YES;
    return YES;
}

#pragma mark - 开始加载更多
- (void)willBeginLoadingMore
{
    ZSTChatroomLoadMoreView *fv = (ZSTChatroomLoadMoreView *)self.loadMoreView;
    [fv becomLoading];
    self.currentPage++;
    [self getMoreByNet];
}

- (void)getMoreByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateCareShowCareProduct WithParameters:@{@"currentPage":@(self.currentPage),@"pageSize":@(10)} callBackBlock:^(NSDictionary *dic) {
        
        MyAttentionViewController *pVC = (MyAttentionViewController *)self.parentViewController;
        
        if([[dic objectNullForKey:@"success"] integerValue]==1)
        {
            self.allDataDic = [dic objectNullForKey:@"data"];
            NSArray *nextGoodArr = [self.allDataDic objectNullForKey:@"beanList"];
            [self.dicArray addObjectsFromArray:nextGoodArr];
            
            self.tableView.scrollEnabled = YES;
            pVC.noResultView.hidden = YES;
            [self loadMoreCompleted];
            [self.tableView reloadData];
            
            if ([[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue] >= self.currentPage + 1) {
                self.isHasMore = YES;
                if (!self.loadMoreView)
                {
                    self.loadMoreView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                    self.tableView.tableFooterView = self.loadMoreView;
                }
            }
            else
            {
                self.isHasMore = NO;
                if (self.loadMoreView)
                {
                    self.tableView.tableFooterView = nil;
                }
            }
            
        }
        else
        {
            
            self.tableView.scrollEnabled = NO;
            [pVC showNoResultViewWithIndex:1];
            
            [self.dicArray removeAllObjects];
            [self.tableView reloadData];
        }
    }showIndicator:YES];
}



#pragma mark - 结束加载更多
- (void) loadMoreCompleted
{
    ZSTChatroomLoadMoreView *fv = self.loadMoreView;
    self.isLoding = NO;
    [fv endLoading];
}


@end
