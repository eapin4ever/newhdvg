//
//  searchResultViewController.m
//  changeViewController
//
//  Created by pmit on 14/11/21.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMSearchResultViewController.h"
#import "MasterViewController.h"
#import "PMMyGoodsViewController.h"
#import "PMScreenTableViewCell.h"
#import "PMScreenDetailsCell.h"
#import "tableDelegate.h"
#import "PMToastHint.h"

#define originY 40

@interface PMSearchResultViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate,PMSerchViewMyDelegate, UITableViewDataSource,UITableViewDelegate>
//topBar相关
@property(nonatomic,strong)UIView *titleView;
@property(nonatomic,strong)UITextField *keyWordTF;

@property(nonatomic,strong)UISegmentedControl *segment;
@property(nonatomic,strong)UILabel *redLine;

@property(nonatomic,strong)UICollectionView *resultCollectionView;
@property(nonatomic,strong)UICollectionViewFlowLayout *flowLayout;

@property(nonatomic,strong)PMFilterSideView *filterView;
@property(nonatomic,strong)NSArray *sectionTitleArray;
@property(nonatomic,strong)NSMutableDictionary *allDataDic;

@property(nonatomic,strong) UIImageView *priceImage;

@property(nonatomic,assign)BOOL isLoad;
@property(nonatomic,strong)NSMutableDictionary *params;
@property(nonatomic,assign)NSInteger totalPages;

@property(nonatomic,strong)NSMutableDictionary *searchParams;
@property (strong,nonatomic) UIView *noResultView;//没有搜索到商品时的View
@property (strong,nonatomic) NSMutableArray *searchHistory;
@property (strong,nonatomic) UITableView *searchView;
@property (strong,nonatomic) UIBarButtonItem *cancelBtn;
@property (strong,nonatomic) UIBarButtonItem *searchBtn;
@property (strong,nonatomic) PMSearchViewDelegate *delegateVC;

@property (assign,nonatomic) BOOL isClear;


// 筛选
@property (strong,nonatomic) UIView *screenBgView;
@property (strong,nonatomic) UIView *screenView;

@property (strong,nonatomic) NSMutableArray *typeNameArray;//一级类别标题数组
@property (strong,nonatomic) NSMutableArray *brandNameArray;//二级类别品牌标题
@property (strong,nonatomic) NSMutableArray *classNameArray;//二级类别分类标题
@property (strong,nonatomic) NSMutableArray *specNameArray;//二级类别规格标题

@property (strong,nonatomic) UITableView *screenDetailsTV;//二级类别详情tableview
@property (strong,nonatomic) NSArray *titleArray;//二级类别标题数组
@property (strong,nonatomic) UILabel *titleLab;//顶部类别标题

@property (strong,nonatomic) tableDelegate *tableDelegate;

@end

@implementation PMSearchResultViewController
{
    BOOL _isShowFilterView;
    BOOL _isAscending;
    BOOL _isSearch;
    
    BOOL _isClickAll;
    BOOL _isClickSell;
    
    NSInteger sellCount;
}

- (instancetype)initWithSearchString:(NSString *)string
{
    self = [super init];
    if (self)
    {
//        self.title = [NSString stringWithFormat:@"\"%@\"的搜索结果",string];
        self.searchString = string;
        self.view.backgroundColor = [UIColor whiteColor];
        self.view.frame = CGRectMake(0, 40, WIDTH, 40);
        _isSearch = YES;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initSegmentedControl];

    self.brandNameStringArr = [NSMutableArray array];
    self.specDic = [NSMutableDictionary dictionary];
    self.isLoad = NO;
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popToLastView:)];
    [self.view addGestureRecognizer:swipe];
    
    [self searchWithString];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _isClickAll = YES;
    _isClickSell = NO;
    
    sellCount = 0;
}


- (void)initSegmentedControl
{
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:@[@"综合",@"销量",@"价格",@"筛选"]];
    segment.frame = CGRectMake(0, 0, WIDTH, 40);
    segment.selectedSegmentIndex = 0;
    
    [segment addTarget:self action:@selector(switchFilter:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segment];
    
    //添加tap手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [segment addGestureRecognizer:tap];
    
    [segment setTintColor:[UIColor clearColor]];
    //选中时字体颜色
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HDVGRed,NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateSelected];
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor lightGrayColor],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    segment.backgroundColor = RGBA(244, 244, 244, 1);
    
    //分割线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(WIDTH/4, 7.5, 1, 25);
    line1.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
    [segment.layer addSublayer:line1];

    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(WIDTH/4*2, 7.5, 1, 25);
    line2.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
    [segment.layer addSublayer:line2];
    
    CALayer *line3 = [[CALayer alloc] init];
    line3.frame = CGRectMake(WIDTH/4*3, 7.5, 1, 25);
    line3.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
    [segment.layer addSublayer:line3];
    
    // 价格走向图标
    UIImageView *priceIma = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH/4*3 - WidthRate(60), 15, 7, 12)];
    [priceIma setImage:[UIImage imageNamed:@"arrow_price"]];
    self.priceImage = priceIma;
    [segment addSubview:priceIma];
    
    // 筛选图标
    UIImageView *screenIma = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(60), 14, 10, 12)];
    [screenIma setImage:[UIImage imageNamed:@"screen_ima"]];
    [segment addSubview:screenIma];
    
    
    //指示条
    self.redLine = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/5/segment.numberOfSegments - 7, 38, WIDTH/5*4/segment.numberOfSegments, 2)];
    self.redLine.backgroundColor = HDVGRed;
    [segment addSubview:self.redLine];
}


- (void)refreshSearchResult
{
    //如果还没创建则创建视图
    if(!self.resultCollectionView)
    {
        [self searchResultUI];
        [self creataScreenPageUI];
    }
    else
    {
        [self.resultCollectionView reloadData];
    }
}


#pragma mark - 搜索结果展示视图
- (void)searchResultUI
{
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    [self.flowLayout setMinimumInteritemSpacing:5];
    [self.flowLayout setMinimumLineSpacing:HeightRate(20)];
    [self.flowLayout setItemSize:CGSizeMake(WidthRate(345), WidthRate(490))];
    [self.flowLayout setSectionInset:UIEdgeInsetsMake(0, WidthRate(20), 0, WidthRate(20))];
    
    self.resultCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, originY , WIDTH, HEIGHT - originY - 64) collectionViewLayout:self.flowLayout];
    [self.resultCollectionView setBackgroundColor:HDVGPageBGGray];
    [self.resultCollectionView setDataSource:self];
    [self.resultCollectionView setDelegate:self];
    [self.resultCollectionView registerClass:[PMGoodsReusableView class] forCellWithReuseIdentifier:@"reuseItem"];
    
    [self.resultCollectionView setContentInset:UIEdgeInsetsMake(5, 0, 5, 0)];
    
    //[self.view addSubview:self.resultCollectionView];
    [self.view insertSubview:self.resultCollectionView atIndex:0];
    
    UIView *noResultView = [[UIView alloc] initWithFrame:CGRectMake(0, originY, WIDTH, HEIGHT - originY - 64)];
    noResultView.backgroundColor = [UIColor whiteColor];
    self.noResultView = noResultView;
    
    UIImageView *noResultIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noResultView.frame.size.height)];
    noResultIV.image = [UIImage imageNamed:@"search_null"];
    noResultIV.contentMode = UIViewContentModeScaleAspectFit;
    [noResultView addSubview:noResultIV];
    
    
    if (self.dicArray.count == 0)
    {
        self.noResultView.hidden = NO;
    }
    else
    {
        self.noResultView.hidden = YES;
    }
    
    //添加下拉刷新和上拉加载
//    [self addRefreshAction];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dicArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMGoodsReusableView *item = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseItem" forIndexPath:indexPath];
    
    NSDictionary *dic = self.dicArray[indexPath.row];
    NSString *discount;
    BOOL isHasDiscount = NO;
    // ---------------------------- wei ----------------------------
    // 折扣
    id discountDic = [dic objectNullForKey:@"zkMap"];
    if ([discountDic isKindOfClass:[NSDictionary class]] && !isNull(discountDic))
    {
        discount = [discountDic objectNullForKey:@"zkblShow"];
        if (discount.length != 0) {
            item.showDiscount = YES;
            isHasDiscount = YES;
        }
        else {
            item.showDiscount = NO;
            isHasDiscount = NO;
        }
    }
    else
    {
        item.showDiscount = NO;
        isHasDiscount = NO;
        discount = @"10";
    }
    // ---------------------------- wei ----------------------------
    
    [item createUI];
    
    
    [item.imageView sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    [item setContentTitle:[dic objectNullForKey:@"productName"] price:isHasDiscount ? [[discountDic objectNullForKey:@"discountPrice"] doubleValue] : [[dic objectNullForKey:@"productPrice"] doubleValue] discount:discount];
    
    
    if (indexPath.row == self.dicArray.count - 7)
    {
        self.isLoad = YES;
        //得到当前页
        NSInteger currentPage = [[self.params objectNullForKey:@"currentPage"] integerValue];
        if(currentPage < self.totalPages)//如果小于总页数则刷新
        {
            //增加当前页
            currentPage++;
            //获取数据
            [self.params setValue:@(currentPage) forKey:@"currentPage"];
            [self searchWithParam:self.params finish:^{
                
                [self.resultCollectionView reloadData];
                //加载刷新
                
            } isBool:YES error:^{
                //不用刷新
            }];
        }
        else
        {
            
        }
    }
    
    return item;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMMyGoodsViewController *detailVC = [[PMMyGoodsViewController alloc] init];
    detailVC.productId = [self.dicArray[indexPath.row] objectNullForKey:@"id"];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:detailVC];
}


#pragma mark - 排序
- (void)tapAction:(UITapGestureRecognizer *)tap
{
    //收键盘
    [self.keyWordTF resignFirstResponder];
    
    UISegmentedControl *segment = (UISegmentedControl *)tap.view;
    CGPoint point = [tap locationInView:segment];
    if(point.x < WIDTH/4)
    {
        segment.selectedSegmentIndex = 0;
    }
    else if(point.x > WIDTH/4*2 && point.x < WIDTH/4*3)
    {
        segment.selectedSegmentIndex = 2;
    }
    else if(point.x > WIDTH/4*3)
    {
        segment.selectedSegmentIndex = 3;
    }
    else
    {
        segment.selectedSegmentIndex = 1;
    }
    [self switchFilter:segment];
    
}

//综合、附近、价格筛选按钮
- (void)switchFilter:(UISegmentedControl *)sender
{
    //指示条滑动的动画
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.redLine.frame;
        rect.origin.x = WIDTH/5/sender.numberOfSegments + WIDTH/sender.numberOfSegments*sender.selectedSegmentIndex - 7;
        self.redLine.frame = rect;
    }];

    _isSearch = NO;//不是搜索
    _isLoad = NO;//不是加载

    NSString *query;
    if(sender.selectedSegmentIndex == 0)//综合排序
    {
        query = @"all";
        _isAscending = YES;
    }
    else if (sender.selectedSegmentIndex == 1)//销量排序
    {
        query = @"sellNum";
        _isAscending = YES;
        sellCount++;
        if (sellCount >= 2) {
            _isClickSell = YES;
        }
    }
    else if(sender.selectedSegmentIndex == 2)//价格排序
    {
        CGFloat angle = 0;
        if(_isAscending == YES) {
            query = @"priceUp";
            angle = _isAscending ? M_PI : 0;
            [self.priceImage setTransform:CGAffineTransformMakeRotation(angle)];
        }
        else {
            query = @"priceDown";
            angle = _isAscending ? M_PI : 0;
            [self.priceImage setTransform:CGAffineTransformMakeRotation(angle)];
        }
        _isAscending = !_isAscending;
    }
    else if(sender.selectedSegmentIndex == 3)//筛选
    {
        self.navigationController.navigationBarHidden = YES;
        [UIView animateWithDuration:0.3f animations:^{
            
            self.screenBgView.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
            
        }];
        
    }
    
    
    //排序/筛选请求
    if ((sender.selectedSegmentIndex == 0 && _isClickAll == YES) || (sender.selectedSegmentIndex == 1 && _isClickSell == YES)) {
        
        [self filterRequestWithQuery:query isBool:NO];// 不显示刷新动画
    }
    else
        [self filterRequestWithQuery:query isBool:YES];
}

//点击返回按钮
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.keyWordTF resignFirstResponder];
}


#pragma mark - 请求数据
- (void)searchWithParam:(NSMutableDictionary *)param finish:(myFinishBlock)finish isBool:(BOOL)isbool error:(myFinishBlock)error
{
    [param setObject:self.searchString forKey:@"key"];
    
    //从服务器获取搜索数据
    [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:self.paramDic callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            self.typeNameArray = [NSMutableArray array];
            self.specNameArray = [NSMutableArray array];
            self.brandNameArray = [NSMutableArray array];
            self.classNameArray = [NSMutableArray array];
            //是不是加载请求
            if(self.isLoad)
            {
                [self.dicArray addObjectsFromArray:[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"]];
            }
            else if([[dic objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
            {
                self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
                self.totalPages = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"totalPages"] integerValue];
                NSDictionary *allClassDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allClassMap"];
                NSArray *classKeyArr = [allClassDic allKeys];
                NSLog(@"allClass --> %@",allClassDic);
                for (NSString *classKey in classKeyArr)
                {
                    NSDictionary *oneClassDic = [allClassDic objectNullForKey:classKey];
                    NSLog(@"one --> %@",oneClassDic);
                    [self.classNameArray addObject:oneClassDic];
                }
                
                NSDictionary *allBrandDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allBrandMap"];
                NSArray *brandKeyArr = [allBrandDic allKeys];
                for (NSString *brandKey in brandKeyArr)
                {
                    NSDictionary *oneBrandDic = [allBrandDic objectNullForKey:brandKey];
                    [self.brandNameArray addObject:oneBrandDic];
                }
                
                NSDictionary *allSpecDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allSpecMap"];
                NSArray *specKeyArr = [allSpecDic allKeys];
                for (NSString *specKey in specKeyArr)
                {
                    NSDictionary *oneSpecDic = [allSpecDic objectNullForKey:specKey];
                    [self.specNameArray addObject:oneSpecDic];
                }
                
                [self.screenTableView reloadData];
            }
            
            self.params = [param mutableCopy];
            
            [self refreshSearchResult];
            
            if (self.dicArray.count == 0)
            {
                self.noResultView.hidden = NO;
            }
            else
            {
                self.noResultView.hidden = YES;
            }
        }
        else
        {
            showRequestFailAlertView;
        }
    }showIndicator:YES];//YES为显示刷新动画，NO不显示
}

- (void)removeAllNullObjInArray:(NSMutableArray *)arr
{
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (id obj in arr)
    {
        if(isNull(obj))
           [indexSet addIndex:[arr indexOfObject:obj]];
    }
    [arr removeObjectsAtIndexes:indexSet];
}


#pragma mark - 下拉刷新等
- (void)addRefreshAction
{
    __weak PMSearchResultViewController *weakSelf = self;
    
    [self.resultCollectionView addRefreshFooterViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        weakSelf.isLoad = YES;
        //得到当前页
        NSInteger currentPage = [[weakSelf.params objectNullForKey:@"currentPage"] integerValue];
        if(currentPage < weakSelf.totalPages)//如果小于总页数则刷新
        {
            //增加当前页
            currentPage++;
            //获取数据
            [weakSelf.params setValue:@(currentPage) forKey:@"currentPage"];
            [weakSelf searchWithParam:weakSelf.params finish:^{
                
                [weakSelf.resultCollectionView reloadData];
                //加载刷新
                [weakSelf.resultCollectionView footerEndRefreshing];
                
            } isBool:YES error:^{
                //不用刷新
                [weakSelf.resultCollectionView footerEndRefreshing];
            }];
        }
        else
        {
            //不用刷新
            [weakSelf.resultCollectionView footerEndRefreshing];
        }
        
    }];
}

#pragma mark 筛选请求
- (void)filterRequestWithQuery:(NSString *)query isBool:(BOOL)isbool
{
    if([query isKindOfClass:[NSString class]] && query != nil && ![query isEqualToString:@""])
    {
        _isSearch = NO;
        NSMutableDictionary *param = [@{@"orderBy":query,@"currentPage":@(1)} mutableCopy];
        if([PMUserInfos shareUserInfo].userId)
            [param setValue:[PMUserInfos shareUserInfo].userId forKey:@"userId"];
        for (NSString *brandId in self.brandIds)
        {
            [param setValue:brandId forKey:@"brandIds"];
        }
        
        for (NSString *specId in self.specIds)
        {
            [param setValue:specId forKey:@"specLists"];
        }
        
        [self searchWithParam:param finish:^{
            
        } isBool:isbool error:^{
            
        }];
    }
}

- (void)searchWithWord:(NSString *)word
{
    
    NSMutableDictionary *param = [@{@"key":self.searchString,@"areaName":[PMUserInfos shareUserInfo].defaultArea,@"currentPage":@(1)} mutableCopy];
    
    for (NSString *brandId in self.brandIds)
    {
        [param setValue:brandId forKey:@"brandIds"];
    }
    
    for (NSString *specId in self.specIds)
    {
        [param setValue:specId forKey:@"specLists"];
    }
    if([PMUserInfos shareUserInfo].userId)
        [param setValue:[PMUserInfos shareUserInfo].userId forKey:@"userId"];
    
    
    [self searchWithParam:param finish:nil isBool:YES error:nil];
}

- (void)clearHistoryWord
{
    [self.searchHistory removeAllObjects];
    [self.delegateVC.searchView reloadData];
}


#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)filterHide
{
    _isShowFilterView = NO;
    [self.filterView hideFilterSideViewFinish:^{
        [self.filterView.view setHidden:YES];
    }];
}

#pragma mark - 创建筛选页面
- (void)creataScreenPageUI
{
    self.screenBgView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH, HEIGHT)];
    self.screenBgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.screenBgView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.8;
    [self.screenBgView addSubview:alphaView];
    
    
    self.screenView = [[UIView alloc] initWithFrame:CGRectMake(40, 0, WIDTH - 40, HEIGHT)];
    self.screenView.backgroundColor = [UIColor whiteColor];
    [self.screenBgView addSubview:self.screenView];
    self.screenView.hidden = NO;
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissScreenView:)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.screenView addGestureRecognizer:swipe];
    
    // 创建取消按钮
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(15, 30, 40, 30);
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.screenView addSubview:cancelBtn];
    
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(80, 30, self.screenView.frame.size.width - 160, 30)];
    titleLB.text = @"筛选";
    titleLB.textAlignment = NSTextAlignmentCenter;
    [self.screenView addSubview:titleLB];
    
    // 创建保存按钮
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(self.screenView.frame.size.width - 55, 30, 40, 30);
    [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [saveBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.screenView addSubview:saveBtn];
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, 64 - 0.5, WIDTH - 40, HeightRate(1));
    layer.backgroundColor = RGBA(220, 220, 220, 1).CGColor;
    [self.screenView.layer addSublayer:layer];
    
    
    // 初始化筛选 tableView
    self.screenTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.screenView.frame.size.width, [[UIScreen mainScreen] bounds].size.height - 64)];
    self.screenTableView.tag = 100;
    self.screenTableView.delegate = self;
    self.screenTableView.dataSource = self;
    [self.screenTableView registerClass:[PMScreenTableViewCell class] forCellReuseIdentifier:@"screenCell"];
    [self.screenView addSubview:self.screenTableView];
    
    self.screenTableView.backgroundColor = HDVGPageBGGray;
    
    self.tableDelegate = [[tableDelegate alloc] init];
    
    // 初始化筛选详情 tableView
    self.screenDetailsTV = [[UITableView alloc] initWithFrame:CGRectMake(self.screenView.frame.size.width, 0, self.screenView.frame.size.width, [[UIScreen mainScreen] bounds].size.height)];
    self.screenDetailsTV.tag = 101;
    self.screenDetailsTV.delegate = self.tableDelegate;
    self.screenDetailsTV.dataSource = self.tableDelegate;
    [self.screenDetailsTV registerClass:[PMScreenDetailsCell class] forCellReuseIdentifier:@"detailsCell"];
    [self.screenView addSubview:self.screenDetailsTV];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 40, 64)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // 创建取消按钮
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(15, 30, 40, 30);
    [backBtn setTitle:@"返回" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [backBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [backBtn addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:backBtn];
    
    UILabel *tableTitle = [[UILabel alloc] initWithFrame:CGRectMake(80, 30, self.screenView.frame.size.width - 160, 30)];
    self.titleLab = tableTitle;
    tableTitle.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:tableTitle];
    
    // 创建保存按钮
    UIButton *okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame = CGRectMake(self.screenView.frame.size.width - 55, 30, 40, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [okBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [okBtn addTarget:self action:@selector(okButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:okBtn];
    
    // 分隔线
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(0, 64 - 0.5, WIDTH - 40, HeightRate(1));
    layer2.backgroundColor = RGBA(220, 220, 220, 1).CGColor;
    [headerView.layer addSublayer:layer2];
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.screenTableView.bounds.size.width, 40)];
    
    UIButton *clearHistoryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearHistoryBtn setTitle:@"清除记录" forState:UIControlStateNormal];
    [clearHistoryBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [clearHistoryBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    clearHistoryBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    clearHistoryBtn.layer.cornerRadius = 8.0;
    clearHistoryBtn.frame = CGRectMake(0, 0, WidthRate(240), 30);
    clearHistoryBtn.center = footerView.center;
    clearHistoryBtn.layer.borderWidth = 1.5;
    clearHistoryBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [clearHistoryBtn addTarget:self action:@selector(clickToClearSearchHistory:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:clearHistoryBtn];
    
    footerView.backgroundColor = [UIColor clearColor];
    self.screenTableView.tableFooterView = footerView;
    
    
    self.screenDetailsTV.tableHeaderView = headerView;
    
    self.screenDetailsTV.backgroundColor = HDVGPageBGGray;
}

- (void)cancelBtnClick:(UIButton *)sender
{
    [self animationDismiss];
}



#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger hasInteger = 0;
    if (self.classNameArray.count > 0)
    {
        hasInteger += 1;
    }
    
    if (self.brandNameArray.count > 0)
    {
        hasInteger += 1;
    }
    
    return hasInteger + self.specNameArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 40, 10)];
    headerView.backgroundColor = HDVGPageBGGray;
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, 10 - 0.5, WIDTH - 40, HeightRate(1));
    layer.backgroundColor = RGBA(220, 220, 220, 1).CGColor;
    [headerView.layer addSublayer:layer];
    
    return headerView;
}

// 设置 footer 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return HeightRate(1);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(1))];
    footerView.backgroundColor = RGBA(200, 200, 200, 1);
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMScreenTableViewCell *screenCell = [tableView dequeueReusableCellWithIdentifier:@"screenCell"];
    
    [screenCell createScreenUI];
    
    // 取出数据
    if (indexPath.row == 0)
    {
        [screenCell setScreenTitle:@"分类"];
        if (self.className)
        {
            screenCell.tipLB.text = self.className;
        }
        
    }
    else if (indexPath.row == 1)
    {
        [screenCell setScreenTitle:@"品牌"];
        if (self.brandNameStringArr)
        {
            NSString *brandName = [self.brandNameStringArr componentsJoinedByString:@","];
            screenCell.tipLB.text = brandName;
        }
    }
    else
    {
        NSDictionary *oneSpecDic = self.specNameArray[indexPath.row - 2];
        NSString *specName = [oneSpecDic objectNullForKey:@"name"];
        
        NSArray *specArr = [self.specDic objectNullForKey:specName];
        NSString *thisSpecString = [specArr componentsJoinedByString:@","];
        screenCell.tipLB.text = thisSpecString;
        
        [screenCell setScreenTitle:specName];
    }
    
    return screenCell;
}


#pragma mark - 点击cell触发的事件响应方法
#pragma mark
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = @"";
    
    if (indexPath.row == 0)
    {
        title = @"分类";
        self.tableDelegate.titleArray = self.classNameArray;
    }
    else if (indexPath.row == 1)
    {
        title = @"品牌";
        self.tableDelegate.titleArray = self.brandNameArray;
    }
    else
    {
        title = [self.specNameArray[indexPath.row - 2] objectNullForKey:@"name"];
        NSArray *specValueList = [self.specNameArray[indexPath.row - 2] objectNullForKey:@"specValueList"];
        NSLog(@"specNameArr --> %@",specValueList);
        self.tableDelegate.titleArray = specValueList;
    }
    self.tableDelegate.titleType = title;
    self.tableDelegate.searchKey = self.searchString;
    [self.screenDetailsTV reloadData];
    [UIView animateWithDuration:0.3 animations:^{
        
        self.titleLab.text = title;
        [self.screenDetailsTV setFrame:CGRectMake(0, 0, self.screenView.frame.size.width, HEIGHT)];
    } completion:^(BOOL finished){
        
    }];
}

- (void)backBtnClick:(UIButton *)button
{
    [self.screenTableView reloadData];
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.screenDetailsTV setFrame:CGRectMake(self.screenView.frame.size.width, 0, self.screenView.frame.size.width, HEIGHT)];
        [self.screenTableView setFrame:CGRectMake(0, 64, self.screenView.frame.size.width, HEIGHT)];
    } completion:^(BOOL finished){
        
    }];
}

- (void)saveBtnClick:(UIButton *)sender
{
    [self animationDismiss];
    
    NSMutableDictionary *param = [@{@"currentPage":@(1),@"pageSize":@(20),@"key":self.searchString} mutableCopy];
    for (NSString *brandId in self.brandIds)
    {
        [param setValue:brandId forKey:@"brandIds"];
    }
    
    for (NSString *specId in self.specIds)
    {
        [param setValue:specId forKey:@"specLists"];
    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
            [self refreshSearchResult];
        }
        
    } showIndicator:YES];

    
}


- (void)okButtonClick:(UIButton *)button
{
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.screenDetailsTV setFrame:CGRectMake(self.screenView.frame.size.width, 0, self.screenView.frame.size.width, HEIGHT)];
        [self.screenTableView setFrame:CGRectMake(0, 64, self.screenView.frame.size.width, HEIGHT)];
    } completion:^(BOOL finished){
        if ([self.tableDelegate.titleType isEqualToString:@"分类"])
        {
            [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:@{@"key":self.searchString,@"currentPage":@(1),@"pageSize":@(20),@"classId3":self.self.classIdString} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    NSDictionary *dataDic = [dic objectNullForKey:@"data"];
                    NSDictionary *allBrandMap = [dataDic objectNullForKey:@"allBrandMap"];
                    NSDictionary *allSpecMap = [dataDic objectNullForKey:@"allSpecMap"];
                    
                    NSMutableArray *brandArr = [NSMutableArray array];
                    NSArray *brandKeyArr = [allBrandMap allKeys];
                    for (NSString *brandKey in brandKeyArr)
                    {
                        NSDictionary *oneBrandDic = [allBrandMap objectNullForKey:brandKey];
                        [brandArr addObject:oneBrandDic];
                    }
                    
                    NSMutableArray *specArr = [NSMutableArray array];
                    NSArray *specKeyArr = [allSpecMap allKeys];
                    for (NSString *specKey in specKeyArr)
                    {
                        NSDictionary *oneSpecDic = [allSpecMap objectNullForKey:specKey];
                        [specArr addObject:oneSpecDic];
                    }
                    
                    self.brandNameArray = [brandArr mutableCopy];
                    self.specNameArray = [specArr mutableCopy];
                    self.brandIds = [NSMutableArray array];
                    self.specIds = [NSMutableArray array];
                    self.brandNameStringArr = [NSMutableArray array];
                    [self.screenTableView reloadData];
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        [self.screenDetailsTV setFrame:CGRectMake(self.screenView.frame.size.width, 0, self.screenView.frame.size.width, HEIGHT)];
                        [self.screenTableView setFrame:CGRectMake(0, 64, self.screenView.frame.size.width, HEIGHT)];
                    } completion:^(BOOL finished){
                        
                    }];
                }
            } showIndicator:NO];
        }
        else if ([self.tableDelegate.titleType isEqualToString:@"品牌"])
        {
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:1 inSection:0];
            [self.screenTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        else
        {
            [self.screenTableView reloadData];
        }
    }];
    
}

- (void)clickToClearSearchHistory:(UIButton *)sender
{
    
    //从服务器获取搜索数据
    
    [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:@{@"key":self.searchString,@"pageSize":@(20),@"currentPage":@(1)} callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            self.brandNameArray = [NSMutableArray array];
            self.specNameArray = [NSMutableArray array];
            self.classNameArray = [NSMutableArray array];
            self.brandIds = [NSMutableArray array];
            self.specIds = [NSMutableArray array];
            self.brandNameStringArr = [NSMutableArray array];
            self.specDic = [NSMutableDictionary dictionary];
            self.className = @"";
            //是不是加载请求
            if(self.isLoad)
            {
                [self.dicArray addObjectsFromArray:[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"]];
            }
            else if([[dic objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
            {
                self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
                self.totalPages = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"totalPages"] integerValue];
                NSDictionary *allClassDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allClassMap"];
                NSArray *classKeyArr = [allClassDic allKeys];
                NSLog(@"allClass --> %@",allClassDic);
                for (NSString *classKey in classKeyArr)
                {
                    NSDictionary *oneClassDic = [allClassDic objectNullForKey:classKey];
                    NSLog(@"one --> %@",oneClassDic);
                    [self.classNameArray addObject:oneClassDic];
                }
                
                NSDictionary *allBrandDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allBrandMap"];
                NSArray *brandKeyArr = [allBrandDic allKeys];
                for (NSString *brandKey in brandKeyArr)
                {
                    NSDictionary *oneBrandDic = [allBrandDic objectNullForKey:brandKey];
                    [self.brandNameArray addObject:oneBrandDic];
                }
                
                NSDictionary *allSpecDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allSpecMap"];
                NSArray *specKeyArr = [allSpecDic allKeys];
                for (NSString *specKey in specKeyArr)
                {
                    NSDictionary *oneSpecDic = [allSpecDic objectNullForKey:specKey];
                    [self.specNameArray addObject:oneSpecDic];
                }
                
                [self.screenTableView reloadData];
            }
            
            [self refreshSearchResult];
            
            if (self.dicArray.count == 0)
            {
                self.noResultView.hidden = NO;
            }
            else
            {
                self.noResultView.hidden = YES;
            }
        }
        else
        {
        }
    }showIndicator:NO];//YES为显示刷新动画，NO不显示
}

- (void)refreshTableView
{
    [self.screenDetailsTV reloadData];
}

- (void)dismissScreenView:(UISwipeGestureRecognizer *)swipe
{
    self.navigationController.navigationBarHidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.screenBgView.frame = CGRectMake(WIDTH, 0, WIDTH, HEIGHT);
        
    } completion:^(BOOL finished) {
        
    }];
    
}

- (void)popToLastView:(UISwipeGestureRecognizer *)swipe
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)animationDismiss
{
    self.navigationController.navigationBarHidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.screenBgView.frame = CGRectMake(WIDTH, 0, WIDTH, HEIGHT);
        
    }];
}

- (void)searchWithString
{
    [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:self.paramDic callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            self.typeNameArray = [NSMutableArray array];
            self.specNameArray = [NSMutableArray array];
            self.brandNameArray = [NSMutableArray array];
            self.classNameArray = [NSMutableArray array];
            //是不是加载请求
            if(self.isLoad)
            {
                [self.dicArray addObjectsFromArray:[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"]];
            }
            else if([[dic objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
            {
                self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
                self.totalPages = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"totalPages"] integerValue];
                NSDictionary *allClassDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allClassMap"];
                NSArray *classKeyArr = [allClassDic allKeys];
                for (NSString *classKey in classKeyArr)
                {
                    NSDictionary *oneClassDic = [allClassDic objectNullForKey:classKey];
                    [self.classNameArray addObject:oneClassDic];
                }
                
                NSDictionary *allBrandDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allBrandMap"];
                NSArray *brandKeyArr = [allBrandDic allKeys];
                for (NSString *brandKey in brandKeyArr)
                {
                    NSDictionary *oneBrandDic = [allBrandDic objectNullForKey:brandKey];
                    [self.brandNameArray addObject:oneBrandDic];
                }
                
                NSDictionary *allSpecDic = [[dic objectNullForKey:@"data"] objectNullForKey:@"allSpecMap"];
                NSArray *specKeyArr = [allSpecDic allKeys];
                for (NSString *specKey in specKeyArr)
                {
                    NSDictionary *oneSpecDic = [allSpecDic objectNullForKey:specKey];
                    [self.specNameArray addObject:oneSpecDic];
                }
                
                [self.screenTableView reloadData];
            }
            self.params = [self.paramDic mutableCopy];
            
            [self refreshSearchResult];
            
            if (self.dicArray.count == 0)
            {
                self.noResultView.hidden = NO;
            }
            else
            {
                self.noResultView.hidden = YES;
            }
        }
        else
        {
            showRequestFailAlertView;
        }
    }showIndicator:YES];//YES为显示刷新动画，NO不显示
}

@end

