//
//  YBMessageCell.h
//  changeViewController
//
//  Created by pmit on 15/6/23.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBMessageCell : UITableViewCell

@property (strong,nonatomic) NSDictionary *messageDic;

- (void)createUI;

@end
