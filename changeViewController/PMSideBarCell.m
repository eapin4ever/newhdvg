//
//  PMSideBarCell.m
//  changeViewController
//
//  Created by pmit on 14/12/4.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMSideBarCell.h"

@implementation PMSideBarCell
{
    UIImageView *iv;
    UILabel *lb;
}
- (void)createUI
{
    if(!iv || !lb)
    {
//        iv = [[UIImageView alloc] initWithFrame:CGRectMake((self.contentView.bounds.size.width - WidthRate(40))/2, HeightRate(35), WidthRate(40), WidthRate(40))];
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(30), WidthRate(15), WidthRate(40), WidthRate(40))];
        [self.contentView addSubview:iv];
        
//        lb = [[UILabel alloc] initWithFrame:CGRectMake(0, HeightRate(100), self.contentView.bounds.size.width, HeightRate(25))];
        lb = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(15), WidthRate(15), self.contentView.bounds.size.width - WidthRate(15), WidthRate(40))];
        lb.textColor = [UIColor whiteColor];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont boldSystemFontOfSize:14];
        [self.contentView addSubview:lb];
    }
}

- (void)setImage:(UIImage *)image title:(NSString *)string
{
    iv.image = image;
    lb.text = string;
}
@end
