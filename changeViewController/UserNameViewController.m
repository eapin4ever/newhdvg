//
//  UserNameViewController.m
//  changeViewController
//
//  Created by P&M on 14/12/3.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "UserNameViewController.h"
#import "MobileAuthcodeViewController.h"
#import "NewPassViewController.h"
#import "MasterViewController.h"

@interface UserNameViewController () <UITextViewDelegate>

@property (assign,nonatomic) BOOL isPush;

@end

@implementation UserNameViewController
{
    NSInteger _textView;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"资料管理";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createUserInfomationUI];
    
    // 初始化完成按钮
    UIBarButtonItem *finishedButton = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(userNameSetFinishClick:)];
    
    [finishedButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:NAVTEXTCOLOR, NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = finishedButton;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 注册通知，监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];
    // 注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
    
    
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    //self.nicknameTextField.text = [userDefaultes stringForKey:@"nicknameString"];
    self.sexTextField.text = [userDefaultes stringForKey:@"sexString"];
    
    if (self.sexTextField.text.length == 0) {
        
        // 从服务器获取用户数据
        PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
        NSDictionary *dict = userInfos.userDataDic;
        
        if ([[[dict objectNullForKey:@"clientUser"] objectNullForKey:@"sex"] isEqual:[NSNull null]]) {
            
            self.sexTextField.text = @"";
        }
        else {
            self.sexTextField.text = [[[dict objectNullForKey:@"clientUser"] objectNullForKey:@"sex"] intValue] == 1 ? @"男" : @"女";
        }
    }
    else {
        NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
        self.sexTextField.text = [userDefaultes stringForKey:@"sexString"];
    }
}

// 创建用户姓名 UI
- (void)createUserInfomationUI
{
    // 创建一个背景空白 view
    self.userInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 250)];
    self.userInfoView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.userInfoView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.userInfoView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(26), 44, WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.userInfoView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(WidthRate(26), 88, WIDTH, HeightRate(1));
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.userInfoView.layer addSublayer:layer3];
    
    CALayer *layer4 = [[CALayer alloc] init];
    layer4.frame = CGRectMake(WidthRate(26), 132, WIDTH, HeightRate(1));
    layer4.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.userInfoView.layer addSublayer:layer4];
    
    CALayer *layer5 = [[CALayer alloc] init];
    layer5.frame = CGRectMake(WidthRate(26), 176, WIDTH, HeightRate(1));
    layer5.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.userInfoView.layer addSublayer:layer5];
    
    CALayer *layer7 = [[CALayer alloc] init];
    layer7.frame = CGRectMake(0, 250 - 0.5, WIDTH, 0.5);
    layer7.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.userInfoView.layer addSublayer:layer7];
    
    
    /**
     *  从服务器获取用户数据
     */
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    NSDictionary *dict = userInfos.userDataDic;
    
    // 新昵称
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(32), 7, WidthRate(200), 30)];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = @"用  户  名";
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.textColor = RGBA(75, 75, 75, 1);
    nameLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.userInfoView addSubview:nameLabel];
    
    // 用户名
    UILabel *userNameLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(520), 7, WidthRate(450), 30)];
    userNameLab.backgroundColor = [UIColor clearColor];
    if ([[[dict objectNullForKey:@"clientUser"] objectNullForKey:@"name"] isEqual:[NSNull null]]) {
        userNameLab.text = @"";
    }
    else {
        userNameLab.text = [[dict objectNullForKey:@"clientUser"] objectNullForKey:@"name"];
    }
    userNameLab.textColor = RGBA(81, 81, 81, 1);
    userNameLab.textAlignment = NSTextAlignmentRight;
    userNameLab.font = [UIFont systemFontOfSize:15.0f];
    [self.userInfoView addSubview:userNameLab];
    
    
    // 性别
    UILabel *sexLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 51, WidthRate(200), 30)];
    sexLabel.backgroundColor = [UIColor clearColor];
    sexLabel.text = @"性       别";
    sexLabel.textAlignment = NSTextAlignmentLeft;
    sexLabel.textColor = RGBA(75, 75, 75, 1);
    sexLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.userInfoView addSubview:sexLabel];
    
    self.sexTextField = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(470), 51, WidthRate(400), 30)];
    self.sexTextField.borderStyle = UITextBorderStyleNone;
    self.sexTextField.delegate = self;
    self.sexTextField.enabled = NO;
    self.sexTextField.placeholder = @"请选择性别";
    self.sexTextField.textColor = RGBA(81, 81, 81, 1);
    if ([[[dict objectNullForKey:@"clientUser"] objectNullForKey:@"sex"] isEqual:[NSNull null]]) {
        
        self.sexTextField.text = @"";
    }
    else {
        self.sexTextField.text = [[[dict objectNullForKey:@"clientUser"] objectNullForKey:@"sex"] intValue] == 1 ? @"男" : @"女";
    }
    self.sexTextField.textAlignment = NSTextAlignmentRight;
    self.sexTextField.font = [UIFont systemFontOfSize:15.0f];
    [self.userInfoView addSubview:self.sexTextField];
    
    UIButton *sexButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sexButton.frame = CGRectMake(WidthRate(160), 45, WIDTH - WidthRate(160), 42);
    [sexButton addTarget:self action:@selector(sexButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.userInfoView addSubview:sexButton];
    
    
//    // 邮箱
//    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 95, WidthRate(200), 30)];
//    emailLabel.backgroundColor = [UIColor clearColor];
//    emailLabel.text = @"邮       箱";
//    emailLabel.textColor = RGBA(75, 75, 75, 1);
//    emailLabel.textAlignment = NSTextAlignmentLeft;
//    emailLabel.font = [UIFont systemFontOfSize:16.0f];
//    [self.userInfoView addSubview:emailLabel];
//    
//    UILabel *emailNumberLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(500), 95, WidthRate(430), 30)];
//    emailNumberLab.backgroundColor = [UIColor clearColor];
//    
//    NSString *email = [[dict objectNullForKey:@"clientUser"] objectNullForKey:@"email"];
//    
//    if (isNull([[dict objectNullForKey:@"clientUser"] objectNullForKey:@"email"]) || [email isEqualToString:@"null"])
//    {
//        
//        emailNumberLab.text = @"未绑定邮箱，可到官网绑定";
//        emailNumberLab.textColor = RGBA(204, 204, 204, 1);
//    }
//    else
//    {
//        emailNumberLab.text = email;
//        emailNumberLab.textColor = RGBA(81, 81, 81, 1);
//    }
//    
//    emailNumberLab.textAlignment = NSTextAlignmentRight;
//    emailNumberLab.font = [UIFont systemFontOfSize:15.0f];
//    [self.userInfoView addSubview:emailNumberLab];
    
    
    // 手机号码
    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 95, WidthRate(200), 30)];
    phoneLabel.backgroundColor = [UIColor clearColor];
    phoneLabel.text = @"手机号码";
    phoneLabel.textColor = RGBA(75, 75, 75, 1);
    phoneLabel.textAlignment = NSTextAlignmentLeft;
    phoneLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.userInfoView addSubview:phoneLabel];
    
    self.mobileNumberLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(500), 95, WidthRate(430), 30)];
    self.mobileNumberLab.backgroundColor = [UIColor clearColor];
    self.mobileNumberLab.text = [[dict objectNullForKey:@"clientUser"] objectNullForKey:@"mobile"];
    self.mobileNumberLab.textColor = RGBA(81, 81, 81, 1);
    self.mobileNumberLab.textAlignment = NSTextAlignmentRight;
    self.mobileNumberLab.font = [UIFont systemFontOfSize:15.0f];
    [self.userInfoView addSubview:self.mobileNumberLab];
    
    // 修改手机号码按钮
    UIButton *mobileButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mobileButton.frame = CGRectMake(0, 89, WIDTH, 42);
    [mobileButton addTarget:self action:@selector(changeMobileClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.userInfoView addSubview:mobileButton];
    
    UIImageView *arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 100, 20, 20)];
    [arrowImage setImage:[UIImage imageNamed:@"arrowRight"]];
    [self.userInfoView addSubview:arrowImage];
    
    
    // 登录密码
    UILabel *loginPassLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 139, WidthRate(200), 30)];
    loginPassLabel.backgroundColor = [UIColor clearColor];
    loginPassLabel.text = @"修改密码";
    loginPassLabel.textColor = RGBA(75, 75, 75, 1);
    loginPassLabel.textAlignment = NSTextAlignmentLeft;
    loginPassLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.userInfoView addSubview:loginPassLabel];
    
    // 登录密码按钮
    UIButton *loginPassButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginPassButton.frame = CGRectMake(0, 133, WIDTH, 42);
    [loginPassButton addTarget:self action:@selector(changePassButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.userInfoView addSubview:loginPassButton];
    
    UIImageView *arrowImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 30, 144, 20, 20)];
    [arrowImage2 setImage:[UIImage imageNamed:@"arrowRight"]];
    [self.userInfoView addSubview:arrowImage2];
    
    UILabel *remarkLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 183, WidthRate(200), 30)];
    remarkLB.backgroundColor = [UIColor clearColor];
    remarkLB.text = @"个性签名";
    remarkLB.textColor = RGBA(75, 75, 75, 1);
    remarkLB.textAlignment = NSTextAlignmentLeft;
    remarkLB.font = [UIFont systemFontOfSize:16.0f];
    [self.userInfoView addSubview:remarkLB];
    
    // 个性签名
    self.remarkTextView = [[UITextView alloc] initWithFrame:CGRectMake(WidthRate(210), 183, WidthRate(500), 60)];
    self.remarkTextView.delegate = self;
    self.remarkTextView.textColor = RGBA(81, 81, 81, 1);
    if ([[[dict objectNullForKey:@"clientUser"] objectNullForKey:@"remark"] isEqual:[NSNull null]]) {
        
        self.remarkTextView.text = @"这家伙很懒，什么都没有写!";
    }
    else {
        self.remarkTextView.text = [[dict objectNullForKey:@"clientUser"] objectNullForKey:@"remark"];
    }
    self.remarkTextView.font = [UIFont systemFontOfSize:15.0f];
    self.remarkTextView.layer.cornerRadius = 6.0f;  // 设置圆角值
    self.remarkTextView.layer.masksToBounds = YES;
    self.remarkTextView.layer.borderWidth = 0.5f;   // 设置边框大小
    self.remarkTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.remarkTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.remarkTextView.returnKeyType = UIReturnKeyDone;
    self.remarkTextView.keyboardType = UIKeyboardTypeDefault;
    [self.userInfoView addSubview:self.remarkTextView];
}

// 回收软键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nicknameTextField resignFirstResponder];
    [self.remarkTextView resignFirstResponder];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([[[UITextInputMode currentInputMode]primaryLanguage] isEqualToString:@"emoji"]) {
        return NO;
    }
//    if ([self isContainsEmoji:textField.text])
//    {
//        return NO;
//    }

    if (self.nicknameTextField.text.length > 12) {
        return NO;
    }
    
    return YES;
}

// 隐藏键盘，实现UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        return NO;
    }
    
    if ([[[UITextInputMode currentInputMode]primaryLanguage] isEqualToString:@"emoji"]) {
        return NO;
    }
//    if ([self isContainsEmoji:text])
//    {
//        return NO;
//    }
    
    // 判断输入是否已经超出最大可输入长度
    if ([text isEqualToString:@""] && range.length > 0) {
        
        //删除字符肯定是安全的
        return YES;
    }
    else {
        NSInteger maxLength = 25;
        NSInteger strLength = textView.text.length - range.length + text.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

#pragma mark - 解决textView输入时键盘遮挡问题
// 监听事件
- (void)handleKeyboardDidShow:(NSNotification *)paramNotification
{
    // 让出现键盘只执行一次，当_textView ＝ 0 时，才向上移动30像素
    if (_textView == 0) {
        // 只针对3.5屏幕向上移动30像素
        [self moveView:(iPhone4s ? -170 : -130)];
        
        _textView = 1;
    }
}

- (void)handleKeyboardDidHidden
{
    [self moveView:(iPhone4s ? 170 : 130)];
    
    // 当键盘消失时，向下移动30像素 ,把_textView置0，
    _textView = 0;
}

- (void)moveView:(CGFloat)move
{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y += move;//view的y轴上移
    self.view.frame = frame;
    [UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)sexButtonClick:(id)sender
{
    [self.nicknameTextField resignFirstResponder];
    
    // 性别修改
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"性别修改" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"男" otherButtonTitles:@"女", nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1) {
        // buttonIndex判断点击的是那个按钮，第一个的buttonIndex是男,第二个是女.
        if (buttonIndex==0) {
            
            self.sexTextField.text = @"男";
        }
        else if (buttonIndex==1) {
            
            self.sexTextField.text = @"女";
        }
    }
}


- (void)changeMobileClick:(UIButton *)sender
{
    [self.remarkTextView resignFirstResponder];
    
    // 手机号码
    MobileAuthcodeViewController *mobileAuthcodeVC = [[MobileAuthcodeViewController alloc] init];
    NSString *string = @"changeMobileAuthcode";
    mobileAuthcodeVC.mobileOrBankAuthcode = string;
    [self.navigationController pushViewController:mobileAuthcodeVC animated:YES];
}

- (void)changePassButtonClick:(UIButton *)sender
{
    [self.remarkTextView resignFirstResponder];
    
    // 登录密码
    NewPassViewController *newPassVC = [[NewPassViewController alloc] init];
    [self.navigationController pushViewController:newPassVC animated:YES];
}


- (void)userNameSetFinishClick:(id)sender
{
    [self.remarkTextView resignFirstResponder];
    
    /**
     *  从服务器获取用户数据
     */
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    
    if (self.sexTextField.text.length != 0) {
        
        if ([self.sexTextField.text isEqualToString:@"男"]) {
            self.sexString = @"1";
        }
        else {
            self.sexString = @"-1";
        }
    }
    else {
        self.sexString = @"";
    }
    
    if (self.sexTextField.text.length != 0) {
        
        /**
         *  用户修改昵称
         *  参数：update:(string)
         */
        NSDictionary *update = @{@"PM_SID":userInfos.PM_SID, @"sex":self.sexString,@"remark":self.remarkTextView.text};
        self.networking = [PMNetworking defaultNetworking];
        
        __block NSDictionary *callBackDict;
        
        [self.networking request:PMRequestStateMyUpdate WithParameters:update callBackBlock:^(NSDictionary *dict) {
           
            callBackDict = dict;
            
            if ([[callBackDict objectNullForKey:@"success"] boolValue] == 1) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"我的资料设置成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alertView.tag = 1;
                [alertView show];
                
                NSMutableDictionary *muDic = [[[PMUserInfos shareUserInfo].userDataDic objectNullForKey:@"clientUser"] mutableCopy];
                [muDic setValue:self.nicknameTextField.text forKey:@"nickName"];
                [muDic setValue:self.remarkTextView.text forKey:@"remark"];
                [[PMUserInfos shareUserInfo].userDataDic setValue:muDic forKey:@"clientUser"];
                
                //1、获取一个NSUserDefaults引用：
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                //2、保存数据
                [userDefaults setObject:self.sexTextField.text forKey:@"sexString"];
//                [userDefaults setObject:self.nicknameTextField.text forKey:@"nicknameString"];
                [userDefaults synchronize];
                
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[callBackDict objectNullForKey:@"message"] message: [callBackDict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }showIndicator:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)isContainsEmoji:(NSString *)string {
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}


@end
