//
//  WebImageCell.h
//  changeViewController
//
//  Created by ZhangEapin on 15/4/26.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebImageCell : UITableViewCell <UIWebViewDelegate>

@property (copy,nonatomic) NSString *urlString;
@property (strong,nonatomic) UIWebView *imageWeb;
@property (assign,nonatomic) double rowHeight;

- (void)createUI;
- (void)loadWeb;

@end
