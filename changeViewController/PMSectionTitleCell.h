//
//  PMSectionTitleCell.h
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMSectionTitleCell : UITableViewCell
- (void)createUI;

- (void)setTitle:(NSString *)title image:(UIImage *)image;
@end
