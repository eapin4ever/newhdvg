//
//  YBPayCell.h
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YBSureOrderViewController;

@interface YBPayCell : UITableViewCell

@property (nonatomic,strong) UIButton *currentSelectButton;
@property (weak,nonatomic) YBSureOrderViewController *myDelegate;

@end
