//
//  PMStoreScreenTitleCell.h
//  changeViewController
//
//  Created by P&M on 15/7/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMStoreScreenTitleCell : UITableViewCell

@property (strong, nonatomic) UIButton *selectedBtn;
@property (strong, nonatomic) UILabel *titleLabel;

- (void)createScreenTitleUI;

- (void)setStoreScreenTitle:(NSString *)title;

@end
