//
//  PMScreenTableViewCell.h
//  changeViewController
//
//  Created by P&M on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMScreenTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong,nonatomic) UILabel *tipLB;

- (void)createScreenUI;

- (void)setScreenTitle:(NSString *)title;

@end
