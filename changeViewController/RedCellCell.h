//
//  RedCellCell.h
//  changeViewController
//
//  Created by EapinZhang on 15/3/23.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedCellCell : UITableViewCell

@property (strong,nonatomic) UIImageView *redIV;
@property (strong,nonatomic) UILabel *timeLabel;
@property (strong,nonatomic) UILabel *redMoneyLabel;

@property (copy,nonatomic) NSString *perRedMoney;

@end
