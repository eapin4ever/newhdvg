//
//  OrderDetailProductCell.m
//  changeViewController
//
//  Created by pmit on 15/10/27.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "OrderDetailProductCell.h"
#import "PMMyPhoneInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation OrderDetailProductCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.productIV)
    {
        self.productIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, WidthRate(180), WidthRate(180))];
        self.productIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.productIV];
        
        self.productLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.productIV.frame) + 10, 10, WIDTH - CGRectGetMaxX(self.productIV.frame) - 10 - 10, 20)];
        self.productLB.textAlignment = NSTextAlignmentLeft;
        self.productLB.textColor = RGBA(59, 59, 59, 1);
        self.productLB.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:self.productLB];
        
        self.productPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.productIV.frame) + 10, WidthRate(180) - 10, (WIDTH - CGRectGetMaxX(self.productIV.frame) - 10 - 15) / 2, 20)];
        self.productPriceLB.textAlignment = NSTextAlignmentLeft;
        self.productPriceLB.font = [UIFont systemFontOfSize:16.0f];
        self.productPriceLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.productPriceLB];
        
        self.productCountLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.productPriceLB.frame),  WidthRate(180) - 10, (WIDTH - CGRectGetMaxX(self.productIV.frame) - 10 - 15) / 2, 20)];
        self.productCountLB.textAlignment = NSTextAlignmentRight;
        self.productCountLB.font = [UIFont systemFontOfSize:16.0f];
        self.productCountLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.productCountLB];
        
        CALayer *line = [CALayer layer];
        line.frame = CGRectMake(0, WidthRate(180) + 19, WIDTH, 1);
        line.backgroundColor = RGBA(241, 241, 241, 1).CGColor;
        [self.contentView.layer addSublayer:line];
    }
}

- (void)setCellData:(NSDictionary *)productDic
{
    NSDictionary *inProductDic = [productDic objectForKey:@"product"];
    NSString *productLogoString = [[inProductDic objectForKey:@"productLogo"] isKindOfClass:[NSString class]] && ![[inProductDic objectForKey:@"productLogo"] isEqualToString:@""] ? [inProductDic objectForKey:@"productLogo"] : @"";
    [self.productIV sd_setImageWithURL:[NSURL URLWithString:productLogoString] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    self.productLB.text = [inProductDic objectForKey:@"productName"];
    NSString *priceString = [NSString stringWithFormat:@"￥%.2lf",[[inProductDic objectForKey:@"productPrice"] doubleValue]];
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} range:NSMakeRange(0, 1)];
    self.productPriceLB.attributedText = nodeString;
    self.productCountLB.text = [NSString stringWithFormat:@"x %@",@([[productDic objectForKey:@"productCount"] integerValue])];
    
}



@end
