//
//  YBPayCell.m
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#define grayColor RGBA(231, 231, 231, 1)

#define itemX WidthRate(30)

#import "YBPayCell.h"
#import "PMMyPhoneInfo.h"
#import "YBSureOrderViewController.h"
#import "WXApi.h"
#import "AppDelegate.h"

@implementation YBPayCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self createUI];
}

#pragma mark - 创建支付方法按钮
- (void)createUI
{
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(itemX, HeightRate(30), WidthRate(160), HeightRate(40))];
    lb.text = @"付款方式";
    lb.font = [UIFont systemFontOfSize:16.0f];
    lb.textColor = [UIColor blackColor];
    [self.contentView addSubview:lb];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.currentSelectButton = btn;
//    self.currentSelectButton.selected = YES;
//    [self.currentSelectButton setBackgroundColor:HDVGRed];
    [self setButtonSelected:btn];
    btn.frame = CGRectMake(itemX, HeightRate(90), WidthRate(230), HeightRate(60));
    [btn.layer setBorderColor:RGBA(180, 24, 41, 1).CGColor];
    [btn.layer setBorderWidth:0.5];
//    [btn setTitle:@"支付宝" forState:UIControlStateNormal];
//    [btn setTitleColor:RGBA(203, 45, 54, 1) forState:UIControlStateNormal];
//    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
//    btn.titleLabel.font = [UIFont systemFontOfSize:HeightRate(26)];
    [btn setImage:[UIImage imageNamed:@"zhifubao_image"] forState:UIControlStateNormal];
    btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    btn.layer.cornerRadius = 6.0;
    btn.layer.borderColor = HDVGRed.CGColor;
    btn.layer.borderWidth = 1.0;
    btn.tag = 1;
    [btn addTarget:self action:@selector(changePayWay:) forControlEvents:UIControlEventTouchDown];
    [self.contentView addSubview:btn];
    
    UIButton *wxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    wxBtn.frame = CGRectMake(itemX + WidthRate(230 + 40), HeightRate(90), WidthRate(230), HeightRate(60));
    [wxBtn.layer setBorderColor:RGBA(180, 24, 41, 1).CGColor];
    [wxBtn setImage:[UIImage imageNamed:@"weizhifu"] forState:UIControlStateNormal];
    wxBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    wxBtn.layer.cornerRadius = 6.0;
    wxBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    wxBtn.layer.borderWidth = 1.0;
    wxBtn.tag = 101;
    [wxBtn addTarget:self action:@selector(changePayWay:) forControlEvents:UIControlEventTouchDown];
    [self.contentView addSubview:wxBtn];
    
    
}

#pragma mark - 切换支付方法
- (void)changePayWay:(UIButton *)button
{
    if (button.tag > 100)
    {
        if ([WXApi isWXAppInstalled])
        {
            self.myDelegate.isWX = YES;
            [self setButtonSelected:button];
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            app.isWXPay = YES;
    
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"您还未安装微信，请先安装微信" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    else
    {
        self.myDelegate.isWX = NO;
        [self setButtonSelected:button];
    }
}

#pragma mark - 设置按钮选中
- (void)setButtonSelected:(UIButton *)button
{
    self.currentSelectButton.selected = NO; //将原本选择的按钮的状态设置为普通
    self.currentSelectButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.currentSelectButton = button; //改变当前支付方式的按钮
    self.currentSelectButton.selected = YES; //将当前的支付方式按钮重新设置为被选择
    self.currentSelectButton.layer.borderColor = HDVGRed.CGColor;
}

@end
