//
//  PMOtherOrderDelegate.h
//  changeViewController
//
//  Created by pmit on 15/10/29.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PMOrderType)
{
    PMOrderTypeWaitPay,
    PMOrderTypeWaitGet,
    PMOrderTypeWaitDiscuss,
};

@protocol PMOtherOrderDelegateDelegate <NSObject>

- (void)delegateBtnClick:(UIButton *)sender;
- (void)goToDetailOrder:(NSArray *)orderArr;

@end

@interface PMOtherOrderDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *orderArr;
@property (assign,nonatomic) PMOrderType orderType;
@property (weak,nonatomic) id<PMOtherOrderDelegateDelegate> doubleDelegate;
@property (assign,nonatomic) NSInteger waitPayPage;
@property (assign,nonatomic) NSInteger waitGetPage;
@property (assign,nonatomic) NSInteger waitDiscussPage;

@end
