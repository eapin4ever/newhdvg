//
//  PMBrowser.m
//  changeViewController
//
//  Created by pmit on 14/11/8.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMBrowser.h"

@interface PMBrowser()
@property(nonatomic,strong)UINavigationController *webBrowserNavigationController;
@end

static PMBrowser *_browser;
static KINWebBrowserViewController *_webBrowser;
@implementation PMBrowser
+ (PMBrowser *)defaultWebBrowserWithController:(id<KINWebBrowserDelegate>)viewController
{
    if(!_browser || !_webBrowser)
    {
        _browser = [[PMBrowser alloc] init];
//        _browser.webBrowserNavigationController = [KINWebBrowserViewController navigationControllerWithWebBrowser];
//        _webBrowser = [_browser.webBrowserNavigationController rootWebBrowser];
//        _webBrowser.delegate = viewController;
        _webBrowser = [KINWebBrowserViewController webBrowser];
        [_webBrowser setDelegate:viewController];
    }
    return _browser;
}


#pragma mark - 进入店铺
- (void)getInShop:(NSString *)address ByPushController:(UIViewController *)viewController;
{
    self.currentURL = address;
    [self loadUrl:address andController:viewController];
}

- (void)loadUrl:(NSString *)address andController:(UIViewController *)viewController
{
    [viewController.navigationController pushViewController:_webBrowser animated:YES];
    viewController.navigationController.toolbarHidden = YES;
    
    [_webBrowser loadURLString:address];
}


@end
