//
//  AddressPickerViewController.h
//  changeViewController
//
//  Created by pmit on 15/9/15.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddressPickerViewControllerDelegate <NSObject>

- (void)finishPick:(NSString *)provinceId CityId:(NSString *)cityId AreaId:(NSString *)areaId ProvinceValue:(NSString *)provinceName CityName:(NSString *)cityName AreaName:(NSString *)areaName;

@end

@interface AddressPickerViewController : UIViewController

@property (copy,nonatomic) NSString *provinceId;
@property (copy,nonatomic) NSString *cityId;
@property (copy,nonatomic) NSString *areaId;
@property (copy,nonatomic) NSString *provinceName;
@property (weak,nonatomic) id<AddressPickerViewControllerDelegate> addressDelegate;

@end
