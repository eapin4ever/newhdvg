//
//  PMResizeImage.m
//  changeViewController
//
//  Created by pmit on 14/12/2.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMResizeImage.h"

@implementation PMResizeImage
+ (UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize
{
    
    UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
    
    [image drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
    
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    
    return reSizeImage;
}

#pragma mark - 从连接得到图片
+ (UIImage *)getImageWithURLString:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    return [UIImage imageWithData:data];
}

+ (void)getImageWithURLString:(NSString *)urlString imageView:(UIImageView *)imageView
{
    __block NSData *data;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //输入子线程代码
        NSURL *url = [NSURL URLWithString:urlString];
        data = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //输入回到主线程后的代码
            imageView.image = [UIImage imageWithData:data];
        });
    });
    
}

@end
