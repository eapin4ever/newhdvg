//
//  RelevantViewController.h
//  changeViewController
//
//  Created by P&M on 14/12/2.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMUserInfos.h"
#import "PMNetworking.h"

@interface RelevantViewController : UIViewController <UITextViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIView *relevantView;

@property (strong, nonatomic) UILabel *numberLabel;
@property (strong, nonatomic) UIButton *relevantBtn;

@property (strong, nonatomic) UIView *describeView;
@property (strong, nonatomic) UITextView *reasonTextView;
@property (strong, nonatomic) UILabel *tipLabel;

@property (strong, nonatomic) UILabel *flagLabel;
@property (strong, nonatomic) NSDictionary *relevantDict;

@property (strong, nonatomic) PMNetworking *networking;

@end
