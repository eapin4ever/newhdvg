//
//  PMStoreScreenDetailCell.h
//  changeViewController
//
//  Created by P&M on 15/7/17.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMStoreScreenDetailCell : UITableViewCell

@property (strong, nonatomic) UILabel *detailTitleLB;
@property (strong,nonatomic) UIImageView *checkIV;

- (void)createScreenDetailUI;

- (void)setStoreScreenDetails:(NSString *)title;

@end
