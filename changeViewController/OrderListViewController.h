//
//  OrderListViewController.h
//  changeViewController
//
//  Created by ZhangEapin on 15/5/6.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderListViewController : UIViewController

@property (strong,nonatomic) NSArray *productArr;
@property (assign,nonatomic) BOOL isTradeBack;
@property (assign,nonatomic) BOOL isFromOrderDetail;

@end
