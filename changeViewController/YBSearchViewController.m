//
//  YBSearchViewController.m
//  changeViewController
//
//  Created by pmit on 15/6/27.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBSearchViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "MasterViewController.h"
#import "PMHistorySearchCell.h"
#import "SearchResultViewController.h"

@interface YBSearchViewController () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (strong,nonatomic) UITableView *historyTableView;
@property (strong,nonatomic) UITableView *keyTableView;
@property (strong,nonatomic) NSMutableArray *historyArr;
@property (strong,nonatomic) UITextField *keyWordTF;
@property (strong,nonatomic) NSMutableArray *keyArr;
@property (assign,nonatomic) BOOL isKey;
@property (assign,nonatomic) NSInteger currentPage;
@property (strong,nonatomic) NSMutableArray *hasRefreshArr;

@end

@implementation YBSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildNavigationBar];
    [self buildHistoryView];
    self.currentPage = 1;
    [self.keyWordTF becomeFirstResponder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeKey) name:UITextFieldTextDidChangeNotification object:self.keyWordTF];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getHistoryByDefault];
}

- (void)getHistoryByDefault
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (![ud objectForKey:@"history"]) {
        self.historyArr = [NSMutableArray array];
    }
    else
    {
        self.historyArr = [[ud objectForKey:@"history"] mutableCopy];
    }
    
    [self.historyTableView reloadData];
}

- (void)buildNavigationBar
{
    self.keyWordTF = [[UITextField alloc] initWithFrame:CGRectMake(0, 7, WIDTH - WidthRate(250), 30)];
    [self.keyWordTF setBorderStyle:UITextBorderStyleRoundedRect];
    //搜索框背景色
    [self.keyWordTF setBackgroundColor:[UIColor whiteColor]];
    [self.keyWordTF.layer setCornerRadius:5.0];
    [self.keyWordTF.layer setBorderWidth:0];
    self.keyWordTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    //预留字f
    self.keyWordTF.placeholder = @"输入关键字搜索商品";
    [self.keyWordTF setValue:[UIFont boldSystemFontOfSize:11] forKeyPath:@"_placeholderLabel.font"];
    [self.keyWordTF setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, self.keyWordTF.bounds.size.width, self.keyWordTF.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [self.keyWordTF setValue:NAVTEXTCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    
    self.keyWordTF.returnKeyType = UIReturnKeySearch;
    self.keyWordTF.textColor = [UIColor darkGrayColor];
    self.keyWordTF.font = [UIFont systemFontOfSize:12];
    //搜索栏中的放大镜
    UIImage * image = [UIImage imageNamed:@"search"];
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    searchIcon.image = image;
    searchIcon.contentMode = UIViewContentModeScaleAspectFit;
    
    self.keyWordTF.leftView = searchIcon;
    self.keyWordTF.leftViewMode = UITextFieldViewModeAlways;
    //设置代理，当点击键盘的搜索后就开始搜索
    [self.keyWordTF setDelegate:self];
    self.navigationItem.titleView = self.keyWordTF;
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(0, 0, WidthRate(125), 30);
    [searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(searchByKeyWord) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setTitleColor:NAVTEXTCOLOR forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
}

- (void)buildHistoryView
{
    self.historyTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT) style:UITableViewStylePlain];
    self.historyTableView.backgroundColor = [UIColor whiteColor];
    self.historyTableView.delegate = self;
    self.historyTableView.dataSource = self;
    self.historyTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.historyTableView registerClass:[PMHistorySearchCell class] forCellReuseIdentifier:@"historyCell"];
    [self.view addSubview:self.historyTableView];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isKey)
    {
        return self.keyArr.count;
    }
    else
    {
        return self.historyArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMHistorySearchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"historyCell"];
    [cell createUI];
    if (self.isKey)
    {
        NSDictionary *keyDic = self.keyArr[indexPath.row];
        cell.historyLB.text = [keyDic objectNullForKey:@"keyword"];
        if (indexPath.row == 5)
        {
            if ([self.hasRefreshArr containsObject:@(indexPath.row)])
            {
                
            }
            else
            {
                self.currentPage++;
                [self geTSearchKeyWithKeys:self.keyWordTF.text];
                [self.hasRefreshArr addObject:@(indexPath.row)];
            }
        }
    }
    else
    {
        
        cell.historyLB.text = self.historyArr[indexPath.row];
       
    }
     return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *keyword = @"";
    if (self.isKey)
    {
        keyword = [self.keyArr[indexPath.row] objectNullForKey:@"keyword"];
    }
    else
    {
       keyword = self.historyArr[indexPath.row];
    }
    
    [self searchWithString:keyword];
}

- (void)searchByKeyWord
{
    //收键盘
    [self.keyWordTF resignFirstResponder];
    
    BOOL isExist = NO;
    for (NSString *history in self.historyArr)
    {
        if ([history isEqualToString:self.keyWordTF.text])
        {
            isExist = YES;
            break;
        }
        else
        {
            isExist = NO;
        }
    }
    
    if(![self.keyWordTF.text isEqualToString:@""])
    {
        [self searchWithString:self.keyWordTF.text];
    }
    
    if (!isExist)
    {
        [self.historyArr addObject:self.keyWordTF.text];
        [[NSUserDefaults standardUserDefaults] setObject:self.historyArr forKey:@"history"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//点击虚拟键盘上的search开始搜索
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self searchByKeyWord];
    
    return YES;
}

- (void)searchWithString:(NSString *)string
{
    SearchResultViewController *searchVC = [[SearchResultViewController alloc] init];
    searchVC.searchString = string;
    searchVC.secondClassId = @"";
    [self.navigationController pushViewControllerWithNavigationControllerTransition:searchVC];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *headerTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), 0, WIDTH, 60)];
    headerTitleLB.text = @"搜索历史";
    headerTitleLB.textColor = [UIColor blackColor];
    headerTitleLB.font = [UIFont systemFontOfSize:20.0f];
    [view addSubview:headerTitleLB];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.isKey)
    {
        return 0;
    }
    else
    {
        return 60;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (self.isKey)
    {
        return nil;
    }
    else
    {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
        footerView.backgroundColor = [UIColor whiteColor];
    
        UIButton *clearHistoryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [clearHistoryBtn setTitle:@"清除历史记录" forState:UIControlStateNormal];
        [clearHistoryBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [clearHistoryBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        clearHistoryBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        clearHistoryBtn.layer.cornerRadius = 8.0;
        clearHistoryBtn.frame = CGRectMake(0, 0, WidthRate(240), 30);
        clearHistoryBtn.center = footerView.center;
        clearHistoryBtn.layer.borderWidth = 1.5;
        clearHistoryBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [clearHistoryBtn addTarget:self action:@selector(clickToClearSearchHistory:) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:clearHistoryBtn];
    
        return footerView;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.isKey)
    {
        return 0;
    }
    else
    {
        return 30;
    }
}

- (void)clickToClearSearchHistory:(UIButton *)sender
{
    [self.historyArr removeAllObjects];
    [self.historyTableView reloadData];
    [[NSUserDefaults standardUserDefaults] setValue:self.historyArr forKey:@"history"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.keyWordTF resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if ([[[UITextInputMode currentInputMode]primaryLanguage] isEqualToString:@"emoji"]) {
//        return NO;
//    }
//    if ([self isContainsEmoji:string])
//    {
//        return NO;
//    }
    
    return YES;
}

- (void)changeKey
{
    if (![self.keyWordTF.text isEqualToString:@""])
    {
        self.keyArr = [NSMutableArray array];
        self.hasRefreshArr = [NSMutableArray array];
        [self geTSearchKeyWithKeys:self.keyWordTF.text];
    }
    else
    {
        self.isKey = NO;
        [self.historyTableView reloadData];
    }
    
}

- (void)geTSearchKeyWithKeys:(NSString *)key
{
    [[PMNetworking defaultNetworking] request:PMRequestStateSearchKey WithParameters:@{@"currentPage":@(self.currentPage),@"pageSize":@(10),@"keyword":key} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            self.isKey = YES;
            if ([[dic objectNullForKey:@"data"] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dataDic = [dic objectNullForKey:@"data"];
                NSArray *beanArr = [dataDic objectNullForKey:@"beanList"];
                self.keyArr = [beanArr mutableCopy];
                [self.historyTableView reloadData];
            }
        }
        
    } showIndicator:NO];
}

- (BOOL)isContainsEmoji:(NSString *)string {
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}

@end
