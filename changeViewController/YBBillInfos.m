//
//  YBBillInfos.m
//  changeViewController
//
//  Created by EapinZhang on 15/3/9.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBBillInfos.h"

@implementation YBBillInfos

static YBBillInfos *_instance;

+ (instancetype)shareInstance
{
    if (!_instance)
    {
        _instance = [[YBBillInfos alloc] init];
    }
    
    return _instance;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super allocWithZone:zone];
    });
    return _instance;
}

- (void)initWithBillNum:(NSString *)billNum andType:(NSInteger)billType andBillContent:(NSInteger)billContent
{
    self.billNum = billNum;
    self.billType = billType;
    self.billContent = billContent;
}

@end
