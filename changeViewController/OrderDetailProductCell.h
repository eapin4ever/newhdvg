//
//  OrderDetailProductCell.h
//  changeViewController
//
//  Created by pmit on 15/10/27.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailProductCell : UITableViewCell

@property (strong,nonatomic) UIImageView *productIV;
@property (strong,nonatomic) UILabel *productLB;
@property (strong,nonatomic) UILabel *productPriceLB;
@property (strong,nonatomic) UILabel *productCountLB;
@property (strong,nonatomic) UILabel *productSpecLB;

- (void)createUI;
- (void)setCellData:(NSDictionary *)productDic;

@end
