//
//  HomeHeaderImageCell.h
//  changeViewController
//
//  Created by pmit on 15/7/7.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeHeaderImageCell : UITableViewCell

@property (strong,nonatomic) UIImageView *headerImageView;

- (void)createUI;
- (void)setCellHeaderImageView:(NSString *)urlString;

@end
