//
//  PMSureOrderTVCell.m
//  changeViewController
//
//  Created by pmit on 14/12/17.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMSureOrderTVCell.h"

@implementation PMSureOrderTVCell
{
    UILabel *titleLB;
    UILabel *classifyLB1;
    UILabel *classifyLB2;
    UILabel *distanceLB;
    UILabel *locationLB;
    UILabel *priceLB;
    UILabel *numLB;
}

- (void)createUI
{
    if(!_iv)
    {
        //图片
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(30), HeightRate(180), HeightRate(180))];
        _iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iv];
        
        self.activeIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        self.activeIV.image = [UIImage imageNamed:@"activeTip"];
        self.activeIV.contentMode = UIViewContentModeScaleAspectFit;
        self.activeIV.hidden = YES;
        [_iv addSubview:self.activeIV];
        
        //商品标题或介绍
        titleLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(240), 0, WidthRate(460), 45)];
        titleLB.font = [UIFont systemFontOfSize:14.0f];
        titleLB.textColor = RGBA(100, 100, 100, 1);
        titleLB.numberOfLines = 2;
        [self.contentView addSubview:titleLB];
        
        //价格
        priceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(240), HeightRate(110), WidthRate(260), HeightRate(40))];
        priceLB.textColor = HDVGRed;
        priceLB.textAlignment = NSTextAlignmentLeft;
        priceLB.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:priceLB];
        
        //类目
        classifyLB1 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(240), HeightRate(155), WidthRate(260), HeightRate(50))];
        classifyLB1.font = [UIFont systemFontOfSize:12.0f];
        classifyLB1.textColor = GoodsClassifyColor;
        [self.contentView addSubview:classifyLB1];
        
//        classifyLB2 = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(240), HeightRate(144), titleLB.bounds.size.width, HeightRate(25))];
//        classifyLB2.font = [UIFont systemFontOfSize: HeightRate(24)];
//        classifyLB2.textColor = [UIColor grayColor];
//        [self.contentView addSubview:classifyLB2];
        
        //距离
//        distanceLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(280), HeightRate(180), WidthRate(160), HeightRate(30))];
//        distanceLB.font = [UIFont systemFontOfSize:12.0f];
//        distanceLB.textAlignment = NSTextAlignmentLeft;
//        distanceLB.textColor = GoodsClassifyColor;
//        [self.contentView addSubview:distanceLB];
        
        //距离图标
//        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(240), HeightRate(174), HeightRate(36), HeightRate(36))];
//        icon.image = [UIImage imageNamed:@"location"];
//        [self.contentView addSubview:icon];
        
        //数量
        numLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(560), HeightRate(155), WidthRate(164), HeightRate(50))];
        numLB.textColor = GoodsClassifyColor;
        numLB.textAlignment = NSTextAlignmentLeft;
        numLB.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:numLB];
        
        self.selectedBackgroundView = [[UIView alloc] init];
    }
}

- (void)setContentTitle:(NSString *)title classify1:(NSString *)classify1 classify2:(NSString *)classify2 Classify3:(NSString *)classify3 Classify4:(NSString *)classify4 Classify5:(NSString *)classify5 distanceOrLocation:(NSString *)distance number:(NSString *)num price:(double)price
{
    titleLB.text = title;
    classifyLB1.numberOfLines = 0;
    if((isNull(classify2) || [classify2 isEqualToString:@""]) && (!isNull(classify1) && ![classify1 isEqualToString:@""]))
    {
        classifyLB1.text = [NSString stringWithFormat:@"%@",classify1];
    }
    else if((!isNull(classify2) && ![classify2 isEqualToString:@""])&& (isNull(classify3) || [classify3 isEqualToString:@""]))
    {
        classifyLB1.text = [NSString stringWithFormat:@"%@,%@",classify1,classify2];
    }
    else if((!isNull(classify3) && ![classify3 isEqualToString:@""]) && (isNull(classify4) || [classify4 isEqualToString:@""]))
    {
        classifyLB1.text = [NSString stringWithFormat:@"%@,%@,%@",classify1,classify2,classify3];
    }
    else if((!isNull(classify4) && ![classify4 isEqualToString:@""]) && (isNull(classify5) || [classify5 isEqualToString:@""]))
    {
        classifyLB1.text = [NSString stringWithFormat:@"%@,%@,%@,%@",classify1,classify2,classify3,classify4];
    }
    else if(!isNull(classify5) && ![classify5 isEqualToString:@""])
    {
        classifyLB1.text = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",classify1,classify2,classify3,classify4,classify5];
    }
    else
    {
        classifyLB1.text = @"";
    }
    numLB.text = [NSString stringWithFormat:@"数量:%@",num];
//    distanceLB.text = distance;//暂时不显示距离
    priceLB.text = [NSString stringWithFormat:@"￥%.2lf",price];
}

@end
