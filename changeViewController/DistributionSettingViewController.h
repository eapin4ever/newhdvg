//
//  DistributionSettingViewController.h
//  changeViewController
//
//  Created by pmit on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DistributionShopViewController.h"

@interface DistributionSettingViewController : UIViewController

@property (strong, nonatomic) NSDictionary *shopDataDic;
@property (weak, nonatomic) DistributionShopViewController *shopVC;

@end
