//
//  AreaDelegate.m
//  changeViewController
//
//  Created by pmit on 15/9/15.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "AreaDelegate.h"

@implementation AreaDelegate

static NSString *const areaCell = @"areaCell";

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.areaArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:areaCell];
    cell.textLabel.text = [self.areaArr[indexPath.row] objectForKey:@"name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *areaId = [NSString stringWithFormat:@"%@",[self.areaArr[indexPath.row] objectForKey:@"id"]];
    NSString *areaName = [self.areaArr[indexPath.row] objectForKey:@"name"];
    if ([self.areaDDelegate respondsToSelector:@selector(goNextData:AreaName:CityName:CityId:)])
    {
        [self.areaDDelegate goNextData:areaId AreaName:areaName CityName:self.cityName CityId:self.cityId];
    }
}

@end
