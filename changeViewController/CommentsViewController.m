//
//  CommentsViewController.m
//  changeViewController
//
//  Created by P&M on 14/11/27.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "CommentsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface CommentsViewController ()

@property (strong,nonatomic) NSString *remarks;
@property (strong,nonatomic) NSString *levels;
@property (assign,nonatomic) NSInteger lastSection;
@property (strong,nonatomic) NSMutableArray *startArr;

@end

@implementation CommentsViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"商品评价";
        
        self.view.backgroundColor = HDVGPageBGGray;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.startArr = [NSMutableArray array];
    for (NSInteger i = 0; i < self.productsArray.count; i++)
    {
        [self.startArr addObject:@"5"];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    // 注册通知，监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardDidShowNotification
                                              object:nil];
    // 注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardDidHideNotification
                                              object:nil];
    [super viewWillAppear:YES];
    
    // 初始化评论商品 tableView
    self.commentsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    [self.commentsTableView setShowsVerticalScrollIndicator:NO];
    self.commentsTableView.delegate = self;
    self.commentsTableView.dataSource = self;
    [self.commentsTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:@"evaluationCell"];
    [self.view addSubview:self.commentsTableView];
    
    
    // 初始化footerView
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 70)];
    footerView.backgroundColor = [UIColor clearColor];
    
    // 初始化提交评论按钮----- 如何检查是否已经评价？？？？
    UIButton *commentsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    commentsButton.frame = CGRectMake(WidthRate(40), 25, WIDTH - WidthRate(40) * 2, 40);
    [commentsButton setTitle:@"提交评价" forState:UIControlStateNormal];
    [commentsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [commentsButton addTarget:self action:@selector(commentsForButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [commentsButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [commentsButton setBackgroundColor:ButtonBgColor];
    [commentsButton.layer setCornerRadius:6.0f];
    [footerView addSubview:commentsButton];
    
    self.commentsTableView.tableFooterView = footerView;
}

#pragma mark - 解决textView输入时键盘遮挡问题
// 监听事件
- (void)handleKeyboardDidShow:(NSNotification *)paramNotification
{
    // 获取键盘高度
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    self.commentsTableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardRect.size.height, 0);
}

- (void)handleKeyboardDidHidden
{
    self.commentsTableView.contentInset = UIEdgeInsetsZero;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 隐藏键盘，实现UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    // 判断输入是否已经超出最大可输入长度
    if ([text isEqualToString:@""] && range.length > 0) {
        
        //删除字符肯定是安全的
        return YES;
    }
    else {
        if (self.commentsTextView.text.length - range.length + text.length > 100) {
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"超出最大可输入长度" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alert show];
            return NO;
        }
        else {
            return YES;
        }
    }
    
    return YES;
}

// 改变输入字数多少，显示几分之几
- (void)textViewDidChange:(UITextView *)textView
{
    self.tipLabel.text = [NSString stringWithFormat:@"%lu/100", (unsigned long)self.commentsTextView.text.length];
}
    

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.commentsTextView resignFirstResponder];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.productsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(70);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(70))];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, HeightRate(69), WIDTH, HeightRate(1));
    layer.backgroundColor = RGBA(220, 220, 220, 1).CGColor;
    [headerView.layer addSublayer:layer];
    
    //NSDictionary *dict = self.evaluationArray[section];
    
    //订单号
    UILabel *orderNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(10), WidthRate(420), HeightRate(60))];
    orderNumLabel.backgroundColor = [UIColor clearColor];
    orderNumLabel.text = [self.productsArray[section] objectNullForKey:@"orderCode"];
    orderNumLabel.textColor = RGBA(117, 116, 121, 1);
    orderNumLabel.font = [UIFont systemFontOfSize:HeightRate(24)];
    [headerView addSubview:orderNumLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 150;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 150)];
    footerView.backgroundColor = [UIColor whiteColor];
    
    // 商品评论
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(30), HeightRate(190), HeightRate(48))];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"商品评价";
    titleLabel.textColor = RGBA(153, 153, 153, 1);
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [footerView addSubview:titleLabel];
    
    /**
     * 评论星级
     */
    NSArray *array = [NSArray arrayWithObjects:@"",@"",@"",@"",@"", nil];
    self.buttonArray = [NSMutableArray array];
    for(int i = 0; i < array.count; i++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(WidthRate(200) + WidthRate(57) * i, HeightRate(25), WidthRate(55), WidthRate(55));
        [button setImage:[UIImage imageNamed:@"star_pressed@2x.png"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"star@2x.png"] forState:UIControlStateSelected];
        // 设置tag，主要是为了添加点击事件时可以正确的知道是那个按钮触发的
        button.tag =  section * 100 + (i + 1);
        [button addTarget:self action:@selector(relevantButtonTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:button];
        
        [self.buttonArray addObject:button];   // 将按钮添加到数组里
    }
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, titleLabel.frame.origin.y + HeightRate(68), WIDTH, HeightRate(1));
    layer.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [footerView.layer addSublayer:layer];
    
    // 初始化评论 textView
//    self.commentsTextView = [[UITextView alloc] initWithFrame:CGRectMake(WidthRate(30), titleLabel.frame.origin.y + HeightRate(70), WIDTH - WidthRate(60), HeightRate(200))];
//    self.commentsTextView.delegate = self;
//    self.commentsTextView.textColor = RGBA(79, 79, 79, 1);
//    self.commentsTextView.font = [UIFont systemFontOfSize:14.0f];
//    self.commentsTextView.layer.cornerRadius = 0.0f;   // 设置圆角值
//    self.commentsTextView.layer.masksToBounds = YES;
//    self.commentsTextView.layer.borderWidth = 0.5f;    // 设置边框大小
//    [self.commentsTextView.layer setBorderColor:[[UIColor clearColor] CGColor]];
//    self.commentsTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    self.commentsTextView.returnKeyType = UIReturnKeyDone;
//    self.commentsTextView.keyboardType = UIKeyboardTypeDefault;
//    self.commentsTextView.scrollEnabled = YES;
    UITextView *commentTextView = [[UITextView alloc] initWithFrame:CGRectMake(WidthRate(30), titleLabel.frame.origin.y + HeightRate(70), WIDTH - WidthRate(60), HeightRate(200))];
    commentTextView.delegate = self;
    commentTextView.textColor = RGBA(79, 79, 79, 1);
    commentTextView.font = [UIFont systemFontOfSize:14.0f];
    commentTextView.layer.cornerRadius = 0.0f;   // 设置圆角值
    commentTextView.layer.masksToBounds = YES;
    commentTextView.layer.borderWidth = 0.5f;    // 设置边框大小
    [commentTextView.layer setBorderColor:[[UIColor clearColor] CGColor]];
    commentTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    commentTextView.returnKeyType = UIReturnKeyDone;
    commentTextView.keyboardType = UIKeyboardTypeDefault;
    commentTextView.scrollEnabled = YES;
    commentTextView.tag = section;
    [footerView addSubview:commentTextView];
    
    // 分隔线
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(0, self.commentsTextView.frame.origin.y + HeightRate(200), WIDTH, HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [footerView.layer addSublayer:layer2];
    
    // 提示输入字数
    self.tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(180), self.commentsTextView.frame.origin.y + WidthRate(200), WidthRate(140), HeightRate(48))];
    self.tipLabel.backgroundColor = [UIColor clearColor];
    self.tipLabel.text = [NSString stringWithFormat:@"%lu/100", (unsigned long)self.commentsTextView.text.length];
    self.tipLabel.textColor = [UIColor grayColor];
    self.tipLabel.textAlignment = NSTextAlignmentRight;
    self.tipLabel.font = [UIFont systemFontOfSize:14.0f];
    [footerView addSubview:self.tipLabel];
    
    return footerView;
}

// 每个cell只有一行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 100;
        }
    }
    return 100;
}

#pragma mark - UITableViewCell delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTradeTableViewCell *evaluationCell = [tableView dequeueReusableCellWithIdentifier:@"evaluationCell"];
    
    [evaluationCell setMyTradeOrderDataUI];

    NSDictionary *dict = self.productsArray[indexPath.section];
    
    evaluationCell.turnBackBtn.hidden = YES;
    
    // 获取下来的数据
    //给一张默认图片，先使用默认图片，当图片加载完成后再替换
    [evaluationCell.iv sd_setImageWithURL:[dict objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    [evaluationCell setTitle:[dict objectNullForKey:@"productName"] setClssify:[dict objectNullForKey:@"aspecVal"] setNumber:[[dict objectNullForKey:@"productNum"] doubleValue]];
    
    evaluationCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return evaluationCell;
}

#pragma mark 星级评论响应方法
- (void)relevantButtonTypeClick:(UIButton *)button
{
    NSInteger section = button.tag / 100;
    NSInteger startLevel = button.tag % 100;

    self.levelsStr = [NSString stringWithFormat:@"%ld", (long)button.tag];
    
    // 改变按钮的选中状态
    button.selected = !button.selected;
}

#pragma mark - 提交评论商品按钮点击事件
- (void)commentsForButtonClick:(id)sender
{
    [self.commentsTextView resignFirstResponder];
    // 获取用户token
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    
    /**
     *  评价商品
     *  参数：
     *  PM_SID        Y    String      Token
     *  remarks       N    String      评论内容   （多个商品评论使用"|$|"隔开）
     *  levels        N    String      星级      （多个使用英文逗号隔开）
     *  orderIds      N    String      订单id
     *  productIds    N    String      商品id    （多个使用英文逗号隔开）
     */
    NSMutableArray *productIdsArr = [NSMutableArray array];
    for (NSDictionary *dic in self.productsArray)
    {
        [productIdsArr addObject:[dic objectNullForKey:@"productId"]];
    }
    
    NSString *remarks = [NSString stringWithFormat:@"%@", self.commentsTextView.text];
    NSString *levels = [NSString stringWithFormat:@"%@", self.levelsStr];
    NSString *orderIds = [self.productsArray[0] objectNullForKey:@"orderId"];
    NSString *productIds = [productIdsArr componentsJoinedByString:@","];
    
    NSDictionary *comments = @{@"PM_SID":userInfos.PM_SID, @"remarks":remarks,@"levels":levels,@"orderIds":orderIds,@"productIds":productIds};
    self.networking = [PMNetworking defaultNetworking];
    
    [self.networking request:PMRequestStateAddOrderDiscuss WithParameters:comments callBackBlock:^(NSDictionary *dict) {
        
        if ([[dict objectNullForKey:@"success"] integerValue] == 1) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提交商品评价成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            alertView.tag = 1;
            [alertView show];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectNullForKey:@"message"] message: [dict objectNullForKey:@"messageCode"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }showIndicator:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
