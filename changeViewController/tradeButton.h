//
//  tradeButton.h
//  changeViewController
//
//  Created by ZhangEapin on 15/4/23.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tradeButton : UIButton

@property (copy,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UIImageView *tradeIV;

- (void)setImage:(UIImage *)image AndTitle:(NSString *)title;

@end
