//
//  PMMyProfitViewController.m
//  changeViewController
//
//  Created by P&M on 14/12/5.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import "PMMyProfitViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMMyProfitTableViewController.h"
#import "WithdrawalMoneyViewController.h"
#import "SWCountingLabel.h"

@interface PMMyProfitViewController ()

@property (strong, nonatomic) SWCountingLabel *noEarningsLab;
@property (strong, nonatomic) SWCountingLabel *cumulativeMoneyLab;
@property (strong, nonatomic) SWCountingLabel *withdrawalMoneyLab;

@end

@implementation PMMyProfitViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"代言获利";
        
        self.view.backgroundColor = [UIColor whiteColor]; // 设置背景色
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self todayEarningsUI];
    [self cumulativeEarningsAndWithdrawalUI];
    [self profitAndWithdrawalButtonUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initProfitDetailDataList];
}

// 今日收益 view
- (void)todayEarningsUI
{
    // 创建一个 view
    self.todayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(670))];
    self.todayView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.todayView];
    
    // 图片
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(75), WidthRate(30), WidthRate(600), WidthRate(600))];
    [image setImage:[UIImage imageNamed:@"profit_bgIma"]];
    [self.todayView addSubview:image];
    
    // 暂无收益
    self.noEarningsLab = [[SWCountingLabel alloc] initWithFrame:CGRectMake(WidthRate(30), WidthRate(230), WidthRate(540), WidthRate(160))];
    self.noEarningsLab.backgroundColor = [UIColor clearColor];
    self.noEarningsLab.textColor = [UIColor whiteColor];
    self.noEarningsLab.textAlignment = NSTextAlignmentCenter;
    self.noEarningsLab.font = [UIFont systemFontOfSize:52.0f];
    self.noEarningsLab.shadowOffset = CGSizeMake(1.0, 1.0);
    self.noEarningsLab.shadowColor = [UIColor grayColor];
    [image addSubview:self.noEarningsLab];
    
    // 今日收益
    UILabel *todayEarningsLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(160), WidthRate(580), WidthRate(280), WidthRate(60))];
    todayEarningsLab.backgroundColor = [UIColor clearColor];
    todayEarningsLab.text = @"今日收益";
    todayEarningsLab.textColor = [UIColor blackColor];
    todayEarningsLab.textAlignment = NSTextAlignmentCenter;
    todayEarningsLab.font = [UIFont systemFontOfSize:20.0f];
    [image addSubview:todayEarningsLab];
}

// 累计收益和可提现金额 view
- (void)cumulativeEarningsAndWithdrawalUI
{
    // 创建一个 view
    self.moneyView = [[UIView alloc] initWithFrame:CGRectMake(0, self.todayView.frame.origin.y + self.todayView.frame.size.height, WIDTH, 100)];
    self.moneyView.backgroundColor = [UIColor whiteColor]; // 设置背景色
    [self.view addSubview:self.moneyView];
    
    // 分隔线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(WIDTH / 2, 15, 0.5, 70);
    line1.backgroundColor = RGBA(252, 128, 55, 1).CGColor;
    [self.moneyView.layer addSublayer:line1];
    
    // 累计收益金额
    self.cumulativeMoneyLab = [[SWCountingLabel alloc] initWithFrame:CGRectMake(WidthRate(30), 10, WidthRate(315), WidthRate(80))];
    self.cumulativeMoneyLab.backgroundColor = [UIColor clearColor];
    self.cumulativeMoneyLab.text = @"";
    self.cumulativeMoneyLab.textColor = RGBA(252, 128, 55, 1);
    self.cumulativeMoneyLab.textAlignment = NSTextAlignmentCenter;
    self.cumulativeMoneyLab.font = [UIFont systemFontOfSize:26.0f];
    [self.moneyView addSubview:self.cumulativeMoneyLab];
    
    // 累计收益
    UILabel *cumulativeEarningsLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), self.cumulativeMoneyLab.frame.origin.y + WidthRate(90), WidthRate(315), WidthRate(80))];
    cumulativeEarningsLab.backgroundColor = [UIColor clearColor];
    cumulativeEarningsLab.text = @"累计收益";
    cumulativeEarningsLab.textColor = RGBA(73, 73, 73, 1);
    cumulativeEarningsLab.textAlignment = NSTextAlignmentCenter;
    cumulativeEarningsLab.font = [UIFont systemFontOfSize:18.0f];
    [self.moneyView addSubview:cumulativeEarningsLab];
    
    // 提现金额
    self.withdrawalMoneyLab = [[SWCountingLabel alloc] initWithFrame:CGRectMake(WIDTH / 2 + WidthRate(30), 10, WidthRate(315), WidthRate(80))];
    self.withdrawalMoneyLab.backgroundColor = [UIColor clearColor];
    self.withdrawalMoneyLab.text = @"";
    self.withdrawalMoneyLab.textColor = RGBA(252, 128, 55, 1);
    self.withdrawalMoneyLab.textAlignment = NSTextAlignmentCenter;
    self.withdrawalMoneyLab.font = [UIFont systemFontOfSize:26.0f];
    [self.moneyView addSubview:self.withdrawalMoneyLab];
    
    // 可提现金额
    UILabel *withdrawalLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 2 + WidthRate(30), self.withdrawalMoneyLab.frame.origin.y + WidthRate(90), WidthRate(315), WidthRate(80))];
    withdrawalLab.backgroundColor = [UIColor clearColor];
    withdrawalLab.text = @"可提现金额";
    withdrawalLab.textColor = RGBA(73, 73, 73, 1);
    withdrawalLab.textAlignment = NSTextAlignmentCenter;
    withdrawalLab.font = [UIFont systemFontOfSize:18.0f];
    [self.moneyView addSubview:withdrawalLab];
}

// 获利、提现按钮 view
- (void)profitAndWithdrawalButtonUI
{
    // 创建一个 view
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - HeightRate(100) - 64, WIDTH, HeightRate(100))];
    self.bottomView.backgroundColor = RGBA(252, 128, 55, 1);
    [self.view addSubview:self.bottomView];
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 2, 0, WidthRate(1), HeightRate(100))];
    line.backgroundColor = [UIColor whiteColor];
    [self.bottomView addSubview:line];
    
    // 初始化获利按钮
    UIButton *profitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    profitButton.frame = CGRectMake(((WIDTH / 2) - WidthRate(220)) / 2, HeightRate(15), WidthRate(220), HeightRate(70));
    [profitButton setTitle:@"获利详情" forState:UIControlStateNormal];
    [profitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [profitButton.titleLabel setFont:[UIFont systemFontOfSize:18.0f]];
    [profitButton addTarget:self action:@selector(profitButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:profitButton];
    
    // 初始化提现按钮
    UIButton *withdrawalButton = [UIButton buttonWithType:UIButtonTypeCustom];
    withdrawalButton.frame = CGRectMake((WIDTH / 2) + ((WIDTH / 2) - WidthRate(220)) / 2, HeightRate(15), WidthRate(220), HeightRate(70));
    [withdrawalButton setTitle:@"提现申请" forState:UIControlStateNormal];
    [withdrawalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [withdrawalButton.titleLabel setFont:[UIFont systemFontOfSize:18.0f]];
    [withdrawalButton addTarget:self action:@selector(withdrawalButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:withdrawalButton];
}

- (void)profitButtonClick:(id)sender
{
    PMMyProfitTableViewController *profitDetailVC = [[PMMyProfitTableViewController alloc] init];
    [self.navigationController pushViewController:profitDetailVC animated:YES];
}

- (void)withdrawalButtonClick:(id)sender
{
    WithdrawalMoneyViewController *withdrawalMoneyVC = [[WithdrawalMoneyViewController alloc] init];
    withdrawalMoneyVC.canMoneyStr = self.withdrawalMoneyLab.text;
    [self.navigationController pushViewController:withdrawalMoneyVC animated:YES];
}

#pragma mark - 获取代言获利数据
- (void)initProfitDetailDataList
{
    PMUserInfos *userInfots = [PMUserInfos shareUserInfo];
    
    //代言获利
    /*
     参数：
     PM_SID        Y    String      Token
     */
    NSDictionary *profitData = @{@"PM_SID":userInfots.PM_SID};
    self.networking = [PMNetworking defaultNetworking];
    
    [self.networking request:PMRequestStateShareShowMySell WithParameters:profitData callBackBlock:^(NSDictionary *dict) {
        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            if ([[dict objectNullForKey:@"success"] integerValue] == 0) {
                
                // 还没有提现明细数据
                self.noEarningsLab.text = @"暂无收益";
                self.cumulativeMoneyLab.text = @"0.00";
                self.withdrawalMoneyLab.text = @"0.00";
            }
            else {
                
                // 获取代言获利页面今日收益、累计收益、可提现金额数据
                double noEarningsStr = [[[[dict objectNullForKey:@"data"] objectNullForKey:@"mySell"] objectNullForKey:@"todayInput"] doubleValue];
                double cumulativeMoneyStr = [[[[dict objectNullForKey:@"data"] objectNullForKey:@"mySell"] objectNullForKey:@"totalMoney"] doubleValue];
                double withdrawalMoneyStr = [[[[dict objectNullForKey:@"data"] objectNullForKey:@"mySell"] objectNullForKey:@"getMoney"] doubleValue];
                
                if (noEarningsStr == 0) {
                    self.noEarningsLab.text = @"0.00";
                }
                else {
                    [self withNumberLabel:self.noEarningsLab changeNumber:noEarningsStr];
                }
                
                //累计收益
                if (cumulativeMoneyStr == 0) {
                    self.cumulativeMoneyLab.text = @"0.00";
                }
                else {
                    [self withNumberLabel:self.cumulativeMoneyLab changeNumber:cumulativeMoneyStr];
                }
                
                //可提现金额
                if (withdrawalMoneyStr == 0) {
                    self.withdrawalMoneyLab.text = @"0.00";
                }
                else {
                    [self withNumberLabel:self.withdrawalMoneyLab changeNumber:withdrawalMoneyStr];
                }
            }
        }];
    }showIndicator:YES];
}

- (void)withNumberLabel:(SWCountingLabel *)label changeNumber:(double)number
{
    label.method = UILabelCountingMethodLinear;
    label.format = @"%.2f";
    [label countFrom:0 to:number withDuration:1.0];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
