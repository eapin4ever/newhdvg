//
//  YBTurnBackViewController.m
//  changeViewController
//
//  Created by ZhangEapin on 15/5/5.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBTurnBackViewController.h"
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "JHRefresh.h"
#import "MyTradeTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TurnBackSendViewController.h"
#import "PMToastHint.h"
#import "RelevantViewController.h"
#import "PMMyGoodsViewController.h"

@interface YBTurnBackViewController () <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) UITableView *turnBackTableView;
@property (strong,nonatomic) NSMutableArray *showArray;
@property (copy,nonatomic) myFinishBlock finishBlock;
@property (copy,nonatomic) myFinishBlock errorBlock;
@property (assign,nonatomic) NSInteger currentPage;
@property (assign,nonatomic) BOOL isLoad;
@property (strong,nonatomic) UIView *noResultView;
@property (assign,nonatomic) NSInteger totalPages;
@property (strong,nonatomic) UILabel *orderLabel;
@property (strong,nonatomic) NSArray *statausArray;
@property (strong,nonatomic) UILabel *nameLB;
@property (strong,nonatomic) UILabel *phoneLB;
@property (strong,nonatomic) UILabel *addressLB;
@property (strong,nonatomic) UIView *backView;

@end

@implementation YBTurnBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"退货进度";
    [self createUI];
    [self createNoResult];
    self.isLoad = NO;
    self.currentPage = 1;
    self.statausArray = @[@[@"待审核",@"取消申请"],@[@"审核通过",@"发货",@"商家信息"],@[@"已退款"],@[@"申请失败",@"重新申请"],@[@"等待商家收货"],@[@"供应商寄货中"],@[@"买家取消申请"],@[@"待退款"]];
    [self createInfoAlert];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getModels];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.backView.hidden = YES;
}

- (void)createUI
{
    UITableView *turnBackTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.turnBackTableView = turnBackTableView;
    [turnBackTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:@"backCell"];
    turnBackTableView.delegate = self;
    turnBackTableView.dataSource = self;
    turnBackTableView.backgroundColor = HDVGPageBGGray;
    turnBackTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:turnBackTableView];
}

- (void)getModels
{
    if(self.isLoad) {
        self.currentPage++;
    }
    else {
        self.currentPage = 1;
    }
    
    NSDictionary *param = @{PMSID,@"tabNum":@(26),@"currentPage":@(self.currentPage)};
    [[PMNetworking defaultNetworking] request:PMRequestStateShowReturn WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if(intSuccess ==1)
        {
            self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
            //得到所有的数据
            self.showArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];

            if(self.finishBlock)
                self.finishBlock();
            self.noResultView.hidden = YES;
            
        }
        else
        {
            if(self.errorBlock)
                self.errorBlock();
            [self.showArray removeAllObjects];
            self.noResultView.hidden = NO;
        }
        [self.turnBackTableView reloadData];
    }showIndicator:YES];
}

- (void)createNoResult
{
    UIView *noResultView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT-64)];
    noResultView.backgroundColor = [UIColor whiteColor];
    self.noResultView = noResultView;
    
    UIImageView *noResultIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noResultView.frame.size.height)];
    noResultIV.image = [UIImage imageNamed:@"order_null2"];
    noResultIV.contentMode = UIViewContentModeScaleAspectFit;
    [noResultView addSubview:noResultIV];
    
    [self.view insertSubview:noResultView atIndex:100];
    
    noResultView.hidden = YES;
}

#pragma mark - 添加刷新机制
- (void)addRefreshAction
{
    __weak YBTurnBackViewController *weakSelf = self;
    
    //使用普通的上拉加载
    [self.turnBackTableView addRefreshFooterViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        
        //得到当前页
        if(weakSelf.currentPage < weakSelf.totalPages)//如果小于总页数则刷新
        {
            weakSelf.isLoad = YES;
            //获取数据
            [weakSelf getModels];
            
            weakSelf.finishBlock = ^{
                
                [weakSelf.turnBackTableView reloadData];
                //加载刷新
                [weakSelf.turnBackTableView footerEndRefreshing];
                
            };
            weakSelf.errorBlock = ^{
                //不用刷新
                [weakSelf.turnBackTableView footerEndRefreshing];
            };
        }
        else {
            //不用刷新
            [weakSelf.turnBackTableView footerEndRefreshing];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.showArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"backCell"];
    [cell setMyTradeOrderDataUI];
    
    NSDictionary *dict = self.showArray[indexPath.section];
    
    //给一张默认图片，先使用默认图片，当图片加载完成后再替换
    [cell.iv sd_setImageWithURL:[dict objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    NSString *aSpecVal = [dict objectNullForKey:@"aSpecVal"];
    NSString *bSpecVal = [dict objectNullForKey:@"bSpecVal"];
    NSString *cSpecVal = [dict objectNullForKey:@"cSpecVal"];
    NSString *dSpecVal = [dict objectNullForKey:@"dSpecVal"];
    NSString *eSpecVal = [dict objectNullForKey:@"eSpecVal"];
    
    NSMutableString *specificationStr = [NSMutableString string];
    
    if (aSpecVal.length != 0) {
        specificationStr = [NSMutableString stringWithFormat:@"%@", aSpecVal];
    }
    else if (bSpecVal.length != 0) {
        specificationStr = [NSMutableString stringWithFormat:@"%@", bSpecVal];
    }
    else if (aSpecVal.length != 0) {
        specificationStr = [NSMutableString stringWithFormat:@"%@", cSpecVal];
    }
    else if (aSpecVal.length != 0) {
        specificationStr = [NSMutableString stringWithFormat:@"%@", dSpecVal];
    }
    else if (aSpecVal.length != 0) {
        specificationStr = [NSMutableString stringWithFormat:@"%@", eSpecVal];
    }
    
    // 获取下来的数据
    [cell setTitle:[dict objectNullForKey:@"productName"] setSpecification:specificationStr setNumber:[[dict objectNullForKey:@"productNum"] integerValue] setPrice:[[dict objectNullForKey:@"productPrice"] doubleValue] * [[dict objectNullForKey:@"productNum"] integerValue]];
    
    cell.turnBackBtn.hidden = YES;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 35;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (iPhone6_Plus || iPhone6 ? 110 : 90);
}

#pragma mark - section For Header
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *dict = self.showArray[section];
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 35)];
    headView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [[CALayer alloc] init];
    line.frame = CGRectMake(0, 0, WIDTH, HeightRate(10));
    line.backgroundColor = [RGBA(231, 231, 231, 1) CGColor];
    [headView.layer addSublayer:line];
    
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(0, 0, WIDTH, 0.5);
    line1.backgroundColor = [UIColor lightGrayColor].CGColor;
    [headView.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(0, 35-0.5, WIDTH, 0.5);
    line2.backgroundColor = [UIColor lightGrayColor].CGColor;
    [headView.layer addSublayer:line2];
    
    //订单号
    self.orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 5, WidthRate(420), 30)];
    self.orderLabel.backgroundColor = [UIColor clearColor];
    self.orderLabel.text = [NSString stringWithFormat:@"订单号：%@", [dict objectNullForKey:@"orderCode"]];
    self.orderLabel.textColor = RGBA(117, 116, 121, 1);
    self.orderLabel.font = [UIFont systemFontOfSize:HeightRate(24)];
    [headView addSubview:self.orderLabel];
    
    //时间
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/2 + WidthRate(30), 5, WIDTH/2 - WidthRate(60), 30)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.text = [dict objectNullForKey:@"createDt"];
    timeLabel.textColor = RGBA(117, 116, 121, 1);
    timeLabel.font = [UIFont systemFontOfSize:HeightRate(24)];
    timeLabel.textAlignment = NSTextAlignmentRight;
    [headView addSubview:timeLabel];
    
    return headView;
}

#pragma mark - section For Footer
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // 初始化footerView
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 35)];
    footerView.backgroundColor = [UIColor whiteColor];
    footerView.tag = section*1000;
    
    UILabel *lineIma = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 0.5)];
    lineIma.backgroundColor = RGBA(200, 200, 200, 1);
    [footerView addSubview:lineIma];
    
    UILabel *lineIma2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 34.5, WIDTH, 0.5)];
    lineIma2.backgroundColor = RGBA(200, 200, 200, 1);
    [footerView addSubview:lineIma2];
    
    //根据不同的状态创建不同的按钮
    [self createButtonWithStateWithFooterView:footerView];
    
    return footerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.showArray[indexPath.section];
    NSString *productId = [dict objectNullForKey:@"productId"];
    PMMyGoodsViewController *goodVC = [[PMMyGoodsViewController alloc] init];
    goodVC.productId = productId;
    [self.navigationController pushViewControllerWithNavigationControllerTransition:goodVC];
}


#pragma mark 根据订单状态显示不同的按钮
- (void)createButtonWithStateWithFooterView:(UIView *)footerView
{
    NSInteger section = footerView.tag/1000;
    NSInteger statusCount = [[self.showArray[section] objectNullForKey:@"status"] integerValue];
//    NSArray *wordArr = [NSArray array];
    NSArray *wordArr = self.statausArray[statusCount + 1];
    for (NSInteger i = 0; i < wordArr.count; i++)
    {
        CGFloat origin_X = WIDTH - WidthRate(220) - WidthRate(220) * i;
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(origin_X, 3, WidthRate(220), 29);
        [button setTitle:wordArr[i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        if (i == 0)
        {
            button.enabled = NO;
        }
        button.tag = footerView.tag + i;
        [button addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:button];
    }
}

- (void)clickBtn:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    NSInteger section = sender.tag / 1000;
    NSInteger buttonType = sender.tag % 1000;
    NSInteger statusCount = [[self.showArray[section] objectNullForKey:@"status"] integerValue];
    if (statusCount == -1)
    {
        self.view.userInteractionEnabled = YES;
        if (buttonType == 1)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"是否确定取消退货审核?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alertView.tag = section;
            [alertView show];
        }
    }
    else if (statusCount == 0)
    {
        if (buttonType == 1)
        {
            self.view.userInteractionEnabled = YES;
            NSString *orderId = [self.showArray[section] objectNullForKey:@"orderId"];
            NSString *productId = [self.showArray[section] objectNullForKey:@"productId"];
            [[PMNetworking defaultNetworking] request:PMRequestStateShowReturnDetail WithParameters:@{@"orderId":orderId,@"productId":productId,@"flag":@(1)} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    NSString *backOrderId = [[dic objectNullForKey:@"data"] objectNullForKey:@"id"];
                    TurnBackSendViewController *sendVC = [[TurnBackSendViewController alloc] init];
                    sendVC.backOrderId = backOrderId;
                    [self.navigationController pushViewController:sendVC animated:YES];
                }
                else
                {
                    showRequestFailAlertView;
                }
            } showIndicator:YES];
        }
        else if (buttonType == 2)
        {
            NSString *orderId = [self.showArray[section] objectNullForKey:@"orderId"];
            NSString *productId = [self.showArray[section] objectNullForKey:@"productId"];
            [[PMNetworking defaultNetworking] request:PMRequestStateShowReturnDetail WithParameters:@{@"orderId":orderId,@"productId":productId,@"flag":@(1)} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    NSString *backOrderId = [[dic objectNullForKey:@"data"] objectNullForKey:@"id"];
                    [[PMNetworking defaultNetworking] request:PMRequestStateShowBackAddress WithParameters:@{@"id":backOrderId} callBackBlock:^(NSDictionary *dic) {
                        if (intSuccess == 1)
                        {
                            NSString *nameInfo = [[dic objectNullForKey:@"data"] objectNullForKey:@"name"];
                            NSString *phoneInfo = [[dic objectNullForKey:@"data"] objectNullForKey:@"mobile"];
                            NSString *addressInfo = [NSString stringWithFormat:@"%@ %@",[[dic objectNullForKey:@"data"] objectNullForKey:@"address"],[[dic objectNullForKey:@"data"] objectNullForKey:@"areaName"]];
                            [self showInfo:nameInfo Phone:phoneInfo Address:addressInfo];
                        }
                        else
                        {
                            self.view.userInteractionEnabled = YES;
                            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"很抱歉，查不到商家信息"];
                        }
                    } showIndicator:YES];
                }
                else
                {
                    self.view.userInteractionEnabled = YES;
                    showRequestFailAlertView;
                }
            } showIndicator:YES];
        }
    }
    else if (statusCount == 2)
    {
        self.view.userInteractionEnabled = YES;
        if (buttonType == 1)
        {
            NSString *orderId = [self.showArray[section] objectNullForKey:@"orderId"];
            
            [[PMNetworking defaultNetworking] request:PMRequestStateCheckReturnBack WithParameters:@{@"orderId":orderId} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    RelevantViewController *relevantVC = [[RelevantViewController alloc] init];
                    relevantVC.relevantDict = self.showArray[section];
                    [self.navigationController pushViewController:relevantVC animated:YES];
                }
                else
                {
                    NSString *message = [dic objectNullForKey:@"message"];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alert show];
                }
            } showIndicator:NO];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSInteger section = alertView.tag;
        NSString *orderId = [self.showArray[section] objectNullForKey:@"orderId"];
        [[PMNetworking defaultNetworking] request:PMRequestStatesCancelApply WithParameters:@{@"orderId":orderId} callBackBlock:^(NSDictionary *dic) {
            if (intSuccess == 1)
            {
                UIAlertView *succAlert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:@"取消成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [succAlert show];
            }
            else
            {
                showRequestFailAlertView;
            }
            
        } showIndicator:YES];
    }
    else if ([alertView.message isEqualToString:@"取消成功"])
    {
        if (buttonIndex == 0)
        {
            [self getModels];
        }
    }
}

- (void)createInfoAlert
{
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    backView.backgroundColor = [UIColor clearColor];
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.4;
    [backView addSubview:alphaView];
    
    self.backView = backView;
    backView.hidden = YES;
    
    UIView *alertView = [[UIView alloc] init];
    alertView.center = CGPointMake(WIDTH / 2, HEIGHT / 2);
    alertView.bounds = (CGRect){CGPointZero,{300,220}};
    alertView.backgroundColor = [UIColor whiteColor];
    [backView addSubview:alertView];
    
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 40)];
    titleLB.text = @"卖家信息";
    titleLB.font = [UIFont systemFontOfSize:16.0f];
    titleLB.textColor = [UIColor blackColor];
    titleLB.textAlignment = NSTextAlignmentCenter;
    [alertView addSubview:titleLB];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = [UIColor lightGrayColor].CGColor;
    line.frame = CGRectMake(0, 40, 300, 1);
    [alertView.layer addSublayer:line];
    
    self.nameLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 45, 280, 40)];
    self.nameLB.font = [UIFont systemFontOfSize:14.0f];
    self.nameLB.textAlignment = NSTextAlignmentLeft;
    self.nameLB.textColor = [UIColor blackColor];
    [alertView addSubview:self.nameLB];
    
    self.phoneLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 85, 280, 40)];
    self.phoneLB.font = [UIFont systemFontOfSize:14.0f];
    self.phoneLB.textAlignment = NSTextAlignmentLeft;
    self.phoneLB.textColor = [UIColor blackColor];
    [alertView addSubview:self.phoneLB];
    
    self.addressLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 125, 280, 40)];
    self.addressLB.font = [UIFont systemFontOfSize:14.0f];
    self.addressLB.textAlignment = NSTextAlignmentLeft;
    self.addressLB.textColor = [UIColor blackColor];
    self.addressLB.numberOfLines = 0;
    [alertView addSubview:self.addressLB];
    
    CALayer *line2 = [CALayer layer];
    line2.backgroundColor = [UIColor lightGrayColor].CGColor;
    line2.frame = CGRectMake(0, 170, 300, 1);
    [alertView.layer addSublayer:line2];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 172, 300, 45);
    [button setTitle:@"我知道了" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(closeInfo:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [alertView addSubview:button];
    
    //UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    [[UIApplication sharedApplication].keyWindow addSubview:backView];
    [self.view addSubview:backView];
    //[window insertSubview:backView aboveSubview:self.turnBackTableView];
}

- (void)showInfo:(NSString *)name Phone:(NSString *)phone Address:(NSString *)address
{
    NSMutableAttributedString *noteStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"卖家姓名:%@",name]];
    [noteStr1 addAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]} range:NSMakeRange(0, 5)];
    self.nameLB.attributedText = noteStr1;
    
    NSMutableAttributedString *noteStr2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"卖家电话:%@",phone]];
    [noteStr2 addAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]} range:NSMakeRange(0, 5)];
    self.phoneLB.attributedText = noteStr2;
    
    NSMutableAttributedString *noteStr3 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"卖家地址:%@",address]];
    [noteStr3 addAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]} range:NSMakeRange(0, 5)];
    self.addressLB.attributedText = noteStr3;
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.backView.hidden = NO;
        self.view.userInteractionEnabled = YES;
    }];
}

- (void)closeInfo:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.backView.hidden = YES;
        self.view.userInteractionEnabled = YES;
    }];
}

@end
