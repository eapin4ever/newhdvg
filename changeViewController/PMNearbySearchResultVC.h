//
//  PMNearbySearchResultVC.h
//  changeViewController
//
//  Created by pmit on 14/12/16.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "JHRefresh.h"
#import "PMNearbySearchViewController.h"

@interface PMNearbySearchResultVC : UIViewController
@property(nonatomic,weak)PMNearbySearchViewController *searchVC;

@property(nonatomic,strong)NSMutableArray *dicArray;
- (void)getModelsWithParam:(NSDictionary *)param finish:(myFinishBlock)finish error:(myFinishBlock)error;
@end
