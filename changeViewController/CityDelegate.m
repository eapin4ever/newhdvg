//
//  CityDelegate.m
//  changeViewController
//
//  Created by pmit on 15/9/15.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "CityDelegate.h"
#import "PMNetworking.h"
#import "PMMyPhoneInfo.h"

@implementation CityDelegate

static NSString *const cityCell = @"cityCell";

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cityArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityCell];
    cell.textLabel.text = [self.cityArr[indexPath.row] objectForKey:@"name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cityId = [self.cityArr[indexPath.row] objectForKey:@"id"];
    NSString *cityName = [self.cityArr[indexPath.row] objectForKey:@"name"];
    
    [[PMNetworking defaultNetworking] request:PMRequestStateGetCityData WithParameters:@{PMSID,@"parentId":cityId} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            NSArray *areaArr = [dic objectNullForKey:@"data"];
            if (areaArr.count > 0)
            {
                if ([self.cityDelegateDelegate respondsToSelector:@selector(successGetNextData:CityId:CityName:)])
                {
                    [self.cityDelegateDelegate successGetNextData:areaArr CityId:cityId CityName:cityName];
                }
                
            }
            else
            {
                if ([self.cityDelegateDelegate respondsToSelector:@selector(noNextData:CityName:)])
                {
                    [self.cityDelegateDelegate noNextData:cityId CityName:cityName];
                }
            }
        }
        else
        {
            
        }
        
    } showIndicator:NO];
}

@end
