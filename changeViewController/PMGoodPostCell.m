//
//  PMGoodPostCell.m
//  changeViewController
//
//  Created by pmit on 15/10/28.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMGoodPostCell.h"
#import "PMMyPhoneInfo.h"

@implementation PMGoodPostCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.postLB)
    {
        self.postLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 30, 20)];
        self.postLB.textColor = [UIColor lightGrayColor];
        self.postLB.font = [UIFont systemFontOfSize:14.0f];
        self.postLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.postLB];
    }
}

- (void)setCellData:(NSString *)globelPostString Pro:(NSString *)globelProString
{
    NSString *postString = @"";
    if ([globelPostString isEqualToString:@""] && [globelProString isEqualToString:@""])
    {
        self.postLB.hidden = YES;
    }
    else
    {
        self.postLB.hidden = NO;
        if ([globelProString isEqualToString:@""])
        {
            postString = [NSString stringWithFormat:@"%@",globelPostString];
        }
        else
        {
            postString = [NSString stringWithFormat:@"%@[%@]",globelPostString,globelProString];
        }
    }
    
    CGSize postStringSize = [postString boundingRectWithSize:CGSizeMake(WIDTH - 30, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
    self.postLB.frame = CGRectMake(self.postLB.frame.origin.x, self.postLB.frame.origin.y, WIDTH - 30, postStringSize.height);
    self.postLB.text = postString;
}

@end
