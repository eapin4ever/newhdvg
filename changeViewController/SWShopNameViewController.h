//
//  SWShopNameViewController.h
//  changeViewController
//
//  Created by P&M on 15/6/29.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMNetworking.h"
#import "DistributionSettingViewController.h"

@protocol SWShopNameViewControllerDelegate <NSObject>

- (void)passShopName:(NSString *)shopName;

@end

@interface SWShopNameViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) UIView *nameView;
@property (strong, nonatomic) UITextField *nameTF;

@property (strong, nonatomic) NSDictionary *shopDataDic;
@property (weak, nonatomic) DistributionSettingViewController *shopSettingVC;
@property (weak,nonatomic) id<SWShopNameViewControllerDelegate> nameDelegate;

@end
