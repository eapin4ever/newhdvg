//
//  PMAppraiseVC.h
//  changeViewController
//
//  Created by pmit on 14/12/15.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"
#import "PMAppraiseTVCell.h"
#import "PMMyGoodsViewController.h"

typedef void (^finishAction)(void);

@interface PMAppraiseVC : UIViewController
//@property(nonatomic,weak)PMGoodsDetailVC *goodsVC;
//@property (weak,nonatomic) YBGoodDetailViewController *goodsVC;
@property (weak,nonatomic) PMMyGoodsViewController *goodsVC;
- (void)getModelsWithProductId:(NSString *)productID finishAction:(finishAction)finishAction;
@end
