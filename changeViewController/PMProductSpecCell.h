//
//  PMProductSpecCell.h
//  changeViewController
//
//  Created by pmit on 14/12/30.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMProductSpecCell : UITableViewCell
@property(nonatomic,strong)NSMutableArray *aBtnArray;
@property(nonatomic,strong)NSMutableArray *bBtnArray;
@property (strong,nonatomic) NSMutableArray *cBtnArray;
@property (strong,nonatomic) NSMutableArray *dBtnArray;
@property (strong,nonatomic) NSMutableArray *eBtnArray;

@property(nonatomic,strong)UIButton *minusBtn;
@property(nonatomic,strong)UIButton *plusBtn;
@property(nonatomic,strong)UILabel *countLB;


- (void)createUIWithASpecName:(NSString *)aName BSpecName:(NSString *)bName CSpecName:(NSString *)cName DSpecName:(NSString *)dName ESpecName:(NSString *)eName ASpecArr:(NSArray *)ASpecArr BSpecArr:(NSArray *)BSpecArr CSpecArr:(NSArray *)CSpecArr DSpecArr:(NSArray *)DSpecArr ESpecArr:(NSArray *)ESpecArr;

@end
