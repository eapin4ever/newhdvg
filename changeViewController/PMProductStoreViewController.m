//
//  PMProductStoreViewController.m
//  changeViewController
//
//  Created by pmit on 15/1/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "PMProductStoreViewController.h"
#import "PMMyGoodsViewController.h"
#import "PMStoreScreenTitleCell.h"
#import "PMStoreScreenDetailCell.h"
#import "PMStoreScreenOneDelegate.h"
#import "PMStoreScreenTwoDelegate.h"

@interface PMProductStoreViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, PMStoreScreenOneDelegate>
@property(nonatomic,strong)NSMutableArray *productArr;
@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)UILabel *redLine;
@property(nonatomic,strong)UITextField *searchTF;

@property(nonatomic,strong)UIView *toastView;
@property(nonatomic,copy)NSString *ascendType;
@property(nonatomic,copy)NSString *ascendKey;

@property(nonatomic,strong)NSMutableDictionary *params;
@property(nonatomic,assign)NSInteger totalPages;
@property(nonatomic,assign)BOOL isLoad;//判断是不是加载
@property (strong,nonatomic) UIView *noResultView;
@property (strong,nonatomic) UILabel *noResultLB;
@property (assign,nonatomic) BOOL isShow;
@property (strong,nonatomic) UIView *searchView;
@property (strong,nonatomic) UISegmentedControl *segment;

@property (strong, nonatomic) UIImageView *priceIma;
@property (strong, nonatomic) UIImageView *priceImage;
@property (strong, nonatomic) UIImageView *screenImage;

// 商品库筛选
@property (strong,nonatomic) UIView *filterView;
@property (strong,nonatomic) UIView *filterCView;
@property (strong, nonatomic) UITableView *screenTableView;
@property (strong, nonatomic) UITableView *screenTableView2;

@property (strong, nonatomic) NSMutableArray *screenTitleArray;
@property (strong, nonatomic) NSMutableArray *screenDetailArray;

@property (strong, nonatomic) PMStoreScreenOneDelegate *oneDelegate;
@property (strong, nonatomic) PMStoreScreenTwoDelegate *twoDelegate;
@property (strong, nonatomic) NSMutableArray *hasRefreshArr;
@property (assign, nonatomic) NSInteger currentPage;

@end

@implementation PMProductStoreViewController
{
    BOOL _isAscending;
    NSInteger _lastSelectedIndex;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.productArr = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.firstClassIdArray = [NSMutableArray array];
    self.secondClassIdArray = [NSMutableArray array];
    self.thirdClassIdArray = [NSMutableArray array];
    self.hasRefreshArr = [NSMutableArray array];
    self.currentPage = 1;
    
    self.title = @"商品库";
    [self initSearchView];
    [self initTopBar];
    [self initTableView];
    
    [self getScreenData];
    [self creataStoreScreenPageUI];
    
    self.isShow = NO;
    _isAscending = YES;
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 20, 20);
    [rightBtn setImage:[UIImage imageNamed:@"search_01"] forState:UIControlStateNormal];
    rightBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [rightBtn addTarget:self action:@selector(searchShow:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = searchItem;
    
    //初始值
    self.ascendKey = @"priceOrder";
    self.ascendType = @"asc";
    
    [self sortWithKey:self.ascendKey ascending:_isAscending];
}

- (void)initSearchView
{
    UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    searchView.backgroundColor = RGBA(231, 231, 231, 1);
    UITextField *searchText = [[UITextField alloc] initWithFrame:CGRectMake(10, 7, WIDTH - 20 - 60, 26)];
    searchText.backgroundColor = [UIColor whiteColor];
    
    searchText.placeholder = @"请输入要搜索的商品";
    [searchText setValue:[UIFont boldSystemFontOfSize:12] forKeyPath:@"_placeholderLabel.font"];
    searchText.contentHorizontalAlignment = UIControlContentVerticalAlignmentCenter;
    [searchText setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, searchText.bounds.size.width,searchText.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [searchText setValue:NAVTEXTCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    
    [searchText.layer setBorderWidth:0];
    searchText.returnKeyType = UIReturnKeySearch;
    searchText.textColor = [UIColor blackColor];
    searchText.font = [UIFont systemFontOfSize:14];
    
    searchText.delegate = self;
    [searchView addSubview:searchText];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.backgroundColor = [UIColor redColor];
    [searchBtn setTitle:@"Search" forState:UIControlStateNormal];
    searchBtn.frame = CGRectMake(WIDTH - 20 - 70, 7, 70, 26);
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [searchBtn addTarget:self action:@selector(clickToSearch:) forControlEvents:UIControlEventTouchUpInside];
    [searchView addSubview:searchBtn];
    
    self.searchTF = searchText;
    self.searchView = searchView;
    [self.view addSubview:searchView];
}

- (void)initTopBar
{
    
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:@[@"价格",@"销量",@"佣金",@"筛选"]];
    //不同屏幕固定大小
    segment.frame = CGRectMake(0, 0, WIDTH, 40 - HeightRate(5));
    self.segment = segment;
    segment.selectedSegmentIndex = 0;
    
    [self.view addSubview:segment];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [segment addGestureRecognizer:tap];
    
    [segment setTintColor:RGBA(246, 246, 246, 1)];

    //选中时字体颜色
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HDVGRed,NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateSelected];
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor lightGrayColor],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    segment.backgroundColor = RGBA(246, 246, 246, 1);
    
    
    //分割线
    CALayer *line1 = [[CALayer alloc] init];
    line1.frame = CGRectMake(WIDTH/4, 0, 1, 40);
    line1.backgroundColor = RGBA(229, 229, 229, .5).CGColor;
    [segment.layer addSublayer:line1];
    
    CALayer *line2 = [[CALayer alloc] init];
    line2.frame = CGRectMake(WIDTH/4*2, 0, 1, 40);
    line2.backgroundColor = RGBA(229, 229, 229, .5).CGColor;
    [segment.layer addSublayer:line2];
    
    CALayer *line3 = [[CALayer alloc] init];
    line3.frame = CGRectMake(WIDTH/4*3, 0, 1, 40);
    line3.backgroundColor = RGBA(229, 229, 229, .5).CGColor;
    [segment.layer addSublayer:line3];
    
    // 价格表示图片
    UIImageView *priceIma = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH / 4 - 25, 15, 7, 12)];
    [priceIma setImage:[UIImage imageNamed:@"arrow_price"]];
    self.priceIma = priceIma;
    [self.view insertSubview:priceIma atIndex:10000];
    self.priceIma.hidden = NO;
    
    // 佣金比率表示图片
    UIImageView *priceImage = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH / 4*3 - 10, 15, 7, 12)];
    //UIImageView *priceImage = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(60), 15, 7, 12)];
    [priceImage setImage:[UIImage imageNamed:@"arrow_price"]];
    self.priceImage = priceImage;
    [self.view insertSubview:priceImage atIndex:10000];
    self.priceImage.hidden = YES;
    
    // 筛选图标
    UIImageView *screenIma = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(60), 14, 10, 12)];
    [screenIma setImage:[UIImage imageNamed:@"screen_ima"]];
    self.screenImage = screenIma;
    [self.view insertSubview:screenIma atIndex:10000];
    
    
    //指示条
    self.redLine = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/5/segment.numberOfSegments - 7, 40 - HeightRate(5), WIDTH/5*4/segment.numberOfSegments, HeightRate(5))];
    self.redLine.backgroundColor = HDVGRed;
    [segment addSubview:self.redLine];
}



#pragma mark - 创建tableView
- (void)initTableView
{
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, WIDTH, HEIGHT - 64-40) style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    self.tableView.backgroundColor = HDVGPageBGGray;
    
    [tableView registerClass:[PMProductStoreCell class] forCellReuseIdentifier:@"Cell"];
    tableView.tableFooterView = [[UIView alloc] init];
    
    //添加下拉刷新和上拉加载
//    [self addRefreshAction];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.productArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMProductStoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSDictionary *dic = self.productArr[indexPath.section];
    [cell createUI];
    
    [cell.iv sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    [cell setContentTitle:[dic objectNullForKey:@"productName"] location:[dic objectNullForKey:@"areaName"] sellNum:[dic objectNullForKey:@"xlTotal"] rate:[dic objectNullForKey:@"sellScale"] price:[dic objectNullForKey:@"productPrice"]];
    
    cell.addToShopBtn.tag = indexPath.section;
    [cell.addToShopBtn addTarget:self action:@selector(clickToAddToShop:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 15)
    {
        if ([self.hasRefreshArr containsObject:@(indexPath.section)])
        {
            
        }
        else
        {
            [self.hasRefreshArr addObject:@(indexPath.section)];
            self.currentPage += 1;
            NSMutableDictionary *dic = [@{PMSID,@"currentPage":@(self.currentPage),@"pageSize":@(20),@"searchContent":@"",@"priceOrder":@"",@"xlTotalOrder":@"",@"sellScale":@""} mutableCopy];
            
            for (NSString *firstId in self.firstClassIdArray)
            {
                [dic setValue:firstId forKey:@"firstClassId"];
            }
            
            for (NSString *secondId in self.secondClassIdArray)
            {
                [dic setValue:secondId forKey:@"secondClassId"];
            }
            
            for (NSString *thirdId in self.thirdClassIdArray)
            {
                [dic setValue:thirdId forKey:@"thirdClassId"];
            }
            
            [self getRefreshWithParam:dic];
        }
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return WidthRate(200) + 50;
}

#pragma mark 点击商品，进入商品详情
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.view.userInteractionEnabled = NO;
    NSDictionary *dic = self.productArr[indexPath.section];

    PMMyGoodsViewController *vc = [[PMMyGoodsViewController alloc] init];
    vc.productId = [dic objectNullForKey:@"productId"];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:vc];

}



#pragma mark - 排序
- (void)tapAction:(UITapGestureRecognizer *)tap
{
    //使用tap去切换可以在segment selected状态下继续点击。
    UISegmentedControl *segment = (UISegmentedControl *)tap.view;
    CGPoint point = [tap locationInView:segment];
    if(point.x < WIDTH/4)
    {
        segment.selectedSegmentIndex = 0;
    }
    else if(point.x > WIDTH/4*2 && point.x < WIDTH/4*3)
    {
        segment.selectedSegmentIndex = 2;
    }
    else if(point.x > WIDTH/4*3)
    {
        segment.selectedSegmentIndex = 3;
    }
    else
    {
        segment.selectedSegmentIndex = 1;
    }
    
//    if(point.x <WIDTH/3)
//    {
//        segment.selectedSegmentIndex = 0;
//    }
//    else if(point.x >WIDTH/3*2)
//    {
//        segment.selectedSegmentIndex = 2;
//    }
//    else
//    {
//        segment.selectedSegmentIndex = 1;
//    }
    [self switchFilter:segment];
    
}

//切换不同条件排序
- (void)switchFilter:(UISegmentedControl *)sender
{
    NSString *key;
    switch (sender.selectedSegmentIndex)
    {
        case 0://价格
            key = @"priceOrder";
            break;
        case 1://销量
            key = @"xlTotalOrder";
            break;
        case 2://佣金率
            key = @"sellScale";
            break;
        case 3://筛选
            key = @"screen";
            break;
    }
    self.ascendKey = key;
    
    if(_lastSelectedIndex == sender.selectedSegmentIndex)
    {
        if(_lastSelectedIndex == 0)//价格
        {
            self.priceIma.hidden = NO;
            self.priceImage.hidden = YES;
        }
        else if(_lastSelectedIndex == 1)//销量
        {
            self.priceIma.hidden = YES;
            self.priceImage.hidden = YES;
        }
        else if(_lastSelectedIndex == 2)//佣金比率
        {
            self.priceIma.hidden = YES;
            self.priceImage.hidden = NO;
        }
        else if(_lastSelectedIndex == 3)//筛选
        {
            self.priceIma.hidden = YES;
            self.priceImage.hidden = YES;
            
            self.filterView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
                
                self.filterCView.frame = CGRectMake(40, 0, WIDTH - 40, HEIGHT);
                
            }];
        }
        
        _isAscending = !_isAscending;
    }
    else {
        _lastSelectedIndex = sender.selectedSegmentIndex;
        
        if(_lastSelectedIndex == 0)//价格
        {
            self.priceIma.hidden = NO;
            self.priceImage.hidden = YES;
        }
        else if(_lastSelectedIndex == 1)//销量
        {
            self.priceIma.hidden = YES;
            self.priceImage.hidden = YES;
        }
        else if(_lastSelectedIndex == 2)//佣金比率
        {
            self.priceIma.hidden = YES;
            self.priceImage.hidden = NO;
        }
        else if(_lastSelectedIndex == 3)//筛选
        {
            self.priceIma.hidden = YES;
            self.priceImage.hidden = YES;
            self.filterView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
                
                self.filterCView.frame = CGRectMake(40, 0, WIDTH - 40, HEIGHT);
                
            }];
        }
        
        
        //指示条滑动的动画
        [UIView animateWithDuration:0.3 animations:^{
            CGRect rect = self.redLine.frame;
            rect.origin.x = WIDTH/5/sender.numberOfSegments + WIDTH/sender.numberOfSegments*sender.selectedSegmentIndex - 7;
            self.redLine.frame = rect;
        }];

        _isAscending = YES;
    }
    
    if (![key isEqualToString:@"screen"])
    {
        [self sortWithKey:key ascending:_isAscending];
    }
}


//根据排序状态不同去请求数据
- (void)sortWithKey:(NSString *)key ascending:(BOOL)ascending
{
    NSString *ascend;
    
    CGFloat angle = 0;
    CGFloat angle2 = 0;
    if(ascending) {
        ascend = @"asc";
        
        // 价格
        angle = _isAscending ? M_PI : 0;
        [self.priceIma setTransform:CGAffineTransformMakeRotation(angle)];
        
        // 佣金比率
        angle2 = _isAscending ? M_PI : 0;
        [self.priceImage setTransform:CGAffineTransformMakeRotation(angle2)];
    }
    else {
        ascend = @"desc";
        
        angle = _isAscending ? M_PI : 0;
        [self.priceIma setTransform:CGAffineTransformMakeRotation(angle)];
        
        angle2 = _isAscending ? M_PI : 0;
        [self.priceImage setTransform:CGAffineTransformMakeRotation(angle2)];
    }
    
    self.ascendType = ascend;
    //NSString *areaId = [[[PMUserInfos shareUserInfo].defaultArea componentsSeparatedByString:@","] firstObject];
    
    NSMutableDictionary *param = [@{PMSID,@"currentPage":@(1),@"pageSize":@(20),@"priceOrder":@"",@"xlTotalOrder":@"",@"sellScale":@""} mutableCopy];
    if([self.searchTF.text isEqualToString:@""] == NO)
    {
        [param setValue:self.searchTF.text forKey:@"searchContent"];
    }
    
    for (NSString *firstId in self.firstClassIdArray)
    {
        [param setValue:firstId forKey:@"firstClassId"];
    }
    
    for (NSString *secondId in self.secondClassIdArray)
    {
        [param setValue:secondId forKey:@"secondClassId"];
    }
    
    for (NSString *thirdId in self.thirdClassIdArray)
    {
        [param setValue:thirdId forKey:@"thirdClassId"];
    }
    
    //给需要排序的赋值
    [param setValue:ascend forKey:key];
    
    self.isLoad = NO;//不是加载数据
    [self getModelsWithParam:param finish:^{
        [self.tableView reloadData];
        //回滚到顶部
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    } error:nil];
}

#pragma mark - 获得筛选数据
- (void)getScreenData
{
    NSMutableDictionary *param = [@{PMSID,@"isMobile":@(1),@"currentPage":@(1),@"pageSize":@(20),@"priceOrder":@"",@"xlTotalOrder":@"",@"sellScale":@"",@"secondClassId":@"",@"threeClassId":@""} mutableCopy];
    
    [[PMNetworking defaultNetworking] request:PMRequestStateClassGetAllClassByTree WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            self.screenTitleArray = [NSMutableArray array];
            self.screenDetailArray = [NSMutableArray array];
            
            if ([dic isKindOfClass:[NSDictionary class]] && !isNull(dic))
            {
                self.screenTitleArray = [dic objectNullForKey:@"data"];
                self.oneDelegate.screenOneArray = self.screenTitleArray;
                [self.screenTableView reloadData];
                
                if (self.screenTitleArray.count > 0)
                {
                    NSIndexPath *ip=[NSIndexPath indexPathForRow:0 inSection:0];
                    [self.screenTableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionBottom];
                    
                    NSDictionary *firstDic = [self.screenTitleArray firstObject];
                    self.twoDelegate.screenTwoDict = firstDic;
                    [self.screenTableView2 reloadData];
                }
                
                
            }
            
            
        }
    }showIndicator:NO];
}

#pragma mark - 点击搜索
- (void)clickToSearch:(UIButton *)sender
{
    //收键盘
    [self.searchTF resignFirstResponder];
    
    //NSString *areaId = [[[PMUserInfos shareUserInfo].defaultArea componentsSeparatedByString:@","] firstObject];
    
    NSMutableDictionary *param = [@{PMSID,@"currentPage":@(1),@"pageSize":@(20),@"searchContent":self.searchTF.text,@"priceOrder":@"",@"xlTotalOrder":@"",@"sellScale":@""} mutableCopy];
    [param setValue:self.ascendType forKey:self.ascendKey];
    
    [self getModelsWithParam:param finish:^{
        [self.tableView reloadData];
    } error:^{
        
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self clickToSearch:nil];
    return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.tableView)
    {
        CGFloat sectionHeaderHeight = 5;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    
    [self.searchTF resignFirstResponder];
    
}

#pragma mark 点击添加到店铺
- (void)clickToAddToShop:(UIButton *)sender
{
    //+我的代言
    /*
     PM_SID     Y	String		Token
     areaId     Y	String		区域ID
     productId	Y	String		商品id
     */
    sender.enabled = NO;
    NSDictionary *dic = self.productArr[sender.tag];
    [self addToShopWithParam:@{PMSID,@"areaId":[dic objectNullForKey:@"areaId"],@"productId":[dic objectNullForKey:@"productId"]} button:sender];
}


//商品库显示
/*
 PM_SID             Y	String		Token
 currentPage		Y	Int         当前页
 pageSize           N	Int         一页显示多少个
 searchContent      N	String		搜索框中的内容
 areaId             Y	String		区域id
 priceOrder         Y	String		价格排序asc 递增  desc递减
 xlTotalOrder       Y	String		销量排序asc 递增  desc递减
 sellScale          Y	String		佣金比率排序asc 递增  desc递减
 */
#pragma mark - 获取数据
- (void)getModelsWithParam:(NSDictionary *)param finish:(myFinishBlock)finish error:(myFinishBlock)error
{
    self.params = [param mutableCopy];
    
    [[PMNetworking defaultNetworking] request:PMRequestStateShareShowProductStoreInfo WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            if(self.isLoad)//如果是加载，则拼接
            {
                [self.productArr addObjectsFromArray:[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"]];
            }
            else
            {
                self.productArr = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
            }
            //得到总页数
            self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
            if (self.productArr.count == 0)
            {
                if (self.noResultView)
                {
                    self.noResultView.hidden = NO;
                }
                else
                {
                    [self createNoResult];
                }
            }
            else
            {
                self.noResultView.hidden = YES;
            }
            
            if(finish)
                finish();
        }
        else
        {
            if(error)
                error();
            if (self.noResultView)
            {
                self.noResultView.hidden = NO;
            }
            else
            {
                [self createNoResult];
            }
            
        }
    }showIndicator:YES];
}

//+我的代言
/*
 PM_SID     Y	String		Token
 areaId     Y	String		区域ID
 productId	Y	String		商品id
 */
#pragma mark  添加到店铺
- (void)addToShopWithParam:(NSDictionary *)param button:(UIButton *)button
{
    [[PMNetworking defaultNetworking] request:PMRequestStateShareAddToShop WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            button.enabled = YES;
            [self showHintByToast:@"成功添加到代言商品中"];
            
            [self.productArr removeObjectAtIndex:button.tag];
            NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:button.tag];
            [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView reloadData];

        }
        else
        {
            button.enabled = YES;
            showRequestFailAlertView;

        }
    }showIndicator:NO];
}


#pragma mark - 使用吐司式提示
- (void)showHintByToast:(NSString *)title
{
    if(!self.toastView)
    {
        UIView *toastView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WidthRate(320), HeightRate(200))];
        toastView.center = self.view.center;
        toastView.alpha = 0;
        toastView.layer.cornerRadius = 6.0;
        toastView.clipsToBounds =YES;
        toastView.backgroundColor = [UIColor blackColor];
        self.toastView = toastView;
        [self.view addSubview:toastView];
        
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(toastView.bounds.size.width/2 - WidthRate(30), HeightRate(40), WidthRate(60), WidthRate(60))];
        iv.image = [UIImage imageNamed:@"hintIcon"];
        [toastView addSubview:iv];
        
        
        UILabel *toastLB = [[UILabel alloc] initWithFrame:CGRectMake(0, HeightRate(140), toastView.bounds.size.width, HeightRate(30))];
        [toastView addSubview:toastLB];
        
        toastLB.text = title;
        toastLB.font = [UIFont boldSystemFontOfSize:HeightRate(25)];
        toastLB.textColor = [UIColor whiteColor];
        toastLB.textAlignment = NSTextAlignmentCenter;
    }
    
    [UIView animateWithDuration:0.6 animations:^{
        self.toastView.alpha = 0.8;
        self.view.userInteractionEnabled = NO;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.6 animations:^{
            self.toastView.alpha = 0;
        } completion:^(BOOL finished) {
            self.view.userInteractionEnabled = YES;
        }];
    }];
}

#pragma mark - 创建没有数据的界面
- (void)createNoResult
{
    UIView *noResultView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, WIDTH, HEIGHT - 64 - 40)];
    noResultView.backgroundColor = [UIColor whiteColor];
    self.noResultView = noResultView;
    
    UIImageView *noResultIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, noResultView.frame.size.height)];
    noResultIV.image = [UIImage imageNamed:@"productStore_null"];
    noResultIV.contentMode = UIViewContentModeScaleAspectFit;
    [noResultView addSubview:noResultIV];
    
    [self.view insertSubview:noResultView atIndex:100];
    self.noResultView.hidden = NO;
}

- (void)searchShow:(UIButton *)sender
{
    self.isShow = !self.isShow;
    [self.searchTF resignFirstResponder];
    if (self.isShow)
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.segment.frame = CGRectMake(0, 40, WIDTH, 40);
            self.tableView.frame = CGRectMake(0, 80, WIDTH, HEIGHT - 64 - 80);
            self.priceIma.frame = CGRectMake(WIDTH / 4 - 25, 55, 7, 12);
            self.priceImage.frame = CGRectMake(WIDTH / 4*3 - 10, 55, 7, 12);
            self.screenImage.frame = CGRectMake(WIDTH - WidthRate(60), 55, 10, 12);
        }];
        
        
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.segment.frame = CGRectMake(0, 0, WIDTH, 40);
            self.tableView.frame = CGRectMake(0, 40, WIDTH, HEIGHT - 64 - 40);
            self.priceIma.frame = CGRectMake(WIDTH / 4 - 25, 15, 7, 12);
            self.priceImage.frame = CGRectMake(WIDTH / 4*3 - 10, 15, 7, 12);
            self.screenImage.frame = CGRectMake(WIDTH - WidthRate(60), 14, 10, 12);
        }];
        
        
        NSMutableDictionary *param = [@{PMSID,@"currentPage":@(1),@"pageSize":@(20),@"searchContent":@"",@"priceOrder":@"",@"xlTotalOrder":@"",@"sellScale":@""} mutableCopy];
        [param setValue:self.ascendType forKey:self.ascendKey];
        
        [self getModelsWithParam:param finish:^{
            [self.tableView reloadData];
        } error:^{
            
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    return 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = HDVGPageBGGray;
    return headerView;
}


#pragma mark - 创建商品库筛选页面
- (void)creataStoreScreenPageUI
{
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    self.filterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.filterView.backgroundColor = [UIColor clearColor];
    self.filterView.hidden = YES;
    [window addSubview:self.filterView];
    
    UIView *darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    darkView.backgroundColor = [UIColor blackColor];
    darkView.alpha = 0.8;
    [self.filterView addSubview:darkView];
    
    self.filterCView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH - 40, HEIGHT)];
    self.filterCView.backgroundColor = [UIColor whiteColor];
    [window addSubview:self.filterCView];
    
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, self.filterCView.bounds.size.width, 40)];
    btnView.backgroundColor = [UIColor whiteColor];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(15, 5, 40, 30);
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelStoreScreenClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnView addSubview:cancelBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame =CGRectMake(btnView.bounds.size.width - 15 - 40, 5, 40, 30);
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureStoreSelect:) forControlEvents:UIControlEventTouchUpInside];
    [btnView addSubview:sureBtn];
    [self.filterCView addSubview:btnView];
    
    self.oneDelegate = [[PMStoreScreenOneDelegate alloc] init];
    self.twoDelegate = [[PMStoreScreenTwoDelegate alloc] init];
    
    self.screenTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.filterCView.bounds.size.width * 0.4, self.filterCView.bounds.size.height - 60) style:UITableViewStylePlain];
    self.oneDelegate.screenOneDelegate = self;
    self.screenTableView.delegate = self.oneDelegate;
    self.screenTableView.dataSource = self.oneDelegate;
    self.screenTableView.backgroundColor = HDVGPageBGGray;
    self.screenTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.screenTableView registerClass:[PMStoreScreenTitleCell class] forCellReuseIdentifier:@"titleCell"];
    [self.filterCView addSubview:self.screenTableView];
    
    self.screenTableView2 = [[UITableView alloc] initWithFrame:CGRectMake(self.filterCView.bounds.size.width * 0.4, 60, self.filterCView.bounds.size.width * 0.6, self.filterCView.bounds.size.height - 60) style:UITableViewStylePlain];
    self.screenTableView2.delegate = self.twoDelegate;
    self.screenTableView2.dataSource = self.twoDelegate;
    self.twoDelegate.productStoreVC = self;
    self.screenTableView2.backgroundColor = [UIColor whiteColor];
    self.screenTableView2.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.screenTableView2 registerClass:[PMStoreScreenDetailCell class] forCellReuseIdentifier:@"detailsCell"];
    [self.filterCView addSubview:self.screenTableView2];
    
}

#pragma mark
- (void)passDictionary:(NSDictionary *)dict
{
    self.twoDelegate.screenTwoDict = dict;
    [self.screenTableView2 reloadData];
}

// 筛选取消按钮响应事件
- (void)cancelStoreScreenClick:(UIButton *)button
{
    [self animationDismiss];
}

- (void)animationDismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.filterCView.frame = CGRectMake(WIDTH, 0, WIDTH - 40, HEIGHT);
        
    } completion:^(BOOL finished) {
        self.filterView.hidden = YES;
    }];
}

- (void)saveBtnClick:(UIButton *)sender
{
    [self animationDismiss];
    
//    NSMutableDictionary *param = [@{@"currentPage":@(1),@"pageSize":@(20),@"key":self.searchString} mutableCopy];
//    for (NSString *brandId in self.brandIds)
//    {
//        [param setValue:brandId forKey:@"brandIds"];
//    }
//    
//    for (NSString *specId in self.specIds)
//    {
//        [param setValue:specId forKey:@"specLists"];
//    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateProductSearch WithParameters:nil callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
//            self.dicArray = [[[[dic objectNullForKey:@"data"] objectNullForKey:@"pager"] objectNullForKey:@"beanList"] mutableCopy];
//            [self refreshSearchResult];
        }
        
    } showIndicator:YES];
}

// 筛选确定按钮响应事件
- (void)screenOkButtonClick:(UIButton *)sender
{
//    [UIView animateWithDuration:0.3 animations:^{
//        
//        [self.screenTableView2 setFrame:CGRectMake(self.screenView.frame.size.width, 40, self.screenView.frame.size.width, HEIGHT)];
//        [self.screenTableView setFrame:CGRectMake(0, 40, self.screenView.frame.size.width, HEIGHT)];
//    } completion:^(BOOL finished){
//        
//    }];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.view.userInteractionEnabled = YES;
}


- (void)sureStoreSelect:(UIButton *)sender
{
    self.productArr = [NSMutableArray array];
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.filterCView.frame = CGRectMake(WIDTH, 0, WIDTH - 40, HEIGHT);
        
    } completion:^(BOOL finished) {
        
        self.filterView.hidden = YES;
        NSMutableDictionary *param = self.params;
        for (NSString *firstId in self.firstClassIdArray)
        {
            [param setValue:firstId forKey:@"firstClassId"];
        }
        
        NSLog(@"ssss1 --> %@",self.firstClassIdArray);
        
        for (NSString *secondId in self.secondClassIdArray)
        {
            [param setValue:secondId forKey:@"secondClassId"];
        }
        
        NSLog(@"ssss2 --> %@",self.secondClassIdArray);
        
        for (NSString *thirdId in self.thirdClassIdArray)
        {
            [param setValue:thirdId forKey:thirdId];
        }
        
        NSLog(@"ssss --> %@",self.thirdClassIdArray);
        
        
        [[PMNetworking defaultNetworking] request:PMRequestStateShareShowProductStoreInfo WithParameters:param callBackBlock:^(NSDictionary *dic) {
            if (intSuccess)
            {
                self.params = param;
                if(self.isLoad)//如果是加载，则拼接
                {
                    [self.productArr addObjectsFromArray:[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"]];
                }
                else
                {
                    self.productArr = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                }
                //得到总页数
                self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
                if (self.productArr.count == 0)
                {
                    if (self.noResultView)
                    {
                        self.noResultView.hidden = NO;
                    }
                    else
                    {
                        [self createNoResult];
                    }
                }
                else
                {
                    self.noResultView.hidden = YES;
                }
                
                [self.tableView reloadData];
            }
            else
            {
                if (self.noResultView)
                {
                    self.noResultView.hidden = NO;
                }
                else
                {
                    [self createNoResult];
                }
            }
            
        } showIndicator:YES];
        
    }];
}

- (void)getRefreshWithParam:(NSDictionary *)param
{
    [[PMNetworking defaultNetworking] request:PMRequestStateShareShowProductStoreInfo WithParameters:param callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            if (!isNull(dic) && [dic isKindOfClass:[NSDictionary class]])
            {
                [self.productArr addObjectsFromArray:[[dic objectForKey:@"data"] objectForKey:@"beanList"]];
                [self.tableView reloadData];
            }
        }
        else
        {
            
        }
    } showIndicator:NO];
}

@end
