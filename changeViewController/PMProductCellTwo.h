//
//  PMProductCellTwo.h
//  changeViewController
//
//  Created by pmit on 15/2/4.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMMyPhoneInfo.h"

@interface PMProductCellTwo : UITableViewCell
@property(nonatomic,strong)UIImageView *iv1;
@property(nonatomic,strong)UIImageView *iv2;
@property(nonatomic,strong)UIImageView *iv3;

- (void)createUI;

- (void)setProductOnetitle1:(NSString *)title1 price1:(double)price1 title2:(NSString *)title2 price2:(double)price2 title3:(NSString *)title3 price3:(double)price3;

- (void)addTarget:(id)target selector:(SEL)sel row:(NSInteger)row;
@end
