//
//  YTGifActivityIndicator.m
//  changeViewController
//
//  Created by pmit on 15/3/27.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YTGifActivityIndicator.h"
#import "PMMyPhoneInfo.h"
#import <UIImageView+PlayGIF.h>

#define IMAGEVIEW_SIZE 200
#define IMAGEVIEW_HEIGHT 300

static YTGifActivityIndicator *_indicator;
@implementation YTGifActivityIndicator
{
    UIImageView *_iv;
}

+ (YTGifActivityIndicator *)defaultIndicator
{
    if(!_indicator)
    {
        _indicator = [[YTGifActivityIndicator alloc] init];
        _indicator.imageArr = [NSMutableArray array];
        _indicator.imageCount = 0;
        _indicator.imageArr = [@[[UIImage imageNamed:@"loading_1.png"],[UIImage imageNamed:@"loading_2.png"]] mutableCopy];
    }
    return _indicator;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.frame = CGRectMake(0, 64, WIDTH, HEIGHT - 64);
        UIView *noAlphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        noAlphaView.backgroundColor = [UIColor clearColor];
        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        backView.backgroundColor = [UIColor whiteColor];
        backView.alpha = 1;
        [noAlphaView addSubview:backView];
        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, IMAGEVIEW_SIZE, IMAGEVIEW_SIZE)];
        _iv.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
        _iv.contentMode = UIViewContentModeScaleAspectFit;
        [noAlphaView addSubview:_iv];
        
        UILabel *loadingLB = [[UILabel alloc] initWithFrame:CGRectMake(_iv.frame.origin.x, _iv.frame.origin.y + _iv.bounds.size.height, _iv.bounds.size.width, 40)];
        loadingLB.text = @"努力加载中...";
        loadingLB.textColor = [UIColor lightGrayColor];
        loadingLB.font = [UIFont systemFontOfSize:16.0f];
        loadingLB.textAlignment = NSTextAlignmentCenter;
        [noAlphaView addSubview:loadingLB];
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(changeImages:) userInfo:nil repeats:YES];
        
        [self addSubview:noAlphaView];
        self.alpha = 1;
    }
    return self;
}

- (void)start
{
    [self.timer setFireDate:[NSDate date]];
}

- (void)stop
{
    ((UIWindow *)[[UIApplication sharedApplication].windows firstObject]).userInteractionEnabled = YES;
    
    [self.timer setFireDate:[NSDate distantFuture]];
    [self removeFromSuperview];
}

- (void)changeImages:(NSTimer *)timer
{
    self.imageCount++;
    NSInteger thisCount = self.imageCount % 2;
    UIImage *thisImage = self.imageArr[thisCount];
    _iv.image = thisImage;
}

@end
