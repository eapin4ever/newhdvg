//
//  PMGetAreaID.h
//  changeViewController
//
//  Created by pmit on 14/12/19.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMGetAreaID : NSObject
//area 以空格分开省市区
//如  广东省 广州市 天河区
+ (NSString *)getAreaIDByArea:(NSString *)area;

@end
