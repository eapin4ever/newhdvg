//
//  PMImageCroperSingleton.h
//  changeViewController
//
//  Created by pmit on 14/11/10.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPImageCropperViewController.h"

@protocol PMImageCroperDelegate <NSObject>

- (void)finishActionWithCompressImgData:(NSData *)data;

@end

@interface PMImageCroperSingleton : UIViewController<VPImageCropperDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>
@property (nonatomic, strong) UIImageView *portraitImageView;
@property (nonatomic, weak)id<PMImageCroperDelegate> delegate;

+ (PMImageCroperSingleton *)addProtraitToTarget:(UIViewController *)viewController SuperView:(UIView *)view WithFrame:(CGRect )portraitFrame;




@end