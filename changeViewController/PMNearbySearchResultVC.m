//
//  PMNearbySearchResultVC.m
//  changeViewController
//
//  Created by pmit on 14/12/16.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#define originY HeightRate(0)

#import "PMNearbySearchResultVC.h"
#import "PMGoodsReusableView.h"
#import "MasterViewController.h"
#import "PMMyGoodsViewController.h"
#import "PMToastHint.h"

@interface PMNearbySearchResultVC ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)UICollectionView *resultCollectionView;

@property(nonatomic,strong)NSMutableDictionary *params;
@property(nonatomic,assign)NSInteger totalPages;
@property(nonatomic,assign)BOOL isRefresh;
@property (strong,nonatomic) NSMutableArray *loadArr;

@end

@implementation PMNearbySearchResultVC
- (void)viewDidLoad
{
    [super viewDidLoad];
    _isRefresh = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    self.loadArr = [NSMutableArray array];
    [self searchResultUI];
}

#pragma mark - 搜索结果展示视图
- (void)searchResultUI
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    [flowLayout setMinimumInteritemSpacing:5];
    [flowLayout setMinimumLineSpacing:HeightRate(20)];//上下间距
    [flowLayout setItemSize:CGSizeMake(WidthRate(345), WidthRate(490))];//宽度、高度
    [flowLayout setSectionInset:UIEdgeInsetsMake(HeightRate(10), WidthRate(20), 0, WidthRate(20))];
    
    self.resultCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, originY , WIDTH, HEIGHT - originY - 64) collectionViewLayout:flowLayout];
    [self.resultCollectionView setBackgroundColor:HDVGPageBGGray];
    [self.resultCollectionView setDataSource:self];
    [self.resultCollectionView setDelegate:self];
    [self.resultCollectionView registerClass:[PMGoodsReusableView class] forCellWithReuseIdentifier:@"reuseItem"];
    
    [self.resultCollectionView setContentInset:UIEdgeInsetsMake(0, 0, 60, 0)];
    
    [self.view addSubview:self.resultCollectionView];
    
    //添加下拉刷新和上拉加载
    [self addRefreshAction];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dicArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMGoodsReusableView *item = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseItem" forIndexPath:indexPath];
    
    NSDictionary *dic = self.dicArray[indexPath.row];
    NSString *discount;
    BOOL isHasDiscount = NO;
    // ---------------------------- wei ----------------------------
    // 折扣
    id discountDic = [dic objectNullForKey:@"zkMap"];
    if ([discountDic isKindOfClass:[NSDictionary class]] && !isNull(discountDic))
    {
        discount = [discountDic objectNullForKey:@"zkblShow"];
        if (discount.length != 0) {
            item.showDiscount = YES;
            isHasDiscount = YES;
        }
        else {
            item.showDiscount = NO;
            isHasDiscount = NO;
        }
    }
    else
    {
        item.showDiscount = NO;
        isHasDiscount = NO;
        discount = @"10";
    }
    // ---------------------------- wei ----------------------------
    
    item.showDistance = YES;
    
    [item createUI];
    
    [item.imageView sd_setImageWithURL:[dic objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"loading_image"]];
    
    item.showDiscount = NO;
    
    [item setContentTitle:[dic objectNullForKey:@"productName"] price:isHasDiscount ? [[discountDic objectNullForKey:@"discountPrice"] doubleValue] : [[dic objectNullForKey:@"productPrice"] doubleValue] discount:discount];
    
    if (indexPath.row == self.dicArray.count - 5)
    {
        if (![self.loadArr containsObject:@(indexPath.row)])
        {
            self.isRefresh = NO;
            NSInteger currentPage = [[self.params objectNullForKey:@"currentPage"] integerValue];
            if(currentPage < self.totalPages)//如果小于总页数则刷新
            {
                //增加当前页
                currentPage++;
                //获取数据
                [self.params setValue:@(currentPage) forKey:@"currentPage"];
                [self getModelsWithParam:self.params finish:^{
                    [self.loadArr addObject:@(indexPath.row)];
                    [self.resultCollectionView reloadData];
                    
                } error:^{
                    
                }];
            }
        }
        
    }
    
    return item;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //显示加载提示视图
//    [self lodingViewShow:YES];
    NSDictionary *dic = self.dicArray[indexPath.row];
//    YBGoodDetailViewController *detailVC = [[YBGoodDetailViewController alloc] init];
    PMMyGoodsViewController *detailVC = [[PMMyGoodsViewController alloc] init];
    //请求数据
    detailVC.productId = [dic objectNullForKey:@"id"];
    [self.navigationController pushViewControllerWithNavigationControllerTransition:detailVC];
}


#pragma mark - 从服务器获得数据
- (void)getModelsWithParam:(NSDictionary *)param finish:(myFinishBlock)finish error:(myFinishBlock)error
{
    /*
     currentPage	Y	int         当前页
     pageSize       N	int         一页显示条数（默认20条）
     longitude      N	String		经度
     latitude       N	String		纬度
     */
    
    if(param == nil)
    {
        param = @{@"currentPage":@(1),@"pageSize":@(20)};
        self.params = [param mutableCopy];
    }
    
    if([PMUserInfos shareUserInfo].myLocation)
    {
        NSString *location = [PMUserInfos shareUserInfo].myLocation;
        NSArray *arr = [location componentsSeparatedByString:@","];
        
        [self.params setValue:arr[0] forKey:@"latitude"];
        [self.params setValue:arr[1] forKey:@"longitude"];
        
    }
    else//获取失败使用默认经纬度
    {
        [self.params setValuesForKeysWithDictionary:@{@"latitude":@"23.123769",@"longitude":@"113.395305"}];
    }
    
    [[PMNetworking defaultNetworking] request:PMRequestStateSearchProductInCountInfo WithParameters:self.params callBackBlock:^(NSDictionary *dic) {
        if(intSuccess == 1)
        {
            if(_isRefresh)
            {
                //刷新数组
                self.dicArray = [[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                //得到总页数
                self.totalPages = [[[dic objectNullForKey:@"data"] objectNullForKey:@"totalPages"] integerValue];
            }
            else
            {
                //添加objs
                [self.dicArray addObjectsFromArray:[[dic objectNullForKey:@"data"] objectNullForKey:@"beanList"]];
            }
            //刷新表单
            [self.resultCollectionView reloadData];
            if(finish)//如果不是nil的话，执行block
                finish();
        }
        else
        {
            if(error)
                error();
            showRequestFailAlertView;
        }
    }showIndicator:NO];
}


#pragma mark - 低内存警告
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


//#pragma mark - 加载提示
//- (void)lodingViewShow:(BOOL)isShow
//{
//    
//    if(isShow)
//    {
//        [UIView animateWithDuration:0.3 animations:^{
//
//            self.view.userInteractionEnabled = NO;
//        }];
//    }
//    else
//    {
//        [UIView animateWithDuration:0.3 animations:^{
//
//            self.view.userInteractionEnabled = YES;
//        }];
//    }
//}

#pragma mark - 下拉刷新等
- (void)addRefreshAction
{
    __weak PMNearbySearchResultVC *weakSelf = self;

    //使用普通的下拉刷新
    [self.resultCollectionView addRefreshHeaderViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        weakSelf.isRefresh = YES;
        [weakSelf.searchVC hideResultVC];
        [weakSelf.resultCollectionView headerEndRefreshingWithResult:JHRefreshResultSuccess];
        
    }];
}

- (void)dealloc
{
    NSLog(@"释放了");
}


@end
