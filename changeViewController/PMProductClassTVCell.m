//
//  PMProductClassTVCell.m
//  changeViewController
//
//  Created by pmit on 15/1/6.
//  Copyright (c) 2015年 wallace. All rights reserved.
//
#define CELLHI 142

#import "PMProductClassTVCell.h"
#import "PMPreBuyBtn.h"

@implementation PMProductClassTVCell
{
    PMPreBuyBtn *_btn1;
    PMPreBuyBtn *_btn2;
    PMPreBuyBtn *_btn3;
}

- (void)createUI
{
    if(!_btn1)
    {
        _btn1 = [[PMPreBuyBtn alloc] init];
        [self createLabel:_btn1 x:0];
        _btn2 = [[PMPreBuyBtn alloc] init];
        [self createLabel:_btn2 x:WidthRate(160)];
        _btn3 = [[PMPreBuyBtn alloc] init];
        [self createLabel:_btn3 x:WidthRate(160)*2];
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //由于button无法使用SDWebImage的方法，所以只能创建UIImageView类。
        //但是是异步下载的，当图片下载下来后，只能替换到UIImageView对象中的image中
        //所以就创建一个UIImageView的子类，并将button传过去
        //当给image赋值时，也给button替换图片
        self.iv1 = [[PMProductCellImageView alloc] init];
        self.iv2 = [[PMProductCellImageView alloc] init];
        self.iv3 = [[PMProductCellImageView alloc] init];
        
        self.iv1.btn = _btn1;
        self.iv2.btn = _btn2;
        self.iv3.btn = _btn3;
    }
}

- (void)createLabel:(PMPreBuyBtn *)btn x:(CGFloat)x
{
    btn.frame = CGRectMake(x, 0, WidthRate(160), CELLHI);
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
//    btn.layer.borderColor = RGBA(226, 226, 226, 1).CGColor;
//    btn.layer.borderWidth = 0.6;
    
    [btn.discountLB removeFromSuperview];
    btn.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:btn];
}

- (void)setContentLb1Text:(NSString *)text1 Lb2Text:(NSString *)text2 Lb3Text:(NSString *)text3
{
    [_btn1 setTitle:text1 forState:UIControlStateNormal];
    if(text2 == nil)
        _btn2.hidden = YES;
    else
    {
        [_btn2 setTitle:text2 forState:UIControlStateNormal];
        _btn2.hidden = NO;
    }
    
    if(text3 == nil)
        _btn3.hidden = YES;
    else
    {
        [_btn3 setTitle:text3 forState:UIControlStateNormal];
        _btn3.hidden = NO;
    }
}

- (void)setBtnTarget:(id)target selector:(SEL)sel indexPath:(NSIndexPath *)myIndexPath
{
    [_btn1 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    _btn1.tag = myIndexPath.section*1000 + myIndexPath.row*100;
    
    [_btn2 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    _btn2.tag = myIndexPath.section*1000 + myIndexPath.row*100 +1;
    
    [_btn3 addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    _btn3.tag = myIndexPath.section*1000 + myIndexPath.row*100 +2;
    
}

@end






