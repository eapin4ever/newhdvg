//
//  PMOrderDetailViewController.m
//  changeViewController
//
//  Created by pmit on 15/10/27.
//  Copyright © 2015年 wallace. All rights reserved.
//

#import "PMOrderDetailViewController.h"
#import "PMNetworking.h"
#import "PMMyPhoneInfo.h"
#import "PMOrderAddressCell.h"
#import "PMOrderStateCell.h"
#import "PMOrderCodeTimeCell.h"
#import "OrderDetailProductCell.h"
#import "OrderTotalPriceCell.h"
#import "YBSureOrderViewController.h"
#import "YBBillInfos.h"
#import "PMToastHint.h"
#import "YBComplaintsViewController.h"
#import "ComplaintsViewController.h"
#import "PMMyAllOrderViewController.h"
#import "PMBillInfoCell.h"
#import "PMOrderSectionTitleCell.h"
#import "PMMyGoodsViewController.h"
#import "OrderListViewController.h"

#define itemX WidthRate(30)

@interface PMOrderDetailViewController () <UITableViewDataSource,UITableViewDelegate,YBComplaintsViewControllerDelegate,ComplaintsViewControllerDelegate,PMOrderSectionTitleCellDelegate>

@property (strong,nonatomic) UITableView *orderDetailTableView;
@property (strong,nonatomic) NSDictionary *orderInfoDic;
@property (strong,nonatomic) NSArray *orderProductsArr;

@end

@implementation PMOrderDetailViewController

static NSString *const ybAddressCell = @"ybAddressCell";
static NSString *const orderStateCell = @"orderStateCell";
static NSString *const orderCodeTimeCell = @"orderCodeTimeCell";
static NSString *const orderProductCell = @"orderProductCell";
static NSString *const orderTotalPriceCell = @"orderTotalPriceCell";
static NSString *const billInfoCell = @"billInfoCell";
static NSString *const orderTitleCell = @"orderTitleCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"订单详情";
    
    [self buildTableView];
    [self getOrderInfoByNet];
    [self buildBottomView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getOrderInfoByNet
{
    [[PMNetworking defaultNetworking] request:PMRequestStateShowMyLogistics WithParameters:@{@"orderCode":self.orderCode} callBackBlock:^(NSDictionary *dic) {
        
        self.orderInfoDic = [dic objectForKey:@"data"];
        self.orderProductsArr = [self.orderInfoDic objectForKey:@"products"];
        [self.orderDetailTableView reloadData];
        
    } showIndicator:YES];
}

- (void)buildTableView
{
    self.orderDetailTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50) style:UITableViewStylePlain];
    self.orderDetailTableView.delegate = self;
    self.orderDetailTableView.dataSource = self;
    self.orderDetailTableView.backgroundColor = RGBA(241, 241, 241, 1);
    [self.orderDetailTableView registerClass:[PMOrderAddressCell class] forCellReuseIdentifier:ybAddressCell];
    [self.orderDetailTableView registerClass:[PMOrderStateCell class] forCellReuseIdentifier:orderStateCell];
    [self.orderDetailTableView registerClass:[PMOrderCodeTimeCell class] forCellReuseIdentifier:orderCodeTimeCell];
    [self.orderDetailTableView registerClass:[OrderDetailProductCell class] forCellReuseIdentifier:orderProductCell];
    [self.orderDetailTableView registerClass:[OrderTotalPriceCell class] forCellReuseIdentifier:orderTotalPriceCell];
    [self.orderDetailTableView registerClass:[PMBillInfoCell class] forCellReuseIdentifier:billInfoCell];
    [self.orderDetailTableView registerClass:[PMOrderSectionTitleCell class] forCellReuseIdentifier:orderTitleCell];
    self.orderDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.orderDetailTableView];
    
    UIView *footerView = [[UIView alloc] init];
    self.orderDetailTableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 2;
    }
    else if (section == 1)
    {
        return (self.orderProductsArr.count > 5 ? 5 : self.orderProductsArr.count) + 2;
    }
    else if (section == 2)
    {
        return 4;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            PMOrderStateCell *cell = [tableView dequeueReusableCellWithIdentifier:orderStateCell];
            [cell createUI];
            NSString *showString = @"";
            if ([self.orderStatus isEqualToString:@"待付款"])
            {
                showString = @"等待买家付款";
            }
            else if ([self.orderStatus isEqualToString:@"待发货"])
            {
                showString = @"商家备货中";
            }
            else if ([self.orderStatus isEqualToString:@"待收货"] || [self.orderStatus isEqualToString:@"已发货"])
            {
                showString = @"等待买家收货";
            }
            else if ([self.orderStatus isEqualToString:@"待评论"] || [self.orderStatus isEqualToString:@"已收货"])
            {
                if (self.isHasDiscuss)
                {
                    showString = @"交易完成";
                }
                else
                {
                    showString = @"交易完成";
                }
                
            }
            else
            {
                showString = @"已取消";
            }
            
            [cell setCellData:showString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else
        {
//            YBAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:ybAddressCell];
            PMOrderAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:ybAddressCell];
            [cell createUI];
            if ([[self.orderInfoDic objectForKey:@"addressMobile"] isKindOfClass:[NSString class]] && ![[self.orderInfoDic objectForKey:@"addressMobile"] isEqualToString:@""]) {
                NSDictionary *addressDic = @{@"name":[self.orderInfoDic objectNullForKey:@"addressName"],@"mobile":[self.orderInfoDic objectNullForKey:@"addressMobile"],@"areas":[self.orderInfoDic objectNullForKey:@"areaName"],@"address":[self.orderInfoDic objectNullForKey:@"addressAddr"]};
                [cell setCellData:addressDic];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == (self.orderProductsArr.count > 5 ? 5 : self.orderProductsArr.count) + 1)
        {
            OrderTotalPriceCell *cell = [tableView dequeueReusableCellWithIdentifier:orderTotalPriceCell];
            [cell createUI];
            [cell setCellData:[[self.orderInfoDic objectForKey:@"totalPrice"] doubleValue]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else if (indexPath.row == 0)
        {
            PMOrderSectionTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:orderTitleCell];
            [cell createUI];
            cell.titleCellDelegate = self;
            [cell setCellData:@"goods" TitleString:@"商品信息" IsMoreBtnShow:self.orderProductsArr.count > 5 ? YES : NO];
            cell.titleLB.hidden = NO;
            cell.sectionIV.hidden = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else
        {
            OrderDetailProductCell *cell = [tableView dequeueReusableCellWithIdentifier:orderProductCell];
            [cell createUI];
            [cell setCellData:self.orderProductsArr[indexPath.row - 1]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
    else if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
            PMOrderSectionTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:orderTitleCell];
            if ([self.orderInfoDic objectNullForKey:@"receipt"] && [[self.orderInfoDic objectNullForKey:@"receipt"] isKindOfClass:[NSDictionary class]])
            {
                cell.sectionIV.hidden = NO;
                cell.titleLB.hidden = NO;
                cell.moreBtn.hidden = NO;
                
                [cell setCellData:@"invoice" TitleString:@"发票信息" IsMoreBtnShow:NO];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            else
            {
                cell.sectionIV.hidden = YES;
                cell.titleLB.hidden = YES;
                cell.moreBtn.hidden = YES;
            }
            
            [cell createUI];
            
            return cell;
        }
        else
        {
            PMBillInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:billInfoCell];
            [cell createUI];
            if ([self.orderInfoDic objectNullForKey:@"receipt"] && [[self.orderInfoDic objectNullForKey:@"receipt"] isKindOfClass:[NSDictionary class]]) {
                cell.titleLB.hidden = NO;
                cell.contentLB.hidden = NO;
                switch (indexPath.row)
                {
                    case 1:
                        [cell setCellData:@"发票抬头" Content:[[self.orderInfoDic objectForKey:@"receipt"] objectForKey:@"receiptTitle"]];
                        break;
                    case 2:
                        [cell setCellData:@"发票类型" Content:[[[self.orderInfoDic objectForKey:@"receipt"] objectForKey:@"receiptType"] integerValue] == -1 ? @"个人发票" : @"单位发票"];
                        break;
                    case 3:
                         [cell setCellData:@"发票内容" Content:[[[self.orderInfoDic objectForKey:@"receipt"] objectForKey:@"receiptContent"] integerValue] == 1 ? @"办公用品" : @"商品明细"];
                        break;
                        
                    default:
                        break;
                }
                
            }
            else
            {
                cell.titleLB.hidden = YES;
                cell.contentLB.hidden = YES;
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
    }
    else
    {
        PMOrderCodeTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:orderCodeTimeCell];
        [cell createUI];
        [cell setcellData:[self.orderInfoDic objectForKey:@"orderCode"] Time:[self.orderInfoDic objectForKey:@"createDt"]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1)
    {
        NSString *detailAddressString = [NSString stringWithFormat:@"%@ %@",[self.orderInfoDic objectForKey:@"areaName"],[self.orderInfoDic objectForKey:@"addressAddr"]];
        CGSize detailAddressSize = [detailAddressString boundingRectWithSize:CGSizeMake(WIDTH - 65, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
        return 50 + detailAddressSize.height;
    }
    else if (indexPath.section == 1 && indexPath.row == 0)
    {
        return 40;
    }
    else if (indexPath.section == 1 && indexPath.row != (self.orderProductsArr.count > 5 ? 5 : self.orderProductsArr.count) + 1)
    {
        return 20 + WidthRate(180);
    }
    else if (indexPath.section == 1 && indexPath.row == (self.orderProductsArr.count > 5 ? 5 : self.orderProductsArr.count) + 1)
    {
        return 40;
    }
    else if (indexPath.section == 2 && indexPath.row != 0)
    {
        if ([self.orderInfoDic objectNullForKey:@"receipt"] && [[self.orderInfoDic objectNullForKey:@"receipt"] isKindOfClass:[NSDictionary class]])
        {
            return 30;
        }
        else
        {
            return 0;
        }
    }
    else if (indexPath.section == 2 && indexPath.row == 0)
    {
        if ([self.orderInfoDic objectNullForKey:@"receipt"] && [[self.orderInfoDic objectNullForKey:@"receipt"] isKindOfClass:[NSDictionary class]])
        {
            return 40;
        }
        else
        {
            return 0;
        }
    }
    else if (indexPath.section == 3)
    {
        return 70;
    }
    
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    else
    {
        return 10;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = RGBA(241, 241, 241, 1);
    return sectionHeaderView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 10;//设置你footer高度
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark - 底部View
- (void)buildBottomView
{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50, WIDTH , 50)];
    bottomView.backgroundColor = RGBA(243, 243, 243, 1);
    [self.view addSubview:bottomView];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(230, 230, 230, 1).CGColor;
    line.frame = CGRectMake(0, 0, WIDTH, 0.5);
    [bottomView.layer addSublayer:line];
    
    if ([self.orderStatus isEqualToString:@"待付款"])
    {
        UIButton *goToPayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        goToPayBtn.frame = CGRectMake(WIDTH - 100, 10, 80, 30);
        [goToPayBtn setTitle:@"去付款" forState:UIControlStateNormal];
        goToPayBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [goToPayBtn setBackgroundColor:RGBA(215, 69, 82, 1)];
        [goToPayBtn addTarget:self action:@selector(goToPayOrder:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:goToPayBtn];
        
        UIButton *cancelOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelOrderBtn.frame = CGRectMake(WIDTH - 100 - 100, 10, 80, 30);
        [cancelOrderBtn setTitle:@"取消订单" forState:UIControlStateNormal];
        cancelOrderBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [cancelOrderBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cancelOrderBtn addTarget:self action:@selector(cancelOrder:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:cancelOrderBtn];
    }
    else if ([self.orderStatus isEqualToString:@"待发货"])
    {
        UIButton *waitSendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        waitSendBtn.frame = CGRectMake(WIDTH - 120, 10, 100, 30);
        [waitSendBtn setTitle:@"商家备货中" forState:UIControlStateNormal];
        waitSendBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [waitSendBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [bottomView addSubview:waitSendBtn];
    }
    else if ([self.orderStatus isEqualToString:@"待收货"] || [self.orderStatus isEqualToString:@"已发货"])
    {
        UIButton *goToPayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        goToPayBtn.frame = CGRectMake(WIDTH - 100, 10, 80, 30);
        [goToPayBtn setTitle:@"查物流" forState:UIControlStateNormal];
        goToPayBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [goToPayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        goToPayBtn.tag = 1004;
        [goToPayBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:goToPayBtn];
        
        UIButton *cancelOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelOrderBtn.frame = CGRectMake(WIDTH - 100 - 100, 10, 80, 30);
        [cancelOrderBtn setTitle:@"确认收货" forState:UIControlStateNormal];
        cancelOrderBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [cancelOrderBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cancelOrderBtn.tag = 1005;
        [cancelOrderBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:cancelOrderBtn];
    }
    else if ([self.orderStatus isEqualToString:@"待评论"] || [self.orderStatus isEqualToString:@"已收货"])
    {
        if (self.isHasDiscuss)
        {
            UIButton *lookDiscussBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            lookDiscussBtn.frame = CGRectMake(WIDTH - 100, 10, 80, 30);
            [lookDiscussBtn setTitle:@"查看评价" forState:UIControlStateNormal];
            lookDiscussBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
            lookDiscussBtn.tag = 1007;
            [lookDiscussBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [lookDiscussBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [bottomView addSubview:lookDiscussBtn];
        }
        else
        {
            UIButton *cancelOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            cancelOrderBtn.frame = CGRectMake(WIDTH - 100, 10, 80, 30);
            [cancelOrderBtn setTitle:@"评价" forState:UIControlStateNormal];
            cancelOrderBtn.tag = 1006;
            cancelOrderBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
            [cancelOrderBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [cancelOrderBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [bottomView addSubview:cancelOrderBtn];
        }
        
    }
    else
    {
        UIButton *hasCancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        hasCancelBtn.frame = CGRectMake(WIDTH - 100, 10, 80, 30);
        [hasCancelBtn setTitle:@"已取消" forState:UIControlStateNormal];
        hasCancelBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [hasCancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [bottomView addSubview:hasCancelBtn];
    }
    
}

- (void)goToPayOrder:(UIButton *)sender
{
    [self getParamsWithOrderArray:self.orderProductsArr];
}

- (void)cancelOrder:(UIButton *)sender
{
    [self cancelTrade:self.productArr];
}

#pragma mark - 付款需要使用的参数
- (void)getParamsWithOrderArray:(NSArray *)array
{
    NSMutableString *products = [NSMutableString string];
    for (NSInteger i = 0; i < array.count; i++)
    {
        NSDictionary *productDic = array[i];
        NSString *productId = [[productDic objectNullForKey:@"product"] objectForKey:@"id"];
        NSString *aSpecValId = [productDic objectNullForKey:@"aSpecValId"];
        NSString *bSpecValId = [productDic objectNullForKey:@"bSpecValId"];
        NSString *cSpecValId = [productDic objectNullForKey:@"cSpecValId"];
        NSString *dSpecValId = [productDic objectNullForKey:@"dSpecValId"];
        NSString *eSpecValId = [productDic objectNullForKey:@"eSpecValId"];
        NSString *buyNum = [productDic objectNullForKey:@"productCount"];
        if (i == 0)
        {
            [products appendString:@"[{"];
        }
        else
        {
            [products appendString:@",{"];
        }
        NSString *productString = [NSString stringWithFormat:@"\"productId\":\"%@\",\"aSpecValId\":\"%@\",\"bSpecValId\":\"%@\",\"cSpecValId\":\"%@\",\"dSpecValId\":\"%@\",\"eSpecValId\":\"%@\",\"buyNum\":\"%@\"",productId,aSpecValId,bSpecValId,cSpecValId,dSpecValId,eSpecValId,buyNum];
        [products appendString:productString];
        
        if (i == array.count - 1)
        {
            [products appendString:@"}]"];
        }
        else
        {
            [products appendString:@"}"];
        }
    }
    NSDictionary *parameter = @{@"products":products};
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckStockTotal WithParameters:parameter callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            if ([[dic objectNullForKey:@"messageCode"] integerValue] == 0)
            {
                NSString *orderCode = [self.orderInfoDic objectForKey:@"orderCode"];
                
                NSDictionary *param = @{@"orderCode":orderCode,PMSID};
                
                [[PMNetworking defaultNetworking] request:PMRequestStateGetOrderByOrderCode WithParameters:param callBackBlock:^(NSDictionary *dicts) {
                    
                    NSMutableArray *newProductArray = [NSMutableArray array];
                    
                    YBSureOrderViewController *vc = [[YBSureOrderViewController alloc] init];
                    
                    
                    vc.orderCode = orderCode;
                    NSDictionary *dataDic = [dicts objectNullForKey:@"data"];
                    NSString *actuallyPay = [dataDic objectNullForKey:@"actuallyPay"];
                    vc.actuallyPay = actuallyPay;
                    NSArray *oldProductArray = [dataDic objectNullForKey:@"orderProductMap"];
                    for (NSDictionary *oldProductDic in oldProductArray)
                    {
                        NSDictionary *newProductDic = @{@"product":oldProductDic};
                        [newProductArray addObject:newProductDic];
                    }
                    
                    
                    NSDictionary *oldAddressDic = [dataDic objectNullForKey:@"receiveMap"];
                    NSDictionary *newAddressDic = @{@"name":[oldAddressDic objectNullForKey:@"receiveName"],@"mobile":[oldAddressDic objectNullForKey:@"receiveMobile"],@"areas":[oldAddressDic objectNullForKey:@"receiveArea"],@"address":[oldAddressDic objectNullForKey:@"receiveAddressDetail"],@"defaultAddressId":[oldAddressDic objectNullForKey:@"receiveAreaId"]};
                    
                    
                    NSDictionary *billDic = [dataDic objectNullForKey:@"billMap"];
                    [YBBillInfos shareInstance].billNum = @"null";
                    [YBBillInfos shareInstance].billContent = 0;
                    [YBBillInfos shareInstance].billType = 0;
                    
                    if (!isNull([billDic objectNullForKey:@"billTitle"]))
                    {
                        [YBBillInfos shareInstance].billNum = [billDic objectNullForKey:@"billTitle"];
                        [YBBillInfos shareInstance].billType = [[billDic objectNullForKey:@"billType"] integerValue];
                        [YBBillInfos shareInstance].billContent = [[billDic objectNullForKey:@"billContent"] integerValue];
                    }
                    
                    vc.productsArray = newProductArray;
                    vc.addressDic = newAddressDic;
                    vc.actuallyPay = [dataDic objectNullForKey:@"actuallyPay"];
                    vc.totalPrice = [dataDic objectNullForKey:@"orderMoney"];
                    vc.totalNum = [dataDic objectNullForKey:@"totalNum"];
                    vc.carriagePrice = [[dataDic objectNullForKey:@"logisticsPrice"] doubleValue];
                    vc.isTradeBack = YES;
                    
                    
                    [vc.tableView reloadData];
                    
                    [self.navigationController pushViewController:vc animated:YES];
                    
                    self.view.userInteractionEnabled = YES;
                    
                }showIndicator:YES];
            }
            else
            {
                NSMutableString *noEnoughString = [NSMutableString string];
                NSArray *dataArr = [dic objectNullForKey:@"data"];
                for (NSDictionary *dataDic in dataArr)
                {
                    NSString *productName = [dataDic objectNullForKey:@"productName"];
                    [noEnoughString appendString:productName];
                }
                
                NSString *alertMessage = [NSString stringWithFormat:@"%@\n库存不足",noEnoughString];
                self.view.userInteractionEnabled = YES;
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"微购提示" message:alertMessage delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else
        {
            self.view.userInteractionEnabled = YES;
            showRequestFailAlertView;
        }
        
        
    } showIndicator:NO];
    
    /**
     参数：
     PM_SID  	  Y	   String		Token
     orderCode	  Y	   String		订单编号（根据上一步提交订单取得）
     totalPrice	  Y	   doubel		总价（根据订单信息获取）
     totalNum	  Y	   int		    商品笔数（根据订单信息获取）
     pro	      Y	   String		商品名称（多个商品用","逗号隔开）
     */
    
}

#pragma mark - 取消订单
- (void)cancelTrade:(NSArray *)orderArr
{
    NSString *orderCode = [self.orderInfoDic objectForKey:@"orderCode"];
    NSString *orderId = [self.orderInfoDic objectForKey:@"orderId"];
    
    [[PMNetworking defaultNetworking] request:PMRequestStateCheckOrderIsPay WithParameters:@{PMSID,@"orderId":orderId} callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            [[PMNetworking defaultNetworking] request:PMRequestStateUpdateOrderStatus WithParameters:@{@"status":@"9999",@"orderCode":orderCode} callBackBlock:^(NSDictionary *dic) {
                if (intSuccess == 1)
                {
                    [[PMToastHint defaultToastWithRight:YES] showHintToView:self.view ByToast:@"订单取消成功"];
                    if ([self.detailViewDelegate respondsToSelector:@selector(detailFinishCancelOrder:)])
                    {
                        [self.detailViewDelegate detailFinishCancelOrder:orderArr];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                else
                {
                    self.view.userInteractionEnabled = YES;
                    showRequestFailAlertView;
                }
                
            } showIndicator:YES];
        }
        else
        {
            self.view.userInteractionEnabled = YES;
            [[PMToastHint defaultToastWithRight:NO] showHintToView:self.view ByToast:@"该订单已经支付"];
        }
        
    } showIndicator:NO];
    
}

- (void)btnClick:(UIButton *)sender
{
    switch (sender.tag) {
        case 1004:
        {
            ComplaintsViewController *complaintsVC = [[ComplaintsViewController alloc] init];
            complaintsVC.productsArray = self.productArr; // 传当前section展示的数据
            complaintsVC.state = PMOrderStateLogistics;
            [self.navigationController pushViewControllerWithNavigationControllerTransition:complaintsVC];
            self.view.userInteractionEnabled = YES;
        }
            break;
        case 1005:
        {
            ComplaintsViewController *complaintsVC = [[ComplaintsViewController alloc] init];
            complaintsVC.productsArray = self.productArr; // 传当前section展示的数据
            complaintsVC.state = PMOrderStateSureOrder;
            complaintsVC.complainDelegate = self;
            complaintsVC.isFromDetail = YES;
            [self.navigationController pushViewController:complaintsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            
            break;
        case 1006:
        {
            YBComplaintsViewController *commentsVC = [[YBComplaintsViewController alloc] init];
            commentsVC.productArr = self.productArr;
            commentsVC.complainDelegate = self;
            commentsVC.orderCode = self.orderCode;
            commentsVC.isFromDetail = YES;
            [self.navigationController pushViewController:commentsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            
            break;
        case 1007:
        {
            YBComplaintsViewController *commentsVC = [[YBComplaintsViewController alloc] init];
            commentsVC.productArr = self.productArr;
            commentsVC.complainDelegate = self;
            commentsVC.isHasDiscuss = YES;
            commentsVC.isFromDetail = YES;
            commentsVC.orderCode = self.orderCode;
            [self.navigationController pushViewController:commentsVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
            break;
            
        default:
            break;
    }
}

- (void)finishSureOrder:(NSArray *)productArr
{
    if ([self.detailViewDelegate respondsToSelector:@selector(detailFinishSureOrder:)])
    {
        [self.detailViewDelegate detailFinishSureOrder:productArr];
    }
}

- (void)finishComplain:(NSArray *)productArr
{
    if ([self.detailViewDelegate respondsToSelector:@selector(detailFinishComments:)])
    {
        [self.detailViewDelegate detailFinishComments:productArr];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.section == 1 && indexPath.row != (self.orderProductsArr.count > 5 ? 5 : self.orderProductsArr.count) + 1) && (indexPath.section == 1 && indexPath.row != 0))
    {
        NSDictionary *dict = self.productArr[indexPath.row - 1];
        NSString *productId = [dict objectNullForKey:@"productId"];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        [parameter setValue:productId forKey:@"Id"];
        [parameter setValue:[PMUserInfos shareUserInfo].userId forKey:@"userId"];
        PMMyGoodsViewController *goodVC = [[PMMyGoodsViewController alloc] init];
        goodVC.productId = productId;
        [self.navigationController pushViewControllerWithNavigationControllerTransition:goodVC];
    }
}

- (void)goToMore
{
    OrderListViewController *orderVC = [[OrderListViewController alloc] init];
    orderVC.productArr = self.productArr;
    orderVC.isTradeBack = YES;
    orderVC.isFromOrderDetail = YES;
    [self.navigationController pushViewController:orderVC animated:YES];
}

@end
