//
//  MyWalletDetailViewController.m
//  changeViewController
//
//  Created by EapinZhang on 15/4/2.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "MyWalletDetailViewController.h"
#import "PMMyPhoneInfo.h"
#import "YBDatePickerView.h"
#import "YBDetailCell.h"
#import "JHRefresh.h"
#import "PMDistrubutionTableViewCell.h"
#import "PMNetworking.h"
#import "WithdrawalMoneyViewController.h"



@interface MyWalletDetailViewController () <UITableViewDataSource,UITableViewDelegate,YBDatePickerViewDelegate>

@property (strong,nonatomic) UITableView *detailTableView;
@property (strong,nonatomic) UILabel *yLB;
@property (strong,nonatomic) UILabel *monLB;
@property (strong,nonatomic) UILabel *oMoneyLB;
@property (strong,nonatomic) UILabel *iMoneyLB;
@property (strong,nonatomic) UIDatePicker *datePicker;
@property (strong,nonatomic) UIView *pickView;
@property (strong,nonatomic) UIView *shadowView;
@property (assign,nonatomic) NSInteger year;
@property (assign,nonatomic) NSInteger month;
@property (strong,nonatomic) NSMutableArray *detailArray; // 收支明细
@property (strong,nonatomic) NSMutableArray *dateArray; // 消费日期
@property (strong,nonatomic) UISegmentedControl *seg;
@property (strong,nonatomic) UILabel *timeLabel;
@property (strong,nonatomic) UILabel *orderMoneyLab;
@property (strong,nonatomic) UILabel *stateLabel;
@property (strong,nonatomic) NSMutableArray *withdrawalDetailArray;
@property (strong,nonatomic) UILabel *tipLabel;
@property (assign,nonatomic) BOOL isGetCash;

@end

@implementation MyWalletDetailViewController

static NSString *_getCashCellIdentifier = @"getCashCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isGetCash = NO;
    self.withdrawalDetailArray = [NSMutableArray array];
    self.detailArray = [NSMutableArray array];
    self.dateArray = [NSMutableArray array];
    self.title = @"钱包详情";
    if ([self.navigationController.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
        NSArray *list=self.navigationController.navigationBar.subviews;
        for (id obj in list) {
            if ([obj isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView=(UIImageView *)obj;
                NSArray *list2=imageView.subviews;
                for (id obj2 in list2) {
                    if ([obj2 isKindOfClass:[UIImageView class]]) {
                        UIImageView *imageView2=(UIImageView *)obj2;
                        imageView2.hidden=YES;
                    }
                }
            }
        }
    }
    
    UIBarButtonItem *rightBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"提现" style:UIBarButtonItemStyleDone target:self action:@selector(getCash)];
    self.navigationItem.rightBarButtonItem = rightBarBtn;
    [self initFake];
    [self createUI];
    [self createYMSelected];
    self.seg.selectedSegmentIndex = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.isGetCash = NO;
    [self.detailTableView reloadData];
}

#pragma mark - 创建表格和表头
- (void)createUI
{
   
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = RGBA(236, 236, 236, 1);
    self.detailTableView = tableView;
    [tableView registerClass:[YBDetailCell class] forCellReuseIdentifier:@"detailCell"];
    [tableView registerClass:[PMDistrubutionTableViewCell class] forCellReuseIdentifier:_getCashCellIdentifier];
    [self.view addSubview:tableView];
    
    //通过月份查看收支明细
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    headView.backgroundColor = RGBA(236, 236, 236, 1);
    tableView.tableHeaderView = headView;
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 124)];
    footView.backgroundColor = [UIColor whiteColor];
    tableView.tableFooterView = footView;
    tableView.scrollEnabled = NO;
    
    UIImageView *noDataIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, footView.frame.size.height / 2)];
    noDataIV.image = [UIImage imageNamed:@"noData"];
    noDataIV.contentMode = UIViewContentModeScaleAspectFit;
    [footView addSubview:noDataIV];
    
    UILabel *noDataLB = [[UILabel alloc] initWithFrame:CGRectMake(0, footView.frame.size.height / 2, WIDTH, footView.frame.size.height / 2)];
    noDataLB.text = @"这个月没有收支详情哦";
    noDataLB.font = [UIFont systemFontOfSize:18.0f];
    noDataLB.textColor = HDVGFontColor;
    noDataLB.textAlignment = NSTextAlignmentCenter;
    [footView addSubview:noDataLB];
    
    
   
    
    //获取当前时间
    NSDate *nowDate = [NSDate new];
    NSCalendar *calendar =  [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:nowDate];
    NSInteger year = [dateComponent year];
    NSInteger month = [dateComponent month];
    self.year = year;
    self.month = month;

    //年Label
    UILabel *yearLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), 5, WIDTH / 6, 15)];
    yearLB.text = [NSString stringWithFormat:@"%ld年",(long)year];
    yearLB.font = [UIFont systemFontOfSize:14.0f];
    yearLB.textColor = HDVGFontColor;
    self.yLB = yearLB;
    [headView addSubview:yearLB];

    UILabel *monthLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), 20, WidthRate(80), 35)];
    NSString *monthString = @"";
    if (month < 10)
    {
        monthString = [NSString stringWithFormat:@"0%ld",(long)month];
    }
    else
    {
        monthString = [NSString stringWithFormat:@"%ld",(long)month];
    }
    monthLB.text = monthString;
    monthLB.font = [UIFont systemFontOfSize:23.0f];
    monthLB.textColor = HDVGFontColor;
    self.monLB = monthLB;
    [headView addSubview:monthLB];
    
    UILabel *monthTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(170), 25, WidthRate(30), 35)];
    monthTitleLB.text = @"月";
    monthTitleLB.font = [UIFont systemFontOfSize:13.0f];
    monthTitleLB.textColor = HDVGFontColor;
    [headView addSubview:monthTitleLB];

    UIImageView *arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(140), 38, 10, 10)];
    arrowIV.image = [UIImage imageNamed:@"下拉"];
    arrowIV.contentMode = UIViewContentModeScaleAspectFit;
    [headView addSubview:arrowIV];

    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(WIDTH / 3 - WidthRate(80), 10, 1, 40);
    line.backgroundColor = [UIColor whiteColor].CGColor;
    [headView.layer addSublayer:line];

    //选择月份
    UIButton *monthButton = [UIButton buttonWithType:UIButtonTypeCustom];
    monthButton.frame = CGRectMake(0, 0, WIDTH / 3 - WidthRate(80), headView.bounds.size.height);
    monthButton.tag = 1;
    [monthButton addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:monthButton];
    
    //支出标题Label
    UILabel *outLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(80) + yearLB.frame.origin.x + yearLB.bounds.size.width + WidthRate(40), 5, WIDTH / 6, 15)];
    outLB.text = @"支出(元)";
    outLB.font = [UIFont systemFontOfSize:14.0f];
    outLB.textColor = HDVGFontColor;
    [headView addSubview:outLB];

    UILabel *outMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20) + yearLB.frame.origin.x + yearLB.bounds.size.width + WidthRate(105), 20, WIDTH / 3 + WidthRate(40), 40)];
    outMoneyLB.text = @"10.00";
    outMoneyLB.font = [UIFont systemFontOfSize:20.0f];
    outMoneyLB.textColor = HDVGFontColor;
    self.oMoneyLB = outMoneyLB;
    [headView addSubview:outMoneyLB];

    //收入标题Label
    UILabel *inLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20) + outLB.frame.origin.x + outLB.bounds.size.width + WidthRate(150), 5, WIDTH / 6, 15)];
    inLB.text = @"收入(元)";
    inLB.font = [UIFont systemFontOfSize:14.0f];
    inLB.textColor = HDVGFontColor;
    [headView addSubview:inLB];

    UILabel *inMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20) + outLB.frame.origin.x + outLB.bounds.size.width + WidthRate(150), 20, WIDTH / 3 + WidthRate(40), 40)];
    inMoneyLB.text = @"110.00";
    inMoneyLB.font = [UIFont systemFontOfSize:20.0f];
    inMoneyLB.textColor = HDVGFontColor;
    self.iMoneyLB = inMoneyLB;
    [headView addSubview:inMoneyLB];
    
    //阴影
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 300)];
    shadowView.backgroundColor = [UIColor lightGrayColor];
    shadowView.alpha = 0.5;
    shadowView.hidden = YES;
    self.shadowView = shadowView;
    [self.view addSubview:shadowView];

    
    
    
    
    
    //下拉刷新
    __weak MyWalletDetailViewController *weakSelf = self;
    
    [tableView addRefreshHeaderViewWithAniViewClass:[JHRefreshCommonAniView class] beginRefresh:^{
        
        //请求数据
        //添加行
        
        //事情做完了别忘了结束刷新动画~~~
        [weakSelf.detailTableView headerEndRefreshingWithResult:JHRefreshResultSuccess];
    }];
}

#pragma mark - 年月选择器
- (void)createYMSelected
{
    YBDatePickerView *datePicker = [[YBDatePickerView alloc] init];
    datePicker.myDelegate = self;
    datePicker.nowYear = [NSString stringWithFormat:@"%ld",(long)self.year];
    datePicker.nowMonth = [NSString stringWithFormat:@"%ld",(long)self.month];
    [datePicker createUIWithFrame:CGRectMake(0, HEIGHT, WIDTH, 300)];
    self.pickView = datePicker;
    [self.view addSubview:datePicker];
    
}

#pragma mark - 定义假数据
- (void)initFake
{
    self.detailArray = [NSMutableArray array];
    NSDictionary *dic1 = @{@"date":@"2015-04-01 17:30:30",@"content":@"iPhone6 16G",@"money":@"- 6000.00"};
    NSDictionary *dic2 = @{@"date":@"2015-04-01 17:30:30",@"content":@"iPhone6 64G",@"money":@"- 7000.00"};
    NSDictionary *dic3 = @{@"date":@"2015-04-02 17:30:30",@"content":@"iPhone6 128G",@"money":@"- 8000.00"};
    NSDictionary *dic4 = @{@"date":@"2015-04-02 17:30:30",@"content":@"iPhone6 256G",@"money":@"- 9000.00"};
    
    self.detailArray = [@[dic1,dic2,dic3,dic4] mutableCopy];
    
    self.dateArray = [NSMutableArray array];
    
    //[self getData];
    
}

//- (void)getData
//{
//    BOOL isSame = NO;
//    for (NSInteger i = 0; i < self.detailArray.count; i++)
//    {
//        isSame = NO;
//        NSDictionary *detailDic = self.detailArray[i];
//        for (NSInteger j = 0; j < self.dateArray.count; j++)
//        {
//            NSMutableArray *dateArr = self.dateArray[j];
//            NSDictionary *dateDic = dateArr[0];
//            if ([[detailDic objectNullForKey:@"date"] isEqualToString:[dateDic objectNullForKey:@"date"]])
//            {
//                isSame = YES;
//                [dateArr addObject:detailDic];
//                break;
//            }
//        }
//        
//        if (!isSame)
//        {
//            NSMutableArray *newDateArr = [NSMutableArray array];
//            [newDateArr addObject:detailDic];
//            [self.dateArray addObject:newDateArr];
//        }
//    }
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.isGetCash)
    {
        YBDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell"];
        
        [cell createUI];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.detailDic = self.detailArray[indexPath.row];
        return cell;
    }
    else
    {
        PMDistrubutionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_getCashCellIdentifier];
        [cell setWithdrawalDetailDataUI];
        NSDictionary *dict = self.withdrawalDetailArray[indexPath.row];
        [cell setTime:[dict objectNullForKey:@"createDt"] setWithdrawalMoney:[[dict objectNullForKey:@"money"] doubleValue] setState:[dict objectNullForKey:@"statusStr"]];
        return cell;
    }
    
    
    
    
}

#pragma mark - 收支明细和体现明细切换
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *typeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(150))];
    UISegmentedControl *segControl = [[UISegmentedControl alloc] initWithItems:@[@"收支明细",@"提现明细"]];
    typeView.backgroundColor = [UIColor whiteColor];
    segControl.frame = CGRectMake(0, 0, WIDTH, HeightRate(100));
    // 选中按钮时标题颜色
    [segControl setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HDVGRed, NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    [segControl setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    // 底部红色指示条
    CGRect rect = self.isGetCash ? CGRectMake(WidthRate(40) + WIDTH / 2,HeightRate(80),WIDTH / 2 - WidthRate(60),1):CGRectMake(WidthRate(40),HeightRate(80),WIDTH / 2 - WidthRate(60),1);
    self.tipLabel = [[UILabel alloc] initWithFrame:rect];
    self.tipLabel.backgroundColor = HDVGRed;
    [segControl addSubview:self.tipLabel];
    [segControl setTintColor:[UIColor clearColor]];
    segControl.segmentedControlStyle = UISegmentedControlStyleBordered;
    [segControl addTarget:self action:@selector(refreshItem:) forControlEvents:UIControlEventValueChanged];
    self.seg = segControl;
    [typeView addSubview:segControl];
    
    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(0, HeightRate(150) - 1, WIDTH, 1);
    line.backgroundColor = RGBA(236, 236, 236, 1).CGColor;
    [typeView.layer addSublayer:line];
    
    // 创建时间
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), HeightRate(100), WidthRate(230), HeightRate(50))];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.text = @"时间";
    self.timeLabel.textColor = RGBA(65, 65, 65, 1);
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.font = [UIFont systemFontOfSize:13.0f];
    [typeView addSubview:self.timeLabel];
    
     // 交易状态
    self.orderMoneyLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(240), HeightRate(100), WidthRate(230), HeightRate(50))];
    self.orderMoneyLab.backgroundColor = [UIColor clearColor];
    self.orderMoneyLab.text = self.isGetCash ? @"状态" : @"项目";
    self.orderMoneyLab.textColor = RGBA(65, 65, 65, 1);
    self.orderMoneyLab.textAlignment = NSTextAlignmentCenter;
    self.orderMoneyLab.font = [UIFont systemFontOfSize:13.0f];
    [typeView addSubview:self.orderMoneyLab];
    
   // 提款金额
    self.stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(550), HeightRate(100), WidthRate(200), HeightRate(50))];
    self.stateLabel.backgroundColor = [UIColor clearColor];
    self.stateLabel.text = @"金额(元)";
    self.stateLabel.textColor = RGBA(65, 65, 65, 1);
    self.stateLabel.textAlignment = NSTextAlignmentCenter;
    self.stateLabel.font = [UIFont systemFontOfSize:13.0f];
    [typeView addSubview:self.stateLabel];
    
    return typeView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(150);
}

#pragma mark - 点击事件
- (void)clickBtn:(UIButton *)btn
{
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT - 300, WIDTH, 300);
    }];
}

- (void)surePickerView:(NSString *)selectedYear And:(NSString *)selectedMonth
{
    self.yLB.text = selectedYear;
    self.monLB.text = selectedMonth;
    
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

- (void)cancelPickerView
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }]; 
}

#pragma mark - 提现
- (void)getCash
{
    WithdrawalMoneyViewController *withdrawalMoneyVC = [[WithdrawalMoneyViewController alloc] init];
    withdrawalMoneyVC.canMoneyStr = [NSString stringWithFormat:@"%.2lf",[self.iMoneyLB.text doubleValue] - [self.oMoneyLB.text doubleValue]];
    [self.navigationController pushViewController:withdrawalMoneyVC animated:YES];
}

#pragma mark - 切换明细
- (void)refreshItem:(UISegmentedControl *)seg
{
    if (seg.selectedSegmentIndex == 0)
    {
        [self initFake];
        self.isGetCash = NO;
        [self.detailTableView reloadData];
        
    }
    else
    {
        self.isGetCash = YES;
        [self initWithdrawalDetailDataList];
        
    }
}


#pragma mark - 钱包

#pragma mark - 代言商提现明细数据
- (void)initWithdrawalDetailDataList
{
    // 获取用户token
    PMUserInfos *userInfos = [PMUserInfos shareUserInfo];
    
    // 如果用户还没有登录 userInfos 为空
    if (userInfos.PM_SID == nil) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"你还没有登录，请先登录" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
    }
    else {
        
        //提现明细
        /*
         参数：
         PM_SID        Y    String      Token
         currentPage   N    int         当前页
         createDt      N    date        开始时间
         createDt2     N    date        结束时间
         */
        
        int currentPage = 1;
        
        NSDictionary *showMyOrderList = @{@"PM_SID":userInfos.PM_SID, @"currentPage":@(currentPage)};
        PMNetworking *netWorking = [PMNetworking defaultNetworking];
        
        [netWorking request:PMRequestStateShareShowGetMoney WithParameters:showMyOrderList callBackBlock:^(NSDictionary *dict) {
            [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                
                if ([[dict objectNullForKey:@"success"] integerValue] == 0) {
                    
                    // 还没有提现明细数据
                    UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), HeightRate(400), WIDTH - WidthRate(40), HeightRate(60))];
                    tipLabel.backgroundColor = [UIColor clearColor];
                    tipLabel.text = @"亲，你暂时还没有提现明细数据";
                    tipLabel.textColor = RGBA(65, 65, 65, 1);
                    tipLabel.textAlignment = NSTextAlignmentCenter;
                    tipLabel.font = [UIFont systemFontOfSize:20.0f];
                    [self.view addSubview:tipLabel];
                }
                else {
                    self.withdrawalDetailArray = [[[dict objectNullForKey:@"data"] objectNullForKey:@"beanList"] mutableCopy];
                    
                    for (NSDictionary *dict in self.withdrawalDetailArray) {
                        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                            
                        }];
                    }
                }
                [self.detailTableView reloadData];
            }];
        }showIndicator:NO];
    }
}


@end
