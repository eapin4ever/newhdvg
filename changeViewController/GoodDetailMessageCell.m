//
//  GoodDetailMessageCell.m
//  changeViewController
//
//  Created by pmit on 15/7/10.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "GoodDetailMessageCell.h"
#import "PMMyPhoneInfo.h"

@implementation GoodDetailMessageCell
{
    UILabel *nameLB;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!nameLB)
    {
        nameLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, WIDTH, 25)];
        nameLB.text = @"恒大微购商城提供服务保障";
        nameLB.textColor = [UIColor lightGrayColor];
        nameLB.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:nameLB];
        
        UIImageView *rightSureIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"goods_1"]];
        rightSureIV.frame = CGRectMake(15, 37.5, 20, 15);
        rightSureIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:rightSureIV];
        
        UILabel *rightSureLB = [[UILabel alloc] initWithFrame:CGRectMake(40, 35, WIDTH / 3 - 10 - 20, 20)];
        rightSureLB.text = @"正品保障";
        rightSureLB.font = [UIFont systemFontOfSize:14.0f];
        rightSureLB.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:rightSureLB];
        
        UIImageView *speedIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"goods_2"]];
        speedIV.frame = CGRectMake(rightSureLB.frame.origin.x + rightSureLB.frame.size.width, 37.5, 20, 15);
        speedIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:speedIV];
        
        UILabel *speedLB = [[UILabel alloc] initWithFrame:CGRectMake(speedIV.frame.origin.x + speedIV.frame.size.width + 5, 35, WIDTH / 3 - 10 - 20, 20)];
        speedLB.text = @"极速发货";
        speedLB.font = [UIFont systemFontOfSize:14.0f];
        speedLB.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:speedLB];
        
        UIImageView *sevenIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"goods_3"]];
        sevenIV.frame = CGRectMake(speedLB.frame.origin.x + speedLB.frame.size.width, 37.5, 20, 15);
        sevenIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:sevenIV];
        
        UILabel *sevenLB = [[UILabel alloc] initWithFrame:CGRectMake(sevenIV.frame.origin.x + sevenIV.frame.size.width + 5, 35, WIDTH / 3 - 10 - 20, 20)];
        sevenLB.text = @"7天退货";
        sevenLB.font = [UIFont systemFontOfSize:14.0f];
        sevenLB.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:sevenLB];
    }
}

@end
