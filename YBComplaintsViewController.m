//
//  YBComplaintsViewController.m
//  changeViewController
//
//  Created by ZhangEapin on 15/5/13.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBComplaintsViewController.h"
#import "PMMyPhoneInfo.h"
#import "MyTradeTableViewCell.h"
#import "PMNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface YBComplaintsViewController () <UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>

@property (strong,nonatomic) UITableView *commentsTableView;
@property (strong,nonatomic) NSMutableArray *levelArr;
@property (strong,nonatomic) NSMutableArray *remarkArr;
@property (strong,nonatomic) NSMutableArray *textViewArr;
@property (strong,nonatomic) NSMutableArray *startBtnArr;

@property (strong,nonatomic) UITextView *textView;
@property (strong,nonatomic) UILabel *tipLabel;

@end

@implementation YBComplaintsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"商品评价";
    self.view.backgroundColor = HDVGGray;
    
    self.textViewArr = [NSMutableArray array];
    self.startBtnArr = [NSMutableArray array];
    self.levelArr = [NSMutableArray arrayWithObjects:@"5",@"5",@"5",@"5",@"5",nil];
    
    for (NSInteger i = 0; i < self.productArr.count; i++)
    {
        UITextView *textView = [[UITextView alloc] init];
        [self.textViewArr addObject:textView];
        
        NSMutableArray *btnArr = [NSMutableArray array];
        for (NSInteger j = 0; j < 5; j++)
        {
            UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnArr addObject:startBtn];
        }
        [self.startBtnArr addObject:btnArr];
    }
    
    
    [self createUI];
    [self createFootView];
}

- (void)viewWillAppear:(BOOL)animated
{
    // 注册通知，监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardDidShowNotification
                                              object:nil];
    // 注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardDidHideNotification
                                              object:nil];
    [super viewWillAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createUI
{
    UITableView *commentTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStyleGrouped];
    commentTableView.delegate = self;
    commentTableView.dataSource = self;
    [commentTableView registerClass:[MyTradeTableViewCell class] forCellReuseIdentifier:@"tradeCell"];
    commentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.commentsTableView = commentTableView;
    
    [self.view addSubview:commentTableView];
}

- (void)createFootView
{
    // 初始化footerView
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    footerView.backgroundColor = [UIColor clearColor];
    // 初始化提交评论按钮----- 如何检查是否已经评价？？？？
    UIButton *commentsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    commentsButton.frame = CGRectMake(WidthRate(40), 10, WIDTH - WidthRate(40) * 2, 40);
    [commentsButton setTitle:@"提交评价" forState:UIControlStateNormal];
    [commentsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [commentsButton addTarget:self action:@selector(commentsForButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [commentsButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
    [commentsButton setBackgroundColor:ButtonBgColor];
    [commentsButton.layer setCornerRadius:6.0f];
    [footerView addSubview:commentsButton];
    
    self.commentsTableView.tableFooterView = footerView;
}

#pragma mark - 解决textView输入时键盘遮挡问题
// 监听事件
- (void)handleKeyboardDidShow:(NSNotification *)paramNotification
{
    // 获取键盘高度
    NSValue *keyboardRectAsObject = [[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    self.commentsTableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardRect.size.height, 0);
}

- (void)handleKeyboardDidHidden
{
    self.commentsTableView.contentInset = UIEdgeInsetsZero;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 隐藏键盘，实现UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    // 判断输入是否已经超出最大可输入长度
    if ([text isEqualToString:@""] && range.length > 0) {
        
        //删除字符肯定是安全的
        return YES;
    }
    else {
        if (self.textView.text.length - range.length + text.length > 100) {
            
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"超出最大可输入长度" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            //[alert show];
            
            return NO;
        }
        else {
            return YES;
        }
    }
    
    return YES;
}

// 改变输入字数多少，显示几分之几
- (void)textViewDidChange:(UITextView *)textView
{
    self.tipLabel.text = [NSString stringWithFormat:@"%lu/100", (unsigned long)self.textView.text.length];
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textView resignFirstResponder];
}

- (void)commentsForButtonClick:(UIButton *)sender
{
    NSMutableArray *productIdsArr = [NSMutableArray array];
    NSMutableArray *orderIdArr = [NSMutableArray array];
    for (NSDictionary *dic in self.productArr)
    {
        [productIdsArr addObject:[dic objectForKey:@"productId"]];
        [orderIdArr addObject:[dic objectForKey:@"orderId"]];
    }
    
    NSMutableArray *remarksArr = [NSMutableArray array];
    for (UITextView *textView in self.textViewArr)
    {
        NSLog(@"%@",textView);
        if ([textView.text isEqualToString:@""])
        {
            [remarksArr addObject:@"_"];
        }
        else
        {
            [remarksArr addObject:textView.text];
        }
    }
    
    NSString *prodcutIds = [productIdsArr componentsJoinedByString:@","];
    NSString *orderIds = [orderIdArr componentsJoinedByString:@","];
    NSString *remarks = [remarksArr componentsJoinedByString:@"@!@"];
    NSString *levels = [self.levelArr componentsJoinedByString:@","];
    
    NSDictionary *comments = @{PMSID,@"remarks":remarks,@"productIds":prodcutIds,@"orderIds":orderIds,@"levels":levels};
    
    [[PMNetworking defaultNetworking] request:PMRequestStateAddOrderDiscuss WithParameters:comments  callBackBlock:^(NSDictionary *dic) {
        if (intSuccess == 1)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提交商品评价成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            alertView.tag = 1;
            [alertView show];
        }
        else
        {
            showRequestFailAlertView;
        }
        
        
    } showIndicator:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.productArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0)
    {
        MyTradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tradeCell"];
        [cell setMyTradeOrderDataUI];
        
        NSDictionary *dict = self.productArr[indexPath.section];
        
        cell.turnBackBtn.hidden = YES;
        
        // 获取下来的数据
        //给一张默认图片，先使用默认图片，当图片加载完成后再替换
        [cell.iv sd_setImageWithURL:[dict objectNullForKey:@"productLogo"] placeholderImage:[UIImage imageNamed:@"productDefault"]];
        
        [cell setTitle:[dict objectForKey:@"productName"] setClssify:[dict objectNullForKey:@"aspecVal"] setNumber:[[dict objectForKey:@"productNum"] doubleValue]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myCell"];
        
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, (iPhone6_Plus ? 190 : 170))];
        footerView.backgroundColor = [UIColor clearColor];
        
        // 商品评论
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(30), HeightRate(190), HeightRate(48))];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.text = @"商品评价";
        titleLabel.textColor = RGBA(153, 153, 153, 1);
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [footerView addSubview:titleLabel];
        
        NSArray *btnArr = self.startBtnArr[indexPath.section];
        for (NSInteger i = 0; i < btnArr.count; i++)
        {
            UIButton *button = (UIButton *)btnArr[i];
            button.frame = CGRectMake(WidthRate(200) + WidthRate(57) * i, HeightRate(25), WidthRate(55), WidthRate(55));
            [button setImage:[UIImage imageNamed:@"star_pressed@2x.png"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"star@2x.png"] forState:UIControlStateSelected];
            button.tag =  indexPath.section * 100 + (i + 1);
            [button addTarget:self action:@selector(relevantButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [footerView addSubview:button];
        }
        
        // 分隔线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(0, titleLabel.frame.origin.y + HeightRate(68), WIDTH, HeightRate(1));
        layer.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
        [footerView.layer addSublayer:layer];
        
        UITextView *commentTextView = self.textViewArr[indexPath.section];
        commentTextView = [[UITextView alloc] initWithFrame:CGRectMake(WidthRate(30), titleLabel.frame.origin.y + HeightRate(70), WIDTH - WidthRate(60), HeightRate(200))];
        commentTextView.delegate = self;
        commentTextView.textColor = RGBA(79, 79, 79, 1);
        commentTextView.font = [UIFont systemFontOfSize:14.0f];
        commentTextView.layer.cornerRadius = 0.0f;   // 设置圆角值
        commentTextView.layer.masksToBounds = YES;
        commentTextView.layer.borderWidth = 0.5f;    // 设置边框大小
        [commentTextView.layer setBorderColor:[[UIColor clearColor] CGColor]];
        commentTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        commentTextView.returnKeyType = UIReturnKeyDone;
        commentTextView.keyboardType = UIKeyboardTypeDefault;
        commentTextView.scrollEnabled = YES;
        self.textView = commentTextView;
        [footerView addSubview:commentTextView];
        [self.textViewArr replaceObjectAtIndex:indexPath.section withObject:commentTextView];
        
        // 分隔线
        CALayer *layer2 = [[CALayer alloc] init];
        layer2.frame = CGRectMake(0, commentTextView.frame.origin.y + HeightRate(200), WIDTH, HeightRate(1));
        layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
        [footerView.layer addSublayer:layer2];
        
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, commentTextView.frame.origin.y + WidthRate(200) - (iPhone4s ? HeightRate(35) : 0), WIDTH, footerView.frame.size.height - HeightRate(280))];
        grayView.backgroundColor = HDVGGray;
        [footerView addSubview:grayView];
        
        // 提示输入字数
        UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(180), commentTextView.frame.origin.y + WidthRate(200), WidthRate(140), HeightRate(48))];
        tipLabel.backgroundColor = [UIColor clearColor];
        tipLabel.text = [NSString stringWithFormat:@"%lu/100", (unsigned long)commentTextView.text.length];
        tipLabel.textColor = [UIColor grayColor];
        tipLabel.textAlignment = NSTextAlignmentRight;
        tipLabel.font = [UIFont systemFontOfSize:14.0f];
        self.tipLabel = tipLabel;
        [footerView addSubview:tipLabel];
        
        [cell.contentView addSubview:footerView];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 != 0)
    {
        return (iPhone6_Plus ? 190 : 170);
    }
    else
    {
        return 100;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(70);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(60))];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(0, HeightRate(69), WIDTH, HeightRate(1));
    layer.backgroundColor = RGBA(220, 220, 220, 1).CGColor;
    [headerView.layer addSublayer:layer];
    
    //NSDictionary *dict = self.evaluationArray[section];
    
    //订单号
    UILabel *orderNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), HeightRate(10), WidthRate(420), HeightRate(60))];
    orderNumLabel.backgroundColor = [UIColor clearColor];
    orderNumLabel.text = [self.productArr[section] objectForKey:@"orderCode"];
    orderNumLabel.textColor = RGBA(117, 116, 121, 1);
    orderNumLabel.font = [UIFont systemFontOfSize:HeightRate(24)];
    [headerView addSubview:orderNumLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (void)relevantButtonClick:(UIButton *)sender
{
    NSInteger section = sender.tag / 100;
    NSInteger level = sender.tag % 100;
    NSString *lastLevel = @"5";
    
    UIView *fatherView = sender.superview;
    if (level == 1)
    {
        UIButton *button2 = (UIButton *)[fatherView viewWithTag:sender.tag + 1];
        UIButton *button3 = (UIButton *)[fatherView viewWithTag:sender.tag + 2];
        UIButton *button4 = (UIButton *)[fatherView viewWithTag:sender.tag + 3];
        UIButton *button5 = (UIButton *)[fatherView viewWithTag:sender.tag + 4];
        button2.selected = YES;
        button3.selected = YES;
        button4.selected = YES;
        button5.selected = YES;
        lastLevel = @"1";
    }
    else if (level == 5)
    {
        if (sender.isSelected)
        {
            UIButton *button1 = (UIButton *)[fatherView viewWithTag:sender.tag - 4];
            UIButton *button2 = (UIButton *)[fatherView viewWithTag:sender.tag - 3];
            UIButton *button3 = (UIButton *)[fatherView viewWithTag:sender.tag - 2];
            UIButton *button4 = (UIButton *)[fatherView viewWithTag:sender.tag - 1];
            button1.selected = NO;
            button2.selected = NO;
            button3.selected = NO;
            button4.selected = NO;
            sender.selected = NO;
            lastLevel = @"5";
        }
        else
        {
            sender.selected = YES;
            lastLevel = @"4";
        }
    }
    else if (level == 4)
    {
        UIButton *button1 = (UIButton *)[fatherView viewWithTag:sender.tag - 3];
        UIButton *button2 = (UIButton *)[fatherView viewWithTag:sender.tag - 2];
        UIButton *button3 = (UIButton *)[fatherView viewWithTag:sender.tag - 1];
        UIButton *button5 = (UIButton *)[fatherView viewWithTag:sender.tag + 1];
        if (sender.isSelected)
        {
            button1.selected = NO;
            button2.selected = NO;
            button3.selected = NO;
            sender.selected = NO;
            lastLevel = @"4";
        }
        else
        {
            if (button5.isSelected)
            {
                sender.selected = YES;
                lastLevel = @"3";
            }
            else
            {
                button5.selected = YES;
                lastLevel = @"4";
            }
        }
    }
    else if (level == 3)
    {
        UIButton *button1 = (UIButton *)[fatherView viewWithTag:sender.tag - 2];
        UIButton *button2 = (UIButton *)[fatherView viewWithTag:sender.tag - 1];
        UIButton *button4 = (UIButton *)[fatherView viewWithTag:sender.tag + 1];
        UIButton *button5 = (UIButton *)[fatherView viewWithTag:sender.tag + 2];
        if (sender.isSelected)
        {
            button1.selected = NO;
            button2.selected = NO;
            sender.selected = NO;
            lastLevel = @"3";
        }
        else {
            
            if (button4.isSelected) {
                
                sender.selected = YES;
                lastLevel = @"2";
            }
            else {
                
                button4.selected = YES;
                button5.selected = YES;
                lastLevel = @"3";
            }
        }
    }
    else if (level == 2) {
        
        UIButton *button1 = (UIButton *)[fatherView viewWithTag:sender.tag - 1];
        UIButton *button3 = (UIButton *)[fatherView viewWithTag:sender.tag + 1];
        UIButton *button4 = (UIButton *)[fatherView viewWithTag:sender.tag + 2];
        UIButton *button5 = (UIButton *)[fatherView viewWithTag:sender.tag + 3];
        
        if (sender.isSelected) {
            
            button1.selected = NO;
            lastLevel = @"2";
        }
        else {
            
            if (button3.isSelected) {
                
                sender.selected = YES;
                lastLevel = @"1";
            }
            else {
                
                button3.selected = YES;
                button4.selected = YES;
                button5.selected = YES;
                lastLevel = @"2";
            }
        }
    }
    
    [self.levelArr replaceObjectAtIndex:section withObject:lastLevel];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
